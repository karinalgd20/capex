﻿using System;
using System.Runtime.Serialization;


namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class UbigeoDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int Id { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CodigoDepartamento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CodigoProvincia { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CodigoDistrito { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Nombre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }

    }
}
