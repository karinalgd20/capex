﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class EmpresaDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdEmpresa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEmpresaPadre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string RazonSocial { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string RUC { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoEmpresa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Contacto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string TelefonoFijo { get; set; }
    }
}
