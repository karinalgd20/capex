using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class ModeloDtoResponse
    {

        [DataMember]  
       public int IdModelo { get; set; }
         [DataMember]
        public string CodigoModelo { get; set; }
          [DataMember]
        public string Modelo1 { get; set; }
          [DataMember]
        public string Descripcion { get; set; }
          [DataMember]
        public decimal? Monto { get; set; }
          [DataMember]
        public int? FlagDefault { get; set; }
          [DataMember]
        public int IdEstado { get; set; }
          [DataMember]
        public int IdUsuarioCreacion { get; set; }
          [DataMember]
        public DateTime FechaCreacion { get; set; }
          [DataMember]
        public int? IdUsuarioEdicion { get; set; }
          [DataMember]
        public DateTime? FechaEdicion { get; set; }
        
    }
}
