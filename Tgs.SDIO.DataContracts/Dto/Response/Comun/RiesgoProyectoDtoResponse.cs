﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;



namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class RiesgoProyectoDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdOportunidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DescripcionProbabilidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DescripcionTipoRiesgo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdTipo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FechaDeteccion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Consecuencias { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PlanAccion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Responsable { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdProbabilidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdImpacto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DescripcionImpacto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdNivel { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string FechaCierre { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Estado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
    }

    [DataContract]
    public class RiesgoProyectoPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<RiesgoProyectoDtoResponse> ListRiesgoProyecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }
}