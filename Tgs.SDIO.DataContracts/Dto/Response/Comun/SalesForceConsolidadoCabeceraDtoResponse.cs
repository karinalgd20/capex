﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class SalesForceConsolidadoCabeceraDtoResponse
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string IdOportunidad { get; set; }
        [DataMember]
        public string NumeroDelCaso { get; set; }
        [DataMember]
        public string PropietarioOportunidad { get; set; }
        [DataMember]
        public string TipologiaOportunidad { get; set; }
        [DataMember]
        public int? IdCLiente { get; set; }
        [DataMember]
        public string NombreOportunidad { get; set; }
        [DataMember]
        public DateTime? FechaCreacion { get; set; }
        [DataMember]
        public string ProbabilidadExito { get; set; }
        [DataMember]
        public int? Etapa { get; set; }
        [DataMember]
        public DateTime? FechaCierreEstimada { get; set; }
        [DataMember]
        public DateTime? FechaCierreReal { get; set; }
        [DataMember]
        public string Asunto { get; set; }
        [DataMember]
        public DateTime? FechaCreacionDB { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }
        [DataMember]
        public string LoginRegistro { get; set; }
        [DataMember]
        public string LoginUltimaModificacion { get; set; }

        [DataMember]
        public int? IdTipoCapex { get; set; }
        [DataMember]
        public int? PorcentajeRealizado { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }

        //
        [DataMember]
        public string NombreCliente { get; set; }
        [DataMember]
        public string LineaNegocio { get; set; }
        [DataMember]
        public string Madurez { get; set; }
        [DataMember]
        public decimal? IngresoTotal { get; set; }
        [DataMember]
        public decimal? Capex { get; set; }

        [DataMember]
        public decimal? Opex { get; set; }
        [DataMember]
        public decimal? Oibda { get; set; }
        [DataMember]
        public int? Oportunidades_Trabajadas { get; set; }
    }
}

