﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class SegmentoNegocioResponseDto
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdSegmentoNegocio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
    }
}
