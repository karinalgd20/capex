using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{

    [DataContract]
    public class ListaDtoResponse

    {
        [DataMember(EmitDefaultValue = false)]
        public string Codigo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }

    }
}