using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class ProveedorDtoResponse : AuditoriaDto
    {
        [DataMember]
        public int IdProveedor { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int? TipoProveedor { get; set; }
        [DataMember]
        public string RazonSocial { get; set; }
        [DataMember]
        public string CodigoProveedor { get; set; }
        [DataMember]
        public string RUC { get; set; }
        [DataMember]
        public string Pais { get; set; }
        [DataMember]
        public string IdSecuencia { get; set; }
        [DataMember]
        public string NombrePersonaContacto { get; set; }
        [DataMember]
        public string TelefonoContactoPrincipal { get; set; }
        [DataMember]
        public string CorreoContactoPrincipal { get; set; }
        [DataMember]
        public string TelefonoContactoSecundario { get; set; }
        [DataMember]
        public string CorreoContactoSecundario { get; set; }
        [DataMember]
        public string CodigoSRM { get; set; }
    }

    public class ProveedorPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<ProveedorDtoResponse> ListaProveedorPaginadoDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }
}
