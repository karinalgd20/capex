﻿using System.Runtime.Serialization;
using System;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class SalesForceConsolidadoCabeceraListarDtoResponse
    {

        [DataMember]
        public string NumeroDelCaso { get; set; }
        [DataMember]
        public string PropietarioOportunidad { get; set; }
        [DataMember]
        public string TipologiaOportunidad { get; set; }
        [DataMember]
        public string NombreCliente { get; set; }
    }
}
