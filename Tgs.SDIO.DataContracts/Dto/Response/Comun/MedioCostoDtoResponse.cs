﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class MedioCostoDtoResponse
    {

        [DataMember]
        public int IdMedioCosto { get; set; }

        [DataMember]
        public int IdMedio { get; set; }

        [DataMember]
        public int IdCosto { get; set; }
    }
}
