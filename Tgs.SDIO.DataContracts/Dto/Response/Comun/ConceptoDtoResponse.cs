﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class ConceptoDtoResponse
    {


        [DataMember]
        public int IdConcepto { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public int? IdTipoConcepto { get; set; }

        [DataMember]
        public int? Orden { get; set; }

        [DataMember]
        public int? Negrita { get; set; }

        [DataMember]
        public int IdDepreciacion { get; set; }

        [DataMember]
        public int IdEstado { get; set; }

        [DataMember]
        public string TipoConcepto { get; set; }

        [DataMember]
        public string Depreciacion { get; set; }

        [DataMember]
        public string Estado { get; set; }
    }
}
