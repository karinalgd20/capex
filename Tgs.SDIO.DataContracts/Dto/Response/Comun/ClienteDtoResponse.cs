﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class ClienteDtoResponse
    {


        [DataMember]
        public int IdCliente { get; set; }
        [DataMember]
        public string CodigoCliente { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int? IdSector { get; set; }
        [DataMember]
        public string GerenteComercial { get; set; }
        [DataMember]
        public int? IdDireccionComercial { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        [DataMember]
        public string NumeroIdentificadorFiscal { get; set; }
        [DataMember]
        public int? IdTipoIdentificadorFiscalTm { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public int? IdTipoEntidad { get; set; }


    }

    [DataContract]
    public class ClienteDtoResponsePaginado : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<ClienteDtoResponse> ListCliente { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }
    }
}
