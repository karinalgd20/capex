﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{

      [DataContract]
    public class SubServicioDtoResponse
    { 
        [DataMember]
        public int IdSubServicio { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public string DescripcionEquivalencia { get; set; }
        [DataMember]
        public int? IdDepreciacion { get; set; }
        [DataMember]
        public int? IdTipoSubServicio { get; set; }
        [DataMember]
        public int? Orden { get; set; }
        [DataMember]
        public int? Negrita { get; set; }
        [DataMember]
        public decimal? CostoInstalacion { get; set; }
        [DataMember]
        public int? IdGrupo { get; set; }
        [DataMember]
        public int?IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }
        [DataMember]
        public string Cruce { get; set; }
        [DataMember]
        public string AEReducido { get; set; }

    }
    [DataContract]
        public class SubServicioPaginadoDtoResponse : IPagedList
        {
            [DataMember(EmitDefaultValue = false)]
            public List<SubServicioDtoResponse> ListSubServicioDtoResponse { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int PageNumber { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int PageSize { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int TotalItemCount { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int PageCount { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool HasPreviousPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool HasNextPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool IsFirstPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool IsLastPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int FirstItemOnPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int LastItemOnPage { get; set; }

       
    }
}
