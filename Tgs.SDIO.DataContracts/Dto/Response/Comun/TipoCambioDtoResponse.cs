﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Entities.Entities.Base;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{

      [DataContract]
    public class TipoCambioDtoResponse   
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdTipoCambio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdLineaNegocio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Anio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Monto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Moneda { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdTipoCambioDetalle { get; set; }
        

    }
    [DataContract]
    public class TipoCambioPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<TipoCambioDtoResponse> ListTipoCambioDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }


    }
}
