﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{

      [DataContract]
    public class ServicioSubServicioDtoResponse
    {
        [DataMember]
        public int IdServicio { get; set; }
        [DataMember]
        public int? IdSubServicio { get; set; }
        [DataMember]
        public int IdServicioSubServicio { get; set; }
        [DataMember]
        public int? IdTipoCosto { get; set; }
        [DataMember]
        public int? IdProveedor { get; set; }
        [DataMember]
        public string ContratoMarco { get; set; }
        [DataMember]
        public int? IdPeriodos { get; set; }
        [DataMember]
        public int? Inicio { get; set; }
        [DataMember]
        public decimal? Ponderacion { get; set; }
        [DataMember]
        public int? IdMoneda { get; set; }
        [DataMember]
        public int? IdGrupo { get; set; }
        [DataMember]
        public int? FlagSISEGO { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }
        [DataMember]
        public int? IDTipoEnlace { get; set; }
        [DataMember]
        public int? IdActivoPasivo { get; set; }
        [DataMember]
        public int? IdLocalidad { get; set; }

        [DataMember]
        public string DescripcionSubServicio { get; set; }
        [DataMember]
        public string DescripcionProveedor { get; set; }
        [DataMember]
        public string DescripcionFlagSISEGO { get; set; }
        [DataMember]
        public string DescripcionTipoCosto { get; set; }
        [DataMember]
        public string DescripcionMoneda { get; set; }
        [DataMember]
        public string DescripcionPeriodo { get; set; }
        [DataMember]
        public string DescripcionGrupo { get; set; }

        [DataMember]
        public string Pestana { get; set; }
        [DataMember]
        public string Grupo { get; set; }

    }
    [DataContract]

    public class ServicioSubServicioPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<ServicioSubServicioDtoResponse> ListServicioSubServicioDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }


    }
}

