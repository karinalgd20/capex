﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class ServicioGrupoDtoResponse
    {
        [DataMember]
        public int IdServicioGrupo { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }
    }
}
