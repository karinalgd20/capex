﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class ActividadDtoResponse
    {

        [DataMember(EmitDefaultValue = false)]
        public int IdActividad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdActividadPadre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Predecesoras { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdFase { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEtapa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? FlgCapexMayor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? AsegurarOferta { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? FlgCapexMenor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdAreaSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? NumeroDiaCapexMenor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? CantidadDiasCapexMenor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? NumeroDiaCapexMayor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? CantidadDiasCapexMayor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionArea { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DescripcionFase { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DescripcionTipoActividad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string IdTipoActividad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Estado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ComboDtoResponse> ListSegmentoNegocio { get; set; }

    }


    [DataContract]
    public class ActividadPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<ActividadDtoResponse> ListActividadDto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }


    }
}
