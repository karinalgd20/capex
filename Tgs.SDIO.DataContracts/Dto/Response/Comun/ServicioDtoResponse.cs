﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{

      [DataContract]
    public class ServicioDtoResponse
    {
        [DataMember]
        public int IdServicio { get; set; }
        [DataMember]
        public int? IdLineaNegocio { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int? IdMedio { get; set; }
        [DataMember]
        public int? IdGrupo { get; set; }
        [DataMember]
        public int? IdServicioCMI { get; set; }
        [DataMember]
        public int? IdTipoEnlace { get; set; }
        [DataMember]
        public int?IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }
        [DataMember]
        public int IdServicioSubServicio { get; set; }

        [DataMember]
        public int IdServicioGrupo { get; set; }
        [DataMember]
        public string Estado { get; set; }
    }
    [DataContract]
        public class ServicioPaginadoDtoResponse : IPagedList
        {
            [DataMember(EmitDefaultValue = false)]
            public List<ServicioDtoResponse> ListServicioDtoResponse { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int PageNumber { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int PageSize { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int TotalItemCount { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int PageCount { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool HasPreviousPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool HasNextPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool IsFirstPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool IsLastPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int FirstItemOnPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int LastItemOnPage { get; set; }

     
    }
}
