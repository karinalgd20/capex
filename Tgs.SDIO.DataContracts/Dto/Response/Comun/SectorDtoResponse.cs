using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class SectorDtoResponse
    {

        [DataMember]
        
       public int IdSector { get; set; }
         [DataMember]
        public string Descripcion { get; set; }
          [DataMember]
        public int IdEstado { get; set; }
          [DataMember]
        public int IdUsuarioCreacion { get; set; }
          [DataMember]
        public DateTime FechaCreacion { get; set; }
          [DataMember]
        public int? IdUsuarioEdicion { get; set; }
          [DataMember]
        public DateTime? FechaEdicion { get; set; }
        
    }
}


