﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class CasoNegocioDtoResponse
    {
        [DataMember]
        public int? IdLineaNegocio { get; set; }

        [DataMember]
        public int IdCasoNegocio { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public string LineaNegocio { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public string FlagDefecto { get; set; }

        [DataMember]
        public int IdCasoNegocioServicio { get; set; }
        [DataMember]
        public string Estado { get; set; }
        
    }

    [DataContract]
    public class CasoNegocioPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<CasoNegocioDtoResponse> ListCasoNegocioDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }
}
