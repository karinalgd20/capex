﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class CCHNDtoResponse
    {

        [DataMember]
        public DateTime? FechaEdicion { get; set; }
        [DataMember]
        public string BW { get; set; }
        [DataMember]
        public decimal FEC { get; set; }
        [DataMember]
        public decimal costo { get; set; }
        [DataMember]
        public decimal inversion { get; set; }
        [DataMember]
        public decimal instalacion { get; set; }
        [DataMember]
        public decimal mantenimiento { get; set; }
    }
}
