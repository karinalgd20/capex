﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class SalesForceConsolidadoCabeceraResponse
    {
        [DataMember]
        public int? ProbabilidadExito { get; set; }

        [DataMember]
        public string Descripcion { get; set; }
    }
}
