using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;


namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public  class RecursoDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdRecurso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? TipoRecurso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string UserNameSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Nombre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DNI { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Estado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioRais { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdCargo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdArea { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEmpresa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdRolDefecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Email { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string UsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string UsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaEdicion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombreInterno { get; set; }

    }

    [DataContract]
    public class RecursoPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<RecursoDtoResponse> ListRecursoDto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }
    }
}
