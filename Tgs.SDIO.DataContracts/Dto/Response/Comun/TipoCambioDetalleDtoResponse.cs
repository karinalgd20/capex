﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Entities.Entities.Base;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{

      [DataContract]
    public class TipoCambioDetalleDtoResponse 
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdTipoCambioDetalle { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoCambio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdMoneda { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipificacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Anio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Monto { get; set; }
    }
}
