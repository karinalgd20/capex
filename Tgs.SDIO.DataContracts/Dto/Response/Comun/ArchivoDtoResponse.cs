﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class ArchivoDtoResponse
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int CodigoTabla { get; set; }

        [DataMember]
        public int IdEntidad { get; set; }

        [DataMember]
        public int? IdCategoria { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string NombreInterno { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public string Ruta { get; set; }
    }

    [DataContract]
    public class ArchivoPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<ArchivoDtoResponse> ListaArchivos { get; set; }

        #region Paginacion

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }

        #endregion
    }
}