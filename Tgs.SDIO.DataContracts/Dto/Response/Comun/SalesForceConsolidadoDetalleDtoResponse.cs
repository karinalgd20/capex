﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class SalesForceConsolidadoDetalleDtoResponse
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string IdOportunidad { get; set; }
        [DataMember]
        public string PropietarioOportunidad { get; set; }
        [DataMember]
        public string TipologiaOportunidad { get; set; }
        [DataMember]
        public int? IdCliente { get; set; }
        [DataMember]
        public string  NombreOportunidad { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public DateTime? FechaCierreEstimada { get; set; }
        [DataMember]
        public DateTime? FechaCierreReal { get; set; }
        [DataMember]
        public string ProbabilidadExito { get; set; }
        [DataMember]
        public int? Etapa { get; set; }
        [DataMember]
        public string TipoOportunidad { get; set; }
        [DataMember]
        public int? PlazoEstimadoProvision { get; set; }
        [DataMember]
        public DateTime? FechaEstimadaInstalacionServicio { get; set; }
        [DataMember]
        public int? DuracionContrato { get; set; }
        [DataMember]
        public int? IngresoPorUnicaVezDivisa { get; set; }
        [DataMember]
        public string IngresoPorUnicaVez { get; set; }
        [DataMember]
        public int? FullContractValueNetoDivisa { get; set; }
        [DataMember]
        public string FullContractValueNeto { get; set; }
        [DataMember]
        public int? RecurrenteBrutoMensualDivisa { get; set; }
        [DataMember]
        public string RecurrenteBrutoMensual { get; set; }
        [DataMember]
        public string NumeroDelCaso { get; set; }
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public string Departamento { get; set; }
        [DataMember]
        public string TipoSolicitud { get; set; }
        [DataMember]
        public string Asunto { get; set; }
        [DataMember]
        public string CasoPrincipal { get; set; }
        [DataMember]
        public DateTime? FechaHoraApertura { get; set; }
        [DataMember]
        public DateTime? FechaUltimaModificacion { get; set; }
        [DataMember]
        public string PropietarioCaso { get; set; }
        [DataMember]
        public string FuncionPropietario { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public string LoginRegistro { get; set; }
        [DataMember]
        public DateTime? FechaCreacionDB { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public string LoginUltimaModificacion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        [DataMember]
        public Boolean EstadoAccion { get; set; }
    }
}

