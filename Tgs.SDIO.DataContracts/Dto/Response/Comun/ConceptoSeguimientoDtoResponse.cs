﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;


namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class ConceptoSeguimientoDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdConcepto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdConceptoPadre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionConceptoPadre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Nivel { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? OrdenVisual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Estado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
    }

    [DataContract]
    public class ConceptoSeguimientoPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<ConceptoSeguimientoDtoResponse> ListConceptoSeguimientoDto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }
    }
}
