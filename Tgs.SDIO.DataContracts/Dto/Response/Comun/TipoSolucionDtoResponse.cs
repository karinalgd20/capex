﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{

      [DataContract]
    public class TipoSolucionDtoResponse
    { 
        [DataMember]
        public int IdTipoSolucion { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public bool Activo { get; set; }
    }
}
