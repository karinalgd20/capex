using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class ContratoMarcoDtoResponse
    {
        [DataMember]
        public int IdContratoMarco { get; set; }
        [DataMember]
        public string NumContrato { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string ProcesoAdjudicacion { get; set; }
        [DataMember]
        public DateTime? FechaInicio { get; set; }
        [DataMember]
        public DateTime? FechaFin { get; set; }
        [DataMember]
        public string Catalogado { get; set; }
        [DataMember]
        public decimal? ImporteContrato { get; set; }
        [DataMember]
        public decimal? ImporteConsumido { get; set; }
        [DataMember]
        public string Moneda { get; set; }
        [DataMember]
        public string ProductoGerencia { get; set; }
        [DataMember]
        public int? IdProveedor { get; set; }

        [DataMember]
        public string RazonSocialProveedor { get; set; }
        [DataMember]
        public string RucProveedor { get; set; }

        
    }

    [DataContract]
    public class ContratoMarcoDtoResponsePaginado : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<ContratoMarcoDtoResponse> ListaContratoMarco { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }
    }
}
