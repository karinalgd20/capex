using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

//EAAR: PF12
namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public class TableroOportunidadListarDtoResponse
    {

        [DataMember(EmitDefaultValue = false)]
        public int IdSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string OportunidadDesc { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEstadoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string EstadoOportunidadDesc { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEtapa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string EtapaDesc { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int RecursoId { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string RecursoNombre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ClienteDesc { get; set; }

         
        [DataMember(EmitDefaultValue = false)]
        public int IdActividad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ActividadDesc { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool IdEstadoEjecucion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string FechaInicioReal { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string FechaFinReal { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public Boolean Tiene { get; set; }
         
    }

    [DataContract]
    public class TableroOportunidadMatrizDtoResponse
    {
        public TableroOportunidadMatrizDtoResponse()
        {
            Oportunidades = new List<TableroOportunidadDtoResponse>();
            Actividades = new List<TableroActividadDtoResponse>();
        }
        [DataMember(EmitDefaultValue = false)]
        public List<TableroOportunidadDtoResponse> Oportunidades { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<TableroActividadDtoResponse> Actividades { get; set; }
        
    }

    [DataContract]
    public class TableroOportunidadDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string OportunidadDesc { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEstadoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string EstadoOportunidadDesc { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdEtapa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string EtapaDesc { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int RecursoId { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string RecursoNombre { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ClienteDesc { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<TableroOportunidadActividadDtoResponse> Actividades { get; set; }
        
    }

    [DataContract]
    public class TableroOportunidadActividadDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdActividad { get; set; }        
        [DataMember(EmitDefaultValue = false)]
        public bool IdEstadoEjecucion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FechaInicioReal { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string FechaFinReal { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool Tiene { get; set; }
    }

    [DataContract]
    public class TableroActividadDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdActividad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ActividadDesc { get; set; }        
    }
}
