﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public class OportunidadSTDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionEtapa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionEstadoCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionSolicitud { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdCasoSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdOportunidadPadre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadAux { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdMotivoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdFuncionPropietario { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdLineaNegocio { get; set; }        
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoEntidadCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdSector { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdSegmentoNegocio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoCicloVenta { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoCicloImplementacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Prioridad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? ProbalidadExito { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? PorcentajeCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? PorcentajeCheckList { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdFase { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEtapa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? FlgCapexMayor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ImporteCapex { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdMoneda { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ImporteFCV { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaApertura { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstadoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdRecursoComercial { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdRecursoPreventa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<ComboDtoResponse> ListSegmentoNegocio { get; set; }
    }

    [DataContract]
    public class OportunidadSTPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<OportunidadSTDtoResponse> ListOportunidadSTDto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }


    }
}
