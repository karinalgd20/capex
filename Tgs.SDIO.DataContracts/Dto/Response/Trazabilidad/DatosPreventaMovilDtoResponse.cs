using System;
using System.Runtime.Serialization;


namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public  class DatosPreventaMovilDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdDatosPreventaMovil { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdOperadorActual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? LineasActuales { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? MesesContratoActual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? RecurrenteMensualActual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? FCVActual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuServicioActual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Lineas { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? MesesContrato { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? RecurrenteMensualVR { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? FCVVR { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuServicioVR { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaPresentacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaBuenaPro { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaPresentacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaBuenaPro { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Observaciones { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? MontoCapex { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstadoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? FCVMovistar { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuTotalMovistar { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? VanVai { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? MesesRecupero { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? FCVClaro { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuTotalClaro { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? FCVEntel { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuTotalEntel { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? FCVViettel { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuTotalViettel { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuServicio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuEquipos { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? ModalidadEquipo { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? CostoPromedioEquipo { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? PorcentajeSubsidio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
    }
}
