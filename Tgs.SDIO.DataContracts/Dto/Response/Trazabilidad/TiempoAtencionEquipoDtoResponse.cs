﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public class TiempoAtencionEquipoDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdTiempoAtencion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCaso { get; set; }        
        [DataMember(EmitDefaultValue = false)]
        public string CodSisego { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdCasoSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionEquipoTrabajo { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public int? IdSisego { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdSisegoSW { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEquipoTrabajo { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaInicio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaFin { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string strFechaInicio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaFin { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasGestion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Observaciones { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Estado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }



        [DataMember(EmitDefaultValue = false)]
        public string UsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string UsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaEdicion { get; set; }
    }

    [DataContract]
    public class TiempoAtencionEquipoPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<TiempoAtencionEquipoDtoResponse> ListTiempoAtencionEquipoDto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }
}
