using System;
using System.Runtime.Serialization;


namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public  class SeguimientoActividadDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdAreaSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdActividad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstadoEjecucion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? FlgObservaciones { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? FlgFilesAdjuntos { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaFinSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstadoCumplimiento { get; set; }
    }
}
