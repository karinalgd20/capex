using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public  class ClienteDtoResponse
    {

        [DataMember(EmitDefaultValue = false)]
        public int IdCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CodigoCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdClienteGrupo { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Grupo { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string RUCCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdSector { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string GerenteComercial { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdDireccionComercial { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Lider { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string EstrategiaMovil { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string EstrategiaFija { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Campania { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public virtual SectorDtoResponse Sector { get; set; }
    }
}
