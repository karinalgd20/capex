﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    public class TipoSolicitudDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdTipoSolicitud { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int OrdenVisual { get; set; }
    }
}
