using System;
using System.Runtime.Serialization;


namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public class CasoOportunidadDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdCasoSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCasoPadre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoSolicitud { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Asunto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Complejidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Prioridad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string SolucionCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdFase { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEtapa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstadoCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaApertura { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaApertura { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCompromisoAtencion { get; set; }
    }
}
