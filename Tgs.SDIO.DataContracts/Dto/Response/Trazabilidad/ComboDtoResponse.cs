﻿using System.Runtime.Serialization;
namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public class ComboDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string label { get; set; }
    }
}
