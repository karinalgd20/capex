using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Tgs.SDIO.Util.Paginacion;


namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public  class ObservacionOportunidadDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdObservacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdConcepto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Estado { get; set; }        
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionConcepto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdCasoSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Observaciones { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string UsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string UsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<ComboDtoResponse> ListSegmentoNegocio { get; set; }
    }

    [DataContract]
    public class ObservacionOportunidadPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<ObservacionOportunidadDtoResponse> ListObservacionOportunidadDto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }


    }
}
