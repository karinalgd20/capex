﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public class OportunidadSTSeguimientoPreventaDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Asunto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionMotivoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionPreventa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionComercial { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionEstadoCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionEtapa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionTipoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionTipoSolicitud { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionSegmento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionComplejidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionTipoEntidadCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdCasoSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad2 { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? ProbalidadExito { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ImporteCapex { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaApertura { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaApertura { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdDatosPreventaMovil { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdCaso2 { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DescripcionOperadorActual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? LineasActuales { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? MesesContratoActual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? RecurrenteMensualActual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? FCVActual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuServicioActual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Lineas { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? MesesContrato { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? RecurrenteMensualVR { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? FCVVR { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuServicioVR { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaPresentacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaBuenaPro { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Observaciones { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? MontoCapex { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionEstadoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? FCVMovistar { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuTotalMovistar { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? VanVai { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? MesesRecupero { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? FCVClaro { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuTotalClaro { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? FCVEntel { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuTotalEntel { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? FCVViettel { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuTotalViettel { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuServicio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ArpuEquipos { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionModalidadEquipo { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? CostoPromedioEquipo { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? PorcentajeSubsidio { get; set; }

    }

    [DataContract]
    public class OportunidadSTSeguimientoPreventaPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<OportunidadSTSeguimientoPreventaDtoResponse> ListOportunidadSTSeguimientoPreventaDto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }


    }
}
