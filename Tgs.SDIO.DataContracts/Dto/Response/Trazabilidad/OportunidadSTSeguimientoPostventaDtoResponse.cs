﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public class OportunidadSTSeguimientoPostventaDtoResponse
    {
        
        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasDemoraOtrasArea { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdSisego { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int DiasSisego { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public int? IdCaso { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string EstadoDeCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Sede { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionEquipoTrabajo { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionTipoEntidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombreRecurso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CodSisego { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string EstadoIngresoSisego { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? DiasBandeja { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaIngreso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaInicioEje { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? TiempoatencionFechaEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCompromisoAtencion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strTiempoatencionFechaEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaCompromisoAtencion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string strFechaIngreso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaInicioEje { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionDireccion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdCasoSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }


        





        [DataMember(EmitDefaultValue = false)]
        public string strFechaInicioSisego { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaFinSisego { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasGestionSisego { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ObservacionesSisego { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaInicioPreventa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaFinPreventa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasGestionPreventa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ObservacionesPreventa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaInicioGics { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaFinGics { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasGestionGics { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ObservacionesGics { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaInicioCalidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaFinCalidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasGestionCalidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ObservacionesCalidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaInicioFinanzas { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaFinFinanzas { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasGestionFinanzas { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ObservacionesFinanzas { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaInicioPlaneamiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaFinPlaneamiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasGestionPlaneamiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ObservacionesPlaneamiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaInicioComercial { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaFinComercial { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasGestionComercial { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ObservacionesComercial { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaInicioProducto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaFinProducto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasGestionProducto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ObservacionesProducto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaInicioJProyecto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaFinJProyecto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasGestionJProyecto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ObservacionesJProyecto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaInicioProgramacionKO { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaFinProgramacionKO { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasGestionProgramacionKO { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ObservacionesProgramacionKO { get; set; }


    }

    [DataContract]
    public class OportunidadSTSeguimientoPostventaPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<OportunidadSTSeguimientoPostventaDtoResponse> ListOportunidadSTSeguimientoPostventaDto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }
    }
}
