﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class ConceptoDatosCapexDtoResponse
    {
        [DataMember]
        public int IdConcepto { get; set; }

        [DataMember]
        public string Concepto { get; set; } 

        [DataMember]
        public int IdPestana { get; set; }

        [DataMember]
        public string Pestana { get; set; }

        [DataMember]
        public int IdGrupo { get; set; }

        [DataMember]
        public string Grupo { get; set; }

        [DataMember]
        public int IdProyecto { get; set; }

        [DataMember]
        public int IdLineaProducto { get; set; }

        [DataMember]
        public int IdServicioCMI { get; set; }

        [DataMember]
        public string ServicioCMI { get; set; }

        [DataMember]
        public int IdServicioConcepto { get; set; }

        [DataMember]
        public int IdAgrupador { get; set; }

        [DataMember]
        public int IdTipoCosto { get; set; }

        [DataMember]
        public string Proyecto { get; set; }

        [DataMember]
        public decimal Ponderacion { get; set; }

        [DataMember]
        public decimal CostoPreOperativo { get; set; }

        [DataMember]
        public int IdProveedor { get; set; }

        [DataMember]
        public int IdPeriodos { get; set; }

        [DataMember]
        public string Periodo { get; set; }

        [DataMember]
        public int Inicio { get; set; }

        [DataMember]
        public int Meses { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public int IdConceptoDatosCapex { get; set; }

        [DataMember]
        public string Circuito { get; set; }

        [DataMember]
        public string SISEGO { get; set; }

        [DataMember]
        public int MesesAntiguedad { get; set; }

        [DataMember]
        public int Cantidad { get; set; }

        [DataMember]
        public decimal CostoUnitarioAntiguo { get; set; }

        [DataMember]
        public decimal ValorResidualSoles { get; set; }

        [DataMember]
        public decimal CostoUnitario { get; set; }

        [DataMember]
        public decimal CapexDolares { get; set; }

        [DataMember]
        public decimal CapexSoles { get; set; }

        [DataMember]
        public int MesRecupero { get; set; }

        [DataMember]
        public decimal TotalCapex { get; set; }

        [DataMember]
        public decimal CapexInstalacion { get; set; }

        [DataMember]
        public int AnioRecupero { get; set; }
        

        [DataMember]
        public int AnioComprometido { get; set; }

        [DataMember]
        public int MesComprometido { get; set; }

        [DataMember]
        public int AnioCertificado { get; set; }

        [DataMember]
        public int MesCertificado { get; set; }

        [DataMember]
        public string Medio { get; set; }

        [DataMember]
        public int IdTipo { get; set; }

        [DataMember]
        public int IdBWOportunidad { get; set; }

        [DataMember]
        public string Garantizado { get; set; }

        [DataMember]
        public string BM { get; set; }

        [DataMember]
        public decimal? BM_Numero { get; set; }

        [DataMember]
        public decimal? BM_Precio { get; set; }

        [DataMember]
        public string Cruce { get; set; }

        [DataMember]
        public string AEReducido { get; set; }

        [DataMember]
        public string Marca { get; set; }

        [DataMember]
        public string Modelo { get; set; }

        [DataMember]
        public string Combo { get; set; }

        [DataMember]
        public string TipoEquipo { get; set; }

        [DataMember]
        public decimal? Instalacion { get; set; }

    }
}
