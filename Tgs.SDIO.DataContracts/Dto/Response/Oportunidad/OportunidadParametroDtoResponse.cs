﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class OportunidadParametroDtoResponse
    {

        [DataMember]
        public int IdOportunidadParametro { get; set; }
        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }
        [DataMember]
        public int IdParametro { get; set; }
        [DataMember]
        public decimal Valor { get; set; }
        [DataMember]
        public string Valor2 { get; set; }
        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public string FloatValor { get; set; }
     
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

    }

    public class OportunidadParametroPaginadoDtoResponse : IPagedList
        {
            [DataMember(EmitDefaultValue = false)]
            public List<OportunidadParametroDtoResponse> ListOportunidadParametroDtoResponse { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int PageNumber { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int PageSize { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int TotalItemCount { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int PageCount { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool HasPreviousPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool HasNextPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool IsFirstPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool IsLastPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int FirstItemOnPage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int LastItemOnPage { get; set; }


       

    }
}
