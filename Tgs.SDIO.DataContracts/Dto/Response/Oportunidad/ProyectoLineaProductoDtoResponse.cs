﻿
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class ProyectoLineaProductoDtoResponse
    {
        [DataMember]
        public int IdProyecto { get; set; }

        [DataMember]
        public int IdLineaProducto { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }
    }
}
