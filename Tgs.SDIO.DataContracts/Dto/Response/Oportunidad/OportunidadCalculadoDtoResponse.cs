﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class OportunidadCalculadoDtoResponse
    {
        [DataMember]
        public int IdOportunidadCalculado { get; set; }

        [DataMember]
        public int IdConceptoFinanciero { get; set; }

        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }

        [DataMember]
        public int Anio { get; set; }

        [DataMember]
        public int Mes { get; set; }

        [DataMember]
        public decimal Monto { get; set; }

        [DataMember]
        public string TipoFicha { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }
    }
}
