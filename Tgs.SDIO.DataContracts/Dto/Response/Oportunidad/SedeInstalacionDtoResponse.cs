﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;
using System.Collections.Generic;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class SedeInstalacionDtoResponse : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdSede { get; set; }
        [DataMember]
        public int IdOportunidad { get; set; }
        [DataMember]
        public string CodSisego { get; set; }
        [DataMember]
        public string DesRequerimiento { get; set; }
        [DataMember]
        public bool? FlgCotizacionReferencial { get; set; }
        [DataMember]
        public bool? FlgRequisitosEspeciales { get; set; }

    }
}