﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class ProyectoBandejaDtoResponse
    {
        [DataMember]
        public int IdProyecto { get; set; }
        [DataMember]
        public string Proyecto { get; set; }
        [DataMember]
        public int IdLineaProducto { get; set; }
        [DataMember]
        public string LineaProducto { get; set; }
        [DataMember]
        public int IdTipoEmpresa { get; set; }
        [DataMember]
        public string TipoEmpresa { get; set; }
        [DataMember]
        public string NumeroSalesForce { get; set; }
        [DataMember]
        public string NumeroCaso { get; set; }
        [DataMember]
        public int IdCliente { get; set; }
        [DataMember]
        public string Cliente { get; set; }
        [DataMember]
        public int IdSector { get; set; }
        [DataMember]
        public string Sector { get; set; }
        [DataMember]
        public int IdGerente { get; set; }
        [DataMember]
        public int IdDireccion { get; set; }
        [DataMember]
        public string DireccionComercial { get; set; }
        [DataMember]
        public decimal OIBDA { get; set; }
        [DataMember]
        public decimal VANProyecto { get; set; }
        [DataMember]
        public decimal VANVAIProyecto { get; set; }
        [DataMember]
        public decimal PayBackProyecto { get; set; }
        [DataMember]
        public decimal ValorRescate { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public DateTime? FechaCreacion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }
        [DataMember]
        public int? IdAnalistaFinanciero { get; set; }
        [DataMember]
        public int? IdProductManager { get; set; }
        [DataMember]
        public int? IdPreVenta { get; set; }
        [DataMember]
        public int? IdUsuarioCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioModifica { get; set; }
        [DataMember]
        public string NomCompletPreventa { get; set; }
        [DataMember]
        public string NomCompletAnalistaFinanciero { get; set; }
        [DataMember]
        public string NomCompletProductManager { get; set; }
        [DataMember]
        public string NomCompletUsuCrea { get; set; }
        [DataMember]
        public string NomCompletUsuMod { get; set; }
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public int? VersionPadre { get; set; }
        [DataMember]
        public int? Version { get; set; }
        [DataMember]
        public int? Versiones { get; set; }
        [DataMember]
        public int? Agrupador { get; set; }
        [DataMember]
        public int? EstadoMin { get; set; }
        

    }
}
