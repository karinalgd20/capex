﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class OportunidadFlujoCajaConfiguracionDtoResponse
    {
        [DataMember]
        public int IdServicioCMISubServicio { get; set; }

        [DataMember]
        public int IdFlujoCajaConfiguracion { get; set; }

        [DataMember]
        public decimal? Ponderacion { get; set; }
        
        [DataMember]
        public decimal? CostoPreOperativo { get; set; }

        [DataMember]
        public int? Inicio { get; set; }

        [DataMember]
        public int? Meses { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public int IdFlujoCaja { get; set; }

        [DataMember]
        public string TipoFicha { get; set; }
        [DataMember]
        public int? IdMoneda { get; set; }

        

    }
}
