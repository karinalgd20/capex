﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class OportunidadAccesoPerfilDtoResponse
    {

        [DataMember]
        public bool TabIndicadores { get; set; }
        [DataMember]
        public bool OptionPreVenta { get; set; }
        [DataMember]
        public bool AccionGrabar { get; set; }
        [DataMember]
        public bool TabDocumento { get; set; }
        [DataMember]
        public bool OptionAnalistaFin { get; set; }
        [DataMember]
        public bool OptionCoordinadorFin { get; set; }
        [DataMember]
        public bool TabCapex { get; set; }
        [DataMember]
        public bool TabCaratula  { get; set; }
        [DataMember]
        public bool TabResumen { get; set; }
        [DataMember]
        public bool TabOCosto { get; set; }
        [DataMember]
        public bool TabFCFinanciero { get; set; }
        [DataMember]
        public bool TabFCContable { get; set; }
        [DataMember]
        public bool TabCAdicional { get; set; }

        [DataMember]
        public bool EditarDato { get; set; }
        [DataMember]
        public bool EditarDatoTable { get; set; }
        public List<ListaDtoResponse> ListaAccionPerfil { get; set; }

    }

 
}

