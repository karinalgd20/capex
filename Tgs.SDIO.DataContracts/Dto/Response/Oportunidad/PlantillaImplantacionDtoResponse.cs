﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class PlantillaImplantacionDtoResponse
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdOportunidad { get; set; }

        [DataMember]
        public int IdTipoEnlaceCircuitoDatos { get; set; }

        [DataMember]
        public int IdMedioCircuitoDatos { get; set; }

        [DataMember]
        public int IdServicioGrupoCircuitoDatos { get; set; }

        [DataMember]
        public int IdTipoCircuitoDatos { get; set; }

        [DataMember]
        public string NumeroCircuitoDatos { get; set; }

        [DataMember]
        public int IdCostoCircuitoDatos { get; set; }

        [DataMember]
        public string ConectadoCircuitoDatos { get; set; }

        [DataMember]
        public string LargaDistanciaNacionalCircuitoDatos { get; set; }

        [DataMember]
        public int IdVozTos5CaudalAntiguo { get; set; }

        [DataMember]
        public int IdVozTos4CaudalAntiguo { get; set; }

        [DataMember]
        public int IdVozTos3CaudalAntiguo { get; set; }

        [DataMember]
        public int IdVozTos2CaudalAntiguo { get; set; }

        [DataMember]
        public int IdVozTos1CaudalAntiguo { get; set; }

        [DataMember]
        public int IdVozTos0CaudalAntiguo { get; set; }

        [DataMember]
        public string UltimaMillaCircuitoDatos { get; set; }

        [DataMember]
        public string VrfCircuitoDatos { get; set; }

        [DataMember]
        public string EquipoCpeCircuitoDatos { get; set; }

        [DataMember]
        public string EquipoTerminalCircuitoDatos { get; set; }

        [DataMember]
        public int IdAccionIsis { get; set; }

        [DataMember]
        public int IdTipoEnlaceServicioOfertado { get; set; }

        [DataMember]
        public int IdMedioServicioOfertado { get; set; }

        [DataMember]
        public int IdServicioGrupoServicioOfertado { get; set; }

        [DataMember]
        public int IdCostoServicioOfertado { get; set; }

        [DataMember]
        public int IdTipoServicioOfertado { get; set; }

        [DataMember]
        public string NumeroServicioOfertado { get; set; }

        [DataMember]
        public string LargaDistanciaNacionalServicioOfertado { get; set; }

        [DataMember]
        public int IdVozTos5ServicioOfertado { get; set; }

        [DataMember]
        public int IdVozTos4ServicioOfertado { get; set; }

        [DataMember]
        public int IdVozTos3ServicioOfertado { get; set; }

        [DataMember]
        public int IdVozTos2ServicioOfertado { get; set; }

        [DataMember]
        public int IdVozTos1ServicioOfertado { get; set; }

        [DataMember]
        public int IdVozTos0ServicioOfertado { get; set; }

        [DataMember]
        public string UltimaMillaServicioOfertado { get; set; }

        [DataMember]
        public string VrfServicioOfertado { get; set; }

        [DataMember]
        public string EquipoCpeServicioOfertado { get; set; }

        [DataMember]
        public string ObservacionServicioOfertado { get; set; }

        [DataMember]
        public string EquipoTerminalServicioOfertado { get; set; }

        [DataMember]
        public string GarantizadoBw { get; set; }

        [DataMember]
        public string Propiedad { get; set; }

        [DataMember]
        public string RecursoTransporte { get; set; }

        [DataMember]
        public string RouterSwitchCentralTelefonica { get; set; }

        [DataMember]
        public string ComponentesRouter { get; set; }

        [DataMember]
        public string TipoAntena { get; set; }

        [DataMember]
        public string SegmentoSatelital { get; set; }

        [DataMember]
        public string CoordenadasUbicacion { get; set; }

        [DataMember]
        public string PozoTierra { get; set; }

        [DataMember]
        public string Ups { get; set; }

        [DataMember]
        public string EquipoTelefonico { get; set; }

        [DataMember]
        public string DescripcionTipoEnlaceCircuitoDatos { get; set; }

        [DataMember]
        public string DescripcionMedioCircuitoDatos { get; set; }

        [DataMember]
        public string DescripcionServicioGrupoCircuitoDatos { get; set; }

        [DataMember]
        public string DescripcionTipoCircuitoDatos { get; set; }

        [DataMember]
        public string DescripcionCostoCircuitoDatos { get; set; }

        [DataMember]
        public string DescripcionVozTos5CaudalAntiguo { get; set; }

        [DataMember]
        public string DescripcionVozTos4CaudalAntiguo { get; set; }

        [DataMember]
        public string DescripcionVozTos3CaudalAntiguo { get; set; }

        [DataMember]
        public string DescripcionVozTos2CaudalAntiguo { get; set; }

        [DataMember]
        public string DescripcionVozTos1CaudalAntiguo { get; set; }

        [DataMember]
        public string DescripcionVozTos0CaudalAntiguo { get; set; }

        [DataMember]
        public string DescripcionAccionIsis { get; set; }

        [DataMember]
        public string DescripcionTipoEnlaceServicioOfertado { get; set; }

        [DataMember]
        public string DescripcionMedioGrupoServicioOfertado { get; set; }

        [DataMember]
        public string DescripcionServicioGrupoServicioOfertado { get; set; }

        [DataMember]
        public string DescripcionCostoServicioOfertado { get; set; }

        [DataMember]
        public string DescripcionTipoServicioOfertado { get; set; }

        [DataMember]
        public string DescripcionVozTos5ServicioOfertado { get; set; }

        [DataMember]
        public string DescripcionVozTos4ServicioOfertado { get; set; }

        [DataMember]
        public string DescripcionVozTos3ServicioOfertado { get; set; }

        [DataMember]
        public string DescripcionVozTos2ServicioOfertado { get; set; }

        [DataMember]
        public string DescripcionVozTos1ServicioOfertado { get; set; }

        [DataMember]
        public string DescripcionVozTos0ServicioOfertado { get; set; }
    }

    public class PlantillaImplantacionPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<PlantillaImplantacionDtoResponse> ListaPlantillaImplantacion { get; set; }

        #region - Paginacion -

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }

        #endregion
    }
}