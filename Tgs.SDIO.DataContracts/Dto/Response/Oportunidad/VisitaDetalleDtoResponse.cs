﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class VisitaDetalleDtoResponse : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdVisita { get; set; }

        [DataMember]
        public DateTime FechaAcuerdo { get; set; }

        [DataMember]
        public string DescripcionPunto { get; set; }

        [DataMember]
        public string Responsable { get; set; }

        [DataMember]
        public bool Cumplimiento { get; set; }
    }

    [DataContract]
    public class VisitaDetallePaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<VisitaDetalleDtoResponse> ListaVisitasDetalle { get; set; }

        #region Paginacion

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }

        #endregion
    }
}
