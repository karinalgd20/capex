﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;
using System.Collections.Generic;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]

    public class ServicioSedeInstalacionDtoResponse : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdSedeInstalacion { get; set; }

        [DataMember]
        public int IdFlujoCaja { get; set; }
    }
}
