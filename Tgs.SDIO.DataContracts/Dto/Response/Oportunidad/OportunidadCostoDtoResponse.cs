﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class OportunidadCostoDtoResponse
    {

        [DataMember]
        public int IdOportunidadCosto { get; set; }

        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }

        [DataMember]
        public int? IdCosto { get; set; }

        [DataMember]
        public int IdTipoCosto { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public decimal Monto { get; set; }

        [DataMember]
        public decimal VelocidadSubidaKBPS { get; set; }

        [DataMember]
        public decimal PorcentajeGarantizado { get; set; }

        [DataMember]
        public decimal PorcentajeSobresuscripcion { get; set; }

        [DataMember]
        public decimal CostoSegmentoSatelital { get; set; }

        [DataMember]
        public decimal InvAntenaHubUSD { get; set; }

        [DataMember]
        public decimal AntenaCasaClienteUSD { get; set; }

        [DataMember]
        public decimal Instalacion { get; set; }

        [DataMember]
        public int IdUnidadConsumo { get; set; }

        [DataMember]
        public int IdTipificacion { get; set; }

        [DataMember]
        public string CodigoModelo { get; set; }

        [DataMember]
        public string Modelo { get; set; }

    }

    public class OportunidadCostoPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<OportunidadCostoDtoResponse> ListOportunidadCostoDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }


    }
}
