﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class OportunidadTipoCambioDtoResponse
    {

        [DataMember]
        public int IdTipoCambioOportunidad { get; set; }
        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? Anio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? Monto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Moneda { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Tipificacion { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public int IdTipoCambioDetalle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
    }

    [DataContract]
    public class OportunidadTipoCambioPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<OportunidadTipoCambioDtoResponse> ListOportunidadTipoCambioDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }


    }
}
