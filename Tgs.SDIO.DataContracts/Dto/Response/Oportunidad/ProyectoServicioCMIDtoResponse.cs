﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class ProyectoServicioCMIDtoResponse
    {
        [DataMember]
        public int IdServicioCMI { get; set; }
        [DataMember]
        public string CodigoServicioCMI { get; set; }
        [DataMember]
        public string DescripcionCMI { get; set; }
        [DataMember]
        public string DescripcionOriginal { get; set; }
        [DataMember]
        public string DescripcionPlantilla { get; set; }
        [DataMember]
        public string LineaNegocio { get; set; }
        [DataMember]
        public string SubLineaNegocio { get; set; }
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public string AnalistaUsuario { get; set; }
        [DataMember]
        public string AnalistaCorreo { get; set; }
        [DataMember]
        public string Porcentaje { get; set; }
        [DataMember]
        public int IdAnalista { get; set; }
        [DataMember]
        public string CodigoCMI { get; set; }
        [DataMember]
        public string GerenciaProducto { get; set; }
        [DataMember]
        public string Linea { get; set; }
        [DataMember]
        public string SubLinea { get; set; }
        [DataMember]
        public string ServicioCMI { get; set; }
        [DataMember]
        public string Producto_AF { get; set; }
        [DataMember]
        public string CentroCosto { get; set; }
        [DataMember]
        public string Procentaje { get; set; }
    }
}
