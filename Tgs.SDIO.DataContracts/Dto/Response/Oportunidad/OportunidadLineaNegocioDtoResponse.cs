﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
 public class OportunidadLineaNegocioDtoResponse
    {
        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }
        [DataMember]
        public int IdOportunidad { get; set; }
        [DataMember]
        public int IdLineaNegocio { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public System.DateTime FechaCreacion { get; set; }
        [DataMember]
        public Nullable<int> IdUsuarioEdicion { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaEdicion { get; set; }

        [DataMember]
        public string FlagGo { get; set; }
    }
}
