﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class OportunidadDtoResponse
    {

        [DataMember]
        public int IdOportunidad { get; set; }
        [DataMember]
        public int? IdTipoEmpresa { get; set; }
        [DataMember]
        public int? IdCliente { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string NumeroSalesForce { get; set; }
        [DataMember]
        public string NumeroCaso { get; set; }
        [DataMember]
        public DateTime Fecha { get; set; }
        [DataMember]
        public string Alcance { get; set; }
        [DataMember]
        public int? Periodo { get; set; }
        [DataMember]
        public int? TiempoImplantacion { get; set; }
        [DataMember]
        public int? IdTipoProyecto { get; set; }
        [DataMember]
        public int? IdTipoServicio { get; set; }
        [DataMember]
        public string IdProyectoAnterior { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int? IdAnalistaFinanciero { get; set; }
        [DataMember]
        public int? IdProductManager { get; set; }
        [DataMember]
        public int? IdPreVenta { get; set; }
        [DataMember]
        public int? IdCoordinadorFinanciero { get; set; }
        [DataMember]
        public int? TiempoProyecto { get; set; }
        [DataMember]
        public int? IdTipoCambio { get; set; }
        [DataMember]
        public int? Agrupador { get; set; }
        [DataMember]
        public int? Version { get; set; }
        [DataMember]
        public int? VersionPadre { get; set; }
        [DataMember]
        public int? FlagGanador { get; set; }
        [DataMember]
        public int? IdMonedaFacturacion { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaEdicion { get; set; }


        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }
        [DataMember]
        public string IdOpcion { get; set; }
        [DataMember]
        public string FechaOportunidad { get; set; }

        [DataMember]
        public int IdLineaNegocio { get; set; }
        [DataMember]
        public int IdGerente { get; set; }
        [DataMember]
        public string Oportunidad { get; set; }
        [DataMember]
        public int IdDireccion { get; set; }
        [DataMember]
        public int IdTipoConcepto { get; set; }

        [DataMember]
        public int IdServicioCMI { get; set; }

        [DataMember]
        public int IdConcepto { get; set; }

        [DataMember]
        public int IdServicioConcepto { get; set; }

        [DataMember]
        public int IdAgrupador { get; set; }

        [DataMember]
        public int IdTipoCosto { get; set; }

        [DataMember]
        public decimal Ponderacion { get; set; }
        [DataMember]
        public decimal CostoPreOperativo { get; set; }

        [DataMember]
        public int IdProveedor { get; set; }

        [DataMember]
        public int IdPeriodos { get; set; }

        [DataMember]
        public int Inicio { get; set; }

        [DataMember]
        public string TipoEmpresa { get; set; }

        [DataMember]
        public string Cliente { get; set; }

        [DataMember]
        public int IdSector { get; set; }

        [DataMember]
        public string Sector { get; set; }

        [DataMember]
        public string DireccionComercial { get; set; }

        [DataMember]
        public decimal OIBDA { get; set; }

        [DataMember]
        public decimal VANProyecto { get; set; }

        [DataMember]
        public decimal VANVAIProyecto { get; set; }

        [DataMember]
        public decimal PayBackProyecto { get; set; }

        [DataMember]
        public decimal ValorRescate { get; set; }

        [DataMember]
        public int? IdUsuarioModifica { get; set; }

        [DataMember]
        public string NomCompletPreventa { get; set; }

        [DataMember]
        public string NomCompletAnalistaFinanciero { get; set; }

        [DataMember]
        public string NomCompletProductManager { get; set; }

        [DataMember]
        public string NomCompletUsuCrea { get; set; }

        [DataMember]
        public string NomCompletUsuMod { get; set; }

        [DataMember]
        public string Estado { get; set; }

        [DataMember]
        public int? Versiones { get; set; }

        [DataMember]
        public string StrFechaCreacion { get; set; }
        [DataMember]
        public string StrFechaEdicion { get; set; }

        [DataMember]
        public int TotalRow { get; set; }

        [DataMember]
        public string FlagGo { get; set; }
    }

    [DataContract]
    public class OportunidadPaginadoDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public List<OportunidadDtoResponse> ListOportunidadPaginadoDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

    }
}
