﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class VisitaDtoResponse : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdOportunidad { get; set; }

        [DataMember]
        public string PreVentaVisitante { get; set; }

        [DataMember]
        public string ClienteVisitado { get; set; }

        [DataMember]
        public int IdTipo { get; set; }

        [DataMember]
        public DateTime Fecha { get; set; }

        [DataMember]
        public TimeSpan HoraInicio { get; set; }

        [DataMember]
        public TimeSpan HoraFin { get; set; }
    }
}