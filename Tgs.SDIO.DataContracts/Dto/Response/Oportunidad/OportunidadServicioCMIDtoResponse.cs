﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class OportunidadServicioCMIDtoResponse
    {
        [DataMember]
        public int IdOportunidadServicioCMI { get; set; }
        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }
        [DataMember]
        public int IdServicioCMI { get; set; }
        [DataMember]
        public string CodigoServicioCMI { get; set; }
        [DataMember]
        public string DescripcionCMI { get; set; }
        [DataMember]
        public string DescripcionOriginal { get; set; }
        [DataMember]
        public string DescripcionPlantilla { get; set; }
        [DataMember]
        public string LineaNegocio { get; set; }
        [DataMember]
        public string SubLineaNegocio { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public string AnalistaUsuario { get; set; }
        [DataMember]
        public string AnalistaCorreo { get; set; }
        [DataMember]
        public decimal? Porcentaje { get; set; }
        [DataMember]
        public int?IdAnalista { get; set; }
        [DataMember]
        public string CodigoCMI { get; set; }
        [DataMember]
        public string GerenciaProducto { get; set; }
        [DataMember]
        public string Linea { get; set; }
        [DataMember]
        public string SubLinea { get; set; }
        [DataMember]
        public string ServicioCMI { get; set; }
        [DataMember]
        public string Producto_AF { get; set; }
        [DataMember]
        public string CentroCosto { get; set; }
        //Paginacion
        [DataMember]
        public int TotalItemCount { get; set; }
        
    }


    [DataContract]
    public class OportunidadServicioCMIPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<OportunidadServicioCMIDtoResponse> ListOportunidadServicioCMIDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }
}
