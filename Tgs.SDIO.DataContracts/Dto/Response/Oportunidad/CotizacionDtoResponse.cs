﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;
using System;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    public class CotizacionDtoResponse : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdSedeInstalacion { get; set; }
        [DataMember]

        public string CodSisegoCotizacion { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public decimal TotalSoles { get; set; }
        [DataMember]
        public decimal TotalDolares { get; set; }
        [DataMember]
        public int Dias { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEnvioPresupuesto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaEnvioPresupuesto { get; set; }
        [DataMember]
        public int EstadoCotizacion { get; set; }
    }

    public class CotizacionPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<CotizacionDtoResponse> ListaCotizacion { get; set; }

        #region Paginacion

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }

        #endregion
    }

}
