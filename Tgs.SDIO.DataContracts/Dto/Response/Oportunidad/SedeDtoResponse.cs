﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;
using System.Collections.Generic;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class SedeDtoResponse : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdSedeInstalacion { get; set; }

        [DataMember]
        public int? IdServicioSedeInstalacion { get; set; }

        [DataMember]
        public int IdSede { get; set; }

        [DataMember]
        public string Codigo { get; set; }

        [DataMember]
        public int IdCliente { get; set; }

        [DataMember]
        public int IdTipoEnlace { get; set; }

        [DataMember]
        public int IdTipoSede { get; set; }

        [DataMember]
        public int IdAccesoCliente { get; set; }

        [DataMember]
        public int IdTendidoExterno { get; set; }

        [DataMember]
        public int? NumeroPisos { get; set; }

        [DataMember]
        public int IdUbigeo { get; set; }

        [DataMember]
        public int? IdTipoVia { get; set; }

        [DataMember]
        public string Direccion { get; set; }

        [DataMember]
        public string Servicio { get; set; }

        [DataMember]
        public string Configurado { get; set; }

        [DataMember]
        public string CodSisego { get; set; }

        [DataMember]
        public string Departamento { get; set; }

        [DataMember]
        public string Provincia { get; set; }

        [DataMember]
        public string Distrito { get; set; }

        [DataMember]
        public string Estado { get; set; }

        [DataMember]
        public string Latitud { get; set; }

        [DataMember]
        public string Longitud { get; set; }

        [DataMember]
        public int? IdTipoServicio { get; set; }

        [DataMember]
        public int? Piso { get; set; }

        [DataMember]
        public string Interior { get; set; }

        [DataMember]
        public string Manzana { get; set; }

        [DataMember]
        public string Lote { get; set; }

        [DataMember]
        public int TotalRow { get; set; }

        [DataMember]
        public string TipoSede { get; set; }

        [DataMember]
        public string DesRequerimiento { get; set; }

        [DataMember]
        public bool? FlgCotizacionReferencial { get; set; }

        [DataMember]
        public bool? FlgRequisitosEspeciales { get; set; }

        [DataMember]
        public string ServicioGrupo { get; set; }

        [DataMember]
        public string Costo { get; set; }

        [DataMember]
        public string LocalizacionSede { get; set; }

        [DataMember]
        public int IdFlujoCaja { get; set; }
    }

    [DataContract]
    public class SedePaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<SedeDtoResponse> ListSedeDto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }
    }
}