﻿using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class SituacionActualDtoResponse
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdOportunidad { get; set; }

        [DataMember]
        public int IdTipo { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        //[DataMember]
        //public ArchivoPaginadoDtoResponse Archivos { get; set; }
    }
}