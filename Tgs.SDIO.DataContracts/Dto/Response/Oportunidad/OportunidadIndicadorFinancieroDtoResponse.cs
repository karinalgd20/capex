﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    public class OportunidadIndicadorFinancieroDtoResponse
    {
        public int IdOportunidadIndicador { get; set; }
        public int IdIndicadorFinanciero { get; set; }
        public int IdOportunidadLineaNegocio { get; set; }
        public int Anio { get; set; }
        public decimal Monto { get; set; }
        public int? IdEstado { get; set; }

        public int IdUsuarioCreacion { get; set; }

        public DateTime FechaCreacion { get; set; }

        public int? IdUsuarioEdicion { get; set; }

        public DateTime? FechaEdicion { get; set; }
    }
}
