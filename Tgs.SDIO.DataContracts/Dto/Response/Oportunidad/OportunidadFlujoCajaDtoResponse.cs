﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class OportunidadFlujoCajaDtoResponse
    {
       
        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }

        [DataMember]
        public int IdLineaNegocio { get; set; }

        [DataMember]
        public int IdServicioSubServicio { get; set; }

        [DataMember]
        public int IdFlujoCaja { get; set; }

        [DataMember]
        public int? IdSubServicio { get; set; }

        [DataMember]
        public string SubServicio { get; set; }

        [DataMember]
        public decimal CostoInstalacion { get; set; }

        [DataMember]
        public int? IdProveedor { get; set; }

        [DataMember]
        public string Proveedor { get; set; }

        [DataMember]
        public int? IdTipoCosto { get; set; }

        [DataMember]
        public string TipoCosto { get; set; }

        [DataMember]
        public int? IdPeriodos { get; set; }

        [DataMember]
        public string Periodos { get; set; }

        [DataMember]
        public int? IdPestana { get; set; }

        [DataMember]
        public int? IdGrupo { get; set; }

        [DataMember]
        public int? IdCasoNegocio { get; set; }

        [DataMember]
        public int? IdServicio { get; set; }

        [DataMember]
        public int? Cantidad { get; set; }

        [DataMember]
        public decimal? CostoUnitario { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public string Estado { get; set; }

        [DataMember]
        public int? IdServicioCMI { get; set; }

        [DataMember]
        public string ServicioCMI { get; set; }

        [DataMember]
        public int? IdAgrupador { get; set; }

        [DataMember]
        public decimal? Ponderacion { get; set; }

        [DataMember]
        public decimal? CostoPreOperativo { get; set; }

        [DataMember]
        public int? Inicio { get; set; }

        [DataMember]
        public int? Meses { get; set; }

        [DataMember]
        public int IdSubServicioDatosCaratula { get; set; }

        [DataMember]
        public string Circuito { get; set; }

        [DataMember]
        public int? IdOportunidadCosto { get; set; }

        [DataMember]
        public int IdMedio { get; set; }
        [DataMember]
        public int? IdMoneda { get; set; }

        [DataMember]
        public int? IdTipoEnlace { get; set; }

        [DataMember]
        public int? IdActivoPasivo { get; set; }

        [DataMember]
        public int? FlagSISEGO   { get; set; }

        [DataMember]      
        public int? IdLocalidad { get; set; }

        [DataMember]
        public decimal? MontoUnitarioMensual { get; set; }

        [DataMember]
        public decimal? MontoTotalMensual { get; set; }

        [DataMember]
        public int? NumeroMesInicioGasto { get; set; }

        [DataMember]
        public string FlagRenovacion { get; set; }

        [DataMember]
        public decimal? Instalacion { get; set; }

        [DataMember]
        public decimal? Desinstalacion { get; set; }

        [DataMember]
        public decimal? PU { get; set; }

        [DataMember]
        public decimal? Alquiler { get; set; }

        [DataMember]
        public decimal? Factor { get; set; }

        [DataMember]
        public decimal? ValorCuota { get; set; }

        [DataMember]
        public decimal? CC { get; set; }

        [DataMember]
        public decimal? CCQProvincia { get; set; }

        [DataMember]
        public decimal? CCQBK { get; set; }

        [DataMember]
        public decimal? CCQCAPEX { get; set; }

        [DataMember]
        public decimal? TIWS { get; set; }

        [DataMember]
        public decimal? RADIO { get; set; }

        [DataMember]
        public string TipoEnlace { get; set; }

        [DataMember]
        public string ActivoPasivo { get; set; }

        [DataMember]
        public string Localidad { get; set; }

        [DataMember]
        public string Costo { get; set; }

        [DataMember]
        public decimal? VelocidadSubidaKBPS { get; set; }

        [DataMember]
        public decimal? MontoCosto { get; set; }

        [DataMember]
        public string CodigoModelo { get; set; }

        [DataMember]
        public string Modelo { get; set; }

        [DataMember]
        public string Cruce { get; set; }

        [DataMember]
        public string AEReducido { get; set; }

        [DataMember]
        public int IdSubServicioDatosCapex { get; set; }

        [DataMember]
        public string SISEGO { get; set; }

        [DataMember]
        public int? MesesAntiguedad { get; set; }

        [DataMember]
        public decimal? CostoUnitarioAntiguo { get; set; }

        [DataMember]
        public decimal? ValorResidualSoles { get; set; }

        [DataMember]
        public decimal? CapexDolares { get; set; }

        [DataMember]
        public decimal? CapexSoles { get; set; }

        [DataMember]
        public decimal? TotalCapex { get; set; }

        [DataMember]
        public int? AnioRecupero { get; set; }

        [DataMember]
        public int? MesRecupero { get; set; }

        [DataMember]
        public int? AnioComprometido { get; set; }

        [DataMember]
        public int? MesComprometido { get; set; }

        [DataMember]
        public int? AnioCertificado { get; set; }

        [DataMember]
        public int? MesCertificado { get; set; }

        [DataMember]
        public int? IdTipo { get; set; }

        [DataMember]
        public string Garantizado { get; set; }

        [DataMember]
        public string Combo { get; set; }

        [DataMember]
        public string TipoEquipo { get; set; }

        [DataMember]
        public string Marca { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public int IdFlujoCajaConfiguracion { get; set; }

        [DataMember]
        public int? IdCosto { get; set; }

        [DataMember]
        public int? NumeroMeses { get; set; }

        [DataMember]
        public int TotalRow { get; set; }

        [DataMember]
        public int TiempoProyecto { get; set; }

        [DataMember]
        public string Servicio { get; set; }
        [DataMember]
        public string ContratoMarco { get; set; }
        [DataMember]
        public int? IdTipoSubServicio { get; set; }

        [DataMember]
        public string Medio { get; set; }

        [DataMember]
        public int? AgrupadorServicio { get; set; }

        [DataMember]
        public string ServicioGrupo { get; set; }

        [DataMember]
        public string VelocidadBW { get; set; }

        [DataMember]
        public string Interfaz { get; set; }

        [DataMember]
        public string TipoServicioSISEGO { get; set; }

        [DataMember]
        public int IdServicioGrupo { get; set; }
    }

    [DataContract]
    public class OportunidadFlujoCajaPaginadoDtoResponse 
    {
        [DataMember(EmitDefaultValue = false)]
        public List<OportunidadFlujoCajaDtoResponse> ListOportunidadFlujoCajaDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

    }
}
