﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    public class EstudiosDtoResponse : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdSedeInstalacion { get; set; }
        [DataMember]
        public string Estudio { get; set; }
        [DataMember]
        public string Departamento { get; set; }
        [DataMember]
        public string TipoRequerimiento { get; set; }
        [DataMember]
        public string Nodo { get; set; }
        [DataMember]
        public int Dias { get; set; }
        [DataMember]
        public decimal TotalSoles { get; set; }
        [DataMember]
        public decimal TotalDolares { get; set; }
        [DataMember]
        public string Responsable { get; set; }
        [DataMember]
        public string FacilidadesTecnicas { get; set; }
        [DataMember]
        public string EstadoSisego { get; set; }
    }

    public class EstudiosPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<EstudiosDtoResponse> ListaEstudios { get; set; }

        #region Paginacion

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }

        #endregion
    }
}
