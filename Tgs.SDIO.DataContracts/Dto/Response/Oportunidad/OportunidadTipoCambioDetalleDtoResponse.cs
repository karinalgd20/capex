﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class OportunidadTipoCambioDetalleDtoResponse
    {

        [DataMember]
        public int IdTipoCambioDetalle { get; set; }
        [DataMember]
        public int IdTipoCambioOportunidad { get; set; }
        [DataMember]
        public int? IdMoneda { get; set; }
        [DataMember]
        public int IdTipificacion { get; set; }
        [DataMember]
        public int? Anio { get; set; }
        [DataMember]
        public decimal? Monto { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

    }
}
