﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    public class SubServicioDatosCaratulaDtoResponse
    {
        [DataMember]
        public int IdConcepto { get; set; }
        [DataMember]
        public string Concepto { get; set; }
        [DataMember]
        public int IdPestana { get; set; }
        [DataMember]
        public string Pestana { get; set; }
        [DataMember]
        public int IdGrupo { get; set; }
        [DataMember]
        public string Grupo { get; set; }
        [DataMember]
        public int IdProyecto { get; set; }
        [DataMember]
        public int IdLineaProducto { get; set; }
        [DataMember]
        public int IdServicioCMI { get; set; }
        [DataMember]
        public string ServicioCMI { get; set; }
        [DataMember]
        public int IdServicioConcepto { get; set; }
        [DataMember]
        public int IdAgrupador { get; set; }
        [DataMember]
        public int IdTipoCosto { get; set; }
        [DataMember]
        public string Proyecto { get; set; }
        [DataMember]
        public decimal Ponderacion { get; set; }
        [DataMember]
        public decimal CostoPreOperativo { get; set; }
        [DataMember]
        public int IdProveedor { get; set; }
        [DataMember]
        public int IdPeriodos { get; set; }
        [DataMember]
        public string Periodo { get; set; }
        [DataMember]
        public int Inicio { get; set; }
        [DataMember]
        public int Meses { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdConceptoDatosCaratula { get; set; }
        [DataMember]
        public string Circuito { get; set; }
        [DataMember]
        public int? IdMedio { get; set; }
        [DataMember]
        public int? IdTipoEnlace { get; set; }
        [DataMember]
        public int? IdActivoPasivo { get; set; }
        [DataMember]
        public int? IdLocalidad { get; set; }
        [DataMember]
        public int Cantidad { get; set; }
        [DataMember]
        public int? IdBM { get; set; }
        [DataMember]
        public int? NumeroMeses { get; set; }
        [DataMember]
        public decimal? MontoUnitarioMensual { get; set; }
        [DataMember]
        public decimal? MontoTotalMensual { get; set; }
        [DataMember]
        public int? NumeroMesInicioGasto { get; set; }
        [DataMember]
        public int? IdModelo { get; set; }
        [DataMember]
        public string FlagRenovacion { get; set; }
        [DataMember]
        public decimal? Instalacion { get; set; }
        [DataMember]
        public decimal? Desinstalacion { get; set; }
        [DataMember]
        public decimal? PU { get; set; }
        [DataMember]
        public decimal? Alquiler { get; set; }
        [DataMember]
        public decimal? Factor { get; set; }
        [DataMember]
        public decimal? ValorCuota { get; set; }
        [DataMember]
        public decimal? CC { get; set; }
        [DataMember]
        public decimal? CCQProvincia { get; set; }
        [DataMember]
        public decimal? CCQBK { get; set; }
        [DataMember]
        public decimal? CCQCAPEX { get; set; }
        [DataMember]
        public decimal? TIWS { get; set; }
        [DataMember]
        public decimal? RADIO { get; set; }
        [DataMember]
        public string BM { get; set; }
        [DataMember]
        public decimal? BM_Numero { get; set; }
        [DataMember]
        public decimal? BM_Precio { get; set; }
        [DataMember]
        public int Medio { get; set; }
        [DataMember]
        public string TipoEnlace { get; set; }
        [DataMember]
        public string ActivoPasivo { get; set; }
        [DataMember]
        public string Localidad { get; set; }
        [DataMember]
        public string CodigoModelo { get; set; }
        [DataMember]
        public string Modelo { get; set; }
        [DataMember]
        public decimal? MontoPxQ { get; set; }
        [DataMember]
        public string Servicio { get; set; }
        [DataMember]
        public string Tipo { get; set; }
        [DataMember]
        public int IdFlujoCaja { get; set; }
        [DataMember]
        public int IdSubServicioDatosCaratula { get; set; }
    }
}
