﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Negocios
{
    [DataContract]
    public class RPAServiciosxNroOfertaDtoResponse : AuditoriaDto
    {
        [DataMember(EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombreServicio { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Cantidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Completa { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Id_Oferta { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Contacto_Id { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int NumeroCD { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int NumeroCDK { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TipoServicio { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Velocidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Accion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ModeloTx { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TipoCircuito { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LDN { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Realtime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Video { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Caudal_oro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Caudal_plata { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Caudal_platino { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Caudal_Bronce { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Caudal_VRF { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VA_Servicio { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VA_Velocidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VA_Acción { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VA_MedioTX { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VA_TipoCircuito { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PagoUnico { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string PagoRecurrente { get; set; }
    }

    
    public class RPAServiciosxNroOfertaPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<RPAServiciosxNroOfertaDtoResponse> ListaServiciosxNroOfertaDtoResponse { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }
}
