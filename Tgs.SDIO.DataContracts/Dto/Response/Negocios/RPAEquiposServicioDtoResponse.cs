﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Negocios
{
    [DataContract]
    public class RPAEquiposServicioDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Id_Oferta { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Id_RPAServOferta { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Cod_Equipo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Tipo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Marca { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Modelo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Componente_1 { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Tipo_de_acceso { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Meses { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Con_Valor_Agregado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ValorAgregado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Equipos1 { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Equipos2 { get; set; }
    }

    public class RPAEquiposServicioPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<RPAEquiposServicioDtoResponse> ListaEquiposServicioDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }
}
