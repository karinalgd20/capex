﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Negocios
{
    [DataContract]
    public class AsociarNroOfertaSisegosDtoResponse : AuditoriaDto
    {
        [DataMember(EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdOferta { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NroOferta { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdSisego { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodSisego { get; set; }
    }

    public class AsociarOfertaSisegosPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<AsociarNroOfertaSisegosDtoResponse> ListaAsociarOfertaSisegosDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }
}
