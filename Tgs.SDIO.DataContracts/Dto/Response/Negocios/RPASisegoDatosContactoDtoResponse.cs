﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Negocios
{
    [DataContract]
    public class RPASisegoDatosContactoDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Id_RPASisegoDetalle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Contacto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Telefono_1 { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Telefono_2 { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Telefono_3 { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Telefono1_Contacto2 { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Telefono2_Contacto2 { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Telefono3_Contacto2 { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Contacto_2 { get; set; }
    }

    public class RPASisegoDatosContactoPaginadoDtoResponse: IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<RPASisegoDatosContactoDtoResponse> ListaDetallesContactoRPAPaginadoDtoResponse { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }
}
