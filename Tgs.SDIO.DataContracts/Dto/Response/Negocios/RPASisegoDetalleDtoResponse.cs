﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Negocios
{
    [DataContract]
    public class RPASisegoDetalleDtoResponse : AuditoriaDto
    {
        [DataMember(EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Id_sisego { get; set; }

        [DataMember]
        public string CodSisego { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Latitud { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Longitud { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Departamento { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Provincia { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Distrito { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Ciudad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TipoSede { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Direccion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Via { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Numero { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Nro_Piso { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Interior { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Lote { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Manzana { get; set; }
    }
    public class RPASisegoDetallePaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<RPASisegoDetalleDtoResponse> ListaSisegoDetalleDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }
}
