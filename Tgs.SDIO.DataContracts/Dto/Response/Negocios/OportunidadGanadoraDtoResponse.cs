﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Negocios
{
    [DataContract]
    public class OportunidadGanadoraDtoResponse : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string PER { get; set; }

        [DataMember]
        public string Ruc { get; set; }

        [DataMember]
        public int IdEtapa { get; set; }

        [DataMember]
        public string Nro_oferta { get; set; }

        [DataMember]
        public int Nro_version_Oferta { get; set; }

        [DataMember]
        public string codigo_sisego { get; set; }

        [DataMember]
        public string CasoDerivado { get; set; }
        [DataMember]
        public string strFechaCreacion { get; set; }
        [DataMember]
        public string Cliente { get; set; }
        [DataMember]
        public string CodigoCliente { get; set; }
        [DataMember]
        public int Manipulado { get; set; }
        [DataMember]
        public string Progreso { get; set; }
    }

    public class OportunidadGanadoraPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<OportunidadGanadoraDtoResponse> ListaOportunidadGanadoraPaginadoDtoResponse { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }

    public class BandejaOportunidadDtoResponse : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string PER { get; set; }

        [DataMember]
        public string RUC { get; set; }

        [DataMember]
        public string Cliente { get; set; }

        [DataMember]
        public string Etapa { get; set; }

        [DataMember]
        public string EstadoRPA { get; set; }

        [DataMember]
        public string Progreso { get; set; }

        [DataMember]
        public string Manipulado { get; set; }
        [DataMember]
        public string strFechaCreacion { get; set; }
    }

    public class BandejaOportunidadPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<BandejaOportunidadDtoResponse> ListaBandejaOportunidadPaginadoDtoResponse { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }

}
