﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Negocios
{
    [DataContract]
    public class AsociarServicioSisegosDtoResponse : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdServicio { get; set; }
        [DataMember]
        public string Servicio { get; set; }
        [DataMember]
        public int IdEquipo { get; set; }
        [DataMember]
        public string Equipo { get; set; }
        [DataMember]
        public int IdSisego { get; set; }
        [DataMember]
        public string CodSisego { get; set; }
        [DataMember]
        public string Departamento { get; set; }
        [DataMember]
        public string Provincia { get; set; }
        [DataMember]
        public string Distrito { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        
    }

    public class AsociarServicioSisegosPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<AsociarServicioSisegosDtoResponse> ListaAsociarServicioSisegosDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }
}
