﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Compra
{
    [DataContract]
    public class PeticionCompraDtoResponse
    {
        [DataMember]
        public int IdPeticionCompra { get; set; }
        [DataMember]
        public string CodigoPeticionCompra { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public bool? AsociadoAProyecto { get; set; }
        [DataMember]
        public int? IdTipoCompra { get; set; }
        [DataMember]
        public string TipoCompra { get; set; }
        [DataMember]
        public int? IdTipoCosto { get; set; }
        [DataMember]
        public string TipoCosto { get; set; }
        [DataMember]
        public int? IdEtapa { get; set; }
        [DataMember]
        public string Etapa { get; set; }
        [DataMember]
        public int? IdEstadoEtapa { get; set; }
        [DataMember]
        public string EstadoEtapa { get; set; }
        [DataMember]
        public int? IdLineaNegocio { get; set; }
        [DataMember]
        public string LineaNegocio { get; set; }
        [DataMember]
        public int? IdComprador { get; set; }
        [DataMember]
        public string NombreComprador { get; set; }
        [DataMember]
        public string CorreoComprador { get; set; }
        [DataMember]
        public bool? AdjuntaCotizacion { get; set; }
        [DataMember]
        public decimal? MontoCotizacion { get; set; }
        [DataMember]
        public string SubGrupoCompras { get; set; }
        [DataMember]
        public int? IdAreaSolicitante { get; set; }
        [DataMember]
        public int? IdCentroCosto { get; set; }
        [DataMember]
        public int? IdRecursoSolicitante { get; set; }
        [DataMember]
        public string Solicitante { get; set; }
        [DataMember]
        public string CorreoSolicitante { get; set; }
        [DataMember]
        public int? IdRecursoGerente { get; set; }
        [DataMember]
        public int? IdRecursoDirector { get; set; }
        [DataMember]
        public int? IdResponsablePostventa { get; set; }
        [DataMember]
        public string NombreResponsablePostventa { get; set; }
        [DataMember]
        public string CorreoResponsablePostventa { get; set; }
        [DataMember]
        public string ElementoPEP { get; set; }
        [DataMember]
        public string Grafo { get; set; }
        [DataMember]
        public string Cuenta { get; set; }
        [DataMember]
        public string DescripcionCuenta { get; set; }
        [DataMember]
        public string AreaFuncional { get; set; }
        [DataMember]
        public bool? PeticionPenalidad { get; set; }
        [DataMember]
        public bool? ArrendamientoRenting { get; set; }
        [DataMember]
        public int? IdTipoMoneda { get; set; }
        [DataMember]
        public decimal? CostoTotal { get; set; }
        [DataMember]
        public string SustentoCualitativo { get; set; }
        [DataMember]
        public string DetalleSolpe { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }
        [DataMember]
        public string CodigoPeticionCompraExtendido
        {
            get
            {
                return string.Format("{0}-{1}", "TDyM", CodigoPeticionCompra);
            }
            private set { }
        }
        [DataMember]
        public string NumContrato { get; set; }
        [DataMember]
        public int? IdContratoMarco { get; set; }
        [DataMember]
        public int? IdPrePeticionDetalle { set; get; }
        [DataMember]
        public int TotalRegistros { get; set; }
        [DataMember]
        public string Cesta { get; set; }
        //Campos Extras para Bandeja

        [DataMember]
        public int? IdCliente { get; set; }
        [DataMember]
        public string Cliente { get; set; }
        [DataMember]
        public int? IdProveedor { get; set; }
        [DataMember]
        public string Proveedor { get; set; }
        [DataMember]
        public string NumeroPedido { get; set; }
        [DataMember]
        public string Unidad { get; set; }
        [DataMember]
        public string Gerencia { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public string CodigoCeCo { get; set; }

        [DataMember]
        public int? IdProyecto { get; set; }
        [DataMember]
        public string IdProyectoLKM { get; set; }
        [DataMember]
        public string DescripcionProyecto { get; set; }
        [DataMember]
        public string NombreJefeProyecto { get; set; }
        [DataMember]
        public string Linea { get; set; }
        [DataMember]
        public string SubLinea { get; set; }
        [DataMember]
        public string Servicio { get; set; }
        [DataMember]
        public string DescripcionCompra { get; set; }
        [DataMember]
        public DateTime Fecha { get; set; }
    }

    [DataContract]
    public class PeticionCompraPaginadoDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public List<PeticionCompraDtoResponse> ListaPeticionDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }
    }

}
