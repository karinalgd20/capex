﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Compra
{
    [DataContract]
    public class PrePeticionOrdinariaDetalleDtoResponse
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdCabecera { get; set; }
        [DataMember]
        public int? Numero { get; set; }
        [DataMember]
        public int? TipoCosto { get; set; }
        [DataMember]
        public string ProductManager { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int? IdProveedor { get; set; }
        [DataMember]
        public string Moneda { get; set; }
        [DataMember]
        public decimal? Monto { get; set; }
        [DataMember]
        public string CoordinadorCompras { get; set; }
        [DataMember]
        public string CompradorAsignado { get; set; }
        [DataMember]
        public int? GrupoCompra { get; set; }
        [DataMember]
        public int? PQAdjudicado { get; set; }
        [DataMember]
        public int? PliegoTecnico { get; set; }
        [DataMember]
        public int? CotizacionAdjunta { get; set; }
        [DataMember]
        public int? PosicionesOC { get; set; }
        [DataMember]
        public DateTime? FechaEntregaOC { get; set; }
        [DataMember]
        public string Observaciones { get; set; }
        [DataMember]
        public int? IdLineaProducto { get; set; }
        
        [DataMember]
        public int IdUsuario { get; set; }

        [DataMember]
        public string RazonSocialProveedor { get; set; }
        [DataMember]
        public string RUC { get; set; }
        [DataMember]
        public string NombreContacto { get; set; }
        [DataMember]
        public string TelefonoContacto { get; set; }
        [DataMember]
        public string EmailContacto { get; set; }

        [DataMember]
        public string DescripcionTipoCosto { get; set; }
        [DataMember]
        public string DescripcionLineaProducto { get; set; }

        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int Tamanio { get; set; }

        [DataMember]
        public int TotalRegistros { get; set; }
        [DataMember]
        public int? GrupoCompraConfirmado { get; set; }
        
    }
}