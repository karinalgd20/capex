﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Compra
{
    [DataContract]
    public class AprobacionDtoResponse
    {
        [DataMember]
        public string Area { get; set; }
        [DataMember]
        public int? IdResponsable { get; set; }
        [DataMember]
        public string Responsable { get; set; }
        [DataMember]
        public DateTime? FechaAprobacion { get; set; }
        [DataMember]
        public string Estado { get; set; }
    }
}
