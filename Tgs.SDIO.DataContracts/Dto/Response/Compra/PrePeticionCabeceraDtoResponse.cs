﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Compra
{
    [DataContract]
    public class PrePeticionCabeceraDtoResponse
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int? IdCliente { get; set; }
        [DataMember]
        public int? IdProyecto { get; set; }
        [DataMember]
        public int? IdLineaCMI { get; set; }
        [DataMember]
        public string CodigoSIGO { get; set; }
        [DataMember]
        public int? Tipo { get; set; }
        [DataMember]
        public DateTime? FechaInicioProyecto { get; set; }
        [DataMember]
        public string DescripcionCliente { get; set; }
        [DataMember]
        public string GerenteComercialCliente { get; set; }
        [DataMember]
        public string DescripcionSector { get; set; }
        [DataMember]
        public string DescripcionProyecto { get; set; }
        [DataMember]
        public string Servicio { get; set; }
        [DataMember]
        public string Linea { get; set; }
        [DataMember]
        public string SubLinea { get; set; }
        [DataMember]
        public string DescripcionPrePeticion { get; set; }
        [DataMember]
        public int? TipoCosto { get; set; }
        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int Tamanio { get; set; }
        [DataMember]
        public int TotalRegistros { get; set; }
        [DataMember]
        public int? IdContratoMarco { get; set; }
        [DataMember]
        public int? CotizacionAdjunta { get; set; }
        [DataMember]
        public string ElementoPEP { get; set; }
        [DataMember]
        public string DescripcionTipoCompra { get; set; }
        [DataMember]
        public string DescripcionTipoCosto { get; set; }
        [DataMember]
        public int? IdProveedor { get; set; }
        [DataMember]
        public string TipoCompra { get; set; }
    }
}