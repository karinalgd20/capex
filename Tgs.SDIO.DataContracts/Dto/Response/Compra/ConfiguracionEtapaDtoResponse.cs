﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Compra
{
    [DataContract]
    public class ConfiguracionEtapaDtoResponse
    {
        [DataMember]
        public int IdTipoCompra { get; set; }
        [DataMember]
        public int IdTipoCosto { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int? Orden { get; set; }
    }
}
