﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Compra
{
    [DataContract]
    public class AnexoDtoResponse
    {
        [DataMember]
        public int IdAnexoPeticionCompra { get; set; }
        [DataMember]
        public int IdAnexoArchivo { get; set; }
        [DataMember]
        public int IdPeticionCompra { get; set; }
        [DataMember]
        public int IdTipoDocumento { get; set; }
        [DataMember]
        public string NombreArchivo { get; set; }
        [DataMember]
        public int IdUsuario { get; set; }

        [DataMember]
        public string DesTipoDocumento { get; set; }
    }
}