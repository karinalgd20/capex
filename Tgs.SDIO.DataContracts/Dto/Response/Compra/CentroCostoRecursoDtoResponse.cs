﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Compra
{
    [DataContract]
    public class CentroCostoRecursoDtoResponse
    {
        [DataMember]
        public int IdCentroCosto { get; set; }
        [DataMember]
        public int IdAreaSolicitante { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string CodigoCeCo { get; set; }
        [DataMember]
        public string Cebe { get; set; }
        [DataMember]
        public string Ar { get; set; }
        [DataMember]
        public int? IdResponsable { get; set; }
        [DataMember]
        public string ResponsableCeCo { get; set; }
        [DataMember]
        public int? IdGerencia { get; set; }
        [DataMember]
        public string Gerencia { get; set; }
        [DataMember]
        public int? IdGerente { get; set; }
        [DataMember]
        public string Gerente { get; set; }
        [DataMember]
        public int? IdDireccion { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public int? IdDirector { get; set; }
        [DataMember]
        public string Director { get; set; }
        [DataMember]
        public int TotalRow { get; set; }
    }

    [DataContract]
    public class CentroCostoRecursoPaginadoDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public List<CentroCostoRecursoDtoResponse> ListCeCoDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }
    }


}
