﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Compra
{
    [DataContract]
    public class EtapaPeticionCompraDtoResponse
    {
        [DataMember]
        public int IdEtapaPeticionCompra { set; get; }
        [DataMember]
        public int IdConfiguracionEtapa { get; set; }
        [DataMember]
        public int Accion { get; set; }
        [DataMember]
        public int Orden { get; set; }
        [DataMember]
        public string Etapa { get; set; }
        [DataMember]
        public int? IdEstadoEtapa { set; get; }
        [DataMember]
        public string EstadoEtapa { get; set; }
        [DataMember]
        public int? IdRecursoAtiende { set; get; }
        [DataMember]
        public string RecursoAtiende { get; set; }
        [DataMember]
        public string CorreoRecursoAtiende { get; set; }
        [DataMember]
        public DateTime FechaEstado { set; get; }
        [DataMember]
        public string FechaAsignacion1 { get; set; }
        [DataMember]
        public string FechaAsignacion2 { get; set; }
        [DataMember]
        public string ResponsableAnterior { get; set; }
        [DataMember]
        public int Observado { get; set; }
        [DataMember]
        public string Comentario { get; set; }
        [DataMember]
        public int TotalRow { get; set; }
        [DataMember]
        public long NUMERO_REGISTRO { get; set; }
    }


    [DataContract]
    public class EtapaPeticionCompraPaginadoDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public List<EtapaPeticionCompraDtoResponse> ListaEtapaDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }
    }


}
