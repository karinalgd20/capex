﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Compra
{
    [DataContract]
    public class CuentaDtoResponse
    {
        [DataMember]
        public int IdCuenta { set; get; }
        [DataMember]
        public string Cuenta { set; get; }
        [DataMember]
        public string Descripcion { set; get; }
        [DataMember]
        public string AFOST { set; get; }
        [DataMember]
        public string DescripcionACTOST { set; get; }
        [DataMember]
        public string Proceso { set; get; }
        [DataMember]
        public string FInACT { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime FechaEdicion { set; get; }
        [DataMember]
        public int TotalRow { get; set; }
    }

    [DataContract]
    public class CuentaPaginadoDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public List<CuentaDtoResponse> ListaCuentaDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }
    }

}
