﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Compra
{
    [DataContract]
    public class AnexoArchivoDtoResponse
    {
        [DataMember]
        public int IdAnexoArchivo { get; set; }
        [DataMember]
        public int IdAnexoPeticionCompras { get; set; }
        [DataMember]
        public byte[] ArchivoAdjunto { get; set; }

        [DataMember]
        public string NombreArchivo { get; set; }
    }
}