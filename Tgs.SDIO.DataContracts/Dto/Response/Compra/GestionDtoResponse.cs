﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Compra
{
    [DataContract]
    public class GestionDtoResponse
    {
        [DataMember]
        public int IdGestionPeticionCompra { get; set; }
        [DataMember]
        public int IdPeticionCompra { get; set; }
        [DataMember]
        public string ContratoMarco { get; set; }
        [DataMember]
        public string Cesta { get; set; }
        [DataMember]
        public bool? CestaLiberada { get; set; }
        [DataMember]
        public DateTime? FechaCesta { get; set; }
        [DataMember]
        public int? PosicionCesta { get; set; }
        [DataMember]
        public string NumeroPedido { get; set; }
        [DataMember]
        public DateTime? FechaPedido { get; set; }
        [DataMember]
        public int? PosicionPedido { get; set; }
        [DataMember]
        public DateTime? EnvioPedido { get; set; }
        [DataMember]
        public decimal? MontoPedido { get; set; }
        [DataMember]
        public decimal? SaldoPedido { get; set; }
        [DataMember]
        public DateTime? FechaActaRecibida { get; set; }
        [DataMember]
        public DateTime? FechaConfirmacion { get; set; }
        [DataMember]
        public DateTime? FechaAtencionGestor { get; set; }
        [DataMember]
        public int? IdTipoMoneda { get; set; }
        [DataMember]
        public string CodigoConfirmacion { get; set; }
    }
}