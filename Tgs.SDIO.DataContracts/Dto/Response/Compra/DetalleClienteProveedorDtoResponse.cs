﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Compra
{
    [DataContract]
    public class DetalleClienteProveedorDtoResponse
    {
        [DataMember]
        public int IdDetalleClientePro { get; set; }
        [DataMember]
        public int IdPeticionCompra { get; set; }
        [DataMember]
        public int IdCliente { get; set; }
        [DataMember]
        public int IdProveedor { get; set; }
        [DataMember]
        public string CodigoSap { get; set; }
        [DataMember]
        public string CodigoSrm { get; set; }
        [DataMember]
        public string NombreContacto { get; set; }
        [DataMember]
        public string EmailContacto { get; set; }
        [DataMember]
        public string TelefonoContacto { get; set; }

        [DataMember]
        public string CodigoCliente { get; set; }
        [DataMember]
        public string RazonSocialCliente { get; set; }
        [DataMember]
        public string RazonSocialProveedor { get; set; }
        [DataMember]
        public string CodigoProveedor { get; set; }
        [DataMember]
        public string RucProveedor { get; set; }
    }
}