﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Capex
{
    [DataContract]
    public class ObservacionDtoResponse
    {
        [DataMember]
        public int IdObservacion { get; set; }

        [DataMember]
        public int IdResponsable { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public int IdAsignado { get; set; }

        [DataMember]
        public string Respuesta { get; set; }

        [DataMember]
        public int IdSeccion { get; set; }

        [DataMember]
        public string Asignado { get; set; }

        [DataMember]
        public string Responsable { get; set; }
    }
}
