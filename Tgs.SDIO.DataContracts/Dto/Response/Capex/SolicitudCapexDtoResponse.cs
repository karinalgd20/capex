﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Capex
{
    [DataContract]
    public class SolicitudCapexDtoResponse
    {
        [DataMember]
        public int IdSolicitudCapex { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public string NumeroSalesForce { get; set; }

        [DataMember]
        public int? IdTipoProyecto { get; set; }

        [DataMember]
        public int? IdTipoEntidad { get; set; }

        [DataMember]
        public int? IdCliente { get; set; }

        [DataMember]
        public string Cliente { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }

        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public int IdUsuarioEdicion { get; set; }

        [DataMember]
        public DateTime? FechaEdicion { get; set; }
        [DataMember]
        public int? IdPreVenta { get; set; }

        [DataMember]
        public string NombrePreventa { get; set; }
        [DataMember]
        public string NombreJefePreventa { get; set; }

        [DataMember]
        public string CodigoPmo { get; set; }

        [DataMember]
        public string NombreCliente { get; set; }

        [DataMember]
        public string Entidad { get; set; }

    }
    [DataContract]
    public class SolicitudCapexPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<SolicitudCapexDtoResponse> ListSolicitudCapexDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }


    }
}
