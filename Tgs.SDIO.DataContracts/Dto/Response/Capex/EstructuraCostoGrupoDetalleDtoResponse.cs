﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Capex
{
    [DataContract]
    public class EstructuraCostoGrupoDetalleDtoResponse
    {
        [DataMember]
        public int IdEstructuraCostoGrupoDetalle { get; set; }

        [DataMember]
        public int? IdEstructuraCostoGrupo { get; set; }

        [DataMember]
        public int? IdServicio { get; set; }

        [DataMember]
        public int? IdConcepto { get; set; }

        [DataMember]
        public string IdUbigeo { get; set; }

        [DataMember]
        public int? IdMedioCosto { get; set; }

        [DataMember]
        public string Modelo { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public decimal? CapexSoles { get; set; }

        [DataMember]
        public decimal? CapexDolares { get; set; }

        [DataMember]
        public string Certificacion { get; set; }

        [DataMember]
        public string IdTipoGrupo { get; set; }

        [DataMember]
        public int? Cantidad { get; set; }

        [DataMember]
        public decimal? CuNuevoDolar { get; set; }

        [DataMember]
        public decimal? CapexTotalSoles { get; set; }

        [DataMember]
        public string Asignacion { get; set; }
        [DataMember]
        public int? IdGrupo { get; set; }
    }
}
