﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Capex
{
    [DataContract]
    public class TopologiaDtoResponse
    {
        [DataMember]
        public int IdTopologia { get; set; }

        [DataMember]
        public int IdEstructuraCosto { get; set; }

        [DataMember]
        public int IdMedioCosto { get; set; }

        [DataMember]
        public string Observaciones { get; set; }

    }
}
