﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Capex
{
    [DataContract]
    public class GrupoDtoResponse
    {
        [DataMember]
        public int IdGrupo { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

    }
}
