﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class CapexAccesoPerfilDtoResponse
    {

        [DataMember]
        public bool TabIndicadores { get; set; }
       
        public List<ListaDtoResponse> ListaAccionPerfil { get; set; }

    }

 
}

