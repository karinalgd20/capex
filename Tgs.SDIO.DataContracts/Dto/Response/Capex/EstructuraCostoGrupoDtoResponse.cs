﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Capex
{
    [DataContract]
    public class EstructuraCostoGrupoDtoResponse
    {
        [DataMember]
        public int IdEstructuraCostoGrupo { get; set; }

        [DataMember]
        public int IdCapexLineaNegocio { get; set; }

        [DataMember]
        public int IdGrupo { get; set; }

        [DataMember]
        public string Descripcion { get; set; }


        [DataMember]
        public string Grupo { set; get; }
        [DataMember]
        public decimal Monto { set; get; }

        [DataMember]
        public int? Cantidad { set; get; }
        [DataMember]
        public decimal? CuNuevoDolar { set; get; }
        [DataMember]
        public decimal? CapexDolares { set; get; }
        [DataMember]
        public decimal? CapexTotalSoles { set; get; }
        [DataMember]
        public string Asignacion { set; get; }
        [DataMember]
        public string Certificacion { set; get; }
        [DataMember]
        public string Modelo { set; get; }
        [DataMember]
        public string Sede { set; get; }
        [DataMember]
        public string Servicio { set; get; }
        [DataMember]
        public string Concepto { set; get; }

    }

    [DataContract]
    public class EstructuraCostoGrupoPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<EstructuraCostoGrupoDtoResponse> ListEstructuraCostoGrupoDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }


    }
}
