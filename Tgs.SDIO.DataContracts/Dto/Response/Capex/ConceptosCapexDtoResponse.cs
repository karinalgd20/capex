﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Capex
{
    [DataContract]
    public class ConceptosCapexDtoResponse
    {

        [DataMember]
        public int IdConceptosCapex { set; get; }
        [DataMember]
        public int? IdGrupo { set; get; }
        [DataMember]
        public string Descripcion { set; get; }


    }


}
