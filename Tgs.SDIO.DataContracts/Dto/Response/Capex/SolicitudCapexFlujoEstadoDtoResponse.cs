﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Capex
{
    [DataContract]
    public class SolicitudCapexFlujoEstadoDtoResponse
    {
        [DataMember]
        public int IdSolicitudCapexFlujoEstado { get; set; }

        [DataMember]
        public int IdSolicitudCapex { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public int? IdUsuarioCreacion { get; set; }
    }
}
