﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Capex
{
    [DataContract]
    public class EstructuraCostoDtoResponse
    {

        [DataMember]
        public int IdEstructuraCosto { get; set; }
        [DataMember]
        public int? IdSolicitudCapex { get; set; }
        [DataMember]
        public int? IdEstado { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }

        
       
    }


}
