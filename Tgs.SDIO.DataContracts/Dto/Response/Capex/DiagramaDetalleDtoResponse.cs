﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Capex
{
    [DataContract]
    public class DiagramaDetalleDtoResponse
    {

        [DataMember]
        public int IdDiagramaDetalle { get; set; }
        [DataMember]
        public int? IdDiagrama { get; set; }
        [DataMember]
        public int? IdConcepto { get; set; }
        [DataMember]
        public int? Orden { get; set; }
        [DataMember]
        public int? IdEstado { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }



    }


}
