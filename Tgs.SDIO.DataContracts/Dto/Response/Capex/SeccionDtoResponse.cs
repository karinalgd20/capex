﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Capex
{
    [DataContract]
    public class SeccionDtoResponse
    {
        [DataMember]
        public int IdSeccion { get; set; }

        [DataMember]
        public int IdSolicitudCapex { get; set; }

        [DataMember]
        public int IdObservacion { get; set; }

        [DataMember]
        public int IdTipoDescripcion { get; set; }

        [DataMember]
        public string Descripcion { get; set; }
    }
}
