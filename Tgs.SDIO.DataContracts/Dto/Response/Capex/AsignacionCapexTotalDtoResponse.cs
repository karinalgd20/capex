﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Capex
{
    [DataContract]
    public class AsignacionCapexTotalDtoResponse
    {

        [DataMember]
        public int IdAsignacionCapexTotal { set; get; }
        [DataMember]
        public int? IdCapexLineaNegocio { set; get; }
        [DataMember]
        public int? IdConcepto { set; get; }
        [DataMember]
        public string Descripcion { set; get; }
        [DataMember]
        public int? IdEstado { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }


    }


}
