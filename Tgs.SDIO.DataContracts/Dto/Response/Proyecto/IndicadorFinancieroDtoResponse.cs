﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Proyecto
{
    [DataContract]
    public class IndicadorFinancieroDtoResponse
    {
        [DataMember]
        public string Indicadores { get; set; }
        [DataMember]
        public string Linea_1 { get; set; }
        [DataMember]
        public string Linea_2 { get; set; }
        [DataMember]
        public string Linea_3 { get; set; }
        [DataMember]
        public string Linea_4 { get; set; }
        [DataMember]
        public string Linea_5 { get; set; }
        [DataMember]
        public string Total { get; set; }
    }
}
