﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Proyecto
{
    [DataContract]
    public class DetalleFinancieroDtoResponse
    {

        [DataMember]
        public int IdDetalleFinanciero { get; set; }

        [DataMember]
        public int IdProyecto { get; set; }

        [DataMember]
        public int IdTipoDetalle { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public string NroOportunidad { get; set; }

        [DataMember]
        public string NroCaso { get; set; }

        [DataMember]
        public string TipoProyecto { get; set; }

        [DataMember]
        public string Linea { get; set; }

        [DataMember]
        public string SubLinea { get; set; }

        [DataMember]
        public string Servicio { get; set; }

        [DataMember]
        public string Producto { get; set; }

        [DataMember]
        public string ProductoAf { get; set; }

        [DataMember]
        public string Porcentaje { get; set; }
        [DataMember]
        public string PorcentajeCalculado { get; set; }
        [DataMember]
        public string AnalistaCGUsuarioLotus { get; set; }

        [DataMember]
        public string Costo { get; set; }

        [DataMember]
        public string ElementoPEP { get; set; }

        [DataMember]
        public string CentroGestor { get; set; }

        [DataMember]
        public string CeCo { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public string Estado { get; set; }

        [DataMember]
        public string CodigoProyecto { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }

        [DataMember]
        public string UsuarioCreacion { get; set; }

        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public decimal ImporteVentas { get; set; }

        [DataMember]
        public decimal CostoOpex { get; set; }

        [DataMember]
        public decimal CostoCapex { get; set; }
        [DataMember]
        public string CodigoCMI { get; set; }
    }
}
