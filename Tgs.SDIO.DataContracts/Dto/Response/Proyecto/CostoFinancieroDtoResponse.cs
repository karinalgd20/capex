﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Proyecto
{
    [DataContract]
    public class CostoFinancieroDtoResponse
    {
        [DataMember]
        public string Costo { get; set; }
        [DataMember]
        public string Centrales_Alquiler { get; set; }
        [DataMember]
        public string Info_Internet { get; set; }
        [DataMember]
        public string Ip_Adsl { get; set; }
        [DataMember]
        public string Ip_Metro { get; set; }
        [DataMember]
        public string Ip_Vpn { get; set; }
        [DataMember]
        public string Total { get; set; }
    }
}
