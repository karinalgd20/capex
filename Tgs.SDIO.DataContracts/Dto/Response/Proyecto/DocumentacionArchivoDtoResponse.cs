﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Proyecto
{
    [DataContract]
    public class DocumentacionArchivoDtoResponse
    {
        [DataMember]
        public int IdDocumentoArchivo { get; set; }
        [DataMember]
        public int IdDocumentoProyecto { get; set; }
        [DataMember]
        public byte[] ArchivoAdjunto { get; set; }
        [DataMember]
        public string NombreArchivo { get; set; }
    }
}