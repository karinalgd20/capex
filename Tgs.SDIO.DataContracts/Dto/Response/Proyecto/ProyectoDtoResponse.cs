﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Proyecto
{
    [DataContract]
    public class ProyectoDtoResponse
    {
        [DataMember]
        public int IdProyecto { get; set; }
        [DataMember]
        public int? IdProyectoPadre { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string IdProyectoSF { get; set; }
        [DataMember]
        public string IdProyectoLKM { get; set; }
        [DataMember]
        public string IdOportunidadSF { get; set; }
        [DataMember]
        public string IdProyectoTDP { get; set; }
        [DataMember]
        public int? IdPortafolio { get; set; }
        [DataMember]
        public int? IdPrograma { get; set; }
        [DataMember]
        public int? IdEmpresa { get; set; }
        [DataMember]
        public int? IdAreaEmpresa { get; set; }
        [DataMember]
        public int? Prioridad { get; set; }
        [DataMember]
        public int? IdCliente { get; set; }
        [DataMember]
        public int? IdComplejidad { get; set; }
        [DataMember]
        public int? IdClaseProyecto { get; set; }
        [DataMember]
        public int? IdFaseActual { get; set; }
        [DataMember]
        public int? IdEtapaActual { get; set; }
        [DataMember]
        public int? EsLicitacion { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public DateTime? FechaOportunidadGanada { get; set; }
        [DataMember]
        public DateTime? FechaAceptacion { get; set; }
        [DataMember]
        public DateTime? FechaFirmaContrato { get; set; }
        [DataMember]
        public int? IdMonedaPagoUnico { get; set; }
        [DataMember]
        public decimal? TotalPagoUnico { get; set; }
        [DataMember]
        public int? IdMonedaRecurrenteMensual { get; set; }
        [DataMember]
        public decimal? TotalRecurrenteMensual { get; set; }
        [DataMember]
        public int? IdMoneda { get; set; }
        [DataMember]
        public decimal? Van { get; set; }
        [DataMember]
        public decimal? VanVai { get; set; }
        [DataMember]
        public decimal? Oibda { get; set; }
        [DataMember]
        public decimal? MargenOibda { get; set; }
        [DataMember]
        public decimal? PayBack { get; set; }
        [DataMember]
        public int TotalRegistros { get; set; }
        [DataMember]
        public int? IdOficinaOportunidad { get; set; }
        [DataMember]
        public int? IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime? FechaCrea { get; set; }
        [DataMember]
        public int? IdUsuarioModifica { get; set; }
        [DataMember]
        public DateTime? FechaModifica { get; set; }

        [DataMember]
        public string CodigoProyectoExtendido
        {
            get
            {
                return string.Format("{0}-{1}", "PY", IdProyectoLKM);
            }

            private set { }
        }

        [DataMember]
        public string NombreCliente { get; set; }
        [DataMember]
        public string DescripcionEtapa { get; set; }
        [DataMember]
        public string ResposanbleEtapa { get; set; }
        [DataMember]
        public string JefeProyecto { get; set; }
        [DataMember]
        public int IdJefeProyecto { get; set; }
        [DataMember]
        public int IdAsignacionJP { get; set; }

        [DataMember]
        public int? TipoEntidad { get; set; }

        [DataMember]
        public string Entidad { get; set; }
        [DataMember]
        public int TieneLineaCMI { get; set; }
    }
}
