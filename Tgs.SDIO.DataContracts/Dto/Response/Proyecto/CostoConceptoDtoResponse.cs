﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Proyecto
{
    [DataContract]
    public class CostoConceptoDtoResponse
    {

        [DataMember]
        public string Concepto { get; set; }

        [DataMember]
        public string CostoPropio { get; set; }

        [DataMember]
        public string CostoTercero { get; set; }
    }
}
