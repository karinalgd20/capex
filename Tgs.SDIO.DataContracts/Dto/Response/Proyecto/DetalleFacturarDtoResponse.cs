﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Proyecto
{
    [DataContract]
    public class DetalleFacturarDtoResponse
    {
        [DataMember]
        public int IdDetalleFacturar { get; set; }
        [DataMember]
        public int? IdProyecto { get; set; }
        [DataMember]
        public int? IdConceptoIngresoEgreso { get; set; }
        [DataMember]
        public int? IdFormaPago { get; set; }
        [DataMember]
        public int? IdMonedaPagoUnico { get; set; }
        [DataMember]
        public string MonedaPagoUnico { get; set; }
        [DataMember]
        public decimal? MontoPagoUnico { get; set; }
        [DataMember]
        public int? IdMonedaRecurrenteMensual { get; set; }
        [DataMember]
        public string MonedaRecurrenteMensual { get; set; }
        [DataMember]
        public decimal? MontoRecurrenteMensual { get; set; }
        [DataMember]
        public int? NumeroMeses { get; set; }
        [DataMember]
        public decimal? TasaTipoCambio { get; set; }
        [DataMember]
        public string Observaciones { get; set; }

    }
}
