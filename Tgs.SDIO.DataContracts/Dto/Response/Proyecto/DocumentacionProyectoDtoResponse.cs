﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Proyecto
{
    [DataContract]
    public class DocumentacionProyectoDtoResponse
    {
        [DataMember]
        public int IdDocumentoProyecto { get; set; }
        [DataMember]
        public int? IdProyecto { get; set; }
        [DataMember]
        public int? IdDocumento { get; set; }
        [DataMember]
        public string NombreArchivo { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        [DataMember]
        public bool Modificado { get; set; }

        [DataMember]
        public string DesTipoDocumento { get; set; }
        [DataMember]
        public byte[] ArchivoAdjunto { get; set; }
        [DataMember]
        public int IdDocumentoArchivo { get; set; }
    }
}