﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Proyecto
{
    [DataContract]
    public class OportunidadGanadaDtoResponse
    {
        [DataMember]
        public int IdCliente { get; set; }
        [DataMember]
        public string Cliente { get; set; }
        [DataMember]
        public string IdOportunidad { get; set; }
        [DataMember]
        public DateTime FechaCierreReal { get; set; }
        [DataMember]
        public string Etapa { get; set; }
        [DataMember]
        public DateTime FechaGanada { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string GerenteComercial { get; set; }
        [DataMember]
        public int TipoRespuesta { get; set; }
        [DataMember]
        public string Mensaje { get; set; }

    }
}
