﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Proyecto
{
    [DataContract]
    public class DetalleSolucionDtoResponse
    {
        [DataMember]
        public int IdDetalleSolucion { get; set; }
        [DataMember]
        public int IdProyecto { get; set; }
        [DataMember]
        public int IdTipoSolucion { get; set; }
    }
}
