﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class ListarLineasNegocioDtoResponse
    {
        [DataMember]
        public int? Orden { get; set; }

        [DataMember]
        public int? Anio { get; set; }

        [DataMember]
        public int? IdLineaNegocio { get; set; }

        [DataMember]
        public string LineaNegocio { get; set; }

        public int? CantidadClientes { get; set; }

        [DataMember]
        public string CantidadClientesCadena { get; set; }

        [DataMember]
        public int? OportunidadesAbiertasCliente { get; set; }

        public decimal? IngresoTotal { get; set; }

        [DataMember]
        public string IngresoTotalCadena { get; set; }

        public decimal? IngresoCapex { get; set; }

        [DataMember]
        public string IngresoCapexTotal { get; set; }

        [DataMember]
        public decimal? CostosDirectos { get; set; }

        [DataMember]
        public int? TotalRow { get; set; }

    }
}

