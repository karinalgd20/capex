﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class IndicadorDashboardPreventaConsolidadoDtoResponse : PaginacionDto
    {
        [DataMember]
        public IEnumerable<IndicadorDashboardPreventaDtoResponse> ListaOportunidades { get; set; }
    }
}
