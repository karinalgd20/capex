﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    public class DashoardOfertasEstadoDtoResponse
    {
        public int Anio { get; set; }
        public int Mes { get; set; }
        public string Etapa { get; set; }
        public int Cantidad { get; set; }
    }
}
