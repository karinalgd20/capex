﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class IndicadorRentabilidadDtoResponse
    {
        [DataMember]
        public int? Anio { get; set; }

        [DataMember]
        public int? IdGerente { get; set; }

        [DataMember]
        public int? IdAnalistaFinanciero { get; set; }

        [DataMember]
        public string AnalistaFinanciero { get; set; }

        public int CarteraClientesLider { get; set; }

        [DataMember]
        public string CarteraClientesLiderCadena { get; set; }

        public decimal? OportunidadesTrabajadas { get; set; }

        [DataMember]
        public string OportunidadesCadena { get; set; }

        [DataMember]
        public string IdOportunidad { get; set; }

        [DataMember]
        public decimal? TiempoEntregaPromedio { get; set; }

        [DataMember]
        public string TiempoEntregaPromedioCadena { get; set; }

        public decimal? PorcentajeCumplimiento { get; set; }

        [DataMember]
        public string PorcentajeCumplimientoCadena { get; set; }

        [DataMember]
        public int? TotalRow { get; set; }

        [DataMember]
        public int? Orden { get; set; }

        [DataMember]
        public int? OrdenMostrar { get; set; }

        [DataMember]
        public int? IdCliente { get; set; }

        [DataMember]
        public string Cliente { get; set; }

        [DataMember]
        public int? OportunidadesAbiertasCliente { get; set; }


        [DataMember]
        public int? OfertasTrabajadasCliente { get; set; }

        [DataMember]
        public int? OfertasGanadasCliente { get; set; }

        [DataMember]
        public decimal? TiempoEntregaPromedioCliente { get; set; }

        [DataMember]
        public decimal? PorcentajeCumplimientoCliente { get; set; }

        [DataMember]
        public int? Nivel { get; set; }

        [DataMember]
        public string TiempoEntregaPromedioClienteCadena { get; set; }

        [DataMember]
        public string CumplimientoEntregaClienteCadena { get; set; }

        [DataMember]
        public int? Desplegado { get; set; }

        [DataMember]
        public string CantidadOfertasClientes { get; set; }

        [DataMember]
        public string FaseOportunidad { get; set; }

        public int ProbabilidadExito { get; set; }

        [DataMember]
        public string ProbabilidadExitoCadena { get; set; }

        public DateTime? FechaCierreEstimada { get; set; }

        [DataMember]
        public string FechaCierreEstimadaCadena { get; set; }

        public decimal? Oibda { get; set; }

        [DataMember]
        public string OibdaCadena { get; set; }

        public decimal? FlujoCaja { get; set; }

        [DataMember]
        public string FlujoCajaCadena { get; set; }

        public decimal? FullContractValueNeto { get; set; }

        [DataMember]
        public string FullContractValueNetoCadena { get; set; }

        [DataMember]
        public string PayBack { get; set; }

        [DataMember]
        public string PagoUnicoDolares { get; set; }

        [DataMember]
        public string PagoRecurrenteDolares { get; set; }

        public decimal? MesesPagoRecurrente { get; set; }

        [DataMember]
        public string MesesPagoRecurrenteCadena { get; set; }

        public decimal? TipoCambio { get; set; }

        [DataMember]
        public string TipoCambioCadena { get; set; }

        [DataMember]
        public string NumeroCaso { get; set; }

        [DataMember]
        public string Preventa { get; set; }

        [DataMember]
        public int DiasAtencionPreventa { get; set; }

        [DataMember]
        public int GraficoEvolutivo { get; set; } = 0;

        public int DiasAtencionFinanzas { get; set; }

        public decimal? Capex { get; set; }

        public decimal? CostosOpex { get; set; }

        public decimal? ImporteVentas { get; set; }
    }
}

