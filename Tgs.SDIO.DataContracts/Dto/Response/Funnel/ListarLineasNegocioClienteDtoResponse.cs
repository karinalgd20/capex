﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class ListarLineasNegocioClienteDtoResponse
    {
        [DataMember]
        public int? Anio { get; set; }

        [DataMember]
        public int? IdLineaNegocio { get; set; }

        [DataMember]
        public int? IdCliente { get; set; }

        [DataMember]
        public string Cliente { get; set; }

        [DataMember]
        public int? OportunidadesAbiertasCliente { get; set; }

        [DataMember]
        public int? OfertasTrabajadasCliente { get; set; }

        [DataMember]
        public int? OfertasGanadasCliente { get; set; }

        [DataMember]
        public decimal? TotalIngresos { get; set; }

        [DataMember]
        public decimal? TotalCapex { get; set; }

        [DataMember]
        public decimal? TotalOpex { get; set; }
    }
}

