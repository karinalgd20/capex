﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    public class DashoardIngresosSectorDtoResponse
    {
        public int Anio { get; set; }
        public string Mes { get; set; }
        public string Sector { get; set; }
        public decimal IngresoTotal { get; set; }
    }
}
