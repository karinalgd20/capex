﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class IndicadorLineasNegocioListadoDtoResponse
    {
        [DataMember]
        public int? Orden { get; set; }

        [DataMember]
        public int? OrdenMostrar { get; set; }

        [DataMember]
        public int? Anio { get; set; }

        [DataMember]
        public int? IdLineaNegocio { get; set; }

        [DataMember]
        public string LineaNegocio { get; set; }

        public int? CantidadClientes { get; set; }

        [DataMember]
        public string CantidadClientesCadena { get; set; }

        public int? OportunidadesAbiertasCliente { get; set; }

        [DataMember]
        public string OportunidadesAbiertasClienteCadena { get; set; }

        [DataMember]
        public string NumeroCaso { get; set; }

        public decimal? IngresoTotal { get; set; }

        [DataMember]
        public string IngresoTotalCadena { get; set; }

        public decimal? IngresoCapex { get; set; }

        [DataMember]
        public string IngresoCapexCadena { get; set; }

        public decimal? CostosDirectos { get; set; }

        [DataMember]
        public string CostosDirectosCadena { get; set; }

        [DataMember]
        public int? TotalRow { get; set; }

        [DataMember]
        public int? Nivel { get; set; }

        [DataMember]
        public int? Desplegado { get; set; }

        [DataMember]
        public int? IdCliente { get; set; }

        public int? OfertasTrabajadasCliente { get; set; }

        [DataMember]
        public string OfertasTrabajadasClienteCadena { get; set; }

        public int? OfertasGanadasCliente { get; set; }

        [DataMember]
        public string OfertasGanadasClienteCadena { get; set; }

        [DataMember]
        public string IdOportunidad { get; set; }

        [DataMember]
        public string FaseOportunidad { get; set; }

        [DataMember]
        public string ProbabilidadExito { get; set; }

        public DateTime? FechaCierreEstimada { get; set; }

        [DataMember]
        public string FechaCierreEstimadaCadena { get; set; }

        public decimal? Oidba { get; set; }

        [DataMember]
        public string OidbaCadena { get; set; }

        public decimal? FlujoCaja { get; set; }

        [DataMember]
        public string FlujoCajaCadena { get; set; }

        public decimal? FullContractValueNeto { get; set; }

        [DataMember]
        public string FullContractValueNetoCadena { get; set; }

        [DataMember]
        public string PayBack { get; set; }

        [DataMember]
        public string PagoUnicoDolares { get; set; }

        [DataMember]
        public string PagoRecurrenteDolares { get; set; }

        public decimal? MesesPagoRecurrente { get; set; }

        [DataMember]
        public string MesesPagoRecurrenteCadena { get; set; }

        public decimal? TipoCambio { get; set; }

        [DataMember]
        public string TipoCambioCadena { get; set; }

        [DataMember]
        public int? GraficoEvolutivo { get; set; } = 0;

    }
}

