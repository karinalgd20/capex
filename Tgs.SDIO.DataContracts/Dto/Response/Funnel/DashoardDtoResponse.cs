﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Funciones;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class DashoardDtoResponse
    {
        [DataMember]
        public List<DashoardMadurezDtoResponse> ListaMadurez { get; set; }

        [DataMember]
        public decimal? OportunidadesTrabajadas { get; set; }
        public decimal? IngresoTotal { get; set; }

        [DataMember]
        public string IngresoTotalMostrar
        {
            get
            {
                return Funciones.MostrarFormatoMoneda(IngresoTotal);
            }
            set
            {
            }
        }

        public decimal? CapexTotal { get; set; }

        [DataMember]
        public string CapexTotalMostrar {
            get
            {
                return Funciones.MostrarFormatoMoneda(CapexTotal);
            }
            set
            {
            }
        }
        public decimal? OpexTotal { get; set; }

        [DataMember]
        public string OpexTotalMostrar
        {
            get
            {
                return Funciones.MostrarFormatoMoneda(OpexTotal);
            }
            set
            {
            }
        }

        public decimal? OibdaTotal { get; set; }

        [DataMember]
        public string OibdaTotalMostrar
            {
            get
            {
                return Funciones.MostrarFormatoMoneda(OibdaTotal);
            }
            set
            {}
        }

        [DataMember]
        public List<DashoardAtencionDtoResponse>  EstadosAtencion { get; set; }

        [DataMember]
        public List<DashoardOfertasLineaNegocioDtoResponse> OfertasLineaNegocio { get; set; }

        [DataMember]
        public List<DashoardOfertasSectorDtoResponse> OfertasSector { get; set; }

        [DataMember]
        public List<DashoardIngresosLineaNegocioDtoResponse> IngresosLineaNegocio { get; set; }

        [DataMember]
        public List<DashoardIngresosSectorDtoResponse> IngresosSector { get; set; }

        [DataMember]
        public List<DashoardOfertasEstadoDtoResponse> OfertasEstado { get; set; }
    }
}
