﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    public class DashoardAtencionDtoResponse
    {
        public string Estado { get; set; }
        public int NroRegistros { get; set; }
    }
}
