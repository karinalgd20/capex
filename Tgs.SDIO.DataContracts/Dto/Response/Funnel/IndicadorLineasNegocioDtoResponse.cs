﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    public class IndicadorLineasNegocioDtoResponse : PaginacionDto
    {
        public IEnumerable<IndicadorLineasNegocioListadoDtoResponse> Lineas { get; set; }
    }
}
