﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    public class DashoardMadurezDtoResponse
    {
        public int? Madurez { get; set; }

        public string Mes { get; set; }

        public int Anio { get; set; }

        public string IdOportunidad { get; set; }

        public decimal? Capex { get; set; }
    }
}
