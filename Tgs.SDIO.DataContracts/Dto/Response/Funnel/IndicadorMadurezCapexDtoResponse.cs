﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class IndicadorMadurezCapexDtoResponse
    {
        [DataMember]
        public string LineaNegocio { get; set; }
        [DataMember]
        public decimal? Capex1 { get; set; }

        [DataMember]
        public decimal? Capex2 { get; set; }

        [DataMember]
        public decimal? Capex3 { get; set; }

        [DataMember]
        public decimal? Capex4 { get; set; }

        [DataMember]
        public decimal? Capex5 { get; set; }
    }
}
