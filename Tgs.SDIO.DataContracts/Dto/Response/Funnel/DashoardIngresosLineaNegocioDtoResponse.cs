﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    public class DashoardIngresosLineaNegocioDtoResponse
    {
        public int Anio { get; set; }

        public string Mes { get; set; }

        public string LineaNegocio { get; set; }

        public decimal IngresoTotal { get; set; }
    }
}
