﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class OportunidadFinancieraOrigenDtoResponse
    {
        public int? Madurez { get; set; }

        [DataMember]
        public string MadurezCadena
        {
            get {

                if (Madurez == null)
                {
                    return null;
                }

                return string.Concat("N", Madurez);
            }
            set { }
        }

        [DataMember]
        public string Mes { get; set; }

        [DataMember]
        public decimal? MesOrden { get; set; }

        [DataMember]
        public string IdOportunidad { get; set; }
        [DataMember]
        public string LineaNegocio { get; set; }
        [DataMember]
        public string Sector { get; set; }
        [DataMember]
        public string NombreCliente { get; set; }
        [DataMember]
        public string CodigoCliente { get; set; }
        [DataMember]
        public decimal? IngresoTotal { get; set; }
        [DataMember]
        public decimal? Capex { get; set; }
        [DataMember]
        public decimal? Opex { get; set; }

        [DataMember]
        public decimal? Oibda { get; set; }

    }
}

