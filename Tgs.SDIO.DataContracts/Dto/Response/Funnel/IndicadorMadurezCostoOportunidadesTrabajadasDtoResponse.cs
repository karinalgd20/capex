﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class IndicadorMadurezCostoOportunidadesTrabajadasDtoResponse
    {
        [DataMember]
        public string LineaNegocio { get; set; }
        [DataMember]
        public int OportunidadesTrabajadasN1 { get; set; }

        [DataMember]
        public int OportunidadesTrabajadasN2 { get; set; }

        [DataMember]
        public int OportunidadesTrabajadasN3 { get; set; }

        [DataMember]
        public int OportunidadesTrabajadasN4 { get; set; }

        [DataMember]
        public int OportunidadesTrabajadasN5 { get; set; }

    }
}
