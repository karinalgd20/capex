﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class IndicadorMadurezOidbaDtoResponse
    {
        [DataMember]
        public string LineaNegocio { get; set; }
        [DataMember]
        public decimal? Oidba1 { get; set; }

        [DataMember]
        public decimal? Oidba2 { get; set; }

        [DataMember]
        public decimal? Oidba3 { get; set; }

        [DataMember]
        public decimal? Oidba4 { get; set; }

        [DataMember]
        public decimal? Oidba5 { get; set; }
    }
}

