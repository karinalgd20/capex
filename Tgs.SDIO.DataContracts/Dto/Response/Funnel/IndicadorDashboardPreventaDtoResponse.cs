﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class IndicadorDashboardPreventaDtoResponse
    {
        [DataMember]
        public int? IdSector { get; set; }

        [DataMember]
        public string Sector { get; set; }

        [DataMember]
        public int? IdLider { get; set; }

        [DataMember]
        public string Lider { get; set; }

        public int? EquipoPreventaLider { get; set; }

        [DataMember]
        public string EquipoPreventaLiderCadena { get; set; }

        public int? CarteraClientesLider { get; set; }

        [DataMember]
        public string CarteraClientesLiderCadena { get; set; }

        public int? NroOportunidadesLider { get; set; }

        [DataMember]
        public string NroOportunidadesLiderCadena { get; set; }

        public decimal? WinRateLider { get; set; }

        [DataMember]
        public string WinRateLiderCadena { get; set; }

        public decimal? TiempoEntregaPromedioLider { get; set; }

        [DataMember]
        public string TiempoEntregaPromedioLiderCadena { get; set; }


        public decimal? PorcentajeCumplimientoLider { get; set; }

        [DataMember]
        public string PorcentajeCumplimientoLiderCadena { get; set; }

        [DataMember]
        public int? IdPreventa { get; set; }

        [DataMember]
        public string Preventa { get; set; }

        [DataMember]
        public int? CantidadClientesPreventa { get; set; }

        [DataMember]
        public string CantidadClientesPreventaCadena { get; set; }

        [DataMember]
        public int? OportunidadesPreventa { get; set; }

        [DataMember]
        public string OportunidadesPreventaCadena { get; set; }

        [DataMember]
        public decimal? WinRatePreventa { get; set; }

        [DataMember]
        public string WinRatePreventaCadena { get; set; }

        [DataMember]
        public int? TiempoEntregaPromedioPreventa { get; set; }

        [DataMember]
        public string TiempoEntregaPromedioPreventaCadena { get; set; }

        [DataMember]
        public decimal? PorcentajeCumplimientoPreventa { get; set; }

        [DataMember]
        public string PorcentajeCumplimientoPreventaCadena { get; set; }

        [DataMember]
        public decimal? IspPreventa { get; set; }

        [DataMember]
        public string IspPreventaCadena { get; set; }

        [DataMember]
        public int? IdCliente { get; set; }

        [DataMember]
        public string Cliente { get; set; }

        [DataMember]
        public int? OportunidadesAbiertasCliente { get; set; }

        [DataMember]
        public string OportunidadesAbiertasClienteCadena { get; set; }

        [DataMember]
        public int? OfertasVigentesCliente { get; set; }

        [DataMember]
        public string OfertasVigentesClienteCadena { get; set; }

        [DataMember]
        public int? OfertasTrabajadasCliente { get; set; }

        [DataMember]
        public string OfertasTrabajadasClienteCadena { get; set; }

        [DataMember]
        public int? OfertasGanadasCliente { get; set; }

        [DataMember]
        public string OfertasGanadasClienteCadena { get; set; }

        public decimal? WinRateCliente { get; set; }

        [DataMember]
        public string WinRateClienteCadena { get; set; }

        public int? TiempoEntregaPromedioCliente { get; set; }

        [DataMember]
        public string TiempoEntregaPromedioClienteCadena { get; set; }

        public decimal? PorcentajeCumplimientoCliente { get; set; }

        [DataMember]
        public string PorcentajeCumplimientoClienteCadena { get; set; }

        [DataMember]
        public decimal? IspCliente { get; set; }

        [DataMember]
        public string IspClienteCadena { get; set; }

        [DataMember]
        public string IdOportunidad { get; set; }

        [DataMember]
        public string LineaNegocio { get; set; }

        [DataMember]
        public string NombreOportunidad { get; set; }

        [DataMember]
        public string TipologiaOportunidad { get; set; }

        [DataMember]
        public int? CantidadOfertas { get; set; }

        [DataMember]
        public string FaseOportunidad { get; set; }

        [DataMember]
        public string ProbabilidadExito { get; set; }

        [DataMember]
        public DateTime? FechaCierreEstimada { get; set; }

        [DataMember]
        public string FechaCierreEstimadaCadena { get; set; }

        [DataMember]
        public string NumeroDelCaso { get; set; }

        [DataMember]
        public string AnalistaFinanciero { get; set; }

        [DataMember]
        public int? DiasAtencion { get; set; }

        [DataMember]
        public decimal? Capex { get; set; }

        [DataMember]
        public decimal? CostosDirectos { get; set; }

        [DataMember]
        public decimal? IngresoTotal { get; set; }

        [DataMember]
        public decimal? OibdaAnual { get; set; }

        [DataMember]
        public string Payback { get; set; }

        [DataMember]
        public decimal? PagoUnicoDolares { get; set; }

        [DataMember]
        public string PagoUnicoDolaresCadena { get; set; }

        [DataMember]
        public decimal? RecurrenteDolares { get; set; }

        [DataMember]
        public string RecurrenteDolaresCadena { get; set; }

        [DataMember]
        public int? PeriodoMeses { get; set; }

        [DataMember]
        public string PeriodoMesesCadena { get; set; }

        [DataMember]
        public decimal? TipoCambio { get; set; }

        [DataMember]
        public string TipoCambioCadena { get; set; }

        [DataMember]
        public string OportunidadesCadena { get; set; }

        [DataMember]
        public int? TotalRow { get; set; }

        [DataMember]
        public int Nivel { get; set; }

        [DataMember]
        public int Desplegado { get; set; }

        [DataMember]
        public long Orden { get; set; }

        [DataMember]
        public long OrdenMostrar { get; set; }

        [DataMember]
        public string EquipoPreventaCadena { get; set; }

        [DataMember]
        public string CarteraClientesCadena { get; set; }

        [DataMember]
        public string TiempoEntregaOfertaCadena { get; set; }

        [DataMember]
        public string PorcentajeCumplimientoEntregaOfertaCadena { get; set; }

        [DataMember]
        public string PropietarioOportunidad { get; set; }

    }
}

