﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    public class DashoardOfertasLineaNegocioDtoResponse
    {
        public string LineaNegocio { get; set; }

        public int NroOportunidades { get; set; }
    }
}
