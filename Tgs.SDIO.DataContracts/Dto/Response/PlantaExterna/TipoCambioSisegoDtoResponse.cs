﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna
{
    [DataContract]
    public class TipoCambioSisegoDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string Estado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public decimal Monto { set; get; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaFin { set; get; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaInicio { set; get; }
    }

    [DataContract]
    public class TipoCambioSisegoPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<TipoCambioSisegoDtoResponse> ListaTipoCambioDtoResponse { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasNextPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsFirstPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLastPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FirstItemOnPage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LastItemOnPage { get; set; }
    }

}
