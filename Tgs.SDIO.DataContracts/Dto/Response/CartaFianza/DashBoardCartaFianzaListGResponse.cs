﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.CartaFianza
{
    [DataContract]
    public class DashBoardCartaFianzaListGResponse
    {

        [DataMember]
        public int Conteo { set; get; }
        [DataMember]
        public string Descripcion { set; get; }
        [DataMember]
        public int IdTipoAccionTm { set; get; }


    }
}
