﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.CartaFianza
{
    [DataContract]
    public class CartaFianzaDetalleRecuperoResponse
    {
        [DataMember]
        public int IdCartaFianzaDetalleRecupero { set; get; }
        [DataMember]
        public int IdCartaFianza { set; get; }
        [DataMember]
        public int IdColaborador { set; get; }
        [DataMember]
        public int? IdColaboradorACargo { set; get; }
        [DataMember]
        public int? IdEstadoRecuperacionCartaFianzaTm { set; get; }
        [DataMember]
        public string Comentario { set; get; }
        [DataMember]
        public DateTime? FechaRecupero { set; get; }
        [DataMember]
        public DateTime? FechaSolicitudRecupero { set; get; }
        [DataMember]
        public DateTime? FechaDevolucionTesoreria { set; get; }
        [DataMember]
        public string FechaRecuperoStr { set; get; }
        [DataMember]
        public string FechaSolicitudRecuperoStr { set; get; }
        [DataMember]
        public string FechaDevolucionTesoreriaStr { set; get; }

        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public int? IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime? FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }

        
        [DataMember]
        public int? IdCartaFianzaDetalleSeguimiento { set; get; }
        [DataMember]
        public string StatusRecupero { set; get; }
        [DataMember]
        public string ColadoradorACargo { set; get; }
        [DataMember]
        public string Cliente { set; get; }
        [DataMember]
        public string NumeroIdentificadorFiscal { set; get; }



        [DataMember]
        public string NumeroGarantia { set; get; }

        [DataMember]
        public decimal ImporteCartaFianza { set; get; }

        [DataMember]
        public string EntidadBancaria { set; get; }

        [DataMember]
        public string FechaFinServicioStr { set; get; }

        [DataMember]
        public DateTime? FechaFinServicio{ set; get; }
        [DataMember]
        public string ImporteStr { set; get; }

        [DataMember]
        public string TipoMoneda { set; get; }



        [DataMember]
        public string SustentoRenovacion { set; get; }
        [DataMember]
        public string Observacion { set; get; }
        [DataMember]
        public string ValidacionContratoGcSm { set; get; }
        [DataMember]
        public DateTime? FechaRespuestaFinalGcSm { set; get; }


    }

    [DataContract]
    public class CartaFianzaDetalleRecuperoResponsePaginado : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<CartaFianzaDetalleRecuperoResponse> ListCartaFianzaRecupero { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }


    }
}
