﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.CartaFianza
{

    [DataContract]
    public class CartaFianzaDetalleEstadoResponse
    {
        [DataMember]
        public int IdCartaFianzaDetalleEstado { set; get; }
        [DataMember]
        public int IdCartaFianza { set; get; }
        [DataMember]
        public int IdColaboradorACargo { set; get; }
        [DataMember]
        public int IdEstadoCartaFianzaTm { set; get; }
        [DataMember]
        public string Observacion { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public Nullable<int> IdUsuarioCreacion { set; get; }
        [DataMember]
        public Nullable<DateTime> FechaCreacion { set; get; }
        [DataMember]
        public Nullable<int> IdUsuarioEdicion { set; get; }
        [DataMember]
        public Nullable<DateTime> FechaEdicion { set; get; }
    }
}
