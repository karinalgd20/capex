﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.CartaFianza
{

    [DataContract]
   public class DashBoardCartaFianzaResponse
    {


        [DataMember]
        public int CF_CartaFianzaTotal               {set; get; }

         [DataMember]
        public  int  CF_SolicitudEmision                    {set; get; }
        [DataMember]
        public  int  CF_SolicitudEmision_Pendiente          {set; get; }
        [DataMember]
        public  int  CF_SolicitudEmision_EnviadoTso          {set; get; }
        [DataMember]
        public  int  CF_SolicitudEmision_EnviadoTso_Errada   {set; get; }

                [DataMember]
                public int CF_SolicitudEmision_FechaCorrecion { set; get; }
                [DataMember]
                public int CF_SolicitudEmision_FechaRecepcionCorrecion { set; get; }


        [DataMember]
        public  int  CF_Emision_Vigente                      {set; get; }
        [DataMember]
        public  int  CF_Emision_EnviadoGC_Seguimiento        {set; get; }
        [DataMember]
        public  int  CF_Emision_EnviadoGC_Reiterativo        {set; get; }
        [DataMember]
        public  int  CF_Emision_EnviadoGC_Escalado           {set; get; }




        [DataMember]
        public  int  CF_Renovacion                           {set; get; }
        [DataMember]
        public int CF_RenovacionEmision_Pendiente { set; get; }
        [DataMember]
        public int CF_RenovacionEmision_EnviadoTso           { set; get; }
        [DataMember]
        public int CF_RenovacionEmision_RecepcionTso { set; get; }
        
        [DataMember]
        public int CF_RenovacionEmision_RenovacionFinalizada { set; get; }

        [DataMember]
        public int CF_RenovacionEmision_GestionXTesoreria { set; get; }
        [DataMember]
        public int CF_RenovacionEmision_EntregadoClienteTso { set; get; }
        [DataMember]
        public int CF_RenovacionEmision_XEntregarClienteTso { set; get; }




        [DataMember]
        public int CF_Recupero                               {set; get; }


        [DataMember]
        public int CF_Recupero_Ejecutada { set; get; }
        [DataMember]
        public int CF_Recupero_Devuelta { set; get; }
        [DataMember]
        public int CF_Recupero_xSol_Recuperada { set; get; }

        [DataMember]
        public int CF_Recupero_conSolicitudDevolucion { set; get; }
        [DataMember]
        public int CF_Recupero_Recuperada { set; get; }
        [DataMember]
        public int CF_Recupero_ProcesoRecupero { set; get; }
        [DataMember]
        public int CF_Recupero_PorRecojer { set; get; }
        [DataMember]
        public int CF_Recupero_Descargadas { set; get; }


    }
}
