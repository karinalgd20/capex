﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.CartaFianza
{

    [DataContract]
    public class CartaFianzaListaResponse
    {

        [DataMember]
        public int IdCartaFianza { set; get; }
        [DataMember]
        public int IdCliente { set; get; }
        [DataMember]
        public string NumeroIdentificadorFiscal { set; get; }
        [DataMember]
        public string NombreCliente { set; get; }
        [DataMember]
        public string NumeroOportunidad { set; get; }
        [DataMember]
        public int IdTipoContratoTm { set; get; }
        [DataMember]
        public string DesTipoContrato { set; get; }
        [DataMember]
        public string NumeroContrato { set; get; }
        [DataMember]
        public int IdProcesoTm { set; get; }
        [DataMember]
        public string NumeroProceso { set; get; }
        [DataMember]
        public string DesProcesoTm { set; get; }
        [DataMember]
        public string Servicio { set; get; }
        [DataMember]
        public string DescripcionServicioCartaFianza { set; get; }


        [DataMember]
        public DateTime? FechaFirmaServicioContrato { set; get; }
        [DataMember]
        public DateTime? FechaFinServicioContrato { set; get; }


        [DataMember]
        public string FechaFirmaServicioContratostr { set; get; }
        [DataMember]
        public string FechaFinServicioContratostr { set; get; }

        [DataMember]
        public int IdEmpresaAdjudicadaTm { set; get; }
        [DataMember]

        public int IdTipoGarantiaTm { set; get; }
        [DataMember]
        public string NumeroGarantia { set; get; }

        [DataMember]
        public int IdTipoMonedaTm { set; get; }
        [DataMember]
        public string DesTipoMonedaTm { set; get; }

        [DataMember]
        public decimal ImporteCartaFianzaSoles { set; get; }
        [DataMember]
        public decimal ImporteCartaFianzaDolares { set; get; }

        [DataMember]
        public decimal ImporteCartaFianza { set; get; }

        [DataMember]
        public string ImporteFianzaStr { set; get; }
        
        [DataMember]
        public int IdBanco { set; get; }


        [DataMember]
        public DateTime? FechaEmisionBanco { set; get; }
        [DataMember]
        public DateTime? FechaVigenciaBanco { set; get; }
        [DataMember]
        public DateTime? FechaVencimientoBanco { set; get; }

        [DataMember]
        public string FechaEmisionBancostr { set; get; }
        [DataMember]
        public string FechaVigenciaBancostr { set; get; }
        [DataMember]
        public string FechaVencimientoBancostr { set; get; }

        [DataMember]
        public int? IdRenovarTm { set; get; }
        [DataMember]
        public string DesRenovarTm { set; get; }

        [DataMember]
        public int? IdEstadoCartaFianzaTm { set; get; }
        [DataMember]
        public string DesEstadoCartaFianzaTm { set; get; }
        [DataMember]
        public int? IdTipoSubEstadoCartaFianzaTm { set; get; }
        [DataMember]
        public string DesTipoSubEstadoCartaFianzaTm { set; get; }

        [DataMember]
        public DateTime? FechaEnvioTso { set; get; }
        

        /***/
        [DataMember]
        public int? IdTipoAccionTm { set; get; }


        [DataMember]
        public int ClienteEspecial { set; get; }
        [DataMember]
        public int SeguimientoImportante { set; get; }
        [DataMember]
        public string Observacion { set; get; }
        [DataMember]
        public string Incidencia { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public int? IdUsuarioCreacion { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public DateTime? FechaCreacion { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public DateTime? FechaEdicion { set; get; }

        [DataMember]
        public int  IdIndicadorSemaforo { set; get; }
        [DataMember]
        public string IdIndicadorSemaforoColor { set; get; }

        [DataMember]
        public int IdIndicadorSemaforoTpsTesoreria { set; get; }
        [DataMember]
        public string IdIndicadorSemaforoColorTpsTesoreria { set; get; }


        [DataMember]
        public DateTime? FechaCorrecionTesoreria { set; get; }
        [DataMember]
        public DateTime? FechaRecepcionTesoreria { set; get; }


        [DataMember]
        public string FechaCorrecionTesoreriaStr { set; get; }
        [DataMember]
        public string FechaRecepcionTesoreriaStr { set; get; }
        [DataMember]
        public string MotivoCartaFianzaErrada { set; get; }
        [DataMember]
        public int? IdCartaFianzaInicial { set; get; }
        [DataMember]
        public string  EsRenovacion { set; get; }
        [DataMember]
        public int? IdTipoRequeridaTm { set; get; }

         [DataMember]
        public string desTipoRequeridaTm { set; get; }
        
        [DataMember]
        public int id { set; get; }
        [DataMember]
        public string text { set; get; }
        [DataMember]
        public int? IdColaborador { set; get; }
        [DataMember]
        public string NombreCompleto { set; get; }

        /****requerida******/
        [DataMember]
        public DateTime? FechaRenovacionEjecucion { set; get; }
        [DataMember]
        public string FechaRenovacionEjecucionStr { set; get; }
        [DataMember]
        public DateTime? FechaDesestimarRequerimiento { set; get; }
        [DataMember]
        public string FechaDesestimarRequerimientoStr { set; get; }
        [DataMember]
        public DateTime? FechaEjecucion { set; get; }
        [DataMember]
        public string FechaEjecucionStr { set; get; }
        [DataMember]      
        public string MotivoCartaFianzaRequerida { set; get; }

        [DataMember]
        public Boolean Eliminado { set; get; }

        [DataMember]
        public int Indicador { set; get; }
    }

    [DataContract]
    public class CartaFianzaListaResponsePaginado : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<CartaFianzaListaResponse> ListCartaFianza { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }

      
    }
}
