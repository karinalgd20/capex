﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.CartaFianza
{
    [DataContract]
   public class CartaFianzaDetalleAccionResponse
    {

        [DataMember]
        public int IdCartaFianzaDetalleAccion { set; get; }
        [DataMember]
        public int IdCartaFianza { set; get; }
        [DataMember]
        public int IdColaboradorACargo { set; get; }
        [DataMember]
        public int IdTipoAccionTm { set; get; }
        [DataMember]
        public int IdEstadoVencimientoTm { set; get; }
        [DataMember]
        public string Observacion { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public DateTime FechaRegistro { set; get; }



        
        [DataMember]
        public string DesTipoAccion { set; get; }
        [DataMember]
        public string FechaCreacionStr { set; get; }
        [DataMember]
        public string FechaRegistroStr { set; get; }


        [DataMember(EmitDefaultValue = true)]
        public Nullable<int> IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public Nullable<int> IdUsuarioEdicion { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public Nullable<DateTime> FechaEdicion { set; get; }

        [DataMember]
        public string UsuarioCreacion { set; get; }

    }
}
