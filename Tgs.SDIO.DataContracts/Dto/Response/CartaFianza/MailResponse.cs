﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.DataContracts.Dto.Response.CartaFianza
{
    [DataContract]
    public class MailResponse : MailDto
    {
        [DataMember]
        public int IdCartaFianza { get; set; }
        [DataMember]
        public int IdColaborador { get; set; }
        [DataMember]
        public string NombreColaborador { get; set; }
        [DataMember]
        public string EmailColaborador { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public string UsuarioEdicion { get; set; }
    }
}
