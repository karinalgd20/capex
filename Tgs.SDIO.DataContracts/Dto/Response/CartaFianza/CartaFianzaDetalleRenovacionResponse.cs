﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.CartaFianza
{
    [DataContract]
    public class CartaFianzaDetalleRenovacionResponse
    {

        [DataMember]
        public int IdCartaFianzaDetalleRenovacion { set; get; }
        [DataMember]
        public int IdCartaFianza { set; get; }
        [DataMember]
        public int IdColaborador { set; get; }

        [DataMember]
        public string NombreCompletoColaborador { set; get; }
        [DataMember]
        public string EntidadBancaria { set; get; }
        

        [DataMember]
        public int IdColaboradorACargo { set; get; }
        [DataMember]
        public int? IdRenovarTm { set; get; }

        [DataMember]
        public string DesTipoRenovacion { set; get; }

        
        [DataMember]
        public int PeriodoMesRenovacion { set; get; }
        [DataMember]
        public DateTime? FechaVencimientoRenovacion { set; get; }
        [DataMember]
        public string FechaVencimientoRenovacionStr { set; get; }
        [DataMember]
        public string SustentoRenovacion { set; get; }
        [DataMember]
        public string Observacion { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public int IdCliente { set; get; }
        [DataMember]
        public string Cliente { set; get; }
        [DataMember]
        public string NumeroIdentificadorFiscal { set; get; }
        [DataMember]

      
        public string TipoMoneda { set; get; }
        [DataMember]
        public decimal Importe { set; get; }
        [DataMember]
        public string ImporteStr { set; get; }
        [DataMember]
        public string EstadoRenovacion { set; get; }
        [DataMember]
        public string DescripcionCartaFianza { set; get; }

        [DataMember]
        public Nullable<int> IdUsuarioCreacion { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public Nullable<DateTime> FechaCreacion { set; get; }
        [DataMember]
        public Nullable<int> IdUsuarioEdicion { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public Nullable<DateTime> FechaEdicion { set; get; }

        [DataMember]
        public DateTime? FechaVencimientoBanco { set; get; }

        [DataMember]
        public string FechaVencimientoBancoStr { set; get; }


        [DataMember]
        public int IdIndicadorSemaforo { set; get; }
        [DataMember]
        public string IdIndicadorSemaforoColor { set; get; }

        [DataMember]
        public int IdIndicadorSemaforoTpsTesoreria { set; get; }
        [DataMember]
        public string IdIndicadorSemaforoColorTpsTesoreria { set; get; }


        [DataMember]
        public string CartaFianzaReemplazo { set; get; }
        [DataMember]
        public DateTime? FechaSolitudTesoria { set; get; }
        [DataMember]
        public string NumeroGarantia { set; get; }
        [DataMember]
        public string NumeroGarantiaReemplazo { set; get; }
        [DataMember]
        public string FechaSolitudTesoriaStr { set; get; }

        [DataMember]
        public int? IdTipoBancoReemplazo { set; get; }

        [DataMember]
        public string DesTipoAccionTm { set; get; }
        [DataMember]
        public int? IdTipoAccionTm { set; get; }
        
        [DataMember]
        public string ValidacionContratoGcSm { set; get; }
        [DataMember]
        public DateTime? FechaRespuestaFinalGcSm { set; get; }
        [DataMember]
        public DateTime? FechaRecepcionCartaFianzaRenovacion { set; get; }
        [DataMember]
        public DateTime? FechaEntregaRenovacion { set; get; }

        [DataMember]
        public string FechaRespuestaFinalGcSmStr { set; get; }
        [DataMember]
        public string FechaRecepcionCartaFianzaRenovacionStr { set; get; }

        [DataMember]
        public string FechaEntregaRenovacionStr { set; get; }

        [DataMember]
        public int? IdCartaFianzaInicial { set; get; }

        [DataMember]
        public int? IdEstadoCartaFianzaTm { set; get; }
        [DataMember]
        public string DesEstadoCartaFianzaTm { set; get; }



        [DataMember]
        public DateTime? FechaEmisionBancoRnv { get; set; }
        [DataMember]
        public DateTime? FechaVigenciaBancoRnv { get; set; }
        [DataMember]
        public DateTime? FechaVencimientoBancoRnv { get; set; }

        [DataMember]
        public string FechaEmisionBancoRnvStr { get; set; }
        [DataMember]
        public string FechaVigenciaBancoRnvStr { get; set; }
        [DataMember]
        public string FechaVencimientoBancoRnvStr { get; set; }

        /****requerida******/
        [DataMember]
        public DateTime? FechaRenovacionEjecucion { set; get; }
        [DataMember]
        public string FechaRenovacionEjecucionStr { set; get; }
        [DataMember]
        public DateTime? FechaDesestimarRequerimiento { set; get; }
        [DataMember]
        public string FechaDesestimarRequerimientoStr { set; get; }
        [DataMember]
        public DateTime? FechaEjecucion { set; get; }
        [DataMember]
        public string FechaEjecucionStr { set; get; }


        [DataMember]
        public int? IdTipoRequeridaTm { set; get; }

        [DataMember]
        public string desTipoRequeridaTm { set; get; }

        [DataMember]
        public string MotivoCartaFianzaRequerida { set; get; }
        /****requerida******/
    }

    [DataContract]
    public class CartaFianzaDetalleRenovacionResponsePaginado : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<CartaFianzaDetalleRenovacionResponse> ListCartaFianzaRenovacion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }
    }
}
