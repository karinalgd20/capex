﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.CartaFianza
{
    [DataContract]
    public class CartaFianzaDetalleSeguimientoResponse
    {
        [DataMember]
        public int? IdCartaFianzaDetalleSeguimiento { set; get; }
        [DataMember]
        public int? IdCartaFianza { set; get; }
        [DataMember]
        public int? IdColaborador { set; get; }
        [DataMember]
        public int? IdColaboradorACargo { set; get; }
        [DataMember]
        public int? IdEstadoRecuperacionCartaFianzaTm { set; get; }
        [DataMember]
        public string Comentario { set; get; }
        [DataMember]
        public int? IdUsuarioCreacion { set; get; }
        [DataMember]
        public int? FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioModificacion { set; get; }
        [DataMember]
        public int? FechaModificacion { set; get; }
        [DataMember]
        public int? Vigencia { set; get; }
        [DataMember]
        public int? Eliminado { set; get; }

        [DataMember]
        public string StatusRecupero { set; get; }
        [DataMember]
        public string ColadoradorACargo { set; get; }

        //Paginado

        [DataMember]
        public int Indice { set; get; }
        [DataMember]
        public int Tamanio { set; get; }

    }


    [DataContract]
    public class CartaFianzaDetalleSeguimientoResponsePaginado : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<CartaFianzaDetalleSeguimientoResponse> ListCartaFianzaSeguimiento { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }


    }
}
