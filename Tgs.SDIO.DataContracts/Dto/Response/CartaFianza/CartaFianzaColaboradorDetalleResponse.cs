﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.CartaFianza
{
    [DataContract]
    public class CartaFianzaColaboradorDetalleResponse
    {
       [DataMember]
        public int IdCartaFianzaColaboradorDetalle { set; get; }
        [DataMember]
        public int IdCartaFianza { set; get; }
        [DataMember]
        public int IdColaborador { set; get; }
        [DataMember]
        public int? IdEstado { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }


        [DataMember]
        public string NombreCompleto { set; get; }
        [DataMember]
        public int IdTipoColaboradorFianzaTm { set; get; }

        [DataMember]
        public int id { set; get; }
        [DataMember]
        public string text { set; get; }
        [DataMember]
        public int? IdSupervisorColaborador { set; get; }
    }


    [DataContract]
    public class CartaFianzaColaboradorDetalleListaResponse
    {
        [DataMember]
        public int IdCartaFianzaColaboradorDetalle { set; get; }
        [DataMember]
        public int IdCartaFianza { set; get; }
        [DataMember]
        public int IdColaborador { set; get; }
        [DataMember]
        public int Colaborador { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public Nullable<int> IdUsuarioCreacion { set; get; }
        [DataMember]
        public Nullable<DateTime> FechaCreacion { set; get; }
        [DataMember]
        public Nullable<int> IdUsuarioEdicion { set; get; }
        [DataMember]
        public Nullable<DateTime> FechaEdicion { set; get; }
    }



}
