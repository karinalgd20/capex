﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.DataContracts.Dto.Response.Seguridad
{
    [DataContract]
    public class UsuarioDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string LoginWindows { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool FlagSolicitarCambioClave { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool FlagUsuarioBloqueado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioSuperior { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaUltimoAcceso { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCambioPassword { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioModificacion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaModoficacion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Apellidos { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Login { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Usuariosistema { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdEmpresa { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Nombres { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CorreoElectronico { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoCip { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCaducidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdEstadoRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IntentosFallidos { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Password { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TipoUsuario { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int CodigoError { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioSistema { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombresCompletos { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<PerfilDtoResponse> PerfilDtoResponseLista { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<UsuarioDtoResponse> UsuarioDtoResponseLista { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string EstadoRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalRegistros { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdArea { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdCargo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioSistemaEmpresa { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombrePreventa { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string JefeProyecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LiderJefeProyecto { get; set; }
    }
}
