﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Seguridad
{
    [DataContract]
    public class OpcionMenuDto
    {
        [DataMember(EmitDefaultValue = false)]
        public string IdOpcion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoOpcion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombreOpcion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DescripcionOpcion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string UrlPagina { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TipoOpcion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FlagOpcion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NumeroOrden { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string IdOpcionPadre { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NumeroNivel { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string OpcionCodigo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoPerfil { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string UrlImage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AltlImage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<OpcionSubMenuDto> OpcionSubMenuDto { get; set; }
    }
}
