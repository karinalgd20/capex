﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Seguridad
{
    [DataContract]
    public class PerfilDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string CodigoPerfil { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombrePerfil { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdPerfil { get; set; }
    }
}
