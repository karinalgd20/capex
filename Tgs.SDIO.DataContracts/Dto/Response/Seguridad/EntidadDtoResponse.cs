﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Seguridad
{
    [DataContract]
    public class EntidadDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdEntidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoEntidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombreEntidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DescripcionEntidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TipoEntidad { get; set; }
    }
}
