﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Seguridad
{
    [DataContract]
    public class OpcionMenuResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public OpcionMenuDto OpcionMenuDto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<OpcionMenuDto> OpcionMenuDtoLista { get; set; }
    }
}
