﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Seguridad
{
    [DataContract]
    public class UsuarioPerfilDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdPerfil { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Perfil { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Estado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioSistemaEmpresa { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalRegistros { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdEstadoRegistro { get; set; }
    }
}
