﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Capex
{
    [DataContract]
    public class TopologiaDtoRequest
    {
        [DataMember]
        public int IdTopologia { get; set; }

        [DataMember]
        public int IdEstructuraCosto { get; set; }

        [DataMember]
        public int IdMedioCosto { get; set; }

        [DataMember]
        public string Observaciones { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }

        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public int IdUsuarioEdicion { get; set; }

        [DataMember]
        public DateTime FechaEdicion { get; set; }
    }
}
