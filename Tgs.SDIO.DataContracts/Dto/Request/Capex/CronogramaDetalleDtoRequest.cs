﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Capex
{
    [DataContract]
    public class CronogramaDetalleDtoRequest
    {
        [DataMember]
        public int IdCronogramaDetalle { set; get; }
        [DataMember]
        public int? IdCronograma { set; get; }
        [DataMember]
        public string Descripcion { set; get; }
        [DataMember]
        public int? Duracion { set; get; }
        [DataMember]
        public DateTime? Inicio { set; get; }
        [DataMember]
        public DateTime? Final { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }



    }
}
