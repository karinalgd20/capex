﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Capex
{
    [DataContract]
    public class DiagramaDetalleDtoRequest
    {
        [DataMember]
        public int IdDiagramaDetalle { get; set; }
        [DataMember]
        public int? IdDiagrama { get; set; }
        [DataMember]
        public int? IdConcepto { get; set; }
        [DataMember]
        public int? Orden { get; set; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }



    }
}
