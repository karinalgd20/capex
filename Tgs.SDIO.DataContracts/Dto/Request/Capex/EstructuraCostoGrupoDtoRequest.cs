﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Capex
{
    [DataContract]
    public class EstructuraCostoGrupoDtoRequest
    {
        [DataMember]
        public int IdEstructuraCostoGrupo { get; set; }

        [DataMember]
        public int IdCapexLineaNegocio { get; set; }

        [DataMember]
        public int IdGrupo { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }

        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public int IdUsuarioEdicion { get; set; }

        [DataMember]
        public DateTime FechaEdicion { get; set; }

        [DataMember]
        public int? IdServicio { set; get; }
        [DataMember]
        public int? IdConcepto { set; get; }
        [DataMember]
        public string Modelo { set; get; }
        [DataMember]
        public decimal CapexSoles { set; get; }
        [DataMember]
        public decimal CapexDolares { set; get; }
        [DataMember]
        public decimal CapexTotalSoles { set; get; }

        [DataMember]
        public int Cantidad { set; get; }
        [DataMember]
        public string Certificacion { set; get; }
        [DataMember]
        public string IdUbigeo { set; get; }
        [DataMember]

        public int? IdMedioCosto { get; set; }
        [DataMember]
        public string IdTipoGrupo { get; set; }
        [DataMember]
        public decimal? CuNuevoDolar { get; set; }
        [DataMember]
        public string Asignacion { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }
    }
}
