﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Capex
{
    [DataContract]
    public class EstructuraCostoDtoRequest
    {
        [DataMember]
        public int IdEstructuraCosto { get; set; }
        [DataMember]
        public int? IdSolicitudCapex { get; set; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }
       


    }
}
