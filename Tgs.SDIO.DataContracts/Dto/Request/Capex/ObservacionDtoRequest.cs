﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Capex
{
    [DataContract]
    public class ObservacionDtoRequest
    {
        [DataMember]
        public int IdObservacion { get; set; }

        [DataMember]
        public int IdResponsable { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public int IdAsignado { get; set; }

        [DataMember]
        public string Respuesta { get; set; }

        [DataMember]
        public int IdSeccion { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }

        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public int IdUsuarioEdicion { get; set; }

        [DataMember]
        public DateTime FechaEdicion { get; set; }
    }
}
