﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Capex
{
    [DataContract]
    public class AsignacionCapexTotalDetalleDtoRequest
    {
        [DataMember]
        public int IdAsignacionCapexTotalDetalle { set; get; }
        [DataMember]
        public int IdAsignacionCapexTotal { set; get; }
        [DataMember]
        public decimal? Monto { set; get; }
        [DataMember]
        public int? IdMes { set; get; }
        [DataMember]
        public int? IdTipo { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }



    }
}
