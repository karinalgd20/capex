﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Capex
{
    [DataContract]
    public class CapexLineaNegocioDtoRequest
    {
        [DataMember]
        public int IdCapexLineaNegocio { set; get; }
        [DataMember]
        public int? IdSolicitudCapex { get; set; }
        [DataMember]
        public int? IdLineaNegocio { get; set; }

        [DataMember]
        public decimal? Van { get; set; }

        [DataMember]
        public decimal? Fc { get; set; }

        [DataMember]
        public int? PayBack { get; set; }

        [DataMember]
        public int? IdEstado { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }

        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }

    }
}
