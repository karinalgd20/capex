﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Negocios
{
    [DataContract]
    public class SisegoCotizadoDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int Id_OportunidadGanadora { get; set; }

        [DataMember]
        public string codigo_sisego { get; set; }

        [DataMember]
        public string PER { get; set; }

    }
}
