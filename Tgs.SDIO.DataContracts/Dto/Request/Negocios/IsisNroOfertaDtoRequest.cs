﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.DataContracts.Dto.Request.Negocios
{
    [DataContract]
    public class IsisNroOfertaDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Nro_oferta { get; set; }

        [DataMember]
        public int Nro_version_Oferta { get; set; }

        [DataMember]
        public int Id_OportunidadGanadora { get; set; }

        [DataMember]
        public string Per { get; set; }

        [DataMember]
        public List<ComboDtoResponse> ListIsisOfertaSelect { get; set; }
    }
}
