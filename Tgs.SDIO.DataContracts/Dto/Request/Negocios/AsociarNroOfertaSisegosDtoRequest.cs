﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.DataContracts.Dto.Request.Negocios
{
    [DataContract]
    public class AsociarNroOfertaSisegosDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdOferta { get; set; }
        [DataMember]
        public string NroOferta { get; set; }
        [DataMember]
        public int IdSisego { get; set; }
        [DataMember]
        public string CodSisego { get; set; }
        [DataMember]
        public string ListSisego { get; set; }
        [DataMember]
        public int IdOportunidad { get; set; }
    }
}
