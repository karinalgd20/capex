﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Negocios
{
    [DataContract]
    public class RPASisegoDetalleDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int Id_sisego { get; set; }

        [DataMember]
        public int Id_Oferta { get; set; }

        [DataMember]
        public string CodSisego { get; set; }

        [DataMember]
        public string Latitud { get; set; }

        [DataMember]
        public string Longitud { get; set; }

        [DataMember]
        public string Departamento { get; set; }

        [DataMember]
        public string Provincia { get; set; }

        [DataMember]
        public string Distrito { get; set; }

        [DataMember]
        public string Ciudad { get; set; }

        [DataMember]
        public string TipoSede { get; set; }

        [DataMember]
        public string Direccion { get; set; }

        [DataMember]
        public string Via { get; set; }

        [DataMember]
        public string Numero { get; set; }

        [DataMember]
        public string Nro_Piso { get; set; }

        [DataMember]
        public string Interior { get; set; }

        [DataMember]
        public string Lote { get; set; }

        [DataMember]
        public string Manzana { get; set; }
    }
}
