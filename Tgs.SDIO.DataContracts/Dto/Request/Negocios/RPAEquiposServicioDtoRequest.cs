﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Negocios
{
    [DataContract]
    public class RPAEquiposServicioDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int Id_Oferta { get; set; }

        [DataMember]
        public int Id_RPAServOferta { get; set; }
        [DataMember]
        public int IdServicioSisego{ get; set; }
        [DataMember]
        public string Cod_Equipo { get; set; }

        [DataMember]
        public string Tipo { get; set; }

        [DataMember]
        public string Marca { get; set; }

        [DataMember]
        public string Modelo { get; set; }

        [DataMember]
        public string Componente_1 { get; set; }

        [DataMember]
        public string Tipo_de_acceso { get; set; }

        [DataMember]
        public string Meses { get; set; }

        [DataMember]
        public string Con_Valor_Agregado { get; set; }
    }
}
