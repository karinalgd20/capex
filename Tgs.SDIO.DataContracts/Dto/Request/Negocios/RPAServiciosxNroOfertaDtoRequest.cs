﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Negocios
{
    [DataContract]
    public class RPAServiciosxNroOfertaDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string NombreServicio { get; set; }

        [DataMember]
        public int Cantidad { get; set; }

        [DataMember]
        public string Completa { get; set; }

        [DataMember]
        public int Id_Oferta { get; set; }
        [DataMember]
        public int NumeroCDK { get; set; }

        [DataMember]
        public int NumeroCD { get; set; }

        [DataMember]

        public int Contacto_Id { get; set; }

        [DataMember]
        public int TipoServicio { get; set; }

        [DataMember]
        public string Velocidad { get; set; }

        [DataMember]
        public string Accion { get; set; }

        [DataMember]
        public string ModeloTx { get; set; }

        [DataMember]
        public string TipoCircuito { get; set; }

        [DataMember]
        public string LDN { get; set; }

        [DataMember]
        public string Realtime { get; set; }

        [DataMember]
        public string Video { get; set; }

        [DataMember]
        public string Caudal_oro { get; set; }

        [DataMember]
        public string Caudal_plata { get; set; }

        [DataMember]
        public string Caudal_platino { get; set; }

        [DataMember]
        public string Caudal_Bronce { get; set; }

        [DataMember]
        public string Caudal_VRF { get; set; }

        [DataMember]
        public string VA_Servicio { get; set; }

        [DataMember]
        public string VA_Velocidad { get; set; }

        [DataMember]
        public string VA_Acción { get; set; }

        [DataMember]
        public string VA_MedioTX { get; set; }

        [DataMember]
        public string VA_TipoCircuito { get; set; }
    }
}
