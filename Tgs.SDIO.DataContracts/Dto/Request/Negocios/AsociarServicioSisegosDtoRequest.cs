﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Negocios
{
    [DataContract]
    public class AsociarServicioSisegosDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdServicio { get; set; }
        [DataMember]
        public string Servicio { get; set; }
        [DataMember]
        public int IdEquipo { get; set; }
        [DataMember]
        public string Equipo { get; set; }
        [DataMember]
        public int IdSisego { get; set; }
        [DataMember]
        public string CodSisego { get; set; }
        [DataMember]
        public int IdOferta { get; set; }
        [DataMember]
        public string NroOferta { get; set; }
        [DataMember]
        public string Departamento { get; set; }
        [DataMember]
        public string Provincia { get; set; }
        [DataMember]
        public string Distrito { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        
    }
}
