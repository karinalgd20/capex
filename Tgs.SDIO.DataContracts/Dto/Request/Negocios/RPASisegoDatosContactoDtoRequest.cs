﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Negocios
{
    [DataContract]
    public class RPASisegoDatosContactoDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int Id_RPASisegoDetalle { get; set; }

        [DataMember]
        public string Contacto { get; set; }

        [DataMember]
        public string Telefono_1 { get; set; }

        [DataMember]
        public string Telefono_2 { get; set; }

        [DataMember]
        public string Telefono_3 { get; set; }

        [DataMember]
        public string Telefono1_Contacto2 { get; set; }

        [DataMember]
        public string Telefono2_Contacto2 { get; set; }

        [DataMember]
        public string Telefono3_Contacto2 { get; set; }

        [DataMember]
        public string Contacto_2 { get; set; }

        [DataMember]
        public int IdServicio { get; set; }
    }
}
