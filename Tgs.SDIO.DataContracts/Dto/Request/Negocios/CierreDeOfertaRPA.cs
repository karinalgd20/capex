﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Negocios
{
    [DataContract]
    public class CierreDeOfertaRPA : AuditoriaDto
    {
        [DataMember]
        public string Etapa { get; set; }
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public int Numero_oferta { get; set; }
        [DataMember]
        public int Version_oferta { get; set; }
        [DataMember]
        public string Razon_social { get; set; }
        [DataMember]
        public string Dirección_legal { get; set; }
        [DataMember]
        public string Tipo_oportunidad { get; set; }
        [DataMember]
        public string Plazo_contrato { get; set; }
        [DataMember]
        public string Tipo_plazo_contrato { get; set; }
        [DataMember]
        public string Creador_oferta { get; set; }
        [DataMember]
        public string Numero_ruc { get; set; }
        [DataMember]
        public string Comercial_oferta { get; set; }
        [DataMember]
        public DateTime Fecha_creacion_oferta { get; set; }
        [DataMember]
        public int Cantidad_servicio { get; set; }
        [DataMember]
        public string Servicio { get; set; }
        [DataMember]
        public string Velocidad_servicio { get; set; }
        [DataMember]
        public string Numero_oportunidad { get; set; }
        [DataMember]
        public int Codigo_equipo { get; set; }
        [DataMember]
        public string Departamento_instalación { get; set; }
        [DataMember]
        public string Provincia_instalación { get; set; }
        [DataMember]
        public string Distrito_instalación { get; set; }
        [DataMember]
        public string Tipo_Via_instalación { get; set; }
        [DataMember]
        public string Dirección_instalación { get; set; }
        [DataMember]
        public Double Latitud_instalación { get; set; }
        [DataMember]
        public Double Longitud_instalación { get; set; }
        [DataMember]
        public string Numero_instalación { get; set; }
        [DataMember]
        public string Piso_instalación { get; set; }
        [DataMember]
        public string Interior_instalación { get; set; }
        [DataMember]
        public string Manzana_instalación { get; set; }
        [DataMember]
        public string Lote_instalación { get; set; }
        [DataMember]
        public string DireccciónDeCobranza { get; set; }
        [DataMember]
        public string PER { get; set; }
    }
}
