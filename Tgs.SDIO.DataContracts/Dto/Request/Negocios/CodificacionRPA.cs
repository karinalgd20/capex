﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Negocios
{
    [DataContract]
    public class CodificacionRPA : AuditoriaDto
    {
        [DataMember]
        public string NumeroIdentificadorFiscal { get; set; }
        [DataMember]
        public string CodigoCliente { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string DireccionLegal { get; set; }
        [DataMember]
        public string Nro_oferta { get; set; }
        [DataMember]
        public int Nro_version_Oferta { get; set; }
        [DataMember]
        public DateTime FechaCreacionOportunidad { get; set; }
        [DataMember]
        public DateTime FechaCreacionOferta { get; set; }
        [DataMember]
        public string PER { get; set; }
        [DataMember]
        public string TipoOportunidad { get; set; }
        [DataMember]
        public string SegmentoCliente { get; set; }
        [DataMember]
        public string EstadoOportunidad { get; set; }
        [DataMember]
        public string EtapaOportunidad { get; set; }
        [DataMember]
        public string PlazoContrato { get; set; }
        [DataMember]
        public string TipoPlazoContrato { get; set; }
        [DataMember]
        public string CreadorOferta { get; set; }
        [DataMember]
        public string ComercialOferta { get; set; }
        [DataMember]
        public string ValorAgregado { get; set; }
        [DataMember]
        public string Equipos1 { get; set; }
        [DataMember]
        public string Equipos2 { get; set; }
        [DataMember]
        public int Cantidad { get; set; }
        [DataMember]
        public string PagoUnico { get; set; }
        [DataMember]
        public string PagoRecurrente { get; set; }
        [DataMember]
        public string NombreServicio { get; set; }
        [DataMember]
        public string Velocidad { get; set; }
        [DataMember]
        public string TipoCircuito { get; set; }
        [DataMember]
        public string Accion { get; set; }
        [DataMember]
        public string Telefono_1 { get; set; }
        [DataMember]
        public string Telefono_2 { get; set; }
        [DataMember]
        public string Telefono_3 { get; set; }
        [DataMember]
        public string Telefono1_Contacto2 { get; set; }
        [DataMember]
        public string Telefono2_Contacto2 { get; set; }
        [DataMember]
        public string Telefono3_Contacto2 { get; set; }
        [DataMember]
        public string Contacto { get; set; }
        [DataMember]
        public string Contacto_2 { get; set; }
        [DataMember]
        public string Latitud { get; set; }
        [DataMember]
        public string Longitud { get; set; }
        [DataMember]
        public string MedioTransmision { get; set; }
        [DataMember]
        public string Via { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public string Numero { get; set; }
        [DataMember]
        public string Nro_Piso { get; set; }
        [DataMember]
        public string Interior { get; set; }
        [DataMember]
        public string Departamento { get; set; }
        [DataMember]
        public string Provincia { get; set; }
        [DataMember]
        public string Distrito { get; set; }
        [DataMember]
        public string Manzana { get; set; }
        [DataMember]
        public string Lote { get; set; }
        [DataMember]
        public string CreadorSisego { get; set; }
        [DataMember]
        public string Ciudad { get; set; }
        [DataMember]
        public string codigo_sisego { get; set; }
    }

   
}
