﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Negocios
{
    [DataContract]
    public class EmisionCircuitoRPA : AuditoriaDto
    {
            [DataMember]
            public string Etapa { get; set; }
            [DataMember]
            public string Estado { get; set; }
            [DataMember]
            public string Num_Circuito { get; set; }
            [DataMember]
            public string Estacion { get; set; }
            [DataMember]
            public string Accion { get; set; }
            [DataMember]
            public int Codigo_cliente { get; set; }
            [DataMember]
            public string Num_rq_servicio { get; set; }
            [DataMember]
            public string Servicio { get; set; }
            [DataMember]
            public string Equipos1 { get; set; }
            [DataMember]
            public string Segmento_cliente { get; set; }
            [DataMember]
            public string Dirección_instalación { get; set; }
            [DataMember]
            public string Medio_transmisión { get; set; }
            [DataMember]
            public string Tipo_circuito { get; set; }
            [DataMember]
            public string Modelo_equipo { get; set; }
            [DataMember]
            public string NombreResponsableRED { get; set; }
            [DataMember]
            public string ApellidoResponsableRED { get; set; }
            [DataMember]
            public string Observaciones { get; set; }

    }
    
}
