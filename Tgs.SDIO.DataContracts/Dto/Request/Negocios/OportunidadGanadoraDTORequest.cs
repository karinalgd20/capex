﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Negocios
{
    [DataContract]
    public class OportunidadGanadoraDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string PER { get; set; }

        [DataMember]
        public string Ruc { get; set; }

        [DataMember]
        public int IdEtapa { get; set; }
        [DataMember]
        public string strFechaCreacion { get; set; }
        [DataMember]
        public string Cliente { get; set; }
        [DataMember]
        public string CodigoCliente { get; set; }
        [DataMember]
        public int Manipulado { get; set; }
        [DataMember]
        public string Progreso { get; set; }
    }
}
