﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Seguridad
{
    [DataContract]
    public class UsuarioSistemaEmpresaDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdEmpresa { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdSistema { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaFin { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdTurno { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioRegistra { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int EstadoRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaInicio { get; set; }

    }
}
