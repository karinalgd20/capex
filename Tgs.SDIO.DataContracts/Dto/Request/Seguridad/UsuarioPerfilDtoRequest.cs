﻿using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Seguridad
{
    [DataContract]
    public class UsuarioPerfilDtoRequest :AuditoriaDto
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdPerfil { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioSistemaEmpresa { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioAsignado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NroPagina { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int RegistrosPagina { get; set; }
    }
}
