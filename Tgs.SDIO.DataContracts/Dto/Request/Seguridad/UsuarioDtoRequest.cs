﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Seguridad
{
    [DataContract]
    public class UsuarioDtoRequest: AuditoriaDto
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioSistema { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdEmpresa { get; set; } 

        [DataMember(EmitDefaultValue = false)]
        public string Apellidos { get; set; }
         
        [DataMember(EmitDefaultValue = false)]
        public string Login { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Password { get; set; } 

        [DataMember(EmitDefaultValue = false)]
        public int? IntentosFallidos { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NroIntentos { get; set; }
         
        [DataMember(EmitDefaultValue = false)]
        public string Nombres { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CorreoElectronico { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TipoUsuario { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoCip { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCaducidad { get; set; }
          
        [DataMember(EmitDefaultValue = false)]
        public string UsuarioEnvia { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoSistema { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NuevoPassword { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public PerfilDtoRequest PerfilDtoRequest { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public UsuarioSistemaEmpresaDtoRequest UsuarioSistemaEmpresaDtoRequest { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoPerfil { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NroPagina { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int RegistrosPagina { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool SolicitaCambioClave { get; set; } 

        [DataMember(EmitDefaultValue = false)]
        public int IdArea { get; set; } 

        [DataMember(EmitDefaultValue = false)]
        public int? IdCargo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TipoRecurso { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombreInterno { get; set; }
    }
}
