﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Seguridad
{
    [DataContract]
    public class PerfilDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdPerfil { get; set; } 
    
        [DataMember(EmitDefaultValue = false)]
        public int IdEstadoRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoPerfil { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombrePerfil { get; set; }
         
    }
}
