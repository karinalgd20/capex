﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad
{
   public  class OportunidadSTSeguimientoPreventaDtoRequest
    {        
        [DataMember(EmitDefaultValue = false)]
        public int PorcentajeCierre { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Codigo { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int UserId { get; set; }       
        [DataMember(EmitDefaultValue = false)]
        public int IdTipoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEstadoCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdSegmentoNegocio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdLineaNegocio { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public int IdEtapa { get; set; }                
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdCasoSF { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool Lider { get; set; }        
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }
    }
}
