﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad
{
    public class OportunidadSTSeguimientoPostventaDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public string CodSisego { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int PorcentajeCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdComplejidad { get; set; }        
        [DataMember(EmitDefaultValue = false)]
        public int IdTipoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEstadoCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdSegmentoNegocio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEtapa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }
    }
}
