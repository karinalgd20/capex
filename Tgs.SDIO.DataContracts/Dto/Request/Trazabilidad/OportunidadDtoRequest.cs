using System;
using System.Runtime.Serialization;


namespace Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad
{
    [DataContract]
    public  class OportunidadDtoRequest
    {

        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdOportunidadPadre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadAux { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdMotivoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoEntidadCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdSegmentoNegocio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Prioridad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? ProbalidadExito { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? PorcentajeCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? PorcentajeCheckList { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdFase { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEtapa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? FlgCapexMayor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ImporteCapex { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaApertura { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
    }
}
