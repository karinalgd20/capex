﻿using System.Runtime.Serialization;
using System;

namespace Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad
{
    [DataContract]
    public  class TiempoAtencionEquipoDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdTiempoAtencion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdSisego { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CodSisego { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public string IdCasoSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdSisegoSW { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEquipoTrabajo { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaInicio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaFin { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? DiasGestion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Observaciones { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }

    }
}
