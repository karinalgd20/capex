﻿using System;
using System.Runtime.Serialization;
namespace Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad
{
    public class OportunidadSTDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdOportunidadPadre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdCasoSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadAux { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdMotivoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdFuncionPropietario { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoEntidadCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdLineaNegocio { get; set; }        
        [DataMember(EmitDefaultValue = false)]
        public int? IdSector { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdSegmentoNegocio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoCicloVenta { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoCicloImplementacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Prioridad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? ProbalidadExito { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? PorcentajeCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? PorcentajeCheckList { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdFase { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEtapa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? FlgCapexMayor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ImporteCapex { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdMoneda { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ImporteFCV { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaApertura { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstadoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdRecursoComercial { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdRecursoPreventa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }

    }
}
