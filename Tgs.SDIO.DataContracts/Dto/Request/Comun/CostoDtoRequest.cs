﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class CostoDtoRequest
    {
        [DataMember]
        public int IdCosto { get; set; }

        [DataMember]
        public int? IdTipoCosto { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public decimal? Monto { get; set; }

        [DataMember]
        public decimal? VelocidadSubidaKBPS { get; set; }

        [DataMember]
        public decimal? PorcentajeGarantizado { get; set; }

        [DataMember]
        public decimal? PorcentajeSobresuscripcion { get; set; }

        [DataMember]
        public decimal? CostoSegmentoSatelital { get; set; }

        [DataMember]
        public decimal? InvAntenaHubUSD { get; set; }

        [DataMember]
        public decimal? AntenaCasaClienteUSD { get; set; }

        [DataMember]
        public decimal? Instalacion { get; set; }

        [DataMember]
        public int? IdUnidadConsumo { get; set; }

        [DataMember]
        public int? IdTipificacion { get; set; }

        [DataMember]
        public string CodigoModelo { get; set; }

        [DataMember]
        public string Modelo { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }

        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public int? IdUsuarioEdicion { get; set; }

        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        [DataMember]
        public int IdMedio { get; set; }

        [DataMember]
        public int IdServicioGrupo { get; set; }
    }
}
