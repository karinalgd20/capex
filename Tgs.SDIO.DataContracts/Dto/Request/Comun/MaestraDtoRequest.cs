using System;
using System.Runtime.Serialization;
namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class MaestraDtoRequest
    {
        [DataMember]
        public int IdMaestra { get; set; }
        [DataMember]
        public int? IdRelacion { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string Valor { get; set; }
        [DataMember]
        public string Valor2 { get; set; }
        [DataMember]
        public string Comentario { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }
        [DataMember]
        public int? IdOpcion { get; set; }
    }
}