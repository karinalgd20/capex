using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class DGDtoRequest
    {

        [DataMember]

        public int IdDG { get; set; }
        [DataMember]

        public int? IdConcepto { get; set; }
        [DataMember]

        public string Concatenado { get; set; }
        [DataMember]

        public string AEReducido { get; set; }
        [DataMember]

        public int IdEstado { get; set; }
        [DataMember]

        public int IdUsuarioCreacion { get; set; }
        [DataMember]

        public DateTime? FechaCreacion { get; set; }
        [DataMember]

        public int? IdUsuarioEdicion { get; set; }
        [DataMember]

        public DateTime? FechaEdicion { get; set; }
        [DataMember]

        public string Tipo { get; set; }
    }
}
