﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class Colaborador
    {
        [DataMember]
        public int IdColaborador { set; get; }
        [DataMember]
        public string IdentificadorColaborador { set; get; }
        [DataMember]
        public string NombreCompleto { set; get; }
        [DataMember]
        public string Email { set; get; }
        [DataMember]
        public int IdTipoColaboradorFianzaTm { set; get; }
        [DataMember]
        public int IdEmailAlertasTm { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime FechaEdicion { set; get; }

    }
}
