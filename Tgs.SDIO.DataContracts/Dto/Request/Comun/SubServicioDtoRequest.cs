﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class SubServicioDtoRequest
    {

        [DataMember]
        public int IdSubServicio { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string DescripcionEquivalencia { get; set; }
        [DataMember]
        public int? IdTipoSubServicio { get; set; }
        [DataMember]
        public int IdDepreciacion { get; set; }
        [DataMember]
        public int Orden { get; set; }
        [DataMember]
        public int Negrita { get; set; }
        [DataMember]
        public decimal CostoInstalacion { get; set; }
        [DataMember]
        public int? IdGrupo { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }

    }
}
