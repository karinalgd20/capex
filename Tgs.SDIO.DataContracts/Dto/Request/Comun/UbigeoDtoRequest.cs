﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;


namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]

    public class UbigeoDtoRequest : AuditoriaDto
    {
        [DataMember(EmitDefaultValue = false)]
        public int Id { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CodigoDepartamento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CodigoProvincia { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string CodigoDistrito { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Nombre { get; set; } 
    }
}
