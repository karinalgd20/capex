using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class ProveedorDtoRequest : AuditoriaDto
    {
        [DataMember]
         public int IdProveedor { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int? TipoProveedor { get; set; }
        [DataMember]
        public string RazonSocial { get; set; }
        [DataMember]
        public string CodigoProveedor { get; set; }
        [DataMember]
        public string RUC { get; set; }
        [DataMember]
        public string Pais { get; set; }
        [DataMember]
        public string IdSecuencia { get; set; }
        [DataMember]
        public string NombrePersonaContacto { get; set; }
        [DataMember]
        public string TelefonoContactoPrincipal { get; set; }
        [DataMember]
        public string CorreoContactoPrincipal { get; set; }
        [DataMember]
        public string TelefonoContactoSecundario { get; set; }
        [DataMember]
        public string CorreoContactoSecundario { get; set; }
        [DataMember]
        public string CodigoSRM { get; set; }
    }
}
