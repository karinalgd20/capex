﻿using System.Runtime.Serialization;


namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class ConceptoSeguimientoDtoRequest
    {

        [DataMember(EmitDefaultValue = false)]
        public int IdConcepto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdConceptoPadre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Nivel { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? OrdenVisual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }

    }
}
