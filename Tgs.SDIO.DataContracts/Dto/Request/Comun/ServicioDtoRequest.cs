﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class ServicioDtoRequest
    {

        [DataMember]
        public int IdServicio { get; set; }
        [DataMember]
        public int? IdLineaNegocio { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int? IdMedio { get; set; }
        [DataMember]
        public int? IdGrupo { get; set; }
        [DataMember]
        public int? IdTipoEnlace { get; set; }

        [DataMember]
        public int? IdServicioCMI { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }

        [DataMember]
        public int IdServicioGrupo { get; set; }

    }
}
