﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;



namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class ActividadDtoRequest
    {

        [DataMember(EmitDefaultValue = false)]
        public int IdActividad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdActividadPadre { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Predecesoras { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdFase { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEtapa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? FlgCapexMayor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? FlgCapexMenor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? AsegurarOferta { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdAreaSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? NumeroDiaCapexMenor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? CantidadDiasCapexMenor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? NumeroDiaCapexMayor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? CantidadDiasCapexMayor { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdTipoActividad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<ComboDtoResponse> ListSegmentoNegocio { get; set; }

    }
}
