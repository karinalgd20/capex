using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class ServicioCMIDtoRequest
    {

        [DataMember]
        public int IdServicioCMI { get; set; }
        [DataMember]
        public string CodigoCMI { get; set; }
        [DataMember]
        public string DescripcionPlantilla { get; set; }
        [DataMember]
        public string DescripcionOriginal { get; set; }
        [DataMember]
        public string DescripcionCMI { get; set; }
        [DataMember]
        public int? IdGerenciaProducto { get; set; }
        [DataMember]
        public int? IdLineaNegocio { get; set; }
        [DataMember]
        public int? IdCentroCosto { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

    }
}
