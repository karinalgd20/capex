﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class ServicioSubServicioDtoRequest
    {
        [DataMember]
        public int IdServicio { get; set; }
        [DataMember]
        public int? IdSubServicio { get; set; }
        [DataMember]
        public int IdServicioSubServicio { get; set; }
        [DataMember]
        public int? IdTipoCosto { get; set; }
        [DataMember]
        public int? IdProveedor { get; set; }
        [DataMember]
        public string ContratoMarco { get; set; }
        [DataMember]
        public int? IdPeriodos { get; set; }
        [DataMember]
        public int? Inicio { get; set; }
        [DataMember]
        public decimal? Ponderacion { get; set; }
        [DataMember]
        public int? IdMoneda { get; set; }
        [DataMember]
        public int? IdGrupo { get; set; }
        [DataMember]
        public int? FlagSISEGO { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        [DataMember]
        public List<ComboDtoRequest> ListServicioSelect { get; set; }

        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }
    }
}
