﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;


namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]

    public class TipoCambioDetalleDtoRequest : AuditoriaDto
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdTipoCambioDetalle { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoCambio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdMoneda { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipificacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Anio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Monto { get; set; }
    }
}
