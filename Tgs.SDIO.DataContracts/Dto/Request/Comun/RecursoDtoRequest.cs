using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public  class RecursoDtoRequest : AuditoriaDto
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdRecurso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? TipoRecurso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string UserNameSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Nombre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DNI { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario{ get; set; }      

        [DataMember(EmitDefaultValue = false)]
        public int IdArea { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Email { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioRais { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEmpresa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdRolDefecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdCargo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombreInterno { get; set; }
    }
}
