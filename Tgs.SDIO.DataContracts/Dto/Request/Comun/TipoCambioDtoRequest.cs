﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;


namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]

    public class TipoCambioDtoRequest : AuditoriaDto
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdTipoCambio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdLineaNegocio { get; set; }   

        [DataMember(EmitDefaultValue = false)]
        public int? IdMoneda { get; set; }
    }
}
