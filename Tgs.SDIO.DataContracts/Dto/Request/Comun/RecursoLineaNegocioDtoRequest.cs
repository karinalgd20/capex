﻿using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class RecursoLineaNegocioDtoRequest :AuditoriaDto 
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdRecurso { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdLineaNegocio { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioRais { get; set; }

        [DataMember]
        public int Id { get; set; }

    }
}
