﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;


namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]

    public class RiesgoProyectoDtoRequest : AuditoriaDto
    {
        [DataMember(EmitDefaultValue = false)]
        public int Id { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int IdTipo { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaDeteccion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Consecuencias { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PlanAccion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Responsable { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdProbabilidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdImpacto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdNivel { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCierre { get; set; }

    }
}
