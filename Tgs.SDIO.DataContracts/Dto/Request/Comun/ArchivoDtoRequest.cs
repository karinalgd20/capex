﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class ArchivoDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int CodigoTabla { get; set; }

        [DataMember]
        public int IdEntidad { get; set; }

        [DataMember]  
        public int? IdCategoria { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string NombreInterno { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public string Ruta { get; set; }
    }
}