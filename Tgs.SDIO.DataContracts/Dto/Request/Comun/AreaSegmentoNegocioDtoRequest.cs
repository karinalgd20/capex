﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class AreaSegmentoNegocioDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdAreaSegmentoNegocio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdAreaSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdSegmentoNegocio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario { get; set; }

    }
}
