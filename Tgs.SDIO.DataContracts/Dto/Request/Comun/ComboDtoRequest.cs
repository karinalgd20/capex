﻿using System.Runtime.Serialization;
namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class ComboDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public string id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string label { get; set; }
    }
}
