﻿using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class RecursoJefeDtoRequest: AuditoriaDto
    {
        [DataMember]
        public int IdJefe { get; set; }

        [DataMember]
        public int IdRecurso { get; set; }

        [DataMember]
        public int? IdUsuarioRais { get; set; }

        [DataMember]
        public int Id { get; set; }
    }
}
