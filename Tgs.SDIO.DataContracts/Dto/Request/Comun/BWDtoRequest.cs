﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class BWDtoRequest
    {

   
        [DataMember]
        public int IdBW { get; set; }
        [DataMember]
        public int? IdConcepto { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public decimal? BW_NUM { get; set; }
        [DataMember]
        public decimal? Precio { get; set; }
        [DataMember]
        public int? IdLineaProducto { get; set; }
        [DataMember]
        public int? IdPestana { get; set; }
        [DataMember]
        public int? IdGrupo { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime? FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }




    }
}
