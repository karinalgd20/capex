using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class ContratoMarcoDtoRequest
    {
        [DataMember]
        public int IdContratoMarco { get; set; }
        [DataMember]
        public string NumContrato { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string ProcesoAdjudicacion { get; set; }
        [DataMember]
        public DateTime? FechaInicio { get; set; }
        [DataMember]
        public DateTime? FechaFin { get; set; }
        [DataMember]
        public string Catalogado { get; set; }
        [DataMember]
        public decimal? ImporteContrato { get; set; }
        [DataMember]
        public decimal? ImporteConsumido { get; set; }
        [DataMember]
        public string Moneda { get; set; }
        [DataMember]
        public string ProductoGerencia { get; set; }
        [DataMember]
        public int? IdProveedor { get; set; }
        [DataMember]
        public string Proveedor { get; set; }
        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int Tamanio { get; set; }
    }
}
