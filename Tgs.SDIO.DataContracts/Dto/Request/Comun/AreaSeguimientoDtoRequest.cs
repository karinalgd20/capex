using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public  class AreaSeguimientoDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdAreaSeguimiento { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DescripcionSegmento { get; set; }        
        [DataMember(EmitDefaultValue = false)]
        public int? OrdenVisual { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<ComboDtoResponse> ListSegmentoNegocio { get; set; }

    }

}
