﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class ClienteDtoRequest
    {


        [DataMember]
        public int IdCliente { get; set; }
        [DataMember]
        public string CodigoCliente { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int? IdSector { get; set; }
        [DataMember]
        public string GerenteComercial { get; set; }
        [DataMember]
        public int? IdDireccionComercial { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public int? IdUsuario { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }
        [DataMember]
        public string NumeroIdentificadorFiscal { get; set; }
        [DataMember]
        public int? IdTipoIdentificadorFiscalTm { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public int? IdTipoEntidad { get; set; }


        [DataMember]
        public int Indice { set; get; }
        [DataMember]
        public int Tamanio { set; get; }
        [DataMember]
        public string ColumnName { set; get; }
        [DataMember]
        public string OrderType { set; get; }

    }
}
