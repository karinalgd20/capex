﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class CasoNegocioServicioDtoRequest
    {
        [DataMember]
        public int IdCasoNegocio { get; set; }
        [DataMember]
        public int IdCasoNegocioServicio { get; set; }
        [DataMember]
        public int IdServicio { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime FechaEdicion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }
    }
}