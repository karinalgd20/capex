﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class ConceptoDtoRequest
    {
        [DataMember]
        public int IdConcepto { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int IdTipoConcepto { get; set; }
        [DataMember]
        public int IdDepreciacion { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }
    }
}