﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Compra
{
    [DataContract]
    public class EtapaPeticionCompraDtoRequest
    {
        [DataMember]
        public int IdPeticionCompra { get; set; }
        [DataMember]
        public int IdEtapaPeticionCompra { get; set; }
        [DataMember]
        public int IdEstadoEtapa { get; set; }
        [DataMember]
        public string Comentario { get; set; }
        [DataMember]
        public int IdOrden { get; set; }
        [DataMember]
        public int IdOrdenObservar { get; set; }
        [DataMember]
        public int IdUsuario { get; set; }
        [DataMember]
        public int IrSiguienteEtapa { get; set; }
        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int Tamanio { get; set; }

    }
}
