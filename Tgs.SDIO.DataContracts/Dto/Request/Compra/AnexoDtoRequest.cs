﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Compra
{
    [DataContract]
    public class AnexoDtoRequest
    {
        [DataMember]
        public int IdAnexoPeticionCompra { get; set; }
        [DataMember]
        public int IdPeticionCompra { get; set; }
        [DataMember]
        public int IdTipoDocumento { get; set; }
        [DataMember]
        public string NombreArchivo { get; set; }
        [DataMember]
        public int IdUsuario { get; set; }

        [DataMember]
        public byte[] ArchivoAdjunto { get; set; }
    }
}