﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Compra
{
    [DataContract]
    public class DetalleClienteProveedorDtoRequest
    {
        [DataMember]
        public int IdDetalleClientePro { get; set; }
        [DataMember]
        public int IdPeticionCompra { get; set; }
        [DataMember]
        public int IdCliente { get; set; }
        [DataMember]
        public int IdProveedor { get; set; }
        [DataMember]
        public string CodigoSap { get; set; }
        [DataMember]
        public string CodigoSrm { get; set; }
        [DataMember]
        public string NombreContacto { get; set; }
        [DataMember]
        public string EmailContacto { get; set; }
        [DataMember]
        public string TelefonoContacto { get; set; }
        [DataMember]
        public int IdUsuario { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
    }
}