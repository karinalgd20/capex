﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Compra
{
    [DataContract]
    public class AnexoArchivoDtoRequest
    {
        [DataMember]
        public int IdAnexoArchivo { get; set; }
        [DataMember]
        public int IdAnexoPeticionCompras { get; set; }
        [DataMember]
        public byte[] ArchivoAdjunto { get; set; }
        [DataMember]
        public int IdUsuario { get; set; }
    }
}