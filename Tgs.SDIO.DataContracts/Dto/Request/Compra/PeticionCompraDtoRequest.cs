﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Compra
{
    [DataContract]
    public class PeticionCompraDtoRequest
    {
        [DataMember]
        public int IdPeticionCompra { get; set; }
        [DataMember]
        public string CodigoPeticionCompra { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public bool? AsociadoAProyecto { get; set; }
        [DataMember]
        public int? IdTipoCompra { get; set; }
        [DataMember]
        public int? IdTipoCosto { get; set; }
        [DataMember]
        public int? IdEtapa { get; set; }
        [DataMember]
        public int? IdLineaNegocio { get; set; }
        [DataMember]
        public int? IdComprador { get; set; }
        [DataMember]
        public int? IdContratoMarco { get; set; }
        [DataMember]
        public int? IdPrePeticionDetalle { set; get; }
        [DataMember]
        public bool? AdjuntaCotizacion { get; set; }
        [DataMember]
        public decimal? MontoCotizacion { get; set; }
        [DataMember]
        public string SubGrupoCompras{ get; set; }
        [DataMember]
        public int? IdAreaSolicitante { get; set; }
        [DataMember]
        public int? IdCentroCosto { get; set; }
        [DataMember]
        public int? IdRecursoSolicitante { get; set; }
        [DataMember]
        public int? IdRecursoGerente { get; set; }
        [DataMember]
        public int? IdRecursoDirector { get; set; }
        [DataMember]
        public int? IdResponsablePostventa { get; set; }
        [DataMember]
        public string ElementoPEP { get; set; }
        [DataMember]
        public string Grafo { get; set; }
        [DataMember]
        public string Cuenta { get; set; }
        [DataMember]
        public string DescripcionCuenta { get; set; }
        [DataMember]
        public string AreaFuncional { get; set; }
        [DataMember]
        public bool? PeticionPenalidad { get; set; }
        [DataMember]
        public bool? ArrendamientoRenting { get; set; }
        [DataMember]
        public int? IdTipoMoneda { get; set; }
        [DataMember]
        public decimal? CostoTotal { get; set; }
        [DataMember]
        public string SustentoCualitativo { get; set; }
        [DataMember]
        public string DetalleSolpe { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime FechaEdicion { get; set; }
        [DataMember]
        public string FechaInicio { get; set; }
        [DataMember]
        public string FechaFin { get; set; }
        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int Tamanio { get; set; }
    }
}
