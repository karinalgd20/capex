﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Compra
{
    [DataContract]
    public class CuentaDtoRequest
    {
        [DataMember]
        public int IdCuenta { set; get; }
        [DataMember]
        public string Cuenta { set; get; }
        [DataMember]
        public string Descripcion { set; get; }
        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int Tamanio { get; set; }
    }
}
