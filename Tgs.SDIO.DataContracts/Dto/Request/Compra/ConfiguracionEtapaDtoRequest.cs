﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Compra
{
    [DataContract]
    public class ConfiguracionEtapaDtoRequest
    {
        [DataMember]
        public int IdTipoCompra { get; set; }
        [DataMember]
        public int IdTipoCosto { get; set; }
        [DataMember]
        public int Orden { get; set; }
    }
}
