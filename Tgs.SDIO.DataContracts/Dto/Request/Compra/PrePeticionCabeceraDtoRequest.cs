﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Compra
{
    [DataContract]
    public class PrePeticionCabeceraDtoRequest
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdCliente { get; set; }
        [DataMember]
        public int? IdProyecto { get; set; }
        [DataMember]
        public int? IdLineaCMI { get; set; }
        [DataMember]
        public string CodigoSIGO { get; set; }
        [DataMember]
        public int? Tipo { get; set; }
        [DataMember]
        public DateTime? FechaInicioProyecto { get; set; }
        [DataMember]
        public int? TipoCosto { get; set; }
        [DataMember]
        public decimal? Monto { get; set; }
        [DataMember]
        public int? IdDetalle { get; set; }
        [DataMember]
        public string DescripcionCliente { get; set; }
        [DataMember]
        public string DescripcionProyecto { get; set; }
        [DataMember]
        public string Servicio { get; set; }
        [DataMember]
        public string DescripcionPrePeticion { get; set; }
        [DataMember]
        public int IdUsuario { get; set; }

        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int Tamanio { get; set; }
    }
}