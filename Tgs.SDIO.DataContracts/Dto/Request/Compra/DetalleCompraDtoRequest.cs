﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Compra
{
    [DataContract]
    public class DetalleCompraDtoRequest
    {
        [DataMember]
        public int IdDetalleCompra { set; get; }
        [DataMember]
        public int IdPeticionCompra { set; get; }
        [DataMember]
        public int? IdCuenta { set; get; }
        [DataMember]
        public string Actividad { set; get; }
        [DataMember]
        public int? IdCuentaAnterior { set; get; }
        [DataMember]
        public string Descripcion { set; get; }
        [DataMember]
        public int? Cantidad { set; get; }
        [DataMember]
        public decimal CostoPorUnidad { set; get; }
        [DataMember]
        public decimal Total { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }

        [DataMember]
        public int? IdTipoMoneda { set; get; }
        [DataMember]
        public string SustentoCualitativo { set; get; }
        [DataMember]
        public string DetalleSolpe { set; get; }
        [DataMember]
        public decimal? TotalCosto { set; get; }

        [DataMember]
        public bool EsNuevo { set; get; }
        [DataMember]
        public bool EsModificado { set; get; }
        [DataMember]
        public string AccionEstrategica { set; get; }
        [DataMember]
        public string Pep2 { set; get; }
    }
}
