﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Compra
{
    [DataContract]
    public class CentroCostoRecursoDtoRequest
    {
        [DataMember]
        public int IdCentroCosto { get; set; }
        [DataMember]
        public int IdAreaSolicitante { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int Tamanio { get; set; }
    }
}
