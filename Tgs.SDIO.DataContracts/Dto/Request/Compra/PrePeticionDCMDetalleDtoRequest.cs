﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Compra
{
    [DataContract]
    public class PrePeticionDetalleDCMDetalleDtoRequest
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdCabecera { get; set; }
        [DataMember]
        public int? Numero { get; set; }
        [DataMember]
        public int? TipoCosto { get; set; }
        [DataMember]
        public string ProductManager { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int? IdProveedor { get; set; }
        [DataMember]
        public int? IdContratoMarco { get; set; }
        [DataMember]
        public int? AutorizadoPor { get; set; }
        [DataMember]
        public int? TipoEntrega { get; set; }
        [DataMember]
        public int? PlazoEntrega { get; set; }
        [DataMember]
        public int? PQAdjudicado { get; set; }
        [DataMember]
        public int? CotizacionAdjunta { get; set; }
        [DataMember]
        public DateTime? FechaEntregaOC { get; set; }
        [DataMember]
        public string Observaciones { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public int? PosicionOC { get; set; }
        [DataMember]
        public string MonedaCompra { get; set; }
        [DataMember]
        public decimal Monto { get; set; }
        [DataMember]
        public int IdUsuario { get; set; }
        [DataMember]
        public int IdLineaProducto { get; set; }
        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int Tamanio { get; set; }
    }
}