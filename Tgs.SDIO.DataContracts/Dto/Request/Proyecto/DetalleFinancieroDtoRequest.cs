﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Proyecto
{
    [DataContract]
    public class DetalleFinancieroDtoRequest
    {
        [DataMember]
        public string Item { get; set; }
        [DataMember]
        public string NumeroDeOportunidad { get; set; }
        [DataMember]
        public string NroDeCaso { get; set; }
        [DataMember]
        public string TipoDeProyecto { get; set; }
        [DataMember]
        public string PagoUnico { get; set; }
        [DataMember]
        public string PagoRecurrente { get; set; }
        [DataMember]
        public string NroDeMesesDePagoRecurrente { get; set; }
        [DataMember]
        public string PagoTotal { get; set; }
        [DataMember]
        public string TipoDeCambio { get; set; }
        [DataMember]
        public string MargenOibda { get; set; }
        [DataMember]
        public string Van { get; set; }
        [DataMember]
        public string VanVai { get; set; }
        [DataMember]
        public string Oibda { get; set; }
        [DataMember]
        public string PaybackMeses { get; set; }
        [DataMember]
        public string AnalistaFinancieroEvaluador { get; set; }
        [DataMember]
        public string AnalistaFinancieroUsuarioLotus { get; set; }
        [DataMember]
        public string AnalistaFinancieroCorreoOutlook { get; set; }
        [DataMember]
        public string ImporteDeVentasUss { get; set; }
        [DataMember]
        public string CostosOpex { get; set; }
        [DataMember]
        public string Capex { get; set; }
        [DataMember]
        public string AntenasVsatCapexCapexValorResidual { get; set; }
        [DataMember]
        public string AntenasVsatCapexLogInversa { get; set; }
        [DataMember]
        public string ClearChannelCapexCapexValorResidual { get; set; }
        [DataMember]
        public string ClearChannelCapexLogInversa { get; set; }
        [DataMember]
        public string EquiposDeSeguridadCapexCapexValorResidual { get; set; }
        [DataMember]
        public string EquiposDeSeguridadCapexLogInversa { get; set; }
        [DataMember]
        public string EquiposEeEeCapexCapexValorResidual { get; set; }
        [DataMember]
        public string EstudiosEspecialesCapexValorResidual { get; set; }
        [DataMember]
        public string GabinetesCapexCapexValorResidual { get; set; }
        [DataMember]
        public string GabinetesCapexLogInversa { get; set; }
        [DataMember]
        public string HardwareCapexCapexValorResidual { get; set; }
        [DataMember]
        public string HardwareCapexLogInversa { get; set; }
        [DataMember]
        public string InstalacionEquiposYLicenciasCchnCapexValorResidual { get; set; }
        [DataMember]
        public string InstalacionEquiposYLicenciasCchnCapexLogInversa { get; set; }
        [DataMember]
        public string InstalacionEquiposYLicenciasVsatCapexValorResidual { get; set; }
        [DataMember]
        public string InstalacionEquiposYLicenciasVsatCapexLogInversa { get; set; }
        [DataMember]
        public string InstalacionesRoutersCapexValorResidual { get; set; }
        [DataMember]
        public string InstalacionesRoutersLogInversa { get; set; }
        [DataMember]
        public string Inversion35GhzCapexValorResidual { get; set; }
        [DataMember]
        public string Inversion35GhzLogInversa { get; set; }
        [DataMember]
        public string ModemsCapexCapexValorResidual { get; set; }
        [DataMember]
        public string ModemsCapexLogInversa { get; set; }
        [DataMember]
        public string RoutersCapexCapexValorResidual { get; set; }
        [DataMember]
        public string RoutersCapexLogInversa { get; set; }
        [DataMember]
        public string SmartvpnCapexCapexValorResidual { get; set; }
        [DataMember]
        public string SoftwareCapexCapexValorResidual { get; set; }
        [DataMember]
        public string SolarwindCapexCapexValorResidual { get; set; }
        [DataMember]
        public string ComisionComercial { get; set; }
        [DataMember]
        public string ContingenciaDeCostos { get; set; }
        [DataMember]
        public string CostosCambium { get; set; }
        [DataMember]
        public string CostosCircuitos { get; set; }
        [DataMember]
        public string CostosInternosDatacenter { get; set; }
        [DataMember]
        public string CostosInternosOutsourcingDesktop { get; set; }
        [DataMember]
        public string CostosInternosOutsourcingImpresion { get; set; }
        [DataMember]
        public string InstalacionEquiposYLicenciasOpex { get; set; }
        [DataMember]
        public string OtrosCostosIndirectos { get; set; }
        [DataMember]
        public string PersonalPropioTelefonica { get; set; }
        [DataMember]
        public string PersonalPropioTelefonicaMantSop { get; set; }
        [DataMember]
        public string SegmentoSatelital { get; set; }
        [DataMember]
        public string SoporteYMttoEqTelecomunicaciones { get; set; }
        [DataMember]
        public string Telecomunicaciones { get; set; }
        [DataMember]
        public string Telecomunicaciones35Ghz { get; set; }
        [DataMember]
        public string TelecomunicacionesSmartvpn { get; set; }
        [DataMember]
        public string TelecomunicacionesSolarwin { get; set; }
        [DataMember]
        public string ViaticosYGastosDePersonalTdp { get; set; }
        [DataMember]
        public string AntenasVsatCapexNoStock { get; set; }
        [DataMember]
        public string AntenasVsatCapexStock { get; set; }
        [DataMember]
        public string ClearChannelCapexNoStock { get; set; }
        [DataMember]
        public string ClearChannelCapexStock { get; set; }
        [DataMember]
        public string EquiposDeSeguridadCapexNoStock { get; set; }
        [DataMember]
        public string EquiposDeSeguridadCapexStock { get; set; }
        [DataMember]
        public string EquiposEeEeCapexEeEe { get; set; }
        [DataMember]
        public string EstudiosEspecialesEeEe { get; set; }
        [DataMember]
        public string GabinetesCapexNoStock { get; set; }
        [DataMember]
        public string HardwareCapexNoStock { get; set; }
        [DataMember]
        public string InstalacionEquiposYLicenciasCchnNoStock { get; set; }
        [DataMember]
        public string InstalacionEquiposYLicenciasCchnStock { get; set; }
        [DataMember]
        public string InstalacionEquiposYLicenciasVsatNoStock { get; set; }
        [DataMember]
        public string InstalacionEquiposYLicenciasVsatStock { get; set; }
        [DataMember]
        public string InstalacionesRoutersNoStock { get; set; }
        [DataMember]
        public string InstalacionesRoutersStock { get; set; }
        [DataMember]
        public string Inversion35GhzNoStock { get; set; }
        [DataMember]
        public string Inversion35GhzStock { get; set; }
        [DataMember]
        public string ModemsCapexNoStock { get; set; }
        [DataMember]
        public string ModemsCapexStock { get; set; }
        [DataMember]
        public string RoutersCapexNoStock { get; set; }
        [DataMember]
        public string RoutersCapexStock { get; set; }
        [DataMember]
        public string SmartvpnCapexNoStock { get; set; }
        [DataMember]
        public string SoftwareCapexNoStock { get; set; }
        [DataMember]
        public string SolarwindCapexNoStock { get; set; }
        [DataMember]
        public string CableadoYSuministrosDc { get; set; }
        [DataMember]
        public string CableadoYSuministrosPe { get; set; }
        [DataMember]
        public string ComisionPartnerDc { get; set; }
        [DataMember]
        public string ComisionPartnerPe { get; set; }
        [DataMember]
        public string GestionDeProyectosDt { get; set; }
        [DataMember]
        public string GestionDeProyectosPe { get; set; }
        [DataMember]
        public string HardwareCostoDeVentas { get; set; }
        [DataMember]
        public string InstalacionEntregaConfiguracionYTransporteDc { get; set; }
        [DataMember]
        public string InstalacionEntregaConfiguracionYTransportePe { get; set; }
        [DataMember]
        public string MesaDeAyuda { get; set; }
        [DataMember]
        public string Penalidades { get; set; }
        [DataMember]
        public string PersonalTercerosDatacenter { get; set; }
        [DataMember]
        public string PersonalTercerosOutsourcingDeImpresion { get; set; }
        [DataMember]
        public string PersonalTercerosRenovacionTecnologica { get; set; }
        [DataMember]
        public string PersonalTercerosServiciosTransaccionales { get; set; }
        [DataMember]
        public string PlataformaTraficoSeguro { get; set; }
        [DataMember]
        public string RentingDeEquiposCcuuYSeguridad { get; set; }
        [DataMember]
        public string RentingDeImpresoras { get; set; }
        [DataMember]
        public string RentingDeLicencias { get; set; }
        [DataMember]
        public string RentingDePcsLaptopsYAccesorios { get; set; }
        [DataMember]
        public string RentingDeServidoresYAccesorios { get; set; }
        [DataMember]
        public string SalidaInternet { get; set; }
        [DataMember]
        public string SegmentoSatelitalIntenet { get; set; }
        [DataMember]
        public string SegurosDeEquipos { get; set; }
        [DataMember]
        public string ServiciosCloud { get; set; }
        [DataMember]
        public string ServiciosDeCapacitacionDc { get; set; }
        [DataMember]
        public string ServiciosDeCapacitacionPe { get; set; }
        [DataMember]
        public string ServicioDeDisponibilidadTecnologica { get; set; }
        [DataMember]
        public string ServiciosParaOutsourcingDeImpresion { get; set; }
        [DataMember]
        public string ServiciosParaRenovacionTecnologica { get; set; }
        [DataMember]
        public string Simcard { get; set; }
        [DataMember]
        public string SoftwareCostosDeVentas { get; set; }
        [DataMember]
        public string SoporteYMantenimientoCcuuCentrales { get; set; }
        [DataMember]
        public string SoporteYMantenimientoCcuuHwSw { get; set; }
        [DataMember]
        public string SoporteYMantenimientoCcuuSsp { get; set; }
        [DataMember]
        public string SoporteYMantenimientoTi { get; set; }
        [DataMember]
        public string SuministrosDeBackup { get; set; }
        [DataMember]
        public string TelecomunicacionesTerceros { get; set; }
        [DataMember]
        public string TerminalesAlquiler { get; set; }
        [DataMember]
        public string TerminalesVenta { get; set; }
        [DataMember]
        public string Tributos { get; set; }
        [DataMember]
        public string ViaticosYGastosDePersonalDc { get; set; }
        [DataMember]
        public string ViaticosYGastosDePersonalPe { get; set; }


        //Datos adiciones del archivo CMI
        [DataMember]
        public string Linea { get; set; }
        [DataMember]
        public string Sublinea { get; set; }
        [DataMember]
        public string Servicio { get; set; }
        [DataMember]
        public string Producto { get; set; }
        [DataMember]
        public string ProductoAf { get; set; }
        [DataMember]
        public string AnalistaControlGastosUsuarioLotus { get; set; }
        [DataMember]
        public string AnalistaControlGastosCorreoOutlook { get; set; }
        [DataMember]
        public string Porcentaje { get; set; }
        [DataMember]
        public string CeCo { get; set; }
        [DataMember]
        public int TipoTabla { get; set; }
        [DataMember]
        public int IdProyecto { get; set; }
        [DataMember]
        public int Fila { get; set; }


    }
}
