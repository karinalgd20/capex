﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Proyecto
{
    [DataContract]
    public class ServicioCMIFinancieroDtoRequest
    {
        [DataMember]
        public int IdServicioFinanciero { get; set; }
        [DataMember]
        public int IdDetalleFinanciero { get; set; }
        [DataMember]
        public string CodigoCMI { get; set; }
        [DataMember]
        public string ElementoPEP { get; set; }
        [DataMember]
        public string CentroGestor { get; set; }
        [DataMember]
        public string CeCo { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public int IdUsuarioEdicion { get; set; }

        [DataMember]
        public int IdProyecto { get; set; }
    }
}
