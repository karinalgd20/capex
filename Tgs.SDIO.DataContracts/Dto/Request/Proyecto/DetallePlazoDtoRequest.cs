﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Proyecto
{
    [DataContract]
    public class DetallePlazoDtoRequest
    {
        [DataMember]
        public int IdDetallePlazo { get; set; }
        [DataMember]
        public int? IdProyecto { get; set; }
        [DataMember]
        public int? IdActividad { get; set; }
        [DataMember]
        public decimal? Cantidad { get; set; }
        [DataMember]
        public int? IdUnidadMedida { get; set; }
        [DataMember]
        public DateTime? FechaInicio { get; set; }
        [DataMember]
        public DateTime? FechaFin { get; set; }
        [DataMember]
        public bool? VentaDirecta { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        [DataMember]
        public bool Modificado { get; set; }
        [DataMember]
        public int? Anios { get; set; }
        [DataMember]
        public int? Meses { get; set; }
        [DataMember]
        public int? Dias { get; set; }
    }
}