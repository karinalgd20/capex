﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Proyecto
{
    [DataContract]
    public class EtapaProyectoDtoRequest
    {
        [DataMember]
        public int IdDetalleEtapa { get; set; }
        [DataMember]
        public int? IdProyecto { get; set; }
        [DataMember]
        public int? IdFase { get; set; }
        [DataMember]
        public int? IdEtapa { get; set; }
        [DataMember]
        public DateTime? FechaInicio { get; set; }
        [DataMember]
        public DateTime? FechaFin { get; set; }
        [DataMember]
        public string Observaciones { get; set; }
        [DataMember]
        public int? IdEstadoEtapa { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        
    }
}