﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Proyecto
{
    [DataContract]
    public class RecursoProyectoDtoRequest
    {
        [DataMember]
        public int IdAsignacion { get; set; }
        [DataMember]
        public int? IdProyecto { get; set; }
        [DataMember]
        public int? IdRecurso { get; set; }
        [DataMember]
        public int? IdRol { get; set; }
        [DataMember]
        public int? PorcentajeDedicacion { get; set; }
        [DataMember]
        public string Observaciones { get; set; }
        [DataMember]
        public DateTime? FInicioAsignacion { get; set; }
        [DataMember]
        public DateTime? FFinAsignacion { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        [DataMember]
        public bool Modificado { get; set; }
    }
}