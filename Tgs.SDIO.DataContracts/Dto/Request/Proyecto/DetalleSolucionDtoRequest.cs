﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Proyecto
{
    [DataContract]
    public class DetalleSolucionDtoRequest
    {
        [DataMember]
        public int IdDetalleSolucion { get; set; }
        [DataMember]
        public int IdProyecto { get; set; }
        [DataMember]
        public int IdTipoSolucion { get; set; }
    }
}
