﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Proyecto
{
    [DataContract]
    public class ProyectoDtoRequest
    {
        [DataMember]
        public int IdProyecto { get; set; }
        [DataMember]
        public int? IdProyectoPadre { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string IdProyectoSF { get; set; }
        [DataMember]
        public string IdProyectoLKM { get; set; }
        [DataMember]
        public string IdOportunidadSF { get; set; }
        [DataMember]
        public string IdProyectoTDP { get; set; }
        [DataMember]
        public int? IdPortafolio { get; set; }
        [DataMember]
        public int? IdPrograma { get; set; }
        [DataMember]
        public int? IdEmpresa { get; set; }
        [DataMember]
        public int? IdAreaEmpresa { get; set; }
        [DataMember]
        public int? Prioridad { get; set; }
        [DataMember]
        public int? IdCliente { get; set; }
        [DataMember]
        public int? IdComplejidad { get; set; }
        [DataMember]
        public int? IdClaseProyecto { get; set; }
        [DataMember]
        public int? IdFaseActual { get; set; }
        [DataMember]
        public int? IdEtapaActual { get; set; }
        [DataMember]
        public int? EsLicitacion { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public DateTime? FechaOportunidadGanada { get; set; }
        [DataMember]
        public DateTime? FechaAceptacion { get; set; }
        [DataMember]
        public DateTime? FechaFirmaContrato { get; set; }
        [DataMember]
        public int? IdMonedaPagoUnico { get; set; }
        [DataMember]
        public decimal? TotalPagoUnico { get; set; }
        [DataMember]
        public int? IdMonedaRecurrenteMensual { get; set; }
        [DataMember]
        public decimal? TotalRecurrenteMensual { get; set; }
        [DataMember]
        public int? IdMoneda { get; set; }
        [DataMember]
        public decimal? Van { get; set; }
        [DataMember]
        public decimal? VanVai { get; set; }
        [DataMember]
        public decimal? Oibda { get; set; }
        [DataMember]
        public decimal? MargenOibda { get; set; }
        [DataMember]
        public decimal? PayBack { get; set; }
        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int Tamanio { get; set; }
        [DataMember]
        public int? IdOficinaOportunidad { get; set; }
        [DataMember]
        public int? IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime? FechaCrea { get; set; }
        [DataMember]
        public int? IdUsuarioModifica { get; set; }
        [DataMember]
        public DateTime? FechaModifica { get; set; }
        [DataMember]
        public string ListaTipoSolucion { get; set; }
        [DataMember]
        public List<DetalleSolucionDtoRequest> ListaTiposNuevo { get; set; }
        [DataMember]
        public List<DetalleSolucionDtoRequest> ListaTiposEliminado { get; set; }

        [DataMember]
        public string Cliente { get; set; }

        [DataMember]
        public string FechaInicio { get; set; }
        [DataMember]
        public string FechaFin { get; set; }

    }
}
