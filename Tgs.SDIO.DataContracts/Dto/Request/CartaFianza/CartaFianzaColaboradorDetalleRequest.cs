﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.CartaFianza
{
    [DataContract]
    public class CartaFianzaColaboradorDetalleRequest
    {
       [DataMember]
        public int IdCartaFianzaColaboradorDetalle { set; get; }
        [DataMember]
        public int IdCartaFianza { set; get; }
        [DataMember]
        public int IdColaborador { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }

        [DataMember]
        public int IdTipoColaboradorFianzaTm { set; get; }

        [DataMember]
        public int IdSupervisorColaborador { set; get; }
        [DataMember]
        public string IdColaboradorStr { set; get; }
        [DataMember]
        public string IdClienteStr { set; get; }

    }
}
