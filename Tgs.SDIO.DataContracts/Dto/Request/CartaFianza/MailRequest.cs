﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.DataContracts.Dto.Request.CartaFianza
{
    [DataContract]
    public class MailRequest : MailDto
    {
        /********Carta Fianza********/
        [DataMember]
        public int IdCartaFianza { get; set; }
        [DataMember]
        public int IdColaborador { get; set; }
        [DataMember]
        public int IdTipoAccionTm { get; set; }
        [DataMember]
        public int IdRenovarTm { get; set; }
        [DataMember]
        public int IdEstadoCartaFianzaTm { get; set; }        
        [DataMember]
        public int IdTipoSubEstadoCartaFianzaTm { get; set; }
        /********Carta Fianza********/
        [DataMember]
        public string NombreColaborador { get; set; }
        [DataMember]
        public string EmailColaborador { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public string UsuarioEdicion { get; set; }

        
    }
}
