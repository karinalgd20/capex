﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.CartaFianza
{

    [DataContract]
    public class CartaFianzaDetalleRecuperoRequest
    {
        [DataMember]
        public int IdCartaFianzaDetalleRecupero { set;get;}
        [DataMember]
        public int IdCartaFianza {set;get;}
        [DataMember]
        public int IdColaborador {set;get;}
        [DataMember]
        public int IdColaboradorACargo {set;get;}
        [DataMember]
        public int IdEstadoRecuperacionCartaFianzaTm {set;get;}
        [DataMember]
        public string Comentario { set; get; }
        [DataMember]
        public DateTime? FechaRecupero { set; get; }
        [DataMember]
        public DateTime? FechaSolicitudRecupero { set; get; }
        [DataMember]
        public DateTime? FechaDevolucionTesoreria { set; get; }

        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public int? IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime? FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }
        [DataMember]
        public int IdCartaFianzaDetalleSeguimiento { set; get; }

        [DataMember]
        public int Tamanio { set; get; }
        [DataMember]
        public int Indice { set; get; }

        [DataMember]
        public string Cliente { set; get; }
        [DataMember]
        public string NumeroGarantia { set; get; }
        [DataMember]
        public string ImporteStr { set; get; }
        [DataMember]
        public int IdBanco { set; get; }
        [DataMember]
        public string EntidadBancaria { set; get; }
        [DataMember]
        public DateTime? FechaFinServicio { set; get; }
        [DataMember]
        public bool Eliminado { set; get; }
        [DataMember]
        public string ColumnName { set; get; }
        [DataMember]
        public string OrderType { set; get; }

        [DataMember]
        public string SustentoRenovacion { set; get; }
        [DataMember]
        public string Observacion { set; get; }
        [DataMember]
        public string ValidacionContratoGcSm { set; get; }
        [DataMember]
        public DateTime? FechaRespuestaFinalGcSm { set; get; }
    }
}
