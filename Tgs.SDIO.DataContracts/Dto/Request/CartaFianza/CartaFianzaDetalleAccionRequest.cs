﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.CartaFianza
{

    [DataContract]
    public class CartaFianzaDetalleAccionRequest
    {
        [DataMember]
        public int IdCartaFianzaDetalleAccion { set; get; }
        [DataMember]
        public int IdCartaFianza  { set; get; }
        [DataMember]
        public int IdColaboradorACargo  { set; get; }
        [DataMember]
        public  int IdTipoAccionTm  { set; get; }
        [DataMember]
        public int IdEstadoVencimientoTm  { set; get; }
        [DataMember]
        public string    Observacion { set; get; }
        [DataMember]
        public int IdEstado  { set; get; }

        [DataMember]
        public DateTime FechaRegistro { set; get; }

        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public Nullable<DateTime> FechaCreacion { set; get; }
        [DataMember]
        public int IdUsuarioEdicion { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public Nullable<DateTime> FechaEdicion { set; get; }
        [DataMember]
        public string UsuarioCreacion { set; get; }


        [DataMember]
        public int  IdEstadoCartaFianzaTm { set; get; }
        [DataMember]
        public int  IdRenovarTm { set; get; }
        [DataMember]
        public int IdTipoSubEstadoCartaFianzaTm { set; get; }

    }
}
