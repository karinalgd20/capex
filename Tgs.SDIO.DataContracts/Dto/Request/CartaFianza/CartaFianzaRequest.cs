﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.CartaFianza
{
 
    [DataContract]
    public class CartaFianzaRequest
    {


        [DataMember]
        public int IdCartaFianza { set; get; }

        [DataMember]
        public int IdCliente { set; get; }
        [DataMember]
        public string NombreCliente { set; get; }
        [DataMember]
        public string NumeroIdentificadorFiscal { set; get; }

        [DataMember]
        public string NumeroOportunidad { set; get; }
        [DataMember]
        public int IdTipoContratoTm { set; get; }
        [DataMember]
        public string NumeroContrato { set; get; }
        [DataMember]
        public int IdProcesoTm { set; get; }
        [DataMember]
        public string NumeroProceso { set; get; }
        [DataMember]
        public string Servicio { set; get; }
        [DataMember]
        public string DescripcionServicioCartaFianza { set; get; }
        [DataMember]
        public DateTime? FechaFirmaServicioContrato { set; get; }
        [DataMember]
        public DateTime? FechaFinServicioContrato { set; get; }
        [DataMember]
        public int IdEmpresaAdjudicadaTm { set; get; }
        [DataMember]
        public int IdTipoGarantiaTm { set; get; }
        [DataMember]
        public string NumeroGarantia { set; get; }


        [DataMember]
        public int? IdTipoAccionTm { set; get; }
        [DataMember]
        public int? IdRenovarTm { set; get; }
       

        [DataMember]
        public int? IdEstadoCartaFianzaTm { set; get; }
        [DataMember]
        public int? IdTipoSubEstadoCartaFianzaTm { set; get; }

        [DataMember]
        public int IdTipoMonedaTm { set; get; }
        [DataMember]
        public decimal ImporteCartaFianzaSoles { set; get; }
        [DataMember]
        public decimal ImporteCartaFianzaDolares { set; get; }
        [DataMember]
        public int IdBanco { set; get; }


        [DataMember]
        public DateTime? FechaEmisionBanco { set; get; }
        [DataMember]
        public DateTime? FechaVigenciaBanco { set; get; }
        [DataMember]
        public DateTime? FechaVencimientoBanco { set; get; }



        [DataMember]
        public int ClienteEspecial { set; get; }
        [DataMember]
        public int SeguimientoImportante { set; get; }
        [DataMember]
        public string Observacion { set; get; }
        [DataMember]
        public string Incidencia { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public int? IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime? FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }



        [DataMember]
        public int Indice { set; get; }
        [DataMember]
        public int Tamanio { set; get; }

        [DataMember]
        public string SortName { get; set; }
        [DataMember]
        public int Sort { get; set; }


        [DataMember]
        public DateTime? FechaCorrecionTesoreria { set; get; }
        [DataMember]
        public DateTime? FechaRecepcionTesoreria { set; get; }


        [DataMember]
        public string FechaCorrecionTesoreriaStr { set; get; }
        [DataMember]
        public string FechaRecepcionTesoreriaStr { set; get; }
        [DataMember]
        public string MotivoCartaFianzaErrada { set; get; }

        [DataMember]
        public string UsuarioEdicion { set; get; }

        [DataMember]
        public DateTime? FechaRenovacionEjecucion { set; get; }
        [DataMember]
        public DateTime? FechaDesestimarRequerimiento { set; get; }
        [DataMember]
        public DateTime? FechaEjecucion { set; get; }

        [DataMember]
        public int? IdTipoRequeridaTm { set; get; }
        [DataMember]
        public string MotivoCartaFianzaRequerida { set; get; }

        [DataMember]
        public int IdColaborador { set; get; }
        [DataMember]
        public Boolean Eliminado { set; get; }

    }
}
