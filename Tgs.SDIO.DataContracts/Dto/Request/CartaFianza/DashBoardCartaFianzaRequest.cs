﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.CartaFianza
{


   [DataContract]
   public class DashBoardCartaFianzaRequest
    {
        [DataMember]
        public int anio { set; get; }

        [DataMember]
        public int mes { set; get; }




    }
}
