﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.CartaFianza
{
    public class CartaFianzaDetalleSeguimientoRequest
    {
        [DataMember]
        public int IdCartaFianzaDetalleSeguimiento { set; get; }
        [DataMember]
        public int IdCartaFianza { set; get; }
        [DataMember]
        public int? IdColaborador { set; get; }
        [DataMember]
        public int? IdColaboradorACargo { set; get; }
        [DataMember]
        public int? IdEstadoRecuperacionCartaFianzaTm { set; get; }
        [DataMember]
        public string Comentario { set; get; }
        [DataMember]
        public int? IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime? FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioModificacion { set; get; }
        [DataMember]
        public DateTime? FechaModificacion { set; get; }
        [DataMember]
        public int? Vigencia { set; get; }
        [DataMember]
        public int? Eliminado { set; get; }

        //Paginado

        [DataMember]
        public int Indice { set; get; }
        [DataMember]
        public int Tamanio { set; get; }

    }
}
