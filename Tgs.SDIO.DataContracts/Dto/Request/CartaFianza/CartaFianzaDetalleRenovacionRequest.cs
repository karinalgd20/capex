﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.CartaFianza
{

    [DataContract]
    public class CartaFianzaDetalleRenovacionRequest
    {
        [DataMember]
        public int IdCartaFianzaDetalleRenovacion { set; get; }
        [DataMember]
        public int IdCartaFianza { set; get; }
        [DataMember]
        public int IdColaborador { set; get; }
        [DataMember]
        public int IdColaboradorACargo { set; get; }
        [DataMember]
        public int? IdRenovarTm { set; get; }
        [DataMember]
        public int PeriodoMesRenovacion { set; get; }
        [DataMember]
        public DateTime? FechaVencimientoRenovacion { set; get; }
        [DataMember]
        public string SustentoRenovacion { set; get; }
        [DataMember]
        public string Observacion { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public string Cliente { set; get; }
        [DataMember]
        public string NumeroIdentificadorFiscal { set; get; }

        [DataMember]
        public decimal Importe { set; get; }
        [DataMember]
        public string NombreCompletoColaborador { set; get; }


        /***********************************/

        [DataMember]

        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime? FechaCreacion { set; get; }
        [DataMember]
        public int? IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime? FechaEdicion { set; get; }

        /***********************************/
        [DataMember]
        public string CartaFianzaReemplazo { set; get; }
        [DataMember]
        public DateTime? FechaSolitudTesoria { set; get; }
        [DataMember]
        public string NumeroGarantiaReemplazo { set; get; }
        [DataMember]
        public string NumeroGarantia { set; get; }
        [DataMember]
        public int? IdTipoBancoReemplazo { set; get; }

        [DataMember]
        public string IdCartaFianzastr { set; get; }
        [DataMember]
        public int IdTipoAccionTm { set; get; }

        [DataMember]
        public string ValidacionContratoGcSm { set; get; }
        [DataMember]
        public DateTime? FechaRespuestaFinalGcSm { set; get; }
        [DataMember]
        public DateTime? FechaRecepcionCartaFianzaRenovacion { set; get; }
        [DataMember]
        public DateTime? FechaEntregaRenovacion { set; get; }

        [DataMember]
        public string UsuarioEdicion { set; get; }



        [DataMember]
        public int IdEstadoCartaFianzaTm { set; get; }
       
        [DataMember]
        public int IdTipoSubEstadoCartaFianzaTm { set; get; }

        //Paginado

        [DataMember]
        public int Indice { set; get; }
        [DataMember]
        public int Tamanio { set; get; }

        [DataMember]
        public string SortName { get; set; }
        [DataMember]
        public int Sort { get; set; }

        [DataMember]
        public DateTime? FechaEmisionBancoRnv { get; set; }
        [DataMember]
        public DateTime? FechaVigenciaBancoRnv { get; set; }
        [DataMember]
        public DateTime? FechaVencimientoBancoRnv { get; set; }
    }
}
