﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class ProyectoLineaProductoDtoRequest
    {
        [DataMember]
        public int IdProyecto { get; set; }

        [DataMember]
        public int IdLineaProducto { get; set; }

        [DataMember]
        public int IdEstado { get; set; }

        [DataMember]
        public Nullable<System.DateTime> FechaEdicion { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }

        [DataMember]
        public System.DateTime FechaCreacion { get; set; }

        [DataMember]
        public Nullable<int> IdUsuarioEdicion { get; set; }

    }
}
