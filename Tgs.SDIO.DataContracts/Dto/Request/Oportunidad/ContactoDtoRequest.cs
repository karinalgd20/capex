﻿using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;


namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class ContactoDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdSede { get; set; }

        [DataMember]
        public string NombreApellidos { get; set; }

        [DataMember]
        public string NumeroTelefono { get; set; }

        [DataMember]
        public string NumeroCelular { get; set; }

        [DataMember]
        public string CorreoElectronico { get; set; }

        [DataMember]
        public string Cargo { get; set; }
    }
}
