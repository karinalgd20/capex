﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class OportunidadFlujoCajaDtoRequest
    {
        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }

        [DataMember]
        public int IdOportunidad { get; set; }

        [DataMember]
        public int IdLineaNegocio { get; set; }

        [DataMember]
        public int? IdServicioCMI { get; set; }

        [DataMember]
        public int? IdSubServicio { get; set; }

        [DataMember]
        public int IdServicioCMISubServicio { get; set; }

        [DataMember]
        public int? IdAgrupador { get; set; }

        [DataMember]
        public int? IdTipoCosto { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public int? IdProveedor { get; set; }

        [DataMember]
        public int? IdPeriodos { get; set; }

        [DataMember]
        public int? IdPestana { get; set; }

        [DataMember]
        public int? IdGrupo { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }

        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public int IdUsuarioEdicion { get; set; }

        [DataMember]
        public DateTime FechaEdicion { get; set; }

        [DataMember]
        public int? IdCasoNegocio { get; set; }

        [DataMember]
        public int? IdServicio { get; set; }

        [DataMember]
        public int? Cantidad { get; set; }

        [DataMember]
        public decimal? CostoUnitario { get; set; }

        [DataMember]
        public string ContratoMarco { get; set; }

        [DataMember]
        public int? IdMoneda { get; set; }

        [DataMember]
        public int? FlagSISEGO { get; set; }

        [DataMember]
        public int IdFlujoCaja { get; set; }

        [DataMember]
        public int IdSubServicioDatosCapex { get; set; }

        [DataMember]
        public int? IdOportunidadCosto { get; set; }
        
        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int Tamanio { get; set; }
        [DataMember]
        public int IdSubServicioDatosCaratula { get; set; }
        [DataMember]
        public int? IdTipoSubServicio { get; set; }

        [DataMember]
        public List<int> ListIdFlujoCaja { get; set; }
        [DataMember]
        public int? AgrupadorServicio { get; set; }

        [DataMember]
        public int? Inicio { get; set; }
        [DataMember]
        public decimal? Ponderacion { get; set; }



    }
}
