﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class SubServicioDatosCaratulaDtoRequest
    {        
        [DataMember]
        public int IdOportunidad { get; set; }
        [DataMember]
        public int IdLineaProducto { get; set; }
        [DataMember]
        public int IdServicioCMI { get; set; } 
        [DataMember]
        public decimal Porcentaje { get; set; }
        [DataMember]
        public int IdAnalista { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }
        [DataMember]
        public int? IdPestana { get; set; }
        [DataMember]
        public int IdSubServicio { get; set; }
        [DataMember]
        public int IdServicioSubServicio { get; set; }
        [DataMember]
        public int? IdAgrupador { get; set; }
        [DataMember]
        public int? IdTipoCosto { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public decimal Ponderacion { get; set; }
        [DataMember]
        public decimal CostoPreOperativo { get; set; }
        [DataMember]
        public int? IdProveedor { get; set; }
        [DataMember]
        public int? IdPeriodos { get; set; }
        [DataMember]
        public int? Inicio { get; set; }
        [DataMember]
        public int? Meses { get; set; }
        [DataMember]
        public int IdSubServicioDatosCaratula { get; set; }
        [DataMember]
        public string Circuito { get; set; }
        [DataMember]
        public int? IdMedio { get; set; }
        [DataMember]
        public int? IdTipoEnlace { get; set; }
        [DataMember]
        public int? IdActivoPasivo { get; set; }
        [DataMember]
        public int? IdLocalidad { get; set; }
        [DataMember]
        public int Cantidad { get; set; }
        [DataMember]
        public int IdBW { get; set; }
        [DataMember]
        public int? NumeroMeses { get; set; }
        [DataMember]
        public decimal? MontoUnitarioMensual { get; set; }
        [DataMember]
        public decimal? MontoTotalMensual { get; set; }
        [DataMember]
        public int? NumeroMesInicioGasto { get; set; }
        [DataMember]
        public int? IdModelo { get; set; }
        [DataMember]
        public string FlagRenovacion { get; set; }
        [DataMember]
        public decimal? Instalacion { get; set; }
        [DataMember]
        public decimal? Desinstalacion { get; set; }
        [DataMember]
        public decimal? PU { get; set; }
        [DataMember]
        public decimal? Alquiler { get; set; }
        [DataMember]
        public decimal? Factor { get; set; }
        [DataMember]
        public decimal? ValorCuota { get; set; }
        [DataMember]
        public decimal? CC { get; set; }
        [DataMember]
        public decimal? CCQProvincia { get; set; }
        [DataMember]
        public decimal? CCQBK { get; set; }
        [DataMember]
        public decimal? CCQCAPEX { get; set; }
        [DataMember]
        public decimal? TIWS { get; set; }
        [DataMember]
        public decimal? RADIO { get; set; }

        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }
   
        [DataMember]
        public int IdGrupo { get; set; }

        [DataMember]
        public int IdFlujoCaja { get; set; }



    }
}
