﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class SubServicioDatosCapexDtoRequest
    {
        [DataMember]
        public int IdSubServicioDatosCapex { get; set; }

        [DataMember]
        public int IdFlujoCaja { get; set; }

        [DataMember]
        public string Circuito { get; set; }

        [DataMember]
        public string SISEGO { get; set; }

        [DataMember]
        public int MesesAntiguedad { get; set; }

        [DataMember]
        public decimal CostoUnitarioAntiguo { get; set; }

        [DataMember]
        public decimal ValorResidualSoles { get; set; }

        [DataMember]
        public decimal CostoUnitario { get; set; }

        [DataMember]
        public decimal CapexDolares { get; set; }

        [DataMember]
        public decimal CapexSoles { get; set; }

        [DataMember]
        public decimal TotalCapex { get; set; }

        [DataMember]
        public int AnioRecupero { get; set; }

        [DataMember]
        public int MesRecupero { get; set; }

        [DataMember]
        public int AnioComprometido { get; set; }

        [DataMember]
        public int MesComprometido { get; set; }

        [DataMember]
        public int AnioCertificado { get; set; }

        [DataMember]
        public int MesCertificado { get; set; }

        [DataMember]
        public int? IdMedio { get; set; }

        [DataMember]
        public int IdTipo { get; set; }
        
        [DataMember]
        public string Garantizado { get; set; }

        [DataMember]
        public string Cruce { get; set; }

        [DataMember]
        public string AEReducido { get; set; }

        [DataMember]
        public string Combo { get; set; }

        [DataMember]
        public int Antiguedad { get; set; }

        [DataMember]
        public decimal CapexInstalacion { get; set; }

        [DataMember]
        public decimal CapexReal { get; set; }

        [DataMember]
        public decimal CapexTotalReal { get; set; }

        [DataMember]
        public string Marca { get; set; }

        [DataMember]
        public string Modelo { get; set; }

        [DataMember]
        public string Tipo { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        [DataMember]
        public decimal Cu { get; set; }
        [DataMember]
        public int Cantidad { get; set; }
        [DataMember]
        public decimal CuAntiguiedad { get; set; }
        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int IdGrupo { get; set; }
        [DataMember]
        public int IdOportunidad { get; set; }
        [DataMember]
        public decimal Instalacion { get; set; }
        [DataMember]
        public decimal? Inversion { get; set; }
        [DataMember]
        public decimal PorcentajeGarantizado { get; set; }
        [DataMember]
        public decimal CostoSegmentoSatelital { get; set; }
        [DataMember]
        public int IdTipoSatelital { get; set; }
        [DataMember]
        public int IdGarantizado { get; set; }
        [DataMember]
        public int IdTipoSubServicio { get; set; }

        
    }
}
