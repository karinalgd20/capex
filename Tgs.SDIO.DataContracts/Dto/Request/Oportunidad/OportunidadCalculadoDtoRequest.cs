﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class OportunidadCalculadoDtoRequest
    {
        [DataMember]
        public int IdOportunidadCalculado { get; set; }

        [DataMember]
        public int IdConceptoFinanciero { get; set; }

        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }

        [DataMember]
        public int Anio { get; set; }

        [DataMember]
        public int Mes { get; set; }

        [DataMember]
        public decimal Monto { get; set; }

        [DataMember]
        public string TipoFicha { get; set; }
        
        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }

        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public int? IdUsuarioEdicion { get; set; }

        [DataMember]
        public DateTime? FechaEdicion { get; set; }
    }
}
