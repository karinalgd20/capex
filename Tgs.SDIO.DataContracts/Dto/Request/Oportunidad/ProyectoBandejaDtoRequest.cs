﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class ProyectoBandejaDtoRequest
    {



        [DataMember(EmitDefaultValue = false)]
        public string Proyecto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdLineaProducto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string NumeroSalesForce { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdGerente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdDireccion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }     
        
    }
}
