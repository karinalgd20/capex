﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class ProyectoDtoRequest
    {
        [DataMember]
        public int IdProyecto { get; set; }
        [DataMember]
        public int IdLineaProducto { get; set; }
        [DataMember]
        public int IdGerente { get; set; }
        [DataMember]
        public string Proyecto { get; set; }
        [DataMember]
        public int IdDireccion { get; set; }
        [DataMember]
        public int IdTipoEmpresa { get; set; }
        [DataMember]
        public int IdCliente { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string NumeroSalesForce { get; set; }
        [DataMember]
        public string NumeroCaso { get; set; }
        [DataMember]
        public Nullable<System.DateTime> Fecha { get; set; }
        [DataMember]
        public string Alcance { get; set; }
        [DataMember]
        public int Periodo { get; set; }
        [DataMember]
        public int TiempoImplantacion { get; set; }
        [DataMember]
        public int IdTipoProyecto { get; set; }
        [DataMember]
        public int IdTipoServicio { get; set; }
        [DataMember]
        public string IdProyectoAnterior { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdAnalistaFinanciero { get; set; }
        [DataMember]
        public int IdProductManager { get; set; }
        [DataMember]
        public int IdPreVenta { get; set; }
        [DataMember]
        public int TiempoProyecto { get; set; }
        [DataMember]
        public int IdTipoCambio { get; set; }
        [DataMember]
        public int Agrupador { get; set; }
        [DataMember]
        public int Version { get; set; }
        [DataMember]
        public int? VersionPadre { get; set; }
        [DataMember]
        public int FlagGanador { get; set; }
        [DataMember]
        public int IdMonedaFacturacion { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public System.DateTime FechaCreacion { get; set; }
        [DataMember]
        public int IdUsuarioEdicion { get; set; }
        [DataMember]
        public System.DateTime FechaEdicion { get; set; }

        [DataMember]
        public int IdTipoConcepto { get; set; }

        [DataMember]
        public int IdServicioCMI { get; set; }

        [DataMember]
        public int IdConcepto { get; set; }

        [DataMember]
        public int IdServicioConcepto { get; set; }

        [DataMember]
        public int IdAgrupador { get; set; }

        [DataMember]
        public int IdTipoCosto { get; set; }

        [DataMember]
        public decimal Ponderacion { get; set; }
        [DataMember]
        public decimal CostoPreOperativo { get; set; }

        [DataMember]
        public int IdProveedor { get; set; }

        [DataMember]
        public int IdPeriodos { get; set; }

        [DataMember]
        public int Inicio { get; set; }

    }
}
