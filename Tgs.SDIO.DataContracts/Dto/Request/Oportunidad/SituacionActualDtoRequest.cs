﻿using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class SituacionActualDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdTipo { get; set; }

        [DataMember]
        public int IdOportunidad { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
    }
}
