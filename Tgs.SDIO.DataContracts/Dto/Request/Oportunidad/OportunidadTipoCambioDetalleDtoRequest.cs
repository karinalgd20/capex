﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class OportunidadTipoCambioDetalleDtoRequest : AuditoriaDto
    {

        [DataMember]
        public int IdTipoCambioDetalle { get; set; }
        [DataMember]
        public int IdTipoCambioOportunidad { get; set; }
        [DataMember]
        public int? IdMoneda { get; set; }
        [DataMember]
        public int IdTipificacion { get; set; }
        [DataMember]
        public int? Anio { get; set; }
        [DataMember]
        public decimal? Monto { get; set; }         

    }
}
