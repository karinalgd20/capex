﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class OportunidadTipoCambioDtoRequest : AuditoriaDto
    {

        [DataMember]
        public int IdTipoCambioOportunidad { get; set; }
        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdMoneda { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdTipificacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Anio { get; set; }

    }
}
