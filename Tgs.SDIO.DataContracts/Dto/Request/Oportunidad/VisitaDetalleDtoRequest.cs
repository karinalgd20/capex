﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class VisitaDetalleDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdVisita { get; set; }

        [DataMember]
        public DateTime FechaAcuerdo { get; set; }

        [DataMember]
        public string DescripcionPunto { get; set; }

        [DataMember]
        public string Responsable { get; set; }

        [DataMember]
        public bool Cumplimiento { get; set; }
    }
}
