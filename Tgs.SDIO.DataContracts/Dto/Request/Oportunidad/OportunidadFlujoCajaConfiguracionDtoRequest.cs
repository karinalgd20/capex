﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class OportunidadFlujoCajaConfiguracionDtoRequest
    {
        [DataMember]
        public int IdOportunidad { get; set; }

        [DataMember]
        public int IdFlujoCaja { get; set; }

        [DataMember]
        public int IdFlujoCajaConfiguracion { get; set; }

        [DataMember]
        public int IdLineaProducto { get; set; }

        [DataMember]
        public int IdServicioCMI { get; set; }

        [DataMember]
        public int IdSubServicio { get; set; }

        [DataMember]
        public int IdServicioCMISubServicio { get; set; }

        [DataMember]
        public int IdPeriodos { get; set; }

        [DataMember]
        public int? Inicio { get; set; }

        [DataMember]
        public int? Meses { get; set; }

        [DataMember]
        public decimal? CostoPreOperativo { get; set; }

        [DataMember]
        public decimal? Ponderacion { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public int IdTipoCosto { get; set; }

        [DataMember]
        public int IdAgrupador { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }

        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public int IdUsuarioEdicion { get; set; }

        [DataMember]
        public DateTime FechaEdicion { get; set; }
    }
}
