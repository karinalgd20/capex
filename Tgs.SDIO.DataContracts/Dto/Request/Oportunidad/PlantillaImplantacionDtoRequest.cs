﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    public class PlantillaImplantacionDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdOportunidad { get; set; }

        [DataMember]
        public int IdTipoEnlaceCircuitoDatos { get; set; }

        [DataMember]
        public int IdMedioCircuitoDatos { get; set; }

        [DataMember]
        public int IdServicioGrupoCircuitoDatos { get; set; }

        [DataMember]
        public int IdTipoCircuitoDatos { get; set; }

        [DataMember]
        public string NumeroCircuitoDatos { get; set; }

        [DataMember]
        public int IdCostoCircuitoDatos { get; set; }

        [DataMember]
        public string ConectadoCircuitoDatos { get; set; }

        [DataMember]
        public string LargaDistanciaNacionalCircuitoDatos { get; set; }

        [DataMember]
        public int IdVozTos5CaudalAntiguo { get; set; }

        [DataMember]
        public int IdVozTos4CaudalAntiguo { get; set; }

        [DataMember]
        public int IdVozTos3CaudalAntiguo { get; set; }

        [DataMember]
        public int IdVozTos2CaudalAntiguo { get; set; }

        [DataMember]
        public int IdVozTos1CaudalAntiguo { get; set; }

        [DataMember]
        public int IdVozTos0CaudalAntiguo { get; set; }

        [DataMember]
        public string UltimaMillaCircuitoDatos { get; set; }

        [DataMember]
        public string VrfCircuitoDatos { get; set; }

        [DataMember]
        public string EquipoCpeCircuitoDatos { get; set; }

        [DataMember]
        public string EquipoTerminalCircuitoDatos { get; set; }

        [DataMember]
        public int IdAccionIsis { get; set; }

        [DataMember]
        public int IdTipoEnlaceServicioOfertado { get; set; }

        [DataMember]
        public int IdMedioServicioOfertado { get; set; }

        [DataMember]
        public int IdServicioGrupoServicioOfertado { get; set; }

        [DataMember]
        public int IdCostoServicioOfertado { get; set; }

        [DataMember]
        public int IdTipoServicioOfertado { get; set; }

        [DataMember]
        public string NumeroServicioOfertado { get; set; }

        [DataMember]
        public string LargaDistanciaNacionalServicioOfertado { get; set; }

        [DataMember]
        public int IdVozTos5ServicioOfertado { get; set; }

        [DataMember]
        public int IdVozTos4ServicioOfertado { get; set; }

        [DataMember]
        public int IdVozTos3ServicioOfertado { get; set; }

        [DataMember]
        public int IdVozTos2ServicioOfertado { get; set; }

        [DataMember]
        public int IdVozTos1ServicioOfertado { get; set; }

        [DataMember]
        public int IdVozTos0ServicioOfertado { get; set; }

        [DataMember]
        public string UltimaMillaServicioOfertado { get; set; }

        [DataMember]
        public string VrfServicioOfertado { get; set; }

        [DataMember]
        public string EquipoCpeServicioOfertado { get; set; }

        [DataMember]
        public string ObservacionServicioOfertado { get; set; }

        [DataMember]
        public string EquipoTerminalServicioOfertado { get; set; }

        [DataMember]
        public string GarantizadoBw { get; set; }

        [DataMember]
        public string Propiedad { get; set; }

        [DataMember]
        public string RecursoTransporte { get; set; }

        [DataMember]
        public string RouterSwitchCentralTelefonica { get; set; }

        [DataMember]
        public string ComponentesRouter { get; set; }

        [DataMember]
        public string TipoAntena { get; set; }

        [DataMember]
        public string SegmentoSatelital { get; set; }

        [DataMember]
        public string CoordenadasUbicacion { get; set; }

        [DataMember]
        public string PozoTierra { get; set; }

        [DataMember]
        public string Ups { get; set; }

        [DataMember]
        public  string EquipoTelefonico { get; set; }
    }
}