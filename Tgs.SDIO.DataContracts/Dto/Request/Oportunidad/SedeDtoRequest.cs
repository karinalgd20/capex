﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;


namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class SedeDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdSedeInstalacion { get; set; }
        [DataMember]
        public int IdSede { get; set; }

        [DataMember]
        public string Codigo { get; set; }

        [DataMember]
        public int IdCliente { get; set; }

        [DataMember]
        public int IdTipoEnlace { get; set; }

        [DataMember]
        public int IdTipoSede { get; set; }

        [DataMember]
        public int IdAccesoCliente { get; set; }

        [DataMember]
        public int IdTendidoExterno { get; set; }

        [DataMember]
        public int? NumeroPisos { get; set; }
        [DataMember]

        public int IdUbigeo { get; set; }
        [DataMember]

        public int? IdTipoVia { get; set; }
        [DataMember]

        public string Direccion { get; set; }
        [DataMember]

        public string Latitud { get; set; }
        [DataMember]

        public string Longitud { get; set; }
        [DataMember]

        public int? IdTipoServicio { get; set; }
        [DataMember]

        public int? Piso { get; set; }
        [DataMember]

        public string Interior { get; set; }
        [DataMember]

        public string Manzana { get; set; }
        [DataMember]

        public string Lote { get; set; }
        [DataMember]
        public string CodigoDepartamento { get; set; }
        [DataMember]
        public string CodigoProvincia { get; set; }
        [DataMember]
        public string CodigoDistrito { get; set; }
        [DataMember]
        public int IdOportunidad { get; set; }
        [DataMember]
        public List<ComboDtoResponse> ListSedeSelect { get; set; }
        [DataMember]
        public string DesRequerimiento { get; set; }
        [DataMember]
        public bool? FlgCotizacionReferencial { get; set; }
        [DataMember]
        public bool? FlgRequisitosEspeciales { get; set; }


    }
}
