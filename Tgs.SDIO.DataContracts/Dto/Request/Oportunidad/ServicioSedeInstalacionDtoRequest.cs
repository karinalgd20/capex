﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    public class ServicioSedeInstalacionDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdServicioSedeInstalacion { get; set; }

        [DataMember]
        public int IdSedeInstalacion { get; set; }

        [DataMember]
        public int IdFlujoCaja { get; set; }

        [DataMember]
        public List<ComboDtoResponse> ListaServicios { get; set; }
    }
}