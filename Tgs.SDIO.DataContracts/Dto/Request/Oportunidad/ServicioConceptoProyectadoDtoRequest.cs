﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class ServicioConceptoProyectadoDtoRequest
    {

        [DataMember]
        public int IdOportunidad { get; set; }
        [DataMember]
        public int IdLineaNegocio { get; set; }
        [DataMember]
        public int IdServicioCMI { get; set; }
        [DataMember]
        public int IdConcepto { get; set; }
        [DataMember]
        public int IdServicioConcepto { get; set; }
        [DataMember]
        public int IdProyectado { get; set; }
        [DataMember]
        public int Anio { get; set; }
        [DataMember]
        public int Mes { get; set; }
        [DataMember]
        public decimal Monto { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdPestana { get; set; }
        [DataMember]
        public int IdGrupo { get; set; }
        [DataMember]
        public string FlagIngresoManual { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaCreacion { get; set; }
        [DataMember]
        public int IdUsuarioEdicion { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FechaEdicion { get; set; }
        [DataMember]
        public string TipoFicha { get; set; }



    }
}
