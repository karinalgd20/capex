﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class OportunidadFlujoCajaDetalleDtoRequest
    {
        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }
        [DataMember]
        public int MesInicio { get; set; }
        [DataMember]
        public int Numeromeses { get; set; }
        [DataMember]
        public int TipoFicha { get; set; }
        [DataMember]
        public int IdFlujoCajaConfiguracion { get; set; }
        [DataMember]
        public int IdFlujoCajaDetalle { get; set; }
        [DataMember]
        public int Anio { get; set; }
        [DataMember]
        public int Mes { get; set; }
        [DataMember]
        public decimal Monto { get; set; }
        
        [DataMember]
        public DateTime FechaInicio { get; set; }
        [DataMember]
        public DateTime FechaFin { get; set; }
        [DataMember]
        public int Intervalo { get; set; }
        
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public System.DateTime FechaCreacion { get; set; }
        [DataMember]
        public int IdUsuarioEdicion { get; set; }
        [DataMember]
        public System.DateTime FechaEdicion { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }
    }
}
