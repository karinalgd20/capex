﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;



namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    public class CotizacionDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdSedeInstalacion { get; set; }
        [DataMember]
        public string CodSisegoCotizacion { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public decimal TotalSoles { get; set; }
        [DataMember]
        public decimal TotalDolares { get; set; }
        [DataMember]
        public int Dias { get; set; }
        [DataMember]
        public DateTime FechaEnvioPresupuesto { get; set; }
        [DataMember]
        public int EstadoCotizacion { get; set; }
    }
}
