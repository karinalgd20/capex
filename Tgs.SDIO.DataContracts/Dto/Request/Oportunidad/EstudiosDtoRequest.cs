﻿using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    public class EstudiosDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdSedeInstalacion { get; set; }
        [DataMember]
        public string Estudio { get; set; }
        [DataMember]
        public string Departamento { get; set; }
        [DataMember]
        public string TipoRequerimiento { get; set; }
        [DataMember]
        public string Nodo { get; set; }
        [DataMember]
        public int Dias { get; set; }
        [DataMember]
        public decimal TotalSoles { get; set; }
        [DataMember]
        public decimal TotalDolares { get; set; }
        [DataMember]
        public string Responsable { get; set; }
        [DataMember]
        public string FacilidadesTecnicas { get; set; }
        [DataMember]
        public string EstadoSisego { get; set; }
    }
}
