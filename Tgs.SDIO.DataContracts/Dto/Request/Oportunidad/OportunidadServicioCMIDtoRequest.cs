﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class OportunidadServicioCMIDtoRequest
    {
        [DataMember]
        public int IdOportunidadServicioCMI { get; set; }
        //Campos BD
        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }
        [DataMember]
        public int IdServicioCMI { get; set; }
        [DataMember]
        public decimal? Porcentaje { get; set; }
        [DataMember]
        public int? IdAnalista { get; set; }

        //Adicionales
        [DataMember]
        public int Indice { get; set; }
        [DataMember]
        public int Tamanio { get; set; }

        //Auditoria
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime FechaEdicion { get; set; }        
    }
}
