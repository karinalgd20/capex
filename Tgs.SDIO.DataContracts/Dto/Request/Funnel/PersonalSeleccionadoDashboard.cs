﻿namespace Tgs.SDIO.DataContracts.Dto.Request.Funnel
{
    public class PersonalSeleccionadoDashboard
    {
        public string Level { get; set; }
        public int idSector { get; set; }
        public int idLider { get; set; }
        public int idLineaNegocio { get; set; }
        public int idPreventa { get; set; }
        public int idCliente { get; set; }
        public string IdOportunidad { get; set; }
        public int idAnalista { get; set; }
        public int NumeroCaso { get; set; }
        public long FechaHora { get; set; }

    }
}
