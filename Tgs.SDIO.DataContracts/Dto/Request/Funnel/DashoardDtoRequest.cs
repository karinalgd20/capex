﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Funnel
{
    [DataContract]
    public class DashoardDtoRequest
    {
        [DataMember]
        public int Anio { get; set; }

        [DataMember]
        public int? Mes { get; set; }

        [DataMember]
        public string Etapa { get; set; }

        [DataMember]
        public int? ProbabilidadExito { get; set; }

        [DataMember]
        public string Madurez { get; set; }

        [DataMember]
        public int? IdLineaNegocio { get; set; }

        [DataMember]
        public int? IdSector { get; set; }
    }
}
