﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Funnel
{
    [DataContract]
    public class IndicadorDashboardSectorDtoRequest
    {
        [DataMember]
        public int Anio { get; set; }

        [DataMember]
        public int IdGerente { get; set; }

        [DataMember]
        public int IdJefe { get; set; }

        [DataMember]
        public int? IdSector { get; set; }

        [DataMember]
        public PaginacionDto Paginacion { get; set; }

        [DataMember]
        public int Nivel { get; set; }

        [DataMember]
        public string ListaLiderIncluir { get; set; }

        [DataMember]
        public string ListaLiderExcluir { get; set; }

        [DataMember]
        public string ListaPreventaIncluir { get; set; }

        [DataMember]
        public string ListaPreventaExcluir { get; set; }

        [DataMember]
        public string ListaClientesIncluir { get; set; }

        [DataMember]
        public string ListaClientesExcluir { get; set; }

        [DataMember]
        public string ListaSectorIncluir { get; set; }

        [DataMember]
        public string ListaSectorExcluir { get; set; }

    }
}

