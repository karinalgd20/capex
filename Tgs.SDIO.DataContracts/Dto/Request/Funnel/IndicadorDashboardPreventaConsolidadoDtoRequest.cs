﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Funnel
{
    [DataContract]
    public class IndicadorDashboardPreventaConsolidadoDtoRequest
    {
        [DataMember]
        public IndicadorDashboardPreventaDtoRequest oportunidadesFiltro { get; set; }
    }
}

