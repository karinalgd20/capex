﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.Funnel
{
    [DataContract]
    public class IndicadorRentabilidadDtoRequest : PaginacionDto
    {
        [DataMember]
        public int? Anio { get; set; }

        [DataMember]
        public int? IdGerente { get; set; }

        [DataMember]
        public int? IdAnalistaFinanciero { get; set; }

        [DataMember]
        public string IdOportunidad { get; set; }

        [DataMember]
        public int Nivel { get; set; }

        [DataMember]
        public string ListaAnalistaFinancierosIncluir { get; set; }

        [DataMember]
        public string ListaAnalistaFinancierosExcluir { get; set; }

        [DataMember]
        public string ListaClientesIncluir { get; set; }

        [DataMember]
        public string ListaClientesExcluir { get; set; }

        [DataMember]
        public string ListaOportunidadesIncluir { get; set; }

        [DataMember]
        public string ListaOportunidadesExcluir { get; set; }

        [DataMember]
        public string ListaCasosIncluir { get; set; }

        [DataMember]
        public string ListaCasosExcluir { get; set; }

    }
}

