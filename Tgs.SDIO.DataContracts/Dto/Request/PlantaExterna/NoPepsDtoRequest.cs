﻿using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna
{
    [DataContract]
    public class NoPepsDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { set; get; }

        [DataMember]
        public string CodigoPep { set; get; } 
    }
}