﻿using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna
{
    [DataContract]
    public class TipoCambioSisegoDtoRequest : AuditoriaDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DateTime? FechaFin { set; get; }

        [DataMember]
        public DateTime? FechaInicio { set; get; }

        [DataMember]
        public decimal Monto { set; get; }
    }
}
