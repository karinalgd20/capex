﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Base
{
    [DataContract]
    public class AuditoriaDto
    {
        [DataMember]
        public Nullable<int> IdEstado { get; set; }

        [DataMember]
        public int IdUsuarioCreacion { get; set; }
         
        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public int? IdUsuarioEdicion { get; set; }

        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }
    }
}