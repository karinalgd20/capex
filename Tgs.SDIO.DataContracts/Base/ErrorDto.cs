﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Base
{
    [DataContract]
    public class ErrorDto
    {
        public ErrorDto()
        {
            Fecha = DateTime.Now;
        }

        [DataMember]
        public Guid IdError { get; set; }

        [DataMember]
        public TipoErrorServicioEnum.TipoErrorServicio Tipo { get; set; }

        [DataMember]
        public DateTime Fecha { get; set; }

        [DataMember]
        public string Mensaje { get; set; }

        [DataMember]
        public string MensajeCorto { get; set; }
    }
}
