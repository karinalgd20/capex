﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Funnel
{
    public partial interface IServicioFunnelSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        MemoryStream ListarIndicadorOfertasAnio(IndicadorMadurezDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        DashoardDtoResponse MostrarDashboard(DashoardDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        IndicadorDashboardPreventaConsolidadoDtoResponse ListarSeguimientoOportunidadesPreventaGerente(IndicadorDashboardPreventaConsolidadoDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        IndicadorDashboardPreventaConsolidadoDtoResponse ListarSeguimientoOportunidadesPreventaLider(IndicadorDashboardPreventaConsolidadoDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        IndicadorDashboardPreventaConsolidadoDtoResponse ListarSeguimientoOportunidadesPreventaPreventa(IndicadorDashboardPreventaConsolidadoDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        IndicadorDashboardPreventaConsolidadoDtoResponse ListarSeguimientoOportunidadesSectorOportunidad(IndicadorDashboardSectorDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        IndicadorRentabilidadConsolidadoDtoResponse ListarRentabilidadAnalistasFinancieros(IndicadorRentabilidadDtoRequest filtro);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        IndicadorLineasNegocioDtoResponse ListarLineasNegocio(IndicadorLineasNegocioDtoRequest filtro);
    }
}
