﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{
    public partial interface IServicioComunSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarCargos(CargoDtoRequest cargoRequest);
        [OperationContract]
        List<ListaDtoResponse> ListarComboCargo();
    }
}
