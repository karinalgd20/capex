﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;


namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{
    public partial interface IServicioComunSDio
    {
        [OperationContract]
        RecursoPaginadoDtoResponse ListadoRecursosPaginado(RecursoDtoRequest request);
        [OperationContract]
        RecursoDtoResponse ObtenerRecursoPorId(RecursoDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarRecurso(RecursoDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarRecurso(RecursoDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarRecurso(RecursoDtoRequest request);
        [OperationContract]
        RolRecursoPaginadoDtoResponse ListadoRolRecursosPaginado(RolRecursoDtoRequest request);
        [OperationContract]
        RolRecursoDtoResponse ObtenerRolRecursoPorId(RolRecursoDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarRolRecurso(RolRecursoDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarRolRecurso(RolRecursoDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarRolRecurso(RolRecursoDtoRequest request);
        [OperationContract]
        RecursoPaginadoDtoResponse ListaRecursoModalRecursoPaginado(RecursoDtoRequest request);
        [OperationContract]
        List<ListaDtoResponse> ListarComboRolRecurso();
        [OperationContract]
        List<RecursoDtoResponse> ListarRecursoPorCargo(RecursoDtoRequest request);
        [OperationContract]
        RecursoPaginadoDtoResponse ListadoRecursosByCargoPaginado(RecursoDtoRequest request);
    }
}