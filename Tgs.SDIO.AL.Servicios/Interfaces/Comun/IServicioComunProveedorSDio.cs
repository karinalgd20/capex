﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarProveedor(ProveedorDtoRequest proveedor);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        ProcesoResponse ActualizarProveedor(ProveedorDtoRequest proveedor);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InactivarProveedor(ProveedorDtoRequest proveedor);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<ListaDtoResponse> ListarProveedor(ProveedorDtoRequest proveedor);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProveedorDtoResponse ObtenerProveedor(ProveedorDtoRequest proveedor);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ProveedorDtoResponse> ListarProveedores(ProveedorDtoRequest proveedor);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProveedorPaginadoDtoResponse ListarProveedorPaginado(ProveedorDtoRequest proveedor);



    }
}
