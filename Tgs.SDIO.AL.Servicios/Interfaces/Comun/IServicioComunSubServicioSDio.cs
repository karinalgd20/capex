﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarSubServicios(SubServicioDtoRequest SubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InhabilitarSubServicio(SubServicioDtoRequest subServicio);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        SubServicioDtoResponse ObtenerSubServicio(SubServicioDtoRequest SubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarSubServicio(SubServicioDtoRequest SubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarSubServicio(SubServicioDtoRequest SubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SubServicioPaginadoDtoResponse ListaSubServicioPaginado(SubServicioDtoRequest SubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarSubServiciosPorIdGrupo(SubServicioDtoRequest subServicio);
    }
}
