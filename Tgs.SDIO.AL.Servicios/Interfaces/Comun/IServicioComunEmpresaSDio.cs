﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{
   public partial interface IServicioComunSDio
    {
        [OperationContract]
        List<ListaDtoResponse> ListarComboEmpresa();
    }
}
