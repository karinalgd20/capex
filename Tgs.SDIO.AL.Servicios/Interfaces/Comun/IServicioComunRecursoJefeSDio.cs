﻿using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{
    public partial interface IServicioComunSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        RecursoJefePaginadoDtoResponse ListarRecursoPorJefePaginado(RecursoJefeDtoRequest recursoJefeDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarEstadoRecursoJefe(RecursoJefeDtoRequest recursoJefeDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarRecursoJefe(RecursoJefeDtoRequest recursoJefeDtoRequest);
    }
}
