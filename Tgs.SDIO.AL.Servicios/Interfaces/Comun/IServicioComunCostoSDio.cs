﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;


namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CostoDtoResponse> ListarComboCosto(CostoDtoRequest medioCosto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CostoDtoResponse> ListarCostoPorMedioServicioGrupo(CostoDtoRequest costo);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CostoDtoResponse ObtenerCostoPorCodigo(CostoDtoRequest medioCosto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
         List<ListaDtoResponse> ListarCosto(CostoDtoRequest medioCosto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CostoDtoResponse ObtenerCosto(CostoDtoRequest medioCosto);

    }
}
