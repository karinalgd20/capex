﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CasoNegocioPaginadoDtoResponse ListarCasoNegocio(CasoNegocioDtoRequest casonegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CasoNegocioDtoResponse ObtenerCasoNegocio(CasoNegocioDtoRequest casonegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarCasoNegocio(CasoNegocioDtoRequest casonegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarCasoNegocio(CasoNegocioDtoRequest casonegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarCasoNegocioDefecto(CasoNegocioDtoRequest casonegocio);
    }
}
