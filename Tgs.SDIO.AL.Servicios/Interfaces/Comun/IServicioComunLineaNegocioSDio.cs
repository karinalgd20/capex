﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<ListaDtoResponse> ListarLineaNegocio(LineaNegocioDtoRequest lineaNegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        LineaNegocioDtoResponse ObtenerLineaNegocio(LineaNegocioDtoRequest lineaNegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarLineaNegocio(LineaNegocioDtoRequest lineaNegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarLineaNegocio(LineaNegocioDtoRequest lineaNegocio);
    }
}
