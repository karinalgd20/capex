﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarCliente(ClienteDtoRequest cliente);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ClienteDtoResponse> ListarCliente(ClienteDtoRequest cliente);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ClienteDtoResponse ObtenerCliente(ClienteDtoRequest cliente);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarCliente(ClienteDtoRequest cliente);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ClienteDtoResponsePaginado ListarClientePaginado(ClienteDtoRequest cliente);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ClienteDtoResponse ObtenerClientePorCodigo(ClienteDtoRequest cliente);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ClienteDtoResponse> ListarClientePorDescripcion(ClienteDtoRequest cliente);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarClienteCartaFianza(ClienteDtoRequest cliente);
    }
}
