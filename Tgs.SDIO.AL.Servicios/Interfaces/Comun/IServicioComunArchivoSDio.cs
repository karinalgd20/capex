﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ArchivoPaginadoDtoResponse ListarArchivoPaginado(ArchivoDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarArchivo(ArchivoDtoRequest archivo, byte[] fileArchivo);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InactivarArchivo(ArchivoDtoRequest archivo);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ArchivoDtoResponse ObtenerArchivo(ArchivoDtoRequest request);
    }
}
