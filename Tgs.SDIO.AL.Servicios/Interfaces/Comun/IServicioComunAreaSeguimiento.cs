﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{
    public partial interface IServicioComunSDio
    {

        [OperationContract]
        AreaSeguimientoPaginadoDtoResponse ListadoAreasSeguimientoPaginado(AreaSeguimientoDtoRequest request);
        [OperationContract]
        AreaSeguimientoDtoResponse ObtenerAreaSeguimientoPorId(AreaSeguimientoDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarAreaSeguimiento(AreaSeguimientoDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarAreaSeguimiento(AreaSeguimientoDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarAreaSeguimiento(AreaSeguimientoDtoRequest request);
        [OperationContract]
        List<ListaDtoResponse> ListarComboAreaSeguimiento();
    }
}
