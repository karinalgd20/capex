﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;


namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{
    public partial interface IServicioComunSDio
    {
        [OperationContract]
        ProcesoResponse RegistrarRiesgoProyecto(RiesgoProyectoDtoRequest request);

        [OperationContract]
        RiesgoProyectoDtoResponse ObtenerRiesgoProyectoPorId(RiesgoProyectoDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarRiesgoProyecto(RiesgoProyectoDtoRequest request);

        [OperationContract]
        ProcesoResponse EliminarRiesgoProyecto(RiesgoProyectoDtoRequest request);

        [OperationContract]
        RiesgoProyectoPaginadoDtoResponse ListadoRiesgoProyectosPaginado(RiesgoProyectoDtoRequest request);
    }
}
