﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarMaestraPorIdRelacion(MaestraDtoRequest maestra);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<MaestraDtoResponse> ObtenerMaestra(MaestraDtoRequest maestra);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarMaestra(MaestraDtoRequest maestra);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarMaestra(MaestraDtoRequest maestra);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarMaestra(MaestraDtoRequest maestra);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarMaestraPorValor(MaestraDtoRequest maestra);
        [OperationContract]
        List<MaestraDtoResponse> ListarMaestraPorIdRelacion2(MaestraDtoRequest maestra);
    }
}

