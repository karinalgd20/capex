﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;


namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {


        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<DireccionComercialDtoResponse> ListarDireccionComercial(DireccionComercialDtoRequest direccionComercial);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        DireccionComercialDtoResponse ObtenerDireccionComercial(DireccionComercialDtoRequest direccionComercial);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarDireccionComercial(DireccionComercialDtoRequest direccionComercial);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarDireccionComercial(DireccionComercialDtoRequest direccionComercial);
    }
}
