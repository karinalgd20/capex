﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;


namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{
    public partial interface IServicioComunSDio
    {
        [OperationContract]
        ConceptoSeguimientoPaginadoDtoResponse ListadoConceptosSeguimientoPaginado(ConceptoSeguimientoDtoRequest request);
        [OperationContract]
        ConceptoSeguimientoDtoResponse ObtenerConceptoSeguimientoPorId(ConceptoSeguimientoDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request);
        [OperationContract]
        List<ListaDtoResponse> ListarComboConceptoSeguimiento();
        [OperationContract]
        List<ListaDtoResponse> ListarComboConceptoSeguimientoAgrupador();
        [OperationContract]
        List<ListaDtoResponse> ListarComboConceptoSeguimientoNiveles();
    }
}