﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;


namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{
    public partial interface IServicioComunSDio
    {
        [OperationContract]
        ActividadPaginadoDtoResponse ListatActividadPaginado(ActividadDtoRequest request);
        [OperationContract]
        ActividadDtoResponse ObtenerActividadPorId(ActividadDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarActividad(ActividadDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarActividad(ActividadDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarActividad(ActividadDtoRequest request);
        [OperationContract]
        List<ListaDtoResponse> ListarComboActividadPadres();
    }
}