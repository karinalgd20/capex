﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{
    public partial interface IServicioComunSDio
    {
        [OperationContract]
        List<ComboDtoResponse> ListarComboSegmentoNegocio();
        [OperationContract]
        List<ListaDtoResponse> ListarComboSegmentoNegocioSimple();
    }
}
