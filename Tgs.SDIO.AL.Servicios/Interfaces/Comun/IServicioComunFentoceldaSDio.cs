﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {


        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<FentoceldaDtoResponse> ListarFentocelda(FentoceldaDtoRequest fentocelda);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        FentoceldaDtoResponse ObtenerFentocelda(FentoceldaDtoRequest fentocelda);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarFentocelda(FentoceldaDtoRequest fentocelda);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarFentocelda(FentoceldaDtoRequest fentocelda);
    }
}
