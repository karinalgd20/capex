﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;


namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{
    public partial interface IServicioComunSDio
    {
        [OperationContract]
        List<ListaDtoResponse> ListarComboUbigeoDepartamento();
        [OperationContract]

        List<ListaDtoResponse> ListarComboUbigeoProvincia(UbigeoDtoRequest request);
        [OperationContract]

        List<ListaDtoResponse> ListarComboUbigeoDistrito(UbigeoDtoRequest request);

        [OperationContract]
        UbigeoDtoResponse ObtenerPorCodigoDistrito(UbigeoDtoRequest ubigeo);
    }
}
