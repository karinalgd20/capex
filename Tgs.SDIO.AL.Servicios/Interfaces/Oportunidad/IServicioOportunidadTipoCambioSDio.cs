﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarOportunidadTipoCambio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadTipoCambioDtoResponse ObtenerOportunidadTipoCambio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarOportunidadTipoCambio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadTipoCambioPaginadoDtoResponse ListaOportunidadTipoCambioPaginado(OportunidadTipoCambioDtoRequest oportunidadTipoCambio);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadTipoCambioDtoResponse ObtenerOportunidadTipoCambioPorLineaNegocio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio);
    }
}
