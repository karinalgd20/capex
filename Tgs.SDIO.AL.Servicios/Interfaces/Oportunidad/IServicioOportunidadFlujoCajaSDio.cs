﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        ProcesoResponse RegistrarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaDtoResponse ObtenerOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<OportunidadFlujoCajaDtoResponse> ListaOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<OportunidadFlujoCajaDtoResponse> ListarOportunidadFlujoCajaBandeja(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListaAnoMesProyecto(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<OportunidadFlujoCajaDtoResponse> GeneraCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse EliminarCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<OportunidadFlujoCajaDtoResponse> GeneraServicio(OportunidadFlujoCajaDtoRequest casonegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarPeriodoOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaPaginadoDtoResponse ListarSubservicioPaginado(OportunidadFlujoCajaDtoRequest proyecto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaDtoResponse ObtenerSubServicio(OportunidadFlujoCajaDtoRequest solicitud);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InhabilitarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InhabilitarOportunidadEcapex(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaPaginadoDtoResponse ListarServiciosCircuitosPaginado(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListaServicioPorOportunidadLineaNegocio(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarComponente(OportunidadFlujoCajaDtoRequest flujocaja);


        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarServicioComponente(ServicioSubServicioDtoRequest servicioSubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarCasoNegocioServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaDtoResponse ObtenerServicioPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadCaratulaTotal(OportunidadFlujoCajaDtoRequest casonegocio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadEcapexTotal(OportunidadFlujoCajaDtoRequest casonegocio);
    }
}
