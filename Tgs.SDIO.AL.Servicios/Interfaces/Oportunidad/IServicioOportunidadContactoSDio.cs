﻿using System.ServiceModel;

using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarContacto(ContactoDtoRequest contacto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarContacto(ContactoDtoRequest contacto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InactivarContacto(ContactoDtoRequest contacto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ContactoPaginadoDtoResponse ListarContactoPaginado(ContactoDtoRequest contacto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ContactoDtoResponse ObtenerContactoPorId(ContactoDtoRequest contacto);
    }
}