﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        ProcesoResponse RegistrarOportunidadCosto(OportunidadCostoDtoRequest oportunidadCosto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        OportunidadCostoDtoResponse ObtenerOportunidadCosto(OportunidadCostoDtoRequest oportunidadCosto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarOportunidadCosto(OportunidadCostoDtoRequest oportunidadCosto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadCostoPaginadoDtoResponse ListaOportunidadCostoPaginado(OportunidadCostoDtoRequest oportunidadCosto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarOportunidadCostoPorLineaNegocio(OportunidadCostoDtoRequest oportunidadCosto);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarOportunidadCostoPorLineaNegocio(OportunidadCostoDtoRequest oportunidadCosto);
    }
}
