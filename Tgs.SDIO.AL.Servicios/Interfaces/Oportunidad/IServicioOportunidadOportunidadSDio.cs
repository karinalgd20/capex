﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadDtoResponse ObtenerOportunidad(OportunidadDtoRequest oportunidad);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarVersionOportunidad(OportunidadDtoRequest oportunidad);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse OportunidadGanador(OportunidadDtoRequest oportunidad);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadPaginadoDtoResponse ListarOportunidadCabecera(OportunidadDtoRequest oportunidad);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<OportunidadDtoResponse> ListarProyectadoOIBDA(OportunidadDtoRequest oportunidad);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse OportunidadInactivar(OportunidadDtoRequest oportunidad);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarOportunidad(OportunidadDtoRequest oportunidad);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarOportunidad(OportunidadDtoRequest oportunidad);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<OportunidadDtoResponse> ObtenerVersiones(OportunidadDtoRequest oportunidad);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse NuevaVersion(OportunidadDtoRequest oportunidad);
    }
}
