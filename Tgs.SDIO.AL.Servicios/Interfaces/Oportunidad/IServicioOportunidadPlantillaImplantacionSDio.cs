﻿using System.ServiceModel;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarPlantillaImplantacion(PlantillaImplantacionDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InactivarPlantillaImplantacion(PlantillaImplantacionDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        PlantillaImplantacionPaginadoDtoResponse ListarPlantillaImplantacionPaginado(PlantillaImplantacionDtoRequest filtro);
    }
}
