﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest proyecto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest proyecto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadServicioCMIPaginadoDtoResponse ListarOportunidadServicioCMIPaginado(OportunidadServicioCMIDtoRequest proyecto);

    }
}
