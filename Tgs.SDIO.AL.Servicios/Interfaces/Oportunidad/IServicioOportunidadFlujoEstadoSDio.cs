﻿using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoEstadoDtoResponse ObtenerOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoEstadoDtoResponse ObtenerOportunidadFlujoEstadoId(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InsertarConfiguracionFlujo(OportunidadLineaNegocioDtoRequest oportunidadFlujoEstado);
    }
}
