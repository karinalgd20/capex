﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        ProcesoResponse RegistrarOportunidadDocumento(OportunidadDocumentoDtoRequest oportunidadDocumento);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarOportunidadDocumento(OportunidadDocumentoDtoRequest oportunidadDocumento);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadDocumentoDtoResponse ObtenerOportunidadDocumento(OportunidadDocumentoDtoRequest oportunidadDocumento);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarOportunidadDocumento(OportunidadDocumentoDtoRequest oportunidadDocumento);

    }
}
