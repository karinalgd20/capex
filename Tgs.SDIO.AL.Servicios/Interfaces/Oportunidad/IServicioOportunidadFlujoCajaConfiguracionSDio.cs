﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaConfiguracionDtoResponse ObtenerOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<OportunidadFlujoCajaConfiguracionDtoResponse> ListaOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<OportunidadFlujoCajaConfiguracionDtoResponse> ListarOportunidadFlujoCajaConfiguracionBandeja(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);
    }
}
