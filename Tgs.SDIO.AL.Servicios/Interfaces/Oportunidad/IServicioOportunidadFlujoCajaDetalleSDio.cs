﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalle);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaDetalleDtoResponse ObtenerOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest flujocaja);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadFlujoCajaDetalleDtoResponse GeneraProyectadoOportunidadFlujoCaja(OportunidadFlujoCajaDetalleDtoRequest flujocaja);
    }
}
