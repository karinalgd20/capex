﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ServicioConceptoProyectadoDtoResponse> ListarConceptos(ServicioConceptoProyectadoDtoRequest concepto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarConceptoPersonalizado(ServicioConceptoProyectadoDtoRequest concepto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarConceptoPersonalizado(ServicioConceptoProyectadoDtoRequest concepto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ServicioConceptoProyectadoDtoResponse ObtenerConceptoPersonalizado(ServicioConceptoProyectadoDtoRequest concepto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ServicioConceptoProyectadoDtoResponse> DatosPorVersion(ServicioConceptoProyectadoDtoRequest concepto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        decimal? IndicadorCostoDirecto(ServicioConceptoProyectadoDtoRequest concepto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        decimal? IndicadorIngresoTotal(ServicioConceptoProyectadoDtoRequest concepto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        decimal? IndicadorCapex(ServicioConceptoProyectadoDtoRequest concepto);
    }
}
