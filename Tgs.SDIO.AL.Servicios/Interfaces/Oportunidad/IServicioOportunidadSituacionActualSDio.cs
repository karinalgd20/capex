﻿using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SituacionActualDtoResponse ObtenerSituacionActualPorIdOportunidad(SituacionActualDtoRequest situacionActual);

        /// <summary>
        /// Registra o actualiza la situacion actual de la oportunidad.
        /// </summary>
        /// <param name="situacionActual">Recepciona la información de la situacion actual</param>
        /// <returns>Retorma un mensaje de la operación realizada.</returns>
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarSituacionActual(SituacionActualDtoRequest situacionActual);
    }
}