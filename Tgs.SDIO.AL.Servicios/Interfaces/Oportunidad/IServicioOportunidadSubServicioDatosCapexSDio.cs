﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SubServicioDatosCapexDtoResponse ObtenerSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SubServicioDatosCapexDtoResponse ListarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SubServicioDatosCapexDtoResponse CantidadEstudiosEsp(SubServicioDatosCapexDtoRequest dto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SubServicioDatosCapexDtoResponse CantidadEquiposEsp(SubServicioDatosCapexDtoRequest dto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SubServicioDatosCapexDtoResponse CantidadRouters(SubServicioDatosCapexDtoRequest dto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SubServicioDatosCapexDtoResponse CantidadModems(SubServicioDatosCapexDtoRequest dto);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SubServicioDatosCapexDtoResponse CantidadEquiposSeguridad(SubServicioDatosCapexDtoRequest dto);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SubServicioDatosCapexDtoResponse CantidadCapex(SubServicioDatosCapexDtoRequest dto);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SubServicioDatosCapexDtoResponse CantidadSolarWindVPN(SubServicioDatosCapexDtoRequest dto);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SubServicioDatosCapexDtoResponse CantidadSatelitales(SubServicioDatosCapexDtoRequest dto);
    }
}
