﻿using System.ServiceModel;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SedePaginadoDtoResponse ListarSedeInstalacion(OportunidadDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SedePaginadoDtoResponse ListarSedeCliente(OportunidadDtoRequest request);
        
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ClienteDtoResponse ObtenerClientePorIdOportunidad(OportunidadDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SedeDtoResponse ObtenerSedePorId(SedeDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarSede(SedeDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse AgregarSedeInstalacion(SedeDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SedeInstalacionDtoResponse obtenerSedeInstalada(int IdSede, int IdOportunidad);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse EliminarSede(SedeDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarSede(SedeDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        UbigeoDtoResponse ObtenerCodDetalleUbigeo(int IdUbigeo);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse EliminarServicios(ServicioSedeInstalacionDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SedePaginadoDtoResponse ListarSedeServiciosPaginado(SedeDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SedeDtoResponse DireccionBuscar(SedeDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SedeDtoResponse DetalleSedePorId(SedeDtoRequest request);
    }
}
