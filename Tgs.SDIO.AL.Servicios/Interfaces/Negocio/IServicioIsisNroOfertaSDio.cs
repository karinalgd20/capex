﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Negocio
{
    public partial interface IServicioNegocioSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse AgregarIsisNroOferta(IsisNroOfertaDtoRequest resquest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse EliminarIsisNroOferta(IsisNroOfertaDtoRequest resquest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<IsisNroOfertaDtoResponse> ListarIsisNroOferta(IsisNroOfertaDtoRequest resquest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        IsisNroOfertaPaginadoDtoResponse ListarIsisNroOfertaPaginado(IsisNroOfertaDtoRequest resquest);
    }
}
