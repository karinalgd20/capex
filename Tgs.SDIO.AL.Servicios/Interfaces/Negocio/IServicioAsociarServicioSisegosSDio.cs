﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Negocio
{
    public partial interface IServicioNegocioSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse AsociarServicioSisegos(AsociarServicioSisegosDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<AsociarServicioSisegosDtoResponse> ListarServicioSisegos(AsociarServicioSisegosDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        AsociarServicioSisegosPaginadoDtoResponse ListarServicioSisegosPaginado(AsociarServicioSisegosDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        RPAServiciosxNroOfertaDtoResponse ObtenerServicioById(AsociarServicioSisegosDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        RPAEquiposServicioDtoResponse ObtenerEquipoById(AsociarServicioSisegosDtoRequest request);
        //[OperationContract]
        //[FaultContract(typeof(ErrorDto))]
        //RPASisegoDetalleDtoResponse ObtenerCodSisegoById(AsociarServicioSisegosDtoRequest request);
    }
}
