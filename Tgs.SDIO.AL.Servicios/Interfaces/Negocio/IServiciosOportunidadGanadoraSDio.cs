﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Negocio
{
    public partial interface IServicioNegocioSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<OportunidadGanadoraDtoResponse> ListarOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadGanadoraDtoResponse ObtenerOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SalesForceConsolidadoCabeceraDtoResponse ObtenerCasoOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ClienteDtoResponse ObtenerRucClientePorIdOportunidad(OportunidadGanadoraDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ValidarOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse EliminarOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarEtapaCierreOportunidad(OportunidadGanadoraDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        BandejaOportunidadPaginadoDtoResponse ListarBandejaOportunidadPaginado(OportunidadGanadoraDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadGanadoraDtoResponse ObtenerOportunidadById(OportunidadGanadoraDtoRequest request);
    }
}
