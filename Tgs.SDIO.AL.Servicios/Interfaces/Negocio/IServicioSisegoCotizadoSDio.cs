﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Negocio
{
    public partial interface IServicioNegocioSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse AgregarSisegoCotizado(SisegoCotizadoDtoRequest resquest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse EliminarSisegoCotizado(SisegoCotizadoDtoRequest resquest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<SisegoCotizadoDtoResponse> ListarSisegosCotizados(SisegoCotizadoDtoRequest resquest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SisegoCotizadoPaginadoDtoResponse ListarSisegosCotizadosPaginado(SisegoCotizadoDtoRequest resquest);
    }
}
