﻿
using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza
{
    public partial interface IServicioCartaFianzaGeneralSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CartaFianzaColaboradorDetalleResponse> ListarColaborador(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestList);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse AsignarSupervisorAGerenteCuenta(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse AsignarGerenteCuentaACartaFianza(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarTesorera(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest);
    }
}
