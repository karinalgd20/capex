﻿
using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza
{
    public partial interface IServicioCartaFianzaGeneralSDio

    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizaARenovacionCartaFianza(CartaFianzaDetalleRenovacionRequest CartaFianzaRequestActualiza);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InsertaCartaFianza(CartaFianzaRequest CartaFianzaRequestInserta);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizaCartaFianza(CartaFianzaRequest CartaFianzaRequestActualiza);

      

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CartaFianzaListaResponsePaginado ListaCartaFianza(CartaFianzaRequest CartaFianzaRequestLista);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CartaFianzaListaResponse> ListaCartaFianzaId(CartaFianzaRequest CartaFianzaRequestLista);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        MailResponse ObtenerDatosCorreo(MailRequest mailRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarCartaFianzaRequerida(CartaFianzaRequest CartaFianzaRequestActualiza);
		
		[OperationContract]
        [FaultContract(typeof(ErrorDto))]
        DashBoardCartaFianzaResponse ListaDatos_DashoBoard_CartaFianza(DashBoardCartaFianzaRequest dashBoardCartaFianza);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CartaFianzaListaResponse> ListaCartaFianzaCombo(CartaFianzaRequest cartaFianzaRequestLista);


        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<DashBoardCartaFianzaListGResponse> ListaDatos_DashoBoard_CartaFianza_Pie(DashBoardCartaFianzaRequest dashBoardCartaFianza);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CartaFianzaListaResponse> CartasFianzasSinRespuesta(CartaFianzaRequest cartaFianzaRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse GuardarRespuestaGerenteCuenta(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse GuardarConfirmacionGerenteCuenta(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacion);
    }
}
