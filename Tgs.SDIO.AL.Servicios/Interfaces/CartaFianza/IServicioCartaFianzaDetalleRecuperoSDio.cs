﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.DataContracts.Dto.Response.CartaFianza.CartaFianzaDetalleSeguimientoResponse;

namespace Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza
{
    public partial interface IServicioCartaFianzaGeneralSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest CartaFianzaDetalleRecuperoActualiza);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CartaFianzaDetalleRecuperoResponsePaginado ListaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest CartaFianzaDetalleRecuperoLista);
    }
}
