﻿
using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza
{
    public partial interface IServicioCartaFianzaGeneralSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InsertaCartaFianzaDetalleAccion(CartaFianzaDetalleAccionRequest CartaFianzaDetalleAccionesRequestInserta);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizaCartaFianzaDetalleAccion(CartaFianzaDetalleAccionRequest CartaFianzaDetalleAccionesRequestActualiza);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CartaFianzaDetalleAccionResponse> ListaCartaFianzaDetalleAccionId(CartaFianzaDetalleAccionRequest CartaFianzaDetalleAccionesRequestLista);


       
    }
}
