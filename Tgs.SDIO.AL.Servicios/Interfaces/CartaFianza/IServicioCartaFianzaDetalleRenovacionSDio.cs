﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza
{
    public partial interface IServicioCartaFianzaGeneralSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest cartaFianzaDetalleRenovacionActualiza);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CartaFianzaDetalleRenovacionResponsePaginado ListaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest cartaFianzaDetalleRenovacionLista);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        MailResponse ObtenerDatosCorreoRenovacion(MailRequest mailRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse EnviarCorreoCartaFianza(MailRequest mailRequest);

    }
}
