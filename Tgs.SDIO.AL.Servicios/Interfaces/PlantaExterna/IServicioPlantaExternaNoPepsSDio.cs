﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.PlantaExterna
{
    public partial interface IServicioPlantaExternaSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        NoPepsDtoResponse ObtenerNoPeps(NoPepsDtoRequest noPepsRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarNoPeps(NoPepsDtoRequest noPepsRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse EliminarNoPeps(NoPepsDtoRequest noPepsRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarNoPeps(NoPepsDtoRequest noPepsRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        NoPepsPaginadoDtoResponse ListarNoPeps(NoPepsDtoRequest noPepsRequest);
    }
}
