﻿using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;

namespace Tgs.SDIO.AL.Servicios.Interfaces.PlantaExterna
{
    public partial interface IServicioPlantaExternaSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        TipoCambioSisegoPaginadoDtoResponse ListarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        TipoCambioSisegoDtoResponse ObtenerTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest);
    }
}
