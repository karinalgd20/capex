﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.PlantaExterna
{
    public partial interface IServicioPlantaExternaSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ComboDtoResponse> ListarAccionEstrategica(AccionEstrategicaDtoRequest accionRequest);
    }
}
