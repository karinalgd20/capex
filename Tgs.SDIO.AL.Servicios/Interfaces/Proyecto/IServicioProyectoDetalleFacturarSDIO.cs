﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Proyecto
{
    public partial interface IServicioProyectoSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        DetalleFacturarDtoResponse ObtenerDetalleFacturarPorIdProyecto(DetalleFacturarDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        DetalleFacturarDtoResponse ObtenerDetalleFacturarPorId(DetalleFacturarDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarDetalleFacturar(DetalleFacturarDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarDetalleFacturar(DetalleFacturarDtoRequest request);

    }
}
