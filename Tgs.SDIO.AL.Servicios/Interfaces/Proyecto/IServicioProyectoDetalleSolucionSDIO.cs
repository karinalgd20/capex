﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Proyecto
{
    public partial interface IServicioProyectoSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<DetalleSolucionDtoResponse> ObtenerDetallePorIdProyecto(DetalleSolucionDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        DetalleSolucionDtoResponse ObtenerDetalleSolucionById(DetalleSolucionDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarDetalleSolucion(DetalleSolucionDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse EliminarDetalleSolucion(DetalleSolucionDtoRequest request);

    }
}
