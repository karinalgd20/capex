﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Proyecto
{
    public partial interface IServicioProyectoSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ProyectoDtoResponse> ListarProyectoCabecera(ProyectoDtoRequest proyecto);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadGanadaDtoResponse ObtenerOportunidadGanada(OportunidadGanadaDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarProyecto(ProyectoDtoRequest proyecto);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarProyecto(ProyectoDtoRequest proyecto);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProyectoDtoResponse ObtenerProyectoByID(ProyectoDtoRequest proyecto);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarProyectoDescripcion(ProyectoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarStageFinanciero(DetalleFinancieroDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ProcesarArchivosFinancieros(ProyectoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<PagoFinancieroDtoResponse> ListarPagos(ProyectoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<IndicadorFinancieroDtoResponse> ListarIndicadores(ProyectoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CostoFinancieroDtoResponse> ListarCostos(ProyectoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CostoConceptoDtoResponse> ListarCostosOpex(ProyectoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CostoConceptoDtoResponse> ListarCostosCapex(ProyectoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<DetalleFinancieroDtoResponse> ListarServiciosCMI(ProyectoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarEtapaEstado(EtapaProyectoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<DetalleFinancieroDtoResponse> BuscarDetalleFinancieroPorProyecto(ProyectoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ProyectoDtoResponse> ListarProyecto(ProyectoDtoRequest request);
    }
}
