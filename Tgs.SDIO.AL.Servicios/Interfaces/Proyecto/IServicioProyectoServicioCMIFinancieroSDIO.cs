﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Proyecto
{
    public partial interface IServicioProyectoSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InsertarServicioCMIFinanciero(ServicioCMIFinancieroDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ServicioCMIFinancieroDtoResponse ObtenerServicioCMIFinanciero(ServicioCMIFinancieroDtoRequest request);
    }
}
