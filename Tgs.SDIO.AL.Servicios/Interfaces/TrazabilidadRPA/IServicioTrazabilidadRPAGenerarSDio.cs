﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Interfaces.TrazabilidadRPA
{
    public partial interface IServicioTrazabilidadRPASDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse GenerarCierreOfertaRPA(CierreDeOfertaRPA request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse GenerarEmisionCircuitoRPA(EmisionCircuitoRPA request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarEstadoRPA(string Numero_oportunidad, string EstadoOportunidad);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadGanadoraPaginadoDtoResponse ObtenerListaOportunidad();
    }
}
