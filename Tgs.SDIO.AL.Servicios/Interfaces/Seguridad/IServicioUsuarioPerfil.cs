﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Seguridad
{
    public partial interface IServicioSeguridadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarEstadoUsuarioPerfil(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<UsuarioPerfilDtoResponse> ListarPerfilesAsignados(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarPerfilesPorSistema();

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarUsuarioPerfil(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest);
    }
}
