﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Seguridad
{
    public partial interface IServicioSeguridadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<PerfilDtoResponse> ObtenerPerfiles(int idUsuarioSistema);
    }
}
