﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Trazabilidad
{
   public  partial interface IServicioTrazabilidadSDio
    {

        [OperationContract]
        ObservacionOportunidadPaginadoDtoResponse ListadoObservacionesOportunidadPaginado(ObservacionOportunidadDtoRequest request);
        [OperationContract]
        ObservacionOportunidadDtoResponse ObtenerObservacionOportunidadPorId(ObservacionOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarObservacionOportunidad(ObservacionOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarObservacionOportunidad(ObservacionOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarObservacionOportunidad(ObservacionOportunidadDtoRequest request);
        [OperationContract]
        RecursoOportunidadPaginadoDtoResponse ListadoRecursosOportunidadPaginado(RecursoOportunidadDtoRequest request);
        [OperationContract]
        RecursoOportunidadDtoResponse ObtenerRecursoOportunidadPorId(RecursoOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarRecursoOportunidad(RecursoOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarRecursoOportunidad(RecursoOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarRecursoOportunidad(RecursoOportunidadDtoRequest request);
        [OperationContract]
        OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalObservacionPaginado(OportunidadSTDtoRequest request);
        [OperationContract]
        OportunidadSTDtoResponse ObtenerOportunidadSTPorId(OportunidadSTDtoRequest request);
        [OperationContract]
        CasoOportunidadDtoResponse ObtenerCasoOportunidadPorId(CasoOportunidadDtoRequest request);
        [OperationContract]
        OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalRecursoPaginado(OportunidadSTDtoRequest request);
        [OperationContract]
        OportunidadSTPaginadoDtoResponse ListaOportunidadSTMasivoPaginado(OportunidadSTDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarRecursoOportunidadMasivo(RecursoOportunidadDtoRequest request);
        [OperationContract]
        OportunidadSTSeguimientoPreventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPreventaPaginado(OportunidadSTSeguimientoPreventaDtoRequest request);
        [OperationContract]
        OportunidadSTSeguimientoPostventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPostventaPaginado(OportunidadSTSeguimientoPostventaDtoRequest request);
        [OperationContract]
        DatosPreventaMovilDtoResponse ObtenerDatosPreventaMovilPorId(DatosPreventaMovilDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarDatosPreventaMovil(DatosPreventaMovilDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarDatosPreventaMovil(DatosPreventaMovilDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarDatosPreventaMovil(DatosPreventaMovilDtoRequest request);
        [OperationContract]
        List<ListaDtoResponse> ListarComboEstadoCaso();
        [OperationContract]
        List<ListaDtoResponse> ListarComboTipoOportunidad();

        [OperationContract]
        List<ListaDtoResponse> ListarComboMotivoOportunidad();
        [OperationContract]
        List<ListaDtoResponse> ListarComboFuncionPropietario();
        [OperationContract]
        ProcesoResponse ActualizarDatosPreventaMovilModalPreventa(DatosPreventaMovilDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarOportunidadSTModalPreventa(OportunidadSTDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarCasoOportunidadModalPreventa(CasoOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse CargaExcelDatosPreventaMovil(DatosPreventaMovilDtoRequest request);

        [OperationContract]
        TiempoAtencionEquipoPaginadoDtoResponse ListadoTiempoAtencionEquipoPaginado(TiempoAtencionEquipoDtoRequest request);
        [OperationContract]
        TiempoAtencionEquipoDtoResponse ObtenerTiempoAtencionEquipoPorId(TiempoAtencionEquipoDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request);
        [OperationContract]
        List<ListaDtoResponse> ListarComboEtapaOportunidadPorIdFase(FaseDtoRequest request);
        [OperationContract]
        List<ListaDtoResponse> ListarComboEtapaOportunidad();
        [OperationContract]
        List<ListaDtoResponse> ListarComboTipoSolicitud();
    }
}
