﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Compra
{
    public partial interface IServicioCompraSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarPrePeticionOrdinariaDetalle(PrePeticionOrdinariaDetalleDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<PrePeticionOrdinariaDetalleDtoResponse> ListarPrePeticionOrdinaria(PrePeticionOrdinariaDetalleDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        PrePeticionOrdinariaDetalleDtoResponse ObtenerDetalleOrdinaria(PrePeticionOrdinariaDetalleDtoRequest request);
        }
}
