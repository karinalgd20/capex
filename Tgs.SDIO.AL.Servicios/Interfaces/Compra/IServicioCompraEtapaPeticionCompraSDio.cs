﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Compra
{
    public partial interface IServicioCompraSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarEtapaPeticionCompra(EtapaPeticionCompraDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        EtapaPeticionCompraDtoResponse ObtenerEtapaPeticionCompraActual(EtapaPeticionCompraDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        EtapaPeticionCompraPaginadoDtoResponse ListarEtapaPeticionCompraPaginado(EtapaPeticionCompraDtoRequest request);
    }
}
