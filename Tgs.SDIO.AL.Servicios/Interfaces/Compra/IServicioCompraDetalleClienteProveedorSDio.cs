﻿using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Compra
{
    public partial interface IServicioCompraSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        DetalleClienteProveedorDtoResponse ObtenerDetalleClienteProveedor(DetalleClienteProveedorDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarDetalleClienteProveedor(DetalleClienteProveedorDtoRequest request);

    }
}
