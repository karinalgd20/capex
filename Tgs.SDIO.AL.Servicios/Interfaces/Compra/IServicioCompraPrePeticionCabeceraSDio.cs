﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Compra
{
    public partial interface IServicioCompraSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarPrePeticionCompraCabecera(PrePeticionCabeceraDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<PrePeticionCabeceraDtoResponse> ListarPrePeticion(PrePeticionCabeceraDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<PrePeticionCabeceraDtoResponse> ListarPrePeticionCabecera(PrePeticionCabeceraDtoRequest request);
        
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        PrePeticionCabeceraDtoResponse ObtenerPrePeticionCabecera(PrePeticionCabeceraDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        int ValidacionMontoLimite(PrePeticionCabeceraDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        string ObtenerSaldo(PrePeticionCabeceraDtoRequest request);
    }
}
