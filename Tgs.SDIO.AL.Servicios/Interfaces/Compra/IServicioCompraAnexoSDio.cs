﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Compra
{
    public partial interface IServicioCompraSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarAnexo(AnexoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<AnexoDtoResponse> ListarAnexos(AnexoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        bool EliminarAnexo(AnexoDtoRequest request);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        AnexoArchivoDtoResponse ObtenerAnexoArchivo(int iIdAnexoPeticionCompra);
    }
}
