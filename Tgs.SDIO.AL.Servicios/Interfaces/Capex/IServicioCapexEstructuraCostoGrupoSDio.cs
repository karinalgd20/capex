﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
  
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<EstructuraCostoGrupoDtoResponse> ListarEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest EstructuraCostoGrupo);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest EstructuraCostoGrupo);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest EstructuraCostoGrupo);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        EstructuraCostoGrupoPaginadoDtoResponse ListaEstructuraCostoGrupoPaginado(EstructuraCostoGrupoDtoRequest EstructuraCosto);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        EstructuraCostoGrupoDetalleDtoResponse ObtenerEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest estructuraCosto);

    }
}
