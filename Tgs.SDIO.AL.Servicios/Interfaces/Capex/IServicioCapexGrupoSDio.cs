﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<GrupoDtoResponse> ListarBandejaGrupo(GrupoDtoRequest Grupo);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarGrupo(GrupoDtoRequest Grupo);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        GrupoDtoResponse ObtenerGrupo(GrupoDtoRequest Grupo);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarGrupo(GrupoDtoRequest Grupo);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarGrupo(GrupoDtoRequest Grupo);
   
    }
}
