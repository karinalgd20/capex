﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<ListaDtoResponse> ListarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        AsignacionCapexTotalDetalleDtoResponse ObtenerAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InhabilitarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle);

    }
}
