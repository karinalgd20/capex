﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{

    public partial interface IServicioCapexSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarSolicitud(SolicitudDtoRequest solicitud);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InhabilitarSolicitud(SolicitudDtoRequest solicitud);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        SolicitudDtoResponse ObtenerSolicitud(SolicitudDtoRequest solicitud);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarSolicitud(SolicitudDtoRequest solicitud);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarSolicitud(SolicitudDtoRequest solicitud);

    }
}
