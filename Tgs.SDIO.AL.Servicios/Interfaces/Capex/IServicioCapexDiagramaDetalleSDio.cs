﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<ListaDtoResponse> ListarDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        DiagramaDetalleDtoResponse ObtenerDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InhabilitarDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle);

    }
}
