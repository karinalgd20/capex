﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
   
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<EstructuraCostoGrupoDetalleDtoResponse> ListarEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalleDtoRequest EstructuraCostoGrupoDetalle);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        EstructuraCostoGrupoDetalleDtoResponse ObtenerEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalleDtoRequest EstructuraCostoGrupoDetalle);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalleDtoRequest EstructuraCostoGrupoDetalle);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalleDtoRequest EstructuraCostoGrupoDetalle);

    }
}
