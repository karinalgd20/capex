﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ObservacionDtoResponse> ListarBandejaObservacion(ObservacionDtoRequest Observacion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ObservacionDtoResponse> ListarObservacion(ObservacionDtoRequest Observacion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ObservacionDtoResponse ObtenerObservacion(ObservacionDtoRequest Observacion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarObservacion(ObservacionDtoRequest Observacion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarObservacion(ObservacionDtoRequest Observacion);
    }
}
