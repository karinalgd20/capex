﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<SolicitudCapexDtoResponse> ListarBandejaSolicitudCapex(SolicitudCapexDtoRequest solicitudCapex);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<SolicitudCapexDtoResponse> ListarSolicitudCapex(SolicitudCapexDtoRequest solicitudCapex);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SolicitudCapexDtoResponse ObtenerSolicitudCapex(SolicitudCapexDtoRequest solicitudCapex);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarSolicitudCapex(SolicitudCapexDtoRequest solicitudCapex);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarSolicitudCapex(SolicitudCapexDtoRequest solicitudCapex);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SolicitudCapexPaginadoDtoResponse ListaSolicitudCapexPaginado(SolicitudCapexDtoRequest solicitudCapex);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarSolicitudCapexEstado(SolicitudCapexDtoRequest solicitudCapex);
    }
}
