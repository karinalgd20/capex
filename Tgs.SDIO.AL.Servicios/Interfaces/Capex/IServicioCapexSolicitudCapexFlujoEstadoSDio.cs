﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<SolicitudCapexFlujoEstadoDtoResponse> ListarBandejaSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<SolicitudCapexFlujoEstadoDtoResponse> ListarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SolicitudCapexFlujoEstadoDtoResponse ObtenerSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SolicitudCapexFlujoEstadoDtoResponse ObtenerSolicitudCapexFlujoEstadoId(SolicitudCapexFlujoEstadoDtoRequest solicitudCapexFlujoEstado);
    }
}
