﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<ListaDtoResponse> ListarConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ConceptosCapexDtoResponse ObtenerConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InhabilitarConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex);

    }
}
