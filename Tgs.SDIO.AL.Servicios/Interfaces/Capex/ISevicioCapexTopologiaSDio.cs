﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<TopologiaDtoResponse> ListarBandejaTopologia(TopologiaDtoRequest Topologia);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<TopologiaDtoResponse> ListarTopologia(TopologiaDtoRequest Topologia);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        TopologiaDtoResponse ObtenerTopologia(TopologiaDtoRequest Topologia);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarTopologia(TopologiaDtoRequest Topologia);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarTopologia(TopologiaDtoRequest Topologia);
    }
}
