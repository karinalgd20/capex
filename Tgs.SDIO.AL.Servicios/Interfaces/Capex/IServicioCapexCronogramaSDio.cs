﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<ListaDtoResponse> ListarCronograma(CronogramaDtoRequest Cronograma);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CronogramaDtoResponse ObtenerCronograma(CronogramaDtoRequest Cronograma);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarCronograma(CronogramaDtoRequest Cronograma);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarCronograma(CronogramaDtoRequest Cronograma);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InhabilitarCronograma(CronogramaDtoRequest Cronograma);

    }
}
