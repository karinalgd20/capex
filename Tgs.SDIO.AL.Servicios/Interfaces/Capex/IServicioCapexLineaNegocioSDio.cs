﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ListaDtoResponse> ListarCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CapexLineaNegocioDtoResponse ObtenerCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InhabilitarCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CapexLineaNegocioPaginadoDtoResponse ListarLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocio);
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CapexLineaNegocioDtoResponse ObtenerCapexLineaNegocioPorIdSolicitud(CapexLineaNegocioDtoRequest capexLineaNegocio);
    }
}
