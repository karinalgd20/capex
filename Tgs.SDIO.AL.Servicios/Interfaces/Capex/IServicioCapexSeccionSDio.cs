﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Capex
{
    public partial interface IServicioCapexSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<SeccionDtoResponse> ListarBandejaSeccion(SeccionDtoRequest Seccion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<SeccionDtoResponse> ListarSeccion(SeccionDtoRequest Seccion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        SeccionDtoResponse ObtenerSeccion(SeccionDtoRequest Seccion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarSeccion(SeccionDtoRequest Seccion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarSeccion(SeccionDtoRequest Seccion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarSeccionObservacion(SeccionDtoRequest seccion);
    }
}
