﻿using System.ServiceModel;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.AL.Servicios.Interfaces.TrazabilidadDigital
{
    public partial interface IServicioTrazabilidadDigitalSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        OportunidadGanadoraPaginadoDtoResponse ObtenerListaCodigo();

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse GenerarCodificacionRPA(CodificacionRPA request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarEstadoRPA(string Numero_oportunidad, string EstadoOportunidad);


    }
}
