﻿using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.PlantaExterna
{
    public partial class ServicioPlantaExternaSDio
    { 

        public NoPepsDtoResponse ObtenerNoPeps(NoPepsDtoRequest noPepsRequest)
        {
            return iNoPepsBl.ObtenerNoPeps(noPepsRequest);
        }

        public ProcesoResponse ActualizarNoPeps(NoPepsDtoRequest noPepsRequest)
        {
            return iNoPepsBl.ActualizarNoPeps(noPepsRequest);
        }

        public ProcesoResponse EliminarNoPeps(NoPepsDtoRequest noPepsRequest)
        {
            return iNoPepsBl.EliminarNoPeps(noPepsRequest);
        }

        public ProcesoResponse RegistrarNoPeps(NoPepsDtoRequest noPepsRequest)
        {
            return iNoPepsBl.RegistrarNoPeps(noPepsRequest);
        }

        public NoPepsPaginadoDtoResponse ListarNoPeps(NoPepsDtoRequest noPepsRequest)
        {
            return iNoPepsBl.ListarNoPeps(noPepsRequest);
        } 

    }
}
