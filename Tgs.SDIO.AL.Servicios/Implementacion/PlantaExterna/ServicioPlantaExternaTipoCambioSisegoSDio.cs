﻿using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.PlantaExterna
{
    public partial class ServicioPlantaExternaSDio
    {
        public TipoCambioSisegoPaginadoDtoResponse ListarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest)
        {
            return iTipoCambioSisegoBl.ListarTipoCambioSisego(tipoCambioDtoRequest);
        }

        public ProcesoResponse ActualizarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest)
        {
            return iTipoCambioSisegoBl.ActualizarTipoCambioSisego(tipoCambioDtoRequest);
        }

        public TipoCambioSisegoDtoResponse ObtenerTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest)
        {
            return iTipoCambioSisegoBl.ObtenerTipoCambioSisego(tipoCambioDtoRequest);
        }

        public ProcesoResponse RegistrarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest)
        {
            return iTipoCambioSisegoBl.RegistrarTipoCambioSisego(tipoCambioDtoRequest);
        }
    }
}
