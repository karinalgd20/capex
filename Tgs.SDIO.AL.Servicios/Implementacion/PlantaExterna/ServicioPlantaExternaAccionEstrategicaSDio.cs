﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.PlantaExterna
{
    public partial class ServicioPlantaExternaSDio
    {
        public List<ComboDtoResponse> ListarAccionEstrategica(AccionEstrategicaDtoRequest accionRequest)
        {
            return iAccionEstrategicaBl.ListarAccionEstrategica(accionRequest);
        }
    }
}
