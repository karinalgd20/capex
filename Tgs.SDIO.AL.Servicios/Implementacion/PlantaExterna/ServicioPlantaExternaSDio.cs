﻿using System.ServiceModel;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.PlantaExterna;
using Tgs.SDIO.AL.Servicios.Interfaces.PlantaExterna;

namespace Tgs.SDIO.AL.Servicios.Implementacion.PlantaExterna
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioPlantaExternaSDio", Name = "ServicioPlantaExternaSDio",
      ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioPlantaExternaSDio : IServicioPlantaExternaSDio
    {
        public readonly INoPepsBl iNoPepsBl;
        public readonly ITipoCambioSisegoBl iTipoCambioSisegoBl;
        public readonly IAccionEstrategicaBl iAccionEstrategicaBl;
        public ServicioPlantaExternaSDio(INoPepsBl noPeps, ITipoCambioSisegoBl tipoCambio, IAccionEstrategicaBl accionEstrategicaBl)
        {
            iNoPepsBl = noPeps;
            iTipoCambioSisegoBl = tipoCambio;
            iAccionEstrategicaBl = accionEstrategicaBl;
        }
    }
}
