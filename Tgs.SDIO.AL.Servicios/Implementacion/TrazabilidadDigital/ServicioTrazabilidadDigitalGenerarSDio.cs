﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Implementacion.TrazabilidadDigital
{
    public partial class ServicioTrazabilidadDigitalSDio
    {
        public OportunidadGanadoraPaginadoDtoResponse ObtenerListaCodigo()
        {
            return iOportunidadGanadoraBl.ObtenerListaCodigo();
        }

        public ProcesoResponse GenerarCodificacionRPA(CodificacionRPA request)
        {
            return iOportunidadGanadoraBl.GenerarCodificacionRPA(request);
        }

        public ProcesoResponse ActualizarEstadoRPA(string Numero_oportunidad, string EstadoOportunidad)
        {
            return iOportunidadGanadoraBl.ActualizarEstadoRPA(Numero_oportunidad, EstadoOportunidad);
        }


    }
}
