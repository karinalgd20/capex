﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.Servicios.Interfaces.TrazabilidadDigital;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.TrazabilidadDigital
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioTrazabilidadDigitalSDio", Name = "ServicioTrazabilidadDigitalSDio",
    ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]

    public partial class ServicioTrazabilidadDigitalSDio : IServicioTrazabilidadDigitalSDio
    {
        public readonly IOportunidadGanadoraBl iOportunidadGanadoraBl;

        public ServicioTrazabilidadDigitalSDio
        (
            IOportunidadGanadoraBl IOportunidadGanadoraBl
        )

        {
            iOportunidadGanadoraBl = IOportunidadGanadoraBl;
        }

    }
}
