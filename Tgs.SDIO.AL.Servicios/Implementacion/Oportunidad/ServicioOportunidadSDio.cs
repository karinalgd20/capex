﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioOportunidadSDio", Name = "ServicioOportunidadSDio",
        ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioOportunidadSDio : IServicioOportunidadSDio
    {
        public readonly IConceptoDatosCapexBl iConceptoDatosCapexBl;
        public readonly ISubServicioDatosCaratulaBl iSubServicioDatosCaratulaBl;
        public readonly ILineaConceptoCMIBl iLineaConceptoCMIBl;
        public readonly ILineaConceptoGrupoBl iLineaConceptoGrupoBl;
        public readonly ILineaPestanaBl iLineaPestanaBl;
        public readonly ILineaProductoCMIBl iLineaProductoCMIBl;
        public readonly IPestanaGrupoBl iPestanaGrupoBl;
        public readonly IOportunidadServicioCMIBl iOportunidadServicioCMIBl;
        public readonly IProyectoLineaConceptoProyectadoBl iProyectoLineaConceptoProyectadoBl;
        public readonly IProyectoLineaProductoBl iProyectoLineaProductoBl;
        public readonly IProyectoServicioCMIBl iProyectoServicioCMIBl;
        public readonly IProyectoServicioConceptoBl iProyectoServicioConceptoBl;
        public readonly IServicioConceptoDocumentoBl iServicioConceptoDocumentoBl;
        public readonly IServicioConceptoProyectadoBl iServicioConceptoProyectadoBl;
        public readonly IOportunidadBl iOportunidadBl;
        public readonly IOportunidadParametroBl iOportunidadParametroBl;
        public readonly IOportunidadFlujoCajaBl iOportunidadFlujoCajaBl;
        public readonly IOportunidadFlujoCajaConfiguracionBl iOportunidadFlujoCajaConfiguracionBl;
        public readonly IOportunidadFlujoCajaDetalleBl iOportunidadFlujoCajaDetalleBl;
        public readonly IOportunidadLineaNegocioBl     iOportunidadLineaNegocioBl;
        public readonly IOportunidadFlujoEstadoBl iOportunidadFlujoEstadoBl;
        public readonly ISubServicioDatosCapexBl iSubServicioDatosCapexBl;
        public readonly IOportunidadDocumentoBl iOportunidadDocumentoBl;
        public readonly IOportunidadTipoCambioBl iOportunidadTipoCambioBl;
        public readonly IOportunidadTipoCambioDetalleBl iOportunidadTipoCambioDetalleBl;
        public readonly IOportunidadCostoBl iOportunidadCostoBl;
        public readonly ISituacionActualBl iSituacionActualBl;
        public readonly IVisitaBl iVisitaBl;
        public readonly IVisitaDetalleBl iVisitaDetalleBl;
        public readonly IPestanaGrupoTipoBl iPestanaGrupoTipoBl;
        public readonly ISedeBl iSedeBl;
        public readonly IContactoBl iContactoBl;
        public readonly IEstudiosBl iEstudiosBl;
        private readonly IServicioSedeInstalacionBl iServicioSedeInstalacionBl;
        public readonly ICotizacionBl iCotizacionBl;
        public readonly IPlantillaImplantacionBl iPlantillaImplantacionBl;

        



        public ServicioOportunidadSDio
            (
            IConceptoDatosCapexBl IConceptoDatosCapexB,
            ISubServicioDatosCaratulaBl ISubservicioDatosCaratulaBl,
            ILineaConceptoCMIBl ILineaConceptoCMIBl,
            ILineaConceptoGrupoBl ILineaConceptoGrupoBl,
            ILineaPestanaBl ILineaPestanaBl,
            ILineaProductoCMIBl ILineaProductoCMIBl,
            IPestanaGrupoBl IPestanaGrupoBl,
            IOportunidadServicioCMIBl IOportunidadServicioCMIBl,
            IProyectoLineaConceptoProyectadoBl IProyectoLineaConceptoProyectadoBl,
            IProyectoLineaProductoBl IProyectoLineaProductoBl,
            IProyectoServicioCMIBl IProyectoServicioCMIBl,
            IProyectoServicioConceptoBl IProyectoServicioConceptoBl,
            IServicioConceptoDocumentoBl IServicioConceptoDocumentoBl,
            IServicioConceptoProyectadoBl IServicioConceptoProyectadoBl,
            IOportunidadBl IOportunidadBl,
            IOportunidadParametroBl IOportunidadParametroBl,
            IOportunidadFlujoCajaBl IOportunidadFlujoCajaBl,
            IOportunidadFlujoCajaConfiguracionBl IOportunidadFlujoCajaConfiguracionBl,
            IOportunidadFlujoCajaDetalleBl IOportunidadFlujoCajaDetalleBl,
            IOportunidadLineaNegocioBl IOportunidadLineaNegocioBl,
            IOportunidadFlujoEstadoBl IOportunidadFlujoEstadoBl,
            ISubServicioDatosCapexBl ISubServicioDatosCapexBl,
            IOportunidadDocumentoBl IOportunidadDocumentoBl,
            IOportunidadTipoCambioBl IOportunidadTipoCambioBl,
            IOportunidadTipoCambioDetalleBl IOportunidadTipoCambioDetalleBl,
            IOportunidadCostoBl IOportunidadCostoBl,
            ISituacionActualBl ISituacionActualBl,
            IVisitaBl IVisitaBl,
            IVisitaDetalleBl IVisitaDetalleBl ,
            IPestanaGrupoTipoBl IPestanaGrupoTipoBl,
            ISedeBl ISedeBl,
            IContactoBl IContactoBl,
            IEstudiosBl IEstudiosBl,            
            IServicioSedeInstalacionBl IServicioSedeInstalacionBl,
            ICotizacionBl ICotizacionBl,
            IPlantillaImplantacionBl IPlantillaImplantacionBl
            )

        { 
            iConceptoDatosCapexBl = IConceptoDatosCapexB;
            iSubServicioDatosCaratulaBl = ISubservicioDatosCaratulaBl;
            iLineaConceptoCMIBl = ILineaConceptoCMIBl;
            iLineaConceptoGrupoBl = ILineaConceptoGrupoBl;
            iLineaPestanaBl = ILineaPestanaBl;
            iLineaProductoCMIBl = ILineaProductoCMIBl;
            iPestanaGrupoBl = IPestanaGrupoBl;
            iOportunidadServicioCMIBl = IOportunidadServicioCMIBl;
            iProyectoLineaConceptoProyectadoBl = IProyectoLineaConceptoProyectadoBl;
            iProyectoLineaProductoBl = IProyectoLineaProductoBl;
            iProyectoServicioCMIBl = IProyectoServicioCMIBl;
            iProyectoServicioConceptoBl = IProyectoServicioConceptoBl;
            iServicioConceptoDocumentoBl = IServicioConceptoDocumentoBl;
            iServicioConceptoProyectadoBl = IServicioConceptoProyectadoBl;
            iOportunidadBl = IOportunidadBl;
            iOportunidadParametroBl = IOportunidadParametroBl;
            iOportunidadFlujoCajaBl = IOportunidadFlujoCajaBl;
            iOportunidadFlujoCajaConfiguracionBl = IOportunidadFlujoCajaConfiguracionBl;
            iOportunidadFlujoCajaDetalleBl = IOportunidadFlujoCajaDetalleBl;
            iOportunidadLineaNegocioBl = IOportunidadLineaNegocioBl;
            iOportunidadFlujoEstadoBl = IOportunidadFlujoEstadoBl;
            iSubServicioDatosCapexBl = ISubServicioDatosCapexBl;
            iOportunidadDocumentoBl = IOportunidadDocumentoBl;
            iOportunidadTipoCambioBl = IOportunidadTipoCambioBl;
            iOportunidadTipoCambioDetalleBl = IOportunidadTipoCambioDetalleBl;
            iOportunidadCostoBl = IOportunidadCostoBl;
            iSituacionActualBl = ISituacionActualBl;
            iVisitaBl = IVisitaBl;
            iVisitaDetalleBl = IVisitaDetalleBl;
            iPestanaGrupoTipoBl =IPestanaGrupoTipoBl;
            iSedeBl = ISedeBl;
            iContactoBl = IContactoBl;
            iEstudiosBl = IEstudiosBl;
            iServicioSedeInstalacionBl = IServicioSedeInstalacionBl;
            iCotizacionBl = ICotizacionBl;
            iPlantillaImplantacionBl = IPlantillaImplantacionBl;

        }
    }
}