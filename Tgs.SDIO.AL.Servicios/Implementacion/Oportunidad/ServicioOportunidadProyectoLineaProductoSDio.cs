﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio { 
        public ProcesoResponse ActualizarProyectoLineaProducto(ProyectoLineaProductoDtoRequest proyectoLineaProducto)
        {
            return iProyectoLineaProductoBl.ActualizarProyectoLineaProducto(proyectoLineaProducto);
        }

        public List<ProyectoLineaProductoDtoResponse> ListaProyectoLineaProducto(ProyectoLineaProductoDtoRequest proyectoLineaProducto)
        {
            return iProyectoLineaProductoBl.ListaProyectoLineaProducto(proyectoLineaProducto);
        }

        public ProcesoResponse RegistrarProyectoLineaProducto(ProyectoLineaProductoDtoRequest proyectoLineaProducto)
        {
            return iProyectoLineaProductoBl.RegistrarProyectoLineaProducto(proyectoLineaProducto);
        }
    }
}
