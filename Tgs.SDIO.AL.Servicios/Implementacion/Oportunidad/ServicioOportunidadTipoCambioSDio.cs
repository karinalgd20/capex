﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse RegistrarOportunidadTipoCambio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio)
        {
            return iOportunidadTipoCambioBl.RegistrarOportunidadTipoCambio(oportunidadTipoCambio);
        }
        public OportunidadTipoCambioDtoResponse ObtenerOportunidadTipoCambio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio)
        {
            return iOportunidadTipoCambioBl.ObtenerOportunidadTipoCambio(oportunidadTipoCambio);
        }
        public ProcesoResponse ActualizarOportunidadTipoCambio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio)
        {
            return iOportunidadTipoCambioBl.ActualizarOportunidadTipoCambio(oportunidadTipoCambio);
        }

        public OportunidadTipoCambioPaginadoDtoResponse ListaOportunidadTipoCambioPaginado(OportunidadTipoCambioDtoRequest oportunidadTipoCambio)
        {
            return iOportunidadTipoCambioBl.ListaOportunidadTipoCambioPaginado(oportunidadTipoCambio);
        }


        public OportunidadTipoCambioDtoResponse ObtenerOportunidadTipoCambioPorLineaNegocio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio)
         {
            return iOportunidadTipoCambioBl.ObtenerOportunidadTipoCambioPorLineaNegocio(oportunidadTipoCambio);
        }
}
}
