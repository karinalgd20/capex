﻿
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse RegistrarServicioSedeInstalacion(ServicioSedeInstalacionDtoRequest servicioSedeInstalacion)
        {
            return iServicioSedeInstalacionBl.RegistrarServicioSedeInstalacion(servicioSedeInstalacion);
        }
    }
}
