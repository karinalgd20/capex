﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio 
    {
        public ProcesoResponse ActualizarProyectoServicioCMI(ProyectoServicioCMIDtoRequest proyecto)
        {
            return iProyectoServicioCMIBl.ActualizarProyectoServicioCMI(proyecto);
        }

        public List<ProyectoServicioCMIDtoResponse> ListarProyectoServicioCMI(ProyectoServicioCMIDtoRequest proyecto)
        {
            return iProyectoServicioCMIBl.ListarProyectoServicioCMI(proyecto);
        }

        public ProcesoResponse RegistrarProyectoServicioCMI(ProyectoServicioCMIDtoRequest proyecto)
        {
            return iProyectoServicioCMIBl.RegistrarProyectoServicioCMI(proyecto);
        }
    }
}
