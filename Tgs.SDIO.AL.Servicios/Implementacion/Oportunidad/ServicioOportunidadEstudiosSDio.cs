﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public EstudiosPaginadoDtoResponse ListarEstudiosPaginado(EstudiosDtoRequest Estudios)
        {
            return iEstudiosBl.ListarEstudiosPaginado(Estudios);
        }
    }
}
