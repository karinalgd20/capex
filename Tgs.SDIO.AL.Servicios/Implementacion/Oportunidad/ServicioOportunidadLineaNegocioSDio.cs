﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public OportunidadLineaNegocioDtoResponse ObtenerOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)
        {
            return iOportunidadLineaNegocioBl.ObtenerOportunidadLineaNegocio(oportunidadLineaNegocio);
        }

        public ProcesoResponse RegistrarOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)
        {
            return iOportunidadLineaNegocioBl.RegistrarOportunidadLineaNegocio(oportunidadLineaNegocio);
        }
        public ProcesoResponse ActualizarOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)
        {
            return iOportunidadLineaNegocioBl.ActualizarOportunidadLineaNegocio(oportunidadLineaNegocio);
        }
        public ProcesoResponse InhabilitarOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)
        {
            return iOportunidadLineaNegocioBl.InhabilitarOportunidadLineaNegocio(oportunidadLineaNegocio);
        }

        public List<ListaDtoResponse> ListarLineaNegocioPorIdOportunidad(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)

        {
            return iOportunidadLineaNegocioBl.ListarLineaNegocioPorIdOportunidad(oportunidadLineaNegocio);
        }

    }
}
