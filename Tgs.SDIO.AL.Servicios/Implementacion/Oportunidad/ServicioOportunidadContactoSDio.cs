﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {

        public ProcesoResponse RegistrarContacto(ContactoDtoRequest contacto)
        {
            return iContactoBl.RegistrarContacto(contacto);
        }

        public ProcesoResponse ActualizarContacto(ContactoDtoRequest contacto)
        {
            return iContactoBl.ActualizarContacto(contacto);
        }

        public ProcesoResponse InactivarContacto(ContactoDtoRequest contacto)
        {
            return iContactoBl.InactivarContacto(contacto);
        }

        public ContactoPaginadoDtoResponse ListarContactoPaginado(ContactoDtoRequest contacto)
        {
            return iContactoBl.ListarContactoPaginado(contacto);
        }

        public ContactoDtoResponse ObtenerContactoPorId(ContactoDtoRequest contacto)
        {
            return iContactoBl.ObtenerContactoPorId(contacto);
        }
    }
}