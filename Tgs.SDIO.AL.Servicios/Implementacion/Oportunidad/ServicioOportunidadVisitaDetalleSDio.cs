﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse RegistrarVisitaDetalle(VisitaDetalleDtoRequest request)
        {
            return iVisitaDetalleBl.RegistrarVisitaDetalle(request);
        }
        public ProcesoResponse InhabilitarVisitaDetalle(VisitaDetalleDtoRequest visitaDetalle)
        {
            return iVisitaDetalleBl.InhabilitarVisitaDetalle(visitaDetalle);
        }

        public VisitaDetallePaginadoDtoResponse ListarVisitaDetallePaginado(VisitaDetalleDtoRequest request)
        {
            return iVisitaDetalleBl.ListarVisitaDetallePaginado(request);
        }
    }
}