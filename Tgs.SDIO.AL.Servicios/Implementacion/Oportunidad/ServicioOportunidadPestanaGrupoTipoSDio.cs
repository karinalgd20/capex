﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio 
    {

        public List<ListaDtoResponse> ListarPestanaGrupoTipo(PestanaGrupoTipoDtoRequest pestanaGrupoTipo)
        {
            return iPestanaGrupoTipoBl.ListarPestanaGrupoTipo(pestanaGrupoTipo);
        }
    }
}
