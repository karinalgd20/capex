﻿using System;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {

        public ProcesoResponse RegistrarOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaConfiguracionBl.RegistrarOportunidadFlujoCajaConfiguracion(flujocaja);
        }

        public ProcesoResponse ActualizarOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaConfiguracionBl.ActualizarOportunidadFlujoCajaConfiguracion(flujocaja);
        }

        public OportunidadFlujoCajaConfiguracionDtoResponse ObtenerOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaConfiguracionBl.ObtenerOportunidadFlujoCajaConfiguracion(flujocaja);
        }

        public List<OportunidadFlujoCajaConfiguracionDtoResponse> ListaOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaConfiguracionBl.ListaOportunidadFlujoCajaConfiguracion(flujocaja);
        }

        public List<OportunidadFlujoCajaConfiguracionDtoResponse> ListarOportunidadFlujoCajaConfiguracionBandeja(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaConfiguracionBl.ListarOportunidadFlujoCajaConfiguracionBandeja(flujocaja);
        }
    }
}
