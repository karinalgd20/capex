﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse RegistrarOportunidadCosto(OportunidadCostoDtoRequest oportunidadCosto)
        {
            return iOportunidadCostoBl.RegistrarOportunidadCosto(oportunidadCosto);
        }

        public OportunidadCostoDtoResponse ObtenerOportunidadCosto(OportunidadCostoDtoRequest oportunidadCosto)
        {
            return iOportunidadCostoBl.ObtenerOportunidadCosto(oportunidadCosto);
        }

        public ProcesoResponse ActualizarOportunidadCosto(OportunidadCostoDtoRequest oportunidadCosto)
        {
            return iOportunidadCostoBl.ActualizarOportunidadCosto(oportunidadCosto);
        }
        public OportunidadCostoPaginadoDtoResponse ListaOportunidadCostoPaginado(OportunidadCostoDtoRequest oportunidadCosto)
        {
            return iOportunidadCostoBl.ListaOportunidadCostoPaginado(oportunidadCosto);
        }
        public ProcesoResponse RegistrarOportunidadCostoPorLineaNegocio(OportunidadCostoDtoRequest oportunidadCosto)
        {
            return iOportunidadCostoBl.RegistrarOportunidadCostoPorLineaNegocio(oportunidadCosto);
        }
        public ProcesoResponse ActualizarOportunidadCostoPorLineaNegocio(OportunidadCostoDtoRequest oportunidadCosto)
        {
            return iOportunidadCostoBl.ActualizarOportunidadCostoPorLineaNegocio(oportunidadCosto);
        }
    }
}
