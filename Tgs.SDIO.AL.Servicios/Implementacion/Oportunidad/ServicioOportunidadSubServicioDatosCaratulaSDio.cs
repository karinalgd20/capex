﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse RegistrarSubServicioDatoCaratula(SubServicioDatosCaratulaDtoRequest concepto)
        {
            return iSubServicioDatosCaratulaBl.RegistrarSubServicioDatosCaratula(concepto);
        }

        public ProcesoResponse ActualizarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest concepto)
        {
            return iSubServicioDatosCaratulaBl.ActualizarSubServicioDatosCaratula(concepto);
        }

        public ProcesoResponse EliminarSubServicioDatoCaratula(SubServicioDatosCaratulaDtoRequest concepto)
        {
            return iSubServicioDatosCaratulaBl.EliminarSubServicioDatosCaratula(concepto);
        }

        public SubServicioDatosCaratulaDtoResponse ObtenerSubServicioDatoCaratula(SubServicioDatosCaratulaDtoRequest concepto)
        {
            return iSubServicioDatosCaratulaBl.ObtenerSubServicioDatosCaratula(concepto);
        }

        public List<SubServicioDatosCaratulaDtoResponse> ListarSubServicioDatoCaratula(SubServicioDatosCaratulaDtoRequest concepto)
        {
            return iSubServicioDatosCaratulaBl.ListarSubServicioDatosCaratula(concepto);
        }


    }
}
