﻿using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse RegistrarVisita(VisitaDtoRequest request)
        {
            return iVisitaBl.RegistrarVisita(request);
        }
        public ProcesoResponse ActualizarVisita(VisitaDtoRequest request)
        {
            return iVisitaBl.ActualizarVisita(request);
        }
        public VisitaDtoResponse ObtenerVisitaPorIdOportunidad(VisitaDtoRequest request)
        {
            return iVisitaBl.ObtenerVisitaPorIdOportunidad(request);
        }
    }
}
