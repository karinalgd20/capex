﻿using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse ActualizarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio)
        {
            return iSubServicioDatosCapexBl.ActualizarSubServicioDatosCapex(SubServicio);
        }

        public SubServicioDatosCapexDtoResponse ListarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio)
        {
            return iSubServicioDatosCapexBl.ListarSubServicioDatosCapex(SubServicio);
        }

        public SubServicioDatosCapexDtoResponse ObtenerSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio)
        {
            return iSubServicioDatosCapexBl.ObtenerSubServicioDatosCapex(SubServicio);
        }

        public ProcesoResponse RegistrarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio)
        {
            return iSubServicioDatosCapexBl.RegistrarSubServicioDatosCapex(SubServicio);
        }

        public SubServicioDatosCapexDtoResponse CantidadEstudiosEsp(SubServicioDatosCapexDtoRequest dto)
        {
            return iSubServicioDatosCapexBl.CantidadEstudiosEsp(dto);
        }

        public SubServicioDatosCapexDtoResponse CantidadEquiposEsp(SubServicioDatosCapexDtoRequest dto)
        {
            return iSubServicioDatosCapexBl.CantidadEquiposEsp(dto);
        }

        public SubServicioDatosCapexDtoResponse CantidadRouters(SubServicioDatosCapexDtoRequest dto)
        {
            return iSubServicioDatosCapexBl.CantidadRouters(dto);
        }

        public SubServicioDatosCapexDtoResponse CantidadModems(SubServicioDatosCapexDtoRequest dto)
        {
            return iSubServicioDatosCapexBl.CantidadModems(dto);
        }

        public SubServicioDatosCapexDtoResponse CantidadEquiposSeguridad(SubServicioDatosCapexDtoRequest dto)
        {
            return iSubServicioDatosCapexBl.CantidadEquiposSeguridad(dto);
        }

        public SubServicioDatosCapexDtoResponse CantidadCapex(SubServicioDatosCapexDtoRequest dto)
        {
            return iSubServicioDatosCapexBl.CantidadCapex(dto);
        }

        public SubServicioDatosCapexDtoResponse CantidadSolarWindVPN(SubServicioDatosCapexDtoRequest dto)
        {
            return iSubServicioDatosCapexBl.CantidadSolarWindVPN(dto);
        }

        public SubServicioDatosCapexDtoResponse CantidadSatelitales(SubServicioDatosCapexDtoRequest dto)
        {
            return iSubServicioDatosCapexBl.CantidadSatelitales(dto);
        }

       
    }
}
