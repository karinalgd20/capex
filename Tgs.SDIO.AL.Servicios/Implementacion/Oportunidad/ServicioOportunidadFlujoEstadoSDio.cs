﻿using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {

        public OportunidadFlujoEstadoDtoResponse ObtenerOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            return iOportunidadFlujoEstadoBl.ObtenerOportunidadFlujoEstado(oportunidadFlujoEstado);
        }

        public ProcesoResponse RegistrarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            return iOportunidadFlujoEstadoBl.RegistrarOportunidadFlujoEstado(oportunidadFlujoEstado);
        }
        public OportunidadFlujoEstadoDtoResponse ObtenerOportunidadFlujoEstadoId(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            return iOportunidadFlujoEstadoBl.ObtenerOportunidadFlujoEstadoId(oportunidadFlujoEstado);
        }
       
        public ProcesoResponse ActualizarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            return iOportunidadFlujoEstadoBl.ActualizarOportunidadFlujoEstado(oportunidadFlujoEstado);
        }

        public ProcesoResponse InsertarConfiguracionFlujo(OportunidadLineaNegocioDtoRequest oportunidadFlujoEstado)
        {
            return iOportunidadFlujoEstadoBl.InsertarConfiguracionFlujo(oportunidadFlujoEstado);
        }

    }
}
