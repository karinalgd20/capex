﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio 
    {
        public ProcesoResponse ActualizarConceptoPersonalizado(ServicioConceptoProyectadoDtoRequest concepto)
        {
            return iServicioConceptoProyectadoBl.ActualizarConceptoPersonalizado(concepto);
        }

        public List<ServicioConceptoProyectadoDtoResponse> DatosPorVersion(ServicioConceptoProyectadoDtoRequest concepto)
        {
            return iServicioConceptoProyectadoBl.DatosPorVersion(concepto);
        }

        public decimal? IndicadorCapex(ServicioConceptoProyectadoDtoRequest concepto)
        {
            return iServicioConceptoProyectadoBl.IndicadorCapex(concepto);
        }

        public decimal? IndicadorCostoDirecto(ServicioConceptoProyectadoDtoRequest concepto)
        {
            return iServicioConceptoProyectadoBl.IndicadorCostoDirecto(concepto);
        }

        public decimal? IndicadorIngresoTotal(ServicioConceptoProyectadoDtoRequest concepto)
        {
            return iServicioConceptoProyectadoBl.IndicadorIngresoTotal(concepto);
        }

        public List<ServicioConceptoProyectadoDtoResponse> ListarConceptos(ServicioConceptoProyectadoDtoRequest concepto)
        {
            return iServicioConceptoProyectadoBl.ListarConceptos(concepto);
        }

        public ServicioConceptoProyectadoDtoResponse ObtenerConceptoPersonalizado(ServicioConceptoProyectadoDtoRequest concepto)
        {
            return iServicioConceptoProyectadoBl.ObtenerConceptoPersonalizado(concepto);
        }

        public ProcesoResponse RegistrarConceptoPersonalizado(ServicioConceptoProyectadoDtoRequest concepto)
        {
            return iServicioConceptoProyectadoBl.RegistrarConceptoPersonalizado(concepto);
        }
    }
}
