﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse RegistrarOportunidadDocumento(OportunidadDocumentoDtoRequest oportunidadDocumento)
        {
            return iOportunidadDocumentoBl.RegistrarOportunidadDocumento(oportunidadDocumento);
        }
        public List<ListaDtoResponse> ListarOportunidadDocumento(OportunidadDocumentoDtoRequest oportunidadDocumento)
        {
            return iOportunidadDocumentoBl.ListarOportunidadDocumento(oportunidadDocumento);
        }
        public OportunidadDocumentoDtoResponse ObtenerOportunidadDocumento(OportunidadDocumentoDtoRequest oportunidadDocumento)
        {
            return iOportunidadDocumentoBl.ObtenerOportunidadDocumento(oportunidadDocumento);
        }
        public ProcesoResponse ActualizarOportunidadDocumento(OportunidadDocumentoDtoRequest oportunidadDocumento)
        {
            return iOportunidadDocumentoBl.ActualizarOportunidadDocumento(oportunidadDocumento);
        }
 

    }
}
