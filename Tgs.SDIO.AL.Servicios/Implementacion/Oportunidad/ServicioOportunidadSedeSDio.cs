﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public SedePaginadoDtoResponse ListarSedeInstalacion(OportunidadDtoRequest request)
        {
            return iSedeBl.ListarSedeInstalacion(request);
        }
        public SedePaginadoDtoResponse ListarSedeCliente(OportunidadDtoRequest request)
        {
            return iSedeBl.ListarSedeCliente(request);
        }
        public ClienteDtoResponse ObtenerClientePorIdOportunidad(OportunidadDtoRequest request)
        {
            return iSedeBl.ObtenerClientePorIdOportunidad(request);
        }
        public SedeDtoResponse ObtenerSedePorId(SedeDtoRequest request)
        {
            return iSedeBl.ObtenerSedePorId(request);
        }
        public ProcesoResponse RegistrarSede(SedeDtoRequest request)
        {
            return iSedeBl.RegistrarSede(request);
        }
        public ProcesoResponse AgregarSedeInstalacion(SedeDtoRequest request)
        {
            return iSedeBl.AgregarSedeInstalacion(request);
        }
        public SedeInstalacionDtoResponse obtenerSedeInstalada(int IdSede, int IdOportunidad)
        {
            return iSedeBl.obtenerSedeInstalada(IdSede, IdOportunidad);
        }
        public ProcesoResponse EliminarSede(SedeDtoRequest request)
        {
            return iSedeBl.EliminarSede(request);
        }
        public ProcesoResponse ActualizarSede(SedeDtoRequest request)
        {
            return iSedeBl.ActualizarSede(request);
        }
        public UbigeoDtoResponse ObtenerCodDetalleUbigeo(int IdUbigeo)
        {
            return iSedeBl.ObtenerCodDetalleUbigeo(IdUbigeo);
        }
        public ProcesoResponse EliminarServicios(ServicioSedeInstalacionDtoRequest request)
        {
            return iSedeBl.EliminarServicios(request);
        }
        public SedePaginadoDtoResponse ListarSedeServiciosPaginado(SedeDtoRequest request)
        {
            return iSedeBl.ListarSedeServiciosPaginado(request);
        }
        public SedeDtoResponse DireccionBuscar(SedeDtoRequest request)
        {
            return iSedeBl.DireccionBuscar(request);
        }
        public SedeDtoResponse DetalleSedePorId(SedeDtoRequest request)
        {
            return iSedeBl.DetalleSedePorId(request);
        }
    }
}
