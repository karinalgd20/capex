﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse RegistrarVersionOportunidad(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadBl.OportunidadInactivar(oportunidad);
        }
        public ProcesoResponse OportunidadInactivar(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadBl.OportunidadInactivar(oportunidad);
        }
        public ProcesoResponse OportunidadGanador(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadBl.OportunidadGanador(oportunidad);
        }
        public OportunidadDtoResponse ObtenerOportunidad(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadBl.ObtenerOportunidad(oportunidad);
        }
        public ProcesoResponse RegistrarOportunidad(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadBl.RegistrarOportunidad(oportunidad);
        }

        public OportunidadPaginadoDtoResponse ListarOportunidadCabecera(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadBl.ListarOportunidadCabecera(oportunidad);
        }
        public ProcesoResponse ActualizarOportunidad(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadBl.ActualizarOportunidad(oportunidad);
        }

        public List<OportunidadDtoResponse> ObtenerVersiones(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadBl.ObtenerVersiones(oportunidad);
        }
        public List<OportunidadDtoResponse> ListarProyectadoOIBDA(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadBl.ListarProyectadoOIBDA(oportunidad);
        }
        
           public ProcesoResponse NuevaVersion(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadBl.NuevaVersion(oportunidad);
        }

    }
}
