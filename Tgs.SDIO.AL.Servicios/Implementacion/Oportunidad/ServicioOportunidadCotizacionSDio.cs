﻿using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {

        public CotizacionPaginadoDtoResponse ListarCotizacionPaginado(CotizacionDtoRequest Cotizacion)
        {
            return iCotizacionBl.ListarCotizacionPaginado(Cotizacion);

        }
    }
}
