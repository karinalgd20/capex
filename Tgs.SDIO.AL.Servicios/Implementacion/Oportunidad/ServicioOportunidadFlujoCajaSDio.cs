﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse RegistrarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.RegistrarOportunidadFlujoCaja(flujocaja);
        }

        public ProcesoResponse ActualizarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ActualizarOportunidadFlujoCaja(flujocaja);
        }

        public OportunidadFlujoCajaDtoResponse ObtenerOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ObtenerOportunidadFlujoCaja(flujocaja);
        }

        public List<OportunidadFlujoCajaDtoResponse> ListaOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ListaOportunidadFlujoCaja(flujocaja);
        }

        public List<OportunidadFlujoCajaDtoResponse> ListarOportunidadFlujoCajaBandeja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ListarOportunidadFlujoCajaBandeja(flujocaja);
        }

        public List<ListaDtoResponse> ListaAnoMesProyecto(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ListaAnoMesProyecto(flujocaja);
        }
        public List<OportunidadFlujoCajaDtoResponse> GeneraCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.GeneraCasoNegocio(casonegocio);
        }

        public ProcesoResponse EliminarCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.EliminarCasoNegocio(casonegocio);
        }

        public List<OportunidadFlujoCajaDtoResponse> GeneraServicio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.GeneraServicio(casonegocio);
        }

        public OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.ListarDetalleOportunidadCaratula(casonegocio);
        }

        public OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadCaratulaTotal(OportunidadFlujoCajaDtoRequest casonegocio)

        {
            return iOportunidadFlujoCajaBl.ListarDetalleOportunidadCaratulaTotal(casonegocio);
        }
        public OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadEcapexTotal(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.ListarDetalleOportunidadEcapexTotal(casonegocio);
        }

        public OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.ListarDetalleOportunidadEcapex(casonegocio);
        }

        public OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.ObtenerDetalleOportunidadEcapex(casonegocio);
        }

        public OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.ObtenerDetalleOportunidadCaratula(casonegocio);
        }

        public ProcesoResponse ActualizarPeriodoOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ActualizarPeriodoOportunidadFlujoCaja(flujocaja);
        }
        
        public OportunidadFlujoCajaPaginadoDtoResponse ListarSubservicioPaginado(OportunidadFlujoCajaDtoRequest oportunidad)
        {
            return iOportunidadFlujoCajaBl.ListarSubservicioPaginado(oportunidad);
        }

        public OportunidadFlujoCajaDtoResponse ObtenerSubServicio(OportunidadFlujoCajaDtoRequest oportunidad)
        {
            return iOportunidadFlujoCajaBl.ObtenerSubServicio(oportunidad);
        }
        public ProcesoResponse InhabilitarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.InhabilitarOportunidadFlujoCaja(flujocaja);
        }

        public ProcesoResponse InhabilitarOportunidadEcapex(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.InhabilitarOportunidadEcapex(flujocaja);
        }
        public OportunidadFlujoCajaPaginadoDtoResponse ListarServiciosCircuitosPaginado(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ListarServiciosCircuitosPaginado(flujocaja);
        }
         public List<ListaDtoResponse> ListaServicioPorOportunidadLineaNegocio(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ListaServicioPorOportunidadLineaNegocio(flujocaja);
        }

        public  ProcesoResponse RegistrarComponente(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.RegistrarComponente(flujocaja);
        }


        public List<ListaDtoResponse> ListarPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ListarPorIdFlujoCaja(flujocaja);
        }

        public ProcesoResponse RegistrarServicioComponente(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            return iOportunidadFlujoCajaBl.RegistrarServicioComponente(servicioSubServicio);
        }

        public ProcesoResponse RegistrarCasoNegocioServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            return iOportunidadFlujoCajaBl.RegistrarCasoNegocioServicio(servicioSubServicio);
        }
        public OportunidadFlujoCajaDtoResponse ObtenerServicioPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ObtenerServicioPorIdFlujoCaja(flujocaja);
        }
    }
}
