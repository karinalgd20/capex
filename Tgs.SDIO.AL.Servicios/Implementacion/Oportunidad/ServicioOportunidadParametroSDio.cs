﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse RegistrarOportunidadParametro(OportunidadParametroDtoRequest oportunidadParametro)
        {
            return iOportunidadParametroBl.RegistrarOportunidadParametro(oportunidadParametro);
        }
        public OportunidadParametroPaginadoDtoResponse ListaOportunidadParametroPaginado(OportunidadParametroDtoRequest oportunidadParametro)
        {
            return iOportunidadParametroBl.ListaOportunidadParametroPaginado(oportunidadParametro);
        }

        public ProcesoResponse ActualizarOportunidadParametro(OportunidadParametroDtoRequest oportunidadParametro)
        {
            return iOportunidadParametroBl.ActualizarOportunidadParametro(oportunidadParametro);
        }
        public OportunidadParametroDtoResponse ObtenerOportunidadParametro(OportunidadParametroDtoRequest oportunidadParametro)
        {
            return iOportunidadParametroBl.ObtenerOportunidadParametro(oportunidadParametro);
        }
    }
}
