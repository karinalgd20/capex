﻿using System;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {

        public OportunidadFlujoCajaDetalleDtoResponse GeneraProyectadoOportunidadFlujoCaja(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaDetalleBl.GeneraProyectadoOportunidadFlujoCaja(flujocaja);
        }

        public OportunidadFlujoCajaDetalleDtoResponse ObtenerOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {
            throw new NotImplementedException();
        }

        public ProcesoResponse RegistrarOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalle)
        {
            throw new NotImplementedException();
        }
    }
}
