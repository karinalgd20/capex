﻿using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public SituacionActualDtoResponse ObtenerSituacionActualPorIdOportunidad(SituacionActualDtoRequest situacionActual)
        {
            return iSituacionActualBl.ObtenerPorIdOportunidad(situacionActual);
        }
        public ProcesoResponse RegistrarSituacionActual(SituacionActualDtoRequest situacionActual)
        {
            return iSituacionActualBl.RegistrarSituacionActual(situacionActual);
        }
    }
}