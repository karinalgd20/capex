﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse ActualizarConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex)
        {
            return iConceptoDatosCapexBl.ActualizarConceptoDatoCapex(capex);
        }

        public List<ConceptoDatosCapexDtoResponse> ConceptosDatoCapex(ConceptoDatosCapexDtoRequest capex)
        {
            return iConceptoDatosCapexBl.ConceptosDatoCapex(capex);
        }

        public List<ConceptoDatosCapexDtoResponse> ListarConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex)
        {
            return iConceptoDatosCapexBl.ListarConceptoDatoCapex(capex);
        }

        public ConceptoDatosCapexDtoResponse ObtenerConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex)
        {
            return iConceptoDatosCapexBl.ObtenerConceptoDatoCapex(capex);
        }

        public ProcesoResponse RegistrarConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex)
        {
            return iConceptoDatosCapexBl.RegistrarConceptoDatoCapex(capex);
        }
    }
}
