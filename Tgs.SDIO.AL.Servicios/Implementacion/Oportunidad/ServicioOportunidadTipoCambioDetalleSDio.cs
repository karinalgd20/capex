﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse RegistrarOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest oportunidadTipoCambioDetalle)
        {
            return iOportunidadTipoCambioDetalleBl.RegistrarOportunidadTipoCambioDetalle(oportunidadTipoCambioDetalle);
        }
        public OportunidadTipoCambioDetalleDtoResponse ObtenerOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest oportunidadTipoCambioDetalle)
        {
            return iOportunidadTipoCambioDetalleBl.ObtenerOportunidadTipoCambioDetalle(oportunidadTipoCambioDetalle);
        }
        public ProcesoResponse ActualizarOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest oportunidadTipoCambioDetalle)
        {
            return iOportunidadTipoCambioDetalleBl.ActualizarOportunidadTipoCambioDetalle(oportunidadTipoCambioDetalle);
        }

    }
}
