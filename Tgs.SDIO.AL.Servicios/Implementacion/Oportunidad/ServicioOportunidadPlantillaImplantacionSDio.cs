﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {
        public ProcesoResponse RegistrarPlantillaImplantacion(PlantillaImplantacionDtoRequest request)
        {
            return iPlantillaImplantacionBl.RegistrarPlantillaImplantacion(request);
        }
        public ProcesoResponse InactivarPlantillaImplantacion(PlantillaImplantacionDtoRequest request)
        {
            return iPlantillaImplantacionBl.InactivarPlantillaImplantacion(request);
        }
        public PlantillaImplantacionPaginadoDtoResponse ListarPlantillaImplantacionPaginado(PlantillaImplantacionDtoRequest filtro)
        {
            return iPlantillaImplantacionBl.ListarPlantillaImplantacionPaginado(filtro);
        }
    }
}
