﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<ListaDtoResponse> ListarCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio)
        {

            return iCapexLineaNegocioBl.ListarCapexLineaNegocio(CapexLineaNegocio);
        }

        public CapexLineaNegocioDtoResponse ObtenerCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio)
        {

            return iCapexLineaNegocioBl.ObtenerCapexLineaNegocio(CapexLineaNegocio);
        }

        public ProcesoResponse RegistrarCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio)
        {

            return iCapexLineaNegocioBl.RegistrarCapexLineaNegocio(CapexLineaNegocio);
        }
        public ProcesoResponse ActualizarCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio)
        {

            return iCapexLineaNegocioBl.ActualizarCapexLineaNegocio(CapexLineaNegocio);
        }
        public ProcesoResponse InhabilitarCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio)
        {

            return iCapexLineaNegocioBl.InhabilitarCapexLineaNegocio(CapexLineaNegocio);
        }

        public CapexLineaNegocioPaginadoDtoResponse ListarLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocio)
        {
            return iCapexLineaNegocioBl.ListarLineaNegocio(capexLineaNegocio);
        }

        public CapexLineaNegocioDtoResponse ObtenerCapexLineaNegocioPorIdSolicitud(CapexLineaNegocioDtoRequest capexLineaNegocio)
        {
            return iCapexLineaNegocioBl.ObtenerCapexLineaNegocioPorIdSolicitud(capexLineaNegocio);
        }
    }
}
