﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<SeccionDtoResponse> ListarBandejaSeccion(SeccionDtoRequest Seccion)
        {
            return iSeccionBl.ListarBandejaSeccion(Seccion);
        }

        public List<SeccionDtoResponse> ListarSeccion(SeccionDtoRequest Seccion)
        {
            return iSeccionBl.ListarSeccion(Seccion);
        }

        public SeccionDtoResponse ObtenerSeccion(SeccionDtoRequest Seccion)
        {
            return iSeccionBl.ObtenerSeccion(Seccion);
        }

        public ProcesoResponse RegistrarSeccion(SeccionDtoRequest Seccion)
        {
            return iSeccionBl.RegistrarSeccion(Seccion);
        }

        public ProcesoResponse ActualizarSeccion(SeccionDtoRequest Seccion)
        {
            return iSeccionBl.ActualizarSeccion(Seccion);
        }

        public ProcesoResponse ActualizarSeccionObservacion(SeccionDtoRequest seccion)
        {
            return iSeccionBl.ActualizarSeccionObservacion(seccion);
        }
    }
}
