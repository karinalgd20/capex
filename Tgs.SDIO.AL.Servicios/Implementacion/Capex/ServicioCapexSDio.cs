﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.Servicios.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioCapexSDio", Name = "ServicioCapexSDio",
        ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioCapexSDio : IServicioCapexSDio
    {
        public readonly ISolicitudBl iSolicitudBl;
        public readonly IAsignacionCapexTotalBl iAsignacionCapexTotalBl;
        public readonly IAsignacionCapexTotalDetalleBl iAsignacionCapexTotalDetalleBl;
        public readonly ICapexLineaNegocioBl iCapexLineaNegocioBl;
        public readonly IConceptosCapexBl iConceptosCapexBl;
        public readonly ICronogramaBl iCronogramaBl;
        public readonly ICronogramaDetalleBl iCronogramaDetalleBl;
        public readonly IDiagramaBl iDiagramaBl;
        public readonly IDiagramaDetalleBl iDiagramaDetalleBl;
        public readonly IEstructuraCostoBl iEstructuraCostoBl;
        public readonly IEstructuraCostoGrupoBl iEstructuraCostoGrupoBl;
        public readonly IEstructuraCostoGrupoDetalleBl iEstructuraCostoGrupoDetalleBl;
        public readonly IGrupoBl iGrupoBl;
        public readonly IObservacionBl iObservacionBl;
        public readonly ISeccionBl iSeccionBl;
        public readonly ISolicitudCapexBl iSolicitudCapexBl;
        public readonly ISolicitudCapexFlujoEstadoBl iSolicitudCapexFlujoEstadoBl;
        public readonly ITopologiaBl iTopologiaBl;
        public ServicioCapexSDio(ISolicitudBl ISolicitudBl,
                                IAsignacionCapexTotalBl IAsignacionCapexTotalBl,
                                IAsignacionCapexTotalDetalleBl IAsignacionCapexTotalDetalleBl,
                                ICapexLineaNegocioBl ICapexLineaNegocioBl,
                                IConceptosCapexBl IConceptosCapexBl,
                                ICronogramaBl ICronogramaBl,
                                ICronogramaDetalleBl ICronogramaDetalleBl,
                                IDiagramaBl IDiagramaBl,
                                IDiagramaDetalleBl IDiagramaDetalleBl,
                                IEstructuraCostoBl IEstructuraCostoBl,
                                 IEstructuraCostoGrupoBl IEstructuraCostoGrupoBl,
                                 IEstructuraCostoGrupoDetalleBl IEstructuraCostoGrupoDetalleBl,
                                 IGrupoBl IGrupoBl,
                                 IObservacionBl IObservacionBl,
                                 ISeccionBl ISeccionBl,
                                 ISolicitudCapexBl ISolicitudCapexBl,
                                 ISolicitudCapexFlujoEstadoBl ISolicitudCapexFlujoEstadoBl,
                                 ITopologiaBl ITopologiaBl)
        {

            iSolicitudBl = ISolicitudBl;
            iAsignacionCapexTotalBl = IAsignacionCapexTotalBl;
            iAsignacionCapexTotalDetalleBl = IAsignacionCapexTotalDetalleBl;
            iCapexLineaNegocioBl = ICapexLineaNegocioBl;
            iConceptosCapexBl = IConceptosCapexBl;
            iCronogramaBl = ICronogramaBl;
            iCronogramaDetalleBl = ICronogramaDetalleBl;
            iDiagramaBl = IDiagramaBl;
            iDiagramaDetalleBl = IDiagramaDetalleBl;
            iEstructuraCostoBl = IEstructuraCostoBl;
            iEstructuraCostoGrupoBl = IEstructuraCostoGrupoBl;
            iEstructuraCostoGrupoDetalleBl = IEstructuraCostoGrupoDetalleBl;
            iGrupoBl = IGrupoBl;
            iObservacionBl = IObservacionBl;
            iSeccionBl = ISeccionBl;
            iSolicitudCapexBl = ISolicitudCapexBl;
            iSolicitudCapexFlujoEstadoBl = ISolicitudCapexFlujoEstadoBl;
            iTopologiaBl = ITopologiaBl;
        }

    }

}