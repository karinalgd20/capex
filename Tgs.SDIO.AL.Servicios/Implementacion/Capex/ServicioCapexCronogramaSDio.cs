﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<ListaDtoResponse> ListarCronograma(CronogramaDtoRequest Cronograma){

            return iCronogramaBl.ListarCronograma(Cronograma);
        }

        public CronogramaDtoResponse ObtenerCronograma(CronogramaDtoRequest Cronograma)
        {

            return iCronogramaBl.ObtenerCronograma(Cronograma);
        }

        public ProcesoResponse RegistrarCronograma(CronogramaDtoRequest Cronograma)
        {

            return iCronogramaBl.RegistrarCronograma(Cronograma);
        }
        public ProcesoResponse ActualizarCronograma(CronogramaDtoRequest Cronograma)
        {

            return iCronogramaBl.ActualizarCronograma(Cronograma);
        }
        public ProcesoResponse InhabilitarCronograma(CronogramaDtoRequest Cronograma)
        {

            return iCronogramaBl.InhabilitarCronograma(Cronograma);
        }

     
    }
}
