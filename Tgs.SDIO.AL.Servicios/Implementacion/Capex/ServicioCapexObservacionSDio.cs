﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<ObservacionDtoResponse> ListarBandejaObservacion(ObservacionDtoRequest Observacion)
        {
            return iObservacionBl.ListarBandejaObservacion(Observacion);
        }

        public List<ObservacionDtoResponse> ListarObservacion(ObservacionDtoRequest Observacion)
        {
            return iObservacionBl.ListarObservacion(Observacion);
        }

        public ObservacionDtoResponse ObtenerObservacion(ObservacionDtoRequest Observacion)
        {
            return iObservacionBl.ObtenerObservacion(Observacion);
        }

        public ProcesoResponse RegistrarObservacion(ObservacionDtoRequest Observacion)
        {
            return iObservacionBl.RegistrarObservacion(Observacion);
        }

        public ProcesoResponse ActualizarObservacion(ObservacionDtoRequest Observacion)
        {
            return iObservacionBl.ActualizarObservacion(Observacion);
        }

    }
}
