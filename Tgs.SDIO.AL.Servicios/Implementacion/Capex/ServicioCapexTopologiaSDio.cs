﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<TopologiaDtoResponse> ListarBandejaTopologia(TopologiaDtoRequest Topologia)
        {
            return iTopologiaBl.ListarBandejaTopologia(Topologia);
        }

        public List<TopologiaDtoResponse> ListarTopologia(TopologiaDtoRequest Topologia)
        {
            return iTopologiaBl.ListarTopologia(Topologia);
        }

        public TopologiaDtoResponse ObtenerTopologia(TopologiaDtoRequest Topologia)
        {
            return iTopologiaBl.ObtenerTopologia(Topologia);
        }

        public ProcesoResponse RegistrarTopologia(TopologiaDtoRequest Topologia)
        {
            return iTopologiaBl.RegistrarTopologia(Topologia);
        }

        public ProcesoResponse ActualizarTopologia(TopologiaDtoRequest Topologia)
        {
            return iTopologiaBl.ActualizarTopologia(Topologia);
        }
    }
}
