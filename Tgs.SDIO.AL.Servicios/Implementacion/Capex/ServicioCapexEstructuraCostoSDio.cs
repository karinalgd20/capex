﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.DataContracts.Dto.Response.Capex.EstructuraCostoDtoResponse;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<ListaDtoResponse> ListarEstructuraCosto(EstructuraCostoDtoRequest EstructuraCosto){

            return iEstructuraCostoBl.ListarEstructuraCosto(EstructuraCosto);
        }

    
        public ProcesoResponse RegistrarEstructuraCosto(EstructuraCostoDtoRequest EstructuraCosto)
        {

            return iEstructuraCostoBl.RegistrarEstructuraCosto(EstructuraCosto);
        }
        public ProcesoResponse ActualizarEstructuraCosto(EstructuraCostoDtoRequest EstructuraCosto)
        {

            return iEstructuraCostoBl.ActualizarEstructuraCosto(EstructuraCosto);
        }
        public ProcesoResponse InhabilitarEstructuraCosto(EstructuraCostoDtoRequest EstructuraCosto)
        {

            return iEstructuraCostoBl.InhabilitarEstructuraCosto(EstructuraCosto);
        }
    }
}
