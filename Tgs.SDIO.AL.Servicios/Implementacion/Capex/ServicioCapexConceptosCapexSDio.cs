﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<ListaDtoResponse> ListarConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex){

            return iConceptosCapexBl.ListarConceptosCapex(ConceptosCapex);
        }

        public ConceptosCapexDtoResponse ObtenerConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex)
        {

            return iConceptosCapexBl.ObtenerConceptosCapex(ConceptosCapex);
        }

        public ProcesoResponse RegistrarConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex)
        {

            return iConceptosCapexBl.RegistrarConceptosCapex(ConceptosCapex);
        }
        public ProcesoResponse ActualizarConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex)
        {

            return iConceptosCapexBl.ActualizarConceptosCapex(ConceptosCapex);
        }
        public ProcesoResponse InhabilitarConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex)
        {

            return iConceptosCapexBl.InhabilitarConceptosCapex(ConceptosCapex);
        }

     
    }
}
