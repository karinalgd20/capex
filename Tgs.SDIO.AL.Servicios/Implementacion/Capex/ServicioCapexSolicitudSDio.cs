﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {

        public ProcesoResponse ActualizarSolicitud(SolicitudDtoRequest solicitud)
        {
            return iSolicitudBl.ActualizarSolicitud(solicitud);
        }
        public List<ListaDtoResponse> ListarSolicitud(SolicitudDtoRequest solicitud)
        {
            return iSolicitudBl.ListarSolicitud(solicitud);
        }
        public ProcesoResponse InhabilitarSolicitud(SolicitudDtoRequest solicitud)
        {
            return iSolicitudBl.InhabilitarSolicitud(solicitud);
        }
        
        public SolicitudDtoResponse ObtenerSolicitud(SolicitudDtoRequest solicitud)
        {
            return iSolicitudBl.ObtenerSolicitud(solicitud);
        }

        public ProcesoResponse RegistrarSolicitud(SolicitudDtoRequest solicitud)
        {
            return iSolicitudBl.RegistrarSolicitud(solicitud);
        }


    }
}
