﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<ListaDtoResponse> ListarDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle){

            return iDiagramaDetalleBl.ListarDiagramaDetalle(DiagramaDetalle);
        }

        public DiagramaDetalleDtoResponse ObtenerDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle)
        {

            return iDiagramaDetalleBl.ObtenerDiagramaDetalle(DiagramaDetalle);
        }

        public ProcesoResponse RegistrarDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle)
        {

            return iDiagramaDetalleBl.RegistrarDiagramaDetalle(DiagramaDetalle);
        }
        public ProcesoResponse ActualizarDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle)
        {

            return iDiagramaDetalleBl.ActualizarDiagramaDetalle(DiagramaDetalle);
        }
        public ProcesoResponse InhabilitarDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle)
        {

            return iDiagramaDetalleBl.InhabilitarDiagramaDetalle(DiagramaDetalle);
        }

     
    }
}
