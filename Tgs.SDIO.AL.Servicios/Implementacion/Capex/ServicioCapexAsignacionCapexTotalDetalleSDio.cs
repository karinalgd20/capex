﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<ListaDtoResponse> ListarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle){

            return iAsignacionCapexTotalDetalleBl.ListarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalle);
        }

        public AsignacionCapexTotalDetalleDtoResponse ObtenerAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle)
        {

            return iAsignacionCapexTotalDetalleBl.ObtenerAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalle);
        }

        public ProcesoResponse RegistrarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle)
        {

            return iAsignacionCapexTotalDetalleBl.RegistrarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalle);
        }
        public ProcesoResponse ActualizarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle)
        {

            return iAsignacionCapexTotalDetalleBl.ActualizarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalle);
        }
        public ProcesoResponse InhabilitarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle)
        {

            return iAsignacionCapexTotalDetalleBl.InhabilitarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalle);
        }

     
    }
}
