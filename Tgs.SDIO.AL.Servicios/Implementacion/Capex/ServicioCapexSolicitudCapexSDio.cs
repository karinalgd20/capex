﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<SolicitudCapexDtoResponse> ListarBandejaSolicitudCapex(SolicitudCapexDtoRequest SolicitudCapex)
        {
            return iSolicitudCapexBl.ListarBandejaSolicitudCapex(SolicitudCapex);
        }

        public List<SolicitudCapexDtoResponse> ListarSolicitudCapex(SolicitudCapexDtoRequest SolicitudCapex)
        {
            return iSolicitudCapexBl.ListarSolicitudCapex(SolicitudCapex);
        }

        public SolicitudCapexDtoResponse ObtenerSolicitudCapex(SolicitudCapexDtoRequest SolicitudCapex) {

            return iSolicitudCapexBl.ObtenerSolicitudCapex(SolicitudCapex);
        }

        public ProcesoResponse RegistrarSolicitudCapex(SolicitudCapexDtoRequest SolicitudCapex)
        {
            return iSolicitudCapexBl.RegistrarSolicitudCapex(SolicitudCapex);
        }

        public ProcesoResponse ActualizarSolicitudCapex(SolicitudCapexDtoRequest SolicitudCapex)
        {
            return iSolicitudCapexBl.ActualizarSolicitudCapex(SolicitudCapex);
        }

        public SolicitudCapexPaginadoDtoResponse ListaSolicitudCapexPaginado(SolicitudCapexDtoRequest SolicitudCapex)
        {
            return iSolicitudCapexBl.ListaSolicitudCapexPaginado(SolicitudCapex);
        }

        public ProcesoResponse ActualizarSolicitudCapexEstado(SolicitudCapexDtoRequest solicitudCapex)
        {
            return iSolicitudCapexBl.ActualizarSolicitudCapexEstado(solicitudCapex);
        }
    }
}
