﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<ListaDtoResponse> ListarAsignacionCapexTotal(AsignacionCapexTotalDtoRequest asignacionCapexTotal){

            return iAsignacionCapexTotalBl.ListarAsignacionCapexTotal(asignacionCapexTotal);
        }

        public AsignacionCapexTotalDtoResponse ObtenerAsignacionCapexTotal(AsignacionCapexTotalDtoRequest asignacionCapexTotal)
        {

            return iAsignacionCapexTotalBl.ObtenerAsignacionCapexTotal(asignacionCapexTotal);
        }

        public ProcesoResponse RegistrarAsignacionCapexTotal(AsignacionCapexTotalDtoRequest asignacionCapexTotal)
        {

            return iAsignacionCapexTotalBl.RegistrarAsignacionCapexTotal(asignacionCapexTotal);
        }
        public ProcesoResponse ActualizarAsignacionCapexTotal(AsignacionCapexTotalDtoRequest asignacionCapexTotal)
        {

            return iAsignacionCapexTotalBl.ActualizarAsignacionCapexTotal(asignacionCapexTotal);
        }
        public ProcesoResponse InhabilitarAsignacionCapexTotal(AsignacionCapexTotalDtoRequest asignacionCapexTotal)
        {

            return iAsignacionCapexTotalBl.InhabilitarAsignacionCapexTotal(asignacionCapexTotal);
        }

     
    }
}
