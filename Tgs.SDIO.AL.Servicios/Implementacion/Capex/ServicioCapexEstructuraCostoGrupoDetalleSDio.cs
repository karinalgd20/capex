﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {

        public List<EstructuraCostoGrupoDetalleDtoResponse> ListarEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalleDtoRequest EstructuraCostoGrupoDetalle)
        {
            return iEstructuraCostoGrupoDetalleBl.ListarEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalle);
        }

        public EstructuraCostoGrupoDetalleDtoResponse ObtenerEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalleDtoRequest EstructuraCostoGrupoDetalle)
        {
            return iEstructuraCostoGrupoDetalleBl.ObtenerEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalle);
        }

        public ProcesoResponse RegistrarEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalleDtoRequest EstructuraCostoGrupoDetalle)
        {
            return iEstructuraCostoGrupoDetalleBl.RegistrarEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalle);
        }

        public ProcesoResponse ActualizarEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalleDtoRequest EstructuraCostoGrupoDetalle)
        {
            return iEstructuraCostoGrupoDetalleBl.ActualizarEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalle);
        }
    }
}
