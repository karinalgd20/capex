﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
      
        public List<EstructuraCostoGrupoDtoResponse> ListarEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest EstructuraCostoGrupo)
        {
            return iEstructuraCostoGrupoBl.ListarEstructuraCostoGrupo(EstructuraCostoGrupo);
        }

        public ProcesoResponse RegistrarEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest EstructuraCostoGrupo)
        {
            return iEstructuraCostoGrupoBl.RegistrarEstructuraCostoGrupo(EstructuraCostoGrupo);
        }

        public ProcesoResponse ActualizarEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest EstructuraCostoGrupo)
        {
            return iEstructuraCostoGrupoBl.ActualizarEstructuraCostoGrupo(EstructuraCostoGrupo);
        }
        public EstructuraCostoGrupoPaginadoDtoResponse ListaEstructuraCostoGrupoPaginado(EstructuraCostoGrupoDtoRequest estructuraCostoGrupo)
        {
            return iEstructuraCostoGrupoBl.ListaEstructuraCostoGrupoPaginado(estructuraCostoGrupo);
        }

        public EstructuraCostoGrupoDetalleDtoResponse ObtenerEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest estructuraCostoGrupo)
        {
            return iEstructuraCostoGrupoBl.ObtenerEstructuraCostoGrupo(estructuraCostoGrupo);
        }
    }
}
