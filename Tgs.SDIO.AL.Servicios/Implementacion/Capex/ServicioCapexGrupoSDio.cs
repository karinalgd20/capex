﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<GrupoDtoResponse> ListarBandejaGrupo(GrupoDtoRequest Grupo)
        {
            return iGrupoBl.ListarBandejaGrupo(Grupo);
        }

        public List<ListaDtoResponse> ListarGrupo(GrupoDtoRequest Grupo)
        {
            return iGrupoBl.ListarGrupo(Grupo);
        }

        public GrupoDtoResponse ObtenerGrupo(GrupoDtoRequest Grupo)
        {
            return iGrupoBl.ObtenerGrupo(Grupo);
        }

        public ProcesoResponse RegistrarGrupo(GrupoDtoRequest Grupo)
        {
            return iGrupoBl.RegistrarGrupo(Grupo);
        }

        public ProcesoResponse ActualizarGrupo(GrupoDtoRequest Grupo)
        {
            return iGrupoBl.ActualizarGrupo(Grupo);
        }
    }
}
