﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<SolicitudCapexFlujoEstadoDtoResponse> ListarBandejaSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado)
        {
            return iSolicitudCapexFlujoEstadoBl.ListarBandejaSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstado);
        }

        public List<SolicitudCapexFlujoEstadoDtoResponse> ListarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado)
        {
            return iSolicitudCapexFlujoEstadoBl.ListarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstado);
        }

        public SolicitudCapexFlujoEstadoDtoResponse ObtenerSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado)
        {
            return iSolicitudCapexFlujoEstadoBl.ObtenerSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstado);
        }

        public ProcesoResponse RegistrarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado)
        {
            return iSolicitudCapexFlujoEstadoBl.RegistrarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstado);
        }

        public ProcesoResponse ActualizarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado)
        {
            return iSolicitudCapexFlujoEstadoBl.ActualizarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstado);
        }
        public SolicitudCapexFlujoEstadoDtoResponse ObtenerSolicitudCapexFlujoEstadoId(SolicitudCapexFlujoEstadoDtoRequest solicitudCapexFlujoEstado)
        {
            return iSolicitudCapexFlujoEstadoBl.ObtenerSolicitudCapexFlujoEstadoId(solicitudCapexFlujoEstado);
        }
    }
}
