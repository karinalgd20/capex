﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<ListaDtoResponse> ListarDiagrama(DiagramaDtoRequest Diagrama){

            return iDiagramaBl.ListarDiagrama(Diagrama);
        }

        public DiagramaDtoResponse ObtenerDiagrama(DiagramaDtoRequest Diagrama)
        {

            return iDiagramaBl.ObtenerDiagrama(Diagrama);
        }

        public ProcesoResponse RegistrarDiagrama(DiagramaDtoRequest Diagrama)
        {

            return iDiagramaBl.RegistrarDiagrama(Diagrama);
        }
        public ProcesoResponse ActualizarDiagrama(DiagramaDtoRequest Diagrama)
        {

            return iDiagramaBl.ActualizarDiagrama(Diagrama);
        }
        public ProcesoResponse InhabilitarDiagrama(DiagramaDtoRequest Diagrama)
        {

            return iDiagramaBl.InhabilitarDiagrama(Diagrama);
        }

     
    }
}
