﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Capex
{
    public partial class ServicioCapexSDio
    {
        public List<ListaDtoResponse> ListarCronogramaDetalle(CronogramaDetalleDtoRequest CronogramaDetalle){

            return iCronogramaDetalleBl.ListarCronogramaDetalle(CronogramaDetalle);
        }

        public CronogramaDetalleDtoResponse ObtenerCronogramaDetalle(CronogramaDetalleDtoRequest CronogramaDetalle)
        {

            return iCronogramaDetalleBl.ObtenerCronogramaDetalle(CronogramaDetalle);
        }

        public ProcesoResponse RegistrarCronogramaDetalle(CronogramaDetalleDtoRequest CronogramaDetalle)
        {

            return iCronogramaDetalleBl.RegistrarCronogramaDetalle(CronogramaDetalle);
        }
        public ProcesoResponse ActualizarCronogramaDetalle(CronogramaDetalleDtoRequest CronogramaDetalle)
        {

            return iCronogramaDetalleBl.ActualizarCronogramaDetalle(CronogramaDetalle);
        }
        public ProcesoResponse InhabilitarCronogramaDetalle(CronogramaDetalleDtoRequest CronogramaDetalle)
        {

            return iCronogramaDetalleBl.InhabilitarCronogramaDetalle(CronogramaDetalle);
        }

     
    }
}
