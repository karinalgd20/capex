﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Funnel
{
    public partial class ServicioFunnelSDio
    {
        public IndicadorLineasNegocioDtoResponse ListarLineasNegocio(IndicadorLineasNegocioDtoRequest filtro)
        {
           return iOportunidadFinancieraOrigenLineaNegocioBl.ListarLineasNegocio(filtro);
        }
    }
}
