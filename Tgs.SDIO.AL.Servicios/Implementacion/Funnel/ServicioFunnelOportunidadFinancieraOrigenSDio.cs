﻿using System;
using System.Collections.Generic;
using System.Linq;
using NPOI.SS.UserModel;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;
using TiposMadurez = Tgs.SDIO.Util.Constantes.Generales.TiposMadurez;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using System.IO;
using static Tgs.SDIO.Util.Constantes.Generales;
using ExportacionMadurez = Tgs.SDIO.Util.Constantes.Funnel.ExportacionMadurez;
using Extensiones = Tgs.SDIO.Util.Constantes.Funnel.Extensiones;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Funnel
{
    public partial class ServicioFunnelSDio
    {
        public MemoryStream ListarIndicadorOfertasAnio(IndicadorMadurezDtoRequest request)
        {
            var indicadorMadurez = FormatearIndicadorMadurez(request);
            var excelMadurez = ExportarIndicadorMadurez(Extensiones.xlsx, indicadorMadurez, request);
            return excelMadurez;
        }

        private IndicadorMadurezConsolidadoDtoResponse FormatearIndicadorMadurez(IndicadorMadurezDtoRequest request)
        {
            var indicadores = iOportunidadFinancieraOrigenBl.ListarIndicadorMadurez(request);
            var oportunidadesTrabajadas = indicadores.ListaMadurezCostos
                                              .GroupBy(c => c.LineaNegocio)
                                              .Select(w => new IndicadorMadurezCostoOportunidadesTrabajadasDtoResponse()
                                              {
                                                  LineaNegocio = w.Key,
                                                  OportunidadesTrabajadasN1 = w.Where(x => x.Madurez == TiposMadurez.N1).Sum(x => x.Oportunidades_Trabajadas.Value),
                                                  OportunidadesTrabajadasN2 = w.Where(x => x.Madurez == TiposMadurez.N2).Sum(x => x.Oportunidades_Trabajadas.Value),
                                                  OportunidadesTrabajadasN3 = w.Where(x => x.Madurez == TiposMadurez.N3).Sum(x => x.Oportunidades_Trabajadas.Value),
                                                  OportunidadesTrabajadasN4 = w.Where(x => x.Madurez == TiposMadurez.N4).Sum(x => x.Oportunidades_Trabajadas.Value),
                                                  OportunidadesTrabajadasN5 = w.Where(x => x.Madurez == TiposMadurez.N5).Sum(x => x.Oportunidades_Trabajadas.Value)
                                              });

            var ingresos = indicadores.ListaMadurezCostos
                                             .GroupBy(c => c.LineaNegocio)
                                             .Select(w => new IndicadorMadurezCostoIngresosDtoResponse()
                                             {
                                                 LineaNegocio = w.Key,
                                                 IngresosN1 = w.Where(x => x.Madurez == TiposMadurez.N1).Sum(x => x.IngresoTotal.Value),
                                                 IngresosN2 = w.Where(x => x.Madurez == TiposMadurez.N2).Sum(x => x.IngresoTotal.Value),
                                                 IngresosN3 = w.Where(x => x.Madurez == TiposMadurez.N3).Sum(x => x.IngresoTotal.Value),
                                                 IngresosN4 = w.Where(x => x.Madurez == TiposMadurez.N4).Sum(x => x.IngresoTotal.Value),
                                                 IngresosN5 = w.Where(x => x.Madurez == TiposMadurez.N5).Sum(x => x.IngresoTotal.Value)
                                             });

            var capex = indicadores.ListaMadurezCostos
                                             .GroupBy(c => c.LineaNegocio)
                                             .Select(w => new IndicadorMadurezCapexDtoResponse()
                                             {
                                                 LineaNegocio = w.Key,
                                                 Capex1 = w.Where(x => x.Madurez == TiposMadurez.N1).Sum(x => x.Capex.Value),
                                                 Capex2 = w.Where(x => x.Madurez == TiposMadurez.N2).Sum(x => x.Capex.Value),
                                                 Capex3 = w.Where(x => x.Madurez == TiposMadurez.N3).Sum(x => x.Capex.Value),
                                                 Capex4 = w.Where(x => x.Madurez == TiposMadurez.N4).Sum(x => x.Capex.Value),
                                                 Capex5 = w.Where(x => x.Madurez == TiposMadurez.N5).Sum(x => x.Capex.Value)
                                             });

            var opex = indicadores.ListaMadurezCostos
                                            .GroupBy(c => c.LineaNegocio)
                                            .Select(w => new IndicadorMadurezOpexDtoResponse()
                                            {
                                                LineaNegocio = w.Key,
                                                Opex1 = w.Where(x => x.Madurez == TiposMadurez.N1).Sum(x => x.Opex.Value),
                                                Opex2 = w.Where(x => x.Madurez == TiposMadurez.N2).Sum(x => x.Opex.Value),
                                                Opex3 = w.Where(x => x.Madurez == TiposMadurez.N3).Sum(x => x.Opex.Value),
                                                Opex4 = w.Where(x => x.Madurez == TiposMadurez.N4).Sum(x => x.Opex.Value),
                                                Opex5 = w.Where(x => x.Madurez == TiposMadurez.N5).Sum(x => x.Opex.Value)
                                            });

            var oidba = indicadores.ListaMadurezCostos
                                            .GroupBy(c => c.LineaNegocio)
                                            .Select(w => new IndicadorMadurezOidbaDtoResponse()
                                            {
                                                LineaNegocio = w.Key,
                                                Oidba1 = w.Where(x => x.Madurez == TiposMadurez.N1).Sum(x => x.Oibda.Value),
                                                Oidba2 = w.Where(x => x.Madurez == TiposMadurez.N2).Sum(x => x.Oibda.Value),
                                                Oidba3 = w.Where(x => x.Madurez == TiposMadurez.N3).Sum(x => x.Oibda.Value),
                                                Oidba4 = w.Where(x => x.Madurez == TiposMadurez.N4).Sum(x => x.Oibda.Value),
                                                Oidba5 = w.Where(x => x.Madurez == TiposMadurez.N5).Sum(x => x.Oibda.Value)
                                            });


            return new IndicadorMadurezConsolidadoDtoResponse
            {
                ListaMadurez = indicadores.ListaMadurez.OrderBy(w => w.MesOrden),
                ListaOportunidadesTrabajadas = oportunidadesTrabajadas,
                ListaIngresos = ingresos,
                ListaCapex = capex,
                ListaOpex = opex,
                ListaOidba = oidba
            };
        }

        private MemoryStream ExportarIndicadorMadurez(string extension,IndicadorMadurezConsolidadoDtoResponse madurez, 
                                                      IndicadorMadurezDtoRequest filtro)
        {
            var reporteMadurez = madurez.ListaMadurez.ToList();
            var oportunidadesTrabajadas = madurez.ListaOportunidadesTrabajadas.ToList();
            var ingresos = madurez.ListaIngresos.ToList();
            var capex = madurez.ListaCapex.ToList();
            var opex = madurez.ListaOpex.ToList();
            var oidba = madurez.ListaOidba.ToList();

            IWorkbook workbook = null;

            if (extension == Extensiones.xlsx)
            {
                workbook = new XSSFWorkbook();
            }
            else if (extension == Extensiones.xls)
            {
                workbook = new HSSFWorkbook();
            }

            var sheet1 = workbook.CreateSheet(ExportacionMadurez.NombreHojaExportacion);


            //Aplicando color a cabecera
            //FillBackgroundcolor cannot be applied without also specifying the FillPattern.

            XSSFCellStyle cabeceraStyle;

            XSSFFont fuenteCabecera = (XSSFFont)workbook.CreateFont();
            fuenteCabecera.Boldweight = (short)FontBoldWeight.Bold;

            cabeceraStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            cabeceraStyle.FillForegroundColor = IndexedColors.LightYellow.Index;
            cabeceraStyle.FillPattern = FillPattern.SolidForeground;
            cabeceraStyle.SetFont(fuenteCabecera);
            cabeceraStyle.Alignment = HorizontalAlignment.Center;
            cabeceraStyle.VerticalAlignment = VerticalAlignment.Center;
            cabeceraStyle.WrapText = true;

            //Fin de Aplicando color cabecera

            //Aplicando color a montos

            XSSFCellStyle montosStyle;

            XSSFDataFormat format = (XSSFDataFormat)workbook.CreateDataFormat();
            montosStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            montosStyle.Alignment = HorizontalAlignment.Right;
            montosStyle.DataFormat = format.GetFormat(ExportacionMadurez.FormatoExportacionMontos);

            //Fin de Aplicando color a montos


            //Inicializamos el ancho de las columnas solo si es la primera exportación de datos
            sheet1.SetColumnWidth(0, 30 * 256);
            sheet1.SetColumnWidth(1, 15 * 256);
            sheet1.SetColumnWidth(2, 30 * 256);
            sheet1.SetColumnWidth(3, 30 * 256);
            sheet1.SetColumnWidth(4, 80 * 256);
            sheet1.SetColumnWidth(5, 15 * 256);
            sheet1.SetColumnWidth(6, 15 * 256);
            sheet1.SetColumnWidth(7, 15 * 256);
            sheet1.SetColumnWidth(8, 15 * 256);
            sheet1.SetColumnWidth(9, 15 * 256);
            sheet1.SetColumnWidth(10, 15 * 256);

            //Exportando reporte Principal
            var numeroFilas = 0;

            if (reporteMadurez.Any())
            {
                numeroFilas = ExportarInformacionMadurez(sheet1, numeroFilas, cabeceraStyle,reporteMadurez);
            }

            if (oportunidadesTrabajadas.Any())
            {
                numeroFilas = ExportarDetalleOfertas(sheet1, numeroFilas,
                    TiposDetalleOfertaAnio.OportunidadesTrabajadas, cabeceraStyle, montosStyle,listaOportunidadesTrabajadas: oportunidadesTrabajadas);
            }

            if (ingresos.Any())
            {
                numeroFilas = ExportarDetalleOfertas(sheet1, numeroFilas,
                   TiposDetalleOfertaAnio.Ingresos, cabeceraStyle, montosStyle, listaIngresos: ingresos);
            }

            if (capex.Any())
            {
                numeroFilas = ExportarDetalleOfertas(sheet1, numeroFilas,
                   TiposDetalleOfertaAnio.Capex, cabeceraStyle, montosStyle, listaCapex: capex);
            }

            if (opex.Any())
            {
                numeroFilas = ExportarDetalleOfertas(sheet1, numeroFilas,
                   TiposDetalleOfertaAnio.Opex, cabeceraStyle, montosStyle, listaOpex: opex);
            }

            if (oidba.Any())
            {
                numeroFilas = ExportarDetalleOfertas(sheet1, numeroFilas,
                   TiposDetalleOfertaAnio.Oidba, cabeceraStyle, montosStyle, listaOidba: oidba);
            }

            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);

                return exportData;
            }                
        }

        private int ExportarInformacionMadurez(ISheet sheet1,int numeroFilas, ICellStyle cabeceraStyle,
            List<OportunidadFinancieraOrigenDtoResponse> reporteMadurez)
        {

            //Hacer una fila cabecera
            var rowMadurez = sheet1.CreateRow(numeroFilas);

            var cellMes = rowMadurez.CreateCell(0); 
            cellMes.SetCellValue(ExportacionMadurez.CabeceraMes);
            cellMes.CellStyle = cabeceraStyle;

            var cellIdOportunidad = rowMadurez.CreateCell(1);
            cellIdOportunidad.SetCellValue(ExportacionMadurez.CabeceraIdOportunidad);
            cellIdOportunidad.CellStyle = cabeceraStyle;

            var cellLineaNegocio = rowMadurez.CreateCell(2);
            cellLineaNegocio.SetCellValue(ExportacionMadurez.CabeceraLineaNegocio);
            cellLineaNegocio.CellStyle = cabeceraStyle;

            var cellSector = rowMadurez.CreateCell(3);
            cellSector.SetCellValue(ExportacionMadurez.CabeceraSector);
            cellSector.CellStyle = cabeceraStyle;

            var cellNombreCliente = rowMadurez.CreateCell(4);
            cellNombreCliente.SetCellValue(ExportacionMadurez.CabeceraNombreCliente);
            cellNombreCliente.CellStyle = cabeceraStyle;

            var cellCodigoCliente = rowMadurez.CreateCell(5);
            cellCodigoCliente.SetCellValue(ExportacionMadurez.CabeceraCodigoCliente);
            cellCodigoCliente.CellStyle = cabeceraStyle;

            var cellIngresoTotal = rowMadurez.CreateCell(6);
            cellIngresoTotal.SetCellValue(ExportacionMadurez.CabeceraIngresoTotal);
            cellIngresoTotal.CellStyle = cabeceraStyle;

            var cellCapex = rowMadurez.CreateCell(7);
            cellCapex.SetCellValue(ExportacionMadurez.CabeceraCapex);
            cellCapex.CellStyle = cabeceraStyle;

            var cellOpex = rowMadurez.CreateCell(8);
            cellOpex.SetCellValue(ExportacionMadurez.CabeceraOpex);
            cellOpex.CellStyle = cabeceraStyle;

            var cellOibda = rowMadurez.CreateCell(9);
            cellOibda.SetCellValue(ExportacionMadurez.CabeceraOibda);
            cellOibda.CellStyle = cabeceraStyle;

            var cellMadurezCadena = rowMadurez.CreateCell(10);
            cellMadurezCadena.SetCellValue(ExportacionMadurez.CabeceraMadurez);
            cellMadurezCadena.CellStyle = cabeceraStyle;

            numeroFilas++;


            //Recorrer la data
            for (int i = 0; i <= reporteMadurez.Count() - 1; i++)
            {
                var row = sheet1.CreateRow(i + 1);

                var cellMesData = row.CreateCell(0);
                cellMesData.SetCellValue(reporteMadurez[i].Mes);

                var cellIdOportunidadData = row.CreateCell(1);
                cellIdOportunidadData.SetCellValue(reporteMadurez[i].IdOportunidad);

                var cellLineaNegocioData = row.CreateCell(2);
                cellLineaNegocioData.SetCellValue(reporteMadurez[i].LineaNegocio);

                var cellSectorData = row.CreateCell(3);
                cellSectorData.SetCellValue(reporteMadurez[i].Sector);

                var cellNombreClienteData = row.CreateCell(4);
                cellNombreClienteData.SetCellValue(reporteMadurez[i].NombreCliente);

                var cellCodigoClienteData = row.CreateCell(5);
                cellCodigoClienteData.SetCellValue(reporteMadurez[i].CodigoCliente);

                var cellIngresoTotalData = row.CreateCell(6);
                cellIngresoTotalData.SetCellValue(Convert.ToDouble(reporteMadurez[i].IngresoTotal));

                var cellCapexData = row.CreateCell(7);
                cellCapexData.SetCellValue(Convert.ToDouble(reporteMadurez[i].Capex));

                var cellOpexData = row.CreateCell(8);
                cellOpexData.SetCellValue(Convert.ToDouble(reporteMadurez[i].Opex));

                var cellOibdaData = row.CreateCell(9);
                cellOibdaData.SetCellValue(Convert.ToDouble(reporteMadurez[i].Oibda));

                var cellMadurezCadenaData = row.CreateCell(10);
                cellMadurezCadenaData.SetCellValue(reporteMadurez[i].MadurezCadena);

                numeroFilas++;
            }

            return numeroFilas;
        }

        private int ExportarDetalleOfertas(ISheet sheet1, int numeroFilas, TiposDetalleOfertaAnio tiposDetalleOfertaAnio,
                ICellStyle cabeceraStyle, ICellStyle montosStyle,
                List<IndicadorMadurezCostoOportunidadesTrabajadasDtoResponse> listaOportunidadesTrabajadas = null,
                List<IndicadorMadurezCostoIngresosDtoResponse> listaIngresos = null,
                List<IndicadorMadurezCapexDtoResponse> listaCapex = null,
                List<IndicadorMadurezOpexDtoResponse> listaOpex = null,
                List<IndicadorMadurezOidbaDtoResponse> listaOidba = null
            )
        {
            //Hacer una fila cabecera
            var rowLinea = sheet1.CreateRow(numeroFilas + 1);
            var cellLineaNegocio = rowLinea.CreateCell(0);

            switch (tiposDetalleOfertaAnio)
            {
                case TiposDetalleOfertaAnio.OportunidadesTrabajadas:
                    cellLineaNegocio.SetCellValue(ExportacionMadurez.CabeceraDetalleOportunidades);
                    break;
                case TiposDetalleOfertaAnio.Ingresos:
                    cellLineaNegocio.SetCellValue(ExportacionMadurez.CabeceraDetalleIngresos);
                    break;
                case TiposDetalleOfertaAnio.Capex:
                    cellLineaNegocio.SetCellValue(ExportacionMadurez.CabeceraDetalleCapex);
                    break;
                case TiposDetalleOfertaAnio.Opex:
                    cellLineaNegocio.SetCellValue(ExportacionMadurez.CabeceraDetalleOpex);
                    break;
                case TiposDetalleOfertaAnio.Oidba:
                    cellLineaNegocio.SetCellValue(ExportacionMadurez.CabeceraDetalleOidba);
                    break;
            }

            cellLineaNegocio.CellStyle = cabeceraStyle;


            var cellN1 = rowLinea.CreateCell(1);
            cellN1.SetCellValue(ExportacionMadurez.CabeceraDetalleN1);
            cellN1.CellStyle = cabeceraStyle;

            var cellN2 = rowLinea.CreateCell(2);
            cellN2.SetCellValue(ExportacionMadurez.CabeceraDetalleN2);
            cellN2.CellStyle = cabeceraStyle;

            var cellN3 = rowLinea.CreateCell(3);
            cellN3.SetCellValue(ExportacionMadurez.CabeceraDetalleN3);
            cellN3.CellStyle = cabeceraStyle;

            var cellN4 = rowLinea.CreateCell(4);
            cellN4.SetCellValue(ExportacionMadurez.CabeceraDetalleN4);
            cellN4.CellStyle = cabeceraStyle;

            var cellN5 = rowLinea.CreateCell(5);
            cellN5.SetCellValue(ExportacionMadurez.CabeceraDetalleN5);
            cellN5.CellStyle = cabeceraStyle;

            numeroFilas++;

            //Recorrer la data

            switch (tiposDetalleOfertaAnio)
            {
                case TiposDetalleOfertaAnio.OportunidadesTrabajadas:
                    numeroFilas = ExportarDetalleOfertasOportunidades(sheet1, listaOportunidadesTrabajadas, numeroFilas);
                    break;
                case TiposDetalleOfertaAnio.Ingresos:
                    numeroFilas = ExportarDetalleOfertasIngresos(sheet1, listaIngresos, numeroFilas, montosStyle);
                    break;
                case TiposDetalleOfertaAnio.Capex:
                    numeroFilas = ExportarDetalleOfertasCapex(sheet1, listaCapex, numeroFilas, montosStyle);
                    break;
                case TiposDetalleOfertaAnio.Opex:
                    numeroFilas = ExportarDetalleOfertasOpex(sheet1, listaOpex, numeroFilas, montosStyle);
                    break;
                case TiposDetalleOfertaAnio.Oidba:
                    numeroFilas = ExportarDetalleOfertasOidba(sheet1, listaOidba, numeroFilas, montosStyle);
                    break;
            }

            numeroFilas++;

            return numeroFilas;
        }

        private int ExportarDetalleOfertasOportunidades(ISheet sheet1,
            List<IndicadorMadurezCostoOportunidadesTrabajadasDtoResponse> listaOportunidadesTrabajadas,int numeroFilas)
        {
            for (int i = 0; i <= listaOportunidadesTrabajadas.Count() - 1; i++)
            {
                var row = sheet1.CreateRow(numeroFilas + 1);

                var cellLineaNegocio = row.CreateCell(0);
                cellLineaNegocio.SetCellValue(listaOportunidadesTrabajadas[i].LineaNegocio);

                var cellN1 = row.CreateCell(1);
                cellN1.SetCellValue(listaOportunidadesTrabajadas[i].OportunidadesTrabajadasN1);

                var cellN2 = row.CreateCell(2);
                cellN2.SetCellValue(listaOportunidadesTrabajadas[i].OportunidadesTrabajadasN2);

                var cellN3 = row.CreateCell(3);
                cellN3.SetCellValue(listaOportunidadesTrabajadas[i].OportunidadesTrabajadasN3);

                var cellN4 = row.CreateCell(4);
                cellN4.SetCellValue(listaOportunidadesTrabajadas[i].OportunidadesTrabajadasN4);

                var cellN5 = row.CreateCell(5);
                cellN5.SetCellValue(listaOportunidadesTrabajadas[i].OportunidadesTrabajadasN5);

                numeroFilas++;
            }

            return numeroFilas;
        }

        private int ExportarDetalleOfertasIngresos(ISheet sheet1,
            List<IndicadorMadurezCostoIngresosDtoResponse> listaIngresos,int numeroFilas, ICellStyle montosStyle)
        {
            for (int i = 0; i <= listaIngresos.Count() - 1; i++)
            {
                var row = sheet1.CreateRow(numeroFilas + 1);

                var cellLineaNegocio = row.CreateCell(0);
                cellLineaNegocio.SetCellValue(listaIngresos[i].LineaNegocio);

                var cellN1 = row.CreateCell(1);
                cellN1.SetCellValue(Convert.ToDouble(listaIngresos[i].IngresosN1));
                cellN1.CellStyle = montosStyle;

                var cellN2 = row.CreateCell(2);
                cellN2.SetCellValue(Convert.ToDouble(listaIngresos[i].IngresosN2));
                cellN2.CellStyle = montosStyle;

                var cellN3 = row.CreateCell(3);
                cellN3.SetCellValue(Convert.ToDouble(listaIngresos[i].IngresosN3));
                cellN3.CellStyle = montosStyle;

                var cellN4 = row.CreateCell(4);
                cellN4.SetCellValue(Convert.ToDouble(listaIngresos[i].IngresosN4));
                cellN4.CellStyle = montosStyle;

                var cellN5 = row.CreateCell(5);
                cellN5.SetCellValue(Convert.ToDouble(listaIngresos[i].IngresosN5));
                cellN5.CellStyle = montosStyle;

                numeroFilas++;
            }

            return numeroFilas;
        }

        private int ExportarDetalleOfertasCapex(ISheet sheet1,
            List<IndicadorMadurezCapexDtoResponse> listaCapex,int numeroFilas, ICellStyle montosStyle)
        {
            for (int i = 0; i <= listaCapex.Count() - 1; i++)
            {
                var row = sheet1.CreateRow(numeroFilas + 1);

                var cellLineaNegocio = row.CreateCell(0);
                cellLineaNegocio.SetCellValue(listaCapex[i].LineaNegocio);

                var cellN1 = row.CreateCell(1);
                cellN1.SetCellValue(Convert.ToDouble(listaCapex[i].Capex1));
                cellN1.CellStyle = montosStyle;

                var cellN2 = row.CreateCell(2);
                cellN2.SetCellValue(Convert.ToDouble(listaCapex[i].Capex2));
                cellN2.CellStyle = montosStyle;

                var cellN3 = row.CreateCell(3);
                cellN3.SetCellValue(Convert.ToDouble(listaCapex[i].Capex3));
                cellN3.CellStyle = montosStyle;

                var cellN4 = row.CreateCell(4);
                cellN4.SetCellValue(Convert.ToDouble(listaCapex[i].Capex4));
                cellN4.CellStyle = montosStyle;

                var cellN5 = row.CreateCell(5);
                cellN5.SetCellValue(Convert.ToDouble(listaCapex[i].Capex5));
                cellN5.CellStyle = montosStyle;

                numeroFilas++;
            }

            return numeroFilas;
        }

        private int ExportarDetalleOfertasOpex(ISheet sheet1,
            List<IndicadorMadurezOpexDtoResponse> listaOpex,int numeroFilas, ICellStyle montosStyle)
        {
            for (int i = 0; i <= listaOpex.Count() - 1; i++)
            {
                var row = sheet1.CreateRow(numeroFilas + 1);

                var cellLineaNegocio = row.CreateCell(0);
                cellLineaNegocio.SetCellValue(listaOpex[i].LineaNegocio);

                var cellN1 = row.CreateCell(1);
                cellN1.SetCellValue(Convert.ToDouble(listaOpex[i].Opex1));
                cellN1.CellStyle = montosStyle;

                var cellN2 = row.CreateCell(2);
                cellN2.SetCellValue(Convert.ToDouble(listaOpex[i].Opex2));
                cellN2.CellStyle = montosStyle;

                var cellN3 = row.CreateCell(3);
                cellN3.SetCellValue(Convert.ToDouble(listaOpex[i].Opex3));
                cellN3.CellStyle = montosStyle;

                var cellN4 = row.CreateCell(4);
                cellN4.SetCellValue(Convert.ToDouble(listaOpex[i].Opex4));
                cellN4.CellStyle = montosStyle;

                var cellN5 = row.CreateCell(5);
                cellN5.SetCellValue(Convert.ToDouble(listaOpex[i].Opex5));
                cellN5.CellStyle = montosStyle;

                numeroFilas++;
            }

            return numeroFilas;
        }
        private int ExportarDetalleOfertasOidba(ISheet sheet1,
            List<IndicadorMadurezOidbaDtoResponse> listaOidba,int numeroFilas, ICellStyle montosStyle)
        {
            for (int i = 0; i <= listaOidba.Count() - 1; i++)
            {
                var row = sheet1.CreateRow(numeroFilas + 1);

                var cellLineaNegocio = row.CreateCell(0);
                cellLineaNegocio.SetCellValue(listaOidba[i].LineaNegocio);

                var cellN1 = row.CreateCell(1);
                cellN1.SetCellValue(Convert.ToDouble(listaOidba[i].Oidba1));
                cellN1.CellStyle = montosStyle;

                var cellN2 = row.CreateCell(2);
                cellN2.SetCellValue(Convert.ToDouble(listaOidba[i].Oidba2));
                cellN2.CellStyle = montosStyle;

                var cellN3 = row.CreateCell(3);
                cellN3.SetCellValue(Convert.ToDouble(listaOidba[i].Oidba3));
                cellN3.CellStyle = montosStyle;

                var cellN4 = row.CreateCell(4);
                cellN4.SetCellValue(Convert.ToDouble(listaOidba[i].Oidba4));
                cellN4.CellStyle = montosStyle;

                var cellN5 = row.CreateCell(5);
                cellN5.SetCellValue(Convert.ToDouble(listaOidba[i].Oidba5));
                cellN5.CellStyle = montosStyle;

                numeroFilas++;
            }

            return numeroFilas;
        }

        public DashoardDtoResponse MostrarDashboard(DashoardDtoRequest request)
        {
            var respuestaDashboard = iOportunidadFinancieraOrigenBl.MostrarDashboard(request);
            return respuestaDashboard;
        }

        public IndicadorDashboardPreventaConsolidadoDtoResponse ListarSeguimientoOportunidadesPreventaGerente(IndicadorDashboardPreventaConsolidadoDtoRequest request)
        {
            var oportunidades = iOportunidadFinancieraOrigenBl.ListarSeguimientoOportunidadesPreventaGerente(request.oportunidadesFiltro);

            return new IndicadorDashboardPreventaConsolidadoDtoResponse
            {
                ListaOportunidades = oportunidades.ListaOportunidades,
                Total = oportunidades.Total
            };
        }

        public IndicadorDashboardPreventaConsolidadoDtoResponse ListarSeguimientoOportunidadesPreventaLider(IndicadorDashboardPreventaConsolidadoDtoRequest request)
        {
            var oportunidades = iOportunidadFinancieraOrigenBl.ListarSeguimientoOportunidadesPreventaLider(request.oportunidadesFiltro);

            return new IndicadorDashboardPreventaConsolidadoDtoResponse
            {
                ListaOportunidades = oportunidades.ListaOportunidades,
                Total = oportunidades.Total
            };
        }

        public IndicadorDashboardPreventaConsolidadoDtoResponse ListarSeguimientoOportunidadesPreventaPreventa(IndicadorDashboardPreventaConsolidadoDtoRequest request)
        {
            var oportunidades = iOportunidadFinancieraOrigenBl.ListarSeguimientoOportunidadesPreventaPreventa(request.oportunidadesFiltro);

            return new IndicadorDashboardPreventaConsolidadoDtoResponse
            {
                ListaOportunidades = oportunidades.ListaOportunidades,
                Total = oportunidades.Total
            };

        }

        public IndicadorDashboardPreventaConsolidadoDtoResponse ListarSeguimientoOportunidadesSectorOportunidad(IndicadorDashboardSectorDtoRequest request)
        {
            var oportunidades = iOportunidadFinancieraOrigenBl.ListarSeguimientoOportunidadesSectorOportunidad(request);

            return new IndicadorDashboardPreventaConsolidadoDtoResponse
            {
                ListaOportunidades = oportunidades.ListaOportunidades,
                Total = oportunidades.Total
            };
        }

        public IndicadorRentabilidadConsolidadoDtoResponse ListarRentabilidadAnalistasFinancieros(IndicadorRentabilidadDtoRequest filtro)
        {
            var oportunidades = iOportunidadFinancieraOrigenBl.ListarRentabilidadAnalistasFinancieros(filtro);

            return new IndicadorRentabilidadConsolidadoDtoResponse
            {
                ListadoOportunidades = oportunidades.ListadoOportunidades,
                Total = oportunidades.Total
            };

        }

    }
}

