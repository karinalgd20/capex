﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Funnel;
using Tgs.SDIO.AL.Servicios.Interfaces.Funnel;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Funnel
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioFunnelSDio", Name = "ServicioFunnelSDio",
        ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioFunnelSDio : IServicioFunnelSDio
    {
        public readonly IOportunidadFinancieraOrigenBl iOportunidadFinancieraOrigenBl;
        public readonly IOportunidadFinancieraOrigenLineaNegocioBl iOportunidadFinancieraOrigenLineaNegocioBl;

        public ServicioFunnelSDio(IOportunidadFinancieraOrigenBl IOportunidadFinancieraOrigenBl,
               IOportunidadFinancieraOrigenLineaNegocioBl IOportunidadFinancieraOrigenLineaNegocioBl)
        {
            iOportunidadFinancieraOrigenBl = IOportunidadFinancieraOrigenBl;
            iOportunidadFinancieraOrigenLineaNegocioBl = IOportunidadFinancieraOrigenLineaNegocioBl;
        }

    }
}
