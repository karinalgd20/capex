﻿using System;
using System.ServiceModel;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.Servicios.Interfaces.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Trazabilidad
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioTrazabilidadSDio", Name = "ServicioTrazabilidadSDio",
    ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioTrazabilidadSDio : IServicioTrazabilidadSDio
    {
        private readonly IObservacionOportunidadBl iobservacionOportunidadBl;
        private readonly IRecursoOportunidadBl irecursoOportunidadBl;
        private readonly IOportunidadSTBl ioportunidadSTBl;
        private readonly ICasoOportunidadBl icasoOportunidadBl;
        private readonly IDatosPreventaMovilBl idatosPreventaMovilBl;
        private readonly IEstadoCasoBl iestadoCasoBl;
        private readonly ITipoOportunidadBl itipoOportunidadBl;
        private readonly IMotivoOportunidadBl imotivoOportunidadBl;
        private readonly IFuncionPropietarioBl ifuncionPropietarioBl;
        private readonly ITiempoAtencionEquipoBl itiempoAtencionEquipoBl;
        private readonly IEtapaOportunidadBl iEtapaOportunidadBl;
        private readonly ITipoSolicitudBl iTipoSolicitudBl;


        public ServicioTrazabilidadSDio(
            IObservacionOportunidadBl observacionOportunidadBl,
            IRecursoOportunidadBl recursoOportunidadBl,
            IOportunidadSTBl oportunidadSTBl,
            ICasoOportunidadBl casoOportunidadBl,
            IDatosPreventaMovilBl datosPreventaMovilBl,
            IEstadoCasoBl estadoCasoBl,
            ITipoOportunidadBl tipoOportunidadBl,
            IMotivoOportunidadBl motivoOportunidadBl,
            IFuncionPropietarioBl funcionPropietarioBl,
            ITiempoAtencionEquipoBl tiempoAtencionEquipoBl,
            IEtapaOportunidadBl IEtapaOportunidadBl,
            ITipoSolicitudBl ITipoSolicitudBl
            )
        {
            iobservacionOportunidadBl = observacionOportunidadBl;
            irecursoOportunidadBl = recursoOportunidadBl;
            ioportunidadSTBl = oportunidadSTBl;
            icasoOportunidadBl = casoOportunidadBl;
            idatosPreventaMovilBl = datosPreventaMovilBl;
            iestadoCasoBl = estadoCasoBl;
            itipoOportunidadBl = tipoOportunidadBl;
            imotivoOportunidadBl = motivoOportunidadBl;
            ifuncionPropietarioBl = funcionPropietarioBl;
            itiempoAtencionEquipoBl = tiempoAtencionEquipoBl;
            iEtapaOportunidadBl = IEtapaOportunidadBl;
            iTipoSolicitudBl = ITipoSolicitudBl;
        }

        
    }
}
