﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Trazabilidad
{
    public partial class ServicioTrazabilidadSDio
    {
        //EAAR:PF12
        public ObservacionOportunidadPaginadoDtoResponse ListadoObservacionesOportunidadPaginado(ObservacionOportunidadDtoRequest request)
        {
            return iobservacionOportunidadBl.ListadoObservacionesOportunidadPaginado(request);
        }
        public ObservacionOportunidadDtoResponse ObtenerObservacionOportunidadPorId(ObservacionOportunidadDtoRequest request)
        {
            return iobservacionOportunidadBl.ObtenerObservacionOportunidadPorId(request);
        }
        public ProcesoResponse ActualizarObservacionOportunidad(ObservacionOportunidadDtoRequest request)
        {
            return iobservacionOportunidadBl.ActualizarObservacionOportunidad(request);
        }
        public ProcesoResponse RegistrarObservacionOportunidad(ObservacionOportunidadDtoRequest request)
        {
            return iobservacionOportunidadBl.RegistrarObservacionOportunidad(request);
        }
        public ProcesoResponse EliminarObservacionOportunidad(ObservacionOportunidadDtoRequest request)
        {
            return iobservacionOportunidadBl.EliminarObservacionOportunidad(request);
        }
        public RecursoOportunidadPaginadoDtoResponse ListadoRecursosOportunidadPaginado(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadBl.ListadoRecursosOportunidadPaginado(request);
        }
        public RecursoOportunidadDtoResponse ObtenerRecursoOportunidadPorId(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadBl.ObtenerRecursoOportunidadPorId(request);
        }
        public ProcesoResponse ActualizarRecursoOportunidad(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadBl.ActualizarRecursoOportunidad(request);
        }
        public ProcesoResponse RegistrarRecursoOportunidad(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadBl.RegistrarRecursoOportunidad(request);
        }
        public ProcesoResponse EliminarRecursoOportunidad(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadBl.EliminarRecursoOportunidad(request);
        }

        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalObservacionPaginado(OportunidadSTDtoRequest request)
        {
            return ioportunidadSTBl.ListaOportunidadSTModalObservacionPaginado(request);
        }
        public CasoOportunidadDtoResponse ObtenerCasoOportunidadPorId(CasoOportunidadDtoRequest request)
        {
            return icasoOportunidadBl.ObtenerCasoOportunidadPorId(request);
        }
        public OportunidadSTDtoResponse ObtenerOportunidadSTPorId(OportunidadSTDtoRequest request)
        {
            return ioportunidadSTBl.ObtenerOportunidadSTPorId(request);
        }
        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalRecursoPaginado(OportunidadSTDtoRequest request)
        {
            return ioportunidadSTBl.ListaOportunidadSTModalRecursoPaginado(request);
        }
        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTMasivoPaginado(OportunidadSTDtoRequest request)
        {
            return ioportunidadSTBl.ListaOportunidadSTMasivoPaginado(request);
        }
        public ProcesoResponse RegistrarRecursoOportunidadMasivo(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadBl.RegistrarRecursoOportunidadMasivo(request);
        }
        public OportunidadSTSeguimientoPreventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPreventaPaginado(OportunidadSTSeguimientoPreventaDtoRequest request)
        {
            return ioportunidadSTBl.ListaOportunidadSTSeguimientoPreventaPaginado(request);
        }

        public OportunidadSTSeguimientoPostventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPostventaPaginado(OportunidadSTSeguimientoPostventaDtoRequest request)
        {
            return ioportunidadSTBl.ListaOportunidadSTSeguimientoPostventaPaginado(request);
        }

        public DatosPreventaMovilDtoResponse ObtenerDatosPreventaMovilPorId(DatosPreventaMovilDtoRequest request)
        {
            return idatosPreventaMovilBl.ObtenerDatosPreventaMovilPorId(request);
        }

        public ProcesoResponse ActualizarDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            return idatosPreventaMovilBl.ActualizarDatosPreventaMovil(request);
        }

        public ProcesoResponse RegistrarDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            return idatosPreventaMovilBl.RegistrarDatosPreventaMovil(request);
        }

        public ProcesoResponse EliminarDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            return idatosPreventaMovilBl.EliminarDatosPreventaMovil(request);
        }

        public List<ListaDtoResponse> ListarComboEstadoCaso()
        {
            return iestadoCasoBl.ListarComboEstadoCaso();
        }

        public List<ListaDtoResponse> ListarComboTipoOportunidad()
        {
            return itipoOportunidadBl.ListarComboTipoOportunidad();
        }

        public List<ListaDtoResponse> ListarComboMotivoOportunidad()
        {
            return imotivoOportunidadBl.ListarComboMotivoOportunidad();
        }

        public List<ListaDtoResponse> ListarComboFuncionPropietario()
        {
            return ifuncionPropietarioBl.ListarComboFuncionPropietario();
        }

        public ProcesoResponse ActualizarDatosPreventaMovilModalPreventa(DatosPreventaMovilDtoRequest request)
        {
            return idatosPreventaMovilBl.ActualizarDatosPreventaMovilModalPreventa(request);
        }
        public ProcesoResponse ActualizarOportunidadSTModalPreventa(OportunidadSTDtoRequest request)
        {
            return ioportunidadSTBl.ActualizarOportunidadSTModalPreventa(request);
        }
        public ProcesoResponse ActualizarCasoOportunidadModalPreventa(CasoOportunidadDtoRequest request)
        {
            return icasoOportunidadBl.ActualizarCasoOportunidadModalPreventa(request);
        }

        public ProcesoResponse CargaExcelDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            return idatosPreventaMovilBl.CargaExcelDatosPreventaMovil(request);
        }

        public TiempoAtencionEquipoPaginadoDtoResponse ListadoTiempoAtencionEquipoPaginado(TiempoAtencionEquipoDtoRequest request)
        {
            return itiempoAtencionEquipoBl.ListadoTiempoAtencionEquipoPaginado(request);
        }

        public TiempoAtencionEquipoDtoResponse ObtenerTiempoAtencionEquipoPorId(TiempoAtencionEquipoDtoRequest request)
        {
            return itiempoAtencionEquipoBl.ObtenerTiempoAtencionEquipoPorId(request);
        }

        public ProcesoResponse ActualizarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request)
        {
            return itiempoAtencionEquipoBl.ActualizarTiempoAtencionEquipo(request);
        }

        public ProcesoResponse RegistrarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request)
        {
            return itiempoAtencionEquipoBl.RegistrarTiempoAtencionEquipo(request);
        }

        public ProcesoResponse EliminarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request)
        {
            return itiempoAtencionEquipoBl.EliminarTiempoAtencionEquipo(request);
        }
        public List<ListaDtoResponse> ListarComboEtapaOportunidadPorIdFase(FaseDtoRequest request)
        {
            return iEtapaOportunidadBl.ListarComboEtapaOportunidadPorIdFase(request);
        }
        public List<ListaDtoResponse> ListarComboEtapaOportunidad()
        {
            return iEtapaOportunidadBl.ListarComboEtapaOportunidad();
        }

        public List<ListaDtoResponse> ListarComboTipoSolicitud()
        {
            return iTipoSolicitudBl.ListarComboTipoSolicitud();
        }
    }
}
