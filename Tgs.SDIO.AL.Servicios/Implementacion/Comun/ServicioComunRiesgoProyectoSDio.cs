﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse RegistrarRiesgoProyecto(RiesgoProyectoDtoRequest request)
        {
            return iriesgoproyectoBl.RegistrarRiesgoProyecto(request);
        }
        public RiesgoProyectoDtoResponse ObtenerRiesgoProyectoPorId(RiesgoProyectoDtoRequest request)
        {
            return iriesgoproyectoBl.ObtenerRiesgoProyectoPorId(request);
        }
        public ProcesoResponse ActualizarRiesgoProyecto(RiesgoProyectoDtoRequest request)
        {
            return iriesgoproyectoBl.ActualizarRiesgoProyecto(request);
        }

        public ProcesoResponse EliminarRiesgoProyecto(RiesgoProyectoDtoRequest request)
        {
            return iriesgoproyectoBl.EliminarRiesgoProyecto(request);
        }
        public RiesgoProyectoPaginadoDtoResponse ListadoRiesgoProyectosPaginado(RiesgoProyectoDtoRequest request)
        {
            return iriesgoproyectoBl.ListadoRiesgoProyectosPaginado(request);
        }

    }
}
