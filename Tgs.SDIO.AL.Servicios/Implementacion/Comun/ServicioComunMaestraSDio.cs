﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun

{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse ActualizarMaestra(MaestraDtoRequest maestra)
        {
            return iMaestraBl.ActualizarMaestra(maestra);
        }
        public List<ListaDtoResponse>ListarMaestraPorIdRelacion(MaestraDtoRequest maestra)
        {
            return iMaestraBl.ListarMaestraPorIdRelacion(maestra);
        }

        public List<MaestraDtoResponse> ObtenerMaestra(MaestraDtoRequest maestra)
        {
            return iMaestraBl.ObtenerMaestra(maestra);
        }

        public ProcesoResponse RegistrarMaestra(MaestraDtoRequest maestra)
        {
            return iMaestraBl.RegistrarMaestra(maestra);
        }

        public List<ComboDtoResponse> ListarComboTiposRecursos()
        {
            return iMaestraBl.ListarComboTiposRecursos();
        }
        public List<ListaDtoResponse> ListarMaestra(MaestraDtoRequest maestra)
        {
            return iMaestraBl.ListarMaestra(maestra);
        }

        public List<ListaDtoResponse> ListarMaestraPorValor(MaestraDtoRequest maestra)
        {
            return iMaestraBl.ListarMaestraPorValor(maestra);
        }

        public List<MaestraDtoResponse> ListarMaestraPorIdRelacion2(MaestraDtoRequest maestra)
        {
            return iMaestraBl.ListarMaestraPorIdRelacion2(maestra);
        }
    }
}

