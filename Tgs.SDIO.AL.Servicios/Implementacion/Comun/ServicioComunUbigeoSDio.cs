﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public List<ListaDtoResponse> ListarComboUbigeoDepartamento()
        {
            return iubigeoBl.ListarComboUbigeoDepartamento();
        }

        public List<ListaDtoResponse> ListarComboUbigeoProvincia(UbigeoDtoRequest request)
        {
            return iubigeoBl.ListarComboUbigeoProvincia(request);
        }

        public List<ListaDtoResponse> ListarComboUbigeoDistrito(UbigeoDtoRequest request)
        {
            return iubigeoBl.ListarComboUbigeoDistrito(request);
        }

        public UbigeoDtoResponse ObtenerPorCodigoDistrito(UbigeoDtoRequest ubigeo)
        {
            return iubigeoBl.ObtenerPorCodigoDistrito(ubigeo);
        }

    }
}
