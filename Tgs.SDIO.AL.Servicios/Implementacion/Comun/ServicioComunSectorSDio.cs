﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun

{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse ActualizarSector(SectorDtoRequest sector)
        {

            return iSectorBl.ActualizarSector(sector);
        }

        public List<ListaDtoResponse> ListarSector(SectorDtoRequest sector)
        {
            return iSectorBl.ListarSector(sector);
        }

        public SectorDtoResponse ObtenerSector(SectorDtoRequest sector)
        {
            return iSectorBl.ObtenerSector(sector);
        }

        public ProcesoResponse RegistrarSector(SectorDtoRequest sector)
        {
            return iSectorBl.RegistrarSector(sector);
        }

        public List<ListaDtoResponse> ListarComboSector()
        {
            return iSectorBl.ListarComboSector();
        }
    }
}
