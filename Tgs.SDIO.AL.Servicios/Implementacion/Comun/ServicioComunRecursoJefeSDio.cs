﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public RecursoJefePaginadoDtoResponse ListarRecursoPorJefePaginado(RecursoJefeDtoRequest recursoJefeDtoRequest)
        {
            return iRecursoJefeBl.ListarRecursoPorJefePaginado(recursoJefeDtoRequest);
        }

        public ProcesoResponse ActualizarEstadoRecursoJefe(RecursoJefeDtoRequest recursoJefeDtoRequest)
        {
            return iRecursoJefeBl.ActualizarEstadoRecursoJefe(recursoJefeDtoRequest);
        }

        public ProcesoResponse RegistrarRecursoJefe(RecursoJefeDtoRequest recursoJefeDtoRequest)
        {
            return iRecursoJefeBl.RegistrarRecursoJefe(recursoJefeDtoRequest);
        }
    }
}
