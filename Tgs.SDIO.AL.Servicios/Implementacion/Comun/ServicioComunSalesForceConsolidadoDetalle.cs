﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;


namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {

        public SalesForceConsolidadoDetalleDtoResponse ObtenerSalesForceConsolidadoDetalle(SalesForceConsolidadoDetalleDtoRequest salesForceConsolidadoDetalle)
        {
            return iSalesForceConsolidadoDetalleBl.ObtenerSalesForceConsolidadoDetalle(salesForceConsolidadoDetalle);
        }

        public List<ListaDtoResponse> ListaNumerodeCasoPorNumeroSalesForceDetalle(SalesForceConsolidadoDetalleDtoRequest salesForceConsolidadoDetalle)
        {
            return iSalesForceConsolidadoDetalleBl.ListaNumerodeCasoPorNumeroSalesForceDetalle(salesForceConsolidadoDetalle);
        }
        
    }
}
