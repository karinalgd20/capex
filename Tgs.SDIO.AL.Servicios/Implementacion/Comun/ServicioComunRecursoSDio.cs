﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public RecursoDtoResponse ListarRecursoDeUsuario(RecursoDtoRequest request)
        {
            return irecursoBl.ListarRecursoDeUsuario(request);
        }

        public RecursoDtoResponse ObtenerRecursoPorIdUsuarioRais(int idUsuarioRais)
        {
            return irecursoBl.ObtenerRecursoPorIdUsuarioRais(idUsuarioRais);
        }
    }
}
