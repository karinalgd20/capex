﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public RecursoLineaNegocioPaginadoDtoResponse ListarRecursoLineaNegocioPaginado(RecursoLineaNegocioDtoRequest recursoLineaRequest)
        {
            return iRecursoLineaNegocioBl.ListarRecursoLineaNegocioPaginado(recursoLineaRequest);
        }

        public ProcesoResponse ActualizarEstadoRecursoLineaNegocio(RecursoLineaNegocioDtoRequest recursoLineaRequest)
        {
            return iRecursoLineaNegocioBl.ActualizarEstadoRecursoLineaNegocio(recursoLineaRequest);
        }

        public ProcesoResponse RegistrarRecursoLineaNegocio(RecursoLineaNegocioDtoRequest recursoLineaRequest)
        {
            return iRecursoLineaNegocioBl.RegistrarRecursoLineaNegocio(recursoLineaRequest);
        }
    }
}
