﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public List<ListaDtoResponse> ListarComboEmpresa()
        {
            return iempresaBl.ListarComboEmpresa();
        }
    }
}
