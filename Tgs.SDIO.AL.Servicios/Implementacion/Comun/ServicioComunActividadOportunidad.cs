﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public ActividadPaginadoDtoResponse ListatActividadPaginado(ActividadDtoRequest request)
        {
            return iActividadBl.ListatActividadPaginado(request);
        }
        public ActividadDtoResponse ObtenerActividadPorId(ActividadDtoRequest request)
        {
            return iActividadBl.ObtenerActividadPorId(request);
        }
        public ProcesoResponse ActualizarActividad(ActividadDtoRequest request)
        {
            return iActividadBl.ActualizarActividad(request);
        }
        public ProcesoResponse RegistrarActividad(ActividadDtoRequest request)
        {
            return iActividadBl.RegistrarActividad(request);
        }
        public ProcesoResponse EliminarActividad(ActividadDtoRequest request)
        {
            return iActividadBl.EliminarActividad(request);
        }

        public List<ListaDtoResponse> ListarComboActividadPadres()
        {
            return iActividadBl.ListarComboActividadPadres();
        }
    }
}




