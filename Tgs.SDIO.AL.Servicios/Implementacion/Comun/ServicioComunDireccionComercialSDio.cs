﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun

{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse ActualizarDireccionComercial(DireccionComercialDtoRequest direccionComercial)
        {


            return iDireccionComercialBl.ActualizarDireccionComercial(direccionComercial);
        }

        public List<DireccionComercialDtoResponse> ListarDireccionComercial(DireccionComercialDtoRequest direccionComercial)
        {
            return iDireccionComercialBl.ListarDireccionComercial(direccionComercial);
        }

        public DireccionComercialDtoResponse ObtenerDireccionComercial(DireccionComercialDtoRequest direccionComercial)
        {
            return iDireccionComercialBl.ObtenerDireccionComercial(direccionComercial);
        }

        public ProcesoResponse RegistrarDireccionComercial(DireccionComercialDtoRequest direccionComercial)
        {
            return iDireccionComercialBl.RegistrarDireccionComercial(direccionComercial);
        }
    }
}
