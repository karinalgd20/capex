﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun

{
    public partial class ServicioComunSDio
    {
        public CostoDtoResponse ObtenerCostoPorCodigo(CostoDtoRequest medioCosto)
        {
            return iCostoBl.ObtenerCostoPorCodigo(medioCosto);
        }
        public List<ListaDtoResponse> ListarCosto(CostoDtoRequest medioCosto)
        {
            return iCostoBl.ListarCosto(medioCosto);
        }
        public List<CostoDtoResponse> ListarComboCosto(CostoDtoRequest medioCosto)
        {
            return iCostoBl.ListarComboCosto(medioCosto);
        }
        public List<CostoDtoResponse> ListarCostoPorMedioServicioGrupo(CostoDtoRequest costo)
        {
            return iCostoBl.ListarCostoPorMedioServicioGrupo(costo);
        }

        public CostoDtoResponse ObtenerCosto(CostoDtoRequest medioCosto)
        {
            return iCostoBl.ObtenerCosto(medioCosto);
        }
    }
}
