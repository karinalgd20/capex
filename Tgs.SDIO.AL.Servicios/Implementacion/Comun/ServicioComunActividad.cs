﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public RecursoPaginadoDtoResponse ListadoRecursosPaginado(RecursoDtoRequest request)
        {
            return irecursoBl.ListadoRecursosPaginado(request);
        }

        public RecursoDtoResponse ObtenerRecursoPorId(RecursoDtoRequest request)
        {
            return irecursoBl.ObtenerRecursoPorId(request);
        }
        public ProcesoResponse ActualizarRecurso(RecursoDtoRequest request)
        {
            return irecursoBl.ActualizarRecurso(request);
        }
        public ProcesoResponse RegistrarRecurso(RecursoDtoRequest request)
        {
            return irecursoBl.RegistrarRecurso(request);
        }
        public ProcesoResponse EliminarRecurso(RecursoDtoRequest request)
        {
            return irecursoBl.EliminarRecurso(request);
        }
        public RolRecursoPaginadoDtoResponse ListadoRolRecursosPaginado(RolRecursoDtoRequest request)
        {
            return irolRecursoBl.ListadoRolRecursosPaginado(request);
        }
        public RolRecursoDtoResponse ObtenerRolRecursoPorId(RolRecursoDtoRequest request)
        {
            return irolRecursoBl.ObtenerRolRecursoPorId(request);
        }
        public ProcesoResponse ActualizarRolRecurso(RolRecursoDtoRequest request)
        {
            return irolRecursoBl.ActualizarRolRecurso(request);
        }
        public ProcesoResponse RegistrarRolRecurso(RolRecursoDtoRequest request)
        {
            return irolRecursoBl.RegistrarRolRecurso(request);
        }
        public ProcesoResponse EliminarRolRecurso(RolRecursoDtoRequest request)
        {
            return irolRecursoBl.EliminarRolRecurso(request);
        }
        public RecursoPaginadoDtoResponse ListaRecursoModalRecursoPaginado(RecursoDtoRequest request)
        {
            return irecursoBl.ListaRecursoModalRecursoPaginado(request);
        }
        public List<ListaDtoResponse> ListarComboRolRecurso()
        {
            return irolRecursoBl.ListarComboRolRecurso();
        }
        public List<RecursoDtoResponse> ListarRecursoPorCargo(RecursoDtoRequest request)
        {
            return irecursoBl.ListarRecursoPorCargo(request);
        }

        public RecursoPaginadoDtoResponse ListadoRecursosByCargoPaginado(RecursoDtoRequest request)
        {
            return irecursoBl.ListadoRecursosByCargoPaginado(request);
        }
    }
}




