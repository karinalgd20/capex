﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public List<ListaDtoResponse> ListarAreas(AreaDtoRequest areaRequest)
        {
            return iAreaBl.ListarAreas(areaRequest);
        }
        public List<ListaDtoResponse> ListarComboArea()
        {
            return iAreaBl.ListarComboArea();
        }
    }
}
