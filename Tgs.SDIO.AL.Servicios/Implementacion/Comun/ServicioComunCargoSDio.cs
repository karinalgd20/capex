﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public List<ListaDtoResponse> ListarCargos(CargoDtoRequest cargoRequest)
        {
            return iCargoBl.ListarCargos(cargoRequest);
        }
        public List<ListaDtoResponse> ListarComboCargo()
        {
            return iCargoBl.ListarComboCargo();
        }
    }
}
