﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Comun;


namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public List<ComboDtoResponse> ListarComboSegmentoNegocio()
        {
            return isegmentoNegocioBl.ListarComboSegmentoNegocio();
        }

        public List<ListaDtoResponse> ListarComboSegmentoNegocioSimple()
        {
            return isegmentoNegocioBl.ListarComboSegmentoNegocioSimple();
        }
    }
}
