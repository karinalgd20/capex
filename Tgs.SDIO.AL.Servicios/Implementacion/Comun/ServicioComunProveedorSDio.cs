﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun

{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse RegistrarProveedor(ProveedorDtoRequest proveedor)
        {
            return iProveedorBl.RegistrarProveedor(proveedor);
        }

        public ProcesoResponse ActualizarProveedor(ProveedorDtoRequest proveedor)
        {
            return iProveedorBl.ActualizarProveedor(proveedor);
        }
        public ProcesoResponse InactivarProveedor(ProveedorDtoRequest proveedor)
        {
            return iProveedorBl.InactivarProveedor(proveedor);
        }
        public List<ListaDtoResponse> ListarProveedor(ProveedorDtoRequest proveedor)
        {
            return iProveedorBl.ListarProveedor(proveedor);
        }

        public ProveedorDtoResponse ObtenerProveedor(ProveedorDtoRequest proveedor)
        {
            return iProveedorBl.ObtenerProveedor(proveedor);
        }

        public List<ProveedorDtoResponse> ListarProveedores(ProveedorDtoRequest proveedor)
        {
            return iProveedorBl.ListarProveedores(proveedor);
        }

        public ProveedorPaginadoDtoResponse ListarProveedorPaginado(ProveedorDtoRequest proveedor)
        {
            return iProveedorBl.ListarProveedorPaginado(proveedor);
        }
    }
}
