﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.Servicios.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioComunSDio", Name = "ServicioComunSDio",
        ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioComunSDio : IServicioComunSDio
    {

        private readonly ISalesForceConsolidadoCabeceraBl _salesForceConsolidadoCabeceraBl;
        public readonly IArchivoBl iArchivoBl;
        public readonly IConceptoBl iConceptoBl;
        public readonly IBWBl iBWBl;
        public readonly ILineaNegocioBl iLineaNegocioBl;
        public readonly IDepreciacionBl iDepreciacionBl;
        public readonly IDGBl iDGBl;
        public readonly IDireccionComercialBl iDireccionComercialBl;
        public readonly IFentoceldaBl iFentoceldaBl;
        public readonly ILineaProductoBl iLineaProductoBl;
        public readonly IMaestraBl iMaestraBl;
        public readonly IProveedorBl iProveedorBl;
        public readonly ISectorBl iSectorBl;
        public readonly IServicioCMIBl iServicioCMIBl;
        public readonly IClienteBl iClienteBl;
        public readonly ISubServicioBl iSubServicioBl;
        public readonly IServicioSubServicioBl iServicioSubServicioBl;
        public readonly IServicioBl iServicioBl;
        public readonly IMedioBl iMedioBl;
        public readonly ICasoNegocioBl iCasoNegocioBl;
        public readonly ICasoNegocioServicioBl iCasoNegocioServicioBl;
        public readonly ICostoBl iCostoBl;
        public readonly IMedioCostoBl iMedioCostoBl;
        public readonly IAreaBl iAreaBl;
        public readonly ICargoBl iCargoBl;
        public readonly IRecursoJefeBl iRecursoJefeBl;
        public readonly IRecursoLineaNegocioBl iRecursoLineaNegocioBl;
        private readonly IRecursoBl irecursoBl;
        private readonly IRolRecursoBl irolRecursoBl;
        private readonly IConceptoSeguimientoBl iconceptoSeguimientoBl;
        private readonly IActividadBl iActividadBl;
        private readonly IAreaSeguimientoBl iareaSeguimientoBl; 
        private readonly IAreaSegmentoNegocioBl iareaSegmentoNegocioBl;
        private readonly ISegmentoNegocioBl isegmentoNegocioBl;
        private readonly IEtapaBl ietapaOportunidadBl;
        private readonly IFaseBl ifaseBl;
        private readonly IUbigeoBl iubigeoBl;
        private readonly ISalesForceConsolidadoDetalleBl iSalesForceConsolidadoDetalleBl;
        private readonly IRiesgoProyectoBl iriesgoproyectoBl;
        private readonly ICorreoBl iCorreoBl;
        private readonly IEmpresaBl iempresaBl;
        private readonly IServicioGrupoBl iServicioGrupoBl;
        private readonly IEquipoTrabajoBl iEquipoTrabajoBl;
        private readonly ITipoSolucionBl iTipoSolucionBl;
        private readonly ITipoCambioBl iTipoCambioBl;
        private readonly ITipoCambioDetalleBl iTipoCambioDetalleBl;
		private readonly IContratoMarcoBl iContratoMarcoBl;

        public ServicioComunSDio(ISalesForceConsolidadoCabeceraBl salesForceConsolidadoCabeceraBl, IArchivoBl IArchivoBl, IConceptoBl IConceptoBl, IBWBl IBWBl, ILineaNegocioBl ILineaNegocioBl,
         IDepreciacionBl IDepreciacionBl, IDGBl IDGBl, IDireccionComercialBl IDireccionComercialBl, IFentoceldaBl IFentoceldaBl, ILineaProductoBl ILineaProductoBl,
         IMaestraBl IMaestraBl, IProveedorBl IProveedorBl, ISectorBl ISectorBl, IServicioCMIBl IServicioCMIBl, IClienteBl IClienteBl, ISubServicioBl ISubServicioBl,
         IServicioSubServicioBl IServicioSubServicioBl, IServicioBl IServicioBl, IMedioBl IMedioBl, ICasoNegocioBl ICasoNegocioBl,
         ICasoNegocioServicioBl ICasoNegocioServicioBl, ICostoBl ICostoBl, IMedioCostoBl IMedioCostoBl,
         IAreaBl IAreaBl, ICargoBl ICargoBl, IRecursoJefeBl IRecursoJefeBl, IRecursoLineaNegocioBl IRecursoLineaNegocioBl,
         IRecursoBl recursoBl,
         IRolRecursoBl rolRecursoBl,
         IConceptoSeguimientoBl conceptoSeguimientoBl,
         IActividadBl ActividadBl,
         IAreaSeguimientoBl areaSeguimientoBl,
         IAreaSegmentoNegocioBl areaSegmentoNegocioBl,
         ISegmentoNegocioBl segmentoNegocioBl,
         IEtapaBl etapaOportunidadBl,
         IFaseBl faseBl,
         IUbigeoBl ubigeoBl,
         ISalesForceConsolidadoDetalleBl ISalesForceConsolidadoDetalleBl,
         IRiesgoProyectoBl riesgoproyectoBl,
         ICorreoBl correoBl,
         IEmpresaBl empresaBl,
         IServicioGrupoBl servicioGrupoBl,
         IEquipoTrabajoBl equipoTrabajoBl,
         ITipoSolucionBl ITipoSolucionBl,
         ITipoCambioBl ITipoCambioBl,
         ITipoCambioDetalleBl ITipoCambioDetalleBl,
		 IContratoMarcoBl IContratoMarcoBl

            )
        {
            _salesForceConsolidadoCabeceraBl = salesForceConsolidadoCabeceraBl;
            iArchivoBl = IArchivoBl;
            iTipoCambioBl = ITipoCambioBl;
            iTipoCambioDetalleBl = ITipoCambioDetalleBl;
            iConceptoBl = IConceptoBl;
            iBWBl = IBWBl;
            iLineaNegocioBl = ILineaNegocioBl;
            iDepreciacionBl = IDepreciacionBl;
            iDGBl = IDGBl;
            iDireccionComercialBl = IDireccionComercialBl;
            iFentoceldaBl = IFentoceldaBl;
            iLineaProductoBl = ILineaProductoBl;
            iMaestraBl = IMaestraBl;
            iProveedorBl = IProveedorBl;
            iSectorBl = ISectorBl;
            iServicioCMIBl = IServicioCMIBl;
            iClienteBl = IClienteBl;
            iSubServicioBl = ISubServicioBl;
            iServicioSubServicioBl = IServicioSubServicioBl;
            iServicioBl = IServicioBl;
            iMedioBl = IMedioBl;
            iCasoNegocioBl = ICasoNegocioBl;
            iCasoNegocioServicioBl = ICasoNegocioServicioBl;
            iCostoBl = ICostoBl;
            iMedioCostoBl = IMedioCostoBl;
            iAreaBl = IAreaBl;
            iCargoBl = ICargoBl;
            iRecursoJefeBl = IRecursoJefeBl;
            iRecursoLineaNegocioBl = IRecursoLineaNegocioBl;
            irecursoBl = recursoBl;
            irolRecursoBl = rolRecursoBl;
            iconceptoSeguimientoBl = conceptoSeguimientoBl;
            iActividadBl = ActividadBl;
            irecursoBl = recursoBl;
            iareaSeguimientoBl = areaSeguimientoBl;
            iareaSegmentoNegocioBl = areaSegmentoNegocioBl;
            isegmentoNegocioBl = segmentoNegocioBl;
            ietapaOportunidadBl = etapaOportunidadBl;
            ifaseBl = faseBl;
            iubigeoBl = ubigeoBl;
            iSalesForceConsolidadoDetalleBl = ISalesForceConsolidadoDetalleBl;
            iriesgoproyectoBl = riesgoproyectoBl;
            iCorreoBl = correoBl;
            iempresaBl = empresaBl;
            iServicioGrupoBl = servicioGrupoBl;
            iEquipoTrabajoBl = equipoTrabajoBl;
            iTipoSolucionBl = ITipoSolucionBl;
			iContratoMarcoBl = IContratoMarcoBl;
        }

    }

}