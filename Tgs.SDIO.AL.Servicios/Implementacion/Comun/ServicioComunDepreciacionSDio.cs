﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse ActualizarDepreciacion(DepreciacionDtoRequest depreciacion)
        {


            return iDepreciacionBl.ActualizarDepreciacion(depreciacion);
        }

        public List<ListaDtoResponse> ListarDepreciacion(DepreciacionDtoRequest depreciacion)
        {
            return iDepreciacionBl.ListarDepreciacion(depreciacion);
        }

        public DepreciacionDtoResponse ObtenerDepreciacion(DepreciacionDtoRequest depreciacion)
        {
            return iDepreciacionBl.ObtenerDepreciacion(depreciacion);
        }

        public ProcesoResponse RegistrarDepreciacion(DepreciacionDtoRequest depreciacion)
        {
            return iDepreciacionBl.RegistrarDepreciacion(depreciacion);
        }
    }
}
