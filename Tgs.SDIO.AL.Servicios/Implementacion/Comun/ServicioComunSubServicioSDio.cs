﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {

        public ProcesoResponse ActualizarSubServicio(SubServicioDtoRequest SubServicio)
        {
            return iSubServicioBl.ActualizarSubServicio(SubServicio);
        }
        public List<ListaDtoResponse> ListarSubServicios(SubServicioDtoRequest SubServicio)
        {
            return iSubServicioBl.ListarSubServicios(SubServicio);
        }
        public ProcesoResponse InhabilitarSubServicio(SubServicioDtoRequest SubServicio)
        {
            return iSubServicioBl.InhabilitarSubServicio(SubServicio);
        }


    
        public SubServicioDtoResponse ObtenerSubServicio(SubServicioDtoRequest SubServicio)
        {
            return iSubServicioBl.ObtenerSubServicio(SubServicio);
        }

        public ProcesoResponse RegistrarSubServicio(SubServicioDtoRequest SubServicio)
        {
            return iSubServicioBl.RegistrarSubServicio(SubServicio);
        }
        public SubServicioPaginadoDtoResponse ListaSubServicioPaginado(SubServicioDtoRequest SubServicio)
        {
            return iSubServicioBl.ListaSubServicioPaginado(SubServicio);
        }
        public List<ListaDtoResponse> ListarSubServiciosPorIdGrupo(SubServicioDtoRequest subServicio)
        {
            return iSubServicioBl.ListarSubServiciosPorIdGrupo(subServicio);
        }
    }
}
