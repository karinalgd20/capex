﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {

        public ProcesoResponse ActualizarConcepto(ConceptoDtoRequest concepto)
        {
            return iConceptoBl.ActualizarConcepto(concepto);
        }

        public List<ConceptoDtoResponse> ListarBandejaConceptos(ConceptoDtoRequest concepto)
        {
            return iConceptoBl.ListarBandejaConceptos(concepto);
        }

        public List<ConceptoDtoResponse> ListarConceptos(ConceptoDtoRequest concepto)
        {
            return iConceptoBl.ListarConceptos(concepto);
        }

        public ConceptoDtoResponse ObtenerConcepto(ConceptoDtoRequest concepto)
        {
            return iConceptoBl.ObtenerConcepto(concepto);
        }

        public ProcesoResponse RegistrarConcepto(ConceptoDtoRequest concepto)
        {
            return iConceptoBl.RegistrarConcepto(concepto);
        }
    }
}
