﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun

{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse ActualizarServicioCMI(ServicioCMIDtoRequest servicioCMI)
        {
            return iServicioCMIBl.ActualizarServicioCMI(servicioCMI);
        }

        public List<ServicioCMIDtoResponse> ListarServicioCMI(ServicioCMIDtoRequest servicioCMI)
        {
            return iServicioCMIBl.ListarServicioCMI(servicioCMI);
        }

        public ServicioCMIDtoResponse ObtenerServicioCMI(ServicioCMIDtoRequest servicioCMI)
        {
            return iServicioCMIBl.ObtenerServicioCMI(servicioCMI);
        }

        public ProcesoResponse RegistrarServicioCMI(ServicioCMIDtoRequest servicioCMI)
        {
            return iServicioCMIBl.RegistrarServicioCMI(servicioCMI);
        }

        public List<ListaDtoResponse> ListarServicioCMIPorLineaNegocio(ServicioCMIDtoRequest servicioCMI)
        {
            return iServicioCMIBl.ListarServicioCMIPorLineaNegocio(servicioCMI);
        }

     
    }
}
