﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;


namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public List<ListaDtoResponse> ListarComboAreaSeguimiento()
        {
            return iareaSeguimientoBl.ListarComboAreaSeguimiento();
        }
        public AreaSeguimientoPaginadoDtoResponse ListadoAreasSeguimientoPaginado(AreaSeguimientoDtoRequest request)
        {
            return iareaSeguimientoBl.ListadoAreasSeguimientoPaginado(request);
        }
        public AreaSeguimientoDtoResponse ObtenerAreaSeguimientoPorId(AreaSeguimientoDtoRequest request)
        {
            return iareaSeguimientoBl.ObtenerAreaSeguimientoPorId(request);
        }
        public ProcesoResponse ActualizarAreaSeguimiento(AreaSeguimientoDtoRequest request)
        {
            return iareaSeguimientoBl.ActualizarAreaSeguimiento(request);
        }
        public ProcesoResponse RegistrarAreaSeguimiento(AreaSeguimientoDtoRequest request)
        {
            return iareaSeguimientoBl.RegistrarAreaSeguimiento(request);
        }
        public ProcesoResponse EliminarAreaSeguimiento(AreaSeguimientoDtoRequest request)
        {
            return iareaSeguimientoBl.EliminarAreaSeguimiento(request);
        }
    }
}
