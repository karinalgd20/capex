﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse EnviarCorreo(MailDto mailDto)
        {
            return iCorreoBl.EnviarCorreo(mailDto);
        }
    }
}
