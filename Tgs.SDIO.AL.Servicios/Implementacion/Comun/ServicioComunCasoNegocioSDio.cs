﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;


namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {

        public CasoNegocioPaginadoDtoResponse ListarCasoNegocio(CasoNegocioDtoRequest casonegocio)
        {
            return iCasoNegocioBl.ListarCasoNegocio(casonegocio);
        }

        public CasoNegocioDtoResponse ObtenerCasoNegocio(CasoNegocioDtoRequest casonegocio)
        {
            return iCasoNegocioBl.ObtenerCasoNegocio(casonegocio);
        }

        public ProcesoResponse RegistrarCasoNegocio(CasoNegocioDtoRequest casonegocio)
        {
            return iCasoNegocioBl.RegistrarCasoNegocio(casonegocio);
        }

        public ProcesoResponse ActualizarCasoNegocio(CasoNegocioDtoRequest casonegocio)
        {
            return iCasoNegocioBl.ActualizarCasoNegocio(casonegocio);
        }
        public List<ListaDtoResponse> ListarCasoNegocioDefecto(CasoNegocioDtoRequest casonegocio)
        {
            return iCasoNegocioBl.ListarCasoNegocioDefecto(casonegocio);
        }

    }
}
