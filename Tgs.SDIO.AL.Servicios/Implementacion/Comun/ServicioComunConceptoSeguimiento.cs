﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public ConceptoSeguimientoPaginadoDtoResponse ListadoConceptosSeguimientoPaginado(ConceptoSeguimientoDtoRequest request)
        {
            return iconceptoSeguimientoBl.ListadoConceptosSeguimientoPaginado(request);
        }
        public ConceptoSeguimientoDtoResponse ObtenerConceptoSeguimientoPorId(ConceptoSeguimientoDtoRequest request)
        {
            return iconceptoSeguimientoBl.ObtenerConceptoSeguimientoPorId(request);
        }
        public ProcesoResponse ActualizarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            return iconceptoSeguimientoBl.ActualizarConceptoSeguimiento(request);
        }
        public ProcesoResponse RegistrarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            return iconceptoSeguimientoBl.RegistrarConceptoSeguimiento(request);
        }
        public ProcesoResponse EliminarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            return iconceptoSeguimientoBl.EliminarConceptoSeguimiento(request);
        }
        public List<ListaDtoResponse> ListarComboConceptoSeguimiento()
        {
            return iconceptoSeguimientoBl.ListarComboConceptoSeguimiento();
        }
        public List<ListaDtoResponse> ListarComboConceptoSeguimientoAgrupador()
        {
            return iconceptoSeguimientoBl.ListarComboConceptoSeguimientoAgrupador();
        }
        public List<ListaDtoResponse> ListarComboConceptoSeguimientoNiveles()
        {
            return iconceptoSeguimientoBl.ListarComboConceptoSeguimientoNiveles();
        }
    }
}




