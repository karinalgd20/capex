﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;


namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public AreaSegmentoNegocioDtoResponse ObtenerAreaSegmentoNegocioPorId(AreaSegmentoNegocioDtoRequest request)
        {
            return iareaSegmentoNegocioBl.ObtenerAreaSegmentoNegocioPorId(request);
        }
        public ProcesoResponse RegistrarAreaSegmentoNegocio(AreaSegmentoNegocioDtoRequest request)
        {
            return iareaSegmentoNegocioBl.RegistrarAreaSegmentoNegocio(request);
        }
        public ProcesoResponse EliminarAreaSegmentoNegocio(AreaSegmentoNegocioDtoRequest request)
        {
            return iareaSegmentoNegocioBl.EliminarAreaSegmentoNegocio(request);
        }
    }
}
