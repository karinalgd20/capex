﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public List<ListaDtoResponse> ListarComboEtapaOportunidad()
        {
            return ietapaOportunidadBl.ListarComboEtapaOportunidad();
        }

        public List<ListaDtoResponse> ListarComboEtapaOportunidadPorIdFase(FaseDtoRequest request)
        {
            return ietapaOportunidadBl.ListarComboEtapaOportunidadPorIdFase(request);
        }
    }
}
