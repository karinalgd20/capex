﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse ActualizarLineaNegocio(LineaNegocioDtoRequest lineaNegocio)
        {
            return iLineaNegocioBl.ActualizarLineaNegocio(lineaNegocio);
        }

        public List<ListaDtoResponse> ListarLineaNegocio(LineaNegocioDtoRequest lineaNegocio)
        {
            return iLineaNegocioBl.ListarLineaNegocio(lineaNegocio);
        }

        public LineaNegocioDtoResponse ObtenerLineaNegocio(LineaNegocioDtoRequest lineaNegocio)
        {
            return iLineaNegocioBl.ObtenerLineaNegocio(lineaNegocio);
        }

        public ProcesoResponse RegistrarLineaNegocio(LineaNegocioDtoRequest lineaNegocio)
        {
            return iLineaNegocioBl.RegistrarLineaNegocio(lineaNegocio);
        }
    }
}
