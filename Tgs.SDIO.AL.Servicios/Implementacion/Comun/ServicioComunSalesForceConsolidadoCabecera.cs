﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Adapter;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Error.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public List<SalesForceConsolidadoCabeceraListarDtoResponse> GetListSalesForceConsolidadoCabecera(SalesForceConsolidadoCabeceraListarDtoRequest request)
        {            
            var filtros = request.ProjectedAs<SalesForceConsolidadoCabecera>();
                return _salesForceConsolidadoCabeceraBl.SalesForceConsolidadoCabecera(filtros)
                .ProjectedAs<List<SalesForceConsolidadoCabeceraListarDtoResponse>>();
        }
        public List<SalesForceConsolidadoCabeceraDtoResponse> ListarNumeroSalesForcePorIdOportunidad(SalesForceConsolidadoCabeceraDtoRequest salesForceConsolidadoCabecera)
        {
            return _salesForceConsolidadoCabeceraBl.ListarNumeroSalesForcePorIdOportunidad(salesForceConsolidadoCabecera);
        }
        public List<SalesForceConsolidadoCabeceraResponse> ListarProbabilidades()
        {
            return _salesForceConsolidadoCabeceraBl.ListarProbabilidades();
        }
    }
}
