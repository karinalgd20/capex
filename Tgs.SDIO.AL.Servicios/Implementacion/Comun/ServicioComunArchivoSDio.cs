﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse RegistrarArchivo(ArchivoDtoRequest archivo, byte[] fileArchivo)
        {
            return iArchivoBl.RegistrarArchivo(archivo, fileArchivo);
        }
        public ProcesoResponse InactivarArchivo(ArchivoDtoRequest archivo)
        {
            return iArchivoBl.InactivarArchivo(archivo);
        }
        public ArchivoPaginadoDtoResponse ListarArchivoPaginado(ArchivoDtoRequest request)
        {
            return iArchivoBl.ListarArchivoPaginado(request);
        }
        public ArchivoDtoResponse ObtenerArchivo(ArchivoDtoRequest request)
        {
            return iArchivoBl.ObtenerArchivo(request);
        }
    }
}