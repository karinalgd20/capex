﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;


namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public List<CasoNegocioServicioDtoResponse> ListarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio)
        {
            return iCasoNegocioServicioBl.ListarCasoNegocioServicio(casoservicio);
        }

        public CasoNegocioServicioDtoResponse ObtenerCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio)
        {
            return iCasoNegocioServicioBl.ObtenerCasoNegocioServicio(casoservicio);
        }

        public ProcesoResponse RegistrarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio)
        {
            return iCasoNegocioServicioBl.RegistrarCasoNegocioServicio(casoservicio);
        }

        public ProcesoResponse ActualizarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio)
        {
            return iCasoNegocioServicioBl.ActualizarCasoNegocioServicio(casoservicio);
        }

        public CasoNegocioServicioPaginadoDtoResponse ListPaginadoCasoNegocioServicio(CasoNegocioServicioDtoRequest casoNegocio)
        {
            return iCasoNegocioServicioBl.ListPaginadoCasoNegocioServicio(casoNegocio);
        }
    }
}
