﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public List<ListaDtoResponse> ListarServicioGrupo(ServicioGrupoDtoRequest serviciogrupo)
        {
            return iServicioGrupoBl.ListarServicioGrupo(serviciogrupo);
        }

    }
}
