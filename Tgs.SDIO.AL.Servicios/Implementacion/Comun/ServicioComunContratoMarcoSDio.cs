﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun

{
    public partial class ServicioComunSDio
    {
        public ContratoMarcoDtoResponsePaginado ListarContratoMarcoPaginado(ContratoMarcoDtoRequest contratomarco)
        {
            return iContratoMarcoBl.ListarContratoMarcoPaginado(contratomarco);
        }

        public ContratoMarcoDtoResponse ObtenerContratoMarcoPorId(ContratoMarcoDtoRequest request)
        {
            return iContratoMarcoBl.ObtenerContratoMarcoPorId(request);
        }
    }
}
