﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse ActualizarBW(BWDtoRequest bw)
        {

           
            return iBWBl.ActualizarBW(bw);
        }

        //public List<BWDtoResponse> ListarBandejaBW(BWDtoRequest bw)
        //{
        //    return iBWBL.ListarBandejaBW(bw);
        //}

        public List<BWDtoResponse> ListarBW(BWDtoRequest bw)
        {
            return iBWBl.ListarBW(bw);
        }

        public BWDtoResponse ObtenerBW(BWDtoRequest bw)
        {
            return iBWBl.ObtenerBW(bw);
        }

        public ProcesoResponse RegistrarBW(BWDtoRequest bw)
        {
            return iBWBl.RegistrarBW(bw);
        }
    }
}
