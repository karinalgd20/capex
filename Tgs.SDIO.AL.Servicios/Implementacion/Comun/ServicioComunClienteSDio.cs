﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun

{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse ActualizarCliente(ClienteDtoRequest cliente)
        {
            return iClienteBl.ActualizarCliente(cliente);
        }
        public List<ClienteDtoResponse> ListarCliente(ClienteDtoRequest cliente)
        {
            return iClienteBl.ListarCliente(cliente);
        }
        public List<ListaDtoResponse> ListarClienteCartaFianza(ClienteDtoRequest cliente)
        {
            return iClienteBl.ListarClienteCartaFianza(cliente);
        }

        public ClienteDtoResponse ObtenerCliente(ClienteDtoRequest cliente)
        {
            return iClienteBl.ObtenerCliente(cliente);
        }
        public ProcesoResponse RegistrarCliente(ClienteDtoRequest cliente)
        {
            return iClienteBl.RegistrarCliente(cliente);
        }

        public ClienteDtoResponsePaginado ListarClientePaginado(ClienteDtoRequest cliente) {
            return iClienteBl.ListarClientePaginado(cliente);
        }
        public ClienteDtoResponse ObtenerClientePorCodigo(ClienteDtoRequest cliente)
        {
            return iClienteBl.ObtenerClientePorCodigo(cliente);
        }
        public List<ClienteDtoResponse> ListarClientePorDescripcion(ClienteDtoRequest cliente)
        {
            return iClienteBl.ListarClientePorDescripcion(cliente);
        }
    }
}
