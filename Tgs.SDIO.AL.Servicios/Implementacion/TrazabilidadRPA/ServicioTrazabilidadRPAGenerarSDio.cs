﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Implementacion.TrazabilidadRPA
{
    
    public partial class ServicioTrazabilidadRPASDio
    {
        public ProcesoResponse GenerarCierreOfertaRPA(CierreDeOfertaRPA request)
        {
            return iOportunidadGanadoraBl.GenerarCierreOfertaRPA(request);
        }

        public ProcesoResponse GenerarEmisionCircuitoRPA(EmisionCircuitoRPA request)
        {
            return iOportunidadGanadoraBl.GenerarEmisionCircuitoRPA(request);
        }
        public ProcesoResponse ActualizarEstadoRPA(string Numero_oportunidad, string EstadoOportunidad)
        {
            return iOportunidadGanadoraBl.ActualizarEstadoRPA(Numero_oportunidad, EstadoOportunidad);
        }
        public OportunidadGanadoraPaginadoDtoResponse ObtenerListaOportunidad()
        {
            return iOportunidadGanadoraBl.ObtenerListaOportunidad();
        }
    }
}
