﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.Servicios.Interfaces.TrazabilidadRPA;

namespace Tgs.SDIO.AL.Servicios.Implementacion.TrazabilidadRPA
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioTrazabilidadRPASDio", Name = "ServicioTrazabilidadRPASDio",
    ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioTrazabilidadRPASDio : IServicioTrazabilidadRPASDio
    {
        public readonly IOportunidadGanadoraBl iOportunidadGanadoraBl;

        public ServicioTrazabilidadRPASDio
        (
            IOportunidadGanadoraBl IOportunidadGanadoraBl
        )

        {
            iOportunidadGanadoraBl = IOportunidadGanadoraBl;
        }
    }
}
