﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Seguridad
{
    public partial class ServicioSeguridadSDio
    {
        public UsuarioDtoResponse ObtenerUsuarioPorLogin(string login)
        {
            return iUsuarioBl.ObtenerUsuarioPorLogin(login);
        }

        public UsuarioDtoResponse ValidarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            return iUsuarioBl.ValidarUsuario(usuarioDtoRequest);
        }

        public string EnvioClaveUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            return iUsuarioBl.EnvioClaveUsuario(usuarioDtoRequest);
        }

        public List<EntidadDtoResponse> ObtenerAmbitoUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            return iUsuarioBl.ObtenerAmbitoUsuario(usuarioDtoRequest);
        }

        public ProcesoResponse RegistrarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            return iUsuarioBl.RegistrarUsuario(usuarioDtoRequest);
        }

        public UsuarioDtoResponse CambiarPassword(UsuarioDtoRequest usuarioDtoRequest)
        {
            return iUsuarioBl.CambiarPassword(usuarioDtoRequest);
        }

        public UsuarioDtoResponse ObtenerUsuarioPerfil(UsuarioDtoRequest usuarioDtoRequest)
        {
            return iUsuarioBl.ObtenerUsuarioPerfil(usuarioDtoRequest);
        }
        public List<UsuarioDtoResponse> ObtenerUsuariosPorPerfil(UsuarioDtoRequest usuarioDtoRequest)
        {
            return iUsuarioBl.ObtenerUsuariosPorPerfil(usuarioDtoRequest);
        }
   
        public UsuarioDtoResponse ObtenerUsuarioPorId(int idUsuario)
        {
            return iUsuarioBl.ObtenerUsuarioPorId(idUsuario);
        }

        public List<UsuarioDtoResponse> ListarUsuariosPaginado(UsuarioDtoRequest usuarioDtoRequest)
        {
            return iUsuarioBl.ListarUsuariosPaginado(usuarioDtoRequest);
        }

        public  ProcesoResponse ActualizarEstadoUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            return iUsuarioBl.ActualizarEstadoUsuario(usuarioDtoRequest); 
        }

        public List<ListaDtoResponse> ListarUsuariosFiltro(UsuarioDtoRequest usuarioDtoRequest)
        {
            return iUsuarioBl.ListarUsuariosFiltro(usuarioDtoRequest);
        }

        public ProcesoResponse ActualizarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            return iUsuarioBl.ActualizarUsuario(usuarioDtoRequest);
        }

    }
}
