﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Seguridad
{
    public partial class ServicioSeguridadSDio
    {
        public ProcesoResponse ActualizarEstadoUsuarioPerfil(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest)
        {
            return iUsuarioPerfilBl.ActualizarEstadoUsuarioPerfil(usuarioPerfilDtoRequest);
        }


        public List<UsuarioPerfilDtoResponse> ListarPerfilesAsignados(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest)
        {
            return iUsuarioPerfilBl.ListarPerfilesAsignados(usuarioPerfilDtoRequest);
        }

        public List<ListaDtoResponse> ListarPerfilesPorSistema()
        {
            return iUsuarioPerfilBl.ListarPerfilesPorSistema();
        }

        public ProcesoResponse RegistrarUsuarioPerfil(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest)
        {
            return iUsuarioPerfilBl.RegistrarUsuarioPerfil(usuarioPerfilDtoRequest);
        }

    }
}
