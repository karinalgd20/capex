﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;
using Tgs.SDIO.Util.OpcionesMenu;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Seguridad
{
    public  partial class ServicioSeguridadSDio
    {
        public List<OpcionMenuResponse> ListarOpcionesUsuario(int idUsuarioEmpresa)
        {
            var opciones = iOpcionMenuBl.ListarOpcionesUsuario(idUsuarioEmpresa);
            foreach (var item in opciones)
            {
                var opcion = item.OpcionMenuDto.NombreOpcion;
                if (opcion.Equals(NombresMenu.Funnel))
                {
                    item.OpcionMenuDto.UrlImage = ImagenesMenu.Funnel;
                    item.OpcionMenuDto.AltlImage = ImagenesMenu.Funnel;
                }
                else
                {
                    if (opcion.Equals(NombresMenu.TrazabilidadF3))
                    {
                        item.OpcionMenuDto.UrlImage = ImagenesMenu.TrazabilidadF3;
                        item.OpcionMenuDto.AltlImage = ImagenesMenu.TrazabilidadF3;
                    }
                }
                ////por quitar 
            }

            return opciones;
        }
    }
}
