﻿using System.ServiceModel;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad;
using Tgs.SDIO.AL.Servicios.Interfaces.Seguridad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Seguridad
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioSeguridad", Name = "ServicioSeguridad",
      ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioSeguridadSDio : IServicioSeguridadSDio
    {
        private readonly IUsuarioBl iUsuarioBl;
        private readonly IOpcionMenuBl iOpcionMenuBl;
        private readonly IPerfilBl iPerfilBl;
        private readonly IUsuarioPerfilBl iUsuarioPerfilBl;

        public ServicioSeguridadSDio(IUsuarioBl usuarioBl, IOpcionMenuBl opcionMenuBl, IPerfilBl perfilBl, IUsuarioPerfilBl usuarioPerfilBl)
        {
            iUsuarioBl = usuarioBl;
            iOpcionMenuBl = opcionMenuBl;
            iPerfilBl = perfilBl;
            iUsuarioPerfilBl = usuarioPerfilBl;
        } 
    }
}
