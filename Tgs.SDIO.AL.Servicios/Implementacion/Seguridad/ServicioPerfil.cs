﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Seguridad
{
    public partial class ServicioSeguridadSDio
    {
        public List<PerfilDtoResponse> ObtenerPerfiles(int idUsuarioSistema)
        {
            return iPerfilBl.ObtenerPerfiles(idUsuarioSistema);
        }
    }
}
