﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Compra

{
    public partial class ServicioCompraSDio
    {
        public ProcesoResponse RegistrarAnexo(AnexoDtoRequest request)
        {
            return iAnexoBl.RegistrarAnexo(request);
        }

        public List<AnexoDtoResponse> ListarAnexos(AnexoDtoRequest request) {
            return iAnexoBl.ListarAnexos(request);
        }

        public bool EliminarAnexo(AnexoDtoRequest request) {
            return iAnexoBl.EliminarAnexo(request);
        }

        public AnexoArchivoDtoResponse ObtenerAnexoArchivo(int iIdAnexoPeticionCompra) {
            return iAnexoArchivoBl.ObtenerAnexoArchivo(iIdAnexoPeticionCompra);
        }
    }
}
