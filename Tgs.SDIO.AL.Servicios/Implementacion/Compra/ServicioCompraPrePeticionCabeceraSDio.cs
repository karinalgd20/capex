﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Compra

{
    public partial class ServicioCompraSDio
    {
        public ProcesoResponse RegistrarPrePeticionCompraCabecera(PrePeticionCabeceraDtoRequest request)
        {
            return iPrePeticionCabeceraBl.RegistrarPrePeticionCompraCabecera(request);
        }

        public List<PrePeticionCabeceraDtoResponse> ListarPrePeticion(PrePeticionCabeceraDtoRequest request) {
            return iPrePeticionCabeceraBl.ListarPrePeticion(request);
        }

        public List<PrePeticionCabeceraDtoResponse> ListarPrePeticionCabecera(PrePeticionCabeceraDtoRequest request)
        {
            return iPrePeticionCabeceraBl.ListarPrePeticionCabecera(request);
        }
        
        public PrePeticionCabeceraDtoResponse ObtenerPrePeticionCabecera(PrePeticionCabeceraDtoRequest request) {
            return iPrePeticionCabeceraBl.ObtenerPrePeticionCabecera(request);
        }

        public int ValidacionMontoLimite(PrePeticionCabeceraDtoRequest request) {
            return iPrePeticionCabeceraBl.ValidacionMontoLimite(request);
        }

        public string ObtenerSaldo(PrePeticionCabeceraDtoRequest request)
        {
            return iPrePeticionCabeceraBl.ObtenerSaldo(request);
        }
    }
}
