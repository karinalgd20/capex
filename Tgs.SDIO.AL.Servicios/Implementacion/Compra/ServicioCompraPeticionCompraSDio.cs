﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Compra
{
    public partial class ServicioCompraSDio
    {
        public PeticionCompraPaginadoDtoResponse ListarPeticionCompraPaginado(PeticionCompraDtoRequest request)
        {
            return iPeticionCompraBl.ListarPeticionCompraPaginado(request);
        }

        public ProcesoResponse RegistrarPeticionCompra(PeticionCompraDtoRequest request)
        {
            return iPeticionCompraBl.RegistrarPeticionCompra(request);
        }

        public ProcesoResponse ActualizarPeticionCompra(PeticionCompraDtoRequest request)
        {
            return iPeticionCompraBl.ActualizarPeticionCompra(request);
        }

        public PeticionCompraDtoResponse ObtenerPeticionCompraPorId(PeticionCompraDtoRequest request)
        {
            return iPeticionCompraBl.ObtenerPeticionCompraPorId(request);
        }

        public ProcesoResponse ActualizarPeticionCompraDetalleCompra(PeticionCompraDtoRequest request)
        {
            return iPeticionCompraBl.ActualizarPeticionCompraDetalleCompra(request);
        }

        public List<AprobacionDtoResponse> ListaAprobaciones(PeticionCompraDtoRequest request)
        {
            return iPeticionCompraBl.ListaAprobaciones(request);
        }

    }
}
