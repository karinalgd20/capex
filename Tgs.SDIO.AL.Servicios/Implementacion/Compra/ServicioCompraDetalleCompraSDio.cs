﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Compra
{
    public partial class ServicioCompraSDio
    {
        public List<DetalleCompraDtoResponse> ListarDetalleCompra(DetalleCompraDtoRequest request)
        {
            return iDetalleCompraBl.ListarDetalleCompra(request);
        }

        public ProcesoResponse RegistrarDetalleCompra(DetalleCompraDtoRequest request)
        {
            return iDetalleCompraBl.RegistrarDetalleCompra(request);
        }

        public DetalleCompraDtoResponse ObtenerDetalleCompraPorId(DetalleCompraDtoRequest request)
        {
            return iDetalleCompraBl.ObtenerDetalleCompraPorId(request);
        }

        public bool EliminarDetalleCompra(DetalleCompraDtoRequest request)
        {
            return iDetalleCompraBl.EliminarDetalleCompra(request);
        }

    }
}
