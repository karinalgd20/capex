﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Compra

{
    public partial class ServicioCompraSDio
    {
        public ProcesoResponse RegistrarPrePeticionDCMDetalle(PrePeticionDetalleDCMDetalleDtoRequest request)
        {
            return iPrePeticionDCMDetalleBl.RegistrarPrePeticionDCMDetalle(request);
        }

        public List<PrePeticionDCMDetalleDtoResponse> ListarPrePeticionDCM(PrePeticionDetalleDCMDetalleDtoRequest request) {
            return iPrePeticionDCMDetalleBl.ListarPrePeticionDCM(request);
        }

        public PrePeticionDCMDetalleDtoResponse ObtenerDetalleDCM(PrePeticionDetalleDCMDetalleDtoRequest request) {
            return iPrePeticionDCMDetalleBl.ObtenerDetalleDCM(request);
        }
    }
}
