﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Compra
{
    public partial class ServicioCompraSDio
    {
        public List<ConfiguracionEtapaDtoResponse> ListarEtapas(ConfiguracionEtapaDtoRequest request) { 
            return iConfiguracionEtapaBl.ListarEtapas(request);
        }
    }
}
