﻿using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Compra

{
    public partial class ServicioCompraSDio
    {
        public GestionDtoResponse ObtenerGestion(GestionDtoRequest request)
        {
            return iGestionBl.ObtenerGestion(request);
        }
        public ProcesoResponse RegistrarGestion(GestionDtoRequest request)
        {
            return iGestionBl.RegistrarGestion(request);
        }
    }
}
