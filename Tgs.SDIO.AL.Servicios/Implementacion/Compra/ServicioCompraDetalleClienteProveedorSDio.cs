﻿using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Compra

{
    public partial class ServicioCompraSDio
    {
        public DetalleClienteProveedorDtoResponse ObtenerDetalleClienteProveedor(DetalleClienteProveedorDtoRequest request)
        {
            return iDetalleClienteProveedorBl.ObtenerDetalleClienteProveedor(request);
        }
        public ProcesoResponse RegistrarDetalleClienteProveedor(DetalleClienteProveedorDtoRequest request)
        {
            return iDetalleClienteProveedorBl.RegistrarDetalleClienteProveedor(request);
        }
    }
}
