﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Compra

{
    public partial class ServicioCompraSDio
    {
        public ProcesoResponse RegistrarPrePeticionOrdinariaDetalle(PrePeticionOrdinariaDetalleDtoRequest request)
        {
            return iPrePeticionOrdinariaDetalleBl.RegistrarPrePeticionOrdinariaDetalle(request);
        }

        public List<PrePeticionOrdinariaDetalleDtoResponse> ListarPrePeticionOrdinaria(PrePeticionOrdinariaDetalleDtoRequest request) {
            return iPrePeticionOrdinariaDetalleBl.ListarPrePeticionOrdinaria(request);
        }

        public PrePeticionOrdinariaDetalleDtoResponse ObtenerDetalleOrdinaria(PrePeticionOrdinariaDetalleDtoRequest request) {
            return iPrePeticionOrdinariaDetalleBl.ObtenerDetalleOrdinaria(request);
        }
    }
}
