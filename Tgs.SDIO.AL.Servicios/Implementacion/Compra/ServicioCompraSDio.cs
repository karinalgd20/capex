﻿using System.ServiceModel;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.Servicios.Interfaces.Compra;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Compra
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioCompraSDio", Name = "ServicioCompraSDio",
        ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioCompraSDio : IServicioCompraSDio
    {
        public readonly IDetalleClienteProveedorBl iDetalleClienteProveedorBl;
        public readonly ICentroCostoRecursoBl iCentroCostoRecursoBl;
        public readonly IPeticionCompraBl iPeticionCompraBl;
        public readonly ICuentaBl iCuentaBl;
        public readonly IDetalleCompraBl iDetalleCompraBl;
        public readonly IGestionBl iGestionBl;
        public readonly IAnexoBl iAnexoBl;
        public readonly IEtapaPeticionCompraBl iEtapaPeticionCompraBl;
        public readonly IConfiguracionEtapaBl iConfiguracionEtapaBl;
        public readonly IAnexoArchivoBl iAnexoArchivoBl;
        public readonly IPrePeticionCabeceraBl iPrePeticionCabeceraBl;
        public readonly IPrePeticionDCMDetalleBl iPrePeticionDCMDetalleBl;
        public readonly IPrePeticionOrdinariaDetalleBl iPrePeticionOrdinariaDetalleBl;

        public ServicioCompraSDio(IDetalleClienteProveedorBl IDetalleClienteProveedorBl,
                                  ICentroCostoRecursoBl ICentroCostoRecursoBl,
                                  IPeticionCompraBl IPeticionCompraBl,
                                  ICuentaBl ICuentaBl,
                                  IDetalleCompraBl IDetalleCompraBl,
                                  IGestionBl IGestionBl,
                                  IAnexoBl IAnexoBl,
                                  IEtapaPeticionCompraBl IEtapaPeticionCompraBl,
                                  IConfiguracionEtapaBl IConfiguracionEtapaBl,
                                  IAnexoArchivoBl IAnexoArchivoBl,
                                  IPrePeticionCabeceraBl IPrePeticionCabeceraBl,
                                  IPrePeticionDCMDetalleBl IPrePeticionDCMDetalleBl,
                                  IPrePeticionOrdinariaDetalleBl IPrePeticionOrdinariaDetalleBl)
        {
            iDetalleClienteProveedorBl = IDetalleClienteProveedorBl;
            iCentroCostoRecursoBl = ICentroCostoRecursoBl;
            iPeticionCompraBl = IPeticionCompraBl;
            iCuentaBl = ICuentaBl;
            iDetalleCompraBl = IDetalleCompraBl;
            iGestionBl = IGestionBl;
            iAnexoBl = IAnexoBl;
            iEtapaPeticionCompraBl = IEtapaPeticionCompraBl;
            iConfiguracionEtapaBl = IConfiguracionEtapaBl;
            iAnexoArchivoBl = IAnexoArchivoBl;
            iPrePeticionCabeceraBl = IPrePeticionCabeceraBl;
            iPrePeticionDCMDetalleBl = IPrePeticionDCMDetalleBl;
            iPrePeticionOrdinariaDetalleBl = IPrePeticionOrdinariaDetalleBl;
        }
    }
}