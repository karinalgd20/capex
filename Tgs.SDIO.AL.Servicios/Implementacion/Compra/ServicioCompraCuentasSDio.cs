﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Compra
{
    public partial class ServicioCompraSDio
    {

        public CuentaPaginadoDtoResponse ListarCuentaPaginado(CuentaDtoRequest request)
        {
            return iCuentaBl.ListarCuentaPaginado(request);
        }

    }
}
