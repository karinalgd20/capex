﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Negocio
{
    public partial class ServicioNegocioSDio
    {
       
        public ProcesoResponse AsociarServicioSisegos(AsociarServicioSisegosDtoRequest request)
        {
            return iAsociarServicioSisegosBl.AsociarServicioSisegos(request);
        }
        public List<AsociarServicioSisegosDtoResponse> ListarServicioSisegos(AsociarServicioSisegosDtoRequest request)
        {
            return iAsociarServicioSisegosBl.ListarServicioSisegos(request);
        }
        public AsociarServicioSisegosPaginadoDtoResponse ListarServicioSisegosPaginado(AsociarServicioSisegosDtoRequest request)
        {
            return iAsociarServicioSisegosBl.ListarServicioSisegosPaginado(request);
        }
        public RPAServiciosxNroOfertaDtoResponse ObtenerServicioById(AsociarServicioSisegosDtoRequest request)
        {
            return iAsociarServicioSisegosBl.ObtenerServicioById(request);
        }
        public RPAEquiposServicioDtoResponse ObtenerEquipoById(AsociarServicioSisegosDtoRequest request)
        {
            return iAsociarServicioSisegosBl.ObtenerEquipoById(request);
        }
        //public RPASisegoDetalleDtoResponse ObtenerCodSisegoById(AsociarServicioSisegosDtoRequest request)
        //{
        //    return iAsociarServicioSisegosBl.ObtenerCodSisegoById(request);
        //}
    }
}
