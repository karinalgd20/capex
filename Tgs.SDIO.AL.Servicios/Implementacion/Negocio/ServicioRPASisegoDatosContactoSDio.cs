﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Negocio
{
    public partial class ServicioNegocioSDio
    {
       
        public ProcesoResponse ActualizarContactoServicio(RPASisegoDatosContactoDtoRequest resquest)
        {
            return iRPASisegoDatosContactoBl.ActualizarContactoServicio(resquest);
        }
       
        public ProcesoResponse RegistrarContactoServicio(RPASisegoDatosContactoDtoRequest resquest)
        {
            return iRPASisegoDatosContactoBl.RegistrarContactoServicio(resquest);
        }
        public List<RPASisegoDatosContactoDtoResponse> ListaDetallesContactoRPA(RPASisegoDatosContactoDtoRequest resquest)
        {
            return iRPASisegoDatosContactoBl.ListaDetallesContactoRPA(resquest);
        }
        public RPASisegoDatosContactoPaginadoDtoResponse ListaDetallesContactoRPAPaginadoDtoResponse(RPASisegoDatosContactoDtoRequest resquest)
        {
            return iRPASisegoDatosContactoBl.ListaDetallesContactoRPAPaginadoDtoResponse(resquest);
        }
    }
}
