﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Negocio
{
    public partial class ServicioNegocioSDio
    {
        public ProcesoResponse AgregarSisegoCotizado(SisegoCotizadoDtoRequest resquest)
        {
            return iSisegoCotizadoBl.AgregarSisegoCotizado(resquest);
        }

        public ProcesoResponse EliminarSisegoCotizado(SisegoCotizadoDtoRequest resquest)
        {
            return iSisegoCotizadoBl.EliminarSisegoCotizado(resquest);
        }

        public List<SisegoCotizadoDtoResponse> ListarSisegosCotizados(SisegoCotizadoDtoRequest resquest)
        {
            return iSisegoCotizadoBl.ListarSisegosCotizados(resquest);
        }

        public SisegoCotizadoPaginadoDtoResponse ListarSisegosCotizadosPaginado(SisegoCotizadoDtoRequest resquest)
        {
            return iSisegoCotizadoBl.ListarSisegoCotizadoPaginado(resquest);
        }
    }
}
