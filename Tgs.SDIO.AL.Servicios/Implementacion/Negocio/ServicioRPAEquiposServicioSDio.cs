﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Negocio
{
    public partial class ServicioNegocioSDio
    {
        
        public ProcesoResponse AgregarEquipoServicio(RPAEquiposServicioDtoRequest request)
        {
            return iRPAEquiposServicioBl.AgregarEquipoServicio(request);
        }
        
        public List<RPAEquiposServicioDtoResponse> ListarEquipoServicio(RPAEquiposServicioDtoRequest request)
        {
            return iRPAEquiposServicioBl.ListarEquipoServicio(request);
        }
       
        public RPAEquiposServicioPaginadoDtoResponse ListarEquipoServicioPaginado(RPAEquiposServicioDtoRequest request)
        {
            return iRPAEquiposServicioBl.ListarEquipoServicioPaginado(request);
        }
    }
}
