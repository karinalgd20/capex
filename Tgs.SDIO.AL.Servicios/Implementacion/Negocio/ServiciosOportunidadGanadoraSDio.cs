﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Negocio
{
    public partial class ServicioNegocioSDio
    {
        public ProcesoResponse RegistrarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraBl.AgregarOportunidadGanadora(request);
        }

        public ProcesoResponse ValidarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraBl.ValidarOportunidadGanadora(request);
        }

        public ProcesoResponse EliminarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraBl.EliminarOportunidadGanadora(request);
        }
        public OportunidadGanadoraDtoResponse ObtenerOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraBl.ObtenerOportunidadGanadora(request);
        }
        public SalesForceConsolidadoCabeceraDtoResponse ObtenerCasoOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraBl.ObtenerCasoOportunidadGanadora(request);
        }
        public ClienteDtoResponse ObtenerRucClientePorIdOportunidad(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraBl.ObtenerRucClientePorIdOportunidad(request);
        }
        public ProcesoResponse ActualizarEtapaCierreOportunidad(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraBl.ActualizarEtapaCierreOportunidad(request);
        }
        public List<OportunidadGanadoraDtoResponse> ListarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraBl.ListarOportunidadGanadora(request);
        }
        public BandejaOportunidadPaginadoDtoResponse ListarBandejaOportunidadPaginado(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraBl.ListarBandejaOportunidadPaginado(request);
        }

        public OportunidadGanadoraDtoResponse ObtenerOportunidadById(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraBl.ObtenerOportunidadById(request);
        }
        
    }
}
