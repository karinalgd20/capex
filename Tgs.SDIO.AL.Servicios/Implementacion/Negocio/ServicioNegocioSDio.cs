﻿using System.ServiceModel;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.Servicios.Interfaces.Negocio;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Negocio
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioNegocioSDio", Name = "ServicioNegocioSDio",
        ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioNegocioSDio : IServicioNegocioSDio
    {
        public readonly IIsisNroOfertaBl iIsisNroOfertaBl;
        public readonly ISisegoCotizadoBl iSisegoCotizadoBl;
        public readonly IOportunidadGanadoraBl iOportunidadGanadoraBl;
        public readonly IAsociarNroOfertaSisegosBl iAsociarNroOfertaSisegosBl;
        public readonly IAsociarServicioSisegosBl iAsociarServicioSisegosBl;
        public readonly IRPASisegoDetalleBl iRPASisegoDetalleBl;
        public readonly IRPAServiciosxNroOfertaBl iRPAServiciosxNroOfertaBl;
        public readonly IRPAEquiposServicioBl iRPAEquiposServicioBl;
        public readonly IRPASisegoDatosContactoBl iRPASisegoDatosContactoBl;
        public ServicioNegocioSDio(IIsisNroOfertaBl IIsisNroOfertaBl, ISisegoCotizadoBl ISisegoCotizadoBl,
                                   IOportunidadGanadoraBl IOportunidadGanadoraBl, IAsociarNroOfertaSisegosBl IAsociarNroOfertaSisegosBl,
                                   IAsociarServicioSisegosBl IAsociarServicioSisegosBl, IRPASisegoDetalleBl IRPASisegoDetalleBl,
                                   IRPAServiciosxNroOfertaBl IRPAServiciosxNroOfertaBl, IRPAEquiposServicioBl IRPAEquiposServicioBl,
                                   IRPASisegoDatosContactoBl IRPASisegoDatosContactoBl)
        {
            iIsisNroOfertaBl = IIsisNroOfertaBl;
            iSisegoCotizadoBl = ISisegoCotizadoBl;
            iOportunidadGanadoraBl = IOportunidadGanadoraBl;
            iAsociarNroOfertaSisegosBl = IAsociarNroOfertaSisegosBl;
            iAsociarServicioSisegosBl = IAsociarServicioSisegosBl;
            iRPASisegoDetalleBl = IRPASisegoDetalleBl;
            iRPAServiciosxNroOfertaBl = IRPAServiciosxNroOfertaBl;
            iRPAEquiposServicioBl = IRPAEquiposServicioBl;
            iRPASisegoDatosContactoBl = IRPASisegoDatosContactoBl;
        }

    }

}
