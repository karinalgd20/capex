﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Negocio
{
    public partial class ServicioNegocioSDio
    {
        public ProcesoResponse AgregarIsisNroOferta(IsisNroOfertaDtoRequest resquest)
        {
            return iIsisNroOfertaBl.AgregarIsisNroOferta(resquest);
        }

        public ProcesoResponse EliminarIsisNroOferta(IsisNroOfertaDtoRequest resquest)
        {
            return iIsisNroOfertaBl.EliminarIsisNroOferta(resquest);
        }

        public List<IsisNroOfertaDtoResponse> ListarIsisNroOferta(IsisNroOfertaDtoRequest resquest)
        {
            return iIsisNroOfertaBl.ListarNroOferta(resquest);
        }
        public IsisNroOfertaPaginadoDtoResponse ListarIsisNroOfertaPaginado(IsisNroOfertaDtoRequest resquest)
        {
            return iIsisNroOfertaBl.ListarNroOfertaPaginado(resquest);
        }
    }
}
