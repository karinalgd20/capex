﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Negocio
{
    public partial class ServicioNegocioSDio
    {
        
        public ProcesoResponse AgregarOfertaSisegos(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            return iAsociarNroOfertaSisegosBl.AgregarOfertaSisegos(resquest);
        }
        public ProcesoResponse EliminarOfertaSisego(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            return iAsociarNroOfertaSisegosBl.EliminarOfertaSisego(resquest);
        }
        public IsisNroOfertaDtoResponse ObtenerNroOfertaById(AsociarNroOfertaSisegosDtoRequest request)
        {
            return iAsociarNroOfertaSisegosBl.ObtenerNroOfertaById(request);
        }
        public SisegoCotizadoDtoResponse ObtenerCodSisegoById(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            return iAsociarNroOfertaSisegosBl.ObtenerCodSisegoById(resquest);
        }
        public List<AsociarNroOfertaSisegosDtoResponse> ListarOfertaSisegos(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            return iAsociarNroOfertaSisegosBl.ListarOfertaSisegos(resquest);
        }
   
        public AsociarOfertaSisegosPaginadoDtoResponse ListarOfertaSisegosPaginado(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            return iAsociarNroOfertaSisegosBl.ListarOfertaSisegosPaginado(resquest);
        }
    }
}
