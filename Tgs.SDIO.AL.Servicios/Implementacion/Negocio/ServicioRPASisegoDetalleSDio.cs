﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Negocio
{
    public partial class ServicioNegocioSDio
    {
        public ProcesoResponse AgregarSisegoDetalle(RPASisegoDetalleDtoRequest request)
        {
            return iRPASisegoDetalleBl.AgregarSisegoDetalle(request);
        }
        public List<RPASisegoDetalleDtoResponse> ListarSisegoDetalle(RPASisegoDetalleDtoRequest request)
        {
            return iRPASisegoDetalleBl.ListarSisegoDetalle(request);
        }
        public RPASisegoDetallePaginadoDtoResponse ListarSisegoDetallePaginado(RPASisegoDetalleDtoRequest request)
        {
            return iRPASisegoDetalleBl.ListarSisegoDetallePaginado(request);
        }
    }
}
