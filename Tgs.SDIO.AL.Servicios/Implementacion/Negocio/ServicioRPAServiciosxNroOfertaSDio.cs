﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Negocio
{
    public partial class ServicioNegocioSDio
    {
        public ProcesoResponse AgregarServiciosNroOferta(RPAServiciosxNroOfertaDtoRequest request)
        {
            return iRPAServiciosxNroOfertaBl.AgregarServiciosNroOferta(request); 
        }
        public List<RPAServiciosxNroOfertaDtoResponse> ListarServiciosNroOferta(RPAServiciosxNroOfertaDtoRequest request)
        {
            return iRPAServiciosxNroOfertaBl.ListarServiciosNroOferta(request);
        }
        public RPAServiciosxNroOfertaPaginadoDtoResponse ListarServiciosNroOfertaPaginado(RPAServiciosxNroOfertaDtoRequest request)
        {
            return iRPAServiciosxNroOfertaBl.ListarServiciosNroOfertaPaginado(request);
        }
    }
}
