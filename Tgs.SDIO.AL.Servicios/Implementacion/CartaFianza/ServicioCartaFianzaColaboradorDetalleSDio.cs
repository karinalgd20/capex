﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.CartaFianza
{
    public partial class ServicioCartaFianzaGeneralSDio 
    {

        public List<CartaFianzaColaboradorDetalleResponse> ListarColaborador(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            return iCartaFianzaColaboradorDetalleBl.ListarColaborador(cartaFianzaColaboradorDetalleRequest);
        }

        public ProcesoResponse AsignarSupervisorAGerenteCuenta(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            return iCartaFianzaColaboradorDetalleBl.AsignarSupervisorAGerenteCuenta(cartaFianzaColaboradorDetalleRequest);
        }

        public ProcesoResponse AsignarGerenteCuentaACartaFianza(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            return iCartaFianzaColaboradorDetalleBl.AsignarGerenteCuentaACartaFianza(cartaFianzaColaboradorDetalleRequest);
        }

        public ProcesoResponse ActualizarTesorera(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            return iCartaFianzaColaboradorDetalleBl.ActualizarTesorera(cartaFianzaColaboradorDetalleRequest);
        }
    }
}
