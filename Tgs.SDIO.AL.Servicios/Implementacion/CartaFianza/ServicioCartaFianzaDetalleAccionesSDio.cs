﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.CartaFianza
{
    public partial class ServicioCartaFianzaGeneralSDio
    {
        public ProcesoResponse ActualizaCartaFianzaDetalleAccion(CartaFianzaDetalleAccionRequest cartaFianzaDetalleAccionRequestActualiza)
        {
            return iCartaFianzaDetalleAccionesBl.ActualizaCartaFianzaDetalleAccion(cartaFianzaDetalleAccionRequestActualiza);
        }

        public ProcesoResponse InsertaCartaFianzaDetalleAccion(CartaFianzaDetalleAccionRequest cartaFianzaDetalleAccionRequestInserta)
        {
            return iCartaFianzaDetalleAccionesBl.InsertaCartaFianzaDetalleAccion(cartaFianzaDetalleAccionRequestInserta);
        }

        public List<CartaFianzaDetalleAccionResponse> ListaCartaFianzaDetalleAccionId(CartaFianzaDetalleAccionRequest cartaFianzaDetalleAccionRequestLista)
        {
        
            return iCartaFianzaDetalleAccionesBl.ListaCartaFianzaDetalleAccionId(cartaFianzaDetalleAccionRequestLista);           
       
        }
    }
}
