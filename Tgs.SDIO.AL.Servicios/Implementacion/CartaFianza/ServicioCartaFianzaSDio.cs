﻿using System;
using System.Collections.Generic;
using Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.CartaFianza
{

    public partial class ServicioCartaFianzaGeneralSDio
    {


        public ProcesoResponse ActualizaARenovacionCartaFianza(CartaFianzaDetalleRenovacionRequest CartaFianzaRequestActualiza)
        {
            return iCartaFianzaBl.ActualizaARenovacionCartaFianza(CartaFianzaRequestActualiza);
        }
        public ProcesoResponse ActualizaCartaFianza(CartaFianzaRequest cartaFianzaRequestActualiza)
        {
            return iCartaFianzaBl.ActualizaCartaFianza(cartaFianzaRequestActualiza);
        }

        public ProcesoResponse InsertaCartaFianza(CartaFianzaRequest cartaFianzaRequestActualiza)
        {
            return iCartaFianzaBl.InsertaCartaFianza(cartaFianzaRequestActualiza);
        }

        public CartaFianzaListaResponsePaginado ListaCartaFianza(CartaFianzaRequest cartaFianzaRequestActualiza)
        {
            return iCartaFianzaBl.ListaCartaFianza(cartaFianzaRequestActualiza);
        }

        public List<CartaFianzaListaResponse> ListaCartaFianzaId(CartaFianzaRequest cartaFianzaRequestActualiza)
        {
            return iCartaFianzaBl.ListaCartaFianzaId(cartaFianzaRequestActualiza);           
        }


        public MailResponse ObtenerDatosCorreo(MailRequest mailResponse)
        {
            return iCartaFianzaBl.ObtenerDatosCorreo(mailResponse);
        }
        public ProcesoResponse ActualizarCartaFianzaRequerida(CartaFianzaRequest cartaFianzaRequestActualiza)
        {
            return iCartaFianzaBl.ActualizarCartaFianzaRequerida( cartaFianzaRequestActualiza);
        }
		
		public DashBoardCartaFianzaResponse ListaDatos_DashoBoard_CartaFianza(DashBoardCartaFianzaRequest dashBoardCartaFianza) {
            return iCartaFianzaBl.ListaDatos_DashoBoard_CartaFianza(dashBoardCartaFianza);
        }

        public List<CartaFianzaListaResponse> ListaCartaFianzaCombo(CartaFianzaRequest cartaFianzaRequestLista)
        {
            return iCartaFianzaBl.ListaCartaFianzaCombo(cartaFianzaRequestLista);
        }

        public List<DashBoardCartaFianzaListGResponse> ListaDatos_DashoBoard_CartaFianza_Pie(DashBoardCartaFianzaRequest dashBoardCartaFianza)
        {
            return iCartaFianzaBl.ListaDatos_DashoBoard_CartaFianza_Pie(dashBoardCartaFianza);
        }

        public List<CartaFianzaListaResponse> CartasFianzasSinRespuesta(CartaFianzaRequest cartaFianzaRequest)
        {
            return iCartaFianzaBl.CartasFianzasSinRespuesta(cartaFianzaRequest);
        }

        public ProcesoResponse GuardarRespuestaGerenteCuenta(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacion)
        {
            return iCartaFianzaBl.GuardarRespuestaGerenteCuenta(cartaFianzaRenovacion);
        }

        public ProcesoResponse GuardarConfirmacionGerenteCuenta(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacion)
        {
            return iCartaFianzaBl.GuardarConfirmacionGerenteCuenta(cartaFianzaRenovacion);
        }

    }
}
