﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.CartaFianza
{
    public partial class ServicioCartaFianzaGeneralSDio 
    {
        public ProcesoResponse ActualizaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest cartaFianzaDetalleRenovacionActualiza)
        {
            return iCartaFianzaDetalleRenovacionBl.ActualizaCartaFianzaDetalleRenovacion(cartaFianzaDetalleRenovacionActualiza);
        }
        public MailResponse ObtenerDatosCorreoRenovacion(MailRequest mailRequest)
        {
            return iCartaFianzaDetalleRenovacionBl.ObtenerDatosCorreoRenovacion(mailRequest);
        }

        public ProcesoResponse EnviarCorreoCartaFianza(MailRequest mailRequest)
        {
            return iCartaFianzaDetalleRenovacionBl.EnviarCorreoCartaFianza(mailRequest);
        }

        public CartaFianzaDetalleRenovacionResponsePaginado ListaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest cartaFianzaDetalleRenovacionLista)
        {
            return iCartaFianzaDetalleRenovacionBl.ListaCartaFianzaDetalleRenovacion(cartaFianzaDetalleRenovacionLista);
        }
    }
}
