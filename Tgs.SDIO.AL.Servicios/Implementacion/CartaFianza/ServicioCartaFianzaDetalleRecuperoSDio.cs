﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.DataContracts.Dto.Response.CartaFianza.CartaFianzaDetalleSeguimientoResponse;

namespace Tgs.SDIO.AL.Servicios.Implementacion.CartaFianza
{
    public partial class ServicioCartaFianzaGeneralSDio 
    {
        public ProcesoResponse ActualizaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest CartaFianzaDetalleRecuperoActualiza)
        {
            return iCartaFianzaDetalleRecuperoBl.ActualizaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoActualiza);
        }

        public CartaFianzaDetalleRecuperoResponsePaginado ListaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest CartaFianzaDetalleRecuperoLista)
        {
            return iCartaFianzaDetalleRecuperoBl.ListaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoLista);
        }
    }
}
