﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.CartaFianza
{
    public partial class ServicioCartaFianzaGeneralSDio
    {
        public CartaFianzaDetalleSeguimientoResponsePaginado ListaCartaFianzaDetalleSeguimiento(CartaFianzaDetalleSeguimientoRequest cartaFianzaDetalleRecuperoLista)
        {
            return iCartaFianzaDetalleSeguimientoBl.ListaCartaFianzaDetalleSeguimiento(cartaFianzaDetalleRecuperoLista);
        }

        public ProcesoResponse InsertaCartaFianzaSeguimiento(CartaFianzaDetalleSeguimientoRequest cartaFianzaDetalleSeguimiento)
        {
            return iCartaFianzaDetalleSeguimientoBl.InsertaCartaFianzaSeguimiento(cartaFianzaDetalleSeguimiento);
        }
    }
}
