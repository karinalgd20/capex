﻿using System;
using System.ServiceModel;
using Tgs.SDIO.AL.BussinesLayer.Interfaces;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza;
using Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;


namespace Tgs.SDIO.AL.Servicios.Implementacion.CartaFianza
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioCartaFianzaGeneralSDio", Name = "ServicioCartaFianzaGeneralSDio",
      ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioCartaFianzaGeneralSDio : IServicioCartaFianzaGeneralSDio
    { 
        public readonly ICartaFianzaBl iCartaFianzaBl;
        public readonly ICartaFianzaColaboradorDetalleBl iCartaFianzaColaboradorDetalleBl;
        public readonly ICartaFianzaDetalleAccionBl iCartaFianzaDetalleAccionesBl;
        public readonly ICartaFianzaDetalleEstadoBl iCartaFianzaDetalleEstadoBl;
        public readonly ICartaFianzaDetalleRecuperoBl iCartaFianzaDetalleRecuperoBl;
        public readonly ICartaFianzaDetalleRenovacionBl iCartaFianzaDetalleRenovacionBl;
        public readonly ICartaFianzaDetalleSeguimientoBl iCartaFianzaDetalleSeguimientoBl;

        public ServicioCartaFianzaGeneralSDio(
                ICartaFianzaBl ICartaFianzaB,
                ICartaFianzaColaboradorDetalleBl ICartaFianzaColaboradorDetalleB,
                ICartaFianzaDetalleAccionBl ICartaFianzaDetalleAccionesB,
                ICartaFianzaDetalleEstadoBl ICartaFianzaDetalleEstadoB,
                ICartaFianzaDetalleRecuperoBl ICartaFianzaDetalleRecuperoB,
               ICartaFianzaDetalleRenovacionBl ICartaFianzaDetalleRenovacionB,
               ICartaFianzaDetalleSeguimientoBl ICartaFianzaDetalleSeguimientoBl)
        {

            iCartaFianzaBl = ICartaFianzaB;
            iCartaFianzaColaboradorDetalleBl = ICartaFianzaColaboradorDetalleB;
            iCartaFianzaDetalleAccionesBl = ICartaFianzaDetalleAccionesB;
            iCartaFianzaDetalleEstadoBl = ICartaFianzaDetalleEstadoB;
            iCartaFianzaDetalleRecuperoBl = ICartaFianzaDetalleRecuperoB;
            iCartaFianzaDetalleRenovacionBl = ICartaFianzaDetalleRenovacionB;
            iCartaFianzaDetalleSeguimientoBl = ICartaFianzaDetalleSeguimientoBl;
        }

     
    }
}
