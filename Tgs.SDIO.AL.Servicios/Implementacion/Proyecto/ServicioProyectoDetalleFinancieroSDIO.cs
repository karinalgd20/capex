﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Proyecto
{
    public partial class ServicioProyectoSDio
    {
        public List<DetalleFinancieroDtoResponse> ListarDetalleFinanciero(int iIdProyecto) {
            return iDetalleFinancieroBl.ListarDetalleFinanciero(iIdProyecto);
        }

    }
}
