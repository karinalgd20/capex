﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Proyecto
{
    public partial class ServicioProyectoSDio
    {
        public ProcesoResponse InsertarDetallePlazo(DetallePlazoDtoRequest detalleplazo)
        {
            return iDetallePlazoBl.InsertarDetallePlazo(detalleplazo);
        }
        public List<DetallePlazoDtoResponse> ListarDetallePlazo(int iIdProyecto)
        {
            return iDetallePlazoBl.ListarDetallePlazo(iIdProyecto);
        }
    }
}
