﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.Servicios.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Proyecto
{
    public partial class ServicioProyectoSDio
    {
        public ProcesoResponse InsertarServicioCMIFinanciero(ServicioCMIFinancieroDtoRequest request)
        {
            return iServicioCMIFinancieroBl.InsertarServicioCMIFinanciero(request);
        }

        public ServicioCMIFinancieroDtoResponse ObtenerServicioCMIFinanciero(ServicioCMIFinancieroDtoRequest request) {
            return iServicioCMIFinancieroBl.ObtenerServicioCMIFinanciero(request);
        }
    }
}
