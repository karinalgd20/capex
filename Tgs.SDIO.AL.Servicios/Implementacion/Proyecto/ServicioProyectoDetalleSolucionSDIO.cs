﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Proyecto
{
    public partial class ServicioProyectoSDio
    {

        public List<DetalleSolucionDtoResponse> ObtenerDetallePorIdProyecto(DetalleSolucionDtoRequest request)
        {
            return iDetalleSolucionBl.ObtenerDetallePorIdProyecto(request);
        }

        public DetalleSolucionDtoResponse ObtenerDetalleSolucionById(DetalleSolucionDtoRequest request)
        {
            return iDetalleSolucionBl.ObtenerDetalleSolucionById(request);
        }

        public ProcesoResponse RegistrarDetalleSolucion(DetalleSolucionDtoRequest request)
        {
            return iDetalleSolucionBl.RegistrarDetalleSolucion(request);
        }

        public ProcesoResponse EliminarDetalleSolucion(DetalleSolucionDtoRequest request)
        {
            return iDetalleSolucionBl.EliminarDetalleSolucion(request);
        }

    }
}
