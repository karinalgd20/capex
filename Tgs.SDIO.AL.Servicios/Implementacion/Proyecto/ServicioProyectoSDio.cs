﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.Servicios.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Proyecto
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioProyectoSDio", Name = "ServicioProyectoSDio",
        ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioProyectoSDio : IServicioProyectoSDio
    {
        public readonly IProyectoBl iProyectoBl;
        public readonly IDetalleSolucionBl iDetalleSolucionBl;
        public readonly IDetalleFacturarBl iDetalleFacturarBl;
        public readonly IDetalleFinancieroBl iDetalleFinancieroBl;

        public readonly IDetallePlazoBl iDetallePlazoBl;
        public readonly IRecursoProyectoBl iRecursoProyectoBl;
        public readonly IDocumentacionProyectoBl iDocumentacionProyectoBl;
        public readonly IDocumentacionArchivoBl iDocumentacionArchivoBl;

        public readonly IServicioCMIFinancieroBl iServicioCMIFinancieroBl;

        public ServicioProyectoSDio(IProyectoBl IProyectoBl, IDetallePlazoBl IDetallePlazoBl, IRecursoProyectoBl IRecursoProyectoBl, IDocumentacionProyectoBl IDocumentacionProyectoBl, IDocumentacionArchivoBl IDocumentacionArchivoBl, IDetalleSolucionBl IDetalleSolucionBl, IDetalleFacturarBl IDetalleFacturarBl, IServicioCMIFinancieroBl IServicioCMIFinancieroBl, IDetalleFinancieroBl IDetalleFinancieroBl)
        {
            iProyectoBl = IProyectoBl;
            iDetalleSolucionBl = IDetalleSolucionBl;
            iDetalleFacturarBl = IDetalleFacturarBl;
            iDetallePlazoBl = IDetallePlazoBl;
            iRecursoProyectoBl = IRecursoProyectoBl;
            iDocumentacionProyectoBl = IDocumentacionProyectoBl;
            iDocumentacionArchivoBl = IDocumentacionArchivoBl;
            iServicioCMIFinancieroBl = IServicioCMIFinancieroBl;
            iDetalleFinancieroBl = IDetalleFinancieroBl;
        }
    }
}
