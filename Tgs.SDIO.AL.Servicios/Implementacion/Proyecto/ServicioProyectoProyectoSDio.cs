﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Proyecto
{
    public partial class ServicioProyectoSDio
    {
        public List<ProyectoDtoResponse> ListarProyectoCabecera(ProyectoDtoRequest proyecto)
        {
            return iProyectoBl.ListarProyectoCabecera(proyecto);
        }

        public OportunidadGanadaDtoResponse ObtenerOportunidadGanada(OportunidadGanadaDtoRequest request)
        {
            return iProyectoBl.ObtenerOportunidadGanada(request);
        }

        public ProcesoResponse RegistrarProyecto(ProyectoDtoRequest proyecto)
        {
            return iProyectoBl.RegistrarProyecto(proyecto);
        }

        public ProcesoResponse ActualizarProyecto(ProyectoDtoRequest proyecto)
        {
            return iProyectoBl.ActualizarProyecto(proyecto);
        }

        public ProyectoDtoResponse ObtenerProyectoByID(ProyectoDtoRequest proyecto)
        {
            return iProyectoBl.ObtenerProyectoByID(proyecto);
        }

        public ProcesoResponse ActualizarProyectoDescripcion(ProyectoDtoRequest request)
        {
            return iProyectoBl.ActualizarProyectoDescripcion(request);
        }

        public ProcesoResponse RegistrarStageFinanciero(DetalleFinancieroDtoRequest request)
        {
            return iProyectoBl.RegistrarStageFinanciero(request);
        }

        public ProcesoResponse ProcesarArchivosFinancieros(ProyectoDtoRequest request)
        {
            return iProyectoBl.ProcesarArchivosFinancieros(request);
        }

        public List<PagoFinancieroDtoResponse> ListarPagos(ProyectoDtoRequest request)
        {
            return iProyectoBl.ListarPagos(request);
        }

        public List<IndicadorFinancieroDtoResponse> ListarIndicadores(ProyectoDtoRequest request)
        {
            return iProyectoBl.ListarIndicadores(request);
        }

        public List<CostoFinancieroDtoResponse> ListarCostos(ProyectoDtoRequest request)
        {
            return iProyectoBl.ListarCostos(request);
        }

        public List<CostoConceptoDtoResponse> ListarCostosOpex(ProyectoDtoRequest request)
        {
            return iProyectoBl.ListarCostosOpex(request);
        }

        public List<CostoConceptoDtoResponse> ListarCostosCapex(ProyectoDtoRequest request)
        {
            return iProyectoBl.ListarCostosCapex(request);
        }

        public List<DetalleFinancieroDtoResponse> ListarServiciosCMI(ProyectoDtoRequest request)
        {
            return iProyectoBl.ListarServiciosCMI(request);
        }

        public ProcesoResponse RegistrarEtapaEstado(EtapaProyectoDtoRequest request)
        {
            return iProyectoBl.RegistrarEtapaEstado(request);
        }

        public List<DetalleFinancieroDtoResponse> BuscarDetalleFinancieroPorProyecto(ProyectoDtoRequest request)
        {
            return iProyectoBl.BuscarDetalleFinancieroPorProyecto(request);
        }
        public List<ProyectoDtoResponse> ListarProyecto(ProyectoDtoRequest request)
        {
            return iProyectoBl.ListarProyecto(request);
        }
    }
}
