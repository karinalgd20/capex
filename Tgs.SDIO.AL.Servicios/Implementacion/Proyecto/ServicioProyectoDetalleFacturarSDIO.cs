﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Proyecto
{
    public partial class ServicioProyectoSDio
    {
        public DetalleFacturarDtoResponse ObtenerDetalleFacturarPorIdProyecto(DetalleFacturarDtoRequest request)
        {
            return iDetalleFacturarBl.ObtenerDetalleFacturarPorIdProyecto(request);
        }

        public DetalleFacturarDtoResponse ObtenerDetalleFacturarPorId(DetalleFacturarDtoRequest request)
        {
            return iDetalleFacturarBl.ObtenerDetalleFacturarPorId(request);
        }

        public ProcesoResponse RegistrarDetalleFacturar(DetalleFacturarDtoRequest request)
        {
            return iDetalleFacturarBl.RegistrarDetalleFacturar(request);
        }

        public ProcesoResponse ActualizarDetalleFacturar(DetalleFacturarDtoRequest request)
        {
            return iDetalleFacturarBl.ActualizarDetalleFacturar(request);
        }

    }
}
