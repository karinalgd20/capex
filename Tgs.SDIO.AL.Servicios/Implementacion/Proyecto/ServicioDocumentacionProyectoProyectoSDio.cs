﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Proyecto
{
    public partial class ServicioProyectoSDio
    {
        public bool InsertarDocumentacionProyecto(DocumentacionProyectoDtoRequest documentacionproyecto)
        {
            return iDocumentacionProyectoBl.InsertarDocumentacionProyecto(documentacionproyecto);
        }
        public bool EliminarDocumentacionProyecto(DocumentacionProyectoDtoRequest documentacionproyecto)
        {
            return iDocumentacionProyectoBl.EliminarDocumentacionProyecto(documentacionproyecto);
        }
        public List<DocumentacionProyectoDtoResponse> ListarDocumentacionProyecto(int iIdProyecto)
        {
            return iDocumentacionProyectoBl.ListarDocumentacionProyecto(iIdProyecto);
        }

        public DocumentacionArchivoDtoResponse ObtenerDocumentacionArchivo(int iIdDocumentoArchivo) {
            return iDocumentacionArchivoBl.ObtenerDocumentacionArchivo(iIdDocumentoArchivo);
        }
    }
}
