﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.Servicios.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Proyecto
{
    public partial class ServicioProyectoSDio
    {
        public bool InsertarRecursoProyecto(RecursoProyectoDtoRequest recursoproyecto)
        {
            return iRecursoProyectoBl.InsertarRecursoProyecto(recursoproyecto);
        }
        public List<RecursoProyectoDtoResponse> ListarRecursoProyecto(int iIdProyecto)
        {
            return iRecursoProyectoBl.ListarRecursoProyecto(iIdProyecto);
        }

        public RecursoProyectoDtoResponse ObtenerRecursoPorProyectoRol(RecursoProyectoDtoRequest request)
        {
            return iRecursoProyectoBl.ObtenerRecursoPorProyectoRol(request);
        }

    }
}
