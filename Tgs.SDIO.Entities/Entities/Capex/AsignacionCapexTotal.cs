﻿namespace Tgs.SDIO.Entities.Entities.Capex
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class AsignacionCapexTotal : Auditoria
    {
        [Key]
        public int IdAsignacionCapexTotal { get; set; }
        public int? IdCapexLineaNegocio { get; set; }
        public int? IdConcepto { get; set; }
        public string Descripcion { get; set; }

    }
}
