﻿namespace Tgs.SDIO.Entities.Entities.Capex
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class Diagrama : Auditoria
    {
        [Key]
        public int IdDiagrama { get; set; }
        public int? IdUbigeo { get; set; }
        public int? IdGrupo { get; set; }
        public int? Orden { get; set; }


    }
}
