﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Capex
{
    public class EstructuraCostoGrupoDetalle : Auditoria
    {
        [Key]
        public int IdEstructuraCostoGrupoDetalle { get; set; }
        public int? IdEstructuraCostoGrupo { get; set; }
        public int? IdServicio { get; set; }
        public int? IdConcepto { get; set; }
        public string IdUbigeo { get; set; }
        public int? IdMedioCosto { get; set; }
        public string Modelo { get; set; }
        public string Descripcion { get; set; }
        public decimal? CapexSoles { get; set; }
        public decimal? CapexDolares { get; set; }
        public string Certificacion { get; set; }
        public string IdTipoGrupo { get; set; }
        public int? Cantidad { get; set; }
        public decimal? CuNuevoDolar { get; set; }
        public decimal? CapexTotalSoles { get; set; }
        public string Asignacion { get; set; }

    }
}
