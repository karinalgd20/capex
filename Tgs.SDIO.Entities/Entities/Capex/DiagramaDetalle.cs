﻿namespace Tgs.SDIO.Entities.Entities.Capex
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class DiagramaDetalle : Auditoria
    {
        [Key]
        public int IdDiagramaDetalle { get; set; }
        public int? IdDiagrama { get; set; }
        public int? IdConcepto { get; set; }
        public int? Orden { get; set; }

    }
}
