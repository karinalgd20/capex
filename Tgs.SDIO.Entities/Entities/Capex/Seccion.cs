﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Capex
{
    public class Seccion : Auditoria
    {
        [Key]
        public int IdSeccion { get; set; }
        public int IdSolicitudCapex { get; set; }
        public int IdObservacion { get; set; }
        public int IdTipoDescripcion { get; set; }
        public string Descripcion { get; set; }
    }
}
