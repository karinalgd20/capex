﻿namespace Tgs.SDIO.Entities.Entities.Capex
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class AsignacionCapexTotalDetalle : Auditoria
    {
        [Key]
        public int IdAsignacionCapexTotalDetalle { get; set; }      
        public int IdAsignacionCapexTotal { set; get; }
        public decimal? Monto { set; get; }
        public int? IdMes { set; get; }
        public int? IdTipo { set; get; }
    }
}
