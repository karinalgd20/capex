﻿namespace Tgs.SDIO.Entities.Entities.Capex
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class CapexLineaNegocio : Auditoria
    {
        [Key]
        public int IdCapexLineaNegocio { get; set; }
        public int? IdSolicitudCapex { get; set; }
        public int? IdLineaNegocio { get; set; }
        public decimal? Van { get; set; }
        public decimal? Fc { get; set; }
        public int? PayBack { get; set; }

    }
}
