﻿namespace Tgs.SDIO.Entities.Entities.Capex
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class Cronograma : Auditoria
    {
        [Key]
        public int IdCronograma { get; set; }
        public int? IdEstructuraCosto { get; set; }
        public string Descripcion { get; set; }

    }
}
