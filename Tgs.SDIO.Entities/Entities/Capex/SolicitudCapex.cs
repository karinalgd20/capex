﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Capex
{
    public class SolicitudCapex : Auditoria
    {
        [Key]
        public int IdSolicitudCapex { get; set; }
        public string Descripcion { get; set; }
        public string NumeroSalesForce { get; set; }
        public int? IdTipoProyecto { get; set; }
        public int? IdTipoEntidad { get; set; }
        public int? IdCliente { get; set; }
        public int IdPreVenta { get; set; }
        public string CodigoPmo { get; set; }
    }
}
