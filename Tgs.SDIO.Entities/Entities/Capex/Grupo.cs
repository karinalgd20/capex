﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Capex
{
    public class Grupo : Auditoria
    {
        [Key]
        public int IdGrupo { get; set; }
        public string Descripcion { get; set; }

    }
}
