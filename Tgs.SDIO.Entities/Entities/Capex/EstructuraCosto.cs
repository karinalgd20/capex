﻿namespace Tgs.SDIO.Entities.Entities.Capex
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class EstructuraCosto : Auditoria
    {
        [Key]
        public int IdEstructuraCosto { get; set; }
        public int? IdSolicitudCapex { get; set; }
    }
}
