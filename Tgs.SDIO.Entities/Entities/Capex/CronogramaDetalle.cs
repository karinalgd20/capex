﻿namespace Tgs.SDIO.Entities.Entities.Capex
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class CronogramaDetalle : Auditoria
    {
        [Key]
        public int IdCronogramaDetalle { set; get; }
        public int? IdCronograma { set; get; }
        public string Descripcion { set; get; }
        public int? Duracion { set; get; }
        public DateTime? Inicio { set; get; }
        public DateTime? Final { set; get; }

    }
}
