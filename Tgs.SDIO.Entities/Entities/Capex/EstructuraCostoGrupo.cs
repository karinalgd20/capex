﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Capex
{
    public class EstructuraCostoGrupo : Auditoria
    {
        [Key]
        public int IdEstructuraCostoGrupo { get; set; }
        public int IdCapexLineaNegocio { get; set; }
        public int IdGrupo { get; set; }
        public string Descripcion { get; set; }
    }
}
