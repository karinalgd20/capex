﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Capex
{
    public class Topologia : Auditoria
    {
        [Key]
        public int IdTopologia { get; set; }
        public int IdEstructuraCosto { get; set; }
        public int IdMedioCosto { get; set; }
        public string Observaciones { get; set; }
    }
}
