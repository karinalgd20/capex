﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Capex
{
    public class SolicitudCapexFlujoEstado
    {
        [Key]
        public int IdSolicitudCapexFlujoEstado { get; set; }
        public int IdSolicitudCapex { get; set; }
        public int? IdEstado { get; set; }
        public int? IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
