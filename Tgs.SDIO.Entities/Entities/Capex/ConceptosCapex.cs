﻿namespace Tgs.SDIO.Entities.Entities.Capex
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class ConceptosCapex 
    {
        [Key]
        public int IdConceptosCapex { get; set; }
        public int? IdGrupo { set; get; }
        public string Descripcion { set; get; }

    }
}
