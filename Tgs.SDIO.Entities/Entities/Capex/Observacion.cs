﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Capex
{
    public class Observacion : Auditoria
    {
        [Key]
        public int IdObservacion { get; set; }
        public int IdResponsable { get; set; }
        public string Descripcion { get; set; }
        public int IdAsignado { get; set; }
        public string Respuesta { get; set; }
        public int IdSeccion { get; set; }
    }
}
