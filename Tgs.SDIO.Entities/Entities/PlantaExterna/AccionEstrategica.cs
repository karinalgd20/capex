﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;
namespace Tgs.SDIO.Entities.Entities.PlantaExterna
{
    public class AccionEstrategica : Auditoria
    {
        [Key]
        public int IdAccionEstrategica { set; get; }
        public string Nombre { set; get; }
        public string Descripcion { set; get; }
        public string NombreEstudioAE { set; get; }

    }
}
