﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;


namespace Tgs.SDIO.Entities.Entities.PlantaExterna
{
    public class Sisego : Auditoria
    {
        [Key]
        public int IdSisego { get; set; }

        public string CodSisego { get; set; }

        public string BiAnualItem { get; set; }

        public string Duplicado { get; set; }

        public int? IdCliente { get; set; }

        public string NombreCliente { get; set; }

        public int? IdTipoRequerimiento { get; set; }

        public decimal? TotalSoles { get; set; }

        public decimal? TotalDolares { get; set; }

        public decimal? TotalSupervision { get; set; }

        public int? DiasCalendarioEjecucion { get; set; }

        public decimal? TotalSisego { get; set; }

        public string Pep1 { get; set; }

        public string Pep2 { get; set; }

        public string Grafo { get; set; }

        public int? IdAccionEstrategica { get; set; }

        public int? IdTipoProyecto { get; set; }

        public string NombreTipoProyecto { get; set; }

        public int? IdTipoDecicionProyecto { get; set; }

        public string NombreSolicitante { get; set; }

        public string NombreSupervisorEstudio { get; set; }

        public string IdProyecto { get; set; }

        public string NombreProyecto { get; set; }

        public string Pmo2 { get; set; }

        public string SalesForce { get; set; }

        public int? IdTipoEntidad { get; set; }

        public int? IdEstadoSisego { get; set; }

        public string nombreEstadoSisego { get; set; }

        public string Bitacora { get; set; }

        public string Quiebre { get; set; }

        public string Responsable { get; set; }

        public string JefeResponsable { get; set; }

        public string AreaResponsable { get; set; }

        public DateTime? FechaIngreso { get; set; }

        public DateTime? MesIngreso { get; set; }

        public string AnioIngreso { get; set; }

        public DateTime? FechaActual { get; set; }

        public int? DiasBandeja { get; set; }

        public DateTime? AprobPlaneaControl { get; set; }

        public DateTime? anioAprobacion { get; set; }

        public DateTime? fechaGrafo { get; set; }

        public DateTime? FechaInicioEje { get; set; }

        public DateTime? FechaFinEje { get; set; }

        public int? IdEstadoImplementacion { get; set; }

        public string Cancelado { get; set; }

        public int? IdTipoCriticidad { get; set; }

        public int? IdEstadoEjecucion { get; set; }

        public int? nivel { get; set; }

        public string Pep0 { get; set; }

        public string Denominacion { get; set; }

        public string Pep1ResultadoDefault { get; set; }

        public string CantidadSisegoKey { get; set; }

        public string FibraRepetida { get; set; }

        public string BaseSandy { get; set; }

        public decimal? MontoSisegoReal { get; set; }

        public string AnioOrigenSisego { get; set; }

        public string KeySisegoAccionEstrategica { get; set; }

        public string ItemPlan { get; set; }

        public string FlagExpediente { get; set; }

        public int? DiasDesdeFechaInicio { get; set; }

        public string RangoInicioObras { get; set; }

        public DateTime? FechaFinObraOMci { get; set; }

        public int? DiasDesdeFinObra { get; set; }

        public DateTime? MesFinObraOMci { get; set; }

        public DateTime? FechaCertificacion { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string ResponsableRed { get; set; }
        public string TipoRequerimiento { get; set; }
        public decimal? TipoCambio { get; set; }
        public string Bandeja { get; set; }
        public string Segmento { get; set; }
        public string Ruc { get; set; }
        public DateTime? FechaPlanControl { get; set; }
        public string PlanControl { get; set; }
        public string NombreEstudio { get; set; }
        public decimal? Soles { get; set; }
        public decimal? Dolares { get; set; }
        public string Sede { get; set; }
        public string Direccion { get; set; }
    }
}
