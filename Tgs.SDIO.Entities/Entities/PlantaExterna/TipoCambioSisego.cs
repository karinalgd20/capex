﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.PlantaExterna
{
    public class TipoCambioSisego : Auditoria
    {
        [Key]
        public int Id { set; get; }

        public DateTime FechaFin { set; get; }

        public DateTime FechaInicio { set; get; }

        public decimal Monto { set; get; }
    }
}
