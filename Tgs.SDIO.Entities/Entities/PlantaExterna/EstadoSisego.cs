﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.PlantaExterna
{
   public class EstadoSisego : Auditoria
    {

        [Key]
        public int IdEstadoSisego { get; set; }

        public string Nombre { get; set; }
    }
}
