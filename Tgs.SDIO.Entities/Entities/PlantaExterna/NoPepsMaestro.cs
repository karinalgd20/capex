﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.PlantaExterna
{
    public class NoPeps : Auditoria
    {
        [Key]
        public int Id { set; get; }
        public string CodigoPep { set; get; } 
    }
}
