﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Proyecto
{
    public class DocumentacionArchivo : Auditoria
    {
        [Key]
        public int IdDocumentoArchivo { get; set; }
        public int? IdDocumentoProyecto { get; set; }
        public byte[] ArchivoAdjunto { get; set; }
    }

}
