﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Proyecto
{
    public class DocumentacionProyecto : Auditoria
    {
        [Key]
        public int IdDocumentoProyecto { get; set; }
        public int? IdProyecto { get; set; }
        public int? IdDocumento { get; set; }
        public string NombreArchivo { get; set; }
    }

}
