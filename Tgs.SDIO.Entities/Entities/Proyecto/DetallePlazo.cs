﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Proyecto
{
    public class DetallePlazo : Auditoria
    {
        [Key]
        public int IdDetallePlazo { get; set; }
        public int? IdProyecto { get; set; }
        public int? IdActividad { get; set; }
        public decimal? Cantidad { get; set; }
        public int? IdUnidadMedida { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public bool? VentaDirecta { get; set; }
        public int? Anios { get; set; }
        public int? Meses { get; set; }
        public int? Dias { get; set; }

    }

}
