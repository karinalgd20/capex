﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Entities.Entities.Proyecto
{
    public class DetalleFacturar
    {
        [Key]
        public int IdDetalleFacturar { get; set; }
        public int? IdProyecto { get; set; }
        public int? IdConceptoIngresoEgreso { get; set; }
        public int? IdFormaPago { get; set; }
        public int? IdMonedaPagoUnico { get; set; }
        public decimal? MontoPagoUnico { get; set; }
        public int? IdMonedaRecurrenteMensual { get; set; }
        public decimal? MontoRecurrenteMensual { get; set; }
        public int? NumeroMeses { get; set; }
        public decimal? TasaTipoCambio { get; set; }
        public string Observaciones { get; set; }
    }
}
