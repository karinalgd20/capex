﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Proyecto
{
    public class Proyecto : Auditoria
    {
        [Key]
        public int IdProyecto { get; set; }
        public int? IdProyectoPadre { get; set; }
        public string Descripcion { get; set; }
        public string IdProyectoSF { get; set; }
        public string IdProyectoLKM { get; set; }
        public string IdOportunidadSF { get; set; }
        public string IdProyectoTDP { get; set; }
        public int? IdPortafolio { get; set; }
        public int? IdPrograma { get; set; }
        public int? IdEmpresa { get; set; }
        public int? IdAreaEmpresa { get; set; }
        public int? Prioridad { get; set; }
        public int? IdCliente { get; set; }
        public int? IdComplejidad { get; set; }
        public int? IdClaseProyecto { get; set; }
        public int? IdFaseActual { get; set; }
        public int? IdEtapaActual { get; set; }
        public int? EsLicitacion { get; set; }
        public int? IdEstado { get; set; }
        public DateTime? FechaOportunidadGanada { get; set; }
        public DateTime? FechaAceptacion { get; set; }
        public DateTime? FechaFirmaContrato { get; set; }
        public int? IdMonedaPagoUnico { get; set; }
        public decimal? TotalPagoUnico { get; set; }
        public int? IdMonedaRecurrenteMensual { get; set; }
        public decimal? TotalRecurrenteMensual { get; set; }
        public int? IdMoneda { get; set; }
        public decimal? Van { get; set; }
        public decimal? VanVai { get; set; }
        public decimal? Oibda { get; set; }
        public decimal? MargenOibda { get; set; }
        public decimal? PayBack { get; set; }
        public int? IdOficinaOportunidad { get; set; }
    }

}
