﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Proyecto
{
    public class DetalleSolucion 
    {
        [Key]
        public int IdDetalleSolucion { get; set; }
        public int IdProyecto { get; set; }
        public int IdTipoSolucion { get; set; }
    }
}
