﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Proyecto
{
    public class ServicioCMIFinanciero : Auditoria
    {
        [Key]
        public int IdServicioFinanciero { get; set; }
        public int IdDetalleFinanciero { get; set; }
        public string CodigoCMI { get; set; }
        public string ElementoPEP { get; set; }
        public string CentroGestor { get; set; }
        public string CeCo { get; set; }
    }
}
