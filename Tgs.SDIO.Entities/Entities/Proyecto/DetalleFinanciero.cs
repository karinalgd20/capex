﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Proyecto
{
    public class DetalleFinanciero : Auditoria
    {

        [Key]
        public int IdDetalleFinanciero { get; set; }
        public int IdProyecto { get; set; }
        public int IdTipoDetalle { get; set; }
        public string Item { get; set; }
        public string NroOportunidad { get; set; }
        public string NroCaso { get; set; }
        public string TipoProyecto { get; set; }
        public string Linea { get; set; }
        public string SubLinea { get; set; }
        public string Servicio { get; set; }
        public string Producto { get; set; }
        public string ProductoAf { get; set; }
        public string AnalistaCGUsuarioLotus { get; set; }
        public string AnalistaCGCorreo { get; set; }
        public string Porcentaje { get; set; }
        public decimal ImporteVentas { get; set; }
        public decimal CostoOpex { get; set; }
        public decimal CostoCapex { get; set; }
        public string CeCo { get; set; }

    }
}
