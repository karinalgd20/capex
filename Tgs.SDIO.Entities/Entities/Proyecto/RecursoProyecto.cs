﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Proyecto
{
    public class RecursoProyecto : Auditoria
    {
        [Key]
        public int IdAsignacion { get; set; }
        public int? IdProyecto { get; set; }
        public int? IdRecurso { get; set; }
        public int? IdRol { get; set; }
        public int? PorcentajeDedicacion { get; set; }
        public string Observaciones { get; set; }
        public DateTime? FInicioAsignacion { get; set; }
        public DateTime? FFinAsignacion { get; set; }

        
    }

}
