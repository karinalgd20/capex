namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Tgs.SDIO.Entities.Entities.Base;
    using System;

    public class CasoOportunidad : Auditoria
    {
        [Key]
        [Column(Order = 0)]
        public int IdCaso { get; set; }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdOportunidad { get; set; }

        public string Descripcion { get; set; }

        public string IdCasoSF { get; set; }

        public int? IdCasoPadre { get; set; }

        public int? IdTipoSolicitud { get; set; }

        public string Asunto { get; set; }

        public int? Complejidad { get; set; }

        public int? Prioridad { get; set; }

        public string SolucionCaso { get; set; }

        public int? IdFase { get; set; }

        public int? IdEtapa { get; set; }

        public int? IdEstadoCaso { get; set; }

        public int? IdEquipoTrabajoActual { get; set; }
        
        public DateTime? FechaGeneraSisego { get; set; }
        public DateTime? FechaApertura { get; set; }

        public DateTime? FechaCierre { get; set; }

        public DateTime? FechaCompromisoAtencion { get; set; }
    }
}
