namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Tgs.SDIO.Entities.Entities.Base;

    public class EtapaOportunidad : Auditoria
    {
        [Key]
        [Column(Order = 0)]
        public int IdEtapa { get; set; }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdFase { get; set; }

        public string Descripcion { get; set; }

        public int? OrdenVisual { get; set; }
    }
}
