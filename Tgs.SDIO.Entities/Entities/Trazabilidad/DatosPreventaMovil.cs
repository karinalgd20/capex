namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Tgs.SDIO.Entities.Entities.Base;

    public class DatosPreventaMovil : Auditoria
    {
        
         [Key]
        public int IdDatosPreventaMovil { get; set; }

        public int IdOportunidad { get; set; }

        public int IdCaso { get; set; }

        public int? IdOperadorActual { get; set; }

        public int? LineasActuales { get; set; }

        public int? MesesContratoActual { get; set; }

        public decimal? RecurrenteMensualActual { get; set; }

        public decimal? FCVActual { get; set; }

        public decimal? ArpuServicioActual { get; set; }

        public int? Lineas { get; set; }

        public int? MesesContrato { get; set; }

        public decimal? RecurrenteMensualVR { get; set; }

        public decimal? FCVVR { get; set; }

        public decimal? ArpuServicioVR { get; set; }

        public DateTime? FechaPresentacion { get; set; }

        public DateTime? FechaBuenaPro { get; set; }

        public string Observaciones { get; set; }

        public decimal? MontoCapex { get; set; }

        public int? IdEstadoOportunidad { get; set; }

        public decimal? FCVMovistar { get; set; }

        public decimal? ArpuTotalMovistar { get; set; }

        public decimal? VanVai { get; set; }

        public int? MesesRecupero { get; set; }

        public decimal? FCVClaro { get; set; }

        public decimal? ArpuTotalClaro { get; set; }

        public decimal? FCVEntel { get; set; }

        public decimal? ArpuTotalEntel { get; set; }

        public decimal? FCVViettel { get; set; }

        public decimal? ArpuTotalViettel { get; set; }

        public decimal? ArpuServicio { get; set; }

        public decimal? ArpuEquipos { get; set; }

        public int? ModalidadEquipo { get; set; }

        public decimal? CostoPromedioEquipo { get; set; }

        public decimal? PorcentajeSubsidio { get; set; }
    }
}
