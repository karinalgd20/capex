namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class TipoOportunidad : Auditoria
    {
        [Key]
        public int IdTipoOportunidad { get; set; }

        public string Descripcion { get; set; }
    }
}
