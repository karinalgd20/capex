﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    public class EstadoCaso : Auditoria
    {
        [Key]
        public int IdEstadoCaso { get; set; }

        public string Descripcion { get; set; }

        public bool? EstadoStop { get; set; }

        public int? OrdenVisual { get; set; }

    }
}
