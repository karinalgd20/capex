﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;
using System;

namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
   public class TiempoAtencionEquipo : Auditoria
    {
        [Key]
        public int IdTiempoAtencion { get; set; }

        public int? IdOportunidad { get; set; }

        public int? IdCaso { get; set; }

        public string IdOportunidadSF { get; set; }

        public string IdCasoSF { get; set; }

        public int? IdSisego { get; set; }

        public string IdSisegoSW { get; set; }
        
        public int? IdEquipoTrabajo { get; set; }

        public DateTime? FechaInicio { get; set; }

        public DateTime? FechaFin { get; set; }

        public decimal? DiasGestion { get; set; }

        public string Observaciones { get; set; }

    }
}
