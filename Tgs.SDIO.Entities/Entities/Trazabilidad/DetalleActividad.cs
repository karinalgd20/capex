﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;
using System;

namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    public class DetalleActividad : Auditoria
    {
        [Key]
        public int IdSeguimiento { get; set; }

        public int IdOportunidad { get; set; }

        public int? IdCaso { get; set; }

        public int? IdAreaSeguimiento { get; set; }

        public int? IdActividad { get; set; }

        public decimal? DuracionEstimada { get; set; }

        public decimal? TrabajoEstimado { get; set; }

        public DateTime? FechaInicioEstimada { get; set; }

        public DateTime? FechaFinEstimada { get; set; }

        public string Predecesoras { get; set; }

        public decimal? DuracionReal { get; set; }

        public decimal? TrabajoReal { get; set; }

        public DateTime? FechaInicioReal { get; set; }

        public DateTime? FechaFinReal { get; set; }

        public decimal? AvanceEstimado { get; set; }

        public decimal? AvanceReal { get; set; }

        public int? IdEstadoEjecucion { get; set; }

        public int? IdEstadoCumplimiento { get; set; }

        public bool? FlgObservaciones { get; set; }

        public bool? FlgFilesAdjuntos { get; set; }

        public int? IdRecursoEjecutor { get; set; }

        public int? IdRecursoRevisor { get; set; }
        public string Observaciones { get; set; }

    }
}
