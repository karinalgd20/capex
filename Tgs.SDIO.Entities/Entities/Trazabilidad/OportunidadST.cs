﻿using System.ComponentModel.DataAnnotations;
using System;
namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
   public class OportunidadST
    {

        [Key]
        public int IdOportunidad { get; set; }

        public int? IdOportunidadPadre { get; set; }

        public string IdOportunidadSF { get; set; }

        public string IdOportunidadAux { get; set; }

        public string Descripcion { get; set; }

        public int? IdTipoOportunidad { get; set; }

        public int? IdMotivoOportunidad { get; set; }

        public int? IdTipoEntidadCliente { get; set; }

        public int? IdCliente { get; set; }

        public int? IdSector { get; set; }
        public int? IdLineaNegocio { get; set; }        

        public int? IdSegmentoNegocio { get; set; }

        public int? IdTipoCicloVenta { get; set; }

        public int? IdTipoCicloImplementacion { get; set; }

        public int? Prioridad { get; set; }

        public int? ProbalidadExito { get; set; }

        public int? PorcentajeCierre { get; set; }

        public int? PorcentajeCheckList { get; set; }

        public int? IdFase { get; set; }

        public int? IdEtapa { get; set; }

        public bool? FlgCapexMayor { get; set; }

        public decimal? ImporteCapex { get; set; }

        public int? IdMoneda { get; set; }

        public decimal? ImporteFCV { get; set; }

        public DateTime? FechaApertura { get; set; }

        public DateTime? FechaCierre { get; set; }

        public int? IdEstadoOportunidad { get; set; }

        public int? IdRecursoComercial { get; set; }

        public int? IdRecursoPreventa { get; set; }

        public int? IdEstado { get; set; }

        public int IdUsuarioCreacion { get; set; }

        public DateTime FechaCreacion { get; set; }

        public int? IdUsuarioEdicion { get; set; }

        public int? IdFuncionPropietario { get; set; }
                
        public DateTime? FechaEdicion { get; set; }
    }
}
