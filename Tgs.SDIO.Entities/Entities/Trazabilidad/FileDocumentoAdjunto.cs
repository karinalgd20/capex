namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Tgs.SDIO.Entities.Entities.Base;

    public class FileDocumentoAdjunto : Auditoria
    {
        [Key]
        [Column(Order = 0)]
        public int IdDocAdjunto { get; set; }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdFileDocAdjunto { get; set; }

        public string Descripcion { get; set; }

        public string RutaFile { get; set; }

        public int? TipoFile { get; set; }

        public decimal? Tamanio { get; set; }
    }
}
