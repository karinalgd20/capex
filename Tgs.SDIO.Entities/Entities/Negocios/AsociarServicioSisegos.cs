﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Negocios
{
    public class AsociarServicioSisegos : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public int IdServicio { get; set; }
        public string Servicio { get; set; }
        public int IdEquipo { get; set; }
        public string Equipo { get; set; }
        public int IdSisego { get; set; }
        public string CodSisego { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Direccion { get; set; }

    }
}
