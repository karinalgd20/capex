﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Negocios
{
    public class RPASisegoDetalle : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public int Id_sisego { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Ciudad { get; set; }
        public string TipoSede { get; set; }
        public string Direccion { get; set; }
        public string Via { get; set; }
        public string Numero { get; set; }
        public string Nro_Piso { get; set; }
        public string Interior { get; set; }
        public string Lote { get; set; }
        public string Manzana { get; set; }
        public string MedioTransmision { get; set; }
        public string CreadorSisego { get; set; }
    }
}
