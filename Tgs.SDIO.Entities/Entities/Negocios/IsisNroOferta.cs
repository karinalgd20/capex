﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Negocios
{
    public class IsisNroOferta : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public string Nro_oferta { get; set; }
        public int Nro_version_Oferta { get; set; }
        public int Id_OportunidadGanadora { get; set; }

    }
}
