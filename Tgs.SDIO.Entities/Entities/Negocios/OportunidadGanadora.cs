﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Negocios
{
   public class OportunidadGanadora : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public string PER { get; set; }
        public string Ruc { get; set; }
        public int IdEtapa { get; set; }
        public string CasoDerivado { get; set; }
        public DateTime FechaCreacionOportunidad { get; set; }
        public DateTime FechaCreacionOferta { get; set; }
        public string TipoOportunidad { get; set; }
        public string SegmentoCliente { get; set; }
        public string PlazoContrato { get; set; }
        public string TipoPlazoContrato { get; set; }
        public string CreadorOferta { get; set; }
        public string ComercialOferta { get; set; }
        public string EstadoOportunidad { get; set; }
        public string EtapaOportunidad { get; set; }
    }
}
