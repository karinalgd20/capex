﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Negocios
{
    public class RPAEquiposServicio : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public int Id_Oferta { get; set; }
        public int Id_RPAServOferta { get; set; }
        public string Cod_Equipo { get; set; }
        public string Tipo { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Componente_1 { get; set; }
        public string Tipo_de_acceso { get; set; }
        public string Meses { get; set; }
        public string Con_Valor_Agregado { get; set; }
        public string ValorAgregado { get; set; }
        public string Equipos1 { get; set; }
        public string Equipos2 { get; set; }
    }
}
