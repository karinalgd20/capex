﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Negocios
{
    public class RPASisegoDatosContacto : Auditoria 
    {
        [Key]
        public int Id { get; set; }
        public int Id_RPASisegoDetalle { get; set; }
        public string Contacto { get; set; }
        public string Telefono_1 { get; set; }
        public string Telefono_2 { get; set; }
        public string Telefono_3 { get; set; }
        public string Telefono1_Contacto2 { get; set; }
        public string Telefono2_Contacto2 { get; set; }
        public string Telefono3_Contacto2 { get; set; }
        public string Contacto_2 { get; set; }
    }
}
