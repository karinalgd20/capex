﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Negocios
{
    public class AsociarNroOfertaSisegos : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public int IdOferta { get; set; }
        public string NroOferta { get; set; }
        public int IdSisego { get; set; }
        public string CodSisego { get; set; }
    }
}
