﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Negocios
{
    public class RPAServiciosxNroOferta : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public string NombreServicio { get; set; }
        public int Cantidad { get; set; }
        public int Id_Oferta { get; set; }
        public int NumeroCD { get; set; }
        public int NumeroCDK { get; set; }
        public int Contacto_Id { get; set; }
        public int TipoServicio { get; set; }
        public string Velocidad { get; set; }
        public string Accion { get; set; }
        public string MedioTx { get; set; }
        public string TipoCircuito { get; set; }
        public string LDN { get; set; }
        public string Realtime { get; set; }
        public string Video { get; set; }
        public string Caudal_oro { get; set; }
        public string Caudal_plata { get; set; }
        public string Caudal_platino { get; set; }
        public string Caudal_Bronce { get; set; }
        public string Caudal_VRF { get; set; }
        public string VA_Servicio { get; set; }
        public string VA_Velocidad { get; set; }
        public string VA_Acción { get; set; }
        public string VA_MedioTX { get; set; }
        public string VA_TipoCircuito { get; set; }
        public string PagoUnico { get; set; }
        public string PagoRecurrente { get; set; }
        
    }

}
