﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Negocios
{
    public class SisegoCotizado : Auditoria 
    {
        [Key]
        public int Id { get; set; }
        public int Id_OportunidadGanadora { get; set; }
        public string codigo_sisego { get; set; }

    }
}
