﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.CartaFianza
{ 
   public class CartaFianzaMaestro
    {
        [Key]
        public int IdCartaFianza { set; get; }
        public int IdCliente { set; get; }
        public string NumeroOportunidad { set; get; }
        public int IdTipoContratoTm { set; get; }
        public string NumeroContrato { set; get; }
        public int IdProcesoTm { set; get; }
        public string NumeroProceso { set; get; }
        public string Servicio { set; get; }
        public string DescripcionServicioCartaFianza { set; get; }
        
        public DateTime? FechaFirmaServicioContrato { set; get; }
        public DateTime? FechaFinServicioContrato { set; get; }
        public int IdEmpresaAdjudicadaTm { set; get; }
        public int IdTipoGarantiaTm { set; get; }
        public string NumeroGarantia { set; get; }
     
        public int IdTipoMonedaTm { set; get; }
        public decimal ImporteCartaFianzaSoles { set; get; }
        public decimal ImporteCartaFianzaDolares { set; get; }
        public int IdBanco  { set; get; }

        public DateTime? FechaEmisionBanco { set; get; }
        public DateTime? FechaVigenciaBanco { set; get; }
        public DateTime? FechaVencimientoBanco { set; get; }

        public int? IdTipoAccionTm  { set; get; }

        public int? IdRenovarTm { set; get; }
     
        public int? IdEstadoCartaFianzaTm { set; get; }
        
        public int? IdTipoSubEstadoCartaFianzaTm { set; get; }

        public int? IdEstadoVencimientoTm { set; get; }

        public int? IdEstadoRecuperacionCartaFianzaTm { set; get; }
  
        public int ClienteEspecial { set; get; }
        public int SeguimientoImportante { set; get; }
        public string Observacion { set; get; }
        public string Incidencia { set; get; }
        public int IdEstado { set; get; }
        public int? IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int? IdUsuarioEdicion { set; get; }
        public DateTime? FechaEdicion { set; get; }

 
        public DateTime? FechaCorrecionTesoreria { set; get; }
        public DateTime? FechaRecepcionTesoreria { set; get; }
        public string MotivoCartaFianzaErrada { set; get; }

        public string UsuarioEdicion { set; get; }

        public DateTime? FechaRenovacionEjecucion { set; get; }
        public DateTime? FechaDesestimarRequerimiento { set; get; }
        public DateTime? FechaEjecucion { set; get; }

        public int? IdCartaFianzaInicial { set; get; }

        public int? IdTipoRequeridaTm { set; get; }

        public string MotivoCartaFianzaRequerida { set; get; }

        public Boolean Eliminado { set; get; }

    }

}
