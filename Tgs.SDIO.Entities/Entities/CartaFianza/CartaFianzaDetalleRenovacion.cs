﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.CartaFianza
{

  
    public class CartaFianzaDetalleRenovacion
    {
        [Key]
        public int IdCartaFianzaDetalleRenovacion { set; get; }
        public int IdCartaFianza { set; get; }
        public int IdColaborador { set; get; }
        public int IdColaboradorACargo { set; get; }
        public int? IdRenovarTm { set; get; }
        public int PeriodoMesRenovacion { set; get; }
        public DateTime? FechaVencimientoRenovacion { set; get; }
        public string SustentoRenovacion { set; get; }
        public string Observacion { set; get; }
        public int IdEstado { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int? IdUsuarioEdicion { set; get; }
        public DateTime? FechaEdicion { set; get; }

 
        public string CartaFianzaReemplazo { set; get; }
        public DateTime? FechaSolitudTesoria { set; get; }

        public string NumeroGarantiaReemplazo { set; get; }

        public int? IdTipoBancoReemplazo { set; get; }

        public string ValidacionContratoGcSm { set; get; }
        public DateTime? FechaRespuestaFinalGcSm { set; get; }
        public DateTime? FechaRecepcionCartaFianzaRenovacion { set; get; }
        public DateTime? FechaEntregaRenovacion { set; get; }

        public string UsuarioEdicion { set; get; }


        public DateTime? FechaEmisionBancoRnv { get; set; }

        public DateTime? FechaVigenciaBancoRnv { get; set; }

        public DateTime? FechaVencimientoBancoRnv { get; set; }


    }
}
