﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.CartaFianza
{

   
    public class CartaFianzaDetalleRecupero
    {
        [Key]
        public int IdCartaFianzaDetalleRecupero { set;get;}
        public int IdCartaFianza {set;get;}
        public int IdColaborador {set;get;}
        public int IdColaboradorACargo {set;get;}
        public int IdEstadoRecuperacionCartaFianzaTm {set;get;}
        public string Comentario { set; get; }
        public DateTime? FechaRecupero { set; get; }
        public DateTime? FechaSolicitudRecupero { set; get; }
        public DateTime? FechaDevolucionTesoreria { set; get; }
        public int IdEstado { set; get; }
        public int? IdUsuarioCreacion { set; get; }
        public int? IdUsuarioEdicion { set; get; }
        public DateTime? FechaEdicion { set; get; }


        public string SustentoRenovacion { set; get; }
        public string Observacion { set; get; }
        public string ValidacionContratoGcSm { set; get; }
        public DateTime?  FechaRespuestaFinalGcSm  { set; get; }

    }
}
