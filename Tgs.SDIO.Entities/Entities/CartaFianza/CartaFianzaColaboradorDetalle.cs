﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.CartaFianza
{
   public class CartaFianzaColaboradorDetalle
    {
        [Key]
        public int IdCartaFianzaColaboradorDetalle { set; get; }
        public int IdCartaFianza { set; get; }
        public int IdColaborador { set; get; }
        public int IdEstado { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int IdUsuarioEdicion { set; get; }
        public DateTime FechaEdicion { set; get; }

    }
}
