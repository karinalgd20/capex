﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Entities.Entities.CartaFianza
{
    public class CartaFianzaDetalleSeguimiento
    {

        [Key]
        public int? IdCartaFianzaDetalleSeguimiento { set; get; }
        public int? IdCartaFianza { set; get; }
        public int? IdColaborador { set; get; }
        public int? IdColaboradorACargo { set; get; }
        public int? IdEstadoRecuperacionCartaFianzaTm { set; get; }
        public string Comentario { set; get; }
        public int? IdUsuarioCreacion { set; get; }
        public DateTime? FechaCreacion { set; get; }
        public int? IdUsuarioModificacion { set; get; }
        public DateTime? FechaModificacion { set; get; }
        public bool? Vigencia { set; get; }
        public bool? Eliminado { set; get; }
        public bool? UltimoSeguimiento { set; get; }


    }
}
