﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Entities.Entities.Compra
{
    public class EtapaPeticionCompras
    {
        [Key]
        public int IdEtapaPeticionCompra { set; get; }
        public int IdPeticionCompra { set; get; }
        public int IdConfiguracionEtapa { set; get; }
        public int? IdEstadoEtapa { set; get; }
        public int? IdEstado { set; get; }
        public string Comentario { set; get; }
        public int? IdRecursoAtiende { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int IdUsuarioEdicion { set; get; }
        public DateTime FechaEdicion { set; get; }
    }
}
