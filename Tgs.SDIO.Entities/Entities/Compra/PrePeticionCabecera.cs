﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Entities.Entities.Compra
{
    public class PrePeticionCabecera
    {
        [Key]
        public int Id { set; get; }
        public int? IdCliente { set; get; }
        public int? IdProyecto { set; get; }
        public int? IdLineaCMI { set; get; }
        public string CodigoSIGO { set; get; }
        public int? Tipo { set; get; }
        public DateTime? FechaInicioProyecto { set; get; }
        public int? IdEstado { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int? IdUsuarioEdicion { set; get; }
        public DateTime? FechaEdicion { set; get; }
    }
}
