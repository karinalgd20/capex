﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Entities.Entities.Compra
{
    public class CentroCostos
    {
        [Key]
        public int IdCentroCosto { set; get; }
        public int? IdAreaSolicitante { set; get; }
        public string Descripcion { set; get; }
        public string CodigoCeCo { set; get; }
        public string Cebe { set; get; }
        public string Ar { set; get; }
        public int IdEstado { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int IdUsuarioEdicion { set; get; }
        public DateTime FechaEdicion { set; get; }
    }
}
