﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Entities.Entities.Compra
{
    public class GestionPeticionCompras
    {
        [Key]
        public int IdGestionPeticionCompra { set; get; }
        public int IdPeticionCompra { set; get; }
        public string ContratoMarco { set; get; }
        public string Cesta { set; get; }
        public bool? CestaLiberada { set; get; }
        public DateTime? FechaCesta { set; get; }
        public int? PosicionCesta { set; get; }
        public string NumeroPedido { set; get; }
        public DateTime? FechaPedido { set; get; }
        public int? PosicionPedido { set; get; }
        public DateTime? EnvioPedido { set; get; }
        public decimal? MontoPedido { set; get; }
        public decimal? SaldoPedido { set; get; }
        public DateTime? FechaActaRecibida { set; get; }
        public DateTime? FechaConfirmacion { set; get; }
        public DateTime? FechaAtencionGestor { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int IdUsuarioEdicion { set; get; }
        public DateTime? FechaEdicion { set; get; }
        public int? IdTipoMoneda { set; get; }
        public string CodigoConfirmacion { set; get; }
        
    }
}
