﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Entities.Entities.Compra
{
    public class PrePeticionDCMDetalle
    {
        [Key]
        public int Id { set; get; }
        public int IdCabecera { set; get; }
        public Int16? Numero { set; get; }
        public int? TipoCosto { set; get; }
        public string ProductManager { set; get; }
        public string Descripcion { set; get; }
        public int? IdProveedor { set; get; }
        public int? IdContratoMarco { set; get; }
        public int? AutorizadoPor { set; get; }
        public int? TipoEntrega { set; get; }
        public int? PlazoEntrega { set; get; }
        public int? PQAdjudicado { set; get; }
        public int? CotizacionAdjunta { set; get; }
        public DateTime? FechaEntregaOC { set; get; }
        public string Observaciones { set; get; }
        public string Direccion { set; get; }
        public int? PosicionOC { set; get; }
        public string MonedaCompra { set; get; }
        public decimal Monto { set; get; }
        public int? IdLineaProducto { set; get; }
        public int? IdEstado { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int? IdUsuarioEdicion { set; get; }
        public DateTime? FechaEdicion { set; get; }
    }
}
