﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Entities.Entities.Compra
{
    public class PeticionCompras
    {
        [Key]
        public int IdPeticionCompra { set; get; }
        public string CodigoPeticionCompra { set; get; }
        public string Descripcion { set; get; }
        public bool? AsociadoAProyecto { set; get; }
        public int? IdTipoCompra { set; get; }
        public int? IdTipoCosto { set; get; }
        public int? IdEtapa { set; get; }
        public int? IdLineaNegocio { set; get; }
        public int? IdComprador { set; get; }
        public bool? AdjuntaCotizacion { set; get; }
        public decimal? MontoCotizacion { set; get; }
        public string SubGrupoCompras { set; get; }
        public int? IdAreaSolicitante { set; get; }
        public int? IdCentroCosto { set; get; }
        public int? IdRecursoSolicitante { set; get; }
        public int? IdRecursoGerente { set; get; }
        public int? IdRecursoDirector { set; get; }
        public int? IdResponsablePostventa { set; get; }
        public int? IdContratoMarco { set; get; }
        public int? IdPrePeticionDetalle { set; get; }
        public string ElementoPEP { set; get; }
        public string Grafo { set; get; }
        public string Cuenta { set; get; }
        public string DescripcionCuenta { set; get; }
        public string AreaFuncional { set; get; }
        public bool? PeticionPenalidad { set; get; }
        public bool? ArrendamientoRenting { set; get; }
        public int? IdTipoMoneda { set; get; }
        public decimal? CostoTotal { set; get; }
        public string SustentoCualitativo { set; get; }
        public string DetalleSolpe { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int? IdUsuarioEdicion { set; get; }
        public DateTime? FechaEdicion { set; get; }
    }
}
