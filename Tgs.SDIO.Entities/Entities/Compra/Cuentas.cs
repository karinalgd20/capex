﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Entities.Entities.Compra
{
    public class Cuentas
    {
        [Key]
        public int IdCuenta { set; get; }
        public string Cuenta { set; get; }
        public string Descripcion { set; get; }
        public string AFOST { set; get; }
        public string DescripcionACTOST { set; get; }
        public string Proceso { set; get; }
        public string FInACT { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int IdUsuarioEdicion { set; get; }
        public DateTime FechaEdicion { set; get; }
    }
}
