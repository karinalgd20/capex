﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Compra
{
    public class DetalleClienteProveedor
    {
        [Key]
        public int IdDetalleClientePro { set; get; }
        public int IdPeticionCompra { set; get; }
        public int IdCliente { set; get; }
        public int IdProveedor { set; get; }
        public string CodigoSap { set; get; }
        public string CodigoSrm { set; get; }
        public string NombreContacto { set; get; }
        public string EmailContacto { set; get; }
        public string TelefonoContacto { set; get; }
        public int IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int? IdUsuarioEdicion { get; set; }
        public DateTime? FechaEdicion { get; set; }
    }
}
