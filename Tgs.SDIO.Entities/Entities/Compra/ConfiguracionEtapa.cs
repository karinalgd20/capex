﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Entities.Entities.Compra
{
    public class ConfiguracionEtapa
    {
        [Key]
        public int IdConfiguracionEtapa { set; get; }
        public int? IdTipoCompra { set; get; }
        public int? IdTipoCosto { set; get; }
        public string Descripcion { set; get; }
        public int? Orden { set; get; }
        public int IdEstado { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int? IdUsuarioEdicion { set; get; }
        public DateTime? FechaEdicion { set; get; }
    }
}
