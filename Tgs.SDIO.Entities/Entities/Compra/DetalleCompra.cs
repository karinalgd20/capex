﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Entities.Entities.Compra
{
    public class DetalleCompra
    {
        [Key]
        public int IdDetalleCompra { set; get; }
        public int IdPeticionCompra { set; get; }
        public int? IdCuenta { set; get; }
        public string Actividad { set; get; }
        public int? IdCuentaAnterior { set; get; }
        public string Descripcion { set; get; }
        public int? Cantidad { set; get; }
        public decimal? CostoPorUnidad { set; get; }
        public decimal? Total { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int? IdUsuarioEdicion { set; get; }
        public DateTime? FechaEdicion { set; get; }
        public string AccionEstrategica { set; get; }
        public string Pep2 { set; get; }
    }
}
