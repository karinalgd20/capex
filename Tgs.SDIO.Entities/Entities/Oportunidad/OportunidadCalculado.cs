﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadCalculado : Auditoria
    {
        [Key]
        public int IdOportunidadCalculado { get; set; }
        public int IdConceptoFinanciero { get; set; }
        public int IdOportunidadLineaNegocio { get; set; }
        public int Anio { get; set; }
        public int Mes { get; set; }
        public decimal Monto { get; set; }
        public string TipoFicha { get; set; }
    }
}
