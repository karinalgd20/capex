using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class ConceptoDatosCapex : Auditoria
    {    
        [Key]
        public int IdServicioConcepto { get; set; }
        public int IdConceptoDatosCapex { get; set; }
        public string Circuito { get; set; }
        public string SISEGO { get; set; }
        public int MesesAntiguedad { get; set; }
        public int Cantidad { get; set; }
        public decimal CostoUnitarioAntiguo { get; set; }
        public decimal ValorResidualSoles { get; set; }
        public decimal CostoUnitario { get; set; }
        public decimal CapexDolares { get; set; }
        public decimal CapexSoles { get; set; }
        public decimal TotalCapex { get; set; }
        public int AnioRecupero { get; set; }
        public int MesRecupero { get; set; }
        public int AnioComprometido { get; set; }
        public int MesComprometido { get; set; }
        public int AnioCertificado { get; set; }
        public int MesCertificado { get; set; }
        public string Medio { get; set; }
        public int IdTipo { get; set; }
        public int IdBWOportunidad { get; set; }
        public string Garantizado { get; set; }       
        public string Cruce { get; set; }
        public string AEReducido { get; set; }
        public string Combo { get; set; }
        public int Antiguedad { get; set; }
        public decimal CapexInstalacion { get; set; }
        public decimal CapexReal { get; set; }
        public decimal CapexTotalReal { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Tipo { get; set; }

    }
}
