using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadDocumento : Auditoria
    {
   
        [Key]
        public int IdDocumento { get; set; }
        public int IdOportunidadLineaNegocio { get; set; }
        public int IdFlujoCaja { get; set; }
        public int TipoDocumento { get; set; }
        public string RutaDocumento { get; set; }
        public string Descripcion { get; set; }
        public int  IdTipoDocumento { get; set; }


    }

}
