﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class PlantillaImplantacion : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public int IdOportunidad { get; set; }
        public int IdTipoEnlaceCircuitoDatos { get; set; }
        public int IdMedioCircuitoDatos { get; set; }
        public int IdServicioGrupoCircuitoDatos { get; set; }
        public int IdTipoCircuitoDatos { get; set; }
        public string NumeroCircuitoDatos { get; set; }
        public int IdCostoCircuitoDatos { get; set; }
        public string ConectadoCircuitoDatos { get; set; }
        public string LargaDistanciaNacionalCircuitoDatos { get; set; }
        public int IdVozTos5CaudalAntiguo { get; set; }
        public int IdVozTos4CaudalAntiguo { get; set; }
        public int IdVozTos3CaudalAntiguo { get; set; }
        public int IdVozTos2CaudalAntiguo { get; set; }
        public int IdVozTos1CaudalAntiguo { get; set; }
        public int IdVozTos0CaudalAntiguo { get; set; }
        public string UltimaMillaCircuitoDatos { get; set; }
        public string VrfCircuitoDatos { get; set; }
        public string EquipoCpeCircuitoDatos { get; set; }
        public string EquipoTerminalCircuitoDatos { get; set; }
        public int IdAccionIsis { get; set; }
        public int IdTipoEnlaceServicioOfertado { get; set; }
        public int IdMedioServicioOfertado { get; set; }
        public int IdServicioGrupoServicioOfertado { get; set; }
        public int IdCostoServicioOfertado { get; set; }
        public int IdTipoServicioOfertado { get; set; }
        public string NumeroServicioOfertado { get; set; }
        public string LargaDistanciaNacionalServicioOfertado { get; set; }
        public int IdVozTos5ServicioOfertado { get; set; }
        public int IdVozTos4ServicioOfertado { get; set; }
        public int IdVozTos3ServicioOfertado { get; set; }
        public int IdVozTos2ServicioOfertado { get; set; }
        public int IdVozTos1ServicioOfertado { get; set; }
        public int IdVozTos0ServicioOfertado { get; set; }
        public string UltimaMillaServicioOfertado { get; set; }
        public string VrfServicioOfertado { get; set; }
        public string EquipoCpeServicioOfertado { get; set; }
        public string ObservacionServicioOfertado { get; set; }
        public string EquipoTerminalServicioOfertado { get; set; }
        public string GarantizadoBw { get; set; }
        public string Propiedad { get; set; }
        public string RecursoTransporte { get; set; }
        public string RouterSwitchCentralTelefonica { get; set; }
        public string ComponentesRouter { get; set; }
        public string TipoAntena { get; set; }
        public string SegmentoSatelital { get; set; }
        public string CoordenadasUbicacion { get; set; }
        public string PozoTierra { get; set; }
        public string Ups { get; set; }
        public string EquipoTelefonico { get; set; }
    }
}
