﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadFlujoCaja : Auditoria
    {
        [Key]
        public int IdFlujoCaja { get; set; }
        public int IdOportunidadLineaNegocio { get; set; }
        public int? IdAgrupador { get; set; }
        public int? IdTipoCosto { get; set; }
        public string Descripcion { get; set; }
        public int? IdProveedor { get; set; }
        public int? IdPeriodos { get; set; }
        public int? IdPestana { get; set; }
        public int? IdGrupo { get; set; }
        public int? IdCasoNegocio { get; set; }
        public int? IdServicio { get; set; }
        public int? IdSubServicio { get; set; }
        public int? Cantidad { get; set; }
        public decimal? CostoUnitario { get; set; }
        public string ContratoMarco { get; set; }
        public int? IdMoneda { get; set; }
        public int? FlagSISEGO { get; set; }
        public int? IdServicioCMI { get; set; }
        public int? IdOportunidadCosto { get; set; }
        public int? IdTipoSubServicio { get; set; }
        public int? AgrupadorServicio { get; set; }
        public int? IdInterfaz { get; set; }
        public int? IdTipoServicioSISEGO { get; set; }

    }
}
