using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public  class FormulaDetalle: Auditoria
    {     
        [Key]
        public int IdFormulaDetalle { get; set; }
        public int IdFormula { get; set; }
        public int IdSubServicio { get; set; }
        public string Componente { get; set; }
        public int Valor1 { get; set; }
        public int Valor2 { get; set; }
        public string Comentario { get; set; }     
    }
}
