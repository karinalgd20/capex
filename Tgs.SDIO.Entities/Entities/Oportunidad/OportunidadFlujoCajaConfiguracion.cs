﻿
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadFlujoCajaConfiguracion : Auditoria
    {
        [Key]
        public int IdFlujoCajaConfiguracion { get; set; }
        public int IdFlujoCaja { get; set; }        
        public decimal? Ponderacion { get; set; }
        public decimal? CostoPreOperativo { get; set; }
        public int? Inicio { get; set; }
        public int? Meses { get; set; }

    }
}
