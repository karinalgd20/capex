﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class ServicioCMILineaSubLinea : Auditoria
    {

        [Key]
        public int IdServicioLineaSunLinea { get; set; }
        public int IdServicioCMI { get; set; }
        public int IdLinea { get; set; }
        public int IdSubLinea { get; set; }

    }
}
