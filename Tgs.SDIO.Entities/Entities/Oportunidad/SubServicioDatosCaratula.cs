using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class SubServicioDatosCaratula : Auditoria
    {        
        [Key]
        public int IdSubServicioDatosCaratula { get; set; }
        public int IdFlujoCaja { get; set; }
        public string Circuito { get; set; }
        public int? IdMedio { get; set; }
        public int? IdTipoEnlace { get; set; }
        public int? IdActivoPasivo { get; set; }
        public int? IdLocalidad { get; set; }
        public int? NumeroMeses { get; set; }
        public decimal? MontoUnitarioMensual { get; set; }
        public decimal? MontoTotalMensual { get; set; }
        public int? NumeroMesInicioGasto { get; set; }
        public string FlagRenovacion { get; set; }
        public decimal? Instalacion { get; set; }
        public decimal? Desinstalacion { get; set; }
        public decimal? PU { get; set; }
        public decimal? Alquiler { get; set; }
        public decimal? Factor { get; set; }
        public decimal? ValorCuota { get; set; }
        public decimal? CC { get; set; }
        public decimal? CCQProvincia { get; set; }
        public decimal? CCQBK { get; set; }
        public decimal? CCQCAPEX { get; set; }
        public decimal? TIWS { get; set; }
        public decimal? RADIO { get; set; }
       
    }
}
