using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class TipoCambioProyecto : Auditoria
    {
        [Key]
        public int IdTipoCambio { get; set; }
        public int IdProyecto { get; set; }
        public int IdLineaProducto { get; set; }
        public int IdMoneda { get; set; }
        public int IdTipificacion { get; set; }
        public int Anio { get; set; }
        public decimal Monto { get; set; }

    }
}
