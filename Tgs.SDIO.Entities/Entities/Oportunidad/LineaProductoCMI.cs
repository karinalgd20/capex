using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class LineaProductoCMI : Auditoria
    { 
        [Key]
        public int IdServicioCMI { get; set; }
        public int IdLineaProducto { get; set; }
        
    }

}
