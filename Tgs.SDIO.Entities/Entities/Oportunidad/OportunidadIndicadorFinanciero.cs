﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadIndicadorFinanciero : Auditoria 
    {
        [Key]
        public int IdOportunidadIndicador { get; set; }
        public int IdIndicadorFinanciero { get; set; }
        public int IdOportunidadLineaNegocio { get; set; }
        public int Anio { get; set; }
        public decimal Monto { get; set; }
        public string TipoFicha { get; set; }
    }
}
