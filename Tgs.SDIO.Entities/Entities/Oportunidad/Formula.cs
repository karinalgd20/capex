using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public  class Formula: Auditoria
    {     
        [Key]
        public int IdFormula { get; set; }
        public int IdSubServicio { get; set; }
        public string Descripcion { get; set; }
        //public string Formula { get; set; }
        public string Comentario { get; set; }
     
    }
}
