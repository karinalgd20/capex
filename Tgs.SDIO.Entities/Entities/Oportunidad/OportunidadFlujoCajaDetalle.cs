﻿using Tgs.SDIO.Entities.Entities.Base;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadFlujoCajaDetalle : Auditoria
    {        
        [Key]
        public int IdFlujoCajaDetalle { get; set; }
        public int IdFlujoCajaConfiguracion { get; set; }
        public int Anio { get; set; }
        public int Mes { get; set; }
        public decimal Monto { get; set; }
        public int TipoFicha { get; set; }
    }
}
