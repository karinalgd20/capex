﻿using Tgs.SDIO.Entities.Entities.Base;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
   public class OportunidadLineaNegocio: Auditoria
    {
        [Key]
        public int IdOportunidadLineaNegocio { get; set; }
        public int IdOportunidad { get; set; }
        public int IdLineaNegocio { get; set; }        
        public string FlagGo { get; set;}

    }
}
