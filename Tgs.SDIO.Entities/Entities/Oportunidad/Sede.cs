﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class Sede : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public string Codigo { get; set; }
        public int IdCliente { get; set; }
        public int IdTipoEnlace { get; set; }
        public int IdTipoSede { get; set; }
        public int IdAccesoCliente { get; set; }
        public int IdTendidoExterno { get; set; }
        public int? NumeroPisos { get; set; }
        public int IdUbigeo { get; set; }
        public int? IdTipoVia { get; set; }
        public string Direccion { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public int? IdTipoServicio { get; set; }
        public int? Piso { get; set; }
        public string Interior { get; set; }
        public string Manzana { get; set; }
        public string Lote { get; set; }
        public string DesRequerimiento { get; set; }
        public bool? FlgCotizacionReferencial { get; set; }
        public bool? FlgRequisitosEspeciales { get; set; }
    }
}
