using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class LineaConceptoGrupo : Auditoria
    {
        [Key]
        public int IdGrupo { get; set; }
        public int IdLineaProducto { get; set; }
        public int IdConcepto { get; set; }
        public int IdPestana { get; set; }
        public int IdDepreciacion { get; set; }
    }
}