﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;
using System;


namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class Cotizacion : Auditoria
    {
        public int Id { get; set; }
        public int IdSedeInstalacion { get; set; }
        public string CodSisegoCotizacion { get; set; }
        public string Nombre { get; set; }
        public decimal TotalSoles { get; set; }
        public decimal TotalDolares { get; set; }
        public int Dias { get; set; }
        public DateTime? FechaEnvioPresupuesto { get; set; }
        public int EstadoCotizacion { get; set; }
    }
}
