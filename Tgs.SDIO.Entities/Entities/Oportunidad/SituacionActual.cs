﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class SituacionActual : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public int IdTipo { get; set; }
        public string Descripcion { get; set; }
        public int IdOportunidad { get; set; }
    }
}