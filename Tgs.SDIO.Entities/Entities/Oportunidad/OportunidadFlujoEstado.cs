using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadFlujoEstado 
    {
   
        [Key]

        public int IdOportunidadFlujoEstado { get; set; }
        public int IdOportunidad { get; set; }
        public int? IdEstado { get; set; }
        public int IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
    }

}
