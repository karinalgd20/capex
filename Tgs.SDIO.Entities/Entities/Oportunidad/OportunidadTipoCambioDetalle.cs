﻿using Tgs.SDIO.Entities.Entities.Base;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadTipoCambioDetalle : Auditoria
    {        
        [Key]
        public int IdTipoCambioDetalle { get; set; }
        public int IdTipoCambioOportunidad { get; set; }
        public int? IdMoneda { get; set; }
        public int IdTipificacion { get; set; }
        public int? Anio { get; set; }
        public decimal? Monto { get; set; }
    }
}
