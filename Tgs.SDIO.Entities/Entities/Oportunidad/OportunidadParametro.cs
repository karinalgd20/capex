using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadParametro : Auditoria
    {
   
        [Key]
        public int IdOportunidadParametro { get; set; }
        public int IdOportunidadLineaNegocio { get; set; }
        public int IdParametro { get; set; }
        public decimal Valor { get; set; }
        public string Valor2 { get; set; }        

    }

}
