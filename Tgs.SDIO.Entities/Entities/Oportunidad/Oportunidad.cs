using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class Oportunidad : Auditoria
    {
   
        [Key]

        public int IdOportunidad { get; set; }
        public Nullable<int> IdTipoEmpresa { get; set; }
        public Nullable<int> IdCliente { get; set; }
        public string Descripcion { get; set; }
        public string NumeroSalesForce { get; set; }
        public string NumeroCaso { get; set; }
        public DateTime Fecha { get; set; }
        public string Alcance { get; set; }
        public Nullable<int> Periodo { get; set; }
        public Nullable<int> TiempoImplantacion { get; set; }
        public Nullable<int> IdTipoProyecto { get; set; }
        public Nullable<int> IdTipoServicio { get; set; }
        public string IdProyectoAnterior { get; set; }
        public Nullable<int> IdAnalistaFinanciero { get; set; }
        public Nullable<int> IdProductManager { get; set; }
        public Nullable<int> IdPreVenta { get; set; }
        public Nullable<int> IdCoordinadorFinanciero { get; set; }
        public Nullable<int> TiempoProyecto { get; set; }
        public Nullable<int> IdTipoCambio { get; set; }
        public Nullable<int> Agrupador { get; set; }
        public Nullable<int> Version { get; set; }
        public Nullable<int> VersionPadre { get; set; }
        public Nullable<int> FlagGanador { get; set; }
        public Nullable<int> IdMonedaFacturacion { get; set; }


    }

}
