using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{

    public class ServicioConceptoProyectado : Auditoria
    {
        [Key]
        public int IdOportunidad { get; set; }
        public int IdLineaNegocio { get; set; }
        public int IdServicioCMI { get; set; }
        public int IdConcepto { get; set; }
        public int IdServicioConcepto { get; set; }
        public int IdProyectado { get; set; }
        public int Anio { get; set; }
        public int Mes { get; set; }
        public decimal Monto { get; set; }
        public int IdPestana { get; set; }
        public int IdGrupo { get; set; }
        public string FlagIngresoManual { get; set; }
        public string TipoFicha { get; set; }
    }

}
