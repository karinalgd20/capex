﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class VisitaDetalle : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public int IdVisita { get; set; }
        public DateTime FechaAcuerdo { get; set; }
        public string DescripcionPunto { get; set; }
        public string Responsable { get; set; }
        public bool Cumplimiento { get; set; }
    }
}