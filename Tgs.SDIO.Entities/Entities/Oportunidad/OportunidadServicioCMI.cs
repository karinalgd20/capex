﻿using Tgs.SDIO.Entities.Entities.Base;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadServicioCMI : Auditoria
    {        
        [Key]
        public int IdOportunidadServicioCMI { get; set; }
        public int IdServicioCMI { get; set; }
        public int IdOportunidadLineaNegocio { get; set; }
        public decimal? Porcentaje { get; set; }
        public int? IdAnalista { get; set; }
    }
}
