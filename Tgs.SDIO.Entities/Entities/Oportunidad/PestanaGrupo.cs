using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public  class PestanaGrupo: Auditoria
    {
     
        [Key]
        public int IdPestana { get; set; }
        public int IdGrupo { get; set; }
        public string Descripcion { get; set; }
        public Nullable<int> FlagServicio { get; set; }
     
    }
}
