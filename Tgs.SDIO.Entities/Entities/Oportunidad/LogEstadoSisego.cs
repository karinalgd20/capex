﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;


namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class LogEstadoSisego : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int IdSedeInstalacion { get; set; }
    }
}
