﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class Estudios : Auditoria
    {
        public int Id { get; set; }
        public int IdSedeInstalacion { get; set; }
        public string Estudio { get; set; }
        public string Departamento { get; set; }
        public string TipoRequerimiento { get; set; }
        public string Nodo { get; set; }
        public int Dias { get; set; }
        public decimal TotalSoles { get; set; }
        public decimal TotalDolares { get; set; }
        public string Responsable { get; set; }
        public string FacilidadesTecnicas { get; set; }
        public string EstadoSisego { get; set; }
    }
}
