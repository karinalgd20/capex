﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadCosto :Auditoria 
    {
        [Key]
        public int IdOportunidadCosto { get; set; }
        public int IdOportunidadLineaNegocio { get; set; }
        public int? IdCosto { get; set; }
        public int IdTipoCosto { get; set; }
        public string Descripcion { get; set; }
        public decimal Monto { get; set; }
        public decimal VelocidadSubidaKBPS { get; set; }
        public decimal PorcentajeGarantizado { get; set; }
        public decimal PorcentajeSobresuscripcion { get; set; }
        public decimal CostoSegmentoSatelital { get; set; }
        public decimal InvAntenaHubUSD { get; set; }
        public decimal AntenaCasaClienteUSD { get; set; }
        public decimal Instalacion { get; set; }
        public int IdUnidadConsumo { get; set; }
        public int IdTipificacion { get; set; }
        public string CodigoModelo { get; set; }
        public string Modelo { get; set; }

    }
}
