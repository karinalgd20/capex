﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;
namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class SedeInstalacion : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public int IdSede { get; set; }

        public int IdOportunidad { get; set; }

        public string CodSisego { get; set; }

        public string DesRequerimiento { get; set; }
        public bool? FlgCotizacionReferencial { get; set; }
        public bool? FlgRequisitosEspeciales { get; set; }

    }
}
