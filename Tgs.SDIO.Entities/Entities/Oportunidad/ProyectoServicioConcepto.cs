using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class ProyectoServicioConcepto : Auditoria
    {
        [Key]
        public int IdServicioConcepto { get; set; }
        public int IdProyecto { get; set; }
        public int IdLineaProducto { get; set; }
        public int IdServicioCMI { get; set; }
        public int IdConcepto { get; set; }
       
        public int IdAgrupador { get; set; }
        public int IdTipoCosto { get; set; }
        public string Descripcion { get; set; }
        public decimal Ponderacion { get; set; }
        public decimal CostoPreOperativo { get; set; }
        public int IdProveedor { get; set; }
        public int IdPeriodos { get; set; }
        public int Inicio { get; set; }
        public int Meses { get; set; }
        public int IdPestana { get; set; }
        public int IdGrupo { get; set; }


    }

}
