﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class Contacto : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public int IdSede { get; set; }
        public string NombreApellidos { get; set; }
        public string NumeroTelefono { get; set; }
        public string NumeroCelular { get; set; }
        public string CorreoElectronico { get; set; }
        public string Cargo { get; set; }
    }
}