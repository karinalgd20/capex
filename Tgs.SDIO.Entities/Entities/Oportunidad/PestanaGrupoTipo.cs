using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public  class PestanaGrupoTipo : Auditoria
    {
     
        [Key]
        public int IdPestanaGrupoTipo { get; set; }
        public int IdGrupo { get; set; }
        public int IdTipoSubServicio { get; set; }           
     
    }
}
