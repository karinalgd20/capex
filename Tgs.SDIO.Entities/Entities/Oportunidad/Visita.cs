﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class Visita : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public int IdOportunidad { get; set; }
        public string PreVentaVisitante { get; set; }
        public string ClienteVisitado { get; set; }
        public int IdTipo { get; set; }
        public DateTime Fecha { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFin { get; set; }
    }
}
