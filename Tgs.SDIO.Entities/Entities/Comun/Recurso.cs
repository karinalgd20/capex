namespace Tgs.SDIO.Entities.Entities.Comun
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class Recurso : Auditoria
    {
        [Key]
        public int IdRecurso { get; set; }

        public int? TipoRecurso { get; set; }

        public string UserNameSF { get; set; }

        public string Nombre { get; set; }

        public string DNI { get; set; }

        public string Email { get; set; }

        public int? IdUsuarioRais { get; set; }

        public int? IdRolDefecto { get; set; }

        public int? IdCargo { get; set; }

        public int? IdOrigen { get; set; }

        public int? IdEmpresa { get; set; }

        public int? IdArea { get; set; }

        //Nuevas Columnas
        public int? IdSupervisor { get; set; }
        public int? IdEmailAlertasTm { get; set; }
        public int? SeguimientoRecupero { get; set; }
         
        public string NombreInterno { get; set; }
    }
}
