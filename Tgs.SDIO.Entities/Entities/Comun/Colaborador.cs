﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class Colaborador
    {

        [Key]
        public int IdColaborador { set; get; }
        public int? IdSupervisorColaborador { set; get; }
        public string IdentificadorColaborador { set; get; }
        public string NombreCompleto { set; get; }
        public string Email { set; get; }
        public int IdTipoColaboradorFianzaTm { set; get; }
        public int IdEmailAlertasTm { set; get; }
        public int IdEstado { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int IdUsuarioEdicion { set; get; }
        public DateTime FechaEdicion { set; get; }
        public int IdRecurso { set; get; }

    }
}
