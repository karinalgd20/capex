﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class GerenciaProducto : Auditoria
    {
   
        [Key]
        public int IdGerenciaProducto { get; set; }
        public string Descripcion { get; set; }
   
    }
}
