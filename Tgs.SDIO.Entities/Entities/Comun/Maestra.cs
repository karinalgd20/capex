﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class Maestra : Auditoria
    {
        [Key]
        public int IdMaestra { get; set; }
        public int? IdRelacion { get; set; }
        public string Descripcion { get; set; }
        public string Valor { get; set; }
        public string Comentario { get; set; }
        public string Valor2 { get; set; }

        public int? IdOpcion { get; set; }
    }
}
