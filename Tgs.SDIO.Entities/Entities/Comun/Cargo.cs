﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class Cargo : Auditoria
    {
        [Key]   
        public int Id { get; set; }

        public string Descripcion { get; set; }
    }
}
