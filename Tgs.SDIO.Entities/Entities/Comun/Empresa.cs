﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class Empresa : Auditoria
    {
        [Key]
        public int IdEmpresa { get; set; }

        public int? IdEmpresaPadre { get; set; }

        public string RazonSocial { get; set; }

        public string RUC { get; set; }

        public int? IdTipoEmpresa { get; set; }

        public string Contacto { get; set; }

        public string TelefonoFijo { get; set; }
       
    }
}
