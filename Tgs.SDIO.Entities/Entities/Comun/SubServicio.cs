﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class SubServicio : Auditoria
    {

        [Key]

        public int IdSubServicio { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionEquivalencia { get; set; }
        public int? IdDepreciacion { get; set; }
        public int? IdTipoSubServicio { get; set; }
        public int? Orden { get; set; }
        public int? Negrita { get; set; }
        public decimal? CostoInstalacion { get; set; }
        public string Cruce { get; set; }
        public string AEReducido { get; set; }
        public int? IdGrupo { get; set; }
        
    }
}
