﻿namespace Tgs.SDIO.Entities.Entities.Comun
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class ConceptoSeguimiento : Auditoria
    {
        [Key]
        public int IdConcepto { get; set; }

        public int? IdConceptoPadre { get; set; }

        public string Descripcion { get; set; }

        public int? Nivel { get; set; }

        public int? OrdenVisual { get; set; }

    }
}
