﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class DireccionComercial : Auditoria
    {
     

        [Key]
        public int IdDireccion { get; set; }
        public string Descripcion { get; set; }
    }
}
