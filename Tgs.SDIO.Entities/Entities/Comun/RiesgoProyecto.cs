﻿namespace Tgs.SDIO.Entities.Entities.Comun
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    using System;
    public class RiesgoProyecto : Auditoria
    {
        [Key]

        public int Id { get; set; }
        public int? IdOportunidad { get; set; }
        public int IdTipo { get; set; }
        public DateTime FechaDeteccion { get; set; }
        public string Descripcion { get; set; }
        public string Consecuencias { get; set; }
        public string PlanAccion { get; set; }
        public string Responsable { get; set; }
        public int IdProbabilidad { get; set; }
        public int IdImpacto { get; set; }
        public int IdNivel { get; set; }
        public DateTime? FechaCierre { get; set; }


    }
}
