﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class DG
    {

        [Key]
        public int IdDG { get; set; }
        public int? IdConcepto { get; set; }
        public string Concatenado { get; set; }
        public string AEReducido { get; set; }
        public int IdEstado { get; set; }
        public int IdUsuarioCreacion { get; set; }
        public DateTime?FechaCreacion { get; set; }
        public int? IdUsuarioEdicion { get; set; }
        public DateTime?FechaEdicion { get; set; }
        public string Tipo { get; set; }



    }
}
