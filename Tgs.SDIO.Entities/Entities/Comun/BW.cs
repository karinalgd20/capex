﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class BW
    {

        [Key]
        public int IdBW { get; set; }
        public int? IdConcepto { get; set; }
        public string Descripcion { get; set; }
        public decimal? BW_NUM { get; set; }
        public decimal? Precio { get; set; }
        public int? IdLineaProducto { get; set; }
        public int? IdPestana { get; set; }
        public int? IdGrupo { get; set; }
        public int IdEstado { get; set; }
        public int IdUsuarioCreacion { get; set; }
        public DateTime?FechaCreacion { get; set; }
        public int? IdUsuarioEdicion { get; set; }
        public DateTime?FechaEdicion { get; set; }


    }
}
