﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class CasoNegocioServicio : Auditoria
    {
        [Key]
        public int IdCasoNegocioServicio { get; set; }
        public int IdCasoNegocio { get; set; }
        public int IdServicio { get; set; }

    }
}
