﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class RecursoLineaNegocio : Auditoria
    {
        [Key]
        public int Id { get; set; }

        public int IdRecurso { get; set; }

        public int IdLineaNegocio { get; set; }         
    }
}
