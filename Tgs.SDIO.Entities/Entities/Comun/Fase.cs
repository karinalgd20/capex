﻿namespace Tgs.SDIO.Entities.Entities.Comun
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class Fase : Auditoria
    {
        [Key]
        public int IdFase { get; set; }

        public string Descripcion { get; set; }

        public int IdGrupoFase { get; set; }

        public int? OrdenVisual { get; set; }

    }
}
