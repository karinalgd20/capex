﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class ServicioCMI : Auditoria
    {

        [Key]
        public int IdServicioCMI { get; set; }
        public string CodigoCMI { get; set; }
        public string DescripcionPlantilla { get; set; }
        public string DescripcionOriginal { get; set; }
        public string DescripcionCMI { get; set; }
        public int IdLinea { get; set; }
        public int? IdGerenciaProducto { get; set; }
        public int? IdLineaNegocio { get; set; }
        public int? IdCentroCosto { get; set; }


    }
}
