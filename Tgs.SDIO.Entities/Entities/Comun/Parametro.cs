﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class Parametro : Auditoria
    {
        [Key]
        public int IdParametro { get; set; }
        public string Descripcion { get; set; }
        public decimal Valor { get; set; }
        public string Valor2 { get; set; }
    }

}
