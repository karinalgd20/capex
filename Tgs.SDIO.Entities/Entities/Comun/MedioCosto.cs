﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class MedioCosto : Auditoria
    {
        [Key]
        public int IdMedioCosto { get; set; }
        public int IdMedio { get; set; }
        public int IdCosto { get; set; }

    }
}
