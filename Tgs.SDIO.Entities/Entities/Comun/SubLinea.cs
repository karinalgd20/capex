﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class SubLinea
    {
        [Key]
        public int IdSubLinea { get; set; }
        public int IdLinea { get; set; }        
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public int IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int? IdUsuarioEdicion { get; set; }
        public DateTime?FechaEdicion { get; set; }


    }
}
