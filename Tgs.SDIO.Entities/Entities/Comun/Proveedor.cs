﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class Proveedor: Auditoria
    {
     
        [Key]
        public int IdProveedor { get; set; }
        public string Descripcion { get; set; }
        public int? TipoProveedor { get; set; }
        public string RazonSocial { get; set; }
        public string CodigoProveedor { get; set; }
        public string RUC { get; set; }
        public string Pais { get; set; }
        public string IdSecuencia { get; set; }
        public string NombrePersonaContacto { get; set; }
        public string TelefonoContactoPrincipal { get; set; }
        public string CorreoContactoPrincipal { get; set; }
        public string TelefonoContactoSecundario { get; set; }
        public string CorreoContactoSecundario { get; set; }
        public string CodigoSRM { get; set; }

    }
}
