﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class ContratoMarco
    {
     
        [Key]
        public int IdContratoMarco { get; set; }
        public string NumContrato { get; set; }
        public string Descripcion { get; set; }
        public string ProcesoAdjudicacion { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public string Catalogado { get; set; }
        public decimal? ImporteContrato { get; set; }
        public decimal? ImporteConsumido { get; set; }
        public string Moneda { get; set; }
        public string ProductoGerencia { get; set; }
        public int? IdProveedor { get; set; }
    }
}
