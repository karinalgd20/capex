﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class Archivo : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public int CodigoTabla { get; set; }
        public int IdEntidad { get; set; }
        public int? IdCategoria { get; set; }
        public string Nombre { get; set; }
        public string NombreInterno { get; set; }
        public string Descripcion { get; set; }
        public string Ruta { get; set; }
    }
}
