﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class tmp_CCHN
    {   
        public string BW { get; set; }
        public decimal FEC { get; set; }
        public decimal costo { get; set; }
        public decimal inversion { get; set; }
        public decimal instalacion { get; set; }
        public decimal mantenimiento { get; set; }
    }
}
