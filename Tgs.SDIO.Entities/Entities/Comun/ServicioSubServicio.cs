﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class ServicioSubServicio  :Auditoria
    {

        [Key]       
        public int IdServicioSubServicio { get; set; }
        public int IdServicio { get; set; }
        public int? IdSubServicio { get; set; }
        public int? IdTipoCosto { get; set; }
        public int? IdProveedor { get; set; }
        public string ContratoMarco { get; set; }
        public int? IdPeriodos { get; set; }
        public int? Inicio { get; set; }
        public decimal? Ponderacion { get; set; }
        public int? IdMoneda { get; set; }
        public int? IdGrupo { get; set; }
        public int? FlagSISEGO { get; set; }
    }
}
