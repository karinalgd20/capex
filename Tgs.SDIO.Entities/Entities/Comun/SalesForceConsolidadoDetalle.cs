﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class SalesForceConsolidadoDetalle : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public string IdOportunidad { get; set; }
        public string PropietarioOportunidad { get; set; }
        public string TipologiaOportunidad { get; set; }
        public int? IdCliente { get; set; }
        public string NombreOportunidad { get; set; }
        public DateTime? FechaCierreEstimada { get; set; }
        public DateTime? FechaCierreReal { get; set; }
        public string ProbabilidadExito { get; set; }
        public int? Etapa { get; set; }
        public string TipoOportunidad { get; set; }
        public int? PlazoEstimadoProvision { get; set; }
        public DateTime? FechaEstimadaInstalacionServicio { get; set; }
        public int? DuracionContrato { get; set; }
        public int? IngresoPorUnicaVezDivisa { get; set; }
        public string IngresoPorUnicaVez { get; set; }
        public int? FullContractValueNetoDivisa { get; set; }
        public string FullContractValueNeto { get; set; }
        public int? RecurrenteBrutoMensualDivisa { get; set; }
        public string RecurrenteBrutoMensual { get; set; }
        public string NumeroDelCaso { get; set; }
        public string Estado { get; set; }
        public string Departamento { get; set; }
        public string TipoSolicitud { get; set; }
        public string Asunto { get; set; }
        public string CasoPrincipal { get; set; }
        public DateTime? FechaHoraApertura { get; set; }
        public DateTime? FechaUltimaModificacion { get; set; }
        public string PropietarioCaso { get; set; }
        public string FuncionPropietario { get; set; } 
        public string LoginRegistro { get; set; }
        public DateTime? FechaCreacionDB { get; set; }    
        public string LoginUltimaModificacion { get; set; }   
    }
}
