﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class TipoCambioDetalle   : Auditoria
    {
        [Key]
        public int IdTipoCambioDetalle { get; set; }
        public int? IdTipoCambio { get; set; }
        public int? IdMoneda { get; set; }       
        public int? IdTipificacion { get; set; }
        public int? Anio { get; set; }
        public int? Monto { get; set; }  
    }
}
