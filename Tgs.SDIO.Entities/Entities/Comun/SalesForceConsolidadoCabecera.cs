﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class SalesForceConsolidadoCabecera : Auditoria
    {
        [Key]
        public int Id { get; set; }
        public string IdOportunidad { get; set; }
        public string NumeroDelCaso { get; set; }
        public string PropietarioOportunidad { get; set; }
        public string TipologiaOportunidad { get; set; }
        public int? IdCLiente { get; set; }
        public string NombreOportunidad { get; set; }
        public string ProbabilidadExito { get; set; }
        public int? Etapa { get; set; }
        public DateTime? FechaCierreEstimada { get; set; }
        public DateTime? FechaCierreReal { get; set; }
        public string Asunto { get; set; }
        public DateTime? FechaCreacionDB { get; set; }
        public string LoginRegistro { get; set; }
        public string LoginUltimaModificacion { get; set; }
        public int? IdTipoCapex { get; set; }
        public int? PorcentajeRealizado { get; set; }
        //

        
        public string Descripcion { get; set; }
        public string NombreCliente { get; set; }
    }
}
