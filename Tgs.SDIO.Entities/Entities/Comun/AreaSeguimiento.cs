namespace Tgs.SDIO.Entities.Entities.Comun
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class AreaSeguimiento : Auditoria
    {
        [Key]
        public int IdAreaSeguimiento { get; set; }

        public string Descripcion { get; set; }

        public int? OrdenVisual { get; set; }

    }  
}
