﻿namespace Tgs.SDIO.Entities.Entities.Comun
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class Actividad : Auditoria
    {
        [Key]
        public int IdActividad { get; set; }

        public int? IdActividadPadre { get; set; }

        public string Descripcion { get; set; }

        public string Predecesoras { get; set; }

        public int? IdClaseProyecto { get; set; }

        public int? IdFase { get; set; }

        public int? IdEtapa { get; set; }

        public bool? FlgCapexMayor { get; set; }

        public bool? FlgCapexMenor { get; set; }

        public int? IdAreaSeguimiento { get; set; }

        public int? NumeroDiaCapexMenor { get; set; }

        public int? CantidadDiasCapexMenor { get; set; }

        public int? NumeroDiaCapexMayor { get; set; }

        public int? CantidadDiasCapexMayor { get; set; }

        public string IdTipoActividad { get; set; }

        public bool? AsegurarOferta { get; set; }
    }
}
