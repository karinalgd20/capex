﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class TipoCambio   : Auditoria
    {
        [Key]
        public int IdTipoCambio { get; set; }
        public int IdLineaNegocio { get; set; }    
    }
}
