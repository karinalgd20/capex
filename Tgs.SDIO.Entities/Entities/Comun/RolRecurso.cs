namespace Tgs.SDIO.Entities.Entities.Comun
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class RolRecurso : Auditoria
    {
        [Key]
        public int IdRol { get; set; }

        public string Descripcion { get; set; }

        public int? Nivel { get; set; }
    }
}
