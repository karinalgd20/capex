﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class Area : Auditoria
    {
        [Key]
        public int IdArea { get; set; }

        public int IdAreaPadre { get; set; }

        public int IdEmpresa { get; set; }

        public string Descripcion { get; set; }
    }
}
