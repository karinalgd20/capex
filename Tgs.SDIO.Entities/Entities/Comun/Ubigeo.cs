﻿namespace Tgs.SDIO.Entities.Entities.Comun
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class Ubigeo : Auditoria
    {
        [Key]
        public int Id { get; set; }

        public string CodigoDepartamento { get; set; }

        public string CodigoProvincia { get; set; }

        public string CodigoDistrito { get; set; }

        public string Nombre { get; set; }

    }
}
