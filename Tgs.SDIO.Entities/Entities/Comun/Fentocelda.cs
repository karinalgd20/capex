﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class Fentocelda
    {
   
        [Key]
        public int IdFentocelda { get; set; }
        public int IdTipificacion { get; set; }
        public string Descripcion { get; set; }
        public decimal? Monto { get; set; }
        public decimal? VelocidadSubidaKBPS { get; set; }
        public decimal? PorcentajeGarantizado { get; set; }
        public decimal? PorcentajeSobresuscripcion { get; set; }
        public decimal? CostoSegmentoSatelital { get; set; }
        public decimal? InvAntenaHubUSD { get; set; }
        public decimal? AntenaCasaClienteUSD { get; set; }
        public decimal? Instalacion { get; set; }
        public int? IdUnidadConsumo { get; set; }
        public int IdEstado { get; set; }
        public int IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int? IdUsuarioEdicion { get; set; }
        public DateTime?FechaEdicion { get; set; }


    }
}
