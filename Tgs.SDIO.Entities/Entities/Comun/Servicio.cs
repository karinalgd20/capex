﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class Servicio :Auditoria
    {

        [Key]
        public int IdServicio { get; set; }
        public int? IdLineaNegocio { get; set; }
        public string Descripcion { get; set; }
        public int IdServicioGrupo { get; set;}
        public int? IdMedio { get; set; }
        public int? IdGrupo { get; set; }
         public int? IdServicioCMI { get; set; }
        public int? IdTipoEnlace { get; set; }
        

    }
}
