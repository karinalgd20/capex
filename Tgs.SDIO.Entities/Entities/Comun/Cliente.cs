﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class Cliente : Auditoria
    {


        [Key]
        public int IdCliente { get; set; }
        public string CodigoCliente { get; set; }
        public string Descripcion { get; set; }
        public int? IdSector { get; set; }
        public string GerenteComercial { get; set; }
        public int? IdDireccionComercial { get; set; }
        public string NumeroIdentificadorFiscal { get; set; }
        public int? IdTipoIdentificadorFiscalTm { get; set; }
        public string Email { get; set; }
        public string Direccion { get; set; }
        public int? IdTipoEntidad { get; set; }
        public string DireccionLegal { get; set; }



    }
}
