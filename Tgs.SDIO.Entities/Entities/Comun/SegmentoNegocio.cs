﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
   public class SegmentoNegocio : Auditoria
    {
        [Key]
        public int IdSegmentoNegocio { get; set; }

        public string Descripcion { get; set; }
    }
}
