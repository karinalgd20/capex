﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class ActividadSegmentoNegocio : Auditoria
    {
        [Key]
        public int IdActividadSegmentoNegocio { get; set; }

        public int IdActividad { get; set; }

        public int IdSegmentoNegocio { get; set; }

    }
}
