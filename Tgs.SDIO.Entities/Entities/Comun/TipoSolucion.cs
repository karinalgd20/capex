﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class TipoSolucion
    {   [Key]
        public int IdTipoSolucion { get; set; }
        public string Descripcion { get; set; }
    }
}
