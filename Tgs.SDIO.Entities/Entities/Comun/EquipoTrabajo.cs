﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class EquipoTrabajo : Auditoria
    {
        [Key]
        public int IdEquipoTrabajo { get; set; }

        public int? IdEquipoTrabajoPadre { get; set; }

        public string Descripcion { get; set; }

        public int? Nivel { get; set; }

        public int? IdRecursoLiderEquipo { get; set; }

        public int? IdTipoEquipo { get; set; }

        public int? OrdenVisual { get; set; }
    }
}
