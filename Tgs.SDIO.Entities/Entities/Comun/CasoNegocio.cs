﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class CasoNegocio : Auditoria
    {
        [Key]
        public int IdCasoNegocio { get; set; }
        public int? IdLineaNegocio { get; set; }

        public string Descripcion { get; set; }
        public string FlagDefecto { get; set; }
    }
}
