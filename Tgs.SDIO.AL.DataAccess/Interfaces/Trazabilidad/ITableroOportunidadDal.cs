﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;

//EAAR:PF12
namespace Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad
{
    public interface ITableroOportunidadDal : IRepository<OportunidadTs>
    {
        TableroOportunidadMatrizDtoResponse ListarTableroOportunidadMatriz(TableroOportunidadListarDtoRequest request);

        //RegistrarTableroOportunidadResponse Registrar(RegistrarTableroOportunidadRequest request);
    }
}
