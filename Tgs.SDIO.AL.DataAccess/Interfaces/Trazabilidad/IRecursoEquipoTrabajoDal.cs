﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad
{
    public interface IRecursoEquipoTrabajoDal : IRepository<RecursoEquipoTrabajo>
    {
    }

}
