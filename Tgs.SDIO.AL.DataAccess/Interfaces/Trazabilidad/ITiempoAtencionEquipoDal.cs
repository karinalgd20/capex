﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;


namespace Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad
{
    public interface ITiempoAtencionEquipoDal : IRepository<TiempoAtencionEquipo>
    {
        TiempoAtencionEquipoPaginadoDtoResponse ListadoTiempoAtencionEquipoPaginado(TiempoAtencionEquipoDtoRequest request);
    }
}
