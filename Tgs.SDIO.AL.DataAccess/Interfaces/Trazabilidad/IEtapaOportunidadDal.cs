﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad
{
    public interface IEtapaOportunidadDal : IRepository<EtapaOportunidad>
    {
        List<ListaDtoResponse> ListarComboEtapaOportunidad();
        List<ListaDtoResponse> ListarComboEtapaOportunidadPorIdFase(FaseDtoRequest request);
    }
}
