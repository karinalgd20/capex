﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad
{
    public interface IDatosPreventaMovilDal : IRepository<DatosPreventaMovil>
    {
        ProcesoResponse CargaExcelDatosPreventaMovil(DatosPreventaMovilDtoRequest request);
    }
}
