﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad
{
    public interface ITipoSolicitudDal : IRepository<TipoSolicitud>
    {
        List<ListaDtoResponse> ListarComboTipoSolicitud();
    }
}
