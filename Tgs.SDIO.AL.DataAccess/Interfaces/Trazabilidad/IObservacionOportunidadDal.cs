﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad
{
    public interface IObservacionOportunidadDal : IRepository<ObservacionOportunidad>
    {
        ObservacionOportunidadPaginadoDtoResponse ListadoObservacionesOportunidadPaginado(ObservacionOportunidadDtoRequest request);
    }
}
