﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;



namespace Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad
{
    public interface IOportunidadSTDal : IRepository<OportunidadST>
    {
        OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalObservacionPaginado(OportunidadSTDtoRequest request);

        OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalRecursoPaginado(OportunidadSTDtoRequest request);

        OportunidadSTPaginadoDtoResponse ListaOportunidadSTMasivoPaginado(OportunidadSTDtoRequest request);

        OportunidadSTSeguimientoPreventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPreventaPaginado(OportunidadSTSeguimientoPreventaDtoRequest request);

        OportunidadSTSeguimientoPostventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPostventaPaginado(OportunidadSTSeguimientoPostventaDtoRequest request);

    }
}
