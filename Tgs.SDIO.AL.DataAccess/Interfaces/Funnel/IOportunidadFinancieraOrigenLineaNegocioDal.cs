﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Funnel
{
    public interface IOportunidadFinancieraOrigenLineaNegocioDal
    {
        IEnumerable<ListarLineasNegocioDtoResponse> ListarLineasNegocio(IndicadorLineasNegocioDtoRequest filtro);

        IEnumerable<ListarLineasNegocioClienteDtoResponse> ListarClientesLineasNegocio(IndicadorLineasNegocioDtoRequest filtro);
    }
}
