﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Funnel
{
    public interface IOportunidadFinancieraOrigenDal
    {
        IEnumerable<OportunidadFinancieraOrigenDtoResponse> ListarIndicadorMadurez(IndicadorMadurezDtoRequest request);
        IEnumerable<DashoardOfertasLineaNegocioDtoResponse> ListarIndicadorOfertasLineaNegocio(DashoardDtoRequest request);
        IEnumerable<DashoardOfertasSectorDtoResponse> ListarIndicadorOfertasSector(DashoardDtoRequest request);
        IEnumerable<DashoardIngresosLineaNegocioDtoResponse> ListarIndicadorIngresosLineaNegocio(DashoardDtoRequest request);
        IEnumerable<DashoardIngresosSectorDtoResponse> ListarIndicadorIngresosSector(DashoardDtoRequest request);
        IEnumerable<DashoardOfertasEstadoDtoResponse> ListarIndicadorOfertasEstado(DashoardDtoRequest request);
        IEnumerable<DashboardTotalesDtoResponse> ListarIndicadoresTotales(DashoardDtoRequest request);
        IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesPreventaGerente(IndicadorDashboardPreventaDtoRequest request);

        IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesPreventaGerenteLider(IndicadorDashboardPreventaDtoRequest request);
        IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesPreventaLider(IndicadorDashboardPreventaDtoRequest request);
        IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesPreventaPreventa(IndicadorDashboardPreventaDtoRequest request);
        IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesSectorOportunidad(IndicadorDashboardSectorDtoRequest request);
        IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesSectorLideres(IndicadorDashboardSectorDtoRequest request);
        IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesSectorPreventas(IndicadorDashboardSectorDtoRequest request);
        IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesSectorClientes(IndicadorDashboardSectorDtoRequest request);
        IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesSectorOportunidades(IndicadorDashboardSectorDtoRequest request);
        IEnumerable<IndicadorRentabilidadDtoResponse> ListarRentabilidadAnalistasFinancieros(IndicadorRentabilidadDtoRequest request);
        IEnumerable<IndicadorRentabilidadDtoResponse> ListarRentabilidadClientes(IndicadorRentabilidadDtoRequest request);
        IEnumerable<IndicadorRentabilidadDtoResponse> ListarRentabilidadCasos(IndicadorRentabilidadDtoRequest request);
    }
}
