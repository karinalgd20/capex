﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto
{

    public interface IDetalleFacturarDal : IRepository<DetalleFacturar>
    {
        DetalleFacturarDtoResponse ObtenerDetalleFacturarPorId(DetalleFacturarDtoRequest request);

        DetalleFacturarDtoResponse ObtenerDetalleFacturarPorIdProyecto(DetalleFacturarDtoRequest request);

    }
}
