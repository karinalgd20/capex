﻿using System;
using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto
{

    public interface IDocumentacionArchivoDal : IRepository<Entities.Entities.Proyecto.DocumentacionArchivo>
    {
        DocumentacionArchivoDtoResponse ObtenerDocumentoArchivo(int iIdDocumentoArchivo);
    }
}



