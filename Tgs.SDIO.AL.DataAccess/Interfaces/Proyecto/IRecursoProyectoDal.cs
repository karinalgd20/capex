﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto
{

    public interface IRecursoProyectoDal : IRepository<Entities.Entities.Proyecto.RecursoProyecto>
    {
        List<RecursoProyectoDtoResponse> ListarRecursoProyecto(int iIdProyecto);

        RecursoProyectoDtoResponse ObtenerRecursoPorProyectoRol(RecursoProyectoDtoRequest request);
    }
}



