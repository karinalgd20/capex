﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto
{

    public interface IServicioCMIFinancieroDal : IRepository<Entities.Entities.Proyecto.ServicioCMIFinanciero>
    {
    }
}



