﻿using System;
using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Entidad = Tgs.SDIO.Entities.Entities.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto
{

    public interface IProyectoDal : IRepository<Entidad.Proyecto>
    {
      
        List<ProyectoDtoResponse> ListarProyectoCabecera(ProyectoDtoRequest proyecto);

        OportunidadGanadaDtoResponse ObtenerOportunidadGanada(OportunidadGanadaDtoRequest request);

        ProcesoResponse RegistrarProyecto(ProyectoDtoRequest proyecto);

        ProyectoDtoResponse ObtenerProyectoByID(ProyectoDtoRequest proyecto);

        ProcesoResponse RegistrarStageFinanciero(DetalleFinancieroDtoRequest request);

        ProcesoResponse ProcesarArchivosFinancieros(ProyectoDtoRequest request);
        List<PagoFinancieroDtoResponse> ListarPagos(ProyectoDtoRequest request);
        List<IndicadorFinancieroDtoResponse> ListarIndicadores(ProyectoDtoRequest request);
        List<CostoFinancieroDtoResponse> ListarCostos(ProyectoDtoRequest request);
        List<CostoConceptoDtoResponse> ListarCostosOpex(ProyectoDtoRequest request);
        List<CostoConceptoDtoResponse> ListarCostosCapex(ProyectoDtoRequest request);
        List<DetalleFinancieroDtoResponse> ListarServiciosCMI(ProyectoDtoRequest request);

        ProcesoResponse RegistrarEtapaEstado(EtapaProyectoDtoRequest request);

        List<DetalleFinancieroDtoResponse> BuscarDetalleFinancieroPorProyecto(ProyectoDtoRequest request);

        List<ProyectoDtoResponse> ListarProyecto(ProyectoDtoRequest request);
       
    }
}



