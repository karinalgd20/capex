﻿using System;
using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto
{

    public interface IDetalleFinancieroDal : IRepository<Entities.Entities.Proyecto.DetalleFinanciero>
    {
    }
}



