﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Capex
{
    public interface ISolicitudCapexDal : IRepository<SolicitudCapex>
    {

        SolicitudCapexPaginadoDtoResponse ListaSolicitudCapexPaginado(SolicitudCapexDtoRequest SolicitudCapex);
        SolicitudCapexDtoResponse ObtenerSolicitudCapex(SolicitudCapexDtoRequest solicitudCapex);
    }
}
