﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Capex
{
    public interface ISolicitudCapexFlujoEstadoDal : IRepository<SolicitudCapexFlujoEstado>
    {
    }
}
