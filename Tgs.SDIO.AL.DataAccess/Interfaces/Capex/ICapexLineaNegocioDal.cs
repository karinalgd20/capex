﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Capex
{
    public interface ICapexLineaNegocioDal : IRepository<CapexLineaNegocio>
    {
        List<ListaDtoResponse> ListarCapexLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocio);

        CapexLineaNegocioPaginadoDtoResponse ListarLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocio);
    }

}