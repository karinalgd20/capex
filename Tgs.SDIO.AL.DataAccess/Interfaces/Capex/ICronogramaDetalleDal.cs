﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Capex
{
    public interface ICronogramaDetalleDal : IRepository<CronogramaDetalle>
    {
    }
}