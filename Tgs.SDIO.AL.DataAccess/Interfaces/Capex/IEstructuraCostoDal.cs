﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.Entities.Entities.Capex;
using static Tgs.SDIO.DataContracts.Dto.Response.Capex.EstructuraCostoDtoResponse;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Capex
{
    public interface IEstructuraCostoDal : IRepository<EstructuraCosto>
    {

    }
}