﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using static Tgs.SDIO.DataContracts.Dto.Response.CartaFianza.CartaFianzaDetalleSeguimientoResponse;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza
{

   
    public interface ICartaFianzaDetalleRecuperoDal:IRepository<CartaFianzaDetalleRecupero>
    {
        ProcesoResponse ActualizaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest cartaFianzaDetalleRecuperoActualiza);
        CartaFianzaDetalleRecuperoResponsePaginado ListaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest cartaFianzaDetalleRecuperoLista);

    }
}
