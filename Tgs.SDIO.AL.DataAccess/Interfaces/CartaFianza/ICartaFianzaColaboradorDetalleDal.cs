﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza
{
    public interface ICartaFianzaColaboradorDetalleDal : IRepository<CartaFianzaColaboradorDetalle>
    {
        List<CartaFianzaColaboradorDetalleResponse> ListarColaborador(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestList);
        ProcesoResponse AsignarSupervisorAGerenteCuenta(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest);
        ProcesoResponse AsignarGerenteCuentaACartaFianza(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest);
        ProcesoResponse ActualizarTesorera(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest);
    }
}