﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza
{
    public interface ICartaFianzaDal : IRepository<CartaFianzaMaestro>
    {

        CartaFianzaListaResponsePaginado ListaCartaFianza(CartaFianzaRequest CartaFianzaRequestLista);
        List<CartaFianzaListaResponse> ListaCartaFianzaId(CartaFianzaRequest CartaFianzaRequestLista);

        ProcesoResponse InsertaAccionMailCartaFianza(CartaFianzaDetalleRenovacionRequest CartaFianzaRequestActualiza);
        ProcesoResponse ActualizaARenovacionCartaFianza(CartaFianzaDetalleRenovacionRequest CartaFianzaRequestActualiza);
        MailResponse ObtenerDatosCorreo(MailRequest mailResponse);
        List<CartaFianzaListaResponse> ListaCartaFianzaCombo(CartaFianzaRequest cartaFianzaRequestLista);
        DashBoardCartaFianzaResponse ListaDatos_DashoBoard_CartaFianza(DashBoardCartaFianzaRequest dashBoardCartaFianza);
        List<DashBoardCartaFianzaListGResponse> ListaDatos_DashoBoard_CartaFianza_Pie(DashBoardCartaFianzaRequest dashBoardCartaFianza);
        List<CartaFianzaListaResponse> CartasFianzasSinRespuesta(CartaFianzaRequest cartaFianzaRequest);
        ProcesoResponse GuardarRespuestaGerenteCuenta(CartaFianzaDetalleRenovacionRequest CartaFianzaRenovacion);
        ProcesoResponse GuardarConfirmacionGerenteCuenta(CartaFianzaDetalleRenovacionRequest CartaFianzaRenovacion);
    }

}
