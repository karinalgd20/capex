﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.Entities.Entities.CartaFianza;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza
{

    
    public interface ICartaFianzaDetalleEstadoDal:IRepository<CartaFianzaDetalleEstado>
    {
        List<CartaFianzaDetalleEstadoResponse> InsertaCartaFianzaDetalleEstado(CartaFianzaDetalleEstadoRequest CartaFianzaDetalleEstadoRequestInserta);
        CartaFianzaDetalleEstadoResponse ActualizaCartaFianzaDetalleEstado(CartaFianzaDetalleEstadoRequest CartaFianzaDetalleEstadoequestActualiza);
        CartaFianzaDetalleEstadoResponse ListaCartaFianzaDetalleEstado(CartaFianzaDetalleEstadoRequest CartaFianzaDetalleEstadoequestLista);


    }
}
