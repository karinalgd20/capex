﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza
{

  
    public interface ICartaFianzaDetalleRenovacionDal:IRepository<CartaFianzaDetalleRenovacion>
    {
        CartaFianzaDetalleRenovacionResponsePaginado ListaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest CartaFianzaDetalleRenovacionLista);
        MailResponse ObtenerDatosCorreoRenovacion(MailRequest mailResponse);

    }
}
