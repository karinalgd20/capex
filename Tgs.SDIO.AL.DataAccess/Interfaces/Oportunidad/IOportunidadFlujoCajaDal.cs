﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad
{
    public interface IOportunidadFlujoCajaDal : IRepository<OportunidadFlujoCaja>
    {
        List<OportunidadFlujoCajaDtoResponse> GeneraCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio);
        List<OportunidadFlujoCajaDtoResponse> GeneraServicio(OportunidadFlujoCajaDtoRequest casonegocio);
        ProcesoResponse EliminarCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio);
        List<OportunidadFlujoCajaDtoResponse> ListarOportunidadFlujoCajaBandeja(OportunidadFlujoCajaDtoRequest flujocaja);
        List<ListaDtoResponse> ListaAnoMesProyecto(OportunidadFlujoCajaDtoRequest flujocaja);
        List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio);
        List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio);
        List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadEcapexTotal(OportunidadFlujoCajaDtoRequest casonegocio);
        OportunidadFlujoCajaPaginadoDtoResponse ListarSubservicioPaginado(OportunidadFlujoCajaDtoRequest oportunidadServicioCMI);
        OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio);
        OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio);
        OportunidadFlujoCajaDtoResponse ObtenerSubServicio(OportunidadFlujoCajaDtoRequest solicitud);
        OportunidadFlujoCajaPaginadoDtoResponse ListarServiciosCircuitosPaginado(OportunidadFlujoCajaDtoRequest flujocaja);
        List<ListaDtoResponse> ListaServicioPorOportunidadLineaNegocio(OportunidadFlujoCajaDtoRequest flujocaja);
        List<ListaDtoResponse> ListarPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        OportunidadFlujoCajaDtoResponse ObtenerServicioPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadCaratulaTotal(OportunidadFlujoCajaDtoRequest casonegocio);
    }
}