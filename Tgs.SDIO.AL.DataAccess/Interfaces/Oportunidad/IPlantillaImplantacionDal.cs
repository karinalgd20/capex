﻿using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad
{
    public interface IPlantillaImplantacionDal : IRepository<PlantillaImplantacion>
    {
        PlantillaImplantacionPaginadoDtoResponse ListarPlantillaImplantacionPaginado(PlantillaImplantacionDtoRequest filtro);
    }
}
