﻿using System;
using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad
{


    public interface IOportunidadDal : IRepository<Entities.Entities.Oportunidad.Oportunidad>
    {
        List<OportunidadDtoResponse> ListarOportunidad(OportunidadDtoRequest oportunidad);

        ProcesoResponse OportunidadGanador(OportunidadDtoRequest oportunidad);

        List<OportunidadDtoResponse> ListarOportunidadCabecera(OportunidadDtoRequest oportunidad);

        List<OportunidadDtoResponse> ListarProyectadoOIBDA(OportunidadDtoRequest oportunidad);

        ProcesoResponse OportunidadInactivar(OportunidadDtoRequest oportunidad);
        ProcesoResponse RegistrarVersionOportunidad(OportunidadDtoRequest oportunidad);
        int OportunidadUltimoId();

        OportunidadDtoResponse ObtenerNumeroVersionesOportunidad(OportunidadDtoRequest oportunidad);
    }
}



