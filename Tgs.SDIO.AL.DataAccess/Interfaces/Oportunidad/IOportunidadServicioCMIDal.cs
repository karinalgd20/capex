﻿using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad
{
    public interface IOportunidadServicioCMIDal : IRepository<OportunidadServicioCMI>
    {
        ProcesoResponse RegistrarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidadServicioCMI);                  
        OportunidadServicioCMIPaginadoDtoResponse ListarOportunidadServicioCMIPaginado(OportunidadServicioCMIDtoRequest oportunidadServicioCMI);
    }
}
 