﻿using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad
{
    public interface ISituacionActualDal : IRepository<SituacionActual>
    {
        /// <summary>
        /// Obtiene un objeto situacion actual por el id de la oportunidad y el tipo
        /// </summary>
        /// <param name="request">Parametro para el filtro</param>
        /// <returns>Retorna un objeto de tipo situación actual</returns>
        SituacionActualDtoResponse ObtenerPorIdOportunidad(SituacionActualDtoRequest request);
    }
}