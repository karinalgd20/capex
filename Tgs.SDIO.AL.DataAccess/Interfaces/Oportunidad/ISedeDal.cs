﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad
{
    public interface ISedeDal : IRepository<Sede>
    {
        SedePaginadoDtoResponse ListarSedeServiciosPaginado(SedeDtoRequest request);
        List<SedeDtoResponse> ListarSedeInstalacion(OportunidadDtoRequest oportunidad);
        SedePaginadoDtoResponse ListarSedeCliente(OportunidadDtoRequest request);
        SedeDtoResponse ObtenerSedePorId(SedeDtoRequest request);
        ServicioSedeInstalacionDtoResponse ObtenerServicioSedeInstalacionPorId(ServicioSedeInstalacionDtoRequest request);
        ClienteDtoResponse ObtenerClientePorIdOportunidad(OportunidadDtoRequest request);
        SedeDtoResponse ObtenerCodUbigeo(SedeDtoRequest request);
        SedeInstalacionDtoResponse obtenerSedeInstalada(int IdSede, int IdOportunidad);
        UbigeoDtoResponse ObtenerCodDetalleUbigeo(int Id);
        SedeDtoResponse DetalleSedePorId(SedeDtoRequest request);
        SedeDtoResponse DireccionBuscar(SedeDtoRequest request);



    }
}