﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad
{
    public interface IOportunidadFlujoCajaConfiguracionDal : IRepository<OportunidadFlujoCajaConfiguracion>
    {
        List<OportunidadFlujoCajaConfiguracionDtoResponse> ListarOportunidadFlujoCajaConfiguracionBandeja(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);

        List<OportunidadFlujoCajaConfiguracionDtoResponse> ListarOportunidadFlujoCajaConfiguraciones(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);
        OportunidadFlujoCajaConfiguracionDtoResponse ObtenerOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);
    }
}
