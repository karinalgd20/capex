﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad
{
    public interface IVisitaDetalleDal : IRepository<VisitaDetalle>
    {
        VisitaDetallePaginadoDtoResponse ListarVisitaDetallePaginado(VisitaDetalleDtoRequest visitaDetalle);
    }
}