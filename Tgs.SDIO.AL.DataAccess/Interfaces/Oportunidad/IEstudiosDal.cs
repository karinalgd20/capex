﻿using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad
{
    public interface IEstudiosDal : IRepository<Estudios>
    {
        EstudiosPaginadoDtoResponse ListarEstudiosPaginado(EstudiosDtoRequest request);

    }
}
