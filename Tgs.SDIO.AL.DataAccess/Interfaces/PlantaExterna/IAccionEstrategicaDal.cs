﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.PlantaExterna;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.PlantaExterna
{
    public interface IAccionEstrategicaDal : IRepository<AccionEstrategica>
    {
    }
}
