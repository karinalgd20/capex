﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;
using Tgs.SDIO.Entities.Entities.PlantaExterna;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.PlantaExterna
{
    public interface INoPepsDal : IRepository<NoPeps>
    {
        NoPepsPaginadoDtoResponse ListarNoPeps(NoPepsDtoRequest noPepsRequest);
    }
}
