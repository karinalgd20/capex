﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.Entities.Entities.Compra;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Compra
{
    public interface IPrePeticionOrdinariaDetalleDal : IRepository<PrePeticionOrdinariaDetalle>
    {
        PrePeticionOrdinariaDetalleDtoResponse ObtenerDetalleOrdinaria(PrePeticionOrdinariaDetalleDtoRequest request);
        List<PrePeticionOrdinariaDetalleDtoResponse> ListarPrePeticionOrdinaria(PrePeticionOrdinariaDetalleDtoRequest request);
    }
}
