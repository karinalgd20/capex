﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.Entities.Entities.Compra;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Compra
{
    public interface IPrePeticionCabeceraDal : IRepository<PrePeticionCabecera>
    {
        int ValidacionMontoLimite(PrePeticionCabeceraDtoRequest request);
        string ObtenerSaldo(PrePeticionCabeceraDtoRequest request);
        PrePeticionCabeceraDtoResponse ObtenerPrePeticionCabecera(PrePeticionCabeceraDtoRequest request);
        List<PrePeticionCabeceraDtoResponse> ListarPrePeticion(PrePeticionCabeceraDtoRequest request);
        List<PrePeticionCabeceraDtoResponse> ListarPrePeticionCabecera(PrePeticionCabeceraDtoRequest request);
    }
}
