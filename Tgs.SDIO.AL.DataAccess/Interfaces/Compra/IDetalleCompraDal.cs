﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Compra
{
    public interface IDetalleCompraDal : IRepository<DetalleCompra>
    {
        List<DetalleCompraDtoResponse> ListarDetalleCompra(DetalleCompraDtoRequest request);

        ProcesoResponse RegistrarDetalleCompra(DetalleCompraDtoRequest request);

        DetalleCompraDtoResponse ObtenerDetalleCompraPorId(DetalleCompraDtoRequest request);
    }
}