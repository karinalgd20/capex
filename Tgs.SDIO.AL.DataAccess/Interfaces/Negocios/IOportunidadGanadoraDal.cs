﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;


namespace Tgs.SDIO.AL.DataAccess.Interfaces.Negocios
{
    public interface IOportunidadGanadoraDal : IRepository<OportunidadGanadora>
    {
        List<OportunidadGanadoraDtoResponse> ListarOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        BandejaOportunidadPaginadoDtoResponse ListarBandejaOportunidadPaginado(OportunidadGanadoraDtoRequest request);
        SalesForceConsolidadoCabeceraDtoResponse ObtenerCasoOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        ClienteDtoResponse ObtenerRucClientePorIdOportunidad(OportunidadGanadoraDtoRequest request);
        OportunidadGanadoraDtoResponse ObtenerOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        OportunidadGanadoraPaginadoDtoResponse ObtenerListaCodigo();
        OportunidadGanadoraPaginadoDtoResponse ObtenerListaOportunidad();
        ClienteDtoResponse ObtenerClientePorRuc(ClienteDtoRequest request);
        SisegoCotizadoDtoResponse ObtenerSisegoCotizadoPorIdOporNroSisego(SisegoCotizadoDtoRequest request);
        ClienteDtoResponse ObtenerDetallesOportunidadGanadora(IsisNroOfertaDtoRequest request);
        IsisNroOfertaDtoResponse ObtenerIsisNroOfertaPorNroPer_NroOferta(IsisNroOfertaDtoRequest request);
        OportunidadGanadoraDtoResponse ObtenerOportunidadxPer(OportunidadGanadoraDtoRequest request);
        OportunidadGanadoraDtoResponse ObtenerOportunidadById(OportunidadGanadoraDtoRequest request);
        RPAEquiposServicioDtoResponse ObtenerRPAEquiposServicioPorIdOferta_IdRPASerNroOfer(RPAEquiposServicioDtoRequest request);
        RPASisegoDetalleDtoResponse ObtenerRPASisegoDetallePorIdSisego(RPASisegoDetalleDtoRequest request);
        RPASisegoDatosContactoDtoResponse ObtenerRPASisegoDatosContactoPorId_RPASisegoDetalle(RPASisegoDatosContactoDtoRequest request);
        RPAServiciosxNroOfertaDtoResponse ObtenerRPAServiciosxNroOfertaPorId_Oferta(RPAServiciosxNroOfertaDtoRequest request);



    }
}
