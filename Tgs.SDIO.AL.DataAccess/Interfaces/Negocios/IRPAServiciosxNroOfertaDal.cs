﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Negocios
{
    public interface IRPAServiciosxNroOfertaDal : IRepository<RPAServiciosxNroOferta>
    {
        ProcesoResponse AgregarServiciosNroOferta(RPAServiciosxNroOfertaDtoRequest request);
        IsisNroOfertaDtoResponse ObtenerNroOfertaById(AsociarNroOfertaSisegosDtoRequest request);
        SisegoCotizadoDtoResponse ObtenerCodSisegoById(AsociarNroOfertaSisegosDtoRequest request);
        List<RPAServiciosxNroOfertaDtoResponse> ListarServiciosNroOferta(RPAServiciosxNroOfertaDtoRequest request);
        RPAServiciosxNroOfertaPaginadoDtoResponse ListarServiciosNroOfertaPaginado(RPAServiciosxNroOfertaDtoRequest request);
        
    }
}
