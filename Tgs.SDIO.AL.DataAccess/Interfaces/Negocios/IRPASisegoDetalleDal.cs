﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Negocios
{
    public interface IRPASisegoDetalleDal : IRepository<RPASisegoDetalle>
    {
        ProcesoResponse AgregarSisegoDetalle(RPASisegoDetalleDtoRequest request);
        List<RPASisegoDetalleDtoResponse> ListarSisegoDetalle(RPASisegoDetalleDtoRequest request);
        RPASisegoDetallePaginadoDtoResponse ListarSisegoDetallePaginado(RPASisegoDetalleDtoRequest request);
    }
}
