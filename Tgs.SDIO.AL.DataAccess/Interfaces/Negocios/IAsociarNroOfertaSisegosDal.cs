﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Negocios
{
    public interface IAsociarNroOfertaSisegosDal : IRepository<AsociarNroOfertaSisegos>
    {
        ProcesoResponse AgregarOfertaSisegos(AsociarNroOfertaSisegosDtoRequest resquest);
        List<AsociarNroOfertaSisegosDtoResponse> ListarOfertaSisegos(AsociarNroOfertaSisegosDtoRequest resquest);

        AsociarOfertaSisegosPaginadoDtoResponse ListarOfertaSisegosPaginado(AsociarNroOfertaSisegosDtoRequest resquest);
        IsisNroOfertaDtoResponse ObtenerNroOfertaById(AsociarNroOfertaSisegosDtoRequest resquest);
        SisegoCotizadoDtoResponse ObtenerCodSisegoById(AsociarNroOfertaSisegosDtoRequest resquest);
    }
}
