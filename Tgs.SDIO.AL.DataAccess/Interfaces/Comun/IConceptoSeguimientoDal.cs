﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IConceptoSeguimientoDal : IRepository<ConceptoSeguimiento>
    {
        ConceptoSeguimientoPaginadoDtoResponse ListadoConceptosSeguimientoPaginado(ConceptoSeguimientoDtoRequest request);
        List<ListaDtoResponse> ListarComboConceptoSeguimiento();
        List<ListaDtoResponse> ListarComboConceptoSeguimientoAgrupador();
        List<ListaDtoResponse> ListarComboConceptoSeguimientoNiveles();
    }
}
