﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IClienteDal: IRepository<Cliente>
    {

        List<ClienteDtoResponse> ListarCliente(ClienteDtoRequest cliente);
        ClienteDtoResponsePaginado ListarClientePaginado(ClienteDtoRequest cliente);
        List<ListaDtoResponse> ListarClienteCartaFianza(ClienteDtoRequest cliente);
        List<ClienteDtoResponse> ListarClientePorDescripcion(ClienteDtoRequest cliente);
    }
}
