﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
   public interface ISegmentoNegocioDal : IRepository<SegmentoNegocio>
    {
        List<ComboDtoResponse> ListarComboSegmentoNegocio();

        List<ListaDtoResponse> ListarComboSegmentoNegocioSimple();
    }
}
