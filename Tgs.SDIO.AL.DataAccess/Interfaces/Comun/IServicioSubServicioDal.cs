﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IServicioSubServicioDal : IRepository<ServicioSubServicio>
    {

        ServicioSubServicioPaginadoDtoResponse ListaServicioSubServicioPaginado(ServicioSubServicioDtoRequest ServiciosubServicio);

        ServicioSubServicioPaginadoDtoResponse ListarServicioSubServicioGrupos(ServicioSubServicioDtoRequest ServiciosubServicio);
        int ObtenerIdServicioSubServicio(int idServicio);
    }
}
