﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface ISectorDal : IRepository<Sector>         
    {
        List<ListaDtoResponse> ListarComboSector();

    }
}
