﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IActividadDal : IRepository<Actividad>
    {
        ActividadPaginadoDtoResponse ListatActividadPaginado(ActividadDtoRequest request);

        ActividadDtoResponse ObtenerActividadPorId(ActividadDtoRequest area);

        List<ListaDtoResponse> ListarComboActividadPadres();
    }
}
