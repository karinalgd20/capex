﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IProveedorDal : IRepository<Proveedor>
    {
        List<ProveedorDtoResponse> ListarProveedores(ProveedorDtoRequest objProveedor);
        ProveedorPaginadoDtoResponse ListarProveedorPaginado(ProveedorDtoRequest objProveedor);

    }
}
