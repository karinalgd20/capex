﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IArchivoDal : IRepository<Archivo>
    {
        /// <summary>
        /// Lista los archivos
        /// </summary>
        /// <param name="request">Parametro de solicitud de archivo</param>
        /// <returns>Devuelve una lista de objetos de archivos</returns>
        ArchivoPaginadoDtoResponse ListarPaginado(ArchivoDtoRequest request);
    }
}