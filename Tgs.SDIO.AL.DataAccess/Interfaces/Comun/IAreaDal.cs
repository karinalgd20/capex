﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;


namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IAreaDal: IRepository<Area>
    {
        List<ListaDtoResponse> ListarComboArea();
    }
}
