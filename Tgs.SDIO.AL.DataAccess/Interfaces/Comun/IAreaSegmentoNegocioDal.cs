﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IAreaSegmentoNegocioDal : IRepository<AreaSegmentoNegocio>
    {
    }
}
