﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;


namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IAreaSeguimientoDal : IRepository<AreaSeguimiento>
    {
        AreaSeguimientoPaginadoDtoResponse ListadoAreasSeguimientoPaginado(AreaSeguimientoDtoRequest request);

        AreaSeguimientoDtoResponse ObtenerAreaSeguimientoPorId(AreaSeguimientoDtoRequest area);

        List<ListaDtoResponse> ListarComboAreaSeguimiento();

    }
}
