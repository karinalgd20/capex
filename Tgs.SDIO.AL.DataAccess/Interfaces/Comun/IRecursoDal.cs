﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IRecursoDal : IRepository<Recurso>
    {
        RecursoPaginadoDtoResponse ListadoRecursosPaginado(RecursoDtoRequest request);

        RecursoPaginadoDtoResponse ListaRecursoModalRecursoPaginado(RecursoDtoRequest request);

        RecursoDtoResponse ListarRecursoDeUsuario(RecursoDtoRequest request);

        RecursoPaginadoDtoResponse ListadoRecursosByCargoPaginado(RecursoDtoRequest request);
    }
}
