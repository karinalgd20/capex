﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface ISalesForceConsolidadoCabeceraDal : IRepository<SalesForceConsolidadoCabecera>
    {
        List<SalesForceConsolidadoCabecera> SalesForceConsolidadoCabecera(SalesForceConsolidadoCabecera request);
        List<SalesForceConsolidadoCabeceraResponse> ListarProbabilidad();
        IEnumerable<SalesForceConsolidadoCabeceraDtoResponse> ListarIndicadorCostoOportunidad(IndicadorMadurezDtoRequest request);
        /// <summary>
        /// Obtiene un SalesForceConsolidadoCabecera por el id de la oportunidad
        /// </summary>
        /// <param name="IdOportunidad">Id de la oportunidad</param>
        /// <returns>Retorna un SalesForceConsolidadoCabecera</returns>
        SalesForceConsolidadoCabecera ObtenerPorIdOportunidad(string IdOportunidad);
        List<SalesForceConsolidadoCabeceraDtoResponse> ListarNumeroSalesForcePorIdOportunidad(SalesForceConsolidadoCabeceraDtoRequest salesForceConsolidadoCabecera);
    }
}