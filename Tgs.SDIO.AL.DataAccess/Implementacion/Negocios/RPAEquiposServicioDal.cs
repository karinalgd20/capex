﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Negocios
{
    public class RPAEquiposServicioDal : Repository<RPAEquiposServicio>, IRPAEquiposServicioDal
    {
        readonly DioContext context;
        public RPAEquiposServicioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public ProcesoResponse AgregarEquipoServicio(RPAEquiposServicioDtoRequest request)
        {
            return null;
        }
        public List<RPAEquiposServicioDtoResponse> ListarEquipoServicio(RPAEquiposServicioDtoRequest request)
        {
            var query = (from eq in context.RPAEquiposServicio
                         join aso in context.AsociarServicioSisegos on eq.Id equals aso.IdEquipo
                         where aso.Id == request.IdServicioSisego
                         select new RPAEquiposServicioDtoResponse
                         {
                             Id = eq.Id,
                             Cod_Equipo = eq.Cod_Equipo,
                             Tipo = eq.Tipo,
                             Marca = eq.Marca,
                             Modelo = eq.Modelo,
                             Componente_1 = eq.Componente_1,
                             Tipo_de_acceso = eq.Tipo_de_acceso

                         }).ToList();

            return query;
        }
        public RPAEquiposServicioPaginadoDtoResponse ListarEquipoServicioPaginado(RPAEquiposServicioDtoRequest request)
        {
            var query = (from eq in context.RPAEquiposServicio
                         join aso in context.AsociarServicioSisegos on eq.Id equals aso.IdEquipo
                         where aso.Id == request.IdServicioSisego
                         select new RPAEquiposServicioDtoResponse
                         {
                             Id = eq.Id,
                             Cod_Equipo = eq.Cod_Equipo,
                             Tipo = eq.Tipo,
                             Marca = eq.Marca,
                             Modelo = eq.Modelo,
                             Componente_1 = eq.Componente_1,
                             Tipo_de_acceso = eq.Tipo_de_acceso

                         }).ToList();

            var resultado = new RPAEquiposServicioPaginadoDtoResponse
            {
                ListaEquiposServicioDtoResponse = query.ToList(),
            };

            return resultado;
        }
    }
}
