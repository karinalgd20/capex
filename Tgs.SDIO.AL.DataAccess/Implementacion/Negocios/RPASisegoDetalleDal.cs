﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Negocios
{
    public class RPASisegoDetalleDal : Repository<RPASisegoDetalle>, IRPASisegoDetalleDal
    {
        readonly DioContext context;
        public RPASisegoDetalleDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = unitOfWork as DioContext;
        }

        public ProcesoResponse AgregarSisegoDetalle(RPASisegoDetalleDtoRequest request)
        {
            return null;
        }
        public List<RPASisegoDetalleDtoResponse> ListarSisegoDetalle(RPASisegoDetalleDtoRequest request)
        {
            
            var query = (from sd in context.RPASisegoDetalle
                         join sc in context.AsociarServicioSisegos on sd.Id equals sc.IdSisego
                         //join of in context.AsociarNroOfertaSisegos on sc.codigo_sisego equals of.CodSisego
                         where sc.Id == request.Id
                         select new RPASisegoDetalleDtoResponse
                         {
                             Via = sd.Via,
                             Nro_Piso = sd.Nro_Piso,
                             Interior = sd.Interior,
                             Numero = sd.Numero,
                             Departamento = sd.Departamento,
                             Provincia = sd.Provincia,
                             Ciudad = sd.Ciudad,
                             Distrito = sd.Distrito,
                             Direccion = sd.Direccion

                         }).ToList();

            return query;
        }
        public RPASisegoDetallePaginadoDtoResponse ListarSisegoDetallePaginado(RPASisegoDetalleDtoRequest request)
        {
            var query = (from sd in context.RPASisegoDetalle
                         join sc in context.SisegoCotizado on sd.Id_sisego equals sc.Id
                         join op in context.OportunidadGanadora on sc.Id_OportunidadGanadora equals op.Id
                         join of in context.IsisNroOferta on op.Id equals of.Id_OportunidadGanadora
                         where of.Id == request.Id_Oferta
                         select new RPASisegoDetalleDtoResponse
                         {
                             Id = sd.Id,
                             Id_sisego = sd.Id_sisego,
                             CodSisego = sc.codigo_sisego,
                             Ciudad = sd.Ciudad,
                             Departamento = sd.Departamento,
                             Provincia = sd.Provincia,
                             Distrito = sd.Distrito,
                             Direccion = sd.Direccion,
                             Nro_Piso = sd.Nro_Piso,
                             Numero = sd.Numero,
                             Interior = sd.Interior,
                             Manzana = sd.Manzana,
                             Lote = sd.Lote
                         });

            var resultado = new RPASisegoDetallePaginadoDtoResponse
            {
                ListaSisegoDetalleDtoResponse = query.ToList()
            };

            return resultado;
        }
    }
}
