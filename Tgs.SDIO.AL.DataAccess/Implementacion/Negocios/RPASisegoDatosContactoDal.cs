﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Negocios
{
    public class RPASisegoDatosContactoDal : Repository<RPASisegoDatosContacto>, IRPASisegoDatosContactoDal
    {
        readonly DioContext context;
        public RPASisegoDatosContactoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = unitOfWork as DioContext;
        }

        public List<RPASisegoDatosContactoDtoResponse> ListaDetallesContactoRPA(RPASisegoDatosContactoDtoRequest resquest)
        {
            var query = (from cd in context.RPASisegoDatosContacto
                         join aso in context.RPAServiciosxNroOferta on cd.Id equals aso.Contacto_Id
                         join ass in context.AsociarServicioSisegos on aso.Id equals ass.IdServicio
                         where ass.Id == resquest.IdServicio
                         select new RPASisegoDatosContactoDtoResponse
                         {
                             Id = cd.Id,
                             Contacto = cd.Contacto,
                             Telefono_1 = cd.Telefono_1,
                             Telefono_2 = cd.Telefono_2,
                             Telefono_3 = cd.Telefono_3,
                             Contacto_2 = cd.Contacto_2,
                             Telefono1_Contacto2 = cd.Telefono1_Contacto2,
                             Telefono2_Contacto2 = cd.Telefono2_Contacto2,
                             Telefono3_Contacto2 = cd.Telefono3_Contacto2

                         }).ToList();
            return query;
        }
        public RPASisegoDatosContactoPaginadoDtoResponse ListaDetallesContactoRPAPaginadoDtoResponse(RPASisegoDatosContactoDtoRequest resquest)
        {
            var query = (from cd in context.RPASisegoDatosContacto
                         join aso in context.RPAServiciosxNroOferta on cd.Id equals aso.Contacto_Id
                         join ass in context.AsociarServicioSisegos on aso.Id equals ass.IdServicio
                         where ass.Id == resquest.IdServicio
                         select new RPASisegoDatosContactoDtoResponse
                         {
                             Id = cd.Id,
                             Contacto = cd.Contacto,
                             Telefono_1 = cd.Telefono_1,
                             Telefono_2 = cd.Telefono_2,
                             Telefono_3 = cd.Telefono_3,
                             Contacto_2 = cd.Contacto_2,
                             Telefono1_Contacto2 = cd.Telefono1_Contacto2,
                             Telefono2_Contacto2 = cd.Telefono2_Contacto2,
                             Telefono3_Contacto2 = cd.Telefono3_Contacto2

                         });

            var resultado = new RPASisegoDatosContactoPaginadoDtoResponse
            {
                ListaDetallesContactoRPAPaginadoDtoResponse = query.ToList(),
            };

            return resultado;
        }
    }
}
