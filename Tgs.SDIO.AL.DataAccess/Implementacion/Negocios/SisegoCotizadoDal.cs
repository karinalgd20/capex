﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Negocios
{
    public class SisegoCotizadoDal : Repository<SisegoCotizado>, ISisegoCotizadoDal
    {
        readonly DioContext context;
        public SisegoCotizadoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public SisegoCotizadoPaginadoDtoResponse ListarSisegoCotizadoPaginado(SisegoCotizadoDtoRequest resquest)
        {
            
            var query = (from sg in context.SisegoCotizado
                         join op in context.OportunidadGanadora on sg.Id_OportunidadGanadora equals op.Id
                         where sg.Id_OportunidadGanadora == resquest.Id_OportunidadGanadora && sg.IdEstado == Generales.Estados.Activo
                         select new SisegoCotizadoDtoResponse
                         {
                             Id = sg.Id,
                             Id_OportunidadGanadora = sg.Id_OportunidadGanadora,
                             codigo_sisego = sg.codigo_sisego,
                             IdEstado = sg.IdEstado,
                             FechaCreacion = sg.FechaCreacion,
                             IdUsuarioCreacion = sg.IdUsuarioCreacion,
                             FechaEdicion = sg.FechaEdicion,
                             IdUsuarioEdicion = sg.IdUsuarioEdicion
                         });
            var resultado = new SisegoCotizadoPaginadoDtoResponse
            {
                ListaSisegoCotizadoDtoResponse = query.ToList(),
            };
            return resultado;
        }

        public List<SisegoCotizadoDtoResponse> ListarSisegosCotizados(SisegoCotizadoDtoRequest resquest)
        {
            var query = (from sise in context.Set<SisegoCotizado>()
                         where sise.IdEstado == Generales.Estados.Activo
                         select new SisegoCotizadoDtoResponse
                         {
                             Id_OportunidadGanadora = sise.Id_OportunidadGanadora,
                             codigo_sisego = sise.codigo_sisego
                         }).ToList<SisegoCotizadoDtoResponse>();
            return query;
        }
             
    }
}
