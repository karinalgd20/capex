﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Negocios
{
    public class AsociarNroOfertaSisegosDal : Repository<AsociarNroOfertaSisegos>, IAsociarNroOfertaSisegosDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public AsociarNroOfertaSisegosDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
        public ProcesoResponse AgregarOfertaSisegos(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            var IdOferta = new SqlParameter { ParameterName = "IdOferta", Value = resquest.IdOferta, SqlDbType = SqlDbType.Int };
            var NroOferta = new SqlParameter { ParameterName = "NroOferta", Value = resquest.NroOferta, SqlDbType = SqlDbType.VarChar };
            var ListSisego = new SqlParameter { ParameterName = "SisegosStr", Value = resquest.ListSisego, SqlDbType = SqlDbType.VarChar };
            var IdUsuarioCreacion = new SqlParameter { ParameterName = "IdUsuarioCreacion", Value = resquest.IdUsuarioCreacion, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = resquest.IdEstado, SqlDbType = SqlDbType.Int };

            var response = context.ExecuteCommand("NEGOCIOS.SP_ASOCIAR_OFERTAS_SISEGO @IdOferta, @NroOferta, @SisegosStr, @IdUsuarioCreacion, @IdEstado",
                                                                                        IdOferta, NroOferta, ListSisego, IdUsuarioCreacion, IdEstado);
            respuesta.Id = resquest.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = "Asociacion se Realizo Correctamente";

            return respuesta;
        }
        public IsisNroOfertaDtoResponse ObtenerNroOfertaById(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            var NroOferta = (from of in context.IsisNroOferta
                             where of.Id == resquest.IdOferta
                             select new IsisNroOfertaDtoResponse
                             {

                                 Nro_oferta = of.Nro_oferta

                             }).AsNoTracking().FirstOrDefault();
            return NroOferta;
        }
        public SisegoCotizadoDtoResponse ObtenerCodSisegoById(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            var CodSisego = (from sg in context.SisegoCotizado
                             where sg.Id == resquest.IdSisego
                             select new SisegoCotizadoDtoResponse
                             {

                                 codigo_sisego = sg.codigo_sisego

                             }).AsNoTracking().FirstOrDefault();
            return CodSisego;

            //foreach (var Codigos in resquest.ListSisego)
            //{
            //    var CodSisego = (from sg in context.SisegoCotizado
            //                     where sg.Id == Convert.ToInt32(Codigos.id)
            //                     select new SisegoCotizadoDtoResponse
            //                     {
            //                         codigo_sisego = sg.codigo_sisego

            //                     });
            //    var resultado = CodSisego.ToList();
            //}

            //return resultado;
        }
        public List<AsociarNroOfertaSisegosDtoResponse> ListarOfertaSisegos(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            // var Lista = (from s in context.AsociarNroOfertaSisegos).ToList<AsociarNroOfertaSisegos>(); 
            return null; 
        }
        public AsociarOfertaSisegosPaginadoDtoResponse ListarOfertaSisegosPaginado(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            var query = (from aso in context.AsociarNroOfertaSisegos
                         join isis in context.IsisNroOferta on aso.IdOferta equals isis.Id
                         join op in context.OportunidadGanadora on isis.Id_OportunidadGanadora equals op.Id
                         where op.Id == resquest.IdOportunidad
                         select new AsociarNroOfertaSisegosDtoResponse
                         {
                             IdOferta = aso.IdOferta,
                             NroOferta = aso.NroOferta,
                             IdSisego = aso.IdSisego,
                             CodSisego = aso.CodSisego, 
                             IdEstado = resquest.IdEstado, 
                             IdUsuarioCreacion = resquest.IdUsuarioCreacion,
                             FechaCreacion = resquest.FechaCreacion                        
                         });
            var resultado = new AsociarOfertaSisegosPaginadoDtoResponse
            {
                ListaAsociarOfertaSisegosDtoResponse = query.ToList()
            };

            return resultado;
        }
    }
}
