﻿using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Negocios;
using System.Data.Entity;
using Tgs.SDIO.Util.Constantes;
using static Tgs.SDIO.Util.Constantes.Generales;
using System;
using static Tgs.SDIO.Util.Constantes.Negocio;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Negocios
{
    public class OportunidadGanadoraDal : Repository<OportunidadGanadora>, IOportunidadGanadoraDal
    {
        readonly DioContext context;
        public OportunidadGanadoraDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public OportunidadGanadoraDtoResponse ObtenerOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            var objOportunidad = (from op in context.Set<OportunidadGanadora>()
                                  where
                                  op.PER == request.PER
                                  select new OportunidadGanadoraDtoResponse
                                  {
                                      Id = op.Id,
                                      FechaCreacion = op.FechaCreacion,
                                      IdUsuarioCreacion = op.IdUsuarioCreacion
                                  }).AsNoTracking().FirstOrDefault();
            return objOportunidad;
        }

        public ClienteDtoResponse ObtenerRucClientePorIdOportunidad(OportunidadGanadoraDtoRequest request)
        {
            var queryRuc = (from og in context.OportunidadGanadora
                            join sfc in context.SalesForceConsolidadoCabecera on og.PER equals sfc.IdOportunidad
                            join  c in context.Cliente on sfc.IdCLiente equals c.IdCliente  
                            where og.PER == request.PER
                            select new ClienteDtoResponse
                               {
                                    Descripcion = c.Descripcion,
                                    NumeroIdentificadorFiscal = (c.NumeroIdentificadorFiscal == null ? "" : c.NumeroIdentificadorFiscal),
                                    CodigoCliente = c.CodigoCliente
                                }).AsNoTracking().FirstOrDefault();

            return queryRuc;
        }

        public SalesForceConsolidadoCabeceraDtoResponse ObtenerCasoOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            var NroCasoDerivado = (from sfc in context.SalesForceConsolidadoCabecera
                                   where sfc.IdOportunidad == request.PER
                                   select new SalesForceConsolidadoCabeceraDtoResponse
                                   {
                                       NumeroDelCaso = sfc.NumeroDelCaso
                                   }).AsNoTracking().FirstOrDefault();
            return NroCasoDerivado;
        }

        public List<OportunidadGanadoraDtoResponse> ListarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            if (request.PER== null) { request.PER = ""; }
            if (request.Cliente == null) { request.Cliente = ""; }
            if (request.Ruc == null) { request.Ruc = ""; }
            if (request.CodigoCliente == null) { request.CodigoCliente = ""; }
            if (request.IdEstado == null) { request.IdEstado = 0; }
            var pPER = new SqlParameter { ParameterName = "pPER", Value = request.PER, SqlDbType = SqlDbType.VarChar};
            var pCLIENTE = new SqlParameter { ParameterName = "pCLIENTE", Value = request.Cliente, SqlDbType = SqlDbType.VarChar};
            var pRUC = new SqlParameter { ParameterName = "pRUC", Value = request.Ruc, SqlDbType = SqlDbType.VarChar };
            var pCODCLIENTE = new SqlParameter { ParameterName = "pCODCLIENTE", Value = request.CodigoCliente, SqlDbType = SqlDbType.VarChar};
            var pIDESTADO = new SqlParameter { ParameterName = "pIDESTADO", Value = request.IdEstado, SqlDbType = SqlDbType.Int };

            var Indice = new SqlParameter { ParameterName = "Indice", Value = request.Indice, SqlDbType = SqlDbType.Int };
            var Tamanio = new SqlParameter { ParameterName = "Tamanio", Value = request.Tamanio, SqlDbType = SqlDbType.Int };

            var lista = context.ExecuteQuery<OportunidadGanadoraDtoResponse>("NEGOCIOS.USP_LISTAR_BANDEJA_OPORTUNIDAD @pPER, @pCLIENTE, @pRUC, @pCODCLIENTE, @pIDESTADO, @Indice, @Tamanio",
                                                                                        pPER, pCLIENTE, pRUC, pCODCLIENTE, pIDESTADO, Indice, Tamanio).ToList<OportunidadGanadoraDtoResponse>();
            List<OportunidadGanadoraDtoResponse> ListarOportunidadGanadora = new List<OportunidadGanadoraDtoResponse>();

            foreach (var item in lista)
            {
                ListarOportunidadGanadora.Add(new OportunidadGanadoraDtoResponse()
                {
                    Id = item.Id,
                    PER = item.PER,
                    Ruc = item.Ruc,
                    IdEstado = item.IdEstado,
                    IdEtapa = item.IdEtapa,
                    FechaCreacion = item.FechaCreacion,
                    strFechaCreacion = item.FechaCreacion.ToString(),
                    Cliente = item.Cliente,
                    CodigoCliente = item.CodigoCliente,
                    Manipulado = item.IdUsuarioCreacion,
                    Progreso = item.Progreso
                });
            }

            if (request.FechaCreacion != DateTime.MinValue)
            {
                ListarOportunidadGanadora = ListarOportunidadGanadora.Where<OportunidadGanadoraDtoResponse>(o => o.FechaCreacion >= request.FechaCreacion).ToList<OportunidadGanadoraDtoResponse>();
            }

            ListarOportunidadGanadora.Sort((x, y) => -x.FechaCreacion.CompareTo(y.FechaCreacion));

            return ListarOportunidadGanadora;
        }

        public BandejaOportunidadPaginadoDtoResponse ListarBandejaOportunidadPaginado(OportunidadGanadoraDtoRequest request)
        {
            var query = (from op in context.OportunidadGanadora
                         join sfc in context.SalesForceConsolidadoCabecera on op.PER equals sfc.IdOportunidad
                         join cl in context.Cliente on sfc.IdCLiente equals cl.IdCliente
                         where
                         request.Ruc == "" && request.PER == "" 
                         &&
                         (op.IdEstado == Negocio.EstadosRPA.PorEnviar || op.IdEstado == Negocio.EstadosRPA.Pendiente ||
                         op.IdEstado == Negocio.EstadosRPA.EnProceso || op.IdEstado == Negocio.EstadosRPA.Expirado ||
                         op.IdEstado == Negocio.EstadosRPA.Observado || op.IdEstado == Negocio.EstadosRPA.Procesado ||
                         op.IdEstado == Negocio.EstadosRPA.Cancelado)
                         ||
                         cl.NumeroIdentificadorFiscal == request.Ruc
                         &&
                         (op.IdEstado == Negocio.EstadosRPA.PorEnviar || op.IdEstado == Negocio.EstadosRPA.Pendiente ||
                         op.IdEstado == Negocio.EstadosRPA.EnProceso || op.IdEstado == Negocio.EstadosRPA.Expirado ||
                         op.IdEstado == Negocio.EstadosRPA.Observado || op.IdEstado == Negocio.EstadosRPA.Procesado ||
                         op.IdEstado == Negocio.EstadosRPA.Cancelado)
                         ||
                         op.PER == request.PER
                         &&
                         (op.IdEstado == Negocio.EstadosRPA.PorEnviar || op.IdEstado == Negocio.EstadosRPA.Pendiente ||
                         op.IdEstado == Negocio.EstadosRPA.EnProceso || op.IdEstado == Negocio.EstadosRPA.Expirado ||
                         op.IdEstado == Negocio.EstadosRPA.Observado || op.IdEstado == Negocio.EstadosRPA.Procesado ||
                         op.IdEstado == Negocio.EstadosRPA.Cancelado)
                         
                         select new BandejaOportunidadDtoResponse
                         {
                             Id = op.Id,
                             PER = op.PER,
                             FechaCreacion = op.FechaCreacion,
                             strFechaCreacion = op.FechaCreacion.ToString(),
                             Cliente = cl.Descripcion,
                             RUC = cl.NumeroIdentificadorFiscal,
                             Etapa = op.IdEtapa.ToString(),
                             EstadoRPA = op.IdEstado.ToString(),
                             Progreso = sfc.ProbabilidadExito,
                             Manipulado = op.IdUsuarioCreacion.ToString()
                         });

          var resultado = new BandejaOportunidadPaginadoDtoResponse
            {
                ListaBandejaOportunidadPaginadoDtoResponse = query.ToList(),
            };

            return resultado;
        }

        public OportunidadGanadoraPaginadoDtoResponse ObtenerListaCodigo()
        {
            var query = (from T0 in context.Set<OportunidadGanadora>()
                         join T1 in context.Set<IsisNroOferta>() on T0.Id equals T1.Id_OportunidadGanadora
                         join T2 in context.Set<SisegoCotizado>() on T1.Id_OportunidadGanadora equals T2.Id_OportunidadGanadora
                         where T0.IdEstado == Negocio.EstadosRPA.Pendiente
                         select new OportunidadGanadoraDtoResponse
                                  {
                                      IdEstado = T0.IdEstado,
                                      IdUsuarioEdicion = T0.IdUsuarioEdicion,
                                      Id = T0.Id,
                                      PER = T0.PER,
                                      Nro_oferta = T1.Nro_oferta,
                                      Nro_version_Oferta = T1.Nro_version_Oferta,
                                      codigo_sisego = T2.codigo_sisego,
                                      CasoDerivado = T0.CasoDerivado
                                  });
            var resultado = new OportunidadGanadoraPaginadoDtoResponse
            {
                ListaOportunidadGanadoraPaginadoDtoResponse = query.ToList(),
            };
            return resultado;
        }

        public OportunidadGanadoraPaginadoDtoResponse ObtenerListaOportunidad()
        {
            var query = (from T0 in context.Set<OportunidadGanadora>()
                         join T1 in context.Set<IsisNroOferta>() on T0.Id equals T1.Id_OportunidadGanadora
                         join T2 in context.Set<SisegoCotizado>() on T1.Id_OportunidadGanadora equals T2.Id_OportunidadGanadora
                         where T0.IdEtapa == Negocio.EtapasRPA.CierreOferta
                         select new OportunidadGanadoraDtoResponse
                         {
                             IdEstado = T0.IdEstado,
                             IdUsuarioCreacion = T0.IdUsuarioCreacion, 
                             FechaCreacion = T0.FechaCreacion,
                             IdUsuarioEdicion = T0.IdUsuarioEdicion,
                             FechaEdicion = T0.FechaEdicion,
                             Id = T0.Id,
                             PER = T0.PER,
                             Nro_oferta = T1.Nro_oferta,
                             Nro_version_Oferta = T1.Nro_version_Oferta,
                             codigo_sisego = T2.codigo_sisego,
                             CasoDerivado = T0.CasoDerivado
                         });
            var resultado = new OportunidadGanadoraPaginadoDtoResponse
            {
                ListaOportunidadGanadoraPaginadoDtoResponse = query.ToList(),
            };
            return resultado;
        }

        public ClienteDtoResponse ObtenerClientePorRuc(ClienteDtoRequest request)
        {
            var query = (from T0 in context.Cliente
                            where T0.NumeroIdentificadorFiscal == request.NumeroIdentificadorFiscal
                            select new ClienteDtoResponse
                            {
                                IdCliente = T0.IdCliente,
                                NumeroIdentificadorFiscal = (T0.NumeroIdentificadorFiscal == null ? "" : T0.NumeroIdentificadorFiscal),
                                Descripcion = T0.NumeroIdentificadorFiscal,
                                FechaCreacion = T0.FechaCreacion,
                                IdUsuarioCreacion = T0.IdUsuarioCreacion,
                                IdSector = T0.IdSector,
                                IdDireccionComercial = T0.IdDireccionComercial
                            }).AsNoTracking().FirstOrDefault();
            return query;
        }

        public IsisNroOfertaDtoResponse ObtenerIsisNroOfertaPorNroPer_NroOferta(IsisNroOfertaDtoRequest request)
        {
            var query = (from T0 in context.OportunidadGanadora
                         join T1 in context.IsisNroOferta on T0.Id equals T1.Id_OportunidadGanadora
                         where T0.PER == request.Per && T1.Nro_oferta == request.Nro_oferta
                         select new IsisNroOfertaDtoResponse
                         {
                             Id = T1.Id,
                             Nro_oferta = T1.Nro_oferta,
                             Nro_version_Oferta = T1.Nro_version_Oferta,
                             Id_OportunidadGanadora = T1.Id_OportunidadGanadora,
                             FechaCreacion = T1.FechaCreacion,
                             IdUsuarioCreacion = T1.IdUsuarioCreacion
                         }).AsNoTracking().FirstOrDefault();
            return query;
        }

        public SisegoCotizadoDtoResponse ObtenerSisegoCotizadoPorIdOporNroSisego(SisegoCotizadoDtoRequest request)
        {
            var query = (from T0 in context.OportunidadGanadora
                             join T1 in context.SisegoCotizado on T0.Id equals T1.Id_OportunidadGanadora
                         where T0.PER == request.PER && T1.codigo_sisego == request.codigo_sisego
                         select new SisegoCotizadoDtoResponse
                         {
                            Id = T1.Id
                         }).AsNoTracking().FirstOrDefault();
            return query;
        }

        public ClienteDtoResponse ObtenerDetallesOportunidadGanadora(IsisNroOfertaDtoRequest request)
        {
            var objOportunidad = (from op in context.OportunidadGanadora 
                                  join isis in context.IsisNroOferta on op.Id equals isis.Id_OportunidadGanadora
                                  join sfc in context.SalesForceConsolidadoCabecera on op.PER equals sfc.IdOportunidad
                                  join cl in context.Cliente on sfc.IdCLiente equals cl.IdCliente
                                  where
                                  isis.Nro_oferta == request.Nro_oferta
                                  select new ClienteDtoResponse
                                  {
                                      //Oportunidad = op.PER,
                                      Descripcion= cl.Descripcion,
                                      NumeroIdentificadorFiscal = cl.NumeroIdentificadorFiscal,
                                      CodigoCliente = cl.CodigoCliente

                                  }).AsNoTracking().FirstOrDefault();

            return null;
        }

        public OportunidadGanadoraDtoResponse ObtenerOportunidadxPer(OportunidadGanadoraDtoRequest OportunidadGanadora)
        {
            var query = (from T0 in context.OportunidadGanadora
                         where T0.PER == OportunidadGanadora.PER
                         select new OportunidadGanadoraDtoResponse
                         {
                             Id = T0.Id,
                             PER = T0.PER,
                             Ruc = T0.Ruc
                         }).AsNoTracking().FirstOrDefault();
            return query;
        }

        public OportunidadGanadoraDtoResponse ObtenerOportunidadById(OportunidadGanadoraDtoRequest request)
        {
            var query = (from og in context.OportunidadGanadora
                         where og.Id == request.Id
                         select new OportunidadGanadoraDtoResponse
                         {

                             PER = og.PER

                         }).AsNoTracking().FirstOrDefault();
                         
                         
            return query;
        }

        public RPAEquiposServicioDtoResponse ObtenerRPAEquiposServicioPorIdOferta_IdRPASerNroOfer(RPAEquiposServicioDtoRequest request)
        {
            var objOportunidad = (from p in context.Set<RPAEquiposServicio>()
                                  where
                                  p.Id_Oferta == request.Id_Oferta && p.Id_RPAServOferta == request.Id_RPAServOferta
                                  select new RPAEquiposServicioDtoResponse
                                  {
                                      Id = p.Id,
                                      ValorAgregado = p.ValorAgregado,
                                      Equipos1 = p.Equipos1,
                                      Equipos2 = p.Equipos2
                                  }).AsNoTracking().FirstOrDefault();
            return objOportunidad;
        }

        public RPASisegoDetalleDtoResponse ObtenerRPASisegoDetallePorIdSisego(RPASisegoDetalleDtoRequest request)
        {
            var objOportunidad = (from p in context.Set<RPASisegoDetalle>()
                                  where
                                  p.Id_sisego == request.Id_sisego
                                  select new RPASisegoDetalleDtoResponse
                                  {
                                      Id = p.Id
                                  }).AsNoTracking().FirstOrDefault();
            return objOportunidad;
        }

        public RPASisegoDatosContactoDtoResponse ObtenerRPASisegoDatosContactoPorId_RPASisegoDetalle(RPASisegoDatosContactoDtoRequest oRPASisegorequest)
        {
            var objOportunidad = (from p in context.Set<RPASisegoDatosContacto>()
                                  where
                                  p.Id_RPASisegoDetalle == oRPASisegorequest.Id_RPASisegoDetalle && p.Contacto == oRPASisegorequest.Contacto
                                  select new RPASisegoDatosContactoDtoResponse
                                  {
                                      Id = p.Id
                                  }).AsNoTracking().FirstOrDefault();
            return objOportunidad;
        }

        public RPAServiciosxNroOfertaDtoResponse ObtenerRPAServiciosxNroOfertaPorId_Oferta(RPAServiciosxNroOfertaDtoRequest request)
        {
            var objOportunidad = (from p in context.Set<RPAServiciosxNroOferta>()
                                  where
                                  p.Id_Oferta == request.Id_Oferta
                                  select new RPAServiciosxNroOfertaDtoResponse
                                  {
                                      Id = p.Id
                                  }).AsNoTracking().FirstOrDefault();
            return objOportunidad;
        }

    }
}
