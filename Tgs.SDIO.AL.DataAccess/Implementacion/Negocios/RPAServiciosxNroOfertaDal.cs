﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Negocios
{
    public class RPAServiciosxNroOfertaDal : Repository<RPAServiciosxNroOferta>, IRPAServiciosxNroOfertaDal
    {
        readonly DioContext context;
        public RPAServiciosxNroOfertaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        #region ---- Transaccionales ----

        public ProcesoResponse AgregarServiciosNroOferta(RPAServiciosxNroOfertaDtoRequest request)
        {
            return null;
        }
        public IsisNroOfertaDtoResponse ObtenerNroOfertaById(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            var NroOferta = (from of in context.IsisNroOferta
                             where of.Id == resquest.IdOferta
                             select new IsisNroOfertaDtoResponse
                             {

                                 Nro_oferta = of.Nro_oferta

                             }).AsNoTracking().FirstOrDefault();
            return NroOferta;
        }
        public SisegoCotizadoDtoResponse ObtenerCodSisegoById(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            var CodSisego = (from sg in context.SisegoCotizado
                             where sg.Id == resquest.IdSisego
                             select new SisegoCotizadoDtoResponse
                             {

                                 codigo_sisego = sg.codigo_sisego

                             }).AsNoTracking().FirstOrDefault();
            return CodSisego;

        }

        #endregion

        #region ---- No Transaccional ----

        public List<RPAServiciosxNroOfertaDtoResponse> ListarServiciosNroOferta(RPAServiciosxNroOfertaDtoRequest request)
        {
            var query = (from so in context.RPAServiciosxNroOferta
                         join aso in context.AsociarServicioSisegos on so.Id equals aso.IdServicio
                         //join op in context.OportunidadGanadora on isis.Id_OportunidadGanadora equals op.Id
                         where aso.Id == request.Id
                         select new RPAServiciosxNroOfertaDtoResponse
                         {
                             Id = so.Id,
                             NombreServicio = so.NombreServicio,
                             Cantidad = so.Cantidad,
                             Velocidad = so.Velocidad,
                             Accion = so.Accion,
                             ModeloTx = so.MedioTx,
                             TipoCircuito = so.TipoCircuito,
                             NumeroCD = so.NumeroCD,
                             NumeroCDK =so.NumeroCDK,
                             /***** Cadudales de Conexion ******/
                             Caudal_Bronce = so.Caudal_Bronce,
                             Caudal_oro = so.Caudal_oro,
                             Caudal_plata = so.Caudal_plata,
                             Caudal_platino = so.Caudal_platino,
                             Caudal_VRF = so.Caudal_VRF,
                             Video = so.Video,
                             VA_Velocidad = so.VA_Velocidad,
                             Completa = Generales.Confirmacion.no

                         }).ToList();
            return query;
        }
        public RPAServiciosxNroOfertaPaginadoDtoResponse ListarServiciosNroOfertaPaginado(RPAServiciosxNroOfertaDtoRequest request)
        {
            var query = (from so in context.RPAServiciosxNroOferta
                         join isis in context.IsisNroOferta on so.Id_Oferta equals isis.Id
                         join op in context.OportunidadGanadora on isis.Id_OportunidadGanadora equals op.Id
                         where so.Id_Oferta == request.Id_Oferta
                         select new RPAServiciosxNroOfertaDtoResponse
                         {
                             Id = so.Id,
                             NombreServicio = so.NombreServicio,
                             Cantidad = so.Cantidad,
                             Velocidad = so.Velocidad,
                             Accion = so.Accion,
                             ModeloTx = so.MedioTx,
                             TipoCircuito = so.TipoCircuito,
                             //NumeroCD = so.NumeroCD,
                             //NumeroCDK = so.NumeroCDK,
                             /***** Cadudales de Conexion ******/
                             Caudal_Bronce = so.Caudal_Bronce,
                             Caudal_oro = so.Caudal_oro,
                             Caudal_plata = so.Caudal_plata,
                             Caudal_platino = so.Caudal_platino,
                             Caudal_VRF = so.Caudal_VRF,
                             Video = so.Video,
                             VA_Velocidad = so.VA_Velocidad,
                             Completa = Generales.Confirmacion.no

                         });
            var resultado = new RPAServiciosxNroOfertaPaginadoDtoResponse
            {
                ListaServiciosxNroOfertaDtoResponse = query.ToList()
            };

            return resultado;
        }

        #endregion

    }
}
