﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.Util.Mensajes.Negocio;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Negocios
{
    public class AsociarServicioSisegosDal : Repository<AsociarServicioSisegos>, IAsociarServicioSisegosDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public AsociarServicioSisegosDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
        public ProcesoResponse AsociarServicioSisegos(AsociarServicioSisegosDtoRequest request)
        {
            var IdServicio = new SqlParameter { ParameterName = "IdServicio", Value = request.IdServicio, SqlDbType = SqlDbType.Int };
            var IdSisego = new SqlParameter { ParameterName = "IdSisego", Value = request.IdSisego, SqlDbType = SqlDbType.Int };
            
            var IdUsuarioCreacion = new SqlParameter { ParameterName = "IdUsuarioCreacion", Value = request.IdUsuarioCreacion, SqlDbType = SqlDbType.Int };
            
            var response = context.ExecuteCommand("NEGOCIOS.USP_ASOCIAR_SERVICIOS_SISEGOS @IdServicio, @IdSisego, @IdUsuarioCreacion", IdServicio, IdSisego, IdUsuarioCreacion);

            respuesta.Id = request.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralNegocio.AsociarServicioSisegoSuccess;

            return respuesta;
        }
        public List<AsociarServicioSisegosDtoResponse> ListarServicioSisegos(AsociarServicioSisegosDtoRequest request)
        {
            var query = (from ass in context.AsociarServicioSisegos
                         select new AsociarServicioSisegosDtoResponse
                         {
                             Id = ass.Id,
                             IdServicio = ass.IdServicio,
                             Servicio = ass.Servicio,
                             IdEquipo = ass.IdEquipo,
                             Equipo = ass.Equipo,
                             IdSisego = ass.IdSisego,
                             Departamento = ass.Departamento,
                             Provincia = ass.Provincia,
                             Distrito = ass.Distrito,
                             Direccion = ass.Direccion
                         }).ToList();
            return query;
        }
        public AsociarServicioSisegosPaginadoDtoResponse ListarServicioSisegosPaginado(AsociarServicioSisegosDtoRequest request)
        {
            var query = (from ass in context.AsociarServicioSisegos
                         join ser in context.RPAServiciosxNroOferta on ass.IdServicio equals ser.Id
                         join sd in context.RPASisegoDetalle on ass.IdSisego equals sd.Id
                         join sc in context.SisegoCotizado on sd.Id_sisego equals sc.Id
                         where ser.Id_Oferta == request.IdOferta
                         select new AsociarServicioSisegosDtoResponse
                         {
                             Id = ass.Id,
                             //IdServicio = ass.IdServicio,
                             Servicio = ass.Servicio,
                             //IdEquipo = ass.IdEquipo,
                             Equipo = ass.Equipo,
                             //IdSisego = ass.IdSisego,
                             CodSisego = sc.codigo_sisego,
                             Departamento = sd.Departamento,
                             Provincia = sd.Provincia,
                             Distrito = sd.Distrito,
                             Direccion = sd.Direccion
                         });

            var resultado = new AsociarServicioSisegosPaginadoDtoResponse
            {
                ListaAsociarServicioSisegosDtoResponse = query.ToList()
            };

            return resultado;
        }
        public RPAServiciosxNroOfertaDtoResponse ObtenerServicioById(AsociarServicioSisegosDtoRequest request)
        {
            return null;
        }
        public RPAEquiposServicioDtoResponse ObtenerEquipoById(AsociarServicioSisegosDtoRequest request)
        {
            return null;
        }
        public RPASisegoDetalleDtoResponse ObtenerCodSisegoById(AsociarServicioSisegosDtoRequest request)
        {
            return null;
        }
    }
}
