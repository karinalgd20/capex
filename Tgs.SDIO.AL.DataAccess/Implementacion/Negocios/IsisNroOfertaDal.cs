﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Negocios
{
    public class IsisNroOfertaDal : Repository<IsisNroOferta>, IIsisNroOfertaDal
    {
        readonly DioContext context;
        public IsisNroOfertaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<IsisNroOfertaDtoResponse> ListarNroOferta(IsisNroOfertaDtoRequest request)
        {
            var query = (from isis in context.Set<IsisNroOferta>()
                         where isis.Id_OportunidadGanadora == request.Id_OportunidadGanadora && isis.IdEstado == Generales.Estados.Activo
                         select new IsisNroOfertaDtoResponse
                         {
                            Codigo = isis.Id.ToString(),
                            Descripcion = isis.Nro_oferta,
                             
                         }).ToList<IsisNroOfertaDtoResponse>();
            return query;
        }

        public IsisNroOfertaPaginadoDtoResponse ListarNroOfertaPaginado(IsisNroOfertaDtoRequest request)
        {
            var query = (from isis in context.IsisNroOferta
                         join op in context.OportunidadGanadora on isis.Id_OportunidadGanadora equals op.Id
                         where isis.Id_OportunidadGanadora == request.Id_OportunidadGanadora && isis.IdEstado == Generales.Estados.Activo
                         select new IsisNroOfertaDtoResponse
                         {
                             Id = isis.Id,
                             Id_OportunidadGanadora = isis.Id_OportunidadGanadora,
                             Nro_oferta = isis.Nro_oferta,
                             Nro_version_Oferta = isis.Nro_version_Oferta,
                             IdEstado = isis.IdEstado,
                             FechaCreacion = isis.FechaCreacion,
                             IdUsuarioCreacion = isis.IdUsuarioCreacion,
                             FechaEdicion = isis.FechaEdicion,
                             IdUsuarioEdicion = isis.IdUsuarioEdicion
                         });
            var resultado = new IsisNroOfertaPaginadoDtoResponse
            {
                ListaOfertasPaginadoDtoResponse = query.ToList(),
            };
            return resultado;
        }

    }
}
