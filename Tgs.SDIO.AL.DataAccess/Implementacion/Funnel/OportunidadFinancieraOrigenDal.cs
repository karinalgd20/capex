﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Funnel;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;
using Tgs.SDIO.Entities.Entities.Funnel;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Funnel
{
    public class OportunidadFinancieraOrigenDal : Repository<OportunidadFinancieraOrigen>, IOportunidadFinancieraOrigenDal
    {
        readonly DioContext context;

        public OportunidadFinancieraOrigenDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public IEnumerable<OportunidadFinancieraOrigenDtoResponse> ListarIndicadorMadurez(IndicadorMadurezDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var idLineaNegocio = new SqlParameter { ParameterName = "IdLineaNegocio", Value = request.IdLineaNegocio ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var idSector = new SqlParameter { ParameterName = "IdSector", Value = request.IdSector ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var etapa = new SqlParameter { ParameterName = "Etapa", Value = request.Etapa ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };


            var listaIndicadores =
                context.ExecuteQuery<OportunidadFinancieraOrigenDtoResponse>("[FUNNEL].[USP_INDICADORMADUREZCONSULTAR] @Anio,@Mes,@IdLineaNegocio,@IdSector,@ProbabilidadExito,@Etapa",
                anio, mes, idLineaNegocio, idSector, probabilidadExito, etapa).ToList().Where(x => x.Madurez != 0);

            return listaIndicadores;
        }

        public IEnumerable<DashoardOfertasLineaNegocioDtoResponse> ListarIndicadorOfertasLineaNegocio(DashoardDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var etapa = new SqlParameter { ParameterName = "Etapa", Value = request.Etapa ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var madurez = new SqlParameter { ParameterName = "Madurez", Value = request.Madurez ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };

            var listaOfertasLineaNegocio =
               context.ExecuteQuery<DashoardOfertasLineaNegocioDtoResponse>("[FUNNEL].[USP_INDICADOR_LINEANEGOCIO_CONSULTAR] @Anio,@Mes,@Etapa,@ProbabilidadExito,@Madurez",
               anio, mes, etapa, probabilidadExito, madurez).ToList();

            return listaOfertasLineaNegocio;

        }

        public IEnumerable<DashoardOfertasSectorDtoResponse> ListarIndicadorOfertasSector(DashoardDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var etapa = new SqlParameter { ParameterName = "Etapa", Value = request.Etapa ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var madurez = new SqlParameter { ParameterName = "Madurez", Value = request.Madurez ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };

            var listaOfertasSector =
             context.ExecuteQuery<DashoardOfertasSectorDtoResponse>("[FUNNEL].[USP_INDICADORSECTORCONSULTAR] @Anio,@Mes,@Etapa,@ProbabilidadExito,@Madurez",
             anio, mes, etapa, probabilidadExito, madurez).ToList();

            return listaOfertasSector;
        }

        public IEnumerable<DashoardIngresosLineaNegocioDtoResponse> ListarIndicadorIngresosLineaNegocio(DashoardDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var etapa = new SqlParameter { ParameterName = "Etapa", Value = request.Etapa ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var madurez = new SqlParameter { ParameterName = "Madurez", Value = request.Madurez ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };

            var listaIngresosLineaNegocio =
                 context.ExecuteQuery<DashoardIngresosLineaNegocioDtoResponse>("[FUNNEL].[USP_INDICADOR_INGRESOLINEA_CONSULTAR] @Anio,@Mes,@Etapa,@ProbabilidadExito,@Madurez",
             anio, mes, etapa, probabilidadExito, madurez).ToList();

            return listaIngresosLineaNegocio;
        }

        public IEnumerable<DashoardIngresosSectorDtoResponse> ListarIndicadorIngresosSector(DashoardDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var etapa = new SqlParameter { ParameterName = "Etapa", Value = request.Etapa ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var madurez = new SqlParameter { ParameterName = "Madurez", Value = request.Madurez ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };

            var listaIngresosSector =
                 context.ExecuteQuery<DashoardIngresosSectorDtoResponse>("[FUNNEL].[USP_INDICADOR_INGRESOSECTOR_CONSULTAR] @Anio,@Mes,@Etapa,@ProbabilidadExito,@Madurez",
             anio, mes, etapa, probabilidadExito, madurez).ToList();

            return listaIngresosSector;

        }

        public IEnumerable<DashoardOfertasEstadoDtoResponse> ListarIndicadorOfertasEstado(DashoardDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var idLineaNegocio = new SqlParameter { ParameterName = "IdLineaNegocio", Value = request.IdLineaNegocio ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var idSector = new SqlParameter { ParameterName = "IdSector", Value = request.IdSector ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var madurez = new SqlParameter { ParameterName = "Madurez", Value = request.Madurez ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };

            var listaOfertasEstado =
                context.ExecuteQuery<DashoardOfertasEstadoDtoResponse>("[FUNNEL].[USP_INDICADOR_OFERTASESTADO_CONSULTAR] @Anio,@Mes,@IdLineaNegocio,@IdSector,@ProbabilidadExito,@Madurez",
            anio, mes, idLineaNegocio, idSector, probabilidadExito, madurez).ToList();

            return listaOfertasEstado;

        }


        public IEnumerable<DashboardTotalesDtoResponse> ListarIndicadoresTotales(DashoardDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };

            var totalesDashboard =
                context.ExecuteQuery<DashboardTotalesDtoResponse>("[FUNNEL].[USP_CONSULTAR_OPORTUNIDADPRINCIPALPIE] @Anio,@Mes",
            anio, mes).ToList();

            return totalesDashboard;

        }

        public IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesPreventaGerente(IndicadorDashboardPreventaDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var idJefe = new SqlParameter { ParameterName = "IdJefe", Value = request.IdJefe, SqlDbType = SqlDbType.Int };
            var indice = new SqlParameter { ParameterName = "Indice", Value = request.Paginacion.Indice, SqlDbType = SqlDbType.Int };
            var tamanio = new SqlParameter { ParameterName = "Tamanio", Value = request.Paginacion.Tamanio, SqlDbType = SqlDbType.Int };


            var listaOportunidades =
                context.ExecuteQuery<IndicadorDashboardPreventaDtoResponse>("FUNNEL.USP_INDICADOR_GERENTE_PREVENTA_DETALLES @Anio,@IdJefe,@Indice,@Tamanio",
            anio, idJefe, indice, tamanio).ToList();

            return listaOportunidades; 

        }

        public IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesPreventaGerenteLider(IndicadorDashboardPreventaDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var idJefe = new SqlParameter { ParameterName = "IdJefe", Value = request.IdJefe, SqlDbType = SqlDbType.Int };
            var indice = new SqlParameter { ParameterName = "Indice", Value = request.Paginacion.Indice, SqlDbType = SqlDbType.Int };
            var tamanio = new SqlParameter { ParameterName = "Tamanio", Value = request.Paginacion.Tamanio, SqlDbType = SqlDbType.Int };


            var listaOportunidades =
                context.ExecuteQuery<IndicadorDashboardPreventaDtoResponse>("FUNNEL.USP_INDICADOR_GERENTE_PREVENTA_DETALLES_LIDER @Anio,@IdJefe,@Indice,@Tamanio",
            anio, idJefe, indice, tamanio).ToList();

            return listaOportunidades;

        }

        public IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesPreventaLider(IndicadorDashboardPreventaDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var idJefe = new SqlParameter { ParameterName = "IdJefe", Value = request.IdJefe, SqlDbType = SqlDbType.Int };
            var indice = new SqlParameter { ParameterName = "Indice", Value = request.Paginacion.Indice, SqlDbType = SqlDbType.Int };
            var tamanio = new SqlParameter { ParameterName = "Tamanio", Value = request.Paginacion.Tamanio, SqlDbType = SqlDbType.Int };


            var listaOportunidades =
                context.ExecuteQuery<IndicadorDashboardPreventaDtoResponse>("FUNNEL.USP_INDICADOR_LIDER_PREVENTA_DETALLES @Anio,@IdJefe,@Indice,@Tamanio",
            anio, idJefe, indice, tamanio).ToList();

            return listaOportunidades;

        }

        public IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesPreventaPreventa(IndicadorDashboardPreventaDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var idPreventa = new SqlParameter { ParameterName = "IdPreventa", Value = request.IdJefe, SqlDbType = SqlDbType.Int };
            var indice = new SqlParameter { ParameterName = "Indice", Value = request.Paginacion.Indice, SqlDbType = SqlDbType.Int };
            var tamanio = new SqlParameter { ParameterName = "Tamanio", Value = request.Paginacion.Tamanio, SqlDbType = SqlDbType.Int };


            var listaOportunidades =
                context.ExecuteQuery<IndicadorDashboardPreventaDtoResponse>("FUNNEL.USP_INDICADOR_PREVENTA_DETALLES @Anio,@IdPreventa,@Indice,@Tamanio",
            anio, idPreventa, indice, tamanio).ToList();

            return listaOportunidades;

        }

        public IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesSectorOportunidad(IndicadorDashboardSectorDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var indice = new SqlParameter { ParameterName = "Indice", Value = request.Paginacion.Indice, SqlDbType = SqlDbType.Int };
            var tamanio = new SqlParameter { ParameterName = "Tamanio", Value = request.Paginacion.Tamanio, SqlDbType = SqlDbType.Int };


            var listaOportunidades =
                context.ExecuteQuery<IndicadorDashboardPreventaDtoResponse>("FUNNEL.USP_INDICADOR_PREVENTA_SECTOR_DETALLES_CABECERA @Anio,@Indice,@Tamanio",
            anio, indice, tamanio).ToList();

            return listaOportunidades;

        }

        public IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesSectorLideres(IndicadorDashboardSectorDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var idJefe = new SqlParameter { ParameterName = "IdJefe", Value = request.IdJefe, SqlDbType = SqlDbType.Int };
            var idSector = new SqlParameter { ParameterName = "IdSector", Value = request.IdSector, SqlDbType = SqlDbType.Int };


            var listaOportunidades =
                context.ExecuteQuery<IndicadorDashboardPreventaDtoResponse>("FUNNEL.USP_INDICADOR_PREVENTA_SECTOR_DETALLES_CABECERA_LIDERES @Anio,@IdJefe,@IdSector",
            anio, idJefe, idSector).ToList();

            return listaOportunidades;

        }


        public IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesSectorPreventas(IndicadorDashboardSectorDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var idJefe = new SqlParameter { ParameterName = "IdJefe", Value = request.IdJefe, SqlDbType = SqlDbType.Int };

            var listaOportunidades =
                context.ExecuteQuery<IndicadorDashboardPreventaDtoResponse>("FUNNEL.USP_INDICADOR_PREVENTA_SECTOR_DETALLES_CABECERA_PREVENTA @Anio,@IdJefe",
            anio, idJefe).ToList();

            return listaOportunidades;

        }

        public IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesSectorClientes(IndicadorDashboardSectorDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var idJefe = new SqlParameter { ParameterName = "IdJefe", Value = request.IdJefe, SqlDbType = SqlDbType.Int };

            var listaOportunidades =
                context.ExecuteQuery<IndicadorDashboardPreventaDtoResponse>("FUNNEL.USP_INDICADOR_PREVENTA_SECTOR_DETALLES_CABECERA_CLIENTE @Anio,@IdJefe",
            anio, idJefe).ToList();

            return listaOportunidades;

        }

        public IEnumerable<IndicadorDashboardPreventaDtoResponse> ListarSeguimientoOportunidadesSectorOportunidades(IndicadorDashboardSectorDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var idCliente = new SqlParameter { ParameterName = "IdCliente", Value = request.IdJefe, SqlDbType = SqlDbType.Int };

            var listaOportunidades =
                context.ExecuteQuery<IndicadorDashboardPreventaDtoResponse>("FUNNEL.USP_INDICADOR_PREVENTA_SECTOR_DETALLES_CABECERA_OPORT @Anio,@IdCliente",
            anio, idCliente).ToList();

            return listaOportunidades;

        }

        #region Rentabilidad

        public IEnumerable<IndicadorRentabilidadDtoResponse> ListarRentabilidadAnalistasFinancieros(IndicadorRentabilidadDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var idGerente = new SqlParameter { ParameterName = "IdGerente", Value = request.IdGerente, SqlDbType = SqlDbType.Int };
            var indice = new SqlParameter { ParameterName = "Indice", Value = request.Indice, SqlDbType = SqlDbType.Int };
            var tamanio = new SqlParameter { ParameterName = "Tamanio", Value = request.Tamanio, SqlDbType = SqlDbType.Int };


            var listaAnalistas =
                context.ExecuteQuery<IndicadorRentabilidadDtoResponse>("FUNNEL.USP_INDICADOR_RENTABILIDAD_ANALISTA @Anio,@IdGerente,@Indice,@Tamanio",
            anio, idGerente, indice, tamanio).ToList();

            return listaAnalistas;

        }

        public IEnumerable<IndicadorRentabilidadDtoResponse> ListarRentabilidadClientes(IndicadorRentabilidadDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var idAnalistaFinanciero = new SqlParameter { ParameterName = "IdAnalistaFinanciero", Value = request.IdAnalistaFinanciero, SqlDbType = SqlDbType.Int };


            var listaClientes =
                context.ExecuteQuery<IndicadorRentabilidadDtoResponse>("FUNNEL.USP_INDICADOR_RENTABILIDAD_CLIENTE @Anio,@IdAnalistaFinanciero",
            anio, idAnalistaFinanciero).ToList();

            return listaClientes;

        }
        public IEnumerable<IndicadorRentabilidadDtoResponse> ListarRentabilidadCasos(IndicadorRentabilidadDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var idOportunidad = new SqlParameter { ParameterName = "IdOportunidad", Value = request.IdOportunidad, SqlDbType = SqlDbType.VarChar };


            var listaCasos =
                context.ExecuteQuery<IndicadorRentabilidadDtoResponse>("FUNNEL.USP_INDICADOR_RENTABILIDAD_CASO @Anio,@IdOportunidad",
            anio, idOportunidad).ToList();

            return listaCasos;

        }

        #endregion


    }
}

