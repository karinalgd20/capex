﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Funnel;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;
using Tgs.SDIO.Entities.Entities.Funnel;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Funnel
{
    public class OportunidadFinancieraOrigenLineaNegocioDal : Repository<OportunidadFinancieraOrigen>, IOportunidadFinancieraOrigenLineaNegocioDal
    {
        readonly DioContext context;

        public OportunidadFinancieraOrigenLineaNegocioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public IEnumerable<ListarLineasNegocioDtoResponse> ListarLineasNegocio(IndicadorLineasNegocioDtoRequest filtro)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = filtro.Anio, SqlDbType = SqlDbType.Int };
            var indice = new SqlParameter { ParameterName = "Indice", Value = filtro.Indice, SqlDbType = SqlDbType.Int };
            var tamanio = new SqlParameter { ParameterName = "Tamanio", Value = filtro.Tamanio, SqlDbType = SqlDbType.Int };

            var listaIndicadores =
                context.ExecuteQuery<ListarLineasNegocioDtoResponse>("[FUNNEL].[USP_INDICADOR_PLANEAMIENTO_LNEGOCIO_CONTROL] @Anio,@Indice,@Tamanio",
                anio, indice, tamanio).ToList();

            return listaIndicadores;

        }

        public IEnumerable<ListarLineasNegocioClienteDtoResponse> ListarClientesLineasNegocio(IndicadorLineasNegocioDtoRequest filtro)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = filtro.Anio, SqlDbType = SqlDbType.Int };
            var idLineaNegocio = new SqlParameter { ParameterName = "IdLineaNegocio", Value = filtro.IdLineaNegocio, SqlDbType = SqlDbType.Int };

            var listaIndicadores =
                context.ExecuteQuery<ListarLineasNegocioClienteDtoResponse>("[FUNNEL].[USP_INDICADOR_PLANEAMIENTO_CONTROL_CLIENTE] @Anio,@IdLineaNegocio",
                anio, idLineaNegocio).ToList();

            return listaIndicadores;

        }
    }
}

