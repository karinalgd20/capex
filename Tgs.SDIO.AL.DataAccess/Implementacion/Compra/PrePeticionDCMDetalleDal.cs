﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using System.Collections.Generic;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class PrePeticionDCMDetalleDal : Repository<Entities.Entities.Compra.PrePeticionDCMDetalle>, IPrePeticionDCMDetalleDal
    {
        ProcesoResponse respuesta = new ProcesoResponse();

        readonly DioContext context;
        
        public PrePeticionDCMDetalleDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public PrePeticionDCMDetalleDtoResponse ObtenerDetalleDCM(PrePeticionDetalleDCMDetalleDtoRequest request)
        {
            PrePeticionDCMDetalleDtoResponse oPrePeticionDCMDetalleDtoResponse = context.Set<PrePeticionDCMDetalle>().
                                                                    Join(context.Set<ContratoMarco>(),
                                                                    PrePeticionDCMDetalle => PrePeticionDCMDetalle.IdContratoMarco,
                                                                    ContratoMarco => ContratoMarco.IdContratoMarco,
                                                                    (PrePeticionDCMDetalle, ContratoMarco) => new { PrePeticionDCMDetalle, ContratoMarco }).
                                                                     Join(context.Set<Proveedor>(),
                                                                    ContratoMarco => ContratoMarco.ContratoMarco.IdProveedor,
                                                                    Proveedor => Proveedor.IdProveedor,
                                                                    (PrePeticionDCMDetalle, Proveedor) => new { PrePeticionDCMDetalle, Proveedor }).ToList()
                                                                   .Where(dp => dp.PrePeticionDCMDetalle.PrePeticionDCMDetalle.Id == request.Id).Select(r => new PrePeticionDCMDetalleDtoResponse
                                                                   {
                                                                       Id = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.Id,
                                                                       IdCabecera = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.IdCabecera,
                                                                       TipoCosto = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.TipoCosto,
                                                                       ProductManager = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.ProductManager,
                                                                       Descripcion = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.Descripcion,
                                                                       IdProveedor = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.IdProveedor,
                                                                       IdContratoMarco = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.IdContratoMarco,
                                                                       AutorizadoPor = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.AutorizadoPor,
                                                                       TipoEntrega = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.TipoEntrega,
                                                                       PlazoEntrega = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.PlazoEntrega,
                                                                       PQAdjudicado = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.PQAdjudicado,
                                                                       CotizacionAdjunta = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.CotizacionAdjunta,
                                                                       FechaEntregaOC = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.FechaEntregaOC,
                                                                       Observaciones = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.Observaciones,
                                                                       Direccion = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.Direccion,
                                                                       PosicionOC = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.PosicionOC,
                                                                       MonedaCompra = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.MonedaCompra,
                                                                       Monto = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.Monto,
                                                                       IdLineaProducto = r.PrePeticionDCMDetalle.PrePeticionDCMDetalle.IdLineaProducto,

                                                                       ProveedorCM = r.Proveedor.Descripcion,
                                                                       FechaFinCM = r.PrePeticionDCMDetalle.ContratoMarco.FechaFin,
                                                                       CatalogadoCM = r.PrePeticionDCMDetalle.ContratoMarco.Catalogado,
                                                                       MonedaCM = r.PrePeticionDCMDetalle.ContratoMarco.Moneda,
                                                                       ImporteCM = r.PrePeticionDCMDetalle.ContratoMarco.ImporteContrato,
                                                                       NumContrato = r.PrePeticionDCMDetalle.ContratoMarco.NumContrato,
                                                                   }).FirstOrDefault();

            if (oPrePeticionDCMDetalleDtoResponse == null) oPrePeticionDCMDetalleDtoResponse = new PrePeticionDCMDetalleDtoResponse();

            return oPrePeticionDCMDetalleDtoResponse;
        }

        public List<PrePeticionDCMDetalleDtoResponse> ListarPrePeticionDCM(PrePeticionDetalleDCMDetalleDtoRequest request)
        {
            SqlParameter pIdCabecera = new SqlParameter
            {
                ParameterName = "IdCabecera",
                Value = request.IdCabecera,
                SqlDbType = SqlDbType.VarChar,
                Size = 20
            };

            SqlParameter Indice = new SqlParameter
            {
                ParameterName = "Indice",
                Value = request.Indice,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter Tamanio = new SqlParameter
            {
                ParameterName = "Tamanio",
                Value = request.Tamanio,
                SqlDbType = SqlDbType.Int
            };

            List<PrePeticionDCMDetalleDtoResponse> resultado = context.ExecuteQuery<PrePeticionDCMDetalleDtoResponse>("[COMPRAS].[USP_LISTAR_PrePeticionDCMDetalle] @IdCabecera, @Indice, @Tamanio",
                                       pIdCabecera, Indice, Tamanio).ToList<PrePeticionDCMDetalleDtoResponse>();
            return resultado;
        }
    }
}