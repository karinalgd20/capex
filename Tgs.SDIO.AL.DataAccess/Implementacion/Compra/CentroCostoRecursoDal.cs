﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.Entities.Entities.Compra;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class CentroCostoRecursoDal : Repository<RecursosCentroCostos>, ICentroCostoRecursoDal
    {
        readonly DioContext context;
        public CentroCostoRecursoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<CentroCostoRecursoDtoResponse> ListarCentroCostoRecurso(CentroCostoRecursoDtoRequest request)
        {
            var IdAreaSolicitante = new SqlParameter
            {
                ParameterName = "IdAreaSolicitante",
                Value = request.IdAreaSolicitante, SqlDbType = SqlDbType.Int
            };

            var IdEstado = new SqlParameter
            {
                ParameterName = "IdEstado",
                Value = request.IdEstado, SqlDbType = SqlDbType.Int
            };

            var Indice = new SqlParameter
            {
                ParameterName = "Indice",
                Value = request.Indice, SqlDbType = SqlDbType.Int
            };

            var Tamanio = new SqlParameter
            {
                ParameterName = "Tamanio",
                Value = request.Tamanio, SqlDbType = SqlDbType.Int
            };

            var listado = context.ExecuteQuery<CentroCostoRecursoDtoResponse>("[COMPRAS].[USP_LISTA_CENTROS_COSTOS] @IdAreaSolicitante,@IdEstado,@Indice,@Tamanio",
                    IdAreaSolicitante, IdEstado, Indice, Tamanio).ToList<CentroCostoRecursoDtoResponse>();

            return listado;
        }
    }
}
