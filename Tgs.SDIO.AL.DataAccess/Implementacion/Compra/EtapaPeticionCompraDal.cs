﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class EtapaPeticionCompraDal : Repository<EtapaPeticionCompras>, IEtapaPeticionCompraDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public EtapaPeticionCompraDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public ProcesoResponse RegistrarEtapaPeticionCompra(EtapaPeticionCompraDtoRequest request)
        {
            try
            {
                SqlParameter pIdPeticionCompra = new SqlParameter
                {
                    ParameterName = "IdPeticionCompra",
                    Value = request.IdPeticionCompra,
                    SqlDbType = SqlDbType.Int
                };

                SqlParameter pIdEstadoEtapa = new SqlParameter
                {
                    ParameterName = "IdEstadoEtapa",
                    Value = request.IdEstadoEtapa,
                    SqlDbType = SqlDbType.Int
                };

                SqlParameter pComentario = new SqlParameter
                {
                    ParameterName = "Comentario",
                    Value = request.Comentario,
                    SqlDbType = SqlDbType.VarChar
                };

                SqlParameter pIdOrden = new SqlParameter
                {
                    ParameterName = "IdOrden",
                    Value = request.IdOrden,
                    SqlDbType = SqlDbType.Int
                };

                SqlParameter pIdOrdenObservar = new SqlParameter
                {
                    ParameterName = "IdOrdenObservar",
                    Value = request.IdOrdenObservar,
                    SqlDbType = SqlDbType.Int
                };

                SqlParameter pIdUsuario = new SqlParameter
                {
                    ParameterName = "IdUsuario",
                    Value = request.IdUsuario,
                    SqlDbType = SqlDbType.Int
                };

                SqlParameter pSiguienteEtapa = new SqlParameter
                {
                    ParameterName = "SiguienteEtapa",
                    Value = request.IrSiguienteEtapa,
                    SqlDbType = SqlDbType.Int
                };

                SqlParameter pIdEtapaPeticionCompraP = new SqlParameter
                {
                    ParameterName = "IdEtapaPeticionCompraP",
                    Value = request.IdEtapaPeticionCompra,
                    SqlDbType = SqlDbType.Int
                };

                context.ExecuteCommand("[COMPRAS].[USP_INSERTAR_ETAPAPETICIONCOMPRA] @IdPeticionCompra, @IdEstadoEtapa, @Comentario, @IdOrden, @IdOrdenObservar, @IdUsuario, @SiguienteEtapa, @IdEtapaPeticionCompraP",
                                        pIdPeticionCompra, pIdEstadoEtapa, pComentario, pIdOrden, pIdOrdenObservar, pIdUsuario, pSiguienteEtapa, pIdEtapaPeticionCompraP);

                respuesta.Id = 1;
                respuesta.Mensaje = MensajesGeneralCompra.RegistrarPeticionCompra;
                respuesta.TipoRespuesta = (respuesta.Id > 0) ? Proceso.Valido : Proceso.Invalido;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }

        public EtapaPeticionCompraDtoResponse ObtenerEtapaPeticionCompraActual(EtapaPeticionCompraDtoRequest request)
        {
            EtapaPeticionCompraDtoResponse oRespuesta = new EtapaPeticionCompraDtoResponse();

            SqlParameter pIdUsuarioActual = new SqlParameter
            {
                ParameterName = "IdUsuarioActual",
                Value = request.IdUsuario,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pIdPeticionCompra = new SqlParameter
            {
                ParameterName = "IdPeticionCompra",
                Value = request.IdPeticionCompra,
                SqlDbType = SqlDbType.Int
            };

            List<EtapaPeticionCompraDtoResponse> oListaRespuesta = context.ExecuteQuery<EtapaPeticionCompraDtoResponse>("[COMPRAS].[USP_OBTENER_ETAPAACTUAL] @IdUsuarioActual, @IdPeticionCompra",
                                    pIdUsuarioActual, pIdPeticionCompra).ToList<EtapaPeticionCompraDtoResponse>();

            if (oListaRespuesta.Count > 0)
            {
                oRespuesta = oListaRespuesta[0];
            }

            return oRespuesta;
        }

        public List<EtapaPeticionCompraDtoResponse> ListarEtapaPeticionCompra(EtapaPeticionCompraDtoRequest request)
        {
            var pIdPeticionCompra = new SqlParameter
            {
                ParameterName = "IdPeticionCompra",
                Value = request.IdPeticionCompra,
                SqlDbType = SqlDbType.VarChar,
                Size = 250
            };

            var pIndice = new SqlParameter
            {
                ParameterName = "Indice",
                Value = request.Indice,
                SqlDbType = SqlDbType.Int
            };

            var pTamanio = new SqlParameter
            {
                ParameterName = "Tamanio",
                Value = request.Tamanio,
                SqlDbType = SqlDbType.Int
            };

            var listado = context.ExecuteQuery<EtapaPeticionCompraDtoResponse>("[COMPRAS].[USP_LISTAR_ETAPASPETICIONCOMPRA] @IdPeticionCompra,@Indice,@Tamanio",
                    pIdPeticionCompra, pIndice, pTamanio).ToList<EtapaPeticionCompraDtoResponse>();

            return listado;
        }

    }
}
