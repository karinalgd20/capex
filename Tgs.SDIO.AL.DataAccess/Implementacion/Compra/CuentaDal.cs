﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.Entities.Entities.Compra;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class CuentaDal : Repository<Cuentas>, ICuentaDal
    {
        readonly DioContext context;
        public CuentaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<CuentaDtoResponse> ListarCuentas(CuentaDtoRequest request)
        {
            var pCodigo = new SqlParameter
            {
                ParameterName = "Codigo",
                Value = string.IsNullOrEmpty(request.Cuenta) ? "" : request.Cuenta,
                SqlDbType = SqlDbType.VarChar,
                Size = 250
            };

            var pDescripcion = new SqlParameter
            {
                ParameterName = "Descripcion",
                Value = string.IsNullOrEmpty(request.Descripcion) ? "" : request.Descripcion,
                SqlDbType = SqlDbType.VarChar, Size = 250
            };

            var pIndice = new SqlParameter
            {
                ParameterName = "Indice",
                Value = request.Indice,
                SqlDbType = SqlDbType.Int
            };

            var pTamanio = new SqlParameter
            {
                ParameterName = "Tamanio",
                Value = request.Tamanio,
                SqlDbType = SqlDbType.Int
            };

            var listado = context.ExecuteQuery<CuentaDtoResponse>("[COMPRAS].[USP_LISTAR_CUENTAS] @Codigo,@Descripcion,@Indice,@Tamanio",
                    pCodigo, pDescripcion, pIndice, pTamanio).ToList<CuentaDtoResponse>();

            return listado;
        }

    }
}
