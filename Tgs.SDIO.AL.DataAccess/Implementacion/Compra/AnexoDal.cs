﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class AnexoDal : Repository<Entities.Entities.Compra.AnexosPeticionCompras>, IAnexoDal
    {
        ProcesoResponse respuesta = new ProcesoResponse();

        readonly DioContext context;
        
        public AnexoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<AnexoDtoResponse> ListarAnexos(AnexoDtoRequest request)
        {
            SqlParameter pIdPeticionCompra = new SqlParameter
            {
                ParameterName = "IdPeticionCompra",
                Value = request.IdPeticionCompra,
                SqlDbType = SqlDbType.VarChar,
                Size = 20
            };

            List<AnexoDtoResponse> resultado = context.ExecuteQuery<AnexoDtoResponse>("COMPRAS.USP_LISTAR_COMENTARIOANEXOS @IdPeticionCompra",
                                       pIdPeticionCompra).ToList<AnexoDtoResponse>();

            return resultado;
        }
    }
}