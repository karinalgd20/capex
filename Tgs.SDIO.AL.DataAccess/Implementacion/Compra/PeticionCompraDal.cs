﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class PeticionCompraDal : Repository<PeticionCompras>, IPeticionCompraDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public PeticionCompraDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<PeticionCompraDtoResponse> ListarPeticionCompraCabecera(PeticionCompraDtoRequest request)
        {
            SqlParameter pCodigoCompra = new SqlParameter
            {
                ParameterName = "CodigoCompra",
                Value = (request.CodigoPeticionCompra == null ? string.Empty : request.CodigoPeticionCompra),
                SqlDbType = SqlDbType.VarChar,
                Size = 20
            };

            SqlParameter pIdAsociadoAProyecto = new SqlParameter
            {
                ParameterName = "AsociadoAProyecto",
                Value = (request.AsociadoAProyecto == null ? -1 : (request.AsociadoAProyecto.Value ? 1 : 0)),
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pIdTipoCompra = new SqlParameter
            {
                ParameterName = "IdTipoCompra",
                Value = request.IdTipoCompra,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pIdTipoCosto = new SqlParameter
            {
                ParameterName = "IdTipoCosto",
                Value = request.IdTipoCosto,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pFechaInicio = new SqlParameter
            {
                ParameterName = "FechaIni",
                Value = (request.FechaInicio == null ? string.Empty : request.FechaInicio),
                SqlDbType = SqlDbType.VarChar,
                Size = 10
            };

            SqlParameter pFechaFin = new SqlParameter
            {
                ParameterName = "FechaFin",
                Value = (request.FechaFin == null ? string.Empty : request.FechaFin),
                SqlDbType = SqlDbType.VarChar,
                Size = 10
            };

            SqlParameter pIdUsuario = new SqlParameter
            {
                ParameterName = "IdUsuario",
                Value = request.IdUsuarioCreacion,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter Indice = new SqlParameter
            {
                ParameterName = "Indice",
                Value = request.Indice,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter Tamanio = new SqlParameter
            {
                ParameterName = "Tamanio",
                Value = request.Tamanio,
                SqlDbType = SqlDbType.Int
            };

            List<PeticionCompraDtoResponse> resultado = context.ExecuteQuery<PeticionCompraDtoResponse>("[COMPRAS].[USP_LISTA_PETICION_COMPRA_CAB] @CodigoCompra, @AsociadoAProyecto, @IdTipoCompra, @IdTipoCosto, @FechaIni, @FechaFin, @IdUsuario, @Indice, @Tamanio",
                                       pCodigoCompra, pIdAsociadoAProyecto, pIdTipoCompra, pIdTipoCosto, pFechaInicio, pFechaFin, pIdUsuario, Indice, Tamanio).ToList<PeticionCompraDtoResponse>();
            return resultado;
        }

        public PeticionCompraDtoResponse ObtenerPeticionCompraPorID(PeticionCompraDtoRequest request)
        {
            SqlParameter IdPeticionCompra = new SqlParameter
            {
                ParameterName = "IdPeticionCompra", Value = request.IdPeticionCompra,
                SqlDbType = SqlDbType.Int
            };

            PeticionCompraDtoResponse resultado = context.ExecuteQuery<PeticionCompraDtoResponse>
                                    ("[COMPRAS].[USP_OBTENER_PETICION_COMPRA] @IdPeticionCompra", IdPeticionCompra)
                                    .FirstOrDefault<PeticionCompraDtoResponse>();
            return resultado;
        }

        public ProcesoResponse RegistrarPeticionCompra(PeticionCompraDtoRequest request)
        {
            try
            {
                var pDescripcion = new SqlParameter
                {
                    ParameterName = "Descripcion",
                    Value = request.Descripcion, SqlDbType = SqlDbType.VarChar, Size = 100
                };

                var pAsociadoAProyecto = new SqlParameter
                {
                    ParameterName = "AsociadoAProyecto",
                    Value = request.AsociadoAProyecto,
                    SqlDbType = SqlDbType.Bit
                };

                var pIdTipoCompra = new SqlParameter
                {
                    ParameterName = "IdTipoCompra",
                    Value = request.IdTipoCompra,
                    SqlDbType = SqlDbType.Int
                };

                var pIdTipoCosto = new SqlParameter
                {
                    ParameterName = "IdTipoCosto",
                    Value = request.IdTipoCosto,   
                    SqlDbType = SqlDbType.Int
                };

                var pIdLineaNegocio = new SqlParameter
                {
                    ParameterName = "IdLineaNegocio",
                    Value = request.IdLineaNegocio,
                    SqlDbType = SqlDbType.Int
                };

                var pIdComprador = new SqlParameter
                {
                    ParameterName = "IdComprador",
                    Value = request.IdComprador,
                    SqlDbType = SqlDbType.Int
                };

                var pAdjuntaCotizacion = new SqlParameter
                {
                    ParameterName = "AdjuntaCotizacion",
                    Value = request.AdjuntaCotizacion.HasValue ? request.AdjuntaCotizacion : (object)DBNull.Value,
                    SqlDbType = SqlDbType.Bit,
                    IsNullable = true
                };

                var pMonto = new SqlParameter
                {
                    ParameterName = "MontoCotizacion",
                    Value = request.MontoCotizacion,
                    SqlDbType = SqlDbType.Decimal
                };

                var pSubGrupo = new SqlParameter
                {
                    ParameterName = "SubGrupoCompras",
                    Value = !string.IsNullOrEmpty(request.SubGrupoCompras) ? request.SubGrupoCompras : (object)DBNull.Value,
                    SqlDbType = SqlDbType.VarChar, Size = 100
                };

                var pIdAreaSolicitante = new SqlParameter
                {
                    ParameterName = "IdAreaSolicitante",
                    Value = request.IdAreaSolicitante,
                    SqlDbType = SqlDbType.Int
                };

                var pIdCentroCosto = new SqlParameter
                {
                    ParameterName = "IdCentroCosto",
                    Value = request.IdCentroCosto.HasValue ? request.IdCentroCosto : (object)DBNull.Value,
                    SqlDbType = SqlDbType.Int
                };

                var pIdResponsablePostventa = new SqlParameter
                {
                    ParameterName = "IdResponsablePostventa",
                    Value = request.IdResponsablePostventa,
                    SqlDbType = SqlDbType.Int
                };

                var pElementoPEP = new SqlParameter
                {
                    ParameterName = "ElementoPEP",
                    Value = !string.IsNullOrEmpty(request.ElementoPEP) ? request.ElementoPEP : (object)DBNull.Value,
                    SqlDbType = SqlDbType.VarChar, Size = 100, IsNullable = true
                };

                var pGrafo = new SqlParameter
                {
                    ParameterName = "Grafo",
                    Value = !string.IsNullOrEmpty(request.Grafo) ? request.Grafo : (object)DBNull.Value,
                    SqlDbType = SqlDbType.VarChar, Size = 100,
                    IsNullable = true
                };

                var pCuenta = new SqlParameter
                {
                    ParameterName = "Cuenta",
                    Value = !string.IsNullOrEmpty(request.Cuenta) ? request.Cuenta : (object)DBNull.Value,
                    SqlDbType = SqlDbType.VarChar, Size = 100,
                    IsNullable = true
                };

                var pDescripcionCuenta = new SqlParameter
                {
                    ParameterName = "DescripcionCuenta",
                    Value = !string.IsNullOrEmpty(request.DescripcionCuenta) ? request.DescripcionCuenta : (object)DBNull.Value,
                    SqlDbType = SqlDbType.VarChar, Size = 100,
                    IsNullable = true
                };

                var pAreaFuncional = new SqlParameter
                {
                    ParameterName = "AreaFuncional",
                    Value = !string.IsNullOrEmpty(request.AreaFuncional) ? request.AreaFuncional : (object)DBNull.Value,
                    SqlDbType = SqlDbType.VarChar, Size = 100,
                    IsNullable = true
                };

                var pPeticionPenalidad = new SqlParameter
                {
                    ParameterName = "PeticionPenalidad",
                    Value = request.PeticionPenalidad.HasValue ? request.PeticionPenalidad : (object)DBNull.Value,
                    SqlDbType = SqlDbType.Bit
                };

                var pArrendamientoRenting = new SqlParameter
                {
                    ParameterName = "ArrendamientoRenting",
                    Value = request.ArrendamientoRenting.HasValue ? request.ArrendamientoRenting : (object)DBNull.Value,
                    SqlDbType = SqlDbType.Bit
                };

                var pUsuarioCreacion = new SqlParameter
                {
                    ParameterName = "IdUsuarioCreacion",
                    Value = request.IdUsuarioCreacion,
                    SqlDbType = SqlDbType.Int
                };

                var pIdContratoMarco = new SqlParameter
                {
                    ParameterName = "IdContratoMarco",
                    Value = request.IdContratoMarco.HasValue ? request.IdContratoMarco : (object)DBNull.Value,
                    SqlDbType = SqlDbType.Int
                };

                var pIdPrePeticionDetalle = new SqlParameter
                {
                    ParameterName = "IdPrePeticionDetalle",
                    Value = request.IdPrePeticionDetalle.HasValue ? request.IdPrePeticionDetalle : (object)DBNull.Value,
                    SqlDbType = SqlDbType.Int
                };


                var pId = new SqlParameter
                {
                    ParameterName = "Id",
                    SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output
                };

                context.ExecuteCommand("[COMPRAS].[USP_INSERTAR_PETICION_COMPRA] @Descripcion, @AsociadoAProyecto, @IdTipoCompra, " +
                                       " @IdTipoCosto, @IdLineaNegocio, @IdComprador, @AdjuntaCotizacion, @MontoCotizacion, " +
                                       " @SubGrupoCompras, @IdAreaSolicitante, @IdCentroCosto, @IdResponsablePostventa, " +
                                       " @ElementoPEP, @Grafo, @Cuenta, @DescripcionCuenta, @AreaFuncional, @PeticionPenalidad, " +
                                       " @ArrendamientoRenting, @IdUsuarioCreacion, @IdContratoMarco, @IdPrePeticionDetalle, @Id out",
                                        pDescripcion, pAsociadoAProyecto, pIdTipoCompra, pIdTipoCosto, pIdLineaNegocio,
                                        pIdComprador, pAdjuntaCotizacion, pMonto, pSubGrupo, pIdAreaSolicitante, pIdCentroCosto,
                                        pIdResponsablePostventa, pElementoPEP, pGrafo, pCuenta, pDescripcionCuenta, pAreaFuncional,
                                        pPeticionPenalidad, pArrendamientoRenting, pUsuarioCreacion, pIdContratoMarco, pIdPrePeticionDetalle, pId);

                respuesta.Id = Convert.ToInt32(pId.Value);
                respuesta.Mensaje = MensajesGeneralCompra.RegistrarPeticionCompra;
                respuesta.TipoRespuesta = (respuesta.Id > 0) ? Proceso.Valido : Proceso.Invalido;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }

        public List<AprobacionDtoResponse> ListaAprobaciones(PeticionCompraDtoRequest request)
        {
            SqlParameter pIdPeticionCompra = new SqlParameter
            {
                ParameterName = "IdPeticionCompra",
                Value = request.IdPeticionCompra,
                SqlDbType = SqlDbType.Int
            };

            List<AprobacionDtoResponse> resultado = context.ExecuteQuery<AprobacionDtoResponse>("[COMPRAS].[USP_LISTA_APROBACION] @IdPeticionCompra",
                                       pIdPeticionCompra).ToList<AprobacionDtoResponse>();
            return resultado;
        }

    }
}
