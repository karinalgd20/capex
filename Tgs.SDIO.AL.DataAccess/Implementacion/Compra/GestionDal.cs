﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class GestionDal : Repository<Entities.Entities.Compra.GestionPeticionCompras>, IGestionDal
    {
        ProcesoResponse respuesta = new ProcesoResponse();

        readonly DioContext context;
        
        public GestionDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

    }
}