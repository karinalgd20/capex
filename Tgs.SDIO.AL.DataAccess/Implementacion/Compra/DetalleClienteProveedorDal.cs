﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class DetalleClienteProveedorDal : Repository<Entities.Entities.Compra.DetalleClienteProveedor>, IDetalleClienteProveedorDal
    {
        ProcesoResponse respuesta = new ProcesoResponse();

        readonly DioContext context;
        
        public DetalleClienteProveedorDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public DetalleClienteProveedorDtoResponse ObtenerDetalleClienteProveedor(DetalleClienteProveedorDtoRequest request)
        {
            SqlParameter pIdPeticionCompra = new SqlParameter
            {
                ParameterName = "IdPeticionCompra",
                Value = request.IdPeticionCompra,
                SqlDbType = SqlDbType.VarChar,
                Size = 20
            };

            List<DetalleClienteProveedorDtoResponse> ListaClienteProveedor = context.ExecuteQuery<DetalleClienteProveedorDtoResponse>("COMPRAS.USP_OBTENER_DetalleClienteProveedor @IdPeticionCompra",
                                       pIdPeticionCompra).ToList<DetalleClienteProveedorDtoResponse>();


            DetalleClienteProveedorDtoResponse resultado = null;

            if (ListaClienteProveedor.Count > 0) {
                resultado = ListaClienteProveedor.FirstOrDefault();
            }

            return resultado;
        }
    }
}