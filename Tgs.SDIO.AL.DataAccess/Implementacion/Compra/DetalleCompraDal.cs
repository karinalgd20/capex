﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class DetalleCompraDal : Repository<DetalleCompra>, IDetalleCompraDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();

        public DetalleCompraDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<DetalleCompraDtoResponse> ListarDetalleCompra(DetalleCompraDtoRequest request)
        {
            var lista = (from dc in context.Set<DetalleCompra>()
                         join c1 in context.Set<Cuentas>() on dc.IdCuenta equals c1.IdCuenta into left1
                         from resultLeft1 in left1.DefaultIfEmpty()
                         join c2 in context.Set<Cuentas>() on dc.IdCuentaAnterior equals c2.IdCuenta into left2
                         from resultLeft2 in left2.DefaultIfEmpty()

                         where dc.IdPeticionCompra == request.IdPeticionCompra
                         select new DetalleCompraDtoResponse()
                            {
                                IdDetalleCompra = dc.IdDetalleCompra,
                                IdPeticionCompra = dc.IdPeticionCompra,
                                IdCuenta = dc.IdCuenta,
                                Cuenta = resultLeft1.Cuenta,
                                Actividad = dc.Actividad,
                                IdCuentaAnterior = dc.IdCuentaAnterior,
                                CuentaAnterior = resultLeft2.Cuenta,
                                Descripcion = dc.Descripcion,
                                Cantidad = dc.Cantidad,
                                CostoPorUnidad = dc.CostoPorUnidad,
                                Total = dc.Total,
                                AccionEstrategica = dc.AccionEstrategica,
                                Pep2 = dc.Pep2
                            }
                         ).ToList();

            return lista;
        }

        public DetalleCompraDtoResponse ObtenerDetalleCompraPorId(DetalleCompraDtoRequest request)
        {
            var detalleCompra = (from dc in context.Set<DetalleCompra>()
                         where dc.IdDetalleCompra == request.IdDetalleCompra
                         select dc)
             .Select(
                dcr => new DetalleCompraDtoResponse()
                {
                    IdDetalleCompra = dcr.IdDetalleCompra,
                    IdPeticionCompra = dcr.IdPeticionCompra,
                    IdCuenta = dcr.IdCuenta,
                    Actividad = dcr.Actividad,
                    IdCuentaAnterior = dcr.IdCuentaAnterior,
                    Descripcion = dcr.Descripcion,
                    Cantidad = dcr.Cantidad,
                    CostoPorUnidad = dcr.CostoPorUnidad,
                    Total = dcr.Total,
                    AccionEstrategica = dcr.AccionEstrategica,
                    Pep2 = dcr.Pep2
                }
             ).SingleOrDefault();

            return detalleCompra;
        }

        public ProcesoResponse RegistrarDetalleCompra(DetalleCompraDtoRequest request)
        {
            try
            {
                
                var pIdDetalleCompra = new SqlParameter
                {
                    ParameterName = "IdDetalleCompra",
                    Value = request.IdDetalleCompra,
                    SqlDbType = SqlDbType.Int,
                };

                var pIdPeticionCompra = new SqlParameter
                {
                    ParameterName = "IdPeticionCompra",
                    Value = request.IdPeticionCompra,
                    SqlDbType = SqlDbType.Int,
                };

                var pIdCuenta = new SqlParameter
                {
                    ParameterName = "IdCuenta",
                    Value = request.IdCuenta != null ? request.IdCuenta : Numeric.Cero,
                    SqlDbType = SqlDbType.Int
                };

                var pActividad = new SqlParameter
                {
                    ParameterName = "Actividad",
                    Value = !string.IsNullOrEmpty(request.Actividad) ? request.Actividad : (object)DBNull.Value,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 100
                };

                var pIdCuentaAnterior = new SqlParameter
                {
                    ParameterName = "IdCuentaAnterior",
                    Value = request.IdCuentaAnterior,
                    SqlDbType = SqlDbType.Int
                };

                var pDescripcion = new SqlParameter
                {
                    ParameterName = "Descripcion",
                    Value = !string.IsNullOrEmpty(request.Descripcion) ? request.Descripcion : (object)DBNull.Value,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 100
                };

                var pCantidad = new SqlParameter
                {
                    ParameterName = "Cantidad",
                    Value = request.Cantidad.HasValue ? request.Cantidad : (object)DBNull.Value,
                    SqlDbType = SqlDbType.Int
                };

                var pCostoUnitario = new SqlParameter
                {
                    ParameterName = "CostoUnitario",
                    Value = request.CostoPorUnidad,
                    SqlDbType = SqlDbType.Decimal
                };

                var pCostoTotal = new SqlParameter
                {
                    ParameterName = "CostoTotal",
                    Value = request.Total,
                    SqlDbType = SqlDbType.Decimal
                };

                var pUsuarioCreacion = new SqlParameter
                {
                    ParameterName = "IdUsuarioCreacion",
                    Value = request.IdUsuarioCreacion,
                    SqlDbType = SqlDbType.Int
                };

                var pAccionEstrategica = new SqlParameter
                {
                    ParameterName = "AccionEstrategica",
                    Value = !string.IsNullOrEmpty(request.AccionEstrategica) ? request.AccionEstrategica : (object)DBNull.Value,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 100
                };

                var pPep2 = new SqlParameter
                {
                    ParameterName = "Pep2",
                    Value = !string.IsNullOrEmpty(request.Pep2) ? request.Pep2 : (object)DBNull.Value,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };

                context.ExecuteCommand("[COMPRAS].[USP_INSERTAR_DETALLE_COMPRA] @IdDetalleCompra, @IdPeticionCompra, @IdCuenta, @Actividad, " +
                                       " @IdCuentaAnterior, @Descripcion, @Cantidad, @CostoUnitario, @CostoTotal, @IdUsuarioCreacion, @AccionEstrategica, @Pep2 ",
                                        pIdDetalleCompra, pIdPeticionCompra, pIdCuenta, pActividad, pIdCuentaAnterior,
                                        pDescripcion, pCantidad, pCostoUnitario, pCostoTotal, pUsuarioCreacion, pAccionEstrategica, pPep2);

                respuesta.Id = request.IdPeticionCompra;
                respuesta.Mensaje = MensajesGeneralCompra.RegistrarDetalleCompra;
                respuesta.TipoRespuesta = Proceso.Valido;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }

    }
}
