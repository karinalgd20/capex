﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.Entities.Entities.Compra;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class AnexoArchivoDal : Repository<Entities.Entities.Compra.AnexosArchivo>, IAnexoArchivoDal
    {
        ProcesoResponse respuesta = new ProcesoResponse();

        readonly DioContext context;
        
        public AnexoArchivoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public AnexoArchivoDtoResponse ObtenerAnexoArchivo(int iIdAnexoPeticionCompra)
        {
            AnexoArchivoDtoResponse oAnexoArchivoDtoResponse = context.Set<AnexosPeticionCompras>().
                                                                    Join(context.Set<AnexosArchivo>(),
                                                                    Anexo => Anexo.IdAnexoPeticionCompra,
                                                                    AnexoArchivo => AnexoArchivo.IdAnexoPeticionCompras,
                                                                    (Anexo, AnexoArchivo) => new { Anexo, AnexoArchivo }).ToList()
                                                                   .Where(dp => dp.Anexo.IdAnexoPeticionCompra == iIdAnexoPeticionCompra).Select(dp => new AnexoArchivoDtoResponse
                                                                   {
                                                                       IdAnexoArchivo = dp.AnexoArchivo.IdAnexoArchivo,
                                                                       IdAnexoPeticionCompras = dp.Anexo.IdAnexoPeticionCompra,
                                                                       ArchivoAdjunto = dp.AnexoArchivo.ArchivoAdjunto,
                                                                       NombreArchivo = dp.Anexo.NombreArchivo
                                                                   }).FirstOrDefault();
            return oAnexoArchivoDtoResponse;
        }
    }
}