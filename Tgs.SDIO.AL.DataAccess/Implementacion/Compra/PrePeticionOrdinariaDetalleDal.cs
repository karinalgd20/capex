﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using System.Collections.Generic;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class PrePeticionOrdinariaDetalleDal : Repository<Entities.Entities.Compra.PrePeticionOrdinariaDetalle>, IPrePeticionOrdinariaDetalleDal
    {
        ProcesoResponse respuesta = new ProcesoResponse();

        readonly DioContext context;
        
        public PrePeticionOrdinariaDetalleDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public PrePeticionOrdinariaDetalleDtoResponse ObtenerDetalleOrdinaria(PrePeticionOrdinariaDetalleDtoRequest request)
        {
            PrePeticionOrdinariaDetalleDtoResponse oPrePeticionOrdinariaDetalleDtoResponse = context.Set<PrePeticionOrdinariaDetalle>().
                                                                    Join(context.Set<Proveedor>(),
                                                                    PrePeticionOrdinariaDetalle => PrePeticionOrdinariaDetalle.IdProveedor,
                                                                    Proveedor => Proveedor.IdProveedor,
                                                                    (PrePeticionOrdinariaDetalle, Proveedor) => new { PrePeticionOrdinariaDetalle, Proveedor }).ToList()
                                                                   .Where(dp => dp.PrePeticionOrdinariaDetalle.Id == request.Id).Select(r => new PrePeticionOrdinariaDetalleDtoResponse
                                                                   {
                                                                       Id = r.PrePeticionOrdinariaDetalle.Id,
                                                                       IdCabecera = r.PrePeticionOrdinariaDetalle.IdCabecera,
                                                                       TipoCosto = r.PrePeticionOrdinariaDetalle.TipoCosto,
                                                                       ProductManager = r.PrePeticionOrdinariaDetalle.ProductManager,
                                                                       Descripcion = r.PrePeticionOrdinariaDetalle.Descripcion,
                                                                       IdProveedor = r.PrePeticionOrdinariaDetalle.IdProveedor,
                                                                       Moneda = r.PrePeticionOrdinariaDetalle.Moneda,
                                                                       Monto = r.PrePeticionOrdinariaDetalle.Monto,
                                                                       CoordinadorCompras = r.PrePeticionOrdinariaDetalle.CoordinadorCompras,
                                                                       CompradorAsignado = r.PrePeticionOrdinariaDetalle.CompradorAsignado,
                                                                       GrupoCompra = r.PrePeticionOrdinariaDetalle.GrupoCompra,
                                                                       PQAdjudicado = r.PrePeticionOrdinariaDetalle.PQAdjudicado,
                                                                       PliegoTecnico = r.PrePeticionOrdinariaDetalle.PliegoTecnico,
                                                                       CotizacionAdjunta = r.PrePeticionOrdinariaDetalle.CotizacionAdjunta,
                                                                       PosicionesOC = r.PrePeticionOrdinariaDetalle.PosicionesOC,
                                                                       FechaEntregaOC = r.PrePeticionOrdinariaDetalle.FechaEntregaOC,
                                                                       Observaciones = r.PrePeticionOrdinariaDetalle.Observaciones,
                                                                       IdLineaProducto = r.PrePeticionOrdinariaDetalle.IdLineaProducto,
                                                                       GrupoCompraConfirmado = r.PrePeticionOrdinariaDetalle.GrupoCompraConfirmado,
                                                                       RazonSocialProveedor = r.Proveedor.RazonSocial,
                                                                       RUC = r.Proveedor.RUC,
                                                                       NombreContacto = r.Proveedor.NombrePersonaContacto,
                                                                       TelefonoContacto = r.Proveedor.TelefonoContactoPrincipal,
                                                                       EmailContacto = r.Proveedor.CorreoContactoPrincipal,
                                                                   }).FirstOrDefault();

            if (oPrePeticionOrdinariaDetalleDtoResponse == null) oPrePeticionOrdinariaDetalleDtoResponse = new PrePeticionOrdinariaDetalleDtoResponse();

            return oPrePeticionOrdinariaDetalleDtoResponse;
        }

        public List<PrePeticionOrdinariaDetalleDtoResponse> ListarPrePeticionOrdinaria(PrePeticionOrdinariaDetalleDtoRequest request)
        {
            SqlParameter pIdCabecera = new SqlParameter
            {
                ParameterName = "IdCabecera",
                Value = request.IdCabecera,
                SqlDbType = SqlDbType.VarChar,
                Size = 20
            };

            SqlParameter Indice = new SqlParameter
            {
                ParameterName = "Indice",
                Value = request.Indice,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter Tamanio = new SqlParameter
            {
                ParameterName = "Tamanio",
                Value = request.Tamanio,
                SqlDbType = SqlDbType.Int
            };

            List<PrePeticionOrdinariaDetalleDtoResponse> resultado = context.ExecuteQuery<PrePeticionOrdinariaDetalleDtoResponse>("[COMPRAS].[USP_LISTAR_PREPETICIONORDINARIADETALLE] @IdCabecera, @Indice, @Tamanio",
                                       pIdCabecera, Indice, Tamanio).ToList<PrePeticionOrdinariaDetalleDtoResponse>();
            return resultado;
        }
    }
}