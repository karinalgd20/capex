﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Entities.Entities.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class PrePeticionCabeceraDal : Repository<Entities.Entities.Compra.PrePeticionCabecera>, IPrePeticionCabeceraDal
    {
        ProcesoResponse respuesta = new ProcesoResponse();

        readonly DioContext context;

        public PrePeticionCabeceraDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<PrePeticionCabeceraDtoResponse> ListarPrePeticion(PrePeticionCabeceraDtoRequest request)
        {
            SqlParameter pDescripcionProyecto = new SqlParameter
            {
                ParameterName = "DescripcionProyecto",
                Value = !string.IsNullOrEmpty(request.DescripcionProyecto) ? request.DescripcionProyecto : "",
                SqlDbType = SqlDbType.VarChar
            };

            SqlParameter pDescripcionPreCompra = new SqlParameter
            {
                ParameterName = "DescripcionPreCompra",
                Value = !string.IsNullOrEmpty(request.DescripcionPrePeticion) ? request.DescripcionPrePeticion : "",
                SqlDbType = SqlDbType.VarChar
            };

            SqlParameter pCMI = new SqlParameter
            {
                ParameterName = "CMI",
                Value = !string.IsNullOrEmpty(request.Servicio) ? request.Servicio : "",
                SqlDbType = SqlDbType.VarChar
            };

            SqlParameter pIndice = new SqlParameter
            {
                ParameterName = "Indice",
                Value = request.Indice,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pTamanio = new SqlParameter
            {
                ParameterName = "Tamanio",
                Value = request.Tamanio,
                SqlDbType = SqlDbType.Int
            };

            List<PrePeticionCabeceraDtoResponse> resultado = context.ExecuteQuery<PrePeticionCabeceraDtoResponse>
                                    ("[COMPRAS].[USP_LISTAR_PREPETICION] @DescripcionProyecto, @DescripcionPreCompra, @CMI, @Indice, @Tamanio", pDescripcionProyecto, pDescripcionPreCompra, pCMI, pIndice, pTamanio)
                                    .ToList<PrePeticionCabeceraDtoResponse>();
            return resultado;
        }

        public List<PrePeticionCabeceraDtoResponse> ListarPrePeticionCabecera(PrePeticionCabeceraDtoRequest request)
        {
            SqlParameter pIdProyecto = new SqlParameter
            {
                ParameterName = "IdProyecto",
                Value = request.IdProyecto.HasValue ? request.IdProyecto : 0,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pIdLineaCMI = new SqlParameter
            {
                ParameterName = "IdLineaCMI",
                Value = request.IdLineaCMI.HasValue ? request.IdLineaCMI : 0,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pCliente = new SqlParameter
            {
                ParameterName = "Cliente",
                Value = !string.IsNullOrEmpty(request.DescripcionCliente) ? request.DescripcionCliente : "",
                SqlDbType = SqlDbType.VarChar
            };

            SqlParameter pIdTipo = new SqlParameter
            {
                ParameterName = "IdTipo",
                Value = request.Tipo.HasValue ? request.Tipo : 0,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pIndice = new SqlParameter
            {
                ParameterName = "Indice",
                Value = request.Indice,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pTamanio = new SqlParameter
            {
                ParameterName = "Tamanio",
                Value = request.Tamanio,
                SqlDbType = SqlDbType.Int
            };

            List<PrePeticionCabeceraDtoResponse> resultado = context.ExecuteQuery<PrePeticionCabeceraDtoResponse>
                                    ("[COMPRAS].[USP_LISTAR_PREPETICION_CAB] @IdProyecto, @IdLineaCMI, @Cliente, @IdTipo, @Indice, @Tamanio", pIdProyecto, pIdLineaCMI, pCliente, pIdTipo, pIndice, pTamanio)
                                    .ToList<PrePeticionCabeceraDtoResponse>();
            return resultado;
        }

        public int ValidacionMontoLimite(PrePeticionCabeceraDtoRequest request)
        {
            SqlParameter pIdCabecera = new SqlParameter
            {
                ParameterName = "IdCabecera",
                Value = request.Id,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pIdDetalle = new SqlParameter
            {
                ParameterName = "IdDetalle",
                Value = request.IdDetalle.HasValue ? request.IdDetalle : 0,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pIdTipoCosto = new SqlParameter
            {
                ParameterName = "IdTipoCosto",
                Value = request.TipoCosto,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pIdTipoCompra = new SqlParameter
            {
                ParameterName = "IdTipoCompra",
                Value = request.Tipo,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pMonto = new SqlParameter
            {
                ParameterName = "Monto",
                Value = request.Monto.HasValue ? request.Monto : 0,
                SqlDbType = SqlDbType.Decimal
            };

            int resultado = context.ExecuteQuery<int>
                                    ("[COMPRAS].[USP_VALIDARMONTO_PREPETICION] @IdCabecera, @IdDetalle, @IdTipoCosto, @IdTipoCompra, @Monto", pIdCabecera, pIdDetalle, pIdTipoCosto, pIdTipoCompra, pMonto)
                                    .FirstOrDefault<int>();
            return resultado;
        }

        public string ObtenerSaldo(PrePeticionCabeceraDtoRequest request)
        {
            SqlParameter pIdCabecera = new SqlParameter
            {
                ParameterName = "IdCabecera",
                Value = request.Id,
                SqlDbType = SqlDbType.Int
            };

            SqlParameter pIdTipoCosto = new SqlParameter
            {
                ParameterName = "IdTipoCosto",
                Value = request.TipoCosto.HasValue ? request.TipoCosto : 0,
                SqlDbType = SqlDbType.Int
            };

            string resultado = context.ExecuteQuery<string>
                                    ("[COMPRAS].[USP_OBTENERSALDO_PREPETICION] @IdCabecera, @IdTipoCosto", pIdCabecera, pIdTipoCosto)
                                    .FirstOrDefault<string>();
            return resultado;
        }

        public PrePeticionCabeceraDtoResponse ObtenerPrePeticionCabecera(PrePeticionCabeceraDtoRequest request)
        {
            PrePeticionCabeceraDtoResponse oPrePeticionCabeceraDtoResponse = context.Set<PrePeticionCabecera>().
                                                                    Join(context.Set<Cliente>(),
                                                                    PrePeticionCabecera => PrePeticionCabecera.IdCliente,
                                                                    Cliente => Cliente.IdCliente,
                                                                    (PrePeticionCabecera, Cliente) => new { PrePeticionCabecera, Cliente }).
                                                                    GroupJoin(context.Set<Sector>(),
                                                                    Cliente => Cliente.Cliente.IdSector,
                                                                    Sector => Sector.IdSector,
                                                                    (Cliente, Sector) => new { Cliente, Sector }).ToList()
                                                                   .Where(dp => dp.Cliente.PrePeticionCabecera.Id == request.Id).Select(r => new PrePeticionCabeceraDtoResponse
                                                                   {
                                                                       Id = r.Cliente.PrePeticionCabecera.Id,
                                                                       IdCliente = r.Cliente.PrePeticionCabecera.IdCliente,
                                                                       IdLineaCMI = r.Cliente.PrePeticionCabecera.IdLineaCMI,
                                                                       IdProyecto = r.Cliente.PrePeticionCabecera.IdProyecto,
                                                                       CodigoSIGO = r.Cliente.PrePeticionCabecera.CodigoSIGO,
                                                                       Tipo = r.Cliente.PrePeticionCabecera.Tipo,
                                                                       FechaInicioProyecto = r.Cliente.PrePeticionCabecera.FechaInicioProyecto,
                                                                       DescripcionCliente = r.Cliente.Cliente.Descripcion,
                                                                       GerenteComercialCliente = r.Cliente.Cliente.GerenteComercial,
                                                                       DescripcionSector = r.Sector.Any() ? r.Sector.FirstOrDefault().Descripcion : ""
                                                                   }).FirstOrDefault();

            if (oPrePeticionCabeceraDtoResponse == null)
            {
                oPrePeticionCabeceraDtoResponse = new PrePeticionCabeceraDtoResponse();
            }

            return oPrePeticionCabeceraDtoResponse;
        }
    }
}