﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Compra
{
    public class ConfiguracionEtapaDal : Repository<ConfiguracionEtapa>, IConfiguracionEtapaDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public ConfiguracionEtapaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

    }
}
