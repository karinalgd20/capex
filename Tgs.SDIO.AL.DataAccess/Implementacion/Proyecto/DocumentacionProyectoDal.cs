﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Proyecto;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Proyecto
{
    public class DocumentacionProyectoDal : Repository<Entities.Entities.Proyecto.DocumentacionProyecto>, IDocumentacionProyectoDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public DocumentacionProyectoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<DocumentacionProyectoDtoResponse> ListarDocumentacionProyecto(int iIdProyecto)
        {
            SqlParameter pIdProyecto = new SqlParameter
            {
                ParameterName = "IdProyecto",
                Value = iIdProyecto,
                SqlDbType = SqlDbType.Int
            };


            List<DocumentacionProyectoDtoResponse> resultado = context.ExecuteQuery<DocumentacionProyectoDtoResponse>("PROYECTO.USP_LISTA_DOCUMENTOS @IdProyecto ",
                                       pIdProyecto).ToList<DocumentacionProyectoDtoResponse>();
            return resultado;

            //int IdRelation = Convert.ToInt32(Generales.TablaMaestra.TipoDocumentoAdjuntos);
            //List <Maestra> ListaTipoDocumento = context.Set<Maestra>().ToList().Where(m => m.IdRelacion == IdRelation).ToList();

            //List<DocumentacionProyectoDtoResponse> ListaDocumentacionProyecto = context.Set<DocumentacionProyecto>().
            //                                                        Join(context.Set<DocumentacionArchivo>(),
            //                                                        DocumentacionProyecto => DocumentacionProyecto.IdDocumentoProyecto,
            //                                                        DocumentacionArchivo => DocumentacionArchivo.IdDocumentoProyecto,
            //                                                        (DocumentacionProyecto, DocumentacionArchivo) => new { DocumentacionProyecto, DocumentacionArchivo }).ToList().
            //                                                        Join(ListaTipoDocumento,
            //                                                        DocumentacionProyecto => DocumentacionProyecto.DocumentacionProyecto.IdDocumento,
            //                                                        TipoDocumento => Convert.ToInt32(TipoDocumento.Valor),
            //                                                        (DocumentacionProyecto, TipoDocumento) => new { DocumentacionProyecto, TipoDocumento })
            //                                                        .Where(dp => dp.DocumentacionProyecto.DocumentacionProyecto.IdProyecto == iIdProyecto).Select(dp => new DocumentacionProyectoDtoResponse
            //                                                        {
            //                                                            IdDocumentoArchivo = dp.DocumentacionProyecto.DocumentacionArchivo.IdDocumentoArchivo,
            //                                                            IdDocumentoProyecto = dp.DocumentacionProyecto.DocumentacionProyecto.IdDocumentoProyecto,
            //                                                            IdProyecto = dp.DocumentacionProyecto.DocumentacionProyecto.IdProyecto,
            //                                                            NombreArchivo = dp.DocumentacionProyecto.DocumentacionProyecto.NombreArchivo,
            //                                                            DesTipoDocumento = dp.TipoDocumento.Descripcion,
            //                                                            ArchivoAdjunto = dp.DocumentacionProyecto.DocumentacionArchivo.ArchivoAdjunto,
            //                                                        }).ToList();
        }
    }
}



