﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Proyecto
{
    public class DetalleFacturarDal : Repository<DetalleFacturar>, IDetalleFacturarDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();

        public DetalleFacturarDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public DetalleFacturarDtoResponse ObtenerDetalleFacturarPorId(DetalleFacturarDtoRequest request)
        {
            var detalleFacturar = (from detalleFacEN in context.Set<DetalleFacturar>()
                                   where (detalleFacEN.IdDetalleFacturar == request.IdProyecto)
                                   select detalleFacEN).SingleOrDefault();

            return new DetalleFacturarDtoResponse()
            {
                IdDetalleFacturar = detalleFacturar.IdDetalleFacturar,
                IdProyecto = detalleFacturar.IdProyecto,
                IdConceptoIngresoEgreso = detalleFacturar.IdConceptoIngresoEgreso,
                IdFormaPago = detalleFacturar.IdFormaPago,
                IdMonedaPagoUnico = detalleFacturar.IdMonedaPagoUnico,
                MontoPagoUnico = detalleFacturar.MontoPagoUnico,
                IdMonedaRecurrenteMensual = detalleFacturar.IdMonedaRecurrenteMensual,
                MontoRecurrenteMensual = detalleFacturar.MontoRecurrenteMensual,
                NumeroMeses = detalleFacturar.NumeroMeses,
                TasaTipoCambio = detalleFacturar.TasaTipoCambio,
                Observaciones = detalleFacturar.Observaciones,
            };
        }

        public DetalleFacturarDtoResponse ObtenerDetalleFacturarPorIdProyecto(DetalleFacturarDtoRequest request)
        {
            var detalleFacturar = (from df in context.Set<DetalleFacturar>().Where(x => x.IdProyecto == request.IdProyecto)
                                   join m in context.Set<Maestra>() on df.IdMonedaPagoUnico.ToString() equals m.Valor into lm
                                   from nlm in lm.Where(x => x.IdRelacion == 98).DefaultIfEmpty()
                                   select new DetalleFacturarDtoResponse()
                                   {
                                       IdDetalleFacturar = df.IdDetalleFacturar,
                                       IdProyecto = df.IdProyecto,
                                       IdConceptoIngresoEgreso = df.IdConceptoIngresoEgreso,
                                       IdFormaPago = df.IdFormaPago,
                                       IdMonedaPagoUnico = df.IdMonedaPagoUnico,
                                       MontoPagoUnico = df.MontoPagoUnico,
                                       IdMonedaRecurrenteMensual = df.IdMonedaRecurrenteMensual,
                                       MontoRecurrenteMensual = df.MontoRecurrenteMensual,
                                       NumeroMeses = df.NumeroMeses,
                                       TasaTipoCambio = df.TasaTipoCambio,
                                       Observaciones = df.Observaciones,
                                       MonedaPagoUnico = nlm.Descripcion,
                                       MonedaRecurrenteMensual = nlm.Descripcion,
                                   });

            return detalleFacturar.SingleOrDefault();
        }

    }
}
