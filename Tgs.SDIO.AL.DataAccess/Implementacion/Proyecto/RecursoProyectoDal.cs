﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Proyecto
{
    public class RecursoProyectoDal : Repository<Entities.Entities.Proyecto.RecursoProyecto>, IRecursoProyectoDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public RecursoProyectoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<RecursoProyectoDtoResponse> ListarRecursoProyecto(int iIdProyecto)
        {
            List<RecursoProyectoDtoResponse> ListaRecursoProyectoDefault = new List<RecursoProyectoDtoResponse>();
            ListaRecursoProyectoDefault.Add(new RecursoProyectoDtoResponse {
                IdCargo = 4,
                DesCargo = "Analista Financiero",
                IdRol = 4
            });
            ListaRecursoProyectoDefault.Add(new RecursoProyectoDtoResponse
            {
                IdCargo = 8,
                DesCargo = "Lider Jefe Proyecto",
                IdRol = 8
            });
            ListaRecursoProyectoDefault.Add(new RecursoProyectoDtoResponse
            {
                IdCargo = 3,
                DesCargo = "Gerente de Cuenta / Account Manager",
                IdRol = 3
            });

            ListaRecursoProyectoDefault.Add(new RecursoProyectoDtoResponse
            {
                IdCargo = 7,
                DesCargo = "Jefe de Proyecto (JP)",
                IdRol = 7
            });

            List<RecursoProyectoDtoResponse> ListaRecursoProyecto = context.Set<RecursoProyecto>().
                                                                    Join(context.Set<Recurso>(),
                                                                    RecursoProyecto => RecursoProyecto.IdRecurso.Value,
                                                                    Recurso => Recurso.IdRecurso,
                                                                    (RecursoProyecto, Recurso) => new { RecursoProyecto, Recurso })
                                                                    .Where(dp => dp.RecursoProyecto.IdProyecto == iIdProyecto).Select(dp => new RecursoProyectoDtoResponse
                                                                    {
                                                                        IdAsignacion = dp.RecursoProyecto.IdAsignacion,
                                                                        IdProyecto = dp.RecursoProyecto.IdProyecto,
                                                                        IdRecurso = dp.RecursoProyecto.IdRecurso,
                                                                        IdRol = dp.RecursoProyecto.IdRol,
                                                                        IdCargo = dp.Recurso.IdCargo.Value,
                                                                        Email = dp.Recurso.Email
                                                                    }).ToList();

            ListaRecursoProyectoDefault = ListaRecursoProyectoDefault.GroupJoin(ListaRecursoProyecto,
                                                                                rpdefault => rpdefault.IdCargo,
                                                                                rp => rp.IdCargo,
                                                                                (rpdefault, rp) => new { rpdefault, rp }).Select(dp => new RecursoProyectoDtoResponse
                                                                                {
                                                                                    IdAsignacion = dp.rp.FirstOrDefault() == null ? 0 : dp.rp.FirstOrDefault().IdAsignacion,
                                                                                    IdProyecto = dp.rp.FirstOrDefault() == null ? 0 : dp.rp.FirstOrDefault().IdProyecto,
                                                                                    IdRecurso = dp.rp.FirstOrDefault() == null ? 0 : dp.rp.FirstOrDefault().IdRecurso,
                                                                                    IdRol = dp.rpdefault.IdRol,
                                                                                    IdCargo = dp.rpdefault.IdCargo,
                                                                                    Email = dp.rp.FirstOrDefault() == null ? "" : dp.rp.FirstOrDefault().Email,
                                                                                    DesCargo = dp.rpdefault.DesCargo
                                                                                }).ToList();

            return ListaRecursoProyectoDefault;
        }

        public RecursoProyectoDtoResponse ObtenerRecursoPorProyectoRol(RecursoProyectoDtoRequest request)
        {

            var resultado = (from r in context.Set<RecursoProyecto>()
                             where (r.IdProyecto == request.IdProyecto &&
                                 r.IdRol == request.IdRol)
                             select new RecursoProyectoDtoResponse
                             {
                                 IdAsignacion = r.IdAsignacion,
                                 IdProyecto = r.IdProyecto,
                                 IdRol = r.IdRol,
                                 IdRecurso = r.IdRecurso
                             }).SingleOrDefault();

            return resultado;
        }

    }
}



