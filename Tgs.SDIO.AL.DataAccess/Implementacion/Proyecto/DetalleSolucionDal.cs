﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Proyecto
{
    public class DetalleSolucionDal : Repository<DetalleSolucion>, IDetalleSolucionDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public DetalleSolucionDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<DetalleSolucionDtoResponse> ObtenerDetallePorIdProyecto(DetalleSolucionDtoRequest request)
        {
            var ListadoDetalleSolucion = (from detalleSolEN in context.Set<DetalleSolucion>()
                                          where (detalleSolEN.IdProyecto == request.IdProyecto)
                                          select new DetalleSolucionDtoResponse
                                          {
                                              IdDetalleSolucion = detalleSolEN.IdDetalleSolucion,
                                              IdProyecto = detalleSolEN.IdProyecto,
                                              IdTipoSolucion = detalleSolEN.IdTipoSolucion
                                          }).OrderBy(x => x.IdDetalleSolucion);

            return ListadoDetalleSolucion.ToList();
        }

        public DetalleSolucionDtoResponse ObtenerDetalleSolucionById(DetalleSolucionDtoRequest request)
        {
            var detalleSolucion = (from detalleSolEN in context.Set<DetalleSolucion>()
                                   where (detalleSolEN.IdProyecto == request.IdProyecto)
                                   select detalleSolEN).SingleOrDefault();

            return new DetalleSolucionDtoResponse()
            {
                IdDetalleSolucion = detalleSolucion.IdDetalleSolucion,
                IdProyecto = detalleSolucion.IdProyecto,
                IdTipoSolucion = detalleSolucion.IdTipoSolucion
            };
        }
    }
}
