﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;
using Tgs.SDIO.Util.Constantes;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Proyecto
{
    public class DocumentacionArchivoDal : Repository<Entities.Entities.Proyecto.DocumentacionArchivo>, IDocumentacionArchivoDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public DocumentacionArchivoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public DocumentacionArchivoDtoResponse ObtenerDocumentoArchivo(int iIdDocumentoArchivo)
        {
            DocumentacionArchivoDtoResponse oDocumentacionArchivoDtoResponse = context.Set<DocumentacionProyecto>().
                                                                    Join(context.Set<DocumentacionArchivo>(),
                                                                    DocumentacionProyecto => DocumentacionProyecto.IdDocumentoProyecto,
                                                                    DocumentacionArchivo => DocumentacionArchivo.IdDocumentoProyecto,
                                                                    (DocumentacionProyecto, DocumentacionArchivo) => new { DocumentacionProyecto, DocumentacionArchivo }).ToList()
                                                                   .Where(dp => dp.DocumentacionArchivo.IdDocumentoArchivo == iIdDocumentoArchivo).Select(dp => new DocumentacionArchivoDtoResponse
                                                                   {
                                                                        IdDocumentoArchivo = dp.DocumentacionArchivo.IdDocumentoArchivo,
                                                                        IdDocumentoProyecto = dp.DocumentacionProyecto.IdDocumentoProyecto,
                                                                        ArchivoAdjunto = dp.DocumentacionArchivo.ArchivoAdjunto,
                                                                        NombreArchivo = dp.DocumentacionProyecto.NombreArchivo
                                                                    }).FirstOrDefault();
            return oDocumentacionArchivoDtoResponse;
        }
    }
}



