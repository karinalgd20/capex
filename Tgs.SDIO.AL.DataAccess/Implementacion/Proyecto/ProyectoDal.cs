﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Entidad = Tgs.SDIO.Entities.Entities.Proyecto;
using Tgs.SDIO.Util.Constantes;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Util.Mensajes.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Proyecto
{
    public class ProyectoDal : Repository<Entidad.Proyecto>, IProyectoDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public ProyectoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ProyectoDtoResponse> ListarProyectoCabecera(ProyectoDtoRequest proyecto)
        {
            SqlParameter pCODIGOPROYECTO = new SqlParameter {
                ParameterName = "CodigoProy", Value = (proyecto.IdProyectoLKM == null ? string.Empty : proyecto.IdProyectoLKM),
                SqlDbType = SqlDbType.VarChar, Size= 20
            };

            SqlParameter pCLIENTE = new SqlParameter {
                ParameterName = "Cliente", Value = (proyecto.Cliente == null ? string.Empty : proyecto.Cliente),
                SqlDbType = SqlDbType.VarChar, Size = 50
            };

            SqlParameter pIDETAPA = new SqlParameter {
                ParameterName = "IdEtapa", Value = proyecto.IdEtapaActual, SqlDbType = SqlDbType.Int
            };

            SqlParameter pFECHAINICIO = new SqlParameter {
                ParameterName = "FechaIni", Value = (proyecto.FechaInicio == null ? string.Empty : proyecto.FechaInicio),
                SqlDbType = SqlDbType.VarChar, Size= 10
            };

            SqlParameter pFECHAFIN = new SqlParameter {
                ParameterName = "FechaFin", Value = (proyecto.FechaFin == null ? string.Empty : proyecto.FechaFin),
                SqlDbType = SqlDbType.VarChar, Size = 10
            };

            SqlParameter Indice = new SqlParameter {
                ParameterName = "Indice", Value = proyecto.Indice, SqlDbType = SqlDbType.Int
            };

            SqlParameter Tamanio = new SqlParameter {
                ParameterName = "Tamanio", Value = proyecto.Tamanio, SqlDbType = SqlDbType.Int
            };

            List<ProyectoDtoResponse> resultado = context.ExecuteQuery<ProyectoDtoResponse>("PROYECTO.USP_LISTA_PROYECTO_CAB @CodigoProy, @Cliente, @IdEtapa, @FechaIni, @FechaFin, @Indice, @Tamanio", 
                                       pCODIGOPROYECTO, pCLIENTE, pIDETAPA, pFECHAINICIO, pFECHAFIN ,Indice, Tamanio).ToList<ProyectoDtoResponse>();
            return resultado;
        }

        public OportunidadGanadaDtoResponse ObtenerOportunidadGanada(OportunidadGanadaDtoRequest request)
        {

            var oportunidad = (from p in context.Set<Entidad.Proyecto>()
                                   where (p.IdProyectoSF == request.IdOportunidad)
                                   select p).SingleOrDefault();

            OportunidadGanadaDtoResponse resultado;
            if (oportunidad == null)
            {
                SqlParameter IdOportunidad = new SqlParameter { ParameterName = "IdOportunidad",
                            Value = request.IdOportunidad, SqlDbType = SqlDbType.VarChar };

                resultado = 
                    context.ExecuteQuery<OportunidadGanadaDtoResponse>("PROYECTO.USP_OBTENER_OPORTUNIDADGANADA @IdOportunidad", 
                    IdOportunidad).FirstOrDefault<OportunidadGanadaDtoResponse>();
                resultado.TipoRespuesta = Proceso.Valido;
                
            }
            else
            {
                resultado = new OportunidadGanadaDtoResponse() {
                    Mensaje = MensajesGeneralProyecto.ValidarOportunidad
                };
                resultado.TipoRespuesta = Proceso.Invalido;
            }

            return resultado;
        }

        public ProcesoResponse RegistrarProyecto(ProyectoDtoRequest proyecto)
        {
            try
            {
                var pIDOportunidad = new SqlParameter {
                        ParameterName = "IDPROYECTOSF",
                        Value = proyecto.IdProyectoSF,
                        SqlDbType = SqlDbType.VarChar, Size = 20};
                var pIDCliente = new SqlParameter
                {
                    ParameterName = "IDCLIENTE",
                    Value = proyecto.IdCliente,
                    SqlDbType = SqlDbType.Int
                };
                var pFechaOportunidad = new SqlParameter
                {
                    ParameterName = "FECHAOPORTUNIDAD",
                    Value = proyecto.FechaOportunidadGanada,
                    SqlDbType = SqlDbType.DateTime
                };
                var pUsuarioCreacion = new SqlParameter
                {
                    ParameterName = "IDUSUARIOCREACION",
                    Value = proyecto.IdUsuarioCreacion,
                    SqlDbType = SqlDbType.Int
                };
                var pId = new SqlParameter
                {
                    ParameterName = "ID",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Output
                };

                context.ExecuteCommand("[PROYECTO].[USP_INSERTAR_PROYECTO] @IDPROYECTOSF, @IDCLIENTE, @FECHAOPORTUNIDAD, @IDUSUARIOCREACION, @ID out", 
                                        pIDOportunidad, pIDCliente, pFechaOportunidad, pUsuarioCreacion, pId);

                respuesta.Id = Convert.ToInt32(pId.Value);
                respuesta.Mensaje = MensajesGeneralProyecto.RegistrarProyecto;
                respuesta.TipoRespuesta = (respuesta.Id > 0) ? Proceso.Valido : Proceso.Invalido;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }

        public ProyectoDtoResponse ObtenerProyectoByID(ProyectoDtoRequest proyecto)
        {
            SqlParameter IdProyecto = new SqlParameter { ParameterName = "IdProyecto", Value = proyecto.IdProyecto, SqlDbType = SqlDbType.Int };

            ProyectoDtoResponse resultado = context.ExecuteQuery<ProyectoDtoResponse>("PROYECTO.USP_OBTENER_PROYECTO @IdProyecto", IdProyecto).FirstOrDefault<ProyectoDtoResponse>();
            return resultado;
        }

        public ProcesoResponse RegistrarStageFinanciero(DetalleFinancieroDtoRequest request)
        {
            try
            {
                var pItem = new SqlParameter { ParameterName = "Item", Value = request.Item, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pNumeroDeOportunidad = new SqlParameter { ParameterName = "NumeroDeOportunidad", Value = request.NumeroDeOportunidad, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pNroDeCaso = new SqlParameter { ParameterName = "NroDeCaso", Value = request.NroDeCaso, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pTipoDeProyecto = new SqlParameter { ParameterName = "TipoDeProyecto", Value = request.TipoDeProyecto, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPagoUnico = new SqlParameter { ParameterName = "PagoUnico", Value = request.PagoUnico, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPagoRecurrente = new SqlParameter { ParameterName = "PagoRecurrente", Value = request.PagoRecurrente, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pNroDeMesesDePagoRecurrente = new SqlParameter { ParameterName = "NroDeMesesDePagoRecurrente", Value = request.NroDeMesesDePagoRecurrente, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPagoTotal = new SqlParameter { ParameterName = "PagoTotal", Value = request.PagoTotal, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pTipoDeCambio = new SqlParameter { ParameterName = "TipoDeCambio", Value = request.TipoDeCambio, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pMargenOibda = new SqlParameter { ParameterName = "MargenOibda", Value = request.MargenOibda, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pVan = new SqlParameter { ParameterName = "Van", Value = request.Van, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pVanVai = new SqlParameter { ParameterName = "VanVai", Value = request.VanVai, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pOibda = new SqlParameter { ParameterName = "Oibda", Value = request.Oibda, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPaybackMeses = new SqlParameter { ParameterName = "PaybackMeses", Value = request.PaybackMeses, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pAnalistaFinancieroEvaluador = new SqlParameter { ParameterName = "AnalistaFinancieroEvaluador", Value = request.AnalistaFinancieroEvaluador, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pAnalistaFinancieroUsuarioLotus = new SqlParameter { ParameterName = "AnalistaFinancieroUsuarioLotus", Value = request.AnalistaFinancieroUsuarioLotus, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pAnalistaFinancieroCorreoOutlook = new SqlParameter { ParameterName = "AnalistaFinancieroCorreoOutlook", Value = request.AnalistaFinancieroCorreoOutlook, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pImporteDeVentasUss = new SqlParameter { ParameterName = "ImporteDeVentasUss", Value = request.ImporteDeVentasUss, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pCostosOpex = new SqlParameter { ParameterName = "CostosOpex", Value = request.CostosOpex, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pCapex = new SqlParameter { ParameterName = "Capex", Value = request.Capex, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pAntenasVsatCapexCapexValorResidual = new SqlParameter { ParameterName = "AntenasVsatCapexCapexValorResidual", Value = request.AntenasVsatCapexCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pAntenasVsatCapexLogInversa = new SqlParameter { ParameterName = "AntenasVsatCapexLogInversa", Value = request.AntenasVsatCapexLogInversa, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pClearChannelCapexCapexValorResidual = new SqlParameter { ParameterName = "ClearChannelCapexCapexValorResidual", Value = request.ClearChannelCapexCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pClearChannelCapexLogInversa = new SqlParameter { ParameterName = "ClearChannelCapexLogInversa", Value = request.ClearChannelCapexLogInversa, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pEquiposDeSeguridadCapexCapexValorResidual = new SqlParameter { ParameterName = "EquiposDeSeguridadCapexCapexValorResidual", Value = request.EquiposDeSeguridadCapexCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pEquiposDeSeguridadCapexLogInversa = new SqlParameter { ParameterName = "EquiposDeSeguridadCapexLogInversa", Value = request.EquiposDeSeguridadCapexLogInversa, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pEquiposEeEeCapexCapexValorResidual = new SqlParameter { ParameterName = "EquiposEeEeCapexCapexValorResidual", Value = request.EquiposEeEeCapexCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pEstudiosEspecialesCapexValorResidual = new SqlParameter { ParameterName = "EstudiosEspecialesCapexValorResidual", Value = request.EstudiosEspecialesCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pGabinetesCapexCapexValorResidual = new SqlParameter { ParameterName = "GabinetesCapexCapexValorResidual", Value = request.GabinetesCapexCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pGabinetesCapexLogInversa = new SqlParameter { ParameterName = "GabinetesCapexLogInversa", Value = request.GabinetesCapexLogInversa, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pHardwareCapexCapexValorResidual = new SqlParameter { ParameterName = "HardwareCapexCapexValorResidual", Value = request.HardwareCapexCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pHardwareCapexLogInversa = new SqlParameter { ParameterName = "HardwareCapexLogInversa", Value = request.HardwareCapexLogInversa, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionEquiposYLicenciasCchnCapexValorResidual = new SqlParameter { ParameterName = "InstalacionEquiposYLicenciasCchnCapexValorResidual", Value = request.InstalacionEquiposYLicenciasCchnCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionEquiposYLicenciasCchnCapexLogInversa = new SqlParameter { ParameterName = "InstalacionEquiposYLicenciasCchnCapexLogInversa", Value = request.InstalacionEquiposYLicenciasCchnCapexLogInversa, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionEquiposYLicenciasVsatCapexValorResidual = new SqlParameter { ParameterName = "InstalacionEquiposYLicenciasVsatCapexValorResidual", Value = request.InstalacionEquiposYLicenciasVsatCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionEquiposYLicenciasVsatCapexLogInversa = new SqlParameter { ParameterName = "InstalacionEquiposYLicenciasVsatCapexLogInversa", Value = request.InstalacionEquiposYLicenciasVsatCapexLogInversa, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionesRoutersCapexValorResidual = new SqlParameter { ParameterName = "InstalacionesRoutersCapexValorResidual", Value = request.InstalacionesRoutersCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionesRoutersLogInversa = new SqlParameter { ParameterName = "InstalacionesRoutersLogInversa", Value = request.InstalacionesRoutersLogInversa, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInversion35GhzCapexValorResidual = new SqlParameter { ParameterName = "Inversion35GhzCapexValorResidual", Value = request.Inversion35GhzCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInversion35GhzLogInversa = new SqlParameter { ParameterName = "Inversion35GhzLogInversa", Value = request.Inversion35GhzLogInversa, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pModemsCapexCapexValorResidual = new SqlParameter { ParameterName = "ModemsCapexCapexValorResidual", Value = request.ModemsCapexCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pModemsCapexLogInversa = new SqlParameter { ParameterName = "ModemsCapexLogInversa", Value = request.ModemsCapexLogInversa, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pRoutersCapexCapexValorResidual = new SqlParameter { ParameterName = "RoutersCapexCapexValorResidual", Value = request.RoutersCapexCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pRoutersCapexLogInversa = new SqlParameter { ParameterName = "RoutersCapexLogInversa", Value = request.RoutersCapexLogInversa, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSmartvpnCapexCapexValorResidual = new SqlParameter { ParameterName = "SmartvpnCapexCapexValorResidual", Value = request.SmartvpnCapexCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSoftwareCapexCapexValorResidual = new SqlParameter { ParameterName = "SoftwareCapexCapexValorResidual", Value = request.SoftwareCapexCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSolarwindCapexCapexValorResidual = new SqlParameter { ParameterName = "SolarwindCapexCapexValorResidual", Value = request.SolarwindCapexCapexValorResidual, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pComisionComercial = new SqlParameter { ParameterName = "ComisionComercial", Value = request.ComisionComercial, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pContingenciaDeCostos = new SqlParameter { ParameterName = "ContingenciaDeCostos", Value = request.ContingenciaDeCostos, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pCostosCambium = new SqlParameter { ParameterName = "CostosCambium", Value = request.CostosCambium, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pCostosCircuitos = new SqlParameter { ParameterName = "CostosCircuitos", Value = request.CostosCircuitos, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pCostosInternosDatacenter = new SqlParameter { ParameterName = "CostosInternosDatacenter", Value = request.CostosInternosDatacenter, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pCostosInternosOutsourcingDesktop = new SqlParameter { ParameterName = "CostosInternosOutsourcingDesktop", Value = request.CostosInternosOutsourcingDesktop, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pCostosInternosOutsourcingImpresion = new SqlParameter { ParameterName = "CostosInternosOutsourcingImpresion", Value = request.CostosInternosOutsourcingImpresion, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionEquiposYLicenciasOpex = new SqlParameter { ParameterName = "InstalacionEquiposYLicenciasOpex", Value = request.InstalacionEquiposYLicenciasOpex, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pOtrosCostosIndirectos = new SqlParameter { ParameterName = "OtrosCostosIndirectos", Value = request.OtrosCostosIndirectos, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPersonalPropioTelefonica = new SqlParameter { ParameterName = "PersonalPropioTelefonica", Value = request.PersonalPropioTelefonica, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPersonalPropioTelefonicaMantSop = new SqlParameter { ParameterName = "PersonalPropioTelefonicaMantSop", Value = request.PersonalPropioTelefonicaMantSop, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSegmentoSatelital = new SqlParameter { ParameterName = "SegmentoSatelital", Value = request.SegmentoSatelital, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSoporteYMttoEqTelecomunicaciones = new SqlParameter { ParameterName = "SoporteYMttoEqTelecomunicaciones", Value = request.SoporteYMttoEqTelecomunicaciones, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pTelecomunicaciones = new SqlParameter { ParameterName = "Telecomunicaciones", Value = request.Telecomunicaciones, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pTelecomunicaciones35Ghz = new SqlParameter { ParameterName = "Telecomunicaciones35Ghz", Value = request.Telecomunicaciones35Ghz, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pTelecomunicacionesSmartvpn = new SqlParameter { ParameterName = "TelecomunicacionesSmartvpn", Value = request.TelecomunicacionesSmartvpn, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pTelecomunicacionesSolarwin = new SqlParameter { ParameterName = "TelecomunicacionesSolarwin", Value = request.TelecomunicacionesSolarwin, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pViaticosYGastosDePersonalTdp = new SqlParameter { ParameterName = "ViaticosYGastosDePersonalTdp", Value = request.ViaticosYGastosDePersonalTdp, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pAntenasVsatCapexNoStock = new SqlParameter { ParameterName = "AntenasVsatCapexNoStock", Value = request.AntenasVsatCapexNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pAntenasVsatCapexStock = new SqlParameter { ParameterName = "AntenasVsatCapexStock", Value = request.AntenasVsatCapexStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pClearChannelCapexNoStock = new SqlParameter { ParameterName = "ClearChannelCapexNoStock", Value = request.ClearChannelCapexNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pClearChannelCapexStock = new SqlParameter { ParameterName = "ClearChannelCapexStock", Value = request.ClearChannelCapexStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pEquiposDeSeguridadCapexNoStock = new SqlParameter { ParameterName = "EquiposDeSeguridadCapexNoStock", Value = request.EquiposDeSeguridadCapexNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pEquiposDeSeguridadCapexStock = new SqlParameter { ParameterName = "EquiposDeSeguridadCapexStock", Value = request.EquiposDeSeguridadCapexStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pEquiposEeEeCapexEeEe = new SqlParameter { ParameterName = "EquiposEeEeCapexEeEe", Value = request.EquiposEeEeCapexEeEe, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pEstudiosEspecialesEeEe = new SqlParameter { ParameterName = "EstudiosEspecialesEeEe", Value = request.EstudiosEspecialesEeEe, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pGabinetesCapexNoStock = new SqlParameter { ParameterName = "GabinetesCapexNoStock", Value = request.GabinetesCapexNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pHardwareCapexNoStock = new SqlParameter { ParameterName = "HardwareCapexNoStock", Value = request.HardwareCapexNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionEquiposYLicenciasCchnNoStock = new SqlParameter { ParameterName = "InstalacionEquiposYLicenciasCchnNoStock", Value = request.InstalacionEquiposYLicenciasCchnNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionEquiposYLicenciasCchnStock = new SqlParameter { ParameterName = "InstalacionEquiposYLicenciasCchnStock", Value = request.InstalacionEquiposYLicenciasCchnStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionEquiposYLicenciasVsatNoStock = new SqlParameter { ParameterName = "InstalacionEquiposYLicenciasVsatNoStock", Value = request.InstalacionEquiposYLicenciasVsatNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionEquiposYLicenciasVsatStock = new SqlParameter { ParameterName = "InstalacionEquiposYLicenciasVsatStock", Value = request.InstalacionEquiposYLicenciasVsatStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionesRoutersNoStock = new SqlParameter { ParameterName = "InstalacionesRoutersNoStock", Value = request.InstalacionesRoutersNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionesRoutersStock = new SqlParameter { ParameterName = "InstalacionesRoutersStock", Value = request.InstalacionesRoutersStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInversion35GhzNoStock = new SqlParameter { ParameterName = "Inversion35GhzNoStock", Value = request.Inversion35GhzNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInversion35GhzStock = new SqlParameter { ParameterName = "Inversion35GhzStock", Value = request.Inversion35GhzStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pModemsCapexNoStock = new SqlParameter { ParameterName = "ModemsCapexNoStock", Value = request.ModemsCapexNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pModemsCapexStock = new SqlParameter { ParameterName = "ModemsCapexStock", Value = request.ModemsCapexStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pRoutersCapexNoStock = new SqlParameter { ParameterName = "RoutersCapexNoStock", Value = request.RoutersCapexNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pRoutersCapexStock = new SqlParameter { ParameterName = "RoutersCapexStock", Value = request.RoutersCapexStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSmartvpnCapexNoStock = new SqlParameter { ParameterName = "SmartvpnCapexNoStock", Value = request.SmartvpnCapexNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSoftwareCapexNoStock = new SqlParameter { ParameterName = "SoftwareCapexNoStock", Value = request.SoftwareCapexNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSolarwindCapexNoStock = new SqlParameter { ParameterName = "SolarwindCapexNoStock", Value = request.SolarwindCapexNoStock, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pCableadoYSuministrosDc = new SqlParameter { ParameterName = "CableadoYSuministrosDc", Value = request.CableadoYSuministrosDc, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pCableadoYSuministrosPe = new SqlParameter { ParameterName = "CableadoYSuministrosPe", Value = request.CableadoYSuministrosPe, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pComisionPartnerDc = new SqlParameter { ParameterName = "ComisionPartnerDc", Value = request.ComisionPartnerDc, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pComisionPartnerPe = new SqlParameter { ParameterName = "ComisionPartnerPe", Value = request.ComisionPartnerPe, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pGestionDeProyectosDt = new SqlParameter { ParameterName = "GestionDeProyectosDt", Value = request.GestionDeProyectosDt, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pGestionDeProyectosPe = new SqlParameter { ParameterName = "GestionDeProyectosPe", Value = request.GestionDeProyectosPe, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pHardwareCostoDeVentas = new SqlParameter { ParameterName = "HardwareCostoDeVentas", Value = request.HardwareCostoDeVentas, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionEntregaConfiguracionYTransporteDc = new SqlParameter { ParameterName = "InstalacionEntregaConfiguracionYTransporteDc", Value = request.InstalacionEntregaConfiguracionYTransporteDc, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pInstalacionEntregaConfiguracionYTransportePe = new SqlParameter { ParameterName = "InstalacionEntregaConfiguracionYTransportePe", Value = request.InstalacionEntregaConfiguracionYTransportePe, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pMesaDeAyuda = new SqlParameter { ParameterName = "MesaDeAyuda", Value = request.MesaDeAyuda, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPenalidades = new SqlParameter { ParameterName = "Penalidades", Value = request.Penalidades, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPersonalTercerosDatacenter = new SqlParameter { ParameterName = "PersonalTercerosDatacenter", Value = request.PersonalTercerosDatacenter, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPersonalTercerosOutsourcingDeImpresion = new SqlParameter { ParameterName = "PersonalTercerosOutsourcingDeImpresion", Value = request.PersonalTercerosOutsourcingDeImpresion, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPersonalTercerosRenovacionTecnologica = new SqlParameter { ParameterName = "PersonalTercerosRenovacionTecnologica", Value = request.PersonalTercerosRenovacionTecnologica, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPersonalTercerosServiciosTransaccionales = new SqlParameter { ParameterName = "PersonalTercerosServiciosTransaccionales", Value = request.PersonalTercerosServiciosTransaccionales, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPlataformaTraficoSeguro = new SqlParameter { ParameterName = "PlataformaTraficoSeguro", Value = request.PlataformaTraficoSeguro, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pRentingDeEquiposCcuuYSeguridad = new SqlParameter { ParameterName = "RentingDeEquiposCcuuYSeguridad", Value = request.RentingDeEquiposCcuuYSeguridad, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pRentingDeImpresoras = new SqlParameter { ParameterName = "RentingDeImpresoras", Value = request.RentingDeImpresoras, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pRentingDeLicencias = new SqlParameter { ParameterName = "RentingDeLicencias", Value = request.RentingDeLicencias, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pRentingDePcsLaptopsYAccesorios = new SqlParameter { ParameterName = "RentingDePcsLaptopsYAccesorios", Value = request.RentingDePcsLaptopsYAccesorios, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pRentingDeServidoresYAccesorios = new SqlParameter { ParameterName = "RentingDeServidoresYAccesorios", Value = request.RentingDeServidoresYAccesorios, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSalidaInternet = new SqlParameter { ParameterName = "SalidaInternet", Value = request.SalidaInternet, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSegmentoSatelitalIntenet = new SqlParameter { ParameterName = "SegmentoSatelitalIntenet", Value = request.SegmentoSatelitalIntenet, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSegurosDeEquipos = new SqlParameter { ParameterName = "SegurosDeEquipos", Value = request.SegurosDeEquipos, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pServiciosCloud = new SqlParameter { ParameterName = "ServiciosCloud", Value = request.ServiciosCloud, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pServiciosDeCapacitacionDc = new SqlParameter { ParameterName = "ServiciosDeCapacitacionDc", Value = request.ServiciosDeCapacitacionDc, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pServiciosDeCapacitacionPe = new SqlParameter { ParameterName = "ServiciosDeCapacitacionPe", Value = request.ServiciosDeCapacitacionPe, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pServicioDeDisponibilidadTecnologica = new SqlParameter { ParameterName = "ServicioDeDisponibilidadTecnologica", Value = request.ServicioDeDisponibilidadTecnologica, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pServiciosParaOutsourcingDeImpresion = new SqlParameter { ParameterName = "ServiciosParaOutsourcingDeImpresion", Value = request.ServiciosParaOutsourcingDeImpresion, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pServiciosParaRenovacionTecnologica = new SqlParameter { ParameterName = "ServiciosParaRenovacionTecnologica", Value = request.ServiciosParaRenovacionTecnologica, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSimcard = new SqlParameter { ParameterName = "Simcard", Value = request.Simcard, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSoftwareCostosDeVentas = new SqlParameter { ParameterName = "SoftwareCostosDeVentas", Value = request.SoftwareCostosDeVentas, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSoporteYMantenimientoCcuuCentrales = new SqlParameter { ParameterName = "SoporteYMantenimientoCcuuCentrales", Value = request.SoporteYMantenimientoCcuuCentrales, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSoporteYMantenimientoCcuuHwSw = new SqlParameter { ParameterName = "SoporteYMantenimientoCcuuHwSw", Value = request.SoporteYMantenimientoCcuuHwSw, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSoporteYMantenimientoCcuuSsp = new SqlParameter { ParameterName = "SoporteYMantenimientoCcuuSsp", Value = request.SoporteYMantenimientoCcuuSsp, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSoporteYMantenimientoTi = new SqlParameter { ParameterName = "SoporteYMantenimientoTi", Value = request.SoporteYMantenimientoTi, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSuministrosDeBackup = new SqlParameter { ParameterName = "SuministrosDeBackup", Value = request.SuministrosDeBackup, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pTelecomunicacionesTerceros = new SqlParameter { ParameterName = "TelecomunicacionesTerceros", Value = request.TelecomunicacionesTerceros, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pTerminalesAlquiler = new SqlParameter { ParameterName = "TerminalesAlquiler", Value = request.TerminalesAlquiler, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pTerminalesVenta = new SqlParameter { ParameterName = "TerminalesVenta", Value = request.TerminalesVenta, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pTributos = new SqlParameter { ParameterName = "Tributos", Value = request.Tributos, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pViaticosYGastosDePersonalDc = new SqlParameter { ParameterName = "ViaticosYGastosDePersonalDc", Value = request.ViaticosYGastosDePersonalDc, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pViaticosYGastosDePersonalPe = new SqlParameter { ParameterName = "ViaticosYGastosDePersonalPe", Value = request.ViaticosYGastosDePersonalPe, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pLinea = new SqlParameter { ParameterName = "Linea", Value = request.Linea, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pSublinea = new SqlParameter { ParameterName = "Sublinea", Value = request.Sublinea, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pServicio = new SqlParameter { ParameterName = "Servicio", Value = request.Servicio, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pProducto = new SqlParameter { ParameterName = "Producto", Value = request.Producto, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pProductoAf = new SqlParameter { ParameterName = "ProductoAf", Value = request.ProductoAf, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pAnalistaControlGastosUsuarioLotus = new SqlParameter { ParameterName = "AnalistaControlGastosUsuarioLotus", Value = request.AnalistaControlGastosUsuarioLotus, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pAnalistaControlGastosCorreoOutlook = new SqlParameter { ParameterName = "AnalistaControlGastosCorreoOutlook", Value = request.AnalistaControlGastosCorreoOutlook, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pPorcentaje = new SqlParameter { ParameterName = "Porcentaje", Value = request.Porcentaje, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pCeCo = new SqlParameter { ParameterName = "CeCo", Value = request.CeCo, SqlDbType = SqlDbType.VarChar, Size = 255 };
                var pTipoTabla = new SqlParameter { ParameterName = "TipoTabla", Value = request.TipoTabla, SqlDbType = SqlDbType.Int};
                var pIdProyecto = new SqlParameter { ParameterName = "IdProyecto", Value = request.IdProyecto, SqlDbType = SqlDbType.Int };
                var pFila = new SqlParameter { ParameterName = "Fila", Value = request.Fila, SqlDbType = SqlDbType.Int };


                context.ExecuteCommand("[PROYECTO].[USP_INSERTAR_STAGES_FINANCIERO] @Item , @NumeroDeOportunidad , @NroDeCaso , @TipoDeProyecto , @PagoUnico , @PagoRecurrente , @NroDeMesesDePagoRecurrente , " +
                    "@PagoTotal, @TipoDeCambio, @MargenOibda, @Van, @VanVai, @Oibda, @PaybackMeses, @AnalistaFinancieroEvaluador, " +
                    "@AnalistaFinancieroUsuarioLotus, @AnalistaFinancieroCorreoOutlook, @ImporteDeVentasUss, @CostosOpex, @Capex, " +
                    "@AntenasVsatCapexCapexValorResidual, @AntenasVsatCapexLogInversa, @ClearChannelCapexCapexValorResidual, " +
                    "@ClearChannelCapexLogInversa, @EquiposDeSeguridadCapexCapexValorResidual, @EquiposDeSeguridadCapexLogInversa, " +
                    "@EquiposEeEeCapexCapexValorResidual, @EstudiosEspecialesCapexValorResidual, @GabinetesCapexCapexValorResidual, @GabinetesCapexLogInversa, " +
                    "@HardwareCapexCapexValorResidual, @HardwareCapexLogInversa, @InstalacionEquiposYLicenciasCchnCapexValorResidual, " +
                    "@InstalacionEquiposYLicenciasCchnCapexLogInversa, @InstalacionEquiposYLicenciasVsatCapexValorResidual, " +
                    "@InstalacionEquiposYLicenciasVsatCapexLogInversa, @InstalacionesRoutersCapexValorResidual, @InstalacionesRoutersLogInversa, " +
                    "@Inversion35GhzCapexValorResidual, @Inversion35GhzLogInversa, @ModemsCapexCapexValorResidual, @ModemsCapexLogInversa, " +
                    "@RoutersCapexCapexValorResidual, @RoutersCapexLogInversa, @SmartvpnCapexCapexValorResidual, @SoftwareCapexCapexValorResidual, " +
                    "@SolarwindCapexCapexValorResidual, @ComisionComercial, @ContingenciaDeCostos, @CostosCambium, @CostosCircuitos, " +
                    "@CostosInternosDatacenter, @CostosInternosOutsourcingDesktop, @CostosInternosOutsourcingImpresion, @InstalacionEquiposYLicenciasOpex," +
                    "@OtrosCostosIndirectos, @PersonalPropioTelefonica, @PersonalPropioTelefonicaMantSop, @SegmentoSatelital," +
                    "@SoporteYMttoEqTelecomunicaciones, @Telecomunicaciones, @Telecomunicaciones35Ghz, @TelecomunicacionesSmartvpn, " +
                    "@TelecomunicacionesSolarwin, @ViaticosYGastosDePersonalTdp, @AntenasVsatCapexNoStock, @AntenasVsatCapexStock, " +
                    "@ClearChannelCapexNoStock, @ClearChannelCapexStock, @EquiposDeSeguridadCapexNoStock, @EquiposDeSeguridadCapexStock, " +
                    "@EquiposEeEeCapexEeEe, @EstudiosEspecialesEeEe, @GabinetesCapexNoStock, @HardwareCapexNoStock, @InstalacionEquiposYLicenciasCchnNoStock, " +
                    "@InstalacionEquiposYLicenciasCchnStock, @InstalacionEquiposYLicenciasVsatNoStock, @InstalacionEquiposYLicenciasVsatStock, " +
                    "@InstalacionesRoutersNoStock, @InstalacionesRoutersStock, @Inversion35GhzNoStock, @Inversion35GhzStock, " +
                    "@ModemsCapexNoStock, @ModemsCapexStock, @RoutersCapexNoStock, @RoutersCapexStock, @SmartvpnCapexNoStock," +
                    "@SoftwareCapexNoStock, @SolarwindCapexNoStock, @CableadoYSuministrosDc, @CableadoYSuministrosPe, @ComisionPartnerDc, " +
                    "@ComisionPartnerPe, @GestionDeProyectosDt, @GestionDeProyectosPe, @HardwareCostoDeVentas, @InstalacionEntregaConfiguracionYTransporteDc, " +
                    "@InstalacionEntregaConfiguracionYTransportePe, @MesaDeAyuda, @Penalidades, @PersonalTercerosDatacenter, " +
                    "@PersonalTercerosOutsourcingDeImpresion, @PersonalTercerosRenovacionTecnologica, @PersonalTercerosServiciosTransaccionales, " +
                    "@PlataformaTraficoSeguro, @RentingDeEquiposCcuuYSeguridad, @RentingDeImpresoras, @RentingDeLicencias, " +
                    "@RentingDePcsLaptopsYAccesorios, @RentingDeServidoresYAccesorios, @SalidaInternet, @SegmentoSatelitalIntenet, @SegurosDeEquipos, " +
                    "@ServiciosCloud, @ServiciosDeCapacitacionDc, @ServiciosDeCapacitacionPe, @ServicioDeDisponibilidadTecnologica, " +
                    "@ServiciosParaOutsourcingDeImpresion, @ServiciosParaRenovacionTecnologica, @Simcard, @SoftwareCostosDeVentas, " +
                    "@SoporteYMantenimientoCcuuCentrales, @SoporteYMantenimientoCcuuHwSw, @SoporteYMantenimientoCcuuSsp, @SoporteYMantenimientoTi, " +
                    "@SuministrosDeBackup, @TelecomunicacionesTerceros, @TerminalesAlquiler, @TerminalesVenta, @Tributos, " +
                    "@ViaticosYGastosDePersonalDc, @ViaticosYGastosDePersonalPe, @Linea, @Sublinea, @Servicio, @Producto, @ProductoAf, " +
                    "@AnalistaControlGastosUsuarioLotus, @AnalistaControlGastosCorreoOutlook, @Porcentaje, @CeCo, @IdProyecto, @TipoTabla, @Fila "
                    , pItem, pNumeroDeOportunidad, pNroDeCaso, pTipoDeProyecto, pPagoUnico, pPagoRecurrente, pNroDeMesesDePagoRecurrente, pPagoTotal,
                    pTipoDeCambio, pMargenOibda, pVan, pVanVai, pOibda, pPaybackMeses, pAnalistaFinancieroEvaluador, pAnalistaFinancieroUsuarioLotus,
                    pAnalistaFinancieroCorreoOutlook, pImporteDeVentasUss, pCostosOpex, pCapex, pAntenasVsatCapexCapexValorResidual,
                    pAntenasVsatCapexLogInversa, pClearChannelCapexCapexValorResidual, pClearChannelCapexLogInversa, pEquiposDeSeguridadCapexCapexValorResidual,
                    pEquiposDeSeguridadCapexLogInversa, pEquiposEeEeCapexCapexValorResidual, pEstudiosEspecialesCapexValorResidual,
                    pGabinetesCapexCapexValorResidual, pGabinetesCapexLogInversa, pHardwareCapexCapexValorResidual, pHardwareCapexLogInversa,
                    pInstalacionEquiposYLicenciasCchnCapexValorResidual, pInstalacionEquiposYLicenciasCchnCapexLogInversa,
                    pInstalacionEquiposYLicenciasVsatCapexValorResidual, pInstalacionEquiposYLicenciasVsatCapexLogInversa,
                    pInstalacionesRoutersCapexValorResidual, pInstalacionesRoutersLogInversa, pInversion35GhzCapexValorResidual,
                    pInversion35GhzLogInversa, pModemsCapexCapexValorResidual, pModemsCapexLogInversa, pRoutersCapexCapexValorResidual,
                    pRoutersCapexLogInversa, pSmartvpnCapexCapexValorResidual, pSoftwareCapexCapexValorResidual, pSolarwindCapexCapexValorResidual,
                    pComisionComercial, pContingenciaDeCostos, pCostosCambium, pCostosCircuitos, pCostosInternosDatacenter,
                    pCostosInternosOutsourcingDesktop, pCostosInternosOutsourcingImpresion, pInstalacionEquiposYLicenciasOpex,
                    pOtrosCostosIndirectos, pPersonalPropioTelefonica, pPersonalPropioTelefonicaMantSop, pSegmentoSatelital,
                    pSoporteYMttoEqTelecomunicaciones, pTelecomunicaciones, pTelecomunicaciones35Ghz, pTelecomunicacionesSmartvpn,
                    pTelecomunicacionesSolarwin, pViaticosYGastosDePersonalTdp, pAntenasVsatCapexNoStock, pAntenasVsatCapexStock,
                    pClearChannelCapexNoStock, pClearChannelCapexStock, pEquiposDeSeguridadCapexNoStock, pEquiposDeSeguridadCapexStock,
                    pEquiposEeEeCapexEeEe, pEstudiosEspecialesEeEe, pGabinetesCapexNoStock, pHardwareCapexNoStock, pInstalacionEquiposYLicenciasCchnNoStock,
                    pInstalacionEquiposYLicenciasCchnStock, pInstalacionEquiposYLicenciasVsatNoStock, pInstalacionEquiposYLicenciasVsatStock,
                    pInstalacionesRoutersNoStock, pInstalacionesRoutersStock, pInversion35GhzNoStock, pInversion35GhzStock, pModemsCapexNoStock,
                    pModemsCapexStock, pRoutersCapexNoStock, pRoutersCapexStock, pSmartvpnCapexNoStock, pSoftwareCapexNoStock, pSolarwindCapexNoStock,
                    pCableadoYSuministrosDc, pCableadoYSuministrosPe, pComisionPartnerDc, pComisionPartnerPe, pGestionDeProyectosDt,
                    pGestionDeProyectosPe, pHardwareCostoDeVentas, pInstalacionEntregaConfiguracionYTransporteDc,
                    pInstalacionEntregaConfiguracionYTransportePe, pMesaDeAyuda, pPenalidades, pPersonalTercerosDatacenter,
                    pPersonalTercerosOutsourcingDeImpresion, pPersonalTercerosRenovacionTecnologica, pPersonalTercerosServiciosTransaccionales,
                    pPlataformaTraficoSeguro, pRentingDeEquiposCcuuYSeguridad, pRentingDeImpresoras, pRentingDeLicencias, pRentingDePcsLaptopsYAccesorios,
                    pRentingDeServidoresYAccesorios, pSalidaInternet, pSegmentoSatelitalIntenet, pSegurosDeEquipos, pServiciosCloud,
                    pServiciosDeCapacitacionDc, pServiciosDeCapacitacionPe, pServicioDeDisponibilidadTecnologica, pServiciosParaOutsourcingDeImpresion,
                    pServiciosParaRenovacionTecnologica, pSimcard, pSoftwareCostosDeVentas, pSoporteYMantenimientoCcuuCentrales, pSoporteYMantenimientoCcuuHwSw,
                    pSoporteYMantenimientoCcuuSsp, pSoporteYMantenimientoTi, pSuministrosDeBackup, pTelecomunicacionesTerceros,
                    pTerminalesAlquiler, pTerminalesVenta, pTributos, pViaticosYGastosDePersonalDc, pViaticosYGastosDePersonalPe, pLinea, pSublinea,
                    pServicio, pProducto, pProductoAf, pAnalistaControlGastosUsuarioLotus, pAnalistaControlGastosCorreoOutlook, pPorcentaje, pCeCo, 
                    pIdProyecto, pTipoTabla , pFila
                    );

                respuesta.Mensaje = "Grabado con éxito.";
                respuesta.TipoRespuesta = Proceso.Valido;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }

        public ProcesoResponse ProcesarArchivosFinancieros(ProyectoDtoRequest request)
        {
            try
            {
                var pIdProyecto = new SqlParameter
                {
                    ParameterName = "IdProyecto",
                    Value = request.IdProyecto,
                    SqlDbType = SqlDbType.Int
                };
                var pIdUsuario = new SqlParameter
                {
                    ParameterName = "IdUsuario",
                    Value = request.IdUsuarioCreacion,
                    SqlDbType = SqlDbType.Int
                };

                context.ExecuteCommand("[PROYECTO].[USP_GENERAR_DATOS_FINANCIERO] @IdProyecto, @IdUsuario",
                                        pIdProyecto, pIdUsuario);

                respuesta.TipoRespuesta = Proceso.Valido;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }

        public List<PagoFinancieroDtoResponse> ListarPagos(ProyectoDtoRequest request)
        {
            SqlParameter pIdProyecto = new SqlParameter
            {
                ParameterName = "IdProyecto",
                Value = request.IdProyecto,
                SqlDbType = SqlDbType.Int
            };


            List<PagoFinancieroDtoResponse> resultado = context.ExecuteQuery<PagoFinancieroDtoResponse>("PROYECTO.USP_LISTA_PAGOFINANCIERO @IdProyecto ",
                                       pIdProyecto).ToList<PagoFinancieroDtoResponse>();
            return resultado;
        }

        public List<IndicadorFinancieroDtoResponse> ListarIndicadores(ProyectoDtoRequest request)
        {
            SqlParameter pIdProyecto = new SqlParameter
            {
                ParameterName = "IdProyecto",
                Value = request.IdProyecto,
                SqlDbType = SqlDbType.Int
            };


            List<IndicadorFinancieroDtoResponse> resultado = context.ExecuteQuery<IndicadorFinancieroDtoResponse>("PROYECTO.USP_LISTA_INDICADORESFINANCIERO @IdProyecto ",
                                       pIdProyecto).ToList<IndicadorFinancieroDtoResponse>();
            return resultado;
        }

        public List<CostoFinancieroDtoResponse> ListarCostos(ProyectoDtoRequest request)
        {
            SqlParameter pIdProyecto = new SqlParameter
            {
                ParameterName = "IdProyecto",
                Value = request.IdProyecto,
                SqlDbType = SqlDbType.Int
            };


            List<CostoFinancieroDtoResponse> resultado = context.ExecuteQuery<CostoFinancieroDtoResponse>("PROYECTO.USP_LISTA_COSTOSFINANCIERO @IdProyecto ",
                                       pIdProyecto).ToList<CostoFinancieroDtoResponse>();
            return resultado;
        }

        public List<CostoConceptoDtoResponse> ListarCostosOpex(ProyectoDtoRequest request)
        {
            SqlParameter pIdProyecto = new SqlParameter
            {
                ParameterName = "IdProyecto",
                Value = request.IdProyecto,
                SqlDbType = SqlDbType.Int
            };


            List<CostoConceptoDtoResponse> resultado = context.ExecuteQuery<CostoConceptoDtoResponse>("PROYECTO.USP_LISTA_COSTOSOPEX @IdProyecto ",
                                       pIdProyecto).ToList<CostoConceptoDtoResponse>();
            return resultado;
        }

        public List<CostoConceptoDtoResponse> ListarCostosCapex(ProyectoDtoRequest request)
        {
            SqlParameter pIdProyecto = new SqlParameter
            {
                ParameterName = "IdProyecto",
                Value = request.IdProyecto,
                SqlDbType = SqlDbType.Int
            };


            List<CostoConceptoDtoResponse> resultado = context.ExecuteQuery<CostoConceptoDtoResponse>("PROYECTO.USP_LISTA_COSTOSCAPEX @IdProyecto ",
                                       pIdProyecto).ToList<CostoConceptoDtoResponse>();
            return resultado;
        }

        public List<DetalleFinancieroDtoResponse> ListarServiciosCMI(ProyectoDtoRequest request)
        {
            SqlParameter pIdProyecto = new SqlParameter
            {
                ParameterName = "IdProyecto",
                Value = request.IdProyecto,
                SqlDbType = SqlDbType.Int
            };


            List<DetalleFinancieroDtoResponse> resultado = context.ExecuteQuery<DetalleFinancieroDtoResponse>("PROYECTO.USP_LISTA_SERVICIOS_CMI @IdProyecto ",
                                       pIdProyecto).ToList<DetalleFinancieroDtoResponse>();
            return resultado;
        }

        public ProcesoResponse RegistrarEtapaEstado(EtapaProyectoDtoRequest request)
        {
            try
            {
                SqlParameter pIdProyecto = new SqlParameter
                {
                    ParameterName = "IdProyecto",
                    Value = request.IdProyecto,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter pIdFase = new SqlParameter
                {
                    ParameterName = "IdFase",
                    Value = request.IdFase,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter pIdEtapa = new SqlParameter
                {
                    ParameterName = "IdEtapa",
                    Value = request.IdEtapa,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter pIdEstado = new SqlParameter
                {
                    ParameterName = "IdEstado",
                    Value = request.IdEstadoEtapa,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter pObservaciones = new SqlParameter
                {
                    ParameterName = "Observaciones",
                    Value = request.Observaciones,
                    SqlDbType = SqlDbType.VarChar
                };
                SqlParameter pIdUsuarioCreacion = new SqlParameter
                {
                    ParameterName = "IdUsuarioCreacion",
                    Value = request.IdUsuarioCreacion,
                    SqlDbType = SqlDbType.Int
                };

                context.ExecuteCommand("PROYECTO.USP_INSERTAR_ETAPA_ESTADO @IdProyecto, @IdFase, @IdEtapa, @IdEstado, @Observaciones, @IdUsuarioCreacion ",
                                           pIdProyecto,
                                           pIdFase,
                                           pIdEtapa,
                                           pIdEstado,
                                           pObservaciones,
                                           pIdUsuarioCreacion);

                respuesta.Mensaje = "Grabado con éxito.";
                respuesta.TipoRespuesta = Proceso.Valido;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }

        public List<DetalleFinancieroDtoResponse> BuscarDetalleFinancieroPorProyecto(ProyectoDtoRequest request)
        {

            var resultado = (from p in context.Set<Entidad.DetalleFinanciero>()
                               where (
                               (p.IdTipoDetalle == TipoDetalleFinanciero.DetalleCMI) &&
                               (p.IdProyecto == request.IdProyecto) &&
                               (p.CostoOpex > 0)) select p)
                               .Select(
                                df => new DetalleFinancieroDtoResponse
                                {
                                    IdDetalleFinanciero = df.IdDetalleFinanciero,
                                    IdProyecto = df.IdProyecto,
                                    IdTipoDetalle = df.IdTipoDetalle,
                                    NroOportunidad = df.NroOportunidad,
                                    NroCaso = df.NroCaso,
                                    TipoProyecto = df.TipoProyecto,
                                    Linea = df.Linea,
                                    SubLinea = df.SubLinea,
                                    Servicio = df.Servicio,
                                    Producto = df.Producto,
                                    ProductoAf = df.ProductoAf,
                                    Porcentaje = df.Porcentaje,
                                    ImporteVentas = df.ImporteVentas,
                                    CostoOpex = df.CostoOpex,
                                    CostoCapex = df.CostoCapex,
                                    CeCo = df.CeCo,
                                    IdEstado = df.IdEstado,
                                    IdUsuarioCreacion = df.IdUsuarioCreacion,
                                    FechaCreacion = df.FechaCreacion
                                }).ToList();

            return resultado;
        }
        public List<ProyectoDtoResponse> ListarProyecto(ProyectoDtoRequest request)
        {
            var consulta = (from p in context.Set<Entidad.Proyecto>()
                            join c in context.Set<Cliente>() on p.IdCliente equals c.IdCliente
                            join m in context.Set<Maestra>().Where(x => x.IdRelacion == 168) on c.IdTipoEntidad.ToString() equals m.Valor
                            select new ProyectoDtoResponse
                            {
                                Descripcion = p.Descripcion,
                                IdCliente = p.IdCliente,
                                NombreCliente = c.Descripcion,
                                IdProyectoSF = p.IdProyectoSF,
                                IdProyecto = p.IdProyecto,
                                TipoEntidad = c.IdTipoEntidad,
                                IdProyectoLKM = p.IdProyectoLKM,
                                Entidad = m.Descripcion
                            }).ToList();

            return consulta;
        }
    }
}