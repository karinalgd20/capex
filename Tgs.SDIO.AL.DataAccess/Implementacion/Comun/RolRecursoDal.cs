﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class RolRecursoDal : Repository<RolRecurso>, IRolRecursoDal
    {
        readonly DioContext context;
        public RolRecursoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public RolRecursoPaginadoDtoResponse ListadoRolRecursosPaginado(RolRecursoDtoRequest request)
        {
            var query = (from rolrecurso in context.Set<RolRecurso>()
                         where ((rolrecurso.Descripcion.Contains(request.Descripcion) || string.IsNullOrEmpty(request.Descripcion))
                         && (rolrecurso.IdEstado ==  Activo || rolrecurso.IdEstado ==  Inactivo))
                         orderby rolrecurso.FechaCreacion descending
                         select new RolRecursoDtoResponse
                         {
                             IdRol = rolrecurso.IdRol,
                             Descripcion = rolrecurso.Descripcion,
                             Nivel = rolrecurso.Nivel,                           
                             IdEstado = rolrecurso.IdEstado,
                             Estado = rolrecurso.IdEstado ==  Activo ? "Activo" : "Inactivo",
                             IdUsuarioCreacion = rolrecurso.IdUsuarioCreacion,
                             FechaCreacion = rolrecurso.FechaCreacion,
                             IdUsuarioEdicion = rolrecurso.IdUsuarioEdicion,
                             FechaEdicion = rolrecurso.FechaEdicion
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);


            var datosCarga = new RolRecursoPaginadoDtoResponse
            {
                ListRolRecursoDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };


            return datosCarga;
        }

        public List<ListaDtoResponse> ListarComboRolRecurso()
        {

            var query = (from bit in context.RolRecurso.Where(x => x.IdEstado ==  Activo) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.IdRol.ToString() });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
