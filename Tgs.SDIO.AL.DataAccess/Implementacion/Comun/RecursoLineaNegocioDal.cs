﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using System.Data.Entity;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class RecursoLineaNegocioDal : Repository<RecursoLineaNegocio>, IRecursoLineaNegocioDal
    {
        private readonly DioContext context;
        public RecursoLineaNegocioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public RecursoLineaNegocioPaginadoDtoResponse ListarRecursoLineaNegocioPaginado(RecursoLineaNegocioDtoRequest recursoLineaRequest)
        {
            var consultaDetalleLinea = (from recursolinea in context.Set<RecursoLineaNegocio>()
                                        join linea in context.Set<LineaNegocio>() on recursolinea.IdLineaNegocio equals linea.IdLineaNegocio
                                        join recurso in context.Set<Recurso>() on recursolinea.IdRecurso equals recurso.IdRecurso
                                        where
                                        recurso.IdUsuarioRais == recursoLineaRequest.IdUsuarioRais
                                        orderby recurso.FechaCreacion descending
                                        select new RecursoLineaNegocioDtoResponse
                                        {
                                            DescripcionLinea = linea.Descripcion,
                                            IdRecursoLineaNegocio = recursolinea.Id,
                                            Estado = recursolinea.IdEstado == EstadosEntidad.Activo ? EstadoDescripcion.Activo : EstadoDescripcion.Inactivo,
                                            IdEstado = recursolinea.IdEstado
                                        }).AsNoTracking().ToPagedList(recursoLineaRequest.Indice, recursoLineaRequest.Tamanio);

            var datosRecursoLinea = new RecursoLineaNegocioPaginadoDtoResponse
            {
                ListaRecursoLineaNegocio = consultaDetalleLinea.ToList(),
                TotalItemCount = consultaDetalleLinea.TotalItemCount
            };

            return datosRecursoLinea;
        }

    }
}
