﻿using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using System.Linq;
using System.Data.Entity;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using static Tgs.SDIO.Util.Constantes.Generales.TablaMaestra;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ActividadDal : Repository<Actividad>, IActividadDal
    {
        readonly DioContext context;

        public ActividadDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public ActividadPaginadoDtoResponse ListatActividadPaginado(ActividadDtoRequest request)
        {
            var query = (from actividad in context.Set<Actividad>()
                         join area in context.Set<AreaSeguimiento>() on actividad.IdAreaSeguimiento equals area.IdAreaSeguimiento
                         join maestra in context.Set<Maestra>().Where(x => x.IdRelacion == TipoActividad) on actividad.IdTipoActividad equals maestra.Valor
                         join fase in context.Set<Fase>() on actividad.IdFase equals fase.IdFase

                         where ((actividad.Descripcion.Contains(request.Descripcion) || string.IsNullOrEmpty(request.Descripcion))
                                && (actividad.IdEtapa == request.IdEtapa || request.IdEtapa == Seleccione)
                                && (area.IdAreaSeguimiento == request.IdAreaSeguimiento || request.IdAreaSeguimiento == Seleccione)
                                && area.IdEstado ==  Activo
                                && maestra.IdEstado == Activo
                                && (actividad.IdEstado == Activo || actividad.IdEstado == Inactivo))
                         orderby actividad.FechaCreacion descending
                         select new ActividadDtoResponse
                         {
                             IdActividad = actividad.IdActividad,
                             IdActividadPadre = actividad.IdActividadPadre,
                             Descripcion = actividad.Descripcion,
                             Predecesoras = actividad.Predecesoras,
                             DescripcionArea = area.Descripcion,
                             DescripcionTipoActividad = maestra.Descripcion,
                             DescripcionFase = fase.Descripcion,
                             AsegurarOferta = actividad.AsegurarOferta,
                             FlgCapexMayor = actividad.FlgCapexMayor,
                             FlgCapexMenor = actividad.FlgCapexMenor,
                             NumeroDiaCapexMenor = actividad.NumeroDiaCapexMenor,
                             CantidadDiasCapexMenor = actividad.CantidadDiasCapexMenor,
                             NumeroDiaCapexMayor = actividad.NumeroDiaCapexMayor,
                             CantidadDiasCapexMayor = actividad.CantidadDiasCapexMayor,
                             Estado = area.IdEstado == Activo ? "Activo" : "Inactivo"
                         }).AsNoTracking().Distinct().OrderBy(x => x.IdActividad).ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new ActividadPaginadoDtoResponse
            {
                ListActividadDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public ActividadDtoResponse ObtenerActividadPorId(ActividadDtoRequest request)
        {

            var querySegmento = (from actiseg in context.Set<ActividadSegmentoNegocio>()
                                 join seg in context.Set<SegmentoNegocio>() on actiseg.IdSegmentoNegocio equals seg.IdSegmentoNegocio
                                 where seg.IdEstado == Activo
                                 && (actiseg.IdEstado == Activo && actiseg.IdActividad == request.IdActividad)
                                 orderby actiseg.FechaCreacion descending
                                 select new ComboDtoResponse
                                 {
                                     id = actiseg.IdSegmentoNegocio.ToString(),
                                     label = seg.Descripcion
                                 }).AsNoTracking().Distinct().ToList();

            var queryActividad = (from actividad in context.Set<Actividad>()
                                  where actividad.IdActividad == request.IdActividad && (actividad.IdEstado == Activo || actividad.IdEstado == Inactivo)
                                  select actividad).AsNoTracking().FirstOrDefault();

            return new ActividadDtoResponse()
            {
                IdActividad = queryActividad.IdActividad,
                IdActividadPadre = queryActividad.IdActividadPadre,
                Descripcion = queryActividad.Descripcion,
                Predecesoras = queryActividad.Predecesoras,
                IdFase = queryActividad.IdFase,
                IdEtapa = queryActividad.IdEtapa,
                FlgCapexMayor = queryActividad.FlgCapexMayor,
                FlgCapexMenor = queryActividad.FlgCapexMenor,
                IdAreaSeguimiento = queryActividad.IdAreaSeguimiento,
                NumeroDiaCapexMenor = queryActividad.NumeroDiaCapexMenor,
                CantidadDiasCapexMenor = queryActividad.CantidadDiasCapexMenor,
                NumeroDiaCapexMayor = queryActividad.NumeroDiaCapexMayor,
                CantidadDiasCapexMayor = queryActividad.CantidadDiasCapexMayor,
                IdTipoActividad = queryActividad.IdTipoActividad,
                AsegurarOferta = queryActividad.AsegurarOferta,
                IdEstado = queryActividad.IdEstado,
                IdUsuarioCreacion = queryActividad.IdUsuarioCreacion,
                FechaCreacion = queryActividad.FechaCreacion,
                IdUsuarioEdicion = queryActividad.IdUsuarioEdicion,
                FechaEdicion = queryActividad.FechaEdicion,
                ListSegmentoNegocio = querySegmento
            };


        }

        public List<ListaDtoResponse> ListarComboActividadPadres()
        {
            var query = (from bit in context.Actividad.Where(x => x.IdEstado == Activo && x.IdActividadPadre == null) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.IdActividad.ToString() });
            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }

    }
}
