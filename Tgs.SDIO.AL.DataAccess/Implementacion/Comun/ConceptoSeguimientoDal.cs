﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ConceptoSeguimientoDal : Repository<ConceptoSeguimiento>, IConceptoSeguimientoDal
    {
        readonly DioContext context;
        public ConceptoSeguimientoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public ConceptoSeguimientoPaginadoDtoResponse ListadoConceptosSeguimientoPaginado(ConceptoSeguimientoDtoRequest request)
        {

            var query = (from concepto in context.Set<ConceptoSeguimiento>()
                         where ((concepto.Descripcion.Contains(request.Descripcion) || string.IsNullOrEmpty(request.Descripcion))
                         && (concepto.IdEstado == Activo || concepto.IdEstado == Inactivo))
                         orderby concepto.FechaCreacion descending
                         select new ConceptoSeguimientoDtoResponse
                         {
                             IdConcepto = concepto.IdConcepto,
                             DescripcionConceptoPadre = (from coceptoPadre in context.Set<ConceptoSeguimiento>() where coceptoPadre.IdConcepto == concepto.IdConceptoPadre && coceptoPadre.IdEstado == Activo select coceptoPadre.Descripcion).FirstOrDefault(),
                             IdConceptoPadre = concepto.IdConceptoPadre,
                             Descripcion = concepto.Descripcion,
                             Nivel = concepto.Nivel,
                             OrdenVisual = concepto.OrdenVisual,
                             IdEstado = concepto.IdEstado,
                             Estado = concepto.IdEstado == Activo ? "Activo" : "Inactivo",
                             IdUsuarioCreacion = concepto.IdUsuarioCreacion,
                             FechaCreacion = concepto.FechaCreacion,
                             IdUsuarioEdicion = concepto.IdUsuarioEdicion,
                             FechaEdicion = concepto.FechaEdicion
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);


            var datosCarga = new ConceptoSeguimientoPaginadoDtoResponse
            {
                ListConceptoSeguimientoDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public List<ListaDtoResponse> ListarComboConceptoSeguimiento()
        {
            var query = (from bit in context.ConceptoSeguimiento.Where(x => x.IdEstado == Activo) select new ListaDtoResponse { Codigo = bit.IdConcepto.ToString(), Descripcion = bit.Descripcion });
            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }

        public List<ListaDtoResponse> ListarComboConceptoSeguimientoAgrupador()
        {

            var lista = context.ExecuteQuery<ListaDtoResponse>("[TRAZABILIDAD].[USP_ListarComboConceptoSeguimientoAgrupador]").ToList<ListaDtoResponse>();

            return lista;
        }

        public List<ListaDtoResponse> ListarComboConceptoSeguimientoNiveles()
        {
            var query = (from bit in context.ConceptoSeguimiento.Where(x => x.IdEstado == Activo) select new ListaDtoResponse { Codigo = bit.Nivel.ToString(), Descripcion = bit.Nivel.ToString() }).Distinct();
            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
