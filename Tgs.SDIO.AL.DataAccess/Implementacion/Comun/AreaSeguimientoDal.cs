﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class AreaSeguimientoDal : Repository<AreaSeguimiento>, IAreaSeguimientoDal
    {
        readonly DioContext context;
        public AreaSeguimientoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public AreaSeguimientoPaginadoDtoResponse ListadoAreasSeguimientoPaginado(AreaSeguimientoDtoRequest request)
        {
            var query = (from area in context.Set<AreaSeguimiento>()
                         join areasegmento in context.Set<AreaSegmentoNegocio>() on area.IdAreaSeguimiento equals areasegmento.IdAreaSeguimiento
                         join segmento in context.Set<SegmentoNegocio>() on areasegmento.IdSegmentoNegocio equals segmento.IdSegmentoNegocio
                         where ((segmento.Descripcion.Contains(request.DescripcionSegmento) || string.IsNullOrEmpty(request.DescripcionSegmento))
                         && (area.Descripcion.Contains(request.Descripcion) || string.IsNullOrEmpty(request.Descripcion))
                         && segmento.IdEstado ==  Activo
                         && areasegmento.IdEstado ==  Activo
                         && (area.IdEstado ==  Activo || area.IdEstado ==  Inactivo))
                         orderby area.FechaCreacion descending
                         select new AreaSeguimientoDtoResponse
                         {
                             IdAreaSeguimiento = area.IdAreaSeguimiento,
                             Descripcion = area.Descripcion,
                             OrdenVisual = area.OrdenVisual,
                             IdEstado = area.IdEstado,
                             Estado = area.IdEstado ==  Activo ? "Activo" : "Inactivo"
                         }).AsNoTracking().Distinct().OrderBy(x => x.IdAreaSeguimiento).ToPagedList(request.Indice, request.Tamanio);

        var datosCarga = new AreaSeguimientoPaginadoDtoResponse
            {
                ListAreaSeguimientoDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }



        public AreaSeguimientoDtoResponse ObtenerAreaSeguimientoPorId(AreaSeguimientoDtoRequest area) {

                var querySegmento = (from areaseg in context.Set<AreaSegmentoNegocio>()
                                     join seg in context.Set<SegmentoNegocio>() on areaseg.IdSegmentoNegocio equals seg.IdSegmentoNegocio
                                     where seg.IdEstado ==  Activo
                                     && (areaseg.IdEstado ==  Activo && areaseg.IdAreaSeguimiento == area.IdAreaSeguimiento)
                                     orderby areaseg.FechaCreacion descending
                                     select new ComboDtoResponse
                                     {
                                         id = areaseg.IdSegmentoNegocio.ToString(),
                                         label = seg.Descripcion
                                     }).AsNoTracking().Distinct().ToList();

                   var  queryArea = (from area1 in context.Set<AreaSeguimiento>()
                                     where area1.IdAreaSeguimiento == area.IdAreaSeguimiento && (area1.IdEstado ==  Activo || area1.IdEstado ==  Inactivo)
                                     select area1).AsNoTracking().FirstOrDefault();

            var objAreaSeguimiento = new AreaSeguimientoDtoResponse()
            {
                IdAreaSeguimiento = queryArea.IdAreaSeguimiento,
                Descripcion = queryArea.Descripcion,
                OrdenVisual = queryArea.OrdenVisual,
                IdEstado = queryArea.IdEstado,
                IdUsuarioCreacion = queryArea.IdUsuarioCreacion,
                FechaCreacion = queryArea.FechaCreacion,
                IdUsuarioEdicion = queryArea.IdUsuarioEdicion,
                FechaEdicion = queryArea.FechaEdicion,
                ListSegmentoNegocio = querySegmento
            };

            return objAreaSeguimiento;
        }

        public List<ListaDtoResponse> ListarComboAreaSeguimiento()
        {

            var query = (from bit in context.AreaSeguimiento.Where(x => x.IdEstado ==  Activo) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.IdAreaSeguimiento.ToString() });
            var resultado = query.ToList();
            return new List<ListaDtoResponse>(resultado);
        }

    }
    
   
    }
