﻿using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using static Tgs.SDIO.Util.Constantes.Generales.TablaMaestra;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class MaestraDal : Repository<Maestra>, IMaestraDal
    {
        readonly DioContext context;
        public MaestraDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ComboDtoResponse> ListarComboTiposRecursos()
        {
            var consulta = (from maestra in context.Set<Maestra>()
                            where (
                            maestra.IdRelacion == TipoRecursos
                            && maestra.IdEstado == Activo)
                            select new ComboDtoResponse
                            {
                                id = maestra.Valor,
                                label = maestra.Descripcion

                            }).OrderBy(x => x.label);

            return consulta.ToList();
        }
    }
}
