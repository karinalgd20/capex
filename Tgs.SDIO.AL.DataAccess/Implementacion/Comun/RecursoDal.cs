﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using Tgs.SDIO.Util.Paginacion;

using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Funciones;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
   public class RecursoDal : Repository<Recurso>, IRecursoDal
    {
        readonly DioContext context;
        public RecursoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public RecursoPaginadoDtoResponse ListadoRecursosPaginado(RecursoDtoRequest request)
        {
            var query = (from recurso in context.Set<Recurso>()
                         where ((recurso.Nombre.Contains(request.Nombre) || string.IsNullOrEmpty(request.Nombre))
                         && (recurso.IdEstado ==  Activo || recurso.IdEstado ==  Inactivo)
                         && ((request.TipoRecurso== Seleccione) || (recurso.TipoRecurso == request.TipoRecurso)))
                         orderby recurso.FechaCreacion descending
                         select new RecursoDtoResponse
                         {
                             IdRecurso = recurso.IdRecurso,
                             TipoRecurso = recurso.TipoRecurso,
                             UserNameSF = recurso.UserNameSF,
                             Nombre = recurso.Nombre,
                             DNI = recurso.DNI,
                             IdEstado = recurso.IdEstado,
                             Estado = recurso.IdEstado ==  Activo ? "Activo" : "Inactivo",
                             UsuarioEdicion = (from recursoEdita in context.Set<Recurso>() where recursoEdita.IdUsuarioRais == recurso.IdUsuarioEdicion && recursoEdita.IdEstado == Activo select recursoEdita.Nombre).FirstOrDefault(),
                             FechaEdicion = recurso.FechaEdicion,
                             UsuarioCreacion = (from recursoCreacion in context.Set<Recurso>() where recursoCreacion.IdUsuarioRais == recurso.IdUsuarioCreacion && recursoCreacion.IdEstado == Activo select recursoCreacion.Nombre).FirstOrDefault(),
                             FechaCreacion = recurso.FechaCreacion
                         }).ToList()
                                        .Select(x => new RecursoDtoResponse
                                         {
                                             IdRecurso = x.IdRecurso,
                                             TipoRecurso = x.TipoRecurso,
                                             UserNameSF = x.UserNameSF,
                                             Nombre = x.Nombre,
                                             DNI = x.DNI,
                                             IdEstado = x.IdEstado,
                                             Estado = x.Estado,
                                             UsuarioEdicion = x.UsuarioEdicion,
                                             strFechaEdicion = x.FechaEdicion != null ? Funciones.FormatoFecha(x.FechaEdicion.Value) : "",
                                             UsuarioCreacion = x.UsuarioCreacion,
                                             strFechaCreacion = x.FechaCreacion != null ? Funciones.FormatoFecha(x.FechaCreacion) : ""
                                         }).Distinct().OrderBy(x => x.IdRecurso).AsQueryable().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new RecursoPaginadoDtoResponse
            {
                ListRecursoDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public RecursoPaginadoDtoResponse ListaRecursoModalRecursoPaginado(RecursoDtoRequest request)
        {
            var query = (from recurso in context.Set<Recurso>()
                         where ((recurso.Nombre.Contains(request.Nombre) || string.IsNullOrEmpty(request.Nombre))
                         && (recurso.IdEstado ==  Activo || recurso.IdEstado ==  Inactivo))
                         orderby recurso.FechaCreacion descending
                         select new RecursoDtoResponse
                         {
                             IdRecurso = recurso.IdRecurso,                          
                             Nombre = recurso.Nombre,                           
                             Estado = recurso.IdEstado ==  Activo ? "Activo" : "Inactivo"                          
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new RecursoPaginadoDtoResponse
            {
                ListRecursoDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public RecursoDtoResponse ListarRecursoDeUsuario(RecursoDtoRequest request)
        {
            var query = (from r in context.Recurso
                         where r.IdUsuarioRais == request.IdUsuarioRais
                         select new
                         {
                             r.IdRecurso
                         }).AsNoTracking().AsQueryable();

            var result = query.FirstOrDefault();

            return new RecursoDtoResponse { IdRecurso = result == null ? 0 : result.IdRecurso };

        }

        public RecursoPaginadoDtoResponse ListadoRecursosByCargoPaginado(RecursoDtoRequest request)
        {
            var query = (from recurso in context.Set<Recurso>()
                         where ((recurso.Nombre.Contains(request.Nombre) || string.IsNullOrEmpty(request.Nombre))
                             && (recurso.IdEstado == request.IdEstado)
                             && ((request.TipoRecurso == -1) || (recurso.TipoRecurso == request.TipoRecurso))
                             && ((request.IdCargo == 0) || (recurso.IdCargo == request.IdCargo))
                         )
                         orderby recurso.Nombre ascending
                         select new RecursoDtoResponse
                         {
                             IdRecurso = recurso.IdRecurso,
                             TipoRecurso = recurso.TipoRecurso,
                             UserNameSF = recurso.UserNameSF,
                             Nombre = recurso.Nombre,
                             DNI = recurso.DNI,
                             IdEstado = recurso.IdEstado,
                             Email = recurso.Email,
                             Estado = recurso.IdEstado == EstadosEntidad.Activo ? "Activo" : "Inactivo"
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new RecursoPaginadoDtoResponse
            {
                ListRecursoDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }
    }
}
