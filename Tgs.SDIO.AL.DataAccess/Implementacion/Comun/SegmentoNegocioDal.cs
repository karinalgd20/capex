﻿using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class SegmentoNegocioDal : Repository<SegmentoNegocio>, ISegmentoNegocioDal
    {
        readonly DioContext context;

        public SegmentoNegocioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }


        public List<ComboDtoResponse> ListarComboSegmentoNegocio()
        {
        
             var query = (from bit in context.SegmentoNegocio.Where(x => x.IdEstado == Activo) select new ComboDtoResponse { label=bit.Descripcion,id=bit.IdSegmentoNegocio.ToString() });

            var resultado = query.ToList();

            return new List<ComboDtoResponse>(resultado);
       }

        public List<ListaDtoResponse> ListarComboSegmentoNegocioSimple()
        {

            var query = (from bit in context.SegmentoNegocio.Where(x => x.IdEstado == Activo) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.IdSegmentoNegocio.ToString() });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }

    }
    }
