﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ServicioCMIDal : Repository<ServicioCMI>, IServicioCMIDal
    {
        readonly DioContext context;

        public ServicioCMIDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarServicioCMIPorLineaNegocio(ServicioCMIDtoRequest servicioCMI)
        {
            var query = (from servicio in context.Set<ServicioCMI>()
                         join lineaNegocioCMI in context.Set<LineaNegocioCMI>() on servicio.IdServicioCMI equals lineaNegocioCMI.IdServicioCMI
                         where servicio.IdEstado == Generales.Estados.Activo && lineaNegocioCMI.IdLineaNegocio== servicioCMI.IdLineaNegocio


                         orderby servicio.IdServicioCMI ascending
                         select new ListaDtoResponse
                         {
                             Codigo = servicio.IdServicioCMI.ToString(),
                             Descripcion = servicio.DescripcionCMI,


                         }).AsNoTracking().Distinct();


            var ListServicioDtoResponse = query.ToList();



            return ListServicioDtoResponse;
        }

    }
}
