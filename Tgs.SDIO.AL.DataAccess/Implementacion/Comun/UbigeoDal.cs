﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class UbigeoDal : Repository<Ubigeo>, IUbigeoDal
    {
        readonly DioContext context;
        public UbigeoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarComboUbigeoDepartamento()
        {
            var query = (from bit in context.Ubigeo.Where(x => x.IdEstado == Activo && x.CodigoProvincia == null) select new ListaDtoResponse { Descripcion = bit.Nombre, Codigo = bit.CodigoDepartamento });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }

        public List<ListaDtoResponse> ListarComboUbigeoProvincia(UbigeoDtoRequest ubigeo)
        {
            var query = (from bit in context.Ubigeo.Where(x => x.IdEstado == Activo && x.CodigoDepartamento == ubigeo.CodigoDepartamento && x.CodigoProvincia != null && x.CodigoDistrito == null) select new ListaDtoResponse { Descripcion = bit.Nombre, Codigo = bit.CodigoProvincia });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }

        public List<ListaDtoResponse> ListarComboUbigeoDistrito(UbigeoDtoRequest ubigeo)
        {
            var query = (from bit in context.Ubigeo.Where(x => x.IdEstado == Activo && x.CodigoProvincia == ubigeo.CodigoProvincia && x.CodigoDistrito != null) select new ListaDtoResponse { Descripcion = bit.Nombre, Codigo = bit.CodigoDistrito });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
