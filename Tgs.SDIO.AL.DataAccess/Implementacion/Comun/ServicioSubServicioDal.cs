﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using System.Collections.Generic;
using System;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{

    public class ServicioSubServicioDal : Repository<ServicioSubServicio>, IServicioSubServicioDal
    {
        readonly DioContext context;
        public ServicioSubServicioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }



        public ServicioSubServicioPaginadoDtoResponse ListaServicioSubServicioPaginado(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            var query = (from obj in context.Set<ServicioSubServicio>()

                         join subSer in context.Set<SubServicio>() on obj.IdSubServicio equals subSer.IdSubServicio
                        //--join prov in context.Set<Proveedor>() on obj.IdProveedor equals prov.IdProveedor
                         join maestra in context.Set<Maestra>().Where(x => x.IdRelacion == 153) on obj.IdTipoCosto.ToString() equals maestra.Valor
                         ////join maestra_P in context.Set<Maestra>().Where(x => x.IdRelacion == 28) on obj.IdPeriodos.ToString() equals maestra_P.Valor
                         ////join maestra_M in context.Set<Maestra>().Where(x => x.IdRelacion == 98) on obj.IdMoneda.ToString() equals maestra_M.Valor
                         join pestanaGrupo in context.Set<PestanaGrupo>() on obj.IdGrupo equals pestanaGrupo.IdGrupo

                         where
                         obj.IdServicio == servicioSubServicio.IdServicio &&
                          obj.IdEstado == Generales.Estados.Activo

                         orderby obj.FechaCreacion ascending
                         select new ServicioSubServicioDtoResponse
                         {
                             IdServicioSubServicio = obj.IdServicioSubServicio,
                             DescripcionSubServicio = subSer.Descripcion,
                             DescripcionTipoCosto = maestra.Descripcion,
                             ////DescripcionMoneda = maestra_M.Descripcion,
                             ////DescripcionPeriodo = maestra_P.Descripcion,
                             DescripcionGrupo = pestanaGrupo.Descripcion,
                            // DescripcionProveedor = prov.Descripcion,
                            // DescripcionFlagSISEGO = obj.FlagSISEGO == Generales.Numeric.Cero ? Generales.Confirmacion.si : Generales.Confirmacion.no,
                            // ContratoMarco = obj.ContratoMarco,


                             Inicio = obj.Inicio,
                             Ponderacion = obj.Ponderacion,




                         }).AsNoTracking().Distinct().OrderBy(x => x.IdServicioSubServicio).ToPagedList(servicioSubServicio.Indice, servicioSubServicio.Tamanio);




            var resultado = new ServicioSubServicioPaginadoDtoResponse
            {
                ListServicioSubServicioDtoResponse = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return resultado;
        }



        public int ObtenerIdServicioSubServicio(int idServicio)
        {
            var IdServicioSubServicio = Numeric.Cero;
            var servicioSubservicio = (from obj in context.Set<ServicioSubServicio>()

                                       join subSer in context.Set<Servicio>() on obj.IdServicio equals subSer.IdServicio
                                       where
                                       obj.IdSubServicio == null &&
                                       obj.IdServicio == idServicio
                                       select new
                                       {
                                           IdServicioSubServicio = obj.IdServicioSubServicio
                                       });

            var datos = servicioSubservicio.Any();
            if (datos)
            {
                var query = servicioSubservicio.Single();
                IdServicioSubServicio = query.IdServicioSubServicio;
            }
            
            return IdServicioSubServicio;
        }
        public ServicioSubServicioPaginadoDtoResponse ListarServicioSubServicioGrupos(ServicioSubServicioDtoRequest ServiciosubServicio)
        {
            var query = (from ssbser in context.Set<ServicioSubServicio>()
                         join s in context.Set<SubServicio>() on ssbser.IdSubServicio equals s.IdSubServicio
                         join maestra in context.Set<Maestra>().Where(x => x.IdRelacion == 153) on ssbser.IdTipoCosto.ToString() equals maestra.Valor
                         join pg in context.Set<PestanaGrupo>() on ssbser.IdGrupo equals pg.IdGrupo
                         join lp in context.Set<LineaPestana>() on pg.IdPestana equals lp.IdPestana
                         where
                             ssbser.IdServicio == ServiciosubServicio.IdServicio &&
                             ssbser.IdEstado == ServiciosubServicio.IdEstado
                             
                         select new ServicioSubServicioDtoResponse
                         {
                             IdServicio = ssbser.IdServicio,
                             IdServicioSubServicio = ssbser.IdServicioSubServicio,
                             DescripcionTipoCosto = maestra.Descripcion,
                             DescripcionSubServicio = s.Descripcion,
                             Pestana = lp.Descripcion,
                             Grupo = pg.Descripcion
                         }).AsNoTracking().Distinct().OrderBy(x => x.IdServicioSubServicio).ToPagedList(ServiciosubServicio.Indice, ServiciosubServicio.Tamanio);

            var resultado = new ServicioSubServicioPaginadoDtoResponse
            {
                ListServicioSubServicioDtoResponse = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return resultado;
        }
    }
}
