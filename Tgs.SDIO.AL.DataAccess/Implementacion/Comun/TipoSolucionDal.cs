﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.Util.Constantes;
using System;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{

    public class TipoSolucionDal : Repository<TipoSolucion>, ITipoSolucionDal
    {
        readonly DioContext context;
        public TipoSolucionDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<TipoSolucionDtoResponse> ListarTipoSolucionByProyecto(DetalleSolucionDtoRequest request)
        {
            var queryTipoSolucion = (from ts in context.Set<TipoSolucion>()
                                     join ds in context.Set<DetalleSolucion>() on ts.IdTipoSolucion equals ds.IdTipoSolucion into lds
                                     from nlds in lds.Where(x => x.IdProyecto == request.IdProyecto || request.IdProyecto == 0).DefaultIfEmpty()
                                     select new TipoSolucionDtoResponse {
                                            IdTipoSolucion = ts.IdTipoSolucion,
                                            Descripcion = ts.Descripcion,
                                            Activo = (nlds.IdProyecto == request.IdProyecto) ? true : false
                                    }).Distinct();

            return queryTipoSolucion.ToList();
        }
    }
}
