﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class CostoDal : Repository<Costo>, ICostoDal
    {
        readonly DioContext context;
        public CostoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
     
        public List<CostoDtoResponse> ListarComboCosto(CostoDtoRequest medioCosto)
        {
            var lista = (from c in context.Set<Costo>()
                         join mc in context.Set<MedioCosto>() on c.IdCosto equals mc.IdCosto
                         where c.IdEstado == medioCosto.IdEstado &&
                         mc.IdMedio == medioCosto.IdMedio &&
                         c.IdTipoCosto == medioCosto.IdTipoCosto

                         select new CostoDtoResponse
                         {
                             IdCosto = c.IdCosto,
                             IdTipoCosto = c.IdTipoCosto,
                             Descripcion = c.Descripcion,
                             Monto = c.Monto,
                             VelocidadSubidaKBPS = c.VelocidadSubidaKBPS,
                             PorcentajeGarantizado = c.PorcentajeGarantizado,
                             PorcentajeSobresuscripcion = c.PorcentajeSobresuscripcion,
                             CostoSegmentoSatelital = c.CostoSegmentoSatelital,
                             InvAntenaHubUSD = c.InvAntenaHubUSD,
                             AntenaCasaClienteUSD = c.AntenaCasaClienteUSD,
                             Instalacion = c.Instalacion,
                             IdUnidadConsumo = c.IdUnidadConsumo,
                             IdTipificacion = c.IdTipificacion,
                             CodigoModelo = c.CodigoModelo,
                             Modelo = c.Modelo
                         }).ToList();
            

            return lista;
        }
        public List<ListaDtoResponse> ListarCosto(CostoDtoRequest medioCosto)
        {
            var lista = (from c in context.Set<Costo>()
                         join mc in context.Set<MedioCosto>() on c.IdCosto equals mc.IdCosto
                         where c.IdEstado == medioCosto.IdEstado &&
                         mc.IdMedio == medioCosto.IdMedio &&
                         c.IdTipoCosto == medioCosto.IdTipoCosto

                         select new ListaDtoResponse
                         {
                             Codigo = c.IdCosto.ToString(),
                             Descripcion = c.CodigoModelo,
                       
                         }).AsNoTracking().OrderBy(x => x.Descripcion).ToList();


            return lista;
        }

       

        public  CostoDtoResponse ObtenerCostoPorCodigo(CostoDtoRequest medioCosto)
        {
            var lista = (from c in context.Set<Costo>()
                         join mc in context.Set<MedioCosto>() on c.IdCosto equals mc.IdCosto
                         where c.IdEstado == medioCosto.IdEstado &&  
                         c.CodigoModelo == medioCosto.CodigoModelo

                         select new CostoDtoResponse
                         {
                             IdCosto = c.IdCosto,
                             IdTipoCosto = c.IdTipoCosto,
                             Descripcion = c.Descripcion,
                             Monto = c.Monto,
                             VelocidadSubidaKBPS = c.VelocidadSubidaKBPS,
                             PorcentajeGarantizado = c.PorcentajeGarantizado,
                             PorcentajeSobresuscripcion = c.PorcentajeSobresuscripcion,
                             CostoSegmentoSatelital = c.CostoSegmentoSatelital,
                             InvAntenaHubUSD = c.InvAntenaHubUSD,
                             AntenaCasaClienteUSD = c.AntenaCasaClienteUSD,
                             Instalacion = c.Instalacion,
                             IdUnidadConsumo = c.IdUnidadConsumo,
                             IdTipificacion = c.IdTipificacion,
                             CodigoModelo = c.CodigoModelo,
                             Modelo = c.Modelo
                         }).Single();


            return lista;
        }
        public List<CostoDtoResponse> ListarCostoPorMedioServicioGrupo(CostoDtoRequest costo)
        {
            var lista = (from m in context.Medio
                         join s in context.Servicio on m.IdMedio equals s.IdMedio
                         join sg in context.ServicioGrupo on s.IdServicioGrupo equals sg.IdServicioGrupo
                         join mc in context.MedioCosto on m.IdMedio equals mc.IdMedio
                         join c in context.Costo on mc.IdCosto equals c.IdCosto
                         where m.IdMedio.Equals(costo.IdMedio)
                         && sg.IdServicioGrupo.Equals(costo.IdServicioGrupo)
                         && c.IdTipoCosto == costo.IdTipoCosto
                         select new CostoDtoResponse
                         {
                             IdCosto = c.IdCosto,
                             Descripcion = c.Descripcion
                         })
                         .Distinct()
                         .ToList();

            return lista;
        }
    }
}
