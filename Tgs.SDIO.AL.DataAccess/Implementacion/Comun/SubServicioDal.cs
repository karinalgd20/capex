﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{

        public class SubServicioDal : Repository<SubServicio>, ISubServicioDal
        {
            readonly DioContext context;
            public SubServicioDal(DioContext unitOfWork) : base(unitOfWork)
            {
                context = UnitOfWork as DioContext;
            }



       public SubServicioPaginadoDtoResponse ListaSubServicioPaginado(SubServicioDtoRequest subServicio)
        {
       

            if (subServicio.Descripcion!=null && subServicio.IdTipoSubServicio > 0)
            {

                var query = (from obj in context.Set<SubServicio>()

                             where
                                //obj.Descripcion==subServicio.Descripcion
                                obj.Descripcion.Contains(subServicio.Descripcion) && 
                                obj.IdTipoSubServicio==subServicio.IdTipoSubServicio
                                      && obj.IdTipoSubServicio != null

                             //&& obj.IdEstado == Generales.Estados.Activo
                             orderby obj.FechaCreacion ascending
                             select new SubServicioDtoResponse
                             {
                                 IdSubServicio = obj.IdSubServicio,
                                 Descripcion = obj.Descripcion,
                                 Orden = obj.Orden,
                                 Estado = obj.IdEstado == Generales.Estados.Activo ? Generales.EstadoDescripcion.Activo : Generales.EstadoDescripcion.Inactivo


                             }).AsNoTracking().Distinct().OrderBy(x => x.IdSubServicio).ToPagedList(subServicio.Indice, subServicio.Tamanio);

                var resultado = new SubServicioPaginadoDtoResponse
                {
                    ListSubServicioDtoResponse = query.ToList(),
                    TotalItemCount = query.TotalItemCount
                };


                return resultado;
            }

            else
            {
                var query = (from obj in context.Set<SubServicio>()

                             where


                           (obj.Descripcion.Contains(subServicio.Descripcion) || string.IsNullOrEmpty(subServicio.Descripcion)
                          && obj.IdTipoSubServicio == (subServicio.IdTipoSubServicio != Generales.Numeric.NegativoUno ? subServicio.IdTipoSubServicio : obj.IdTipoSubServicio))
                           && obj.IdTipoSubServicio!=null

                             //&& obj.IdEstado == Generales.Estados.Activo
                             orderby obj.FechaCreacion ascending
                             select new SubServicioDtoResponse
                             {
                                 IdSubServicio = obj.IdSubServicio,
                                 Descripcion = obj.Descripcion,
                                 Orden = obj.Orden,
                                 Estado = obj.IdEstado == Generales.Estados.Activo ? Generales.EstadoDescripcion.Activo : Generales.EstadoDescripcion.Inactivo


                             }).AsNoTracking().Distinct().OrderBy(x => x.IdSubServicio).ToPagedList(subServicio.Indice, subServicio.Tamanio);

                var resultado = new SubServicioPaginadoDtoResponse
                {
                    ListSubServicioDtoResponse = query.ToList(),
                    TotalItemCount = query.TotalItemCount
                };

                return resultado;

            }
      
        }

    }
}
