﻿using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ActividadSegmentoNegocioDal : Repository<ActividadSegmentoNegocio>, IActividadSegmentoNegocioDal
    {
        readonly DioContext context;

        public ActividadSegmentoNegocioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
    }
}
