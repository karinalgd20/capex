﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class AreaDal : Repository<Area>, IAreaDal
    {
        private readonly DioContext context;
        public AreaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarComboArea()
        {
            var query = (from bit in context.Area.Where(x => x.IdEstado == Activo) select new ListaDtoResponse { Codigo = bit.IdArea.ToString(), Descripcion = bit.Descripcion });
            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
