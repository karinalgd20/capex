﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Tgs.SDIO.Util.Paginacion;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ContratoMarcoDal : Repository<ContratoMarco>, IContratoMarcoDal
    {
        readonly DioContext context;

        public ContratoMarcoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public ContratoMarcoDtoResponsePaginado ListarContratoMarcoPaginado(ContratoMarcoDtoRequest contratomarco)
        {
            DateTime dFechaActual = Convert.ToDateTime(DateTime.Now.ToShortDateString());

            PagedList<ContratoMarcoDtoResponse> ListaContratoMarco = (from cm in context.Set<ContratoMarco>()
                                                                      from p in context.Set<Proveedor>().Where(p => cm.IdProveedor == p.IdProveedor).DefaultIfEmpty()
                                                                      where
                                                                    (contratomarco.Descripcion == null || cm.Descripcion.Contains(contratomarco.Descripcion)) &&
                                                                    (contratomarco.NumContrato == null || cm.NumContrato.Contains(contratomarco.NumContrato)) &&
                                                                    (contratomarco.Proveedor == null || p.RazonSocial.Contains(contratomarco.Proveedor)) &&
                                                                    (cm.FechaInicio <= dFechaActual && dFechaActual <= cm.FechaFin )
                                                                      orderby cm.Descripcion ascending
                                                                      select new { cm, p }).ToList().Select(cm =>
                                                                     new ContratoMarcoDtoResponse
                                                                     {
                                                                         IdContratoMarco = cm.cm.IdContratoMarco,
                                                                         NumContrato = cm.cm.NumContrato,
                                                                         Descripcion = cm.cm.Descripcion,
                                                                         ProcesoAdjudicacion = cm.cm.ProcesoAdjudicacion,
                                                                         FechaInicio = cm.cm.FechaInicio,
                                                                         FechaFin = cm.cm.FechaFin,
                                                                         Catalogado = cm.cm.Catalogado,
                                                                         ImporteContrato = cm.cm.ImporteContrato,
                                                                         ImporteConsumido = cm.cm.ImporteConsumido,
                                                                         Moneda = cm.cm.Moneda,
                                                                         ProductoGerencia = cm.cm.ProductoGerencia,
                                                                         IdProveedor = cm.cm.IdProveedor,
                                                                         RazonSocialProveedor = cm.p.RazonSocial,
                                                                         RucProveedor = cm.p.RUC
                                                                     }).AsQueryable().ToPagedList(contratomarco.Indice, contratomarco.Tamanio);

            ContratoMarcoDtoResponsePaginado oContratoMarcoPaginado = new ContratoMarcoDtoResponsePaginado
            {
                ListaContratoMarco = ListaContratoMarco.ToList(),
                TotalItemCount = ListaContratoMarco.TotalItemCount
            };

            return oContratoMarcoPaginado;
        }

        public ContratoMarcoDtoResponse ObtenerContratoMarcoPorId(ContratoMarcoDtoRequest request)
        {

            ContratoMarcoDtoResponse oContratoMarco = (from cm in context.Set<ContratoMarco>()
                                                      from p in context.Set<Proveedor>().Where(p => cm.IdProveedor == p.IdProveedor).DefaultIfEmpty()
                                                      where (cm.IdContratoMarco == request.IdContratoMarco)
                                                      select new { cm, p }).ToList().Select(cm =>
                                                        new ContratoMarcoDtoResponse
                                                        {
                                                            IdContratoMarco = cm.cm.IdContratoMarco,
                                                            NumContrato = cm.cm.NumContrato,
                                                            Descripcion = cm.cm.Descripcion,
                                                            ProcesoAdjudicacion = cm.cm.ProcesoAdjudicacion,
                                                            FechaInicio = cm.cm.FechaInicio,
                                                            FechaFin = cm.cm.FechaFin,
                                                            Catalogado = cm.cm.Catalogado,
                                                            ImporteContrato = cm.cm.ImporteContrato,
                                                            ImporteConsumido = cm.cm.ImporteConsumido,
                                                            Moneda = cm.cm.Moneda,
                                                            ProductoGerencia = cm.cm.ProductoGerencia,
                                                            IdProveedor = cm.cm.IdProveedor,
                                                            RazonSocialProveedor = cm.p.RazonSocial,
                                                            RucProveedor = cm.p.RUC
                                                        }).SingleOrDefault();

            return oContratoMarco;
        }

    }
}
