﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.Entities.Entities.CartaFianza;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ClienteDal : Repository<Cliente>, IClienteDal
    {
        readonly DioContext context;

        public ClienteDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }


        public List<ClienteDtoResponse> ListarCliente(ClienteDtoRequest cliente)
        {

            var Listcliente = (from c in context.Set<Cliente>()

                               where
                            ((c.NumeroIdentificadorFiscal.Contains(cliente.NumeroIdentificadorFiscal) || cliente.NumeroIdentificadorFiscal == null)) &&
                                                  c.IdEstado == cliente.IdEstado &&
                                                  ((cliente.Descripcion == null) || (c.Descripcion.Contains(cliente.Descripcion)))
                               select new ClienteDtoResponse
                               {
                                   IdCliente = c.IdCliente,
                                   CodigoCliente = c.CodigoCliente,
                                   Descripcion = c.Descripcion,
                                   IdEstado = c.IdEstado,
                                   IdSector = c.IdSector,
                                   GerenteComercial = c.GerenteComercial,
                                   IdDireccionComercial = c.IdDireccionComercial,
                                   NumeroIdentificadorFiscal = c.NumeroIdentificadorFiscal,
                                   IdTipoIdentificadorFiscalTm = c.IdTipoIdentificadorFiscalTm
                               }).ToList<ClienteDtoResponse>();

            return Listcliente;

        }
        public List<ClienteDtoResponse> ListarClientePorDescripcion(ClienteDtoRequest cliente)
        {

            var Listcliente = (from c in context.Set<Cliente>()
                               where c.IdEstado == cliente.IdEstado && (cliente.Descripcion == null || c.Descripcion.Contains(cliente.Descripcion))
                               select new ClienteDtoResponse
                               {
                                   IdCliente = c.IdCliente,
                                   CodigoCliente = c.CodigoCliente,
                                   Descripcion = c.Descripcion,
                                   IdEstado = c.IdEstado,
                                   IdSector = c.IdSector,
                                   GerenteComercial = c.GerenteComercial,
                                   IdDireccionComercial = c.IdDireccionComercial,
                                   NumeroIdentificadorFiscal = c.NumeroIdentificadorFiscal,
                                   IdTipoIdentificadorFiscalTm = c.IdTipoIdentificadorFiscalTm
                               }).ToList<ClienteDtoResponse>();

            return Listcliente;

        }

        public List<ListaDtoResponse> ListarClienteCartaFianza(ClienteDtoRequest cliente)
        {
            var Listcliente = (from c in context.Set<Cliente>()
                               join cf in context.Set<CartaFianzaMaestro>() on c.IdCliente equals cf.IdCliente

                               where c.IdEstado == cliente.IdEstado

                               orderby c.Descripcion ascending

                               select new ListaDtoResponse
                               {
                                   Codigo = c.IdCliente.ToString(),
                                   Descripcion = c.Descripcion,
                               }).Distinct().ToList();
            return Listcliente;
        }

        public ClienteDtoResponsePaginado ListarClientePaginado(ClienteDtoRequest cliente)
        {
            var propertyInfo = typeof(ClienteDtoResponse).GetProperty(cliente.ColumnName);

            var Listcliente = (from c in context.Set<Cliente>()

                               where c.IdEstado == cliente.IdEstado &&
                               (cliente.Descripcion == null || c.Descripcion.Contains(cliente.Descripcion)) &&
                               (cliente.GerenteComercial == null || c.GerenteComercial.Contains(cliente.GerenteComercial)) &&
                               (cliente.NumeroIdentificadorFiscal == null || c.NumeroIdentificadorFiscal.Contains(cliente.NumeroIdentificadorFiscal)) &&
                               (cliente.IdDireccionComercial == null || cliente.IdDireccionComercial == -1 || cliente.IdDireccionComercial == 0 || c.IdDireccionComercial == cliente.IdDireccionComercial) &&
                               (cliente.IdSector == null ||  cliente.IdSector == -1 || cliente.IdSector == 0 || c.IdSector == cliente.IdSector) &&
                               (cliente.IdTipoIdentificadorFiscalTm == null || cliente.IdTipoIdentificadorFiscalTm == -1 || cliente.IdTipoIdentificadorFiscalTm == 0 || c.IdTipoIdentificadorFiscalTm == cliente.IdTipoIdentificadorFiscalTm) &&
                               (cliente.IdTipoEntidad == null || cliente.IdTipoEntidad == -1 || cliente.IdTipoEntidad == 0 || c.IdTipoEntidad == cliente.IdTipoEntidad)

                               select new ClienteDtoResponse
                               {
                                   IdCliente = c.IdCliente,
                                   CodigoCliente = c.CodigoCliente,
                                   Descripcion = c.Descripcion,
                                   IdEstado = c.IdEstado,
                                   IdSector = (c.IdSector.ToString() != "") ? c.IdSector : -1,
                                   GerenteComercial = c.GerenteComercial,
                                   IdDireccionComercial = (c.IdDireccionComercial.ToString() != "") ? c.IdDireccionComercial : -1,
                                   NumeroIdentificadorFiscal = c.NumeroIdentificadorFiscal,
                                   IdTipoIdentificadorFiscalTm = c.IdTipoIdentificadorFiscalTm,
                                   Email = c.Email,
                                   Direccion = c.Direccion,
                                   IdTipoEntidad = (c.IdTipoEntidad != null) ? c.IdTipoEntidad : -1
                               }).ToList()
                               .Select(
                                        x => new ClienteDtoResponse
                                        {
                                            IdCliente = x.IdCliente,
                                            CodigoCliente = x.CodigoCliente,
                                            Descripcion = x.Descripcion,
                                            IdEstado = x.IdEstado,
                                            IdSector = x.IdSector,
                                            GerenteComercial = x.GerenteComercial,
                                            IdDireccionComercial = x.IdDireccionComercial,
                                            NumeroIdentificadorFiscal = x.NumeroIdentificadorFiscal,
                                            IdTipoIdentificadorFiscalTm = x.IdTipoIdentificadorFiscalTm,
                                            Email = x.Email,
                                            Direccion = x.Direccion,
                                            IdTipoEntidad = x.IdTipoEntidad
                                        }
                           );

            var orderByListCliente = (cliente.OrderType == "asc") ? (Listcliente.OrderBy(x => propertyInfo.GetValue(x, null))) : (Listcliente.OrderByDescending(x => propertyInfo.GetValue(x, null)));
            var ListClientePagination = orderByListCliente.AsQueryable().ToPagedList(cliente.Indice, cliente.Tamanio);

            var Listclientepg = new ClienteDtoResponsePaginado
            {
                ListCliente = ListClientePagination.ToList(),
                TotalItemCount = ListClientePagination.TotalItemCount

            };
            return Listclientepg;

        }


    }
}
