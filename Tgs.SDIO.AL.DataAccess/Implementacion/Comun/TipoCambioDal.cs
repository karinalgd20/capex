﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.Util.Constantes;
using System;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{

    public class TipoCambioDal : Repository<TipoCambio>, ITipoCambioDal
    {
        readonly DioContext context;
        public TipoCambioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public TipoCambioPaginadoDtoResponse ListaTipoCambioPaginado(TipoCambioDtoRequest tipoCambio)
        {
            var query = (from tc in context.Set<TipoCambio>()

                         join tcd in context.Set<TipoCambioDetalle>() on tc.IdTipoCambio equals tcd.IdTipoCambio
                         join tmm in context.Set<Maestra>() on Generales.TablaMaestra.TipoMoneda equals tmm.IdRelacion

                         where
                          tc.IdLineaNegocio == tipoCambio.IdLineaNegocio &&
                          tc.IdEstado == Generales.Estados.Activo &&
                          tmm.Valor==tipoCambio.IdMoneda.ToString()
                         orderby tcd.FechaCreacion ascending
                         select new TipoCambioDtoResponse
                         {
                             IdTipoCambioDetalle=tcd.IdTipoCambioDetalle,
                            // Tipificacion=tc.Tipificacion
                             Moneda= tmm.Descripcion,
                             Anio = tcd.Anio,
                             Monto=tcd.Monto

                         }).AsNoTracking().Distinct().OrderBy(x => x.IdTipoCambio).ToPagedList(tipoCambio.Indice, tipoCambio.Tamanio);




            var resultado = new TipoCambioPaginadoDtoResponse
            {
                ListTipoCambioDtoResponse = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return resultado;
        }

    }
}
