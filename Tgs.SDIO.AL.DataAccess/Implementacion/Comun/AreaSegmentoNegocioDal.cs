﻿using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
   public  class AreaSegmentoNegocioDal : Repository<AreaSegmentoNegocio>, IAreaSegmentoNegocioDal
    {
        readonly DioContext context;

        public AreaSegmentoNegocioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
    }
}
