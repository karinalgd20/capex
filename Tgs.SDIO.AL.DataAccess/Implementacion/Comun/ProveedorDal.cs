﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ProveedorDal : Repository<Proveedor>, IProveedorDal
    {
        readonly DioContext context;

        public ProveedorDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ProveedorDtoResponse> ListarProveedores(ProveedorDtoRequest objProveedor)
        {
            var query = (from p in context.Proveedor
                         where p.IdEstado == Generales.Estados.Activo
                         select new ProveedorDtoResponse
                         {
                             IdProveedor = p.IdProveedor,
                             Descripcion = p.Descripcion,
                             TipoProveedor = p.TipoProveedor,
                             RazonSocial = p.RazonSocial,
                             CodigoProveedor = p.CodigoProveedor,
                             RUC = p.RUC,
                             Pais = p.Pais,
                             IdSecuencia = p.IdSecuencia,
                             NombrePersonaContacto = p.NombrePersonaContacto,
                             TelefonoContactoPrincipal = p.TelefonoContactoPrincipal,
                             CorreoContactoPrincipal = p.CorreoContactoPrincipal,
                             TelefonoContactoSecundario = p.TelefonoContactoSecundario,
                             CorreoContactoSecundario = p.CorreoContactoSecundario,
                             CodigoSRM = p.CodigoSRM

                         }).ToList();
            return query;
        }
        public ProveedorPaginadoDtoResponse ListarProveedorPaginado(ProveedorDtoRequest objProveedor)
        {
            var query = (from p in context.Proveedor
                         where p.IdEstado == Generales.Estados.Activo
                         orderby p.FechaCreacion descending
                         select new ProveedorDtoResponse
                         {
                             IdProveedor = p.IdProveedor,
                             Descripcion = p.Descripcion,
                             TipoProveedor = p.TipoProveedor,
                             RazonSocial = p.RazonSocial,
                             CodigoProveedor = p.CodigoProveedor,
                             RUC = p.RUC,
                             Pais = p.Pais,
                             IdSecuencia = p.IdSecuencia,
                             NombrePersonaContacto = p.NombrePersonaContacto,
                             TelefonoContactoPrincipal = p.TelefonoContactoPrincipal,
                             CorreoContactoPrincipal = p.CorreoContactoPrincipal,
                             TelefonoContactoSecundario = p.TelefonoContactoSecundario,
                             CorreoContactoSecundario = p.CorreoContactoSecundario,
                             CodigoSRM = p.CodigoSRM,

                         }).AsNoTracking()
                         .Distinct()
                         .OrderBy(x => x.Descripcion)
                         .ToPagedList(objProveedor.Indice, objProveedor.Tamanio);

            var resultado = new ProveedorPaginadoDtoResponse
            {
                ListaProveedorPaginadoDtoResponse = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return resultado;
        }

    }
}
