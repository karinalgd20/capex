﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.Util.Constantes;
using System;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{

    public class TipoCambioDetalleDal : Repository<TipoCambioDetalle>, ITipoCambioDetalleDal
    {
        readonly DioContext context;
        public TipoCambioDetalleDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
                                
    }
}
