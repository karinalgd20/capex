﻿using System.Data.Entity;
using System.Linq;

using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Generales.TablaMaestra;
using Tgs.SDIO.Util.Funciones;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class RiesgoProyectoDal : Repository<RiesgoProyecto>, IRiesgoProyectoDal
    {
        readonly DioContext context;
        public RiesgoProyectoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public RiesgoProyectoPaginadoDtoResponse ListadoRiesgoProyectosPaginado(RiesgoProyectoDtoRequest request)
        {
                var query = (from RiesgoProyecto in context.Set<RiesgoProyecto>()
                             join maestraProbabilidad in context.Set<Maestra>().Where(x => x.IdRelacion == TipoProbabilidad) on RiesgoProyecto.IdProbabilidad.ToString() equals maestraProbabilidad.Valor
                             join maestraImpacto in context.Set<Maestra>().Where(x => x.IdRelacion == TipoImpacto) on RiesgoProyecto.IdImpacto.ToString() equals maestraImpacto.Valor
                             join maestraEstado in context.Set<Maestra>().Where(x => x.IdRelacion == TipoEstado) on RiesgoProyecto.IdEstado.ToString() equals maestraEstado.Valor
                             join maestraTipoRiesgo in context.Set<Maestra>().Where(x => x.IdRelacion == TipoRiesgo) on RiesgoProyecto.IdTipo.ToString() equals maestraTipoRiesgo.Valor
                             where
                             (RiesgoProyecto.IdOportunidad == request.IdOportunidad)
                             && (request.IdTipo.Equals(-1) || RiesgoProyecto.IdTipo == request.IdTipo)
                             && (request.IdEstado == -1 || RiesgoProyecto.IdEstado == request.IdEstado)
                             orderby RiesgoProyecto.FechaCreacion descending
                             select new RiesgoProyectoDtoResponse
                             {
                                 Id = RiesgoProyecto.Id,
                                 DescripcionTipoRiesgo = maestraTipoRiesgo.Descripcion,
                                 FechaDeteccion = RiesgoProyecto.FechaDeteccion.ToString(),
                                 Descripcion = RiesgoProyecto.Descripcion,
                                 Consecuencias = RiesgoProyecto.Consecuencias,
                                 DescripcionProbabilidad = maestraProbabilidad.Descripcion,
                                 DescripcionImpacto = maestraImpacto.Descripcion,
                                 Responsable = RiesgoProyecto.Responsable,
                                 PlanAccion = RiesgoProyecto.PlanAccion,
                                 Estado = maestraEstado.Descripcion,
                                 //FechaCierre = RiesgoProyecto.FechaCierre.HasValue ? RiesgoProyecto.FechaCierre.Value : "",
                                 IdNivel = RiesgoProyecto.IdNivel
                             }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

                var datosCarga = new RiesgoProyectoPaginadoDtoResponse
                {
                    ListRiesgoProyecto = query.ToList(),
                    TotalItemCount = query.TotalItemCount
                };
                return datosCarga;
        }
    }
}