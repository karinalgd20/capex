﻿using System.Data.Entity;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ArchivoDal : Repository<Archivo>, IArchivoDal
    {
        readonly DioContext context;
        public ArchivoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        #region  - No Transaccional -

        public ArchivoPaginadoDtoResponse ListarPaginado(ArchivoDtoRequest request)
        {
            var lista = (from a in context.Archivo
                         where a.CodigoTabla.Equals(request.CodigoTabla)
                         && a.IdEntidad.Equals(request.IdEntidad)
                         && (request.IdCategoria == null || a.IdCategoria == request.IdCategoria)
                         && (a.IdEstado == 1)
                         select new ArchivoDtoResponse
                         {
                             Id = a.Id,
                             CodigoTabla = a.CodigoTabla,
                             IdEntidad = a.IdEntidad,
                             IdCategoria = a.IdCategoria,
                             Nombre = a.Nombre,
                             Descripcion = a.Descripcion,
                             Ruta = a.Ruta
                         }).AsNoTracking()
                         .Distinct()
                         .OrderBy(x => x.Nombre)
                         .ToPagedList(request.Indice, request.Tamanio);

            return new ArchivoPaginadoDtoResponse
            {
                ListaArchivos = lista.ToList(),
                TotalItemCount = lista.TotalItemCount
            };
        }

        #endregion
    }
}