﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using static Tgs.SDIO.Util.Constantes.Generales.TipoEquipo;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class EquipoTrabajoDal : Repository<EquipoTrabajo>, IEquipoTrabajoDal
    {
        readonly DioContext context;
        public EquipoTrabajoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarComboEquipoTrabajoAreas()
        {

            var query = (from bit in context.EquipoTrabajo.Where(x => x.IdEstado == Activo && x.IdTipoEquipo == areas) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.IdEquipoTrabajo.ToString() });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
