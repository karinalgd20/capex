﻿using System.Linq;
using System.Data.Entity;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun; 
using Tgs.SDIO.Util.Paginacion; 
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class RecursoJefeDal : Repository<RecursoJefe>, IRecursoJefeDal
    {
        private readonly DioContext context;

        public RecursoJefeDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public RecursoJefePaginadoDtoResponse ListarRecursoPorJefePaginado(RecursoJefeDtoRequest recursoJefeRequest)
        {
            var consultaDetalleLinea = (from recursoJefe in context.Set<RecursoJefe>()
                                        join recurso in context.Set<Recurso>() on recursoJefe.IdRecurso equals recurso.IdRecurso
                                        join jefe in context.Set<Recurso>() on recursoJefe.IdJefe equals jefe.IdRecurso
                                        where
                                        recursoJefe.IdJefe.Equals(recursoJefeRequest.IdJefe)
                                        orderby recurso.FechaCreacion descending
                                        select new RecursoJefeDtoResponse
                                        {
                                            IdRecursoJefe = recursoJefe.Id,
                                            Estado = recursoJefe.IdEstado == EstadosEntidad.Activo ? EstadoDescripcion.Activo : EstadoDescripcion.Inactivo,
                                            NombreRecurso = recurso.Nombre,
                                            IdEstado = recursoJefe.IdEstado
                                        }).AsNoTracking().ToPagedList(recursoJefeRequest.Indice, recursoJefeRequest.Tamanio);

            var datosRecursoLinea = new RecursoJefePaginadoDtoResponse
            {
                ListaRecursoJefeDtoResponse = consultaDetalleLinea.ToList(),
                TotalItemCount = consultaDetalleLinea.TotalItemCount
            };

            return datosRecursoLinea;
        }

    }
}
