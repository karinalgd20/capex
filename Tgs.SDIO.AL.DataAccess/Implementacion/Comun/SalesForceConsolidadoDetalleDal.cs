﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using System.Data.Entity;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class SalesForceConsolidadoDetalleDal : Repository<SalesForceConsolidadoDetalle>, ISalesForceConsolidadoDetalleDal
    {
        readonly DioContext context;
        public SalesForceConsolidadoDetalleDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

 
        public SalesForceConsolidadoDetalleDtoResponse ObtenerSalesForceConsolidadoDetalle(SalesForceConsolidadoDetalleDtoRequest salesForceConsolidadoDetalle)
        {


                var resultado = (from obj in context.Set<SalesForceConsolidadoDetalle>()

                             where
                                obj.IdOportunidad.Contains(salesForceConsolidadoDetalle.IdOportunidad)
                                   &&   obj.Estado == "En Curso"
                             select new SalesForceConsolidadoDetalleDtoResponse
                             {
                                    IdCliente = obj.IdCliente,
                                    TipologiaOportunidad=obj.TipologiaOportunidad,
                                    NombreOportunidad=obj.NombreOportunidad ,
                                    DuracionContrato=obj.DuracionContrato  ,
                                    PropietarioOportunidad = obj.PropietarioOportunidad,

                             }).AsNoTracking().FirstOrDefault();

            resultado.EstadoAccion = false;
            if (resultado.Etapa == 132 || resultado.Etapa == 133)
            {
                resultado.EstadoAccion = true;


            }
          

            return resultado;
        }

        public List<ListaDtoResponse> ListaNumerodeCasoPorNumeroSalesForceDetalle(SalesForceConsolidadoDetalleDtoRequest salesForceConsolidadoDetalle)
        {


            var resultado = (from obj in context.Set<SalesForceConsolidadoDetalle>()

                             where
                                // obj.IdOportunidad.Contains(salesForceConsolidadoCabecera.IdOportunidad) 
                                obj.IdOportunidad == salesForceConsolidadoDetalle.IdOportunidad    &&
                                obj.Estado=="En Curso"
                             select new ListaDtoResponse
                             {
                                 Codigo = obj.NumeroDelCaso,
                                 Descripcion = obj.NumeroDelCaso

                             }).AsNoTracking().OrderBy(x => x.Codigo).ToList();

            return resultado;
        }




    }
}