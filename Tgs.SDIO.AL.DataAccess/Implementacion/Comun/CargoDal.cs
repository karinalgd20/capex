﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;
using System.Linq;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class CargoDal : Repository<Cargo>, ICargoDal
    {
        private readonly DioContext context;
        public CargoDal(DioContext unitOfWork) : base(unitOfWork)
        {
           context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarComboCargo()
        {

            var query = (from bit in context.Cargo.Where(x => x.IdEstado == Activo) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.Id.ToString() });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }

    }
}
