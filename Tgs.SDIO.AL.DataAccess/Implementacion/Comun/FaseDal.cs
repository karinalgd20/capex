﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class FaseDal : Repository<Fase>, IFaseDal
    {
        readonly DioContext context;
        public FaseDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarComboFase()
        {

            var query = (from bit in context.Fase.Where(x => x.IdEstado == Activo) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.IdFase.ToString() });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
