﻿using System.Linq;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class EmpresaDal : Repository<Empresa>, IEmpresaDal
    {
        readonly DioContext context;
        public EmpresaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarComboEmpresa()
        {
            var query = (from bit in context.Empresa.Where(x => x.IdEstado == Activo) select new ListaDtoResponse { Codigo = bit.IdEmpresa.ToString(), Descripcion = bit.RazonSocial });
            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
