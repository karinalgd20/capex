﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.Util.Constantes;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{

    public class ServicioDal : Repository<Servicio>, IServicioDal
    {
        readonly DioContext context;
        public ServicioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public ServicioPaginadoDtoResponse ListaServicioPaginado(ServicioDtoRequest Servicio)
        {
            if(Servicio.IdLineaNegocio>0 && Servicio.Descripcion!=null)
            {

                var query = (from obj in context.Set<Servicio>()

                             where
                         (obj.Descripcion.Contains(Servicio.Descripcion)
                         && obj.IdLineaNegocio==Servicio.IdLineaNegocio)
                           //&& obj.IdEstado == Generales.Estados.Activo)
                             orderby obj.FechaCreacion ascending
                             select new ServicioDtoResponse
                             {
                                 IdServicio = obj.IdServicio,
                                 Descripcion = obj.Descripcion,
                                 Estado = obj.IdEstado == Generales.Estados.Activo ? Generales.EstadoDescripcion.Activo : Generales.EstadoDescripcion.Inactivo

                             }).AsNoTracking().Distinct().OrderBy(x => x.IdServicio).ToPagedList(Servicio.Indice, Servicio.Tamanio);

                var resultado = new ServicioPaginadoDtoResponse
                {
                    ListServicioDtoResponse = query.ToList(),
                    TotalItemCount = query.TotalItemCount
                };

                return resultado;

            }
            else
            {
                var query = (from obj in context.Set<Servicio>()

                             where
                         (obj.Descripcion.Contains(Servicio.Descripcion) || string.IsNullOrEmpty(Servicio.Descripcion)
                           && obj.IdLineaNegocio ==( Servicio.IdLineaNegocio != Generales.Numeric.NegativoUno ? Servicio.IdLineaNegocio : obj.IdLineaNegocio))
                           //&& obj.IdEstado == Generales.Estados.Activo)
                             orderby obj.FechaCreacion ascending
                             select new ServicioDtoResponse
                             {
                                 IdServicio = obj.IdServicio,
                                 Descripcion = obj.Descripcion,
                                 Estado = obj.IdEstado == Generales.Estados.Activo ? Generales.EstadoDescripcion.Activo : Generales.EstadoDescripcion.Inactivo

                             }).AsNoTracking().Distinct().OrderBy(x => x.IdServicio).ToPagedList(Servicio.Indice, Servicio.Tamanio);

                var resultado = new ServicioPaginadoDtoResponse
                {
                    ListServicioDtoResponse = query.ToList(),
                    TotalItemCount = query.TotalItemCount
                };

                return resultado;

            }


            
        }
  
    }
}
