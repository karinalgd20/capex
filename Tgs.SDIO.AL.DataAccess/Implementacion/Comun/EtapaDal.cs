﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class EtapaDal : Repository<Etapa>, IEtapaDal
    {
        readonly DioContext context;
        public EtapaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarComboEtapaOportunidad()
        {

            var query = (from bit in context.EtapaOportunidad.Where(x => x.IdEstado ==  Activo && (x.IdFase == 1 || x.IdFase == 2|| x.IdFase == 3)) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.IdEtapa.ToString() });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }

        public List<ListaDtoResponse> ListarComboEtapaOportunidadPorIdFase(FaseDtoRequest request)
        {

            var query = (from bit in context.EtapaOportunidad.Where(x => x.IdEstado ==  Activo && x.IdFase == request.IdFase) select new ListaDtoResponse { Codigo = bit.IdEtapa.ToString(), Descripcion = bit.Descripcion });
            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
