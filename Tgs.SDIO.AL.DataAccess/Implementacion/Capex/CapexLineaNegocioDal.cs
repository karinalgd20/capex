﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Entities.Entities.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.Util.Constantes;
using System.Data.Entity;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Capex
{
    public class CapexLineaNegocioDal : Repository<CapexLineaNegocio>, ICapexLineaNegocioDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();

        public CapexLineaNegocioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarCapexLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocio)
        {
            var query = (from cl in context.Set<CapexLineaNegocio>()
                         join ts in context.Set<TipoSolucion>() on cl.IdLineaNegocio equals ts.IdTipoSolucion
                         where cl.IdSolicitudCapex == capexLineaNegocio.IdSolicitudCapex &&
                               cl.IdEstado == capexLineaNegocio.IdEstado
                         select new ListaDtoResponse()
                         {
                             Codigo = cl.IdCapexLineaNegocio.ToString(),
                             Descripcion = ts.Descripcion
                         }).ToList<ListaDtoResponse>();

            return query;
        }

        public CapexLineaNegocioPaginadoDtoResponse ListarLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocio)
        {
            var query = (from cl in context.Set<CapexLineaNegocio>()
                         join ts in context.Set<TipoSolucion>() on cl.IdLineaNegocio equals ts.IdTipoSolucion
                         where cl.IdSolicitudCapex == capexLineaNegocio.IdSolicitudCapex &&
                               cl.IdEstado == capexLineaNegocio.IdEstado
                         select new CapexLineaNegocioDtoResponse()
                         {
                             IdCapexLineaNegocio = cl.IdCapexLineaNegocio,
                             IdLineaNegocio = cl.IdLineaNegocio,
                             Fc = cl.Fc,
                             Van = cl.Van,
                             PayBack = cl.PayBack,
                             LineaNegocio = ts.Descripcion
                         }).AsNoTracking().Distinct().OrderBy(x => x.IdCapexLineaNegocio).ToPagedList(capexLineaNegocio.Indice, capexLineaNegocio.Tamanio);

            var resultado = new CapexLineaNegocioPaginadoDtoResponse
            {
                ListCapexLineaNegocioDtoResponse = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return resultado;
        }
    }
}