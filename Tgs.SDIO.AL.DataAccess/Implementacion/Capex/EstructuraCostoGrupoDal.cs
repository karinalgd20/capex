﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Capex
{
    public class EstructuraCostoGrupoDal : Repository<EstructuraCostoGrupo>, IEstructuraCostoGrupoDal
    {
        readonly DioContext context;
        public EstructuraCostoGrupoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public EstructuraCostoGrupoPaginadoDtoResponse ListaEstructuraCostoGrupoPaginado(EstructuraCostoGrupoDtoRequest estructuraCosto)
        {

            var query = ( from cln in context.Set<CapexLineaNegocio>()
                         join ecg in context.Set<EstructuraCostoGrupo>() on cln.IdCapexLineaNegocio equals ecg.IdCapexLineaNegocio
                         join g in context.Set<Grupo>() on ecg.IdGrupo equals g.IdGrupo
                         join ecgd in context.Set<EstructuraCostoGrupoDetalle>() on ecg.IdEstructuraCostoGrupo equals ecgd.IdEstructuraCostoGrupo
                         join u in context.Set<Ubigeo>() on ecgd.IdUbigeo equals u.CodigoDistrito
                         into uEmpty
                         from u in uEmpty.DefaultIfEmpty()
                         join cc in context.Set<ConceptosCapex>() on ecgd.IdConcepto equals cc.IdConceptosCapex
                         into ccEmpty
                         from cc in ccEmpty.DefaultIfEmpty()
                         join sg in context.Set<ServicioGrupo>() on ecgd.IdServicio equals sg.IdServicioGrupo
                         into sgEmpty
                         from sg in sgEmpty.DefaultIfEmpty()

                             // IdTipoGrupo
                         where
                          cln.IdCapexLineaNegocio == estructuraCosto.IdCapexLineaNegocio &&
                          ecg.IdGrupo == estructuraCosto.IdGrupo &&
                          ecg.IdEstado == Generales.Estados.Activo


                         orderby ecg.FechaCreacion ascending
                         select new EstructuraCostoGrupoDtoResponse
                         {
                             IdEstructuraCostoGrupo = ecg.IdEstructuraCostoGrupo,
                             Grupo = g.Descripcion,
                             CuNuevoDolar = ecgd.CuNuevoDolar,
                             CapexDolares = ecgd.CapexDolares,
                             CapexTotalSoles = ecgd.CapexTotalSoles,
                             Asignacion = ecgd.Asignacion,
                             Certificacion = ecgd.Certificacion,
                             Cantidad = ecgd.Cantidad,
                             Modelo = ecgd.Modelo,
                             Sede = u.Nombre,
                             Servicio = sg.Descripcion,
                             Concepto = cc.Descripcion


                             //Monto =ecgd.CapexSoles



                         }).AsNoTracking().Distinct().OrderBy(x => x.Grupo).ToPagedList(estructuraCosto.Indice, estructuraCosto.Tamanio);

            var resultado = new EstructuraCostoGrupoPaginadoDtoResponse
            {
                ListEstructuraCostoGrupoDtoResponse = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return resultado;
        }

        public EstructuraCostoGrupoDetalleDtoResponse ObtenerEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest estructuraCosto)
        {

            var resultado = (
                         from ecg in context.Set<EstructuraCostoGrupo>()
                         join g in context.Set<Grupo>() on ecg.IdGrupo equals g.IdGrupo
                         join ecgd in context.Set<EstructuraCostoGrupoDetalle>() on ecg.IdEstructuraCostoGrupo equals ecgd.IdEstructuraCostoGrupo
                         join u in context.Set<Ubigeo>() on ecgd.IdUbigeo equals u.CodigoDistrito
                         into uEmpty
                         from u in uEmpty.DefaultIfEmpty()
                         join cc in context.Set<ConceptosCapex>() on ecgd.IdConcepto equals cc.IdConceptosCapex
                         into ccEmpty
                         from cc in ccEmpty.DefaultIfEmpty()
                         join sg in context.Set<ServicioGrupo>() on ecgd.IdServicio equals sg.IdServicioGrupo
                         into sgEmpty
                         from sg in sgEmpty.DefaultIfEmpty()

                             // IdTipoGrupo
                         where
                          ecg.IdEstructuraCostoGrupo == estructuraCosto.IdEstructuraCostoGrupo &&
                          ecg.IdEstado == Generales.Estados.Activo


                         orderby ecg.FechaCreacion ascending
                         select new EstructuraCostoGrupoDetalleDtoResponse
                         {
                             IdEstructuraCostoGrupo = ecg.IdEstructuraCostoGrupo,
                             IdEstructuraCostoGrupoDetalle = ecgd.IdEstructuraCostoGrupoDetalle,
                             Asignacion = ecgd.Asignacion,
                             Modelo = ecgd.Modelo,
                             Cantidad = ecgd.Cantidad,
                             CapexDolares = ecgd.CapexDolares,
                             CapexSoles = ecgd.CapexSoles,
                             CapexTotalSoles = ecgd.CapexTotalSoles,
                             Certificacion = ecgd.Certificacion,
                             CuNuevoDolar = ecgd.CuNuevoDolar,
                             IdConcepto = ecgd.IdConcepto,
                             IdMedioCosto = ecgd.IdMedioCosto,
                             IdServicio = ecgd.IdServicio,
                             IdTipoGrupo = ecgd.IdTipoGrupo,
                             IdUbigeo = ecgd.IdUbigeo,
                             Descripcion = ecgd.Descripcion,

                             IdGrupo = ecg.IdGrupo



                         }).AsNoTracking().Distinct().Single();



            return resultado;
        }

    }
}
