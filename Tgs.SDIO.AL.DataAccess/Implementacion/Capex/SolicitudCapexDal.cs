﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Capex
{
    public class SolicitudCapexDal : Repository<SolicitudCapex>, ISolicitudCapexDal
    {
        readonly DioContext context;
        public SolicitudCapexDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }


        public SolicitudCapexPaginadoDtoResponse ListaSolicitudCapexPaginado(SolicitudCapexDtoRequest solicitudCapex)
        {
            var query = (from sc in context.Set<SolicitudCapex>()

                         join cl in context.Set<CapexLineaNegocio>() on sc.IdSolicitudCapex equals cl.IdSolicitudCapex
                         
                        join c in context.Set<Cliente>() on sc.IdCliente equals c.IdCliente

                         where

                       (sc.IdPreVenta ==solicitudCapex.IdPreVenta ) 
                       //&&
                       //(sc.FechaCreacion == solicitudCapex.FechaCreacion || string.IsNullOrEmpty(solicitudCapex.IdPreVenta.ToString())) &&
                       // (sc.NumeroSalesForce == solicitudCapex.NumeroSalesForce || string.IsNullOrEmpty(solicitudCapex.NumeroSalesForce)) &&
                       // (cl.IdLineaNegocio == solicitudCapex.IdLineaNegocio || string.IsNullOrEmpty(solicitudCapex.NumeroSalesForce)) &&
                       //  (sc.IdEstado == solicitudCapex.IdEstado || string.IsNullOrEmpty(solicitudCapex.IdEstado.ToString()))

                        // orderby sc.FechaCreacion ascending
                         select new SolicitudCapexDtoResponse
                         {
                             IdSolicitudCapex = sc.IdSolicitudCapex,
                             Descripcion=sc.Descripcion,
                             NumeroSalesForce=sc.NumeroSalesForce,
                             Cliente=c.Descripcion,
                             IdPreVenta=sc.IdPreVenta,
                             FechaCreacion=sc.FechaCreacion,
                             FechaEdicion=sc.FechaEdicion
                         }).AsNoTracking().Distinct().OrderBy(x => x.FechaCreacion).ToPagedList(solicitudCapex.Indice, solicitudCapex.Tamanio);

            var resultado = new SolicitudCapexPaginadoDtoResponse
            {
                ListSolicitudCapexDtoResponse = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return resultado;
        }

        public SolicitudCapexDtoResponse ObtenerSolicitudCapex(SolicitudCapexDtoRequest solicitudCapex)
        {
            var consulta = (from p in context.Set<SolicitudCapex>()
                            join c in context.Set<Cliente>() on p.IdCliente equals c.IdCliente
                            join m in context.Set<Maestra>().Where(x => x.IdRelacion == 168) on c.IdTipoEntidad.ToString() equals m.Valor
                            where p.IdSolicitudCapex == solicitudCapex.IdSolicitudCapex 
                            select new SolicitudCapexDtoResponse()
                            {
                                Descripcion = p.Descripcion,
                                IdCliente = p.IdCliente,
                                NombreCliente = c.Descripcion,
                                NumeroSalesForce = p.NumeroSalesForce,
                                CodigoPmo = p.CodigoPmo,
                                IdTipoEntidad = p.IdTipoEntidad,
                                IdTipoProyecto = p.IdTipoProyecto,
                                Entidad = c.Descripcion,
                                IdEstado = p.IdEstado
                            }).Single();

            return consulta;
        }
    }
}
