﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.DataContracts.Dto.Response.CartaFianza.CartaFianzaDetalleSeguimientoResponse;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza
{
    public class CartaFianzaDetalleSeguimientoDal : Repository<CartaFianzaDetalleSeguimiento>, ICartaFianzaDetalleSeguimientoDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public CartaFianzaDetalleSeguimientoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public CartaFianzaDetalleSeguimientoResponsePaginado ListaCartaFianzaDetalleSeguimiento(CartaFianzaDetalleSeguimientoRequest cartaFianzaDetalleRecuperoLista)
        {
            var registroCartaFianzaSeguimiento = (from cfds in context.Set<CartaFianzaDetalleSeguimiento>()

                                                  join cl in context.Set<Recurso>() on cfds.IdColaboradorACargo equals cl.IdRecurso
                                                  into clEmpty
                                                  from cl in clEmpty.DefaultIfEmpty()

                                                  join stat in context.Set<Maestra>().Where(x => x.IdRelacion == 225) on cfds.IdEstadoRecuperacionCartaFianzaTm equals stat.IdOpcion
                                                  into statEmpty
                                                  from stat in statEmpty.DefaultIfEmpty()

                                                  where cfds.IdCartaFianza == cartaFianzaDetalleRecuperoLista.IdCartaFianza
                                                  && cfds.Eliminado == false

                                                  select new CartaFianzaDetalleSeguimientoResponse
                                                  {
                                                      IdCartaFianza = cfds.IdCartaFianza,
                                                      IdCartaFianzaDetalleSeguimiento = cfds.IdCartaFianzaDetalleSeguimiento,

                                                      IdColaboradorACargo = cfds.IdColaboradorACargo,
                                                      IdEstadoRecuperacionCartaFianzaTm = cfds.IdEstadoRecuperacionCartaFianzaTm,
                                                      StatusRecupero = stat.Descripcion,
                                                      Comentario = cfds.Comentario,
                                                      ColadoradorACargo = cl.Nombre
                                                  }
                                                ).ToList().AsQueryable().ToPagedList(cartaFianzaDetalleRecuperoLista.Indice, cartaFianzaDetalleRecuperoLista.Tamanio);

            var Lista = new CartaFianzaDetalleSeguimientoResponsePaginado
            {
                ListCartaFianzaSeguimiento = registroCartaFianzaSeguimiento.ToList(),
                TotalItemCount = registroCartaFianzaSeguimiento.TotalItemCount
            };
            return Lista;
        }

    }
}
