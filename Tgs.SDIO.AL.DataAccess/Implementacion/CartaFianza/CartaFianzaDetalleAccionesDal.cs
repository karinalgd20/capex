﻿using System;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using System.Data.Entity;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Funciones;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza
{

    public class CartaFianzaDetalleAccionesDal : Repository<CartaFianzaDetalleAccion>, ICartaFianzaDetalleAccionDal
    {
        readonly DioContext context;
        public CartaFianzaDetalleAccionesDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

       

        public List<CartaFianzaDetalleAccionResponse> ListaCartaFianzaDetalleAccionId(CartaFianzaDetalleAccionRequest cartaFianzaDetalleAccionesRequestLista)
        {
     
            var regCFAciones = (from cfa in context.Set<CartaFianzaDetalleAccion>()
                                join cf in context.Set<CartaFianzaMaestro>() on cfa.IdCartaFianza equals cf.IdCartaFianza
                                join mtac in context.Set<Maestra>().Where(x => x.IdRelacion == 200) on cfa.IdTipoAccionTm equals mtac.IdOpcion
                                   into mtacEmpty     from mtac in mtacEmpty.DefaultIfEmpty()
                                    //into joined
                                    //from cfi in joined.DefaultIfEmpty()
                                where cfa.IdCartaFianza == cartaFianzaDetalleAccionesRequestLista.IdCartaFianza
                                //&& (cfa.IdTipoAccionTm == 1 )
                                orderby cfa.FechaCreacion ascending

                                select new CartaFianzaDetalleAccionResponse
                                {
                                    IdCartaFianzaDetalleAccion = cfa.IdCartaFianzaDetalleAccion,
                                    IdCartaFianza = cfa.IdCartaFianza,
                                    DesTipoAccion = mtac.Descripcion,
                                    Observacion = cfa.Observacion,
                                    IdColaboradorACargo = cfa.IdColaboradorACargo,
                                    IdEstadoVencimientoTm = cfa.IdEstadoVencimientoTm,
                                    FechaCreacion = cfa.FechaCreacion,
                                    IdUsuarioCreacion = cfa.IdUsuarioCreacion,
                                    FechaRegistro = cfa.FechaRegistro,
                                    UsuarioCreacion = cfa.UsuarioCreacion

                                }

                ).ToList()
                .Select(
                    x => new CartaFianzaDetalleAccionResponse
                    {
                        IdCartaFianzaDetalleAccion = x.IdCartaFianzaDetalleAccion,
                        IdCartaFianza = x.IdCartaFianza,
                        DesTipoAccion = x.DesTipoAccion,
                        Observacion = x.Observacion,
                        IdColaboradorACargo = x.IdColaboradorACargo,
                        IdEstadoVencimientoTm = x.IdEstadoVencimientoTm,
                        FechaCreacion = x.FechaCreacion,
                        IdUsuarioCreacion = x.IdUsuarioCreacion,
                        FechaRegistro = x.FechaRegistro,
                        FechaCreacionStr = Funciones.FormatoFecha(x.FechaCreacion),
                        FechaRegistroStr = Funciones.FormatoFecha(x.FechaRegistro),
                        UsuarioCreacion = x.UsuarioCreacion

                    }
                );

            return regCFAciones.ToList();
        }

    }
}
/*
)
*/
