﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using System;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Util.Funciones;
using Tgs.SDIO.Util.Paginacion;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Mensajes.CartaFianza;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza
{
    public class CartaFianzaDal : Repository<CartaFianzaMaestro>, ICartaFianzaDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        MailResponse respuestaMail = new MailResponse();
        public CartaFianzaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }


        public MailResponse ObtenerDatosCorreo(MailRequest mailResponse)
        {
            var IdCartaFianzaParam = new SqlParameter { ParameterName = "IdCartaFianza", Value = mailResponse.IdCartaFianza, SqlDbType = SqlDbType.Int };

            respuestaMail = context.ExecuteQuery<MailResponse>("CARTAFIANZA.OBTENER_DATOS_CORREO_FIANZA @IdCartaFianza", IdCartaFianzaParam ).FirstOrDefault();

            return respuestaMail;
        }

        public ProcesoResponse ActualizaARenovacionCartaFianza(CartaFianzaDetalleRenovacionRequest CartaFianzaRequestActualiza)
        {
            try
            {
                var pIdCartaFianzastr = new SqlParameter { ParameterName = "p_IdCartaFianzastr", Value = CartaFianzaRequestActualiza.IdCartaFianzastr, SqlDbType = SqlDbType.VarChar };
                var pIdColaborador = new SqlParameter { ParameterName = "p_IdColaborador", Value = CartaFianzaRequestActualiza.IdColaborador, SqlDbType = SqlDbType.Int };
                var pIdRenovarTm = new SqlParameter { ParameterName = "p_IdRenovarTm", Value = CartaFianzaRequestActualiza.IdRenovarTm, SqlDbType = SqlDbType.Int };
                var pIdUsuarioCreacion = new SqlParameter { ParameterName = "p_IdUsuarioCreacion", Value = CartaFianzaRequestActualiza.IdUsuarioCreacion, SqlDbType = SqlDbType.Int };
                var pIdTipoAccionTm = new SqlParameter { ParameterName = "p_IdTipoAccionTm", Value = CartaFianzaRequestActualiza.IdTipoAccionTm, SqlDbType = SqlDbType.Int };

                var pFechaRespuestaFinalGcSm = new SqlParameter { ParameterName = "p_FechaRespuestaFinalGcSm", Value = CartaFianzaRequestActualiza.FechaRespuestaFinalGcSm, SqlDbType = SqlDbType.DateTime };
                var pFechaVencimientoRenovacion = new SqlParameter { ParameterName = "p_FechaVencimientoRenovacion", Value = CartaFianzaRequestActualiza.FechaVencimientoRenovacion, SqlDbType = SqlDbType.DateTime };

                var pValidacionContratoGcSm = new SqlParameter { ParameterName = "p_ValidacionContratoGcSm", Value = CartaFianzaRequestActualiza.ValidacionContratoGcSm, SqlDbType = SqlDbType.VarChar };
                var pSustentoRenovacion = new SqlParameter { ParameterName = "p_SustentoRenovacion", Value = CartaFianzaRequestActualiza.SustentoRenovacion, SqlDbType = SqlDbType.VarChar };
                var pObservacion = new SqlParameter { ParameterName = "p_Observacion", Value = CartaFianzaRequestActualiza.Observacion, SqlDbType = SqlDbType.VarChar };
                var pPeriodoMesRenovacion = new SqlParameter { ParameterName = "p_PeriodoMesRenovacion", Value = CartaFianzaRequestActualiza.PeriodoMesRenovacion, SqlDbType = SqlDbType.Int };


                    
                respuesta.TipoRespuesta = (context.ExecuteCommand("CARTAFIANZA.USP_INSERTA_CARTAFIANZAR_RENOVACION  @p_IdCartaFianzastr,@p_IdColaborador,@p_IdRenovarTm,@p_IdTipoAccionTm,@p_IdUsuarioCreacion,@p_FechaRespuestaFinalGcSm,@p_FechaVencimientoRenovacion,@p_ValidacionContratoGcSm, @p_SustentoRenovacion,@p_Observacion,@p_PeriodoMesRenovacion", pIdCartaFianzastr, pIdColaborador, pIdRenovarTm, pIdTipoAccionTm, pIdUsuarioCreacion, pFechaRespuestaFinalGcSm, pFechaVencimientoRenovacion, pValidacionContratoGcSm, pSustentoRenovacion, pObservacion, pPeriodoMesRenovacion)) == -1 ? Proceso.Valido : Proceso.Invalido;
                respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }


            return respuesta;
        }

        public ProcesoResponse InsertaAccionMailCartaFianza(CartaFianzaDetalleRenovacionRequest CartaFianzaRequestActualiza)
        {
            try
            {
                var pIdCartaFianza = new SqlParameter { ParameterName = "p_IdCartaFianza", Value = CartaFianzaRequestActualiza.IdCartaFianza, SqlDbType = SqlDbType.Int };
                var pIdColaborador = new SqlParameter { ParameterName = "p_IdColaborador", Value = CartaFianzaRequestActualiza.IdColaborador, SqlDbType = SqlDbType.Int };
                var pIdRenovarTm = new SqlParameter { ParameterName = "p_IdRenovarTm", Value = CartaFianzaRequestActualiza.IdRenovarTm, SqlDbType = SqlDbType.Int };
                var pIdEstadoCartaFianzaTm = new SqlParameter { ParameterName = "p_IdEstadoCartaFianzaTm", Value = CartaFianzaRequestActualiza.IdEstadoCartaFianzaTm, SqlDbType = SqlDbType.Int };
                var pIdTipoSubEstadoCartaFianzaTm = new SqlParameter { ParameterName = "p_IdTipoSubEstadoCartaFianzaTm", Value = CartaFianzaRequestActualiza.IdTipoSubEstadoCartaFianzaTm, SqlDbType = SqlDbType.Int };
                var pIdUsuarioCreacion = new SqlParameter { ParameterName = "p_IdUsuarioCreacion", Value = CartaFianzaRequestActualiza.IdUsuarioCreacion, SqlDbType = SqlDbType.Int };
                var pIdTipoAccionTm = new SqlParameter { ParameterName = "p_IdTipoAccionTm", Value = CartaFianzaRequestActualiza.IdTipoAccionTm, SqlDbType = SqlDbType.Int };
                var pUsuarioEdicion = new SqlParameter { ParameterName = "p_UsuarioEdicion", Value = CartaFianzaRequestActualiza.UsuarioEdicion, SqlDbType = SqlDbType.VarChar };

                respuesta.TipoRespuesta = (context.ExecuteCommand("CARTAFIANZA.USP_INSERTA_CARTAFIANZAR_ACCIONESMAIL  @p_IdCartaFianza,@p_IdColaborador,@p_IdRenovarTm,@p_IdTipoAccionTm,@p_IdEstadoCartaFianzaTm,@p_IdTipoSubEstadoCartaFianzaTm,@p_IdUsuarioCreacion,@p_UsuarioEdicion ", pIdCartaFianza, pIdColaborador, pIdRenovarTm, pIdEstadoCartaFianzaTm, pIdTipoSubEstadoCartaFianzaTm, pIdTipoAccionTm, pIdUsuarioCreacion, pUsuarioEdicion)) == -1 ? Proceso.Valido : Proceso.Invalido;
                respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }


            return respuesta;
        }

        public CartaFianzaListaResponsePaginado ListaCartaFianza(CartaFianzaRequest cartaFianzaRequestLista)
        {
            /** Indicador G.C **/
            var indicador_cg = (
                                from m in context.Set<Maestra>().Where(x => x.IdRelacion == 440)
                                select new MaestraDtoResponse
                                {
                                    Comentario = m.Comentario,
                                    IdOpcion = m.IdOpcion
                                }
                ).OrderByDescending(x => x.IdOpcion).ToList();

            Dictionary<int, string> indicadorCF = new Dictionary<int, string>();
            foreach (var x in indicador_cg)
            {
                indicadorCF.Add((int)x.IdOpcion, x.Comentario);
            }


            /** Indicador tso **/
            var indicador_tso = (
                                from m in context.Set<Maestra>().Where(x => x.IdRelacion == 435)
                                select new MaestraDtoResponse
                                {
                                    Comentario = m.Comentario,
                                    IdOpcion = m.IdOpcion
                                }

               ).OrderByDescending(x => x.IdOpcion).ToList();

            Dictionary<int, string> indicadortso = new Dictionary<int, string>();
            foreach (var x in indicador_tso)
            {
                indicadortso.Add((int)x.IdOpcion, x.Comentario);
            }




            IList<int> IdTipoAccionTm = new List<int>() { 1 };
            IList<int> IdTipoSubEstadoCartaFianzaTm = new List<int>() { 4, 5 };

            var registroCartaFianza = (from cf in context.Set<CartaFianzaMaestro>() //.Where(x => x.IdTipoAccionTm)
                                       join c in context.Set<Cliente>() on cf.IdCliente equals c.IdCliente
                                       join m in context.Set<Maestra>().Where(x => x.IdRelacion == 186) on cf.IdTipoContratoTm equals m.IdOpcion
                                       into mtcEmpty from m in mtcEmpty.DefaultIfEmpty()

                                       join mp in context.Set<Maestra>().Where(x => x.IdRelacion == 269) on cf.IdProcesoTm equals mp.IdOpcion
                                       into mtpEmpty from mp in mtpEmpty.DefaultIfEmpty()

                                       join mtm in context.Set<Maestra>().Where(x => x.IdRelacion == 98) on cf.IdTipoMonedaTm equals mtm.IdOpcion
                                       into mtmEmpty from mtm in mtmEmpty.DefaultIfEmpty()

                                       join mtst in context.Set<Maestra>().Where(x => x.IdRelacion == 212) on cf.IdEstadoCartaFianzaTm equals mtst.IdOpcion
                                       into mtstEmpty from mtst in mtstEmpty.DefaultIfEmpty()

                                       join mtr in context.Set<Maestra>().Where(x => x.IdRelacion == 218) on cf.IdRenovarTm equals mtr.IdOpcion
                                       into mtrEmpty from mtr in mtrEmpty.DefaultIfEmpty()

                                       join mtsec in context.Set<Maestra>().Where(x => x.IdRelacion == 341) on cf.IdTipoSubEstadoCartaFianzaTm equals mtsec.IdOpcion
                                       into mtsecEmpty from mtsec in mtsecEmpty.DefaultIfEmpty()

                                       join mtreq in context.Set<Maestra>().Where(x => x.IdRelacion == 390) on cf.IdTipoRequeridaTm equals mtreq.IdOpcion
                                       into mtreqEmpty from mtreq in mtreqEmpty.DefaultIfEmpty()

                                       join cfr in context.Set<CartaFianzaMaestro>() on cf.IdCartaFianza equals cfr.IdCartaFianzaInicial
                                       into cfrtEmpty from cfr in cfrtEmpty.DefaultIfEmpty()

                                       join cfac in context.Set<CartaFianzaDetalleAccion>().Where(x => IdTipoSubEstadoCartaFianzaTm.Contains(x.IdTipoSubEstadoCartaFianzaTm)) on cf.IdCartaFianza equals cfac.IdCartaFianza
                                       into cfacEmpty from cfac in cfacEmpty.DefaultIfEmpty().Take(1)//.OrderBy( x => x.FechaCreacion )

                                       where
                                             IdTipoAccionTm.Contains((int)cf.IdTipoAccionTm)
                                                //&&
                                                //IdTipoSubEstadoCartaFianzaTm.Contains((int)cfac.IdTipoSubEstadoCartaFianzaTm)
                                                &&
                                              (  (cartaFianzaRequestLista.NombreCliente == null) || (c.Descripcion.Contains(cartaFianzaRequestLista.NombreCliente)) )
                                                &&
                                              ( (cartaFianzaRequestLista.NumeroContrato == null) || (cf.NumeroContrato.Contains(cartaFianzaRequestLista.NumeroContrato))    )
                                                &&
                                              ( (cartaFianzaRequestLista.NumeroIdentificadorFiscal == null) || (c.NumeroIdentificadorFiscal.Contains(cartaFianzaRequestLista.NumeroIdentificadorFiscal))    )
                                                &&
                                              ( (cartaFianzaRequestLista.NumeroGarantia == null) || (cf.NumeroGarantia.Contains(cartaFianzaRequestLista.NumeroGarantia))    )
                                                &&
                                              ( (cartaFianzaRequestLista.ImporteCartaFianzaSoles == 0) || (cf.ImporteCartaFianzaSoles.Equals(cartaFianzaRequestLista.ImporteCartaFianzaSoles))  )
                                                 &&
                                              ( (cartaFianzaRequestLista.FechaVencimientoBanco == null) || (cf.FechaVencimientoBanco == cartaFianzaRequestLista.FechaVencimientoBanco)  )
                                                  &&
                                              ((cartaFianzaRequestLista.IdEstadoCartaFianzaTm == -1) || (cf.IdEstadoCartaFianzaTm == cartaFianzaRequestLista.IdEstadoCartaFianzaTm))
                                                 &&
                                              ((cartaFianzaRequestLista.IdTipoRequeridaTm == -1) || (cf.IdTipoRequeridaTm == cartaFianzaRequestLista.IdTipoRequeridaTm))
                                               &&
                                               cf.Eliminado == false
                                              

                                       orderby cf.FechaVencimientoBanco ascending

                                       select new CartaFianzaListaResponse
                                       {
                                           IdCartaFianza = cf.IdCartaFianza,
                                           IdCliente = c.IdCliente,
                                           NombreCliente = c.Descripcion,
                                           NumeroOportunidad = cf.NumeroOportunidad,
                                           NumeroIdentificadorFiscal = c.NumeroIdentificadorFiscal,
                                           IdTipoContratoTm = cf.IdTipoContratoTm,
                                           DesTipoContrato = m.Descripcion,
                                           NumeroContrato = cf.NumeroContrato,
                                           IdProcesoTm = cf.IdProcesoTm,
                                           DesProcesoTm = mp.Comentario,

                                           Servicio = cf.Servicio,
                                           DescripcionServicioCartaFianza = cf.DescripcionServicioCartaFianza,

                                           FechaEmisionBanco = cf.FechaEmisionBanco,
                                           FechaVigenciaBanco = cf.FechaVigenciaBanco,
                                           FechaVencimientoBanco = cf.FechaVencimientoBanco,

                                           IdTipoAccionTm = cf.IdTipoAccionTm,

                                           IdRenovarTm = cf.IdRenovarTm,
                                           DesRenovarTm = mtr.Descripcion,

                                           IdEstadoCartaFianzaTm = cf.IdEstadoCartaFianzaTm,
                                           DesEstadoCartaFianzaTm = mtst.Descripcion,

                                           IdTipoSubEstadoCartaFianzaTm = cf.IdTipoSubEstadoCartaFianzaTm,
                                           DesTipoSubEstadoCartaFianzaTm = mtsec.Descripcion,

                                           IdEmpresaAdjudicadaTm = cf.IdEmpresaAdjudicadaTm,
                                           IdTipoGarantiaTm = cf.IdTipoGarantiaTm,
                                           NumeroGarantia = cf.NumeroGarantia,
                                           IdTipoMonedaTm = cf.IdTipoMonedaTm,
                                           DesTipoMonedaTm = mtm.Descripcion,
                                           ImporteCartaFianzaSoles = cf.ImporteCartaFianzaSoles,
                                           ImporteCartaFianzaDolares = cf.ImporteCartaFianzaDolares,
                                           ImporteCartaFianza = (cf.IdTipoMonedaTm == 1) ? (cf.ImporteCartaFianzaSoles) : (cf.ImporteCartaFianzaDolares),
                                           IdBanco = cf.IdBanco,

                                           ClienteEspecial = cf.ClienteEspecial,
                                           SeguimientoImportante = cf.SeguimientoImportante,
                                           Observacion = cf.Observacion,
                                           Incidencia = cf.Incidencia,
                                           IdEstado = cf.IdEstado,
                                           MotivoCartaFianzaErrada = cf.MotivoCartaFianzaErrada,
                                           IdUsuarioCreacion = cf.IdUsuarioCreacion,
                                           FechaCreacion = cf.FechaCreacion,

                                           IdCartaFianzaInicial = cfr.IdCartaFianzaInicial,

                                           FechaEnvioTso =cfac.FechaCreacion,

                                           FechaRenovacionEjecucion = cf.FechaRenovacionEjecucion,
                                           FechaDesestimarRequerimiento = cf.FechaDesestimarRequerimiento,
                                           FechaEjecucion = cf.FechaEjecucion,
                                           IdTipoRequeridaTm = cf.IdTipoRequeridaTm,
                                           desTipoRequeridaTm = mtreq.Descripcion,
                                           MotivoCartaFianzaRequerida = cf.MotivoCartaFianzaRequerida


                                       }
                                            ).ToList()
                                            .Select(x => new CartaFianzaListaResponse
                                            {
                                                IdCartaFianza = x.IdCartaFianza,
                                                IdCliente = x.IdCliente,
                                                NombreCliente = x.NombreCliente,
                                                NumeroIdentificadorFiscal = x.NumeroIdentificadorFiscal,
                                                NumeroOportunidad = x.NumeroOportunidad,
                                                IdTipoContratoTm = x.IdTipoContratoTm,
                                                DesTipoContrato = x.DesTipoContrato,
                                                NumeroContrato = x.NumeroContrato,
                                                IdProcesoTm = x.IdProcesoTm,
                                                DesProcesoTm = x.DesProcesoTm,

                                                Servicio = x.Servicio,
                                                DescripcionServicioCartaFianza = x.DescripcionServicioCartaFianza,

                                                FechaEmisionBancostr = (x.FechaEmisionBanco==null)? "" : Funciones.FormatoFecha(  (DateTime)x.FechaEmisionBanco)  ,
                                                FechaVigenciaBancostr = (x.FechaVigenciaBanco == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaVigenciaBanco),
                                                FechaVencimientoBancostr = (x.FechaVencimientoBanco == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaVencimientoBanco),

                                                IdTipoAccionTm = x.IdTipoAccionTm,

                                                IdRenovarTm = x.IdRenovarTm,
                                                DesRenovarTm = x.DesRenovarTm,

                                                IdEstadoCartaFianzaTm = x.IdEstadoCartaFianzaTm,
                                                DesEstadoCartaFianzaTm = x.DesEstadoCartaFianzaTm,
                                                IdTipoSubEstadoCartaFianzaTm =x.IdTipoSubEstadoCartaFianzaTm,
                                                DesTipoSubEstadoCartaFianzaTm = x.DesTipoSubEstadoCartaFianzaTm,

                                                IdEmpresaAdjudicadaTm = x.IdEmpresaAdjudicadaTm,
                                                IdTipoGarantiaTm = x.IdTipoGarantiaTm,
                                                NumeroGarantia = x.NumeroGarantia,
                                                IdTipoMonedaTm = x.IdTipoMonedaTm,
                                                ImporteCartaFianzaSoles = x.ImporteCartaFianzaSoles,
                                                ImporteCartaFianzaDolares = x.ImporteCartaFianzaDolares,
                                                ImporteFianzaStr = string.Join(" ", x.ImporteCartaFianza.ToString("###,###,##0.00"), x.DesTipoMonedaTm ),
                                                IdBanco = x.IdBanco,

                                                ClienteEspecial = x.ClienteEspecial,
                                                SeguimientoImportante = x.SeguimientoImportante,
                                                Observacion = x.Observacion,
                                                Incidencia = x.Incidencia,
                                                IdEstado = x.IdEstado,
                                                MotivoCartaFianzaErrada = (x.MotivoCartaFianzaErrada != null) ? x.MotivoCartaFianzaErrada : " ",
                                                IdUsuarioCreacion = x.IdUsuarioCreacion,
                                                FechaCreacion = x.FechaCreacion,

                                                IdIndicadorSemaforo = (x.FechaVencimientoBanco == null)? 0 : Funciones.DiferenciasDias(  DateTime.Now,(DateTime) x.FechaVencimientoBanco),
                                                IdIndicadorSemaforoColor = (x.FechaVencimientoBanco == null) ? "blue" : Funciones.ColoIndicador(Funciones.DiferenciasDias(DateTime.Now, (DateTime)x.FechaVencimientoBanco),indicadorCF),

                                                FechaEnvioTso = x.FechaEnvioTso,
                                                IdIndicadorSemaforoTpsTesoreria = (x.FechaEnvioTso == null) ? 0 : Funciones.DiferenciasDias(DateTime.Now, (DateTime)x.FechaEnvioTso),                                                
                                                IdIndicadorSemaforoColorTpsTesoreria = (x.FechaEnvioTso == null) ? "blue" : Funciones.ColoIndicadorRenovacion(Funciones.DiasHabiles(DateTime.Now, ((DateTime)x.FechaEnvioTso)), indicadortso),

                                                FechaRenovacionEjecucion = x.FechaRenovacionEjecucion,
                                                FechaRenovacionEjecucionStr = (x.FechaRenovacionEjecucion == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaRenovacionEjecucion),
                                                FechaDesestimarRequerimiento = x.FechaDesestimarRequerimiento,
                                                FechaDesestimarRequerimientoStr = (x.FechaDesestimarRequerimiento == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaDesestimarRequerimiento),
                                                FechaEjecucion = x.FechaEjecucion,
                                                FechaEjecucionStr = (x.FechaEjecucion == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaEjecucion),
                                                IdTipoRequeridaTm = x.IdTipoRequeridaTm,
                                                desTipoRequeridaTm = x.desTipoRequeridaTm,
                                                MotivoCartaFianzaRequerida = x.MotivoCartaFianzaRequerida

                                            }

                                        ).AsQueryable().ToPagedList(cartaFianzaRequestLista.Indice, cartaFianzaRequestLista.Tamanio);


            var registroCartaFianza2 = ListaOrdenada(registroCartaFianza.ToList(), cartaFianzaRequestLista);

            var Lista = new CartaFianzaListaResponsePaginado
            {
                ListCartaFianza = registroCartaFianza2.ToList(),
                TotalItemCount = registroCartaFianza.TotalItemCount
            };
            return Lista;
        }
 
        public List<CartaFianzaListaResponse> ListaOrdenada(List<CartaFianzaListaResponse> registroCartaFianza, CartaFianzaRequest cartaFianzaRequestLista)
        {
              switch (cartaFianzaRequestLista.SortName) {
                            case "NombreCliente":
                                if (cartaFianzaRequestLista.Sort == 1) { registroCartaFianza = registroCartaFianza.OrderBy(x => x.NombreCliente).ToList(); }
                                else { registroCartaFianza = registroCartaFianza.OrderByDescending(x => x.NombreCliente).ToList();  }
                            break;
                            case "NumeroContrato":
                                if (cartaFianzaRequestLista.Sort == 1) { registroCartaFianza = registroCartaFianza.OrderBy(x => x.NombreCliente).ToList(); }
                                else { registroCartaFianza = registroCartaFianza.OrderByDescending(x => x.NombreCliente).ToList(); }
                                break;
                            case "NumeroIdentificadorFiscal":
                                if (cartaFianzaRequestLista.Sort == 1) { registroCartaFianza = registroCartaFianza.OrderBy(x => x.NombreCliente).ToList(); }
                                else { registroCartaFianza = registroCartaFianza.OrderByDescending(x => x.NombreCliente).ToList(); }
                                break;
                            case "NumeroGarantia":
                                if (cartaFianzaRequestLista.Sort == 1) { registroCartaFianza = registroCartaFianza.OrderBy(x => x.NombreCliente).ToList(); }
                                else { registroCartaFianza = registroCartaFianza.OrderByDescending(x => x.NombreCliente).ToList(); }
                                break;
                            case "FechaVencimientoBanco":
                                if (cartaFianzaRequestLista.Sort == 1) { registroCartaFianza = registroCartaFianza.OrderBy(x => x.NombreCliente).ToList(); }
                                else { registroCartaFianza = registroCartaFianza.OrderByDescending(x => x.NombreCliente).ToList(); }
                                break;
                            case "ImporteCartaFianzaSoles":
                                if (cartaFianzaRequestLista.Sort == 1) { registroCartaFianza = registroCartaFianza.OrderBy(x => x.NombreCliente).ToList(); }
                                else { registroCartaFianza = registroCartaFianza.OrderByDescending(x => x.NombreCliente).ToList(); }
                                break;
                default:

                    IEnumerable<CartaFianzaListaResponse> sortedCartaFianza =
                        from CartaFianzaListaResponse in registroCartaFianza
                        orderby CartaFianzaListaResponse.FechaVencimientoBanco ascending, CartaFianzaListaResponse.IdRenovarTm ascending, CartaFianzaListaResponse.IdEstadoCartaFianzaTm ascending
                        select CartaFianzaListaResponse;

                    registroCartaFianza = sortedCartaFianza.ToList();
                    break;

            }

            return registroCartaFianza;
            }

        public List<CartaFianzaListaResponse> ListaCartaFianzaId(CartaFianzaRequest cartaFianzaRequestLista)
        {

            var registroCartaFianzaId = (
                          from cf in context.Set<CartaFianzaMaestro>()
                          join c in context.Set<Cliente>() on cf.IdCliente equals c.IdCliente                        
                          where    
                                       cf.IdCartaFianza == cartaFianzaRequestLista.IdCartaFianza
                                       && cf.IdCliente == cartaFianzaRequestLista.IdCliente
                          orderby cf.FechaVencimientoBanco descending

                          select new CartaFianzaListaResponse
                               {
                              IdCartaFianza = cf.IdCartaFianza,
                              IdCliente = c.IdCliente,
                              NumeroIdentificadorFiscal = c.NumeroIdentificadorFiscal,
                              NombreCliente = c.Descripcion,
                              NumeroOportunidad = cf.NumeroOportunidad,
                              IdTipoContratoTm = cf.IdTipoContratoTm,
                              NumeroContrato = cf.NumeroContrato,
                              IdProcesoTm = cf.IdProcesoTm,
                              NumeroProceso = cf.NumeroProceso,

                              Servicio = cf.Servicio,
                              DescripcionServicioCartaFianza = cf.DescripcionServicioCartaFianza,

                              FechaFirmaServicioContrato = cf.FechaFirmaServicioContrato,
                              FechaFinServicioContrato = cf.FechaFinServicioContrato,

                              IdEmpresaAdjudicadaTm = cf.IdEmpresaAdjudicadaTm,
                              IdTipoGarantiaTm = cf.IdTipoGarantiaTm,
                              IdTipoAccionTm = cf.IdTipoAccionTm,
                              NumeroGarantia = cf.NumeroGarantia,
                              IdTipoMonedaTm = cf.IdTipoMonedaTm,
                              ImporteCartaFianzaSoles = cf.ImporteCartaFianzaSoles,
                              ImporteCartaFianzaDolares = cf.ImporteCartaFianzaDolares,
                              IdBanco = cf.IdBanco,

                              FechaEmisionBanco = cf.FechaEmisionBanco,
                              FechaVigenciaBanco = cf.FechaVigenciaBanco,
                              FechaVencimientoBanco = cf.FechaVencimientoBanco,

                              ClienteEspecial = cf.ClienteEspecial,
                              SeguimientoImportante = cf.SeguimientoImportante,
                              Observacion = cf.Observacion,
                              Incidencia = cf.Incidencia,
                              IdEstado = cf.IdEstado,
                              IdUsuarioCreacion = cf.IdUsuarioCreacion,
                              FechaCreacion = cf.FechaCreacion,
                              IdUsuarioEdicion = cf.IdUsuarioEdicion,
                              FechaEdicion = cf.FechaEdicion,

                              FechaCorrecionTesoreria = cf.FechaCorrecionTesoreria,
                              FechaRecepcionTesoreria = cf.FechaRecepcionTesoreria,
                              MotivoCartaFianzaErrada = cf.MotivoCartaFianzaErrada

                          }
                                            ).ToList()//<CartaFianzaListaResponse>();
                                            .Select(x => new CartaFianzaListaResponse
                                            {
                                                IdCartaFianza = x.IdCartaFianza,
                                                IdCliente = x.IdCliente,
                                                NombreCliente = x.NombreCliente,
                                                NumeroIdentificadorFiscal = x.NumeroIdentificadorFiscal,
                                                NumeroOportunidad = x.NumeroOportunidad,
                                                IdTipoContratoTm = x.IdTipoContratoTm,
                                                NumeroContrato = x.NumeroContrato,
                                                IdProcesoTm = x.IdProcesoTm,
                                                NumeroProceso = x.NumeroProceso,

                                                Servicio = x.Servicio,
                                                DescripcionServicioCartaFianza = x.DescripcionServicioCartaFianza,

                                                FechaFirmaServicioContratostr = (x.FechaFirmaServicioContrato == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaFirmaServicioContrato),
                                                FechaFinServicioContratostr = (x.FechaFinServicioContrato == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaFinServicioContrato),

                                                IdEmpresaAdjudicadaTm = x.IdEmpresaAdjudicadaTm,
                                                IdTipoGarantiaTm = x.IdTipoGarantiaTm,
                                                NumeroGarantia = x.NumeroGarantia,
                                                IdTipoAccionTm = x.IdTipoAccionTm,
                                                IdTipoMonedaTm = x.IdTipoMonedaTm,
                                                ImporteCartaFianzaSoles = x.ImporteCartaFianzaSoles,
                                                ImporteCartaFianzaDolares = x.ImporteCartaFianzaDolares,
                                                IdBanco = x.IdBanco,

                                                FechaEmisionBancostr = (x.FechaEmisionBanco == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaEmisionBanco),
                                                FechaVigenciaBancostr = (x.FechaVigenciaBanco == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaVigenciaBanco),
                                                FechaVencimientoBancostr = (x.FechaVencimientoBanco == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaVencimientoBanco),

                                                ClienteEspecial = x.ClienteEspecial,
                                                SeguimientoImportante = x.SeguimientoImportante,
                                                Observacion = x.Observacion,
                                                Incidencia = x.Incidencia,
                                                IdEstado = x.IdEstado,
                                                IdUsuarioCreacion = x.IdUsuarioCreacion,
                                                FechaCreacion = x.FechaCreacion,
                                                IdUsuarioEdicion = x.IdUsuarioEdicion,
                                                FechaEdicion = x.FechaEdicion,

                                                //IdIndicadorSemaforo = (x.FechaVencimientoBanco == null) ? -999 : Funciones.DiferenciasDias(DateTime.Now, (DateTime)x.FechaVencimientoBanco),
                                                //IdIndicadorSemaforoColor = (x.FechaVencimientoBanco == null) ? "" : Funciones.ColoIndicador(Funciones.DiferenciasDias(DateTime.Now, (DateTime)x.FechaVencimientoBanco)),

                                                FechaCorrecionTesoreriaStr = (x.FechaCorrecionTesoreria == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaCorrecionTesoreria),
                                                FechaRecepcionTesoreriaStr = (x.FechaRecepcionTesoreria == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaRecepcionTesoreria),
                                                MotivoCartaFianzaErrada = x.MotivoCartaFianzaErrada
                                            }

                                        );

            return registroCartaFianzaId.ToList();


        }

        public DashBoardCartaFianzaResponse ListaDatos_DashoBoard_CartaFianza(DashBoardCartaFianzaRequest dashBoardCartaFianza)
        {
            var Anio = new SqlParameter { ParameterName = "p_Anio", Value = dashBoardCartaFianza.anio, SqlDbType = SqlDbType.Int };
            var Mes = new SqlParameter { ParameterName = "p_Mes", Value = dashBoardCartaFianza.mes, SqlDbType = SqlDbType.Int };
            var respuesta = context.ExecuteQuery<DashBoardCartaFianzaResponse>("CARTAFIANZA.OBTENER_DATOS_DASHBOARDPRINCIPAL_CARTAFIANZA @p_Anio,@p_mes", Anio, Mes).FirstOrDefault();

            return respuesta;
        }

        public List<CartaFianzaListaResponse> ListaCartaFianzaCombo(CartaFianzaRequest cartaFianzaRequestLista)
        {
            var IdColaboradorParam = new SqlParameter { ParameterName = "IdColaborador", Value = cartaFianzaRequestLista.IdColaborador, SqlDbType = SqlDbType.Int };

            var respuesta = context.ExecuteQuery<CartaFianzaListaResponse>("CARTAFIANZA.OBTENER_CARTAFIANZA_ASIGNADA @IdColaborador", IdColaboradorParam).ToList();

            return respuesta.Select(x => new CartaFianzaListaResponse
            {
                id = x.IdCliente,
                text = x.NombreCliente,
                IdColaborador = x.IdColaborador,
                NombreCompleto = x.NombreCompleto,
                FechaEdicion = x.FechaEdicion
            }).ToList();
        }

        public List<DashBoardCartaFianzaListGResponse> ListaDatos_DashoBoard_CartaFianza_Pie(DashBoardCartaFianzaRequest dashBoardCartaFianza)
        {
            var Anio = new SqlParameter { ParameterName = "p_Anio", Value = dashBoardCartaFianza.anio, SqlDbType = SqlDbType.Int };
            var Mes = new SqlParameter { ParameterName = "p_Mes", Value = dashBoardCartaFianza.mes, SqlDbType = SqlDbType.Int };
            var respuesta = context.ExecuteQuery<DashBoardCartaFianzaListGResponse>("CARTAFIANZA.USP_DASHBOARD_LISTADETALLEAGRUPADO @p_Anio,@p_mes", Anio, Mes).ToList();

            respuesta.Select(x => new DashBoardCartaFianzaListGResponse
            {
                Conteo = x.Conteo,
                Descripcion = x.Descripcion,
                IdTipoAccionTm = x.IdTipoAccionTm
            }
            ).ToList();

            return respuesta;
        }

        public List<CartaFianzaListaResponse> CartasFianzasSinRespuesta(CartaFianzaRequest CartaFianzaRequest)
        {
            var IdColaborador = new SqlParameter { ParameterName = "IdColaborador", Value = CartaFianzaRequest.IdColaborador, SqlDbType = SqlDbType.Int };
            var respuesta = context.ExecuteQuery<CartaFianzaListaResponse>("CARTAFIANZA.OBTENER_CARTASFIANZAS_POR_COLABORADOR @IdColaborador", IdColaborador).ToList();

            return respuesta;
        }

        public ProcesoResponse GuardarRespuestaGerenteCuenta(CartaFianzaDetalleRenovacionRequest CartaFianzaRenovacion)
        {
            var IdCartaFianza = new SqlParameter { ParameterName = "IdCartaFianza", Value = CartaFianzaRenovacion.IdCartaFianza, SqlDbType = SqlDbType.Int };
            var IdColaborador = new SqlParameter { ParameterName = "IdColaborador", Value = CartaFianzaRenovacion.IdColaborador, SqlDbType = SqlDbType.Int };
            var IdRenovarTm = new SqlParameter { ParameterName = "IdRenovarTm", Value = CartaFianzaRenovacion.IdRenovarTm, SqlDbType = SqlDbType.Int };
            var PeriodoMesRenovacion = new SqlParameter { ParameterName = "PeriodoMesRenovacion", Value = CartaFianzaRenovacion.PeriodoMesRenovacion, SqlDbType = SqlDbType.Int };
            var SustentoRenovacion = new SqlParameter { ParameterName = "SustentoRenovacion", Value = CartaFianzaRenovacion.SustentoRenovacion, SqlDbType = SqlDbType.VarChar };
            var IdUsuarioCreacion = new SqlParameter { ParameterName = "IdUsuarioCreacion", Value = CartaFianzaRenovacion.IdUsuarioCreacion, SqlDbType = SqlDbType.Int };
            var response = context.ExecuteQuery<CartaFianzaListaResponse>("CARTAFIANZA.GUARDAR_RESPUESTA_GERENTECUENTA @IdCartaFianza,@IdColaborador,@IdRenovarTm,@PeriodoMesRenovacion,@SustentoRenovacion,@IdUsuarioCreacion",
                                                                                                                        IdCartaFianza, IdColaborador, IdRenovarTm, PeriodoMesRenovacion, SustentoRenovacion, IdUsuarioCreacion).ToList();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;

            return respuesta;
        }

        public ProcesoResponse GuardarConfirmacionGerenteCuenta(CartaFianzaDetalleRenovacionRequest CartaFianzaRenovacion)
        {
            var IdColaborador = new SqlParameter { ParameterName = "IdColaborador", Value = CartaFianzaRenovacion.IdColaborador, SqlDbType = SqlDbType.Int };
            var response = context.ExecuteQuery<CartaFianzaListaResponse>("CARTAFIANZA.GUARDAR_CONFIRMACION_GERENTECUENTA @IdColaborador", IdColaborador).ToList();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;

            return respuesta;
        }

    }

}
