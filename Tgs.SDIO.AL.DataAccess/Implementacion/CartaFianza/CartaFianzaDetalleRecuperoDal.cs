﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Funciones;
using Tgs.SDIO.Util.Mensajes.CartaFianza;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.DataContracts.Dto.Response.CartaFianza.CartaFianzaDetalleSeguimientoResponse;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza
{


    public class CartaFianzaDetalleRecuperoDal : Repository<CartaFianzaDetalleRecupero>, ICartaFianzaDetalleRecuperoDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public CartaFianzaDetalleRecuperoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
        public ProcesoResponse ActualizaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest cartaFianzaDetalleRecuperoActualiza)
        {
            var IdCartaFianzaDetalleRecupero = new SqlParameter { ParameterName = "IdCartaFianzaDetalleRecupero", Value = cartaFianzaDetalleRecuperoActualiza.IdCartaFianzaDetalleRecupero, SqlDbType = SqlDbType.Int };
            var IdCartaFianza = new SqlParameter { ParameterName = "IdCartaFianza", Value = cartaFianzaDetalleRecuperoActualiza.IdCartaFianza, SqlDbType = SqlDbType.Int };
            var IdUsuarioEdicion = new SqlParameter { ParameterName = "IdUsuarioEdicion", Value = cartaFianzaDetalleRecuperoActualiza.IdUsuarioEdicion, SqlDbType = SqlDbType.Int };
            var FechaRecupero = new SqlParameter { ParameterName = "FechaRecupero", Value = cartaFianzaDetalleRecuperoActualiza.FechaRecupero ?? (object)DBNull.Value, SqlDbType = SqlDbType.DateTime };
            var FechaSolicitudRecupero = new SqlParameter { ParameterName = "FechaSolicitudRecupero", Value = cartaFianzaDetalleRecuperoActualiza.FechaSolicitudRecupero ?? (object)DBNull.Value, SqlDbType = SqlDbType.DateTime };
            var FechaDevolucionTesoreria = new SqlParameter { ParameterName = "FechaDevolucionTesoreria", Value = cartaFianzaDetalleRecuperoActualiza.FechaDevolucionTesoreria ?? (object)DBNull.Value, SqlDbType = SqlDbType.DateTime };
            var IdCartaFianzaDetalleSeguimiento = new SqlParameter { ParameterName = "IdCartaFianzaDetalleSeguimiento", Value = cartaFianzaDetalleRecuperoActualiza.IdCartaFianzaDetalleSeguimiento, SqlDbType = SqlDbType.Int };
            var Comentario = new SqlParameter { ParameterName = "Comentario", Value = cartaFianzaDetalleRecuperoActualiza.Comentario ?? (object)DBNull.Value, SqlDbType = SqlDbType.VarChar };
            var IdEstadoRecuperacionCartaFianzaTm = new SqlParameter { ParameterName = "IdEstadoRecuperacionCartaFianzaTm", Value = cartaFianzaDetalleRecuperoActualiza.IdEstadoRecuperacionCartaFianzaTm, SqlDbType = SqlDbType.Int };
            var IdColaboradorACargo = new SqlParameter { ParameterName = "IdColaboradorACargo", Value = cartaFianzaDetalleRecuperoActualiza.IdColaboradorACargo, SqlDbType = SqlDbType.Int };
            var Eliminado = new SqlParameter { ParameterName = "Eliminado", Value = cartaFianzaDetalleRecuperoActualiza.Eliminado, SqlDbType = SqlDbType.Bit };

            var response = context.ExecuteCommand("CARTAFIANZA.INSERTAR_RECUPERO_SEGUIMIENTO @IdCartaFianzaDetalleRecupero,@IdCartaFianza,@IdUsuarioEdicion,@FechaRecupero,@FechaSolicitudRecupero,@FechaDevolucionTesoreria,@IdCartaFianzaDetalleSeguimiento,@Comentario,@IdEstadoRecuperacionCartaFianzaTm,@IdColaboradorACargo,@Eliminado",
                                                                                              IdCartaFianzaDetalleRecupero, IdCartaFianza, IdUsuarioEdicion, FechaRecupero, FechaSolicitudRecupero, FechaDevolucionTesoreria, IdCartaFianzaDetalleSeguimiento, Comentario, IdEstadoRecuperacionCartaFianzaTm, IdColaboradorACargo, Eliminado);
            respuesta.TipoRespuesta = (response > 0) ? Proceso.Valido : Proceso.Invalido;
            respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;

            return respuesta;
        }

        public CartaFianzaDetalleRecuperoResponsePaginado ListaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest cartaFianzaDetalleRecuperoLista)
        {
            IList<int> IdTipoAccionTm = new List<int>() { 1, 2 };

            var propertyInfo = typeof(CartaFianzaDetalleRecuperoResponse).GetProperty(cartaFianzaDetalleRecuperoLista.ColumnName);

            var registroCartaFianzaRecupero = (
                                               from cf in context.Set<CartaFianzaMaestro>()

                                               join cfdr in context.Set<CartaFianzaDetalleRecupero>() on   cf.IdCartaFianza equals cfdr.IdCartaFianza
                                                //into cfdrEmpty
                                                //from cfdrl in cfdrEmpty.DefaultIfEmpty()
                                                  

                                               join c in context.Set<Cliente>() on cf.IdCliente equals c.IdCliente

                                               join m in context.Set<Maestra>().Where(x => x.IdRelacion == 98) on cf.IdTipoMonedaTm equals m.IdOpcion

                                               join cfds in context.Set<CartaFianzaDetalleSeguimiento>().Where(x => x.UltimoSeguimiento == true) on cfdr.IdCartaFianza equals cfds.IdCartaFianza
                                               into cfdsEmpty
                                               from cfds in cfdsEmpty.DefaultIfEmpty()

                                               join b in context.Set<Maestra>().Where(x => x.IdRelacion == 239) on cf.IdBanco equals b.IdOpcion
                                                into mEbEmpty
                                               from b in mEbEmpty.DefaultIfEmpty()

                                               join cl in context.Set<Recurso>() on cfds.IdColaboradorACargo equals cl.IdRecurso
                                               into clEmpty
                                               from cl in clEmpty.DefaultIfEmpty()

                                               join stat in context.Set<Maestra>().Where(x => x.IdRelacion == 225) on cfds.IdEstadoRecuperacionCartaFianzaTm equals stat.IdOpcion
                                               into statEmpty
                                               from stat in statEmpty.DefaultIfEmpty()

                                               where
                                               !IdTipoAccionTm.Contains((int)cf.IdTipoAccionTm)
                                               &&
                                               ((cartaFianzaDetalleRecuperoLista.Cliente == null) || (c.Descripcion.Contains(cartaFianzaDetalleRecuperoLista.Cliente)))
                                               &&
                                               ((cartaFianzaDetalleRecuperoLista.NumeroGarantia == null) || (cf.NumeroGarantia.Contains(cartaFianzaDetalleRecuperoLista.NumeroGarantia)))
                                               &&
                                               ((cartaFianzaDetalleRecuperoLista.ImporteStr == null) || (cf.ImporteCartaFianzaDolares.ToString().Contains(cartaFianzaDetalleRecuperoLista.ImporteStr)) || (cf.ImporteCartaFianzaSoles.ToString().Contains(cartaFianzaDetalleRecuperoLista.ImporteStr)))
                                               &&
                                               ((cartaFianzaDetalleRecuperoLista.IdBanco == -1) || (cartaFianzaDetalleRecuperoLista.IdBanco == 0) || (cf.IdBanco == cartaFianzaDetalleRecuperoLista.IdBanco)
                                               &&
                                               ((cartaFianzaDetalleRecuperoLista.FechaFinServicio == null) || (cf.FechaFinServicioContrato == cartaFianzaDetalleRecuperoLista.FechaFinServicio))
                                               &&
                                               ((cartaFianzaDetalleRecuperoLista.IdEstadoRecuperacionCartaFianzaTm == -1) || (cartaFianzaDetalleRecuperoLista.IdEstadoRecuperacionCartaFianzaTm == 0) || (cfds.IdEstadoRecuperacionCartaFianzaTm == cartaFianzaDetalleRecuperoLista.IdEstadoRecuperacionCartaFianzaTm)))
                                                       &&
                                               cf.Eliminado == false

                                               select new CartaFianzaDetalleRecuperoResponse
                                               {
                                                   IdCartaFianza = cfdr.IdCartaFianza,

                                                   IdCartaFianzaDetalleRecupero = cfdr.IdCartaFianzaDetalleRecupero,
                                                   IdCartaFianzaDetalleSeguimiento = cfds.IdCartaFianzaDetalleSeguimiento,

                                                   FechaSolicitudRecupero = cfdr.FechaSolicitudRecupero,
                                                   FechaRecupero = cfdr.FechaRecupero,
                                                   FechaDevolucionTesoreria = cfdr.FechaDevolucionTesoreria,

                                                   IdColaboradorACargo = cfds.IdColaboradorACargo,
                                                   IdEstadoRecuperacionCartaFianzaTm = cfds.IdEstadoRecuperacionCartaFianzaTm,
                                                   StatusRecupero = stat.Descripcion,
                                                   Comentario = cfds.Comentario,
                                                   ColadoradorACargo = cl.Nombre,

                                                   NumeroIdentificadorFiscal = c.NumeroIdentificadorFiscal,
                                                   Cliente = c.Descripcion,
                                                   NumeroGarantia = cf.NumeroGarantia,
                                                   TipoMoneda = m.Descripcion,
                                                   ImporteCartaFianza = (cf.IdTipoMonedaTm == 1) ? (cf.ImporteCartaFianzaSoles) : (cf.ImporteCartaFianzaDolares),
                                                   EntidadBancaria = b.Descripcion,
                                                   FechaFinServicio = cf.FechaFinServicioContrato

                                               }
                                            ).ToList()
                                            .Select(x => new CartaFianzaDetalleRecuperoResponse
                                            {
                                                IdCartaFianza = x.IdCartaFianza,
                                                IdCartaFianzaDetalleRecupero = x.IdCartaFianzaDetalleRecupero,
                                                IdCartaFianzaDetalleSeguimiento = x.IdCartaFianzaDetalleSeguimiento,

                                                FechaSolicitudRecuperoStr = (x.FechaSolicitudRecupero == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaSolicitudRecupero),
                                                FechaRecuperoStr = (x.FechaRecupero == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaRecupero),
                                                FechaDevolucionTesoreriaStr = (x.FechaDevolucionTesoreria == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaDevolucionTesoreria),

                                                IdColaboradorACargo = (x.IdColaboradorACargo == null) ? -1 : x.IdColaboradorACargo,
                                                IdEstadoRecuperacionCartaFianzaTm = (x.IdEstadoRecuperacionCartaFianzaTm == null) ? -1 : x.IdEstadoRecuperacionCartaFianzaTm,
                                                StatusRecupero = x.StatusRecupero,
                                                Comentario = x.Comentario,
                                                ColadoradorACargo = x.ColadoradorACargo,

                                                NumeroIdentificadorFiscal = x.NumeroIdentificadorFiscal,
                                                Cliente = x.Cliente,
                                                NumeroGarantia = x.NumeroGarantia,
                                                ImporteStr = string.Join(" ", x.ImporteCartaFianza.ToString("###,###,##0.00"), x.TipoMoneda),
                                                EntidadBancaria = x.EntidadBancaria,
                                                FechaFinServicioStr = (x.FechaFinServicio == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaFinServicio)
                                            }

                                        );

            var orderByListRecupero = (cartaFianzaDetalleRecuperoLista.OrderType == "asc") ? (registroCartaFianzaRecupero.OrderBy(x => propertyInfo.GetValue(x, null))) : (registroCartaFianzaRecupero.OrderByDescending(x => propertyInfo.GetValue(x, null)));

            

            //IEnumerable<CartaFianzaListaResponse> sortedCartaFianza =
            //          from CartaFianzaListaResponse in orderByListRecupero
            //          orderby CartaFianzaListaResponse.FechaRecupero ascending,  CartaFianzaListaResponse.IdEstadoRecuperacionCartaFianzaTm ascending
            //          select CartaFianzaListaResponse;

            //orderByListRecupero = sortedCartaFianza.ToList();


            var ListRecuperoPagination = orderByListRecupero.AsQueryable().ToPagedList(cartaFianzaDetalleRecuperoLista.Indice, cartaFianzaDetalleRecuperoLista.Tamanio);

            var Lista = new CartaFianzaDetalleRecuperoResponsePaginado
            {
                ListCartaFianzaRecupero = ListRecuperoPagination.ToList(),
                TotalItemCount = ListRecuperoPagination.TotalItemCount
            };
            return Lista;
        }

    }
}
