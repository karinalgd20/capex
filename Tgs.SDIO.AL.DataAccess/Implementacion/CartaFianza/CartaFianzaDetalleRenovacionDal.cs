﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Funciones;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza
{


    public class CartaFianzaDetalleRenovacionDal : Repository<CartaFianzaDetalleRenovacion>, ICartaFianzaDetalleRenovacionDal
    {
        readonly DioContext context;
        MailResponse respuesta = new MailResponse();
        public CartaFianzaDetalleRenovacionDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        
        public MailResponse ObtenerDatosCorreoRenovacion(MailRequest mailResponse)
        {
            var IdCartaFianza = new SqlParameter { ParameterName = "IdCartaFianza", Value = mailResponse.IdCartaFianza, SqlDbType = SqlDbType.Int };
            var IdColaborador = new SqlParameter { ParameterName = "IdColaborador", Value = mailResponse.IdColaborador, SqlDbType = SqlDbType.Int };
            respuesta = context.ExecuteQuery<MailResponse>("CARTAFIANZA.OBTENER_DATOS_CORREO_RENOVACION @IdCartaFianza, @IdColaborador", IdCartaFianza, IdColaborador).FirstOrDefault();

            return respuesta;
        }
        public CartaFianzaDetalleRenovacionResponsePaginado ListaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest cartaFianzaDetalleRenovacionLista)
        {
            IList<int> IdTipoAccionTm = new List<int>() { 2, 3 };

            /** Indicador G.C **/
            var indicador_cg = (
                                from m in context.Set<Maestra>().Where(x => x.IdRelacion == 440)
                                select new MaestraDtoResponse
                                {
                                    Comentario = m.Comentario,
                                    IdOpcion = m.IdOpcion
                                }
                ).OrderByDescending(x => x.IdOpcion).ToList();

            Dictionary<int, string> indicadorCF = new Dictionary<int, string>();
            foreach (var x in indicador_cg)
            {
                indicadorCF.Add((int)x.IdOpcion, x.Comentario);
            }


            /** Indicador tso **/
            var indicador_tso = (
                                from m in context.Set<Maestra>().Where(x => x.IdRelacion == 435)
                                select new MaestraDtoResponse
                                {
                                    Comentario = m.Comentario,
                                    IdOpcion = m.IdOpcion
                                }

               ).OrderByDescending(x => x.IdOpcion).ToList();

            Dictionary<int, string> indicadortso = new Dictionary<int, string>();
            foreach (var x in indicador_tso)
            {
                indicadortso.Add((int)x.IdOpcion, x.Comentario);
            }



            var listaCartaFianzaRenovacion = (from cf in context.Set<CartaFianzaMaestro>()
                                              join c in context.Set<Cliente>() on cf.IdCliente equals c.IdCliente
                                              join cfr in context.Set<CartaFianzaDetalleRenovacion>() on cf.IdCartaFianza equals cfr.IdCartaFianza
                                              into cfrEmpty
                                              from cfrl in cfrEmpty.DefaultIfEmpty()

                                              join cfcd in context.Set<CartaFianzaColaboradorDetalle>().Where(x => x.IdEstado == 0) on cf.IdCartaFianza equals cfcd.IdCartaFianza

                                              join rc in context.Set<Recurso>().Where(x => x.IdCargo == 3) on cfcd.IdColaborador equals rc.IdRecurso
                                              //into rcCEmpty  from rcl in rcCEmpty.DefaultIfEmpty()

                                              join b in context.Set<Maestra>().Where(x => x.IdRelacion == 239) on cf.IdBanco equals b.IdOpcion
                                              into mEbEmpty
                                              from b in mEbEmpty.DefaultIfEmpty()

                                              join m in context.Set<Maestra>().Where(x => x.IdRelacion == 98) on cf.IdTipoMonedaTm equals m.IdOpcion
                                              into mEmpty
                                              from ml in mEmpty.DefaultIfEmpty()

                                              join r in context.Set<Maestra>().Where(x => x.IdRelacion == 218) on cfrl.IdRenovarTm equals r.IdOpcion
                                               into rEmpty
                                              from rl in rEmpty.DefaultIfEmpty()

                                              join mtst in context.Set<Maestra>().Where(x => x.IdRelacion == 212) on cf.IdEstadoCartaFianzaTm equals mtst.IdOpcion
                                              into mtstEmpty
                                              from mtst in mtstEmpty.DefaultIfEmpty()

                                              join ncf in context.Set<CartaFianzaMaestro>() on cf.IdCartaFianza equals ncf.IdCartaFianzaInicial
                                              into ncfEmpty
                                              from ncf in ncfEmpty.DefaultIfEmpty()

                                              join mtreq in context.Set<Maestra>().Where(x => x.IdRelacion == 390) on cf.IdTipoRequeridaTm equals mtreq.IdOpcion
                                              into mtreqEmpty
                                              from mtreq in mtreqEmpty.DefaultIfEmpty()

                                              join mta in context.Set<Maestra>().Where(x => x.IdRelacion == 200) on cf.IdTipoAccionTm equals mta.IdOpcion
                                              into mtaEmpty
                                              from mtal in mtaEmpty.DefaultIfEmpty()


                                              where
                                              IdTipoAccionTm.Contains((int)cf.IdTipoAccionTm)
                                              &&
                                              ((cartaFianzaDetalleRenovacionLista.Cliente == null) || (c.Descripcion.Contains(cartaFianzaDetalleRenovacionLista.Cliente)))
                                               &&
                                              ((cartaFianzaDetalleRenovacionLista.NumeroIdentificadorFiscal == null) || (c.NumeroIdentificadorFiscal.Contains(cartaFianzaDetalleRenovacionLista.NumeroIdentificadorFiscal)))
                                              &&
                                              ((cartaFianzaDetalleRenovacionLista.Importe.ToString() == null) || (cf.ImporteCartaFianzaDolares.ToString().Contains(cartaFianzaDetalleRenovacionLista.Importe.ToString())) || (cf.ImporteCartaFianzaSoles.ToString().Contains(cartaFianzaDetalleRenovacionLista.Importe.ToString())))
                                              &&
                                              ((cartaFianzaDetalleRenovacionLista.IdColaborador == -1 || cartaFianzaDetalleRenovacionLista.IdColaborador == 0) || (rc.IdRecurso == cartaFianzaDetalleRenovacionLista.IdColaborador))
                                                 &&
                                              ((cartaFianzaDetalleRenovacionLista.NumeroGarantia == null) || (cf.NumeroGarantia.Contains(cartaFianzaDetalleRenovacionLista.NumeroGarantia)))
                                              &&
                                              ((cartaFianzaDetalleRenovacionLista.FechaVencimientoRenovacion == null) || (cfrl.FechaVencimientoRenovacion >= cartaFianzaDetalleRenovacionLista.FechaVencimientoRenovacion))
                                              &&
                                              ((cartaFianzaDetalleRenovacionLista.IdRenovarTm == -1) || (cartaFianzaDetalleRenovacionLista.IdRenovarTm == 0) || (cfrl.IdRenovarTm == cartaFianzaDetalleRenovacionLista.IdRenovarTm))
                                                      &&
                                               cf.Eliminado == false

                                              orderby cf.IdCartaFianzaInicial descending

                                              select new CartaFianzaDetalleRenovacionResponse
                                              {
                                                  IdCartaFianza = cf.IdCartaFianza,
                                                  IdCartaFianzaDetalleRenovacion = cfrl.IdCartaFianzaDetalleRenovacion,
                                                  NumeroIdentificadorFiscal = c.NumeroIdentificadorFiscal,
                                                  IdCliente = c.IdCliente,
                                                  Cliente = c.Descripcion,
                                                  TipoMoneda = ml.Descripcion,
                                                  Importe = (cf.IdTipoMonedaTm == 1) ? (cf.ImporteCartaFianzaSoles) : (cf.ImporteCartaFianzaDolares),
                                                  FechaVencimientoRenovacion = cfrl.FechaVencimientoRenovacion,
                                                  SustentoRenovacion = cfrl.SustentoRenovacion,
                                                  Observacion = cfrl.Observacion,
                                                  NombreCompletoColaborador = rc.Nombre,
                                                  IdRenovarTm = cfrl.IdRenovarTm,
                                                  DesTipoAccionTm = mtal.Descripcion,
                                                  IdTipoAccionTm = cf.IdTipoAccionTm,
                                                  PeriodoMesRenovacion = cfrl.PeriodoMesRenovacion,
                                                  EstadoRenovacion = rl.Descripcion,
                                                  EntidadBancaria = b.Descripcion,
                                                  DescripcionCartaFianza = cf.DescripcionServicioCartaFianza,
                                                  FechaVencimientoBanco = cf.FechaVencimientoBanco,
                                                  FechaSolitudTesoria = cfrl.FechaSolitudTesoria,
                                                  CartaFianzaReemplazo = cfrl.CartaFianzaReemplazo,
                                                  NumeroGarantia = cf.NumeroGarantia,
                                                  NumeroGarantiaReemplazo = cfrl.NumeroGarantiaReemplazo,

                                                  IdTipoBancoReemplazo = cfrl.IdTipoBancoReemplazo,

                                                  ValidacionContratoGcSm = cfrl.ValidacionContratoGcSm,
                                                  FechaRespuestaFinalGcSm = cfrl.FechaRespuestaFinalGcSm,
                                                  FechaEntregaRenovacion = cfrl.FechaEntregaRenovacion,
                                                  FechaRecepcionCartaFianzaRenovacion = cfrl.FechaRecepcionCartaFianzaRenovacion,

                                                  IdCartaFianzaInicial = ncf.IdCartaFianzaInicial,
                                                  IdEstadoCartaFianzaTm = cf.IdEstadoCartaFianzaTm,
                                                  DesEstadoCartaFianzaTm = mtst.Descripcion,

                                                  FechaEmisionBancoRnv = cfrl.FechaEmisionBancoRnv,
                                                  FechaVigenciaBancoRnv = cfrl.FechaVigenciaBancoRnv,
                                                  FechaVencimientoBancoRnv = cfrl.FechaVencimientoBancoRnv,

                                                  FechaRenovacionEjecucion = cf.FechaRenovacionEjecucion,
                                                  FechaDesestimarRequerimiento = cf.FechaDesestimarRequerimiento,
                                                  FechaEjecucion = cf.FechaEjecucion,
                                                  IdTipoRequeridaTm = cf.IdTipoRequeridaTm,
                                                  desTipoRequeridaTm = mtreq.Descripcion,
                                                  MotivoCartaFianzaRequerida = cf.MotivoCartaFianzaRequerida
                                              }

                                              ).ToList()
                                              .Select(x => new CartaFianzaDetalleRenovacionResponse
                                              {
                                                  IdCartaFianza = x.IdCartaFianza,
                                                  IdCartaFianzaDetalleRenovacion = x.IdCartaFianzaDetalleRenovacion,
                                                  NumeroIdentificadorFiscal = x.NumeroIdentificadorFiscal,
                                                  IdCliente = x.IdCliente,
                                                  Cliente = x.Cliente,
                                                  Importe = x.Importe,
                                                  ImporteStr = string.Join(" ", x.Importe.ToString("###,###,##0.00"), x.TipoMoneda),
                                                  FechaVencimientoRenovacionStr = (x.FechaVencimientoRenovacion == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaVencimientoRenovacion),
                                                  FechaVencimientoBancoStr = (x.FechaVencimientoBanco == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaVencimientoBanco),
                                                  FechaSolitudTesoriaStr = (x.FechaSolitudTesoria == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaSolitudTesoria),
                                                  SustentoRenovacion = x.SustentoRenovacion,
                                                  Observacion = x.Observacion,
                                                  NombreCompletoColaborador = x.NombreCompletoColaborador,
                                                  IdRenovarTm = x.IdRenovarTm,
                                                  IdTipoAccionTm = x.IdTipoAccionTm,
                                                  DesTipoAccionTm = x.DesTipoAccionTm,
                                                  PeriodoMesRenovacion = x.PeriodoMesRenovacion,
                                                  EstadoRenovacion = x.EstadoRenovacion,
                                                  IdEstadoCartaFianzaTm = x.IdEstadoCartaFianzaTm,
                                                  DesEstadoCartaFianzaTm = x.DesEstadoCartaFianzaTm,
                                                  EntidadBancaria = x.EntidadBancaria,
                                                  DescripcionCartaFianza = x.DescripcionCartaFianza,
                                                  IdIndicadorSemaforo = (x.FechaVencimientoBancoRnv == null) ? 0 : Funciones.DiferenciasDias(DateTime.Now, (DateTime)x.FechaVencimientoBancoRnv),
                                                  IdIndicadorSemaforoColor = (x.FechaVencimientoBancoRnv == null) ? "000" : Funciones.ColoIndicador(Funciones.DiferenciasDias(DateTime.Now, (DateTime)x.FechaVencimientoBancoRnv), indicadorCF),
                                                  IdIndicadorSemaforoTpsTesoreria = (x.FechaSolitudTesoria == null) ? 0 : Funciones.DiferenciasDias(DateTime.Now, (DateTime)x.FechaSolitudTesoria),
                                                  IdIndicadorSemaforoColorTpsTesoreria = (x.FechaSolitudTesoria == null) ? "000" : Funciones.ColoIndicadorRenovacion(Funciones.DiasHabiles(DateTime.Now, (DateTime)x.FechaSolitudTesoria), indicadortso),
                                                  CartaFianzaReemplazo = x.CartaFianzaReemplazo,
                                                  NumeroGarantia = x.NumeroGarantia,
                                                  NumeroGarantiaReemplazo = x.NumeroGarantiaReemplazo,
                                                  IdTipoBancoReemplazo = x.IdTipoBancoReemplazo,

                                                  ValidacionContratoGcSm = x.ValidacionContratoGcSm,
                                                  FechaRespuestaFinalGcSm = x.FechaRespuestaFinalGcSm,
                                                  FechaEntregaRenovacion = x.FechaEntregaRenovacion,
                                                  FechaRecepcionCartaFianzaRenovacion = x.FechaRecepcionCartaFianzaRenovacion,

                                                  FechaRecepcionCartaFianzaRenovacionStr = (x.FechaRecepcionCartaFianzaRenovacion == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaRecepcionCartaFianzaRenovacion),

                                                  FechaRespuestaFinalGcSmStr = (x.FechaRespuestaFinalGcSm == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaRespuestaFinalGcSm),
                                                  FechaEntregaRenovacionStr = (x.FechaEntregaRenovacion == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaEntregaRenovacion),


                                                  FechaVencimientoBancoRnvStr = (x.FechaVencimientoBancoRnv == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaVencimientoBancoRnv),
                                                  FechaVigenciaBancoRnvStr = (x.FechaVigenciaBancoRnv == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaVigenciaBancoRnv),
                                                  FechaEmisionBancoRnvStr = (x.FechaEmisionBancoRnv == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaEmisionBancoRnv),

                                                  IdCartaFianzaInicial = x.IdCartaFianzaInicial,

                                                  FechaRenovacionEjecucion = x.FechaRenovacionEjecucion,
                                                  FechaRenovacionEjecucionStr = (x.FechaRenovacionEjecucion == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaRenovacionEjecucion),
                                                  FechaDesestimarRequerimiento = x.FechaDesestimarRequerimiento,
                                                  FechaDesestimarRequerimientoStr = (x.FechaDesestimarRequerimiento == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaDesestimarRequerimiento),
                                                  FechaEjecucion = x.FechaEjecucion,
                                                  FechaEjecucionStr = (x.FechaEjecucion == null) ? "" : Funciones.FormatoFecha((DateTime)x.FechaEjecucion),
                                                  IdTipoRequeridaTm = x.IdTipoRequeridaTm,
                                                  desTipoRequeridaTm = x.desTipoRequeridaTm,
                                                  MotivoCartaFianzaRequerida = x.MotivoCartaFianzaRequerida



                                              }
                                              ).AsQueryable().ToPagedList(cartaFianzaDetalleRenovacionLista.Indice, cartaFianzaDetalleRenovacionLista.Tamanio);

            var registroCartaFianzaRenovacion2 = ListaOrdenada(listaCartaFianzaRenovacion.ToList(), cartaFianzaDetalleRenovacionLista);


            var Lista = new CartaFianzaDetalleRenovacionResponsePaginado
            {
                ListCartaFianzaRenovacion = registroCartaFianzaRenovacion2.ToList(),
                TotalItemCount = listaCartaFianzaRenovacion.TotalItemCount
            };
            return Lista;
        }


        public List<CartaFianzaDetalleRenovacionResponse> ListaOrdenada(List<CartaFianzaDetalleRenovacionResponse> listaCartaFianzaRenovacion, CartaFianzaDetalleRenovacionRequest cartaFianzaDetalleRenovacionLista)
        {
            switch (cartaFianzaDetalleRenovacionLista.SortName)
            {
                case "Cliente":
                    if (cartaFianzaDetalleRenovacionLista.Sort == 1) { listaCartaFianzaRenovacion = listaCartaFianzaRenovacion.OrderBy(x => x.Cliente).ToList(); }
                    else { listaCartaFianzaRenovacion = listaCartaFianzaRenovacion.OrderByDescending(x => x.Cliente).ToList(); }
                    break;
                case "IdRenovarTm":
                    if (cartaFianzaDetalleRenovacionLista.Sort == 1) { listaCartaFianzaRenovacion = listaCartaFianzaRenovacion.OrderBy(x => x.IdRenovarTm).ToList(); }
                    else { listaCartaFianzaRenovacion = listaCartaFianzaRenovacion.OrderByDescending(x => x.IdRenovarTm).ToList(); }
                    break;
                case "TipoAccion":
                    if (cartaFianzaDetalleRenovacionLista.Sort == 1) { listaCartaFianzaRenovacion = listaCartaFianzaRenovacion.OrderBy(x => x.IdTipoAccionTm).ToList(); }
                    else { listaCartaFianzaRenovacion = listaCartaFianzaRenovacion.OrderByDescending(x => x.IdTipoAccionTm).ToList(); }
                    break;
                default:

                    IEnumerable<CartaFianzaDetalleRenovacionResponse> sortedCartaFianzaDetalleRenovacio =
                        from CartaFianzaDetalleRenovacionResponse in listaCartaFianzaRenovacion
                        orderby CartaFianzaDetalleRenovacionResponse.FechaVencimientoBancoRnv ascending, CartaFianzaDetalleRenovacionResponse.IdRenovarTm ascending, CartaFianzaDetalleRenovacionResponse.IdEstadoCartaFianzaTm ascending
                        select CartaFianzaDetalleRenovacionResponse;

                    listaCartaFianzaRenovacion = sortedCartaFianzaDetalleRenovacio.ToList();
                    break;


            }

            return listaCartaFianzaRenovacion;
        }

    }
}
