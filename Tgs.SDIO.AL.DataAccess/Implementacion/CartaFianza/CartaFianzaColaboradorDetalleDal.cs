﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Mensajes.CartaFianza;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza
{
    public class CartaFianzaColaboradorDetalleDal : Repository<CartaFianzaColaboradorDetalle>, ICartaFianzaColaboradorDetalleDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();

        public CartaFianzaColaboradorDetalleDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<CartaFianzaColaboradorDetalleResponse> ListarColaborador(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            var consulta = (from cl in context.Set<Recurso>()

                            join su in context.Set<Recurso>() on cl.IdSupervisor equals su.IdRecurso
                            into suEmpty from su in suEmpty.DefaultIfEmpty()

                            where cl.IdCargo == cartaFianzaColaboradorDetalleRequest.IdTipoColaboradorFianzaTm
                            &&
                            ((cartaFianzaColaboradorDetalleRequest.IdSupervisorColaborador == -1) || (su.IdRecurso.ToString() == "") || (cl.IdSupervisor == cartaFianzaColaboradorDetalleRequest.IdSupervisorColaborador ))

                            orderby cl.FechaEdicion descending, su.Nombre descending

                            select new CartaFianzaColaboradorDetalleResponse
                            {
                                id = cl.IdRecurso,
                                text = cl.Nombre,
                                IdSupervisorColaborador = su.IdRecurso,
                                NombreCompleto = su.Nombre,
                                IdEstado = cl.IdEstado,
                                FechaEdicion = cl.FechaEdicion
                            }).ToList();

            return consulta;
        }

        public ProcesoResponse AsignarSupervisorAGerenteCuenta(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            var IdSupervisorColaborador = new SqlParameter { ParameterName = "IdSupervisorColaborador", Value = cartaFianzaColaboradorDetalleRequest.IdSupervisorColaborador, SqlDbType = SqlDbType.Int };
            var IdColaboradorStr = new SqlParameter { ParameterName = "IdColaboradorStr", Value = cartaFianzaColaboradorDetalleRequest.IdColaboradorStr, SqlDbType = SqlDbType.VarChar };
            var IdUsuarioEdicion = new SqlParameter { ParameterName = "IdUsuarioEdicion", Value = cartaFianzaColaboradorDetalleRequest.IdUsuarioEdicion, SqlDbType = SqlDbType.Int };

            var response = context.ExecuteCommand("CARTAFIANZA.ASIGNAR_SUPERVISOR_GERENTECUENTA @IdSupervisorColaborador,@IdColaboradorStr,@IdUsuarioEdicion", 
                                                                                                IdSupervisorColaborador, IdColaboradorStr, IdUsuarioEdicion);

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;

            return respuesta;
        }

        public ProcesoResponse AsignarGerenteCuentaACartaFianza(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            var IdColaborador = new SqlParameter { ParameterName = "IdColaborador", Value = cartaFianzaColaboradorDetalleRequest.IdColaborador, SqlDbType = SqlDbType.Int };
            var IdClienteStr = new SqlParameter { ParameterName = "IdClienteStr", Value = cartaFianzaColaboradorDetalleRequest.IdClienteStr, SqlDbType = SqlDbType.VarChar };
            var IdColaboradorStr = new SqlParameter { ParameterName = "IdColaboradorStr", Value = cartaFianzaColaboradorDetalleRequest.IdColaboradorStr, SqlDbType = SqlDbType.VarChar };
            var IdUsuarioEdicion = new SqlParameter { ParameterName = "IdUsuarioEdicion", Value = cartaFianzaColaboradorDetalleRequest.IdUsuarioEdicion, SqlDbType = SqlDbType.Int };

            var response = context.ExecuteCommand("CARTAFIANZA.ASIGNAR_GERENTECUENTA_CARTAFIANZA @IdColaborador,@IdClienteStr,@IdColaboradorStr,@IdUsuarioEdicion",
                                                                                                  IdColaborador, IdClienteStr, IdColaboradorStr, IdUsuarioEdicion);

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;

            return respuesta;
        }

        public ProcesoResponse ActualizarTesorera(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            var IdColaborador = new SqlParameter { ParameterName = "IdColaborador", Value = cartaFianzaColaboradorDetalleRequest.IdColaborador, SqlDbType = SqlDbType.Int };
            var IdUsuarioEdicion = new SqlParameter { ParameterName = "IdUsuarioEdicion", Value = cartaFianzaColaboradorDetalleRequest.IdUsuarioEdicion, SqlDbType = SqlDbType.Int };
            var response = context.ExecuteQuery<CartaFianzaColaboradorDetalleResponse>("CARTAFIANZA.ACTUALIZAR_TESORERA @IdColaborador, @IdUsuarioEdicion", IdColaborador, IdUsuarioEdicion).FirstOrDefault();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;

            return respuesta;
        }

    }
}