﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadFlujoEstadoDal : Repository<OportunidadFlujoEstado>, IOportunidadFlujoEstadoDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadFlujoEstadoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public int OportunidadFlujoEstadoUltimoId()

        {
            var maxValue = context.OportunidadFlujoEstado.Max(x => x.IdOportunidadFlujoEstado);

            return maxValue;
        }

        public ProcesoResponse InsertarConfiguracionFlujo(OportunidadLineaNegocioDtoRequest oportunidadFlujoEstado) {
            var resultado = new ProcesoResponse();
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = oportunidadFlujoEstado.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = oportunidadFlujoEstado.IdEstado, SqlDbType = SqlDbType.Int };
            var IdUsuarioCreacion = new SqlParameter { ParameterName = "IdUsuarioCreacion", Value = oportunidadFlujoEstado.IdUsuarioCreacion, SqlDbType = SqlDbType.Int };

            resultado.TipoRespuesta = context.ExecuteCommand("[OPORTUNIDAD].[USP_INSERTAR_CONFIGURACIONFLUJO] @IdOportunidadLineaNegocio,@IdEstado,@IdUsuarioCreacion",
                                                                IdOportunidadLineaNegocio, IdEstado, IdUsuarioCreacion);

            return resultado;
        }
    }
}



