﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Funciones;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadDal : Repository<Entities.Entities.Oportunidad.Oportunidad>, IOportunidadDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
        
        public List<OportunidadDtoResponse> ListarOportunidadCabecera(OportunidadDtoRequest oportunidad)
        {

            if (oportunidad.Descripcion == null) { oportunidad.Descripcion = ""; }
            if (oportunidad.NumeroSalesForce == null) { oportunidad.NumeroSalesForce = ""; }
            var pIDLINEAPRODUCTO = new SqlParameter { ParameterName = "pIDLINEAPRODUCTO", Value = oportunidad.IdLineaNegocio, SqlDbType = SqlDbType.Int };
            var pIDCLIENTE = new SqlParameter { ParameterName = "pIDCLIENTE", Value = oportunidad.IdCliente, SqlDbType = SqlDbType.Int };
            var pDESCRIPCION = new SqlParameter { ParameterName = "pDESCRIPCION", Value = oportunidad.Descripcion, SqlDbType = SqlDbType.VarChar };
            var pNUMEROSALESFORCE = new SqlParameter { ParameterName = "pNUMEROSALESFORCE", Value = oportunidad.NumeroSalesForce, SqlDbType = SqlDbType.VarChar };
            //var pIDGERENTE = new SqlParameter { ParameterName = "pIDGERENTE", Value = oportunidad.IdGerente, SqlDbType = SqlDbType.Int };
            //var pIDDIRECCION = new SqlParameter { ParameterName = "pIDDIRECCION", Value = oportunidad.IdDireccion, SqlDbType = SqlDbType.Int };
            var pIDESTADO = new SqlParameter { ParameterName = "pIDESTADO", Value = oportunidad.IdEstado, SqlDbType = SqlDbType.Int };


            var Indice = new SqlParameter { ParameterName = "Indice", Value = oportunidad.Indice, SqlDbType = SqlDbType.Int };
            var Tamanio = new SqlParameter { ParameterName = "Tamanio", Value = oportunidad.Tamanio, SqlDbType = SqlDbType.Int };

            var lista = context.ExecuteQuery<OportunidadDtoResponse>("OPORTUNIDAD.USP_LISTA_OPORTUNIDAD_CAB @pIDLINEAPRODUCTO, @pIDCLIENTE, @pDESCRIPCION, @pNUMEROSALESFORCE, @pIDESTADO,@Indice,@Tamanio",
             pIDLINEAPRODUCTO, pIDCLIENTE, pDESCRIPCION, pNUMEROSALESFORCE, pIDESTADO, Indice, Tamanio).ToList<OportunidadDtoResponse>();
            //var lista = context.ExecuteQuery<OportunidadDtoResponse>("OPORTUNIDAD.USP_LISTA_OPORTUNIDAD_CAB @pIDLINEAPRODUCTO, @pIDCLIENTE, @pDESCRIPCION, @pNUMEROSALESFORCE, @pIDESTADO",
            //  pIDLINEAPRODUCTO, pIDCLIENTE, pDESCRIPCION, pNUMEROSALESFORCE, pIDESTADO).ToList<OportunidadDtoResponse>();


           List<OportunidadDtoResponse> ListarOportunidadCabecera = new List<OportunidadDtoResponse>();
            foreach (var part in lista)
            {


                ListarOportunidadCabecera.Add(new OportunidadDtoResponse()
                {

                    IdOportunidad = part.IdOportunidad,
                    IdPreVenta = part.IdPreVenta,
                    IdCoordinadorFinanciero = part.IdCoordinadorFinanciero,
                    IdAnalistaFinanciero = part.IdAnalistaFinanciero,
                    FechaEdicion = part.FechaEdicion,
                    Oportunidad = part.Oportunidad,
                    IdLineaNegocio = part.IdLineaNegocio,
                    // LineaProducto=part.LineaProducto,
                    IdTipoEmpresa = part.IdTipoEmpresa,
                    TipoEmpresa = part.TipoEmpresa,
                    NumeroSalesForce = part.NumeroSalesForce,
                    NumeroCaso = part.NumeroCaso,
                    IdCliente = part.IdCliente,
                    Cliente = part.Cliente,
                    IdSector = part.IdSector,
                    Sector = part.Sector,
                    // GerenteComercial = part.GerenteComercial,
                    //IdDireccion = part.IdDireccion,
                    DireccionComercial = part.DireccionComercial,
                    OIBDA = part.OIBDA,
                    VANProyecto = part.VANProyecto,
                    VANVAIProyecto = part.VANVAIProyecto,
                    ValorRescate = part.ValorRescate,
                    PayBackProyecto = part.PayBackProyecto,
                    IdEstado = part.IdEstado,
                    Estado = part.Estado,
                    VersionPadre = part.VersionPadre,
                    Version = part.Version,
                    Agrupador = part.Agrupador,
                    Versiones = part.Versiones,
                    FechaCreacion = part.FechaCreacion,
                    StrFechaCreacion = part.FechaCreacion.ToString(),
                    StrFechaEdicion = part.FechaEdicion != null ? part.FechaEdicion.ToString() : "",
                    TotalRow =part.TotalRow
                });

            }

       

            if (oportunidad.FechaCreacion != DateTime.MinValue)
            {
                //ListarOportunidadCabecera = ListarOportunidadCabecera.Where<OportunidadDtoResponse>(p => p.FechaCreacion.ToString("d/MM/yyyy") == oportunidad.FechaCreacion.ToString("d/MM/yyyy")).ToList<OportunidadDtoResponse>();
                ListarOportunidadCabecera = ListarOportunidadCabecera.Where<OportunidadDtoResponse>(p => p.FechaCreacion >= oportunidad.FechaCreacion).ToList<OportunidadDtoResponse>();

            }

            if (oportunidad.FechaEdicion != null)
            {


                ListarOportunidadCabecera = ListarOportunidadCabecera.Where<OportunidadDtoResponse>(p => p.FechaEdicion <= oportunidad.FechaEdicion).ToList<OportunidadDtoResponse>();
            }

            if (oportunidad.IdPreVenta != null)
            {
                ListarOportunidadCabecera = ListarOportunidadCabecera.Where<OportunidadDtoResponse>(p => p.IdPreVenta == oportunidad.IdPreVenta).ToList<OportunidadDtoResponse>();
            }
            if (oportunidad.IdCoordinadorFinanciero != null)
            {
                ListarOportunidadCabecera = ListarOportunidadCabecera.Where<OportunidadDtoResponse>(p => p.IdCoordinadorFinanciero == oportunidad.IdCoordinadorFinanciero).ToList<OportunidadDtoResponse>();
            }
            if (oportunidad.IdProductManager != null)
            {
                ListarOportunidadCabecera = ListarOportunidadCabecera.Where<OportunidadDtoResponse>(p => p.IdProductManager == oportunidad.IdProductManager).ToList<OportunidadDtoResponse>();
            }
            if (oportunidad.IdAnalistaFinanciero != null)
            {
                ListarOportunidadCabecera = ListarOportunidadCabecera.Where<OportunidadDtoResponse>(p => p.IdAnalistaFinanciero == oportunidad.IdAnalistaFinanciero).ToList<OportunidadDtoResponse>();
            }

            ListarOportunidadCabecera.Sort((x, y) => -x.FechaCreacion.CompareTo(y.FechaCreacion));


            return ListarOportunidadCabecera;
        }



        public List<OportunidadDtoResponse> ListarProyectadoOIBDA(OportunidadDtoRequest oportunidad)
        {
            var pIDOportunidad = new SqlParameter { ParameterName = "IDOportunidad", Value = oportunidad.IdOportunidad, SqlDbType = SqlDbType.Int };
            var pIDESTADO = new SqlParameter { ParameterName = "IDESTADO", Value = oportunidad.IdEstado, SqlDbType = SqlDbType.Int };
            var pIDUSUARIOCREACION = new SqlParameter { ParameterName = "IDUSUARIOCREACION", Value = oportunidad.IdUsuarioCreacion, SqlDbType = SqlDbType.Int };
            var pIDLINEAPRODUCTO = new SqlParameter { ParameterName = "IDLINEAPRODUCTO", Value = oportunidad.IdLineaNegocio, SqlDbType = SqlDbType.Int };

            var lista = context.ExecuteQuery<OportunidadDtoResponse>("OPORTUNIDAD.tmp_STF_PROYECTADOOIBDA @IDOportunidad, @IDESTADO, @IDUSUARIOCREACION, @IDLINEAPRODUCTO",
                pIDOportunidad, pIDESTADO, pIDUSUARIOCREACION, pIDLINEAPRODUCTO).ToList<OportunidadDtoResponse>();

            return lista;
        }

        public List<OportunidadDtoResponse> ListarOportunidad(OportunidadDtoRequest oportunidad)
        {
            var pIDLINEAPRODUCTO = new SqlParameter { ParameterName = "pIDLINEAPRODUCTO", Value = oportunidad.IdLineaNegocio, SqlDbType = SqlDbType.Int };
            var pIDCLIENTE = new SqlParameter { ParameterName = "pIDCLIENTE", Value = oportunidad.IdCliente, SqlDbType = SqlDbType.Int };
            var pDESCRIPCION = new SqlParameter { ParameterName = "pDESCRIPCION", Value = oportunidad.Descripcion, SqlDbType = SqlDbType.VarChar };
            var pNUMEROSALESFORCE = new SqlParameter { ParameterName = "pNUMEROSALESFORCE", Value = oportunidad.NumeroSalesForce, SqlDbType = SqlDbType.VarChar };
            var pIDGERENTE = new SqlParameter { ParameterName = "pIDGERENTE", Value = oportunidad.IdGerente, SqlDbType = SqlDbType.Int };
            var pIDDIRECCION = new SqlParameter { ParameterName = "pIDDIRECCION", Value = oportunidad.IdDireccion, SqlDbType = SqlDbType.Int };
            var pIDESTADO = new SqlParameter { ParameterName = "pIDESTADO", Value = oportunidad.IdEstado, SqlDbType = SqlDbType.Int };

            var lista = context.ExecuteQuery<OportunidadDtoResponse>("OPORTUNIDAD.STF_LISTAOportunidadS @pIDLINEAPRODUCTO, @pIDCLIENTE, @pDESCRIPCION, @pNUMEROSALESFORCE, @pIDGERENTE, @pIDDIRECCION, @pIDESTADO",
                pIDLINEAPRODUCTO, pIDCLIENTE, pDESCRIPCION, pNUMEROSALESFORCE, pIDGERENTE, pIDDIRECCION, pIDESTADO).ToList<OportunidadDtoResponse>();

            return lista;
        }


        public ProcesoResponse OportunidadGanador(OportunidadDtoRequest oportunidad)
        {
            try
            {
                var pIdOportunidad = new SqlParameter { ParameterName = "IdOportunidad", Value = oportunidad.IdOportunidad, SqlDbType = SqlDbType.Int };
                var pIdUsuarioEdicion = new SqlParameter { ParameterName = "IdUsuarioEdicion", Value = oportunidad.IdUsuarioEdicion, SqlDbType = SqlDbType.Int };
                var pFechaEdicion = new SqlParameter { ParameterName = "FechaEdicion", Value = oportunidad.FechaEdicion, SqlDbType = SqlDbType.DateTime };
                respuesta.TipoRespuesta = context.ExecuteCommand("OPORTUNIDAD.STF_Oportunidad_GANADOR @IdOportunidad,@IdUsuarioEdicion,@FechaEdicion", pIdOportunidad, pIdUsuarioEdicion, pFechaEdicion);
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }

        public ProcesoResponse OportunidadInactivar(OportunidadDtoRequest oportunidad)
        {
            try
            {
                var pIdOportunidad = new SqlParameter { ParameterName = "IdOportunidad", Value = oportunidad.IdOportunidad, SqlDbType = SqlDbType.Int };
                var pIdUsuarioEdicion = new SqlParameter { ParameterName = "IdUsuarioEdicion", Value = oportunidad.IdUsuarioEdicion, SqlDbType = SqlDbType.Int };
                var pFechaEdicion = new SqlParameter { ParameterName = "FechaEdicion", Value = oportunidad.FechaEdicion, SqlDbType = SqlDbType.DateTime };
                respuesta.TipoRespuesta = context.ExecuteCommand("OPORTUNIDAD.STF_Oportunidad_INACTIVAR @IdOportunidad,@IdUsuarioEdicion,@FechaEdicion", pIdOportunidad, pIdUsuarioEdicion, pFechaEdicion);
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }

        public ProcesoResponse RegistrarVersionOportunidad(OportunidadDtoRequest oportunidad)
        {
            try
            {
                var pIDOportunidad = new SqlParameter { ParameterName = "IDOportunidad", Value = oportunidad.IdOportunidad, SqlDbType = SqlDbType.Int };
                respuesta.TipoRespuesta = context.ExecuteCommand("OPORTUNIDAD.STF_NUEVA_VERSION @IDOportunidad", pIDOportunidad);
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }
        public int OportunidadUltimoId()
        {
            bool valida = context.Oportunidad.Any();
            var maxValue = (valida) ? context.Oportunidad.Max(x => x.IdOportunidad) : Numeric.Uno;

            return maxValue;
        }


        public ProcesoResponse NuevaVersionOportunidad(OportunidadDtoRequest oportunidad)
        {
            var resultado = (from obj in context.Set<Entities.Entities.Oportunidad.Oportunidad>()
                           where
                           obj.IdOportunidad == oportunidad.IdOportunidad
                             orderby obj.FechaCreacion ascending
                         select new OportunidadDtoResponse
                         {

                             IdCliente = obj.IdCliente,
                             Descripcion = obj.Descripcion,
                             NumeroSalesForce = obj.NumeroSalesForce,
                             NumeroCaso = obj.NumeroCaso,
                             Fecha = obj.Fecha,
                             Alcance = obj.Alcance,
                             Periodo = obj.Periodo,
                             TiempoImplantacion = obj.TiempoImplantacion,
                             IdTipoServicio = obj.IdTipoServicio,
                             IdEstado = obj.IdEstado,
                             IdAnalistaFinanciero = obj.IdAnalistaFinanciero,
                             IdProductManager = obj.IdProductManager,
                             IdTipoProyecto = obj.IdTipoProyecto,
                             IdTipoCambio = obj.IdTipoCambio,
                             IdProyectoAnterior = obj.IdProyectoAnterior,
                             IdCoordinadorFinanciero = obj.IdCoordinadorFinanciero



                         }).AsNoTracking().ToList();


            return respuesta;
        }

        public OportunidadDtoResponse ObtenerNumeroVersionesOportunidad(OportunidadDtoRequest oportunidad)
        {
            OportunidadDtoResponse resultado = new OportunidadDtoResponse();
            bool valida = context.Oportunidad.Any();
            var maxValue = (valida) ? context.Oportunidad.Where(c => c.Agrupador == oportunidad.Agrupador).Max(c => c.Version) : Numeric.Uno;
            resultado.Versiones = maxValue;

            return resultado;
        }
  




    }
}



