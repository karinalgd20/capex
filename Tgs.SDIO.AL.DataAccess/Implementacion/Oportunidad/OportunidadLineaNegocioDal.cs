﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadLineaNegocioDal: Repository<OportunidadLineaNegocio>, IOportunidadLineaNegocioDal
    {
        readonly DioContext context;
        public OportunidadLineaNegocioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
                                       
        public List<ListaDtoResponse> ListarLineaNegocioPorIdOportunidad(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)                    
        {
            var result = (from obj in context.Set<OportunidadLineaNegocio>()
                                                                                           
                         join lineaNegocio in context.Set<LineaNegocio>() on obj.IdLineaNegocio equals lineaNegocio.IdLineaNegocio
                         join oportunidad in context.Set < Entities.Entities.Oportunidad.Oportunidad > ()on obj.IdOportunidad equals oportunidad.IdOportunidad     
                                 
                                                        
                         where
                         obj.IdOportunidad== oportunidadLineaNegocio.IdOportunidad &&
                          obj.IdEstado == Generales.Estados.Activo

                         orderby obj.FechaCreacion ascending
                         select new ListaDtoResponse
                         {                   
                                 Codigo = obj.IdLineaNegocio.ToString(),
                                 Descripcion = lineaNegocio.Descripcion
                                      

                         }).AsNoTracking().ToList();


            return result;
        }

        public OportunidadDtoResponse ObtenerOportunidadPorLineaNegocio(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)
        {
            var result = (from oportunidad in context.Set<Entities.Entities.Oportunidad.Oportunidad>()
                          join lineaNegocio in context.Set<OportunidadLineaNegocio>() on oportunidad.IdOportunidad equals lineaNegocio.IdOportunidad
                          where
                          lineaNegocio.IdOportunidadLineaNegocio == oportunidadLineaNegocio.IdOportunidadLineaNegocio &&
                          lineaNegocio.IdEstado == oportunidadLineaNegocio.IdEstado
                          select new OportunidadDtoResponse
                          {
                              Periodo = oportunidad.Periodo,
                              TiempoImplantacion = oportunidad.TiempoImplantacion,
                              TiempoProyecto = oportunidad.TiempoProyecto,
                              FlagGo = lineaNegocio.FlagGo
                          }).AsNoTracking().Single<OportunidadDtoResponse>();
            return result;
        }

    }
}
