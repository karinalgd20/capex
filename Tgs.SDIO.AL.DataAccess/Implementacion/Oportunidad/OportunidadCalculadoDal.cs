﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadCalculadoDal : Repository<OportunidadCalculado>, IOportunidadCalculadoDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadCalculadoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<OportunidadCalculadoDtoResponse> ListaOportunidadCalculado(OportunidadCalculadoDtoRequest dto)
        {
            var lista = (from c in context.OportunidadCalculado
                         where
                         c.IdOportunidadLineaNegocio == dto.IdOportunidadLineaNegocio &&
                         c.IdConceptoFinanciero == dto.IdConceptoFinanciero &&
                         c.IdEstado == dto.IdEstado &&
                         c.TipoFicha == dto.TipoFicha
                         select new OportunidadCalculadoDtoResponse
                         {
                             IdOportunidadCalculado = c.IdOportunidadCalculado,
                             Anio = c.Anio,
                             Mes = c.Mes,
                             Monto = c.Monto
                         }).ToList();
            return lista;
        }
    }
}
