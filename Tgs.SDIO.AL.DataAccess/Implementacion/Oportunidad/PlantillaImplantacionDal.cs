﻿using System.Data.Entity;
using System.Linq;

using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class PlantillaImplantacionDal : Repository<PlantillaImplantacion>, IPlantillaImplantacionDal
    {
        readonly DioContext context;
        public PlantillaImplantacionDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        #region - No Transaccionales - 
        public PlantillaImplantacionPaginadoDtoResponse ListarPlantillaImplantacionPaginado(PlantillaImplantacionDtoRequest filtro)
        {
            var lista = (from pli in context.PlantillaImplantacion
                         join tecd in context.Maestra.Where(x => x.IdRelacion == 89) on pli.IdTipoEnlaceCircuitoDatos.ToString() equals tecd.Valor
                         join tccd in context.Maestra.Where(x => x.IdRelacion == 540) on pli.IdTipoCircuitoDatos.ToString() equals tccd.Valor
                         join sgcd in context.ServicioGrupo on pli.IdServicioGrupoCircuitoDatos equals sgcd.IdServicioGrupo
                         join ccd in context.Costo on pli.IdCostoCircuitoDatos equals ccd.IdCosto
                         join leftcv5ca in context.Costo on pli.IdVozTos5CaudalAntiguo equals leftcv5ca.IdCosto into rcv5ca
                         from cv5ca in rcv5ca.DefaultIfEmpty()
                         join leftcv4ca in context.Costo on pli.IdVozTos4CaudalAntiguo equals leftcv4ca.IdCosto into rcv4ca
                         from cv4ca in rcv4ca.DefaultIfEmpty()
                         join leftcv3ca in context.Costo on pli.IdVozTos3CaudalAntiguo equals leftcv3ca.IdCosto into rcv3ca
                         from cv3ca in rcv3ca.DefaultIfEmpty()
                         join leftcv2ca in context.Costo on pli.IdVozTos2CaudalAntiguo equals leftcv2ca.IdCosto into rcv2ca
                         from cv2ca in rcv2ca.DefaultIfEmpty()
                         join leftcv1ca in context.Costo on pli.IdVozTos1CaudalAntiguo equals leftcv1ca.IdCosto into rcv1ca
                         from cv1ca in rcv1ca.DefaultIfEmpty()
                         join leftcv0ca in context.Costo on pli.IdVozTos0CaudalAntiguo equals leftcv0ca.IdCosto into rcv0ca
                         from cv0ca in rcv0ca.DefaultIfEmpty()
                         join ai in context.Maestra.Where(x => x.IdRelacion == 543) on pli.IdAccionIsis.ToString() equals ai.Valor
                         join teso in context.Maestra.Where(x => x.IdRelacion == 89) on pli.IdTipoEnlaceServicioOfertado.ToString() equals teso.Valor
                         join tcso in context.Maestra.Where(x => x.IdRelacion == 540) on pli.IdTipoServicioOfertado.ToString() equals tcso.Valor
                         join sgso in context.ServicioGrupo on pli.IdServicioGrupoServicioOfertado equals sgso.IdServicioGrupo
                         where
                         pli.IdEstado == 1
                         && pli.IdOportunidad.Equals(filtro.IdOportunidad)
                         select new PlantillaImplantacionDtoResponse
                         {
                             Id = pli.Id,
                             IdOportunidad = pli.IdOportunidad,
                             DescripcionTipoEnlaceCircuitoDatos = tecd.Descripcion,
                             DescripcionTipoCircuitoDatos = tccd.Descripcion,
                             NumeroCircuitoDatos = pli.NumeroCircuitoDatos,
                             ConectadoCircuitoDatos = pli.ConectadoCircuitoDatos,
                             DescripcionServicioGrupoCircuitoDatos = sgcd.Descripcion,
                             DescripcionCostoCircuitoDatos = ccd.Descripcion,
                             LargaDistanciaNacionalCircuitoDatos = pli.LargaDistanciaNacionalCircuitoDatos,
                             DescripcionVozTos5CaudalAntiguo = cv5ca.Descripcion,
                             DescripcionVozTos4CaudalAntiguo = cv4ca.Descripcion,
                             DescripcionVozTos3CaudalAntiguo = cv3ca.Descripcion,
                             DescripcionVozTos2CaudalAntiguo = cv2ca.Descripcion,
                             DescripcionVozTos1CaudalAntiguo = cv1ca.Descripcion,
                             DescripcionVozTos0CaudalAntiguo = cv0ca.Descripcion,
                             UltimaMillaCircuitoDatos = pli.UltimaMillaCircuitoDatos,
                             EquipoCpeCircuitoDatos = pli.EquipoCpeCircuitoDatos,
                             EquipoTerminalCircuitoDatos = pli.EquipoTerminalCircuitoDatos,
                             VrfCircuitoDatos = pli.VrfCircuitoDatos,

                             DescripcionAccionIsis = ai.Descripcion,
                             DescripcionTipoEnlaceServicioOfertado = teso.Descripcion,
                             DescripcionTipoServicioOfertado = tcso.Descripcion,
                             NumeroServicioOfertado = pli.NumeroServicioOfertado,
                             DescripcionServicioGrupoServicioOfertado = sgso.Descripcion

                         }).AsNoTracking()
                         .Distinct()
                         .OrderBy(x => x.Id)
                         .ToPagedList(filtro.Indice, filtro.Tamanio);

            return new PlantillaImplantacionPaginadoDtoResponse
            {
                ListaPlantillaImplantacion = lista.ToList(),
                TotalItemCount = lista.TotalItemCount
            };
        }
        #endregion
    }
}