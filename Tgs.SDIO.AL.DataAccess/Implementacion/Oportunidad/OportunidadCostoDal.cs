﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadCostoDal : Repository<OportunidadCosto>, IOportunidadCostoDal
    {
        readonly DioContext context;
        public OportunidadCostoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public OportunidadCostoPaginadoDtoResponse ListaOportunidadCostoPaginado(OportunidadCostoDtoRequest oportunidadCosto)
        {
                         var query = (from obj in context.Set<OportunidadCosto>()
                                      join c in context.Set<Costo>() on obj.IdCosto equals c.IdCosto
                                      where
                             // obj.Descripcion.Contains(OportunidadCosto.Descripcion) &&
                           obj.IdOportunidadLineaNegocio== oportunidadCosto.IdOportunidadLineaNegocio &&
                              c.IdTipoCosto == oportunidadCosto.IdTipoCosto
                         //     && obj.IdEstado == Generales.Estados.Activo
                         orderby obj.FechaCreacion ascending
                         select new OportunidadCostoDtoResponse
                         {
                             IdOportunidadCosto = obj.IdOportunidadCosto, 
                             IdOportunidadLineaNegocio=obj.IdOportunidadLineaNegocio,
                             Descripcion = c.Descripcion,
                             Monto = obj.Monto,
                             PorcentajeGarantizado=obj.PorcentajeGarantizado,
                             CodigoModelo=obj.CodigoModelo,
                             Instalacion=obj.Instalacion,
                             Modelo=obj.Modelo,
                             CostoSegmentoSatelital=obj.CostoSegmentoSatelital,
                             PorcentajeSobresuscripcion=obj.PorcentajeSobresuscripcion,
                             AntenaCasaClienteUSD=obj.AntenaCasaClienteUSD
                         }).AsNoTracking().Distinct().OrderBy(x => x.IdOportunidadLineaNegocio).ToPagedList(oportunidadCosto.Indice, oportunidadCosto.Tamanio);

                var resultado = new OportunidadCostoPaginadoDtoResponse
                {
                    ListOportunidadCostoDtoResponse = query.ToList(),
                    TotalItemCount = query.TotalItemCount
                };
                return resultado;
        }
    }
}
