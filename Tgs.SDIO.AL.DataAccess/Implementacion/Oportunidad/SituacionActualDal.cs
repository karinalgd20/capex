﻿using System;
using System.Data.Entity;
using System.Linq;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class SituacionActualDal : Repository<SituacionActual>, ISituacionActualDal
    {
        readonly DioContext context;
        public SituacionActualDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        #region  - No Transaccional -
        public SituacionActualDtoResponse ObtenerPorIdOportunidad(SituacionActualDtoRequest request)
        {
            var result = (from st in context.SituacionActual
                          where
                          st.IdOportunidad.Equals(request.IdOportunidad)
                          && (st.IdTipo.Equals(request.IdTipo))
                          select new SituacionActualDtoResponse
                          {
                              Id = st.Id,
                              IdOportunidad = st.IdOportunidad,
                              IdTipo = st.IdTipo,
                              Descripcion = st.Descripcion
                          }).AsNoTracking().FirstOrDefault();

            return result;
        }
        #endregion
    }
}