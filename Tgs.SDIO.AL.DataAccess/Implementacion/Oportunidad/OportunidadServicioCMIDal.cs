﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadServicioCMIDal : Repository<OportunidadServicioCMI>, IOportunidadServicioCMIDal
    {
        readonly DioContext context;

        public OportunidadServicioCMIDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }


        public void RegistrarFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalleFlujo)
        {

        }

        public ProcesoResponse RegistrarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidadServicioCMI)
        {
            throw new NotImplementedException();
        }

        public OportunidadServicioCMIPaginadoDtoResponse ListarOportunidadServicioCMIPaginado(OportunidadServicioCMIDtoRequest oportunidadServicioCMI)
        {
            var lista = (from osCMI in context.Set<OportunidadServicioCMI>()
                         join sCMI in context.Set<ServicioCMI>() on osCMI.IdServicioCMI equals sCMI.IdServicioCMI
                         join gp in context.Set<GerenciaProducto>() on sCMI.IdGerenciaProducto equals gp.IdGerenciaProducto
                         join cc in context.Set<CentroCosto>() on sCMI.IdCentroCosto equals cc.IdCentroCosto
                         join ln in context.Set<Linea>() on sCMI.IdLinea equals ln.IdLinea
                         join srv in context.Set<ServicioCMILineaSubLinea>() on osCMI.IdServicioCMI equals srv.IdServicioCMI
                         join sln in context.Set<SubLinea>() on ln.IdLinea equals sln.IdLinea

                         where (osCMI.IdOportunidadLineaNegocio == oportunidadServicioCMI.IdOportunidadLineaNegocio &&
                                sCMI.IdEstado == oportunidadServicioCMI.IdEstado &&
                                sln.IdSubLinea == srv.IdSubLinea &&
                                osCMI.IdEstado == oportunidadServicioCMI.IdEstado)
                         select new OportunidadServicioCMIDtoResponse
                         {
                             IdOportunidadLineaNegocio = osCMI.IdOportunidadLineaNegocio,
                             IdServicioCMI = osCMI.IdServicioCMI,
                             CodigoCMI = sCMI.CodigoCMI,
                             DescripcionCMI = sCMI.DescripcionCMI,
                             Porcentaje = osCMI.Porcentaje,
                             Linea = ln.Descripcion,
                             GerenciaProducto = gp.Descripcion,
                             CentroCosto = cc.Descripcion,
                             Producto_AF = sCMI.DescripcionPlantilla,
                             SubLinea = sln.Descripcion
                         }).AsNoTracking().OrderBy(x => x.IdServicioCMI).ToPagedList(oportunidadServicioCMI.Indice, oportunidadServicioCMI.Tamanio);

            var resultado = new OportunidadServicioCMIPaginadoDtoResponse
            {
                ListOportunidadServicioCMIDtoResponse = lista.ToList(),
                TotalItemCount = lista.TotalItemCount
            };

            return resultado;
        }
    }
}
