﻿using System.Data.Entity;
using System.Linq;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class VisitaDetalleDal : Repository<VisitaDetalle>, IVisitaDetalleDal
    {
        readonly DioContext context;
        public VisitaDetalleDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        #region - No Transaccionales -

        public VisitaDetallePaginadoDtoResponse ListarVisitaDetallePaginado(VisitaDetalleDtoRequest visitaDetalle)
        {
            var lista = (from vd in context.VisitaDetalle
                         where vd.IdVisita.Equals(visitaDetalle.IdVisita) && (vd.IdEstado == 1)
                         select new VisitaDetalleDtoResponse
                         {
                             Id = vd.Id,
                             IdVisita = vd.IdVisita,
                             FechaAcuerdo = vd.FechaAcuerdo,
                             DescripcionPunto = vd.DescripcionPunto,
                             Responsable = vd.Responsable,
                             Cumplimiento = vd.Cumplimiento
                         }).AsNoTracking()
                         .Distinct()
                         .OrderBy(x => x.DescripcionPunto)
                         .ToPagedList(visitaDetalle.Indice, visitaDetalle.Tamanio);

            return new VisitaDetallePaginadoDtoResponse
            {
                ListaVisitasDetalle = lista.ToList(),
                TotalItemCount = lista.TotalItemCount
            };
        }

        #endregion
    }
}