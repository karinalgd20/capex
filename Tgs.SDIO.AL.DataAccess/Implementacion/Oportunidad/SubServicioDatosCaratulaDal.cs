﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class SubServicioDatosCaratulaDal : Repository<SubServicioDatosCaratula>, ISubServicioDatosCaratulaDal
    {
        readonly DioContext context;
        public SubServicioDatosCaratulaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
                
        public List<SubServicioDatosCaratulaDtoResponse> ListarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest casonegocio)
        {
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };
            var IdGrupo = new SqlParameter { ParameterName = "IdGrupo", Value = casonegocio.IdGrupo, SqlDbType = SqlDbType.Int };


            var OportunidadFlujoCaja = context.ExecuteQuery<SubServicioDatosCaratulaDtoResponse>("[OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_CARATULA] @IdOportunidadLineaNegocio,@IdEstado,@IdGrupo",
                    IdOportunidadLineaNegocio, IdEstado, IdGrupo).ToList<SubServicioDatosCaratulaDtoResponse>();

            return OportunidadFlujoCaja;
        } 

        public ProcesoResponse RegistrarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {
            throw new NotImplementedException();
        }

        public ProcesoResponse ActualizarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {
            throw new NotImplementedException();
        }

        public ProcesoResponse EliminarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {
            throw new NotImplementedException();
        }

        public SubServicioDatosCaratulaDtoResponse ObtenerSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {
            throw new NotImplementedException();
        }
    }
}
