﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class ConceptoDatosCapexDal : Repository<ConceptoDatosCapex>, IConceptoDatosCapexDal
    {
        readonly DioContext context;
        public ConceptoDatosCapexDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ConceptoDatosCapexDtoResponse> ListarConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex)
        {
            var pIDPROYECTO = new SqlParameter { ParameterName = "pIDPROYECTO", Value = capex.IdProyecto, SqlDbType = SqlDbType.Int };
            var pIDLINEAPRODUCTO = new SqlParameter { ParameterName = "pIDLINEAPRODUCTO", Value = capex.IdLineaProducto, SqlDbType = SqlDbType.Int };
            var pIDPESTANA = new SqlParameter { ParameterName = "pIDPESTANA", Value = capex.IdPestana, SqlDbType = SqlDbType.Int };
            var pIDGRUPO = new SqlParameter { ParameterName = "pIDGRUPO", Value = capex.IdGrupo, SqlDbType = SqlDbType.Int };
            var pIDESTADO = new SqlParameter { ParameterName = "pIDESTADO", Value = capex.IdEstado, SqlDbType = SqlDbType.Int };

            var lista = context.ExecuteQuery<ConceptoDatosCapexDtoResponse>("[OPORTUNIDAD].[usp_OportunidadListasCapex] @pIDPROYECTO, @pIDLINEAPRODUCTO, @pIDPESTANA,@pIDGRUPO, @pIDESTADO", pIDPROYECTO, pIDLINEAPRODUCTO, pIDPESTANA, pIDGRUPO, pIDESTADO).ToList<ConceptoDatosCapexDtoResponse>();

            return lista;
        }
    }
}
