﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using static Tgs.SDIO.Util.Constantes.Generales;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class SedeDal : Repository<Sede>, ISedeDal
    {
        readonly DioContext context;
        public SedeDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        #region - No Transaccionales -
        public SedeDtoResponse ObtenerSedePorId(SedeDtoRequest request)
        {
            var queryInstalacin = (from T0 in context.Set<Tgs.SDIO.Entities.Entities.Oportunidad.SedeInstalacion>()
                             join T1 in context.Set<Sede>() on T0.IdSede equals T1.Id
                             where T0.Id == request.IdSedeInstalacion && (T1.IdEstado == Activo || T1.IdEstado == Inactivo)
                             select new SedeDtoResponse
                             {
                                 Id = T1.Id,
                             }).AsNoTracking().FirstOrDefault();

            var queryUbigeo = ObtenerCodDetalleUbigeo(queryInstalacin.Id);

            var querySede = (from T0 in context.Set<Tgs.SDIO.Entities.Entities.Oportunidad.SedeInstalacion>()
                                   join T1 in context.Set<Sede>() on T0.IdSede equals T1.Id
                                    where T0.Id == request.IdSedeInstalacion && (T1.IdEstado == Activo || T1.IdEstado == Inactivo)
                                   select new SedeDtoResponse
                                   {
                                       Id = T1.Id,
                                       Codigo = T1.Codigo,
                                       IdCliente = T1.IdCliente,
                                       IdTipoEnlace = T1.IdTipoEnlace,
                                       IdTipoSede = T1.IdTipoSede,
                                       IdAccesoCliente = T1.IdAccesoCliente,
                                       IdTendidoExterno = T1.IdTendidoExterno,
                                       NumeroPisos = T1.NumeroPisos,
                                       IdUbigeo = T1.IdUbigeo,
                                       IdTipoVia = T1.IdTipoVia,
                                       Departamento = queryUbigeo.CodigoDepartamento,
                                       Provincia = queryUbigeo.CodigoProvincia,
                                       Distrito = queryUbigeo.CodigoDistrito,
                                       Direccion = T1.Direccion,
                                       Latitud = T1.Latitud,
                                       Longitud = T1.Longitud,
                                       IdTipoServicio = T1.IdTipoServicio,
                                       Piso = T1.Piso,
                                       Interior = T1.Interior,
                                       Manzana = T1.Manzana,
                                       Lote = T1.Lote,
                                       IdEstado = T1.IdEstado,
                                       IdUsuarioCreacion = T1.IdUsuarioCreacion,
                                       FechaCreacion = T1.FechaCreacion,
                                       IdUsuarioEdicion = T1.IdUsuarioEdicion,
                                       FechaEdicion = T1.FechaEdicion,
                                       DesRequerimiento = T0.DesRequerimiento,
                                       FlgCotizacionReferencial = T0.FlgCotizacionReferencial,
                                       FlgRequisitosEspeciales = T0.FlgRequisitosEspeciales,
                                       IdSedeInstalacion = T0.Id,
                                   }).AsNoTracking().FirstOrDefault();

            return querySede;
        }
        public ServicioSedeInstalacionDtoResponse ObtenerServicioSedeInstalacionPorId(ServicioSedeInstalacionDtoRequest request)
        {

            var querySede = (from T0 in context.Set<Tgs.SDIO.Entities.Entities.Oportunidad.ServicioSedeInstalacion>()
                             where T0.Id == request.IdServicioSedeInstalacion && (T0.IdEstado == Activo || T0.IdEstado == Inactivo)
                             select new ServicioSedeInstalacionDtoResponse
                             {
                                 Id = T0.Id,
                                 IdSedeInstalacion = T0.IdSedeInstalacion,
                                 IdFlujoCaja = T0.IdFlujoCaja,
                                 IdEstado = T0.IdEstado,
                                 IdUsuarioCreacion = T0.IdUsuarioCreacion,
                                 FechaCreacion = T0.FechaCreacion,
                                 IdUsuarioEdicion = T0.IdUsuarioEdicion,
                                 FechaEdicion = T0.FechaEdicion,
                             }).AsNoTracking().FirstOrDefault();

            return querySede;
        }
        public ClienteDtoResponse ObtenerClientePorIdOportunidad(OportunidadDtoRequest request)
        {
            var queryOportunida = (from T0 in context.Set<Tgs.SDIO.Entities.Entities.Oportunidad.Oportunidad>()
                                   join T1 in context.Set<Cliente>() on T0.IdCliente equals T1.IdCliente
                                    where T0.IdOportunidad == request.IdOportunidad && T1.IdTipoIdentificadorFiscalTm == 1 && (T1.IdEstado == Activo || T1.IdEstado == Inactivo)
                                   select new ClienteDtoResponse
                                   {
                                       Descripcion = T1.Descripcion,
                                       NumeroIdentificadorFiscal = T1.NumeroIdentificadorFiscal,
                                       IdCliente = T1.IdCliente,
                                   }).AsNoTracking().FirstOrDefault();

            return queryOportunida;
        }
        public SedeInstalacionDtoResponse obtenerSedeInstalada(int IdSede, int IdOportunidad)
        {
            var query = (from T0 in context.Set<SedeInstalacion>()
                         where T0.IdOportunidad == IdOportunidad && T0.IdSede == IdSede && T0.IdEstado == EstadoSedeInstalacion.Activo
                         select new SedeInstalacionDtoResponse
                         {
                             Id = T0.Id,
                         }).AsNoTracking().FirstOrDefault();

            return query;
        }
        public List<SedeDtoResponse> ListarSedeInstalacion(OportunidadDtoRequest oportunidad)
        {
            var pIDOportunidad = new SqlParameter { ParameterName = "IDOportunidad", Value = oportunidad.IdOportunidad, SqlDbType = SqlDbType.Int };
            var Indice = new SqlParameter { ParameterName = "Indice", Value = oportunidad.Indice, SqlDbType = SqlDbType.Int };
            var Tamanio = new SqlParameter { ParameterName = "Tamanio", Value = oportunidad.Tamanio, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = EstadoSedeInstalacion.Activo, SqlDbType = SqlDbType.Int };
            var lista = context.ExecuteQuery<SedeDtoResponse>("OPORTUNIDAD.USP_LISTAR_SEDE_INSTALACION @IDOportunidad,@Indice,@Tamanio,@IdEstado",
                pIDOportunidad, Indice, Tamanio, IdEstado).ToList<SedeDtoResponse>();
            return lista;
        }
        public SedePaginadoDtoResponse ListarSedeCliente(OportunidadDtoRequest request)
        {
            var queryOportunidad = ObtenerClientePorIdOportunidad(request);

            var lista = (from TC in context.Set<Cliente>()
                         join T0 in context.Set<Sede>() on TC.IdCliente equals T0.IdCliente
                         //join T4 in context.Set <SedeInstalacion>() on T0.Id equals T4.IdSede
                         join T1 in context.Set<Ubigeo>() on T0.IdUbigeo equals T1.Id
                         join T2 in context.Set<Ubigeo>().Where(x => x.CodigoDistrito == null) on T1.CodigoProvincia equals T2.CodigoProvincia
                         join T3 in context.Set<Ubigeo>().Where(x => x.CodigoProvincia == null) on T2.CodigoDepartamento equals T3.CodigoDepartamento
                         //where (TC.IdCliente == queryOportunidad.IdCliente && T4.IdOportunidad != request.IdOportunidad)
                         where (TC.IdCliente == queryOportunidad.IdCliente)
                         orderby TC.IdCliente descending
                         select new SedeDtoResponse
                         {
                             Id = T0.Id,
                             Direccion = T0.Direccion,
                             Departamento = T3.Nombre,
                             Provincia = T2.Nombre,
                             Distrito = T1.Nombre,
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var resultado = new SedePaginadoDtoResponse
            {
                ListSedeDto = lista.ToList(),
                TotalItemCount = lista.TotalItemCount
            };

            return resultado;

        }
        public SedeDtoResponse ObtenerCodUbigeo(SedeDtoRequest request)
        {
            var queryUbigeo = (from T0 in context.Set<Ubigeo>()
                                   where T0.CodigoDepartamento == request.CodigoDepartamento && T0.CodigoProvincia == request.CodigoProvincia && T0.CodigoDistrito == request.CodigoDistrito && (T0.IdEstado == Activo || T0.IdEstado == Inactivo)
                                   select new SedeDtoResponse
                                   {
                                       Id =T0.Id,
                                   }).AsNoTracking().FirstOrDefault();

            return queryUbigeo;
        }
        public UbigeoDtoResponse ObtenerCodDetalleUbigeo(int Id)
        {
            var querySed = (from T0 in context.Set<Sede>()
                         where T0.Id == Id
                         select new SedeDtoResponse
                         {
                            IdUbigeo = T0.IdUbigeo,
                         }).AsNoTracking().FirstOrDefault();

            var query = (from T0 in context.Set<Ubigeo>()
                         where T0.Id == querySed.IdUbigeo
                         select new UbigeoDtoResponse
                         {
                             CodigoDepartamento = T0.CodigoDepartamento,
                             CodigoProvincia = T0.CodigoProvincia,
                             CodigoDistrito = T0.CodigoDistrito,
                         }).AsNoTracking().FirstOrDefault();
            return query;
        }
        public SedePaginadoDtoResponse ListarSedeServiciosPaginado(SedeDtoRequest request)
        {
            var query = (from o in context.Oportunidad
                         join c in context.Cliente on o.IdCliente equals c.IdCliente
                         join s in context.Sede on c.IdCliente equals s.IdCliente
                         join ts in context.Maestra.Where(x => x.IdRelacion == 406) on s.IdTipoSede.ToString() equals ts.Valor
                         join d in context.Ubigeo on s.IdUbigeo equals d.Id
                         join p in context.Ubigeo.Where(x => x.CodigoDistrito == null) on d.CodigoProvincia equals p.CodigoProvincia
                         join dp in context.Ubigeo.Where(x => x.CodigoProvincia == null) on p.CodigoDepartamento equals dp.CodigoDepartamento
                         join si in context.SedeInstalacion.Where(x => x.IdOportunidad == request.IdOportunidad && x.IdEstado == 1) on s.Id equals si.IdSede
                         join ssi in context.ServicioSedeInstalacion on si.Id equals ssi.IdSedeInstalacion
                         join ofc in context.OportunidadFlujoCaja on ssi.IdFlujoCaja equals ofc.IdFlujoCaja
                         join srv in context.Servicio on ofc.IdServicio equals srv.IdServicio
                         join sg in context.ServicioGrupo on srv.IdServicioGrupo equals sg.IdServicioGrupo
                         join oc in context.OportunidadCosto on ofc.IdOportunidadCosto equals oc.IdOportunidadCosto
                         join cs in context.Costo on oc.IdCosto equals cs.IdCosto
                         where o.IdOportunidad.Equals(request.IdOportunidad)
                         select new SedeDtoResponse
                         {
                             IdSede = s.Id,
                             IdSedeInstalacion = si.Id,
                             TipoSede = ts.Descripcion,
                             Direccion = s.Direccion,
                             Departamento = dp.Nombre,
                             Provincia = p.Nombre,
                             Distrito = d.Nombre,
                             Servicio = srv.Descripcion,
                             ServicioGrupo = sg.Descripcion,
                             Costo = cs.Descripcion,
                             IdFlujoCaja = ofc.IdFlujoCaja
                         }).AsNoTracking()
                         .OrderBy(x=> x.ServicioGrupo)
                         .ToPagedList(request.Indice, request.Tamanio);

            return new SedePaginadoDtoResponse
            {
                TotalItemCount = query.TotalItemCount,
                ListSedeDto = query.ToList()
            };
        }
        public SedeDtoResponse DetalleSedePorId(SedeDtoRequest request)
        {
            var sede = (from s in context.Sede
                         join ts in context.Maestra.Where(x => x.IdRelacion == 406) on s.IdTipoSede.ToString() equals ts.Valor
                         join d in context.Ubigeo on s.IdUbigeo equals d.Id
                         join p in context.Ubigeo.Where(x => x.CodigoDistrito == null) on d.CodigoProvincia equals p.CodigoProvincia
                         join dp in context.Ubigeo.Where(x => x.CodigoProvincia == null) on p.CodigoDepartamento equals dp.CodigoDepartamento
                         where s.Id.Equals(request.IdSede)
                         select new SedeDtoResponse
                         {
                             IdSede = s.Id,
                             TipoSede = ts.Descripcion,
                             Direccion = s.Direccion,
                             Departamento = dp.Nombre,
                             Provincia = p.Nombre,
                             Distrito = d.Nombre
                         }).AsNoTracking().FirstOrDefault();

            return sede; 
        }

        public SedeDtoResponse DireccionBuscar(SedeDtoRequest request)
        {
            var lista = (from T0 in context.Set<Ubigeo>()
                         join T1 in context.Set<Ubigeo>() on T0.CodigoDepartamento equals T1.CodigoDepartamento
                         join T2 in context.Set<Ubigeo>().Where(x => x.CodigoDistrito == null) on T1.CodigoProvincia equals T2.CodigoProvincia
                         join T3 in context.Set<Ubigeo>().Where(x => x.CodigoProvincia == null) on T2.CodigoDepartamento equals T3.CodigoDepartamento
                         //where (TC.IdCliente == queryOportunidad.IdCliente && T4.IdOportunidad != request.IdOportunidad)
                         where (T1.CodigoDistrito == request.CodigoDistrito)
                         orderby T0.Id descending
                         select new SedeDtoResponse
                         {
                             LocalizacionSede = T1.Nombre +  "," + T2.Nombre + "," + T3.Nombre,
                         }).AsNoTracking().FirstOrDefault();
            return lista;
        }

        #endregion
    }
}