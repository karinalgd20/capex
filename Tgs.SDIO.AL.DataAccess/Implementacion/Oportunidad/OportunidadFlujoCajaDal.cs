﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Linq;

using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Mensajes.Comun;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadFlujoCajaDal : Repository<OportunidadFlujoCaja>, IOportunidadFlujoCajaDal
    {
        readonly DioContext context;
        public OportunidadFlujoCajaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        #region - Transaccionales -
        public List<OportunidadFlujoCajaDtoResponse> GeneraCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdCasoNegocio = new SqlParameter { ParameterName = "IdCasoNegocio", Value = casonegocio.IdCasoNegocio, SqlDbType = SqlDbType.Int };
            var IdUsuarioCreacion = new SqlParameter { ParameterName = "IdUsuarioCreacion", Value = casonegocio.IdUsuarioCreacion, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };

            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_INSERTAR_CASO_NEGOCIO] @IdOportunidadLineaNegocio,@IdCasoNegocio,@IdUsuarioCreacion,@IdEstado",
                    IdOportunidadLineaNegocio, IdCasoNegocio, IdUsuarioCreacion, IdEstado).ToList<OportunidadFlujoCajaDtoResponse>();

            return OportunidadFlujoCaja;
        }
        public List<OportunidadFlujoCajaDtoResponse> GeneraServicio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdServicio = new SqlParameter { ParameterName = "IdServicio", Value = casonegocio.IdServicio, SqlDbType = SqlDbType.Int };
            var IdUsuarioCreacion = new SqlParameter { ParameterName = "IdUsuarioCreacion", Value = casonegocio.IdUsuarioCreacion, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };

            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_INSERTAR_SERVICIO] @IdOportunidadLineaNegocio,@IdServicio,@IdUsuarioCreacion,@IdEstado",
                    IdOportunidadLineaNegocio, IdServicio, IdUsuarioCreacion, IdEstado).ToList<OportunidadFlujoCajaDtoResponse>();

            return OportunidadFlujoCaja;
        }
        public ProcesoResponse EliminarCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var resultado = new ProcesoResponse();
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdCasoNegocio = new SqlParameter { ParameterName = "IdCasoNegocio", Value = casonegocio.IdCasoNegocio, SqlDbType = SqlDbType.Int };
            var IdUsuarioEdicion = new SqlParameter { ParameterName = "IdUsuarioEdicion", Value = casonegocio.IdUsuarioEdicion, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };

            resultado.TipoRespuesta = context.ExecuteCommand("[OPORTUNIDAD].[USP_ELIMINAR_CASO_NEGOCIO] @IdOportunidadLineaNegocio,@IdCasoNegocio,@IdUsuarioEdicion,@IdEstado",
                                                                IdOportunidadLineaNegocio, IdCasoNegocio, IdUsuarioEdicion, IdEstado);
            resultado.Mensaje = MensajesGeneralComun.EliminarCasoNegocio;
            return resultado;
        }
        #endregion

        #region - No Transaccionales -
        public List<OportunidadFlujoCajaDtoResponse> ListarOportunidadFlujoCajaBandeja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return new List<OportunidadFlujoCajaDtoResponse>();
        }
        public List<ListaDtoResponse> ListaAnoMesProyecto(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var ListaTiempoProyecto = new List<ListaDtoResponse>();
            var tiempo = (from o in context.Set<Entities.Entities.Oportunidad.Oportunidad>()
                          join oln in context.Set<OportunidadLineaNegocio>() on o.IdOportunidad equals oln.IdOportunidad
                          where oln.IdOportunidadLineaNegocio == flujocaja.IdOportunidadLineaNegocio
                          select new OportunidadDtoResponse()
                          {
                              Fecha = o.Fecha,
                              Periodo = o.Periodo + o.TiempoImplantacion
                          }

                          ).Single();

            decimal ano = decimal.Ceiling((decimal)tiempo.Periodo / 12);
            DateTime fecha = DateTime.Parse(tiempo.Fecha.ToLongDateString());

            for (int i = 0; i <= tiempo.Periodo; i++)
            {
                var c = new ListaDtoResponse()
                {
                    Codigo = (i + 1).ToString(),
                    Descripcion = fecha.AddMonths(i).Year.ToString() + " - " + fecha.AddMonths(i).Month.ToString()
                };
                ListaTiempoProyecto.Add(c);
            }

            return ListaTiempoProyecto;
        }
        public List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };
            var IdGrupo = new SqlParameter { ParameterName = "IdGrupo", Value = casonegocio.IdGrupo, SqlDbType = SqlDbType.Int };
            var Indice = new SqlParameter { ParameterName = "Indice", Value = casonegocio.Indice, SqlDbType = SqlDbType.Int };
            var Tamanio = new SqlParameter { ParameterName = "Tamanio", Value = casonegocio.Tamanio, SqlDbType = SqlDbType.Int };

            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_ECAPEX] @IdOportunidadLineaNegocio,@IdEstado,@IdGrupo,@Indice,@Tamanio",
                    IdOportunidadLineaNegocio, IdEstado, IdGrupo,Indice,Tamanio).ToList<OportunidadFlujoCajaDtoResponse>();

            return OportunidadFlujoCaja;
        }
        public List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadEcapexTotal(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };
            var IdGrupo = new SqlParameter { ParameterName = "IdGrupo", Value = casonegocio.IdGrupo, SqlDbType = SqlDbType.Int };

            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_ECAPEX_TOTAL] @IdOportunidadLineaNegocio,@IdEstado,@IdGrupo",
                    IdOportunidadLineaNegocio, IdEstado, IdGrupo).ToList<OportunidadFlujoCajaDtoResponse>();

            return OportunidadFlujoCaja;
        }
        public List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };
            var IdGrupo = new SqlParameter { ParameterName = "IdGrupo", Value = casonegocio.IdGrupo, SqlDbType = SqlDbType.Int };
            var Indice = new SqlParameter { ParameterName = "Indice", Value = casonegocio.Indice, SqlDbType = SqlDbType.Int };
            var Tamanio = new SqlParameter { ParameterName = "Tamanio", Value = casonegocio.Tamanio, SqlDbType = SqlDbType.Int };

            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_CARATULA] @IdOportunidadLineaNegocio,@IdEstado,@IdGrupo,@Indice,@Tamanio",
                    IdOportunidadLineaNegocio, IdEstado, IdGrupo, Indice, Tamanio).ToList<OportunidadFlujoCajaDtoResponse>();
            
            return OportunidadFlujoCaja;
        }
        public List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadCaratulaTotal(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };
            var IdGrupo = new SqlParameter { ParameterName = "IdGrupo", Value = casonegocio.IdGrupo, SqlDbType = SqlDbType.Int };


            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_CARATULA_TOTAL] @IdOportunidadLineaNegocio,@IdEstado,@IdGrupo",
                    IdOportunidadLineaNegocio, IdEstado, IdGrupo).ToList<OportunidadFlujoCajaDtoResponse>();

            return OportunidadFlujoCaja;
        }
        public OportunidadFlujoCajaPaginadoDtoResponse ListarSubservicioPaginado(OportunidadFlujoCajaDtoRequest solicitud)
        {
            var lista = (from ofc in context.Set<OportunidadFlujoCaja>()
                         join ss in context.Set<SubServicio>() on ofc.IdSubServicio equals ss.IdSubServicio
                         join fo in context.Set<Formula>() on 7 equals fo.IdFormula
                         join fd in context.Set<FormulaDetalle>() on fo.IdFormula equals fd.IdFormula
                         join m in context.Set<Maestra>() on ofc.IdPeriodos.ToString() equals m.Valor
                         join fcc in context.Set<OportunidadFlujoCajaConfiguracion>() on ofc.IdFlujoCaja equals fcc.IdFlujoCaja

                         where (ofc.IdOportunidadLineaNegocio == solicitud.IdOportunidadLineaNegocio &&
                         m.IdRelacion == 28 &&
                         ofc.IdEstado == Generales.Numeric.Uno
                         && fd.IdSubServicio == ofc.IdSubServicio
                         )
                         select new OportunidadFlujoCajaDtoResponse
                         {
                             IdSubServicio = ss.IdSubServicio,
                             IdFlujoCaja = ofc.IdFlujoCaja,
                             IdFlujoCajaConfiguracion = fcc.IdFlujoCajaConfiguracion,
                             IdPeriodos = ofc.IdPeriodos,
                             SubServicio = ss.Descripcion,
                             IdTipoCosto = ofc.IdTipoCosto,
                             CostoPreOperativo = fcc.CostoPreOperativo
                         }).AsNoTracking().OrderBy(x => x.IdSubServicio).ToPagedList(solicitud.Indice, solicitud.Tamanio);

            var resultado = new OportunidadFlujoCajaPaginadoDtoResponse
            {
                ListOportunidadFlujoCajaDtoResponse = lista.ToList(),
                TotalItemCount = lista.TotalItemCount
            };

            return resultado;
        }
        public OportunidadFlujoCajaDtoResponse ObtenerSubServicio(OportunidadFlujoCajaDtoRequest solicitud)
        {
            var subServicio = (from ofc in context.Set<OportunidadFlujoCaja>()
                               join ss in context.Set<SubServicio>() on ofc.IdSubServicio equals ss.IdSubServicio
                               join fo in context.Set<Formula>() on 7 equals fo.IdFormula
                               join fd in context.Set<FormulaDetalle>() on fo.IdFormula equals fd.IdFormula
                               join m in context.Set<Maestra>() on ofc.IdPeriodos.ToString() equals m.Valor
                               join fcc in context.Set<OportunidadFlujoCajaConfiguracion>() on ofc.IdFlujoCaja equals fcc.IdFlujoCaja

                               where (
                               ofc.IdOportunidadLineaNegocio == solicitud.IdOportunidadLineaNegocio &&
                               m.IdRelacion == 28 &&
                               ofc.IdEstado == Generales.Numeric.Uno &&
                               fd.IdSubServicio == ofc.IdSubServicio &&
                               ofc.IdSubServicio == solicitud.IdSubServicio
                               )
                               select new OportunidadFlujoCajaDtoResponse
                               {
                                   IdSubServicio = ss.IdSubServicio,
                                   IdFlujoCaja = ofc.IdFlujoCaja,
                                   IdFlujoCajaConfiguracion = fcc.IdFlujoCajaConfiguracion,
                                   IdPeriodos = ofc.IdPeriodos,
                                   SubServicio = ss.Descripcion,
                                   IdTipoCosto = ofc.IdTipoCosto,
                                   CostoPreOperativo = fcc.CostoPreOperativo
                               }).Single();

            return subServicio;

        }
        public OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var IdSubServicioDatosCapex = new SqlParameter { ParameterName = "IdSubServicioDatosCapex", Value = casonegocio.IdSubServicioDatosCapex, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };
            var IdGrupo = new SqlParameter { ParameterName = "IdGrupo", Value = casonegocio.IdGrupo, SqlDbType = SqlDbType.Int };


            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_OBTENER_OPORTUNIDAD_ECAPEX] @IdSubServicioDatosCapex,@IdEstado,@IdGrupo",
                    IdSubServicioDatosCapex, IdEstado, IdGrupo).Single<OportunidadFlujoCajaDtoResponse>();

            return OportunidadFlujoCaja;
        }
        public OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var IdSubServicioDatosCaratula = new SqlParameter { ParameterName = "IdSubServicioDatosCaratula", Value = casonegocio.IdSubServicioDatosCaratula, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };
            var IdGrupo = new SqlParameter { ParameterName = "IdGrupo", Value = casonegocio.IdGrupo, SqlDbType = SqlDbType.Int };


            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_OBTENER_OPORTUNIDAD_CARATULA] @IdSubServicioDatosCaratula,@IdEstado,@IdGrupo",
                    IdSubServicioDatosCaratula, IdEstado, IdGrupo).Single<OportunidadFlujoCajaDtoResponse>();

            return OportunidadFlujoCaja;
        }
        public OportunidadFlujoCajaPaginadoDtoResponse ListarServiciosCircuitosPaginado(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var listaFlujo = (from si in context.SedeInstalacion
                              join ssi in context.ServicioSedeInstalacion on si.Id equals ssi.IdSedeInstalacion
                              where si.IdOportunidad == flujocaja.IdOportunidad && si.IdEstado == 1
                              select new OportunidadFlujoCajaDtoRequest { IdFlujoCaja = ssi.IdFlujoCaja })
                              .AsNoTracking().ToList();

            flujocaja.ListIdFlujoCaja = new List<int>();
            if (listaFlujo.Any()) listaFlujo.ForEach(x => { flujocaja.ListIdFlujoCaja.Add(x.IdFlujoCaja); });

            var lista = (from o in context.Oportunidad
                         join oln in context.OportunidadLineaNegocio on o.IdOportunidad equals oln.IdOportunidad
                         join ofc in context.OportunidadFlujoCaja on oln.IdOportunidadLineaNegocio equals ofc.IdOportunidadLineaNegocio
                         join s in context.Servicio on ofc.IdServicio equals s.IdServicio
                         join sg in context.ServicioGrupo on s.IdServicioGrupo equals sg.IdServicioGrupo
                         join te in context.Maestra.Where(x => x.IdRelacion == 89) on s.IdTipoEnlace.ToString() equals te.Valor

                         join leftoc in context.OportunidadCosto on ofc.IdOportunidadCosto equals leftoc.IdOportunidadCosto into roc
                         from oc in roc.DefaultIfEmpty()
                         join leftCs in context.Costo on oc.IdCosto equals leftCs.IdCosto into rcs
                         from cs in rcs.DefaultIfEmpty()
                         join leftI in context.Maestra.Where(x => x.IdRelacion == 521) on ofc.IdInterfaz.Value.ToString() equals leftI.Valor into ri
                         from i in ri.DefaultIfEmpty()
                         join leftTss in context.Maestra.Where(x => x.IdRelacion == 529) on ofc.IdTipoServicioSISEGO.Value.ToString() equals leftTss.Valor into rtss
                         from tss in rtss.DefaultIfEmpty()

                         where
                         ofc.IdPestana == 1
                         && o.IdOportunidad == flujocaja.IdOportunidad
                         && ofc.IdEstado == 1
                         && (!flujocaja.ListIdFlujoCaja.Any() || !flujocaja.ListIdFlujoCaja.Contains(ofc.IdFlujoCaja))
                         orderby ofc.FechaCreacion ascending
                         select new OportunidadFlujoCajaDtoResponse
                         {
                             IdFlujoCaja = ofc.IdFlujoCaja,
                             IdServicio = ofc.IdServicio.Value,
                             Servicio = s.Descripcion,
                             ServicioGrupo = sg.Descripcion,
                             TipoEnlace = te.Descripcion,
                             VelocidadBW = (cs.Descripcion == null ? "" : cs.Descripcion),
                             Interfaz = (i.Descripcion == null ? "" : i.Descripcion),
                             TipoServicioSISEGO = (tss.Descripcion == null ? "" : tss.Descripcion)
                         })
                         .AsNoTracking()
                         .Distinct()
                         .OrderBy(x=> x.IdServicio)
                         .ToPagedList(flujocaja.Indice, flujocaja.Tamanio);

            return new OportunidadFlujoCajaPaginadoDtoResponse
            {
                ListOportunidadFlujoCajaDtoResponse = lista.ToList(),
                TotalItemCount = lista.TotalItemCount
            };
        }
        public List<ListaDtoResponse> ListaServicioPorOportunidadLineaNegocio(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var lista = (from o in context.Oportunidad
                         join oportLineaNegocio in context.OportunidadLineaNegocio on o.IdOportunidad equals oportLineaNegocio.IdOportunidad
                         join oportFlujoCaja in context.OportunidadFlujoCaja on oportLineaNegocio.IdOportunidadLineaNegocio equals oportFlujoCaja.IdOportunidadLineaNegocio
                         join s in context.Servicio on oportFlujoCaja.IdServicio equals s.IdServicio

                         where
                          o.IdOportunidad == flujocaja.IdOportunidad
                         orderby s.IdServicio ascending
                         select new ListaDtoResponse
                         {
                             Codigo = oportFlujoCaja.IdServicio.ToString(),
                             Descripcion = s.Descripcion
                         }).Distinct().ToList();

            return lista;
        }
        public List<ListaDtoResponse> ListarPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var lista = (from o in context.OportunidadFlujoCaja
                             //join oln in context.OportunidadLineaNegocio on o.IdOportunidad equals oln.IdOportunidad
                         join s in context.Servicio on o.IdServicio equals s.IdServicio

                         where       
                         o.IdOportunidadLineaNegocio == flujocaja.IdOportunidadLineaNegocio
                         && o.IdEstado == Estados.Activo && o.IdServicioCMI!=null                                                               
                         orderby o.IdAgrupador ascending
                         select new ListaDtoResponse
                         {
                             Codigo = o.IdFlujoCaja.ToString(),
                             Descripcion = s.Descripcion
                         }).ToList();

            return lista;
        }
        public OportunidadFlujoCajaDtoResponse ObtenerServicioPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var servicio = (from ofc in context.OportunidadFlujoCaja
                            join ssdc in context.SubServicioDatosCaratula on ofc.IdFlujoCaja equals ssdc.IdFlujoCaja
                            join s in context.Servicio on ofc.IdServicio equals s.IdServicio
                            join sg in context.ServicioGrupo on s.IdServicioGrupo equals sg.IdServicioGrupo
                            join m in context.Medio on ssdc.IdMedio equals m.IdMedio
                            join te in context.Maestra.Where(x => x.IdRelacion == 89) on ssdc.IdTipoEnlace.ToString() equals te.Valor
                            join oc in context.OportunidadCosto on ofc.IdOportunidadCosto equals oc.IdOportunidadCosto
                            join c in context.Costo on oc.IdCosto equals c.IdCosto
                            where ofc.IdFlujoCaja.Equals(flujocaja.IdFlujoCaja)
                            select new OportunidadFlujoCajaDtoResponse
                            {
                                IdFlujoCaja = ofc.IdFlujoCaja,
                                IdTipoEnlace = ssdc.IdTipoEnlace,
                                IdServicioGrupo = sg.IdServicioGrupo,
                                IdMedio = ssdc.IdMedio.Value,
                                IdCosto = oc.IdCosto,
                                Descripcion = c.Descripcion
                            }).Single();

            return servicio;
        }
        #endregion
    }
}