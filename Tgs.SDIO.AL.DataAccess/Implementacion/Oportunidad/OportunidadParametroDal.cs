﻿using System.Data.Entity;
using System.Linq;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.Entities.Entities.Comun;
using System;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadParametroDal : Repository<OportunidadParametro>, IOportunidadParametroDal
    {
        readonly DioContext context;
        public OportunidadParametroDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
        public OportunidadParametroPaginadoDtoResponse ListaOportunidadParametroPaginado(OportunidadParametroDtoRequest OportunidadParametro)
        {


            var query = (from obj in context.Set<OportunidadParametro>()
                         join p in context.Set<Parametro>() on obj.IdParametro equals p.IdParametro
                         where  obj.IdOportunidadLineaNegocio == OportunidadParametro.IdOportunidadLineaNegocio 

                                                  
                         select new OportunidadParametroDtoResponse
                         {
                             IdOportunidadParametro = obj.IdOportunidadParametro,
                                Descripcion =p.Descripcion,
                                 FloatValor = obj.Valor.ToString()
                             

                         }).AsNoTracking().Distinct().OrderBy(x => x.Descripcion).ToPagedList(OportunidadParametro.Indice, OportunidadParametro.Tamanio);


            var resultado = new OportunidadParametroPaginadoDtoResponse
            {
                ListOportunidadParametroDtoResponse = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };


            return resultado;



        }

        public OportunidadParametroDtoResponse ObtenerOportunidadParametro(OportunidadParametroDtoRequest oportunidadParametro)
        {


            var response = (from obj in context.Set<OportunidadParametro>()
                         join p in context.Set<Parametro>() on obj.IdParametro equals p.IdParametro
                         where obj.IdOportunidadParametro == oportunidadParametro.IdOportunidadParametro


                         select new OportunidadParametroDtoResponse
                         {
                            
                             Descripcion = p.Descripcion,
                             Valor = obj.Valor


                         }).AsNoTracking().Single();


                 return response;





        }
    }
}