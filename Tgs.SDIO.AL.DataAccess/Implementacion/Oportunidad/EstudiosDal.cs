﻿using System.Data.Entity;
using System.Linq;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class EstudiosDal : Repository<Estudios>, IEstudiosDal
    {
        readonly DioContext context;
        public EstudiosDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        #region - No Transaccionales -

        public EstudiosPaginadoDtoResponse ListarEstudiosPaginado(EstudiosDtoRequest request)
        {
            var lista = (from c in context.Estudios
                         where
                         c.IdEstado == 1 && c.IdSedeInstalacion == request.IdSedeInstalacion
                         select new EstudiosDtoResponse
                         {
                             Id = c.Id,
                             IdSedeInstalacion = c.IdSedeInstalacion,
                             Estudio = c.Estudio,
                             Departamento = c.Departamento,
                             TipoRequerimiento = c.TipoRequerimiento,
                             Nodo = c.Nodo,
                             Dias = c.Dias,
                             TotalSoles = c.TotalSoles,
                             TotalDolares = c.TotalDolares,
                             Responsable = c.Responsable,
                             FacilidadesTecnicas = c.FacilidadesTecnicas,
                             EstadoSisego = c.EstadoSisego
                         }).AsNoTracking()
                         .Distinct()
                         .OrderBy(x => x.Estudio)
                         .ToPagedList(request.Indice, request.Tamanio);

            return new EstudiosPaginadoDtoResponse
            {
                ListaEstudios = lista.ToList(),
                TotalItemCount = lista.TotalItemCount
            };
        }

        #endregion
    }
}
