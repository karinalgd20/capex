﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadFlujoCajaConfiguracionDal : Repository<OportunidadFlujoCajaConfiguracion>, IOportunidadFlujoCajaConfiguracionDal
    {
        readonly DioContext context;

        public OportunidadFlujoCajaConfiguracionDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<OportunidadFlujoCajaConfiguracionDtoResponse> ListarOportunidadFlujoCajaConfiguracionBandeja(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            throw new NotImplementedException();
        }

        public List<OportunidadFlujoCajaConfiguracionDtoResponse> ListarOportunidadFlujoCajaConfiguraciones(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            var query = (from obj in context.Set<OportunidadFlujoCajaConfiguracion>()
                         where obj.IdEstado == Generales.Estados.Activo && obj.IdFlujoCaja == flujocaja.IdFlujoCaja

                         orderby obj.IdFlujoCajaConfiguracion ascending
                         select new OportunidadFlujoCajaConfiguracionDtoResponse
                         {
                             IdFlujoCajaConfiguracion = obj.IdFlujoCajaConfiguracion,
                             CostoPreOperativo = obj.CostoPreOperativo,
                             Ponderacion = obj.Ponderacion,
                             Inicio = obj.Inicio,
                             Meses = obj.Meses
                         }).AsNoTracking().Distinct();

            var ListServicioDtoResponse = query.ToList();

            return ListServicioDtoResponse;
        }



        public OportunidadFlujoCajaConfiguracionDtoResponse ObtenerOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            var query = (from obj in context.Set<OportunidadFlujoCajaConfiguracion>()

                         join ofc in context.Set<OportunidadFlujoCaja>() on obj.IdFlujoCaja equals ofc.IdFlujoCaja

                         where obj.IdEstado == Generales.Estados.Activo && obj.IdFlujoCajaConfiguracion == flujocaja.IdFlujoCajaConfiguracion

                         orderby obj.IdFlujoCajaConfiguracion ascending
                         select new OportunidadFlujoCajaConfiguracionDtoResponse
                         {    IdFlujoCaja=obj.IdFlujoCaja,
                             IdFlujoCajaConfiguracion = obj.IdFlujoCajaConfiguracion,
                             CostoPreOperativo = obj.CostoPreOperativo,
                             Ponderacion = obj.Ponderacion,
                             Inicio = obj.Inicio,
                             Meses = obj.Meses,
                             IdMoneda=ofc.IdMoneda
                               
                         }).AsNoTracking().Distinct();

            var ListServicioDtoResponse = query.Single();

            return ListServicioDtoResponse;
        }

    
    }
}
