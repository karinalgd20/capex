﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadTipoCambioDal : Repository<OportunidadTipoCambio>, IOportunidadTipoCambioDal
    {
        readonly DioContext context;
        public OportunidadTipoCambioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public OportunidadTipoCambioPaginadoDtoResponse ListaOportunidadTipoCambioPaginado(OportunidadTipoCambioDtoRequest OportunidadTipoCambio)
        {
            var query = (from tc in context.Set<OportunidadTipoCambio>()
                         join tcd in context.Set<OportunidadTipoCambioDetalle>() on tc.IdTipoCambioOportunidad equals tcd.IdTipoCambioOportunidad
                         join m in context.Maestra.Where(x => x.IdRelacion == Generales.TablaMaestra.TipoMoneda) on tcd.IdMoneda.ToString() equals m.Valor
                         join mtc in context.Maestra.Where(x => x.IdRelacion == Generales.TablaMaestra.OportunidadTipoCosto) on tcd.IdTipificacion.ToString() equals mtc.Valor
                         
                         where
                          tc.IdOportunidadLineaNegocio == OportunidadTipoCambio.IdOportunidadLineaNegocio &&
                          tc.IdEstado == Generales.Estados.Activo
                         /// && tmm.Valor == OportunidadTipoCambio.IdMoneda.ToString()
                         orderby tcd.FechaCreacion ascending
                         select new OportunidadTipoCambioDtoResponse
                         {
                             IdTipoCambioDetalle = tcd.IdTipoCambioDetalle,
                             Tipificacion=mtc.Descripcion,
                             Moneda = m.Descripcion,
                             Anio = tcd.Anio,
                             Monto = tcd.Monto

                         }).AsNoTracking().Distinct().OrderBy(x => x.IdTipoCambioDetalle).ToPagedList(OportunidadTipoCambio.Indice, OportunidadTipoCambio.Tamanio);




            var resultado = new OportunidadTipoCambioPaginadoDtoResponse
            {
                ListOportunidadTipoCambioDtoResponse = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return resultado;
        }

        public OportunidadTipoCambioDtoResponse ObtenerOportunidadTipoCambioPorLineaNegocio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio)
        {
            var resultado = (from tc in context.Set<OportunidadTipoCambio>()
                         join tcd in context.Set<OportunidadTipoCambioDetalle>() on tc.IdTipoCambioOportunidad equals tcd.IdTipoCambioOportunidad
                         join m in context.Maestra.Where(x => x.IdRelacion == Generales.TablaMaestra.TipoMoneda) on tcd.IdMoneda.ToString() equals m.Valor
                         join mtc in context.Maestra.Where(x => x.IdRelacion == Generales.TablaMaestra.OportunidadTipoCosto) on tcd.IdTipificacion.ToString() equals mtc.Valor

                         where
                          tc.IdOportunidadLineaNegocio == oportunidadTipoCambio.IdOportunidadLineaNegocio &&
                          tc.IdEstado == Generales.Estados.Activo  &&
                          tcd.IdTipificacion== oportunidadTipoCambio.IdTipificacion  &&
                          tcd.Anio== oportunidadTipoCambio.Anio                           

                             orderby tcd.FechaCreacion ascending
                         select new OportunidadTipoCambioDtoResponse
                         {
                             IdTipoCambioDetalle = tcd.IdTipoCambioDetalle,
                             Tipificacion = mtc.Descripcion,
                             Moneda = m.Descripcion,
                             Anio = tcd.Anio,
                             Monto = tcd.Monto 
                         }).AsNoTracking().Distinct().OrderBy(x => x.IdTipoCambioDetalle).Single();



      
            return resultado;
        }
    }
}
