﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class PestanaGrupoTipoDal : Repository<PestanaGrupoTipo>, IPestanaGrupoTipoDal
    {
        readonly DioContext context;
        public PestanaGrupoTipoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarPestanaGrupoTipo(PestanaGrupoTipoDtoRequest pestanaGrupoTipo)
        {
            var result = (from obj in context.Set<PestanaGrupoTipo>()

                         join ma in context.Set<Maestra>().Where(x=> x.IdRelacion== Generales.TablaMaestra.TipoComponente) on obj.IdTipoSubServicio.ToString() equals ma.Valor
                  
                         where
                   
                           obj.IdEstado == Generales.Estados.Activo
                           && obj.IdGrupo==pestanaGrupoTipo.IdGrupo
                       

                          orderby obj.FechaCreacion ascending
                          select new ListaDtoResponse
                          {
                              Codigo = ma.Valor,
                              Descripcion = ma.Descripcion


                          }).AsNoTracking().Distinct().ToList();


            return result;
        }
    }
}
