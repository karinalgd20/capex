﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class ProyectoServicioConceptoDal : Repository<ProyectoServicioConcepto>, IProyectoServicioConceptoDal
    {
        readonly DioContext context;
        public ProyectoServicioConceptoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ProyectoConceptoDtoResponse> ListarCMIProyecto(ProyectoDtoRequest proyecto)
        {
            var pIDPROYECTO = new SqlParameter { ParameterName = "IDPROYECTO", Value = proyecto.IdProyecto, SqlDbType = SqlDbType.Int };
            var pIDESTADO = new SqlParameter { ParameterName = "IDESTADO", Value = proyecto.IdEstado, SqlDbType = SqlDbType.Int };
            var pIDTIPOCONCEPTO = new SqlParameter { ParameterName = "IDTIPOCONCEPTO", Value = proyecto.IdTipoConcepto, SqlDbType = SqlDbType.Int };

            var lista = context.ExecuteQuery<ProyectoConceptoDtoResponse>("STF_ListarProyectoConceptos @IDPROYECTO, @IDESTADO, @IDTIPOCONCEPTO", pIDPROYECTO, pIDESTADO, pIDTIPOCONCEPTO).ToList<ProyectoConceptoDtoResponse>();

            return lista;

        }

        public List<ProyectoConceptoDtoResponse> ListarCMIProyectoCabecera(ProyectoDtoRequest proyecto)
        {
            var pIDPROYECTO = new SqlParameter { ParameterName = "IDPROYECTO", Value = proyecto.IdProyecto, SqlDbType = SqlDbType.Int };

            var lista = context.ExecuteQuery<ProyectoConceptoDtoResponse>("STF_ListarProyectoConceptosCab @IDPROYECTO", pIDPROYECTO).ToList<ProyectoConceptoDtoResponse>();

            return lista;
        }
    }
}
