﻿using System.Data.Entity;
using System.Linq;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.Util.Funciones;
using Tgs.SDIO.DataContracts.Base;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class CotizacionDal : Repository<Cotizacion>, ICotizacionDal
    {
        readonly DioContext context;
        public CotizacionDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;

        }

        public CotizacionPaginadoDtoResponse ListarCotizacionPaginado(CotizacionDtoRequest request)
        {
            var lista = (from c in context.Set<Cotizacion>()
                         where
                         c.IdEstado == 1 && c.IdSedeInstalacion == request.IdSedeInstalacion
                         orderby c.Id descending
                         select new 
                         {
                             IdSedeInstalacion = c.IdSedeInstalacion,
                             CodSisegoCotizacion = c.CodSisegoCotizacion,
                             Nombre = c.Nombre,
                             TotalSoles = c.TotalSoles,
                             TotalDolares = c.TotalDolares,
                             Dias = c.Dias,
                             FechaEnvioPresupuesto = c.FechaEnvioPresupuesto,
                             EstadoCotizacion = c.EstadoCotizacion
                         }).ToList()
                         .Select(x => new CotizacionDtoResponse
                         {
                             IdSedeInstalacion = x.IdSedeInstalacion,
                             CodSisegoCotizacion = x.CodSisegoCotizacion,
                             Nombre = x.Nombre,
                             TotalSoles = x.TotalSoles,
                             TotalDolares = x.TotalDolares,
                             Dias = x.Dias,
                             strFechaEnvioPresupuesto = x.FechaEnvioPresupuesto != null ? Funciones.FormatoFecha(x.FechaEnvioPresupuesto.Value) : "",
                             EstadoCotizacion = x.EstadoCotizacion

                         }).Distinct().OrderBy(x => x.Id).AsQueryable().ToPagedList(request.Indice, request.Tamanio);
            var datosCarga = new CotizacionPaginadoDtoResponse
            {
                ListaCotizacion = lista.ToList(),
                TotalItemCount = lista.TotalItemCount
            };
            return datosCarga;
        }

    }
}
