﻿using System.Data.Entity;
using System.Linq;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class ContactoDal : Repository<Contacto>, IContactoDal
    {
        readonly DioContext context;
        public ContactoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        #region - No Transaccionales -

        public ContactoPaginadoDtoResponse ListarContactoPaginado(ContactoDtoRequest request)
        {
            var lista = (from c in context.Contacto
                         where
                         c.IdSede.Equals(request.IdSede)
                         //&& (request.IdEstado == -1 || c.IdEstado == request.IdEstado.Value)
                         && c.IdEstado == 1
                         select new ContactoDtoResponse
                         {
                             Id = c.Id,
                             NombreApellidos = c.NombreApellidos,
                             NumeroTelefono = c.NumeroTelefono,
                             NumeroCelular = c.NumeroCelular,
                             Cargo = c.Cargo,
                             CorreoElectronico = c.CorreoElectronico
                             
                         }).AsNoTracking()
                         .Distinct()
                         .OrderBy(x => x.NombreApellidos)
                         .ToPagedList(request.Indice, request.Tamanio);

            return new ContactoPaginadoDtoResponse
            {
                ListaContactos = lista.ToList(),
                TotalItemCount = lista.TotalItemCount
            };
        }

        #endregion
    }
}