﻿using System.Data.Entity;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;
using Tgs.SDIO.Entities.Entities.PlantaExterna;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.PlantaExterna
{
    public class TipoCambioSisegoDal : Repository<TipoCambioSisego>, ITipoCambioSisegoDal
    {
        readonly DioContext context;
       
        public TipoCambioSisegoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public TipoCambioSisegoPaginadoDtoResponse ListarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest)
        {
            var consultarTipoCambio = (from tipoCambio in context.Set<TipoCambioSisego>()
                                       where
                                       ((tipoCambioDtoRequest.FechaInicio == null || tipoCambioDtoRequest.FechaFin == null) ||
                                       (tipoCambio.FechaInicio >= tipoCambioDtoRequest.FechaInicio && tipoCambio.FechaFin <= tipoCambioDtoRequest.FechaFin))                                       
                                       orderby tipoCambio.FechaCreacion descending
                                       select new TipoCambioSisegoDtoResponse
                                       { 
                                           Id = tipoCambio.Id,
                                           Estado = tipoCambio.IdEstado == EstadosEntidad.Activo ? EstadoDescripcion.Activo : EstadoDescripcion.Inactivo,
                                           IdEstado = tipoCambio.IdEstado,
                                           Monto=tipoCambio.Monto,
                                           FechaFin=tipoCambio.FechaFin,
                                           FechaInicio=tipoCambio.FechaInicio
                                       }).AsNoTracking().ToPagedList(tipoCambioDtoRequest.Indice, tipoCambioDtoRequest.Tamanio);

            var datosTipoCambio = new TipoCambioSisegoPaginadoDtoResponse
            {
                ListaTipoCambioDtoResponse = consultarTipoCambio.ToList(),
                TotalItemCount = consultarTipoCambio.TotalItemCount
            };

            return datosTipoCambio;
        }
    }
}
