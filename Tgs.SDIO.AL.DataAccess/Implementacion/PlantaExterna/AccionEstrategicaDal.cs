﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.PlantaExterna;
using Tgs.SDIO.Entities.Entities.PlantaExterna;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.PlantaExterna
{
    public class AccionEstrategicaDal : Repository<AccionEstrategica>, IAccionEstrategicaDal
    {
        readonly DioContext context;
        public AccionEstrategicaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }


    }
}
