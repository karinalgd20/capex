﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;
using System.Data;
using Tgs.SDIO.Entities.Entities.PlantaExterna;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Util.Paginacion;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.PlantaExterna
{
    public class NoPepsDal : Repository<NoPeps>, INoPepsDal
    {
        readonly DioContext context; 
        public NoPepsDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
   
        public NoPepsPaginadoDtoResponse ListarNoPeps(NoPepsDtoRequest noPepsRequest)
        {
            var consultarNoPeps = (from peps in context.Set<NoPeps>()
                         where (peps.CodigoPep.Contains(noPepsRequest.CodigoPep) || string.IsNullOrEmpty(noPepsRequest.CodigoPep))
                         orderby peps.FechaCreacion descending
                         select new NoPepsDtoResponse
                         {
                             CodigoPep = peps.CodigoPep,
                             Id = peps.Id,
                             Estado = peps.IdEstado == EstadosEntidad.Activo ? EstadoDescripcion.Activo : EstadoDescripcion.Inactivo,
                             IdEstado = peps.IdEstado
                         }).AsNoTracking().ToPagedList(noPepsRequest.Indice, noPepsRequest.Tamanio);

            var datosNoPeps = new NoPepsPaginadoDtoResponse
            {
                ListaNoPeps = consultarNoPeps.ToList(),
                TotalItemCount = consultarNoPeps.TotalItemCount
            };

            return datosNoPeps;
        }

        
 
    }
}
