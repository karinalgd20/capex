﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
   public class DatosPreventaMovilDal : Repository<DatosPreventaMovil>, IDatosPreventaMovilDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public DatosPreventaMovilDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }


        public ProcesoResponse CargaExcelDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            try
            {
                var pRutaArchivo = new SqlParameter { ParameterName = "RutaArchivo", Value = request.RutaArchivo, SqlDbType = SqlDbType.VarChar, Size=300};
                var pIdUsuarioCreacion = new SqlParameter { ParameterName = "IdUsuarioCreacion", Value = request.IdUsuario, SqlDbType = SqlDbType.Int };
                var pFechaCreacion = new SqlParameter { ParameterName = "FechaCreacion", Value = DateTime.Now.Date, SqlDbType = SqlDbType.Date };

                respuesta = context.ExecuteQuery<ProcesoResponse>("[TRAZABILIDAD].[USP_CARGAR_EXCEL]  @RutaArchivo,@IdUsuarioCreacion,@FechaCreacion", pRutaArchivo, pIdUsuarioCreacion, pFechaCreacion).FirstOrDefault();     
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146232060)
                {
                    respuesta.Mensaje = "El tamaño del texto en la celda es muy largo.";
                    respuesta.TipoRespuesta = Proceso.Invalido;
                }
                else {               
                    respuesta.Mensaje = ex.Message;
                    respuesta.TipoRespuesta = Proceso.Invalido;

                }

            }
  
            return respuesta;
        }
    }
}
