﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
   public class EstadoCasoDal : Repository<EstadoCaso>, IEstadoCasoDal
    {
        readonly DioContext context;
        public EstadoCasoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarComboEstadoCaso()
        {

            var query = (from bit in context.EstadoCaso.Where(x => x.IdEstado == Activo) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.IdEstadoCaso.ToString() });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
