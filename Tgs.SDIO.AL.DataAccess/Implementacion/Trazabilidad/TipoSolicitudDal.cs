﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
   public class TipoSolicitudDal : Repository<TipoSolicitud>, ITipoSolicitudDal
    {
        readonly DioContext context;
        public TipoSolicitudDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarComboTipoSolicitud()
        {

            var query = (from bit in context.TipoSolicitud.Where(x => x.IdEstado == Activo) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.IdTipoSolicitud.ToString() });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
