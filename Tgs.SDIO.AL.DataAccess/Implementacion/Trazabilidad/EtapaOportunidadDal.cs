﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
    public class EtapaOportunidadDal : Repository<EtapaOportunidad>, IEtapaOportunidadDal
    {
        readonly DioContext context;
        public EtapaOportunidadDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarComboEtapaOportunidad()
        {

            var query = (from bit in context.EtapaOportunidad.Where(x => x.IdEstado == EstadosEntidad.Activo) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.IdEtapa.ToString() });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }

        public List<ListaDtoResponse> ListarComboEtapaOportunidadPorIdFase(FaseDtoRequest request)
        {

            var query = (from bit in context.EtapaOportunidad.Where(x => x.IdEstado == EstadosEntidad.Activo && x.IdFase == request.IdFase) select new ListaDtoResponse { Codigo = bit.IdEtapa.ToString(), Descripcion = bit.Descripcion });
            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
