﻿using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.PlantaExterna;
using Tgs.SDIO.AL.DataAccess.Core;
using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using static Tgs.SDIO.Util.Constantes.Generales.EquipoTrabajo;
using Tgs.SDIO.Util.Funciones;
using static Tgs.SDIO.Util.Constantes.Generales.TablaMaestra;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
    public class OportunidadSTDal : Repository<OportunidadST>, IOportunidadSTDal
    {
        readonly DioContext context;
        Negocio_Vacaciones negocio = new Negocio_Vacaciones();
        public OportunidadSTDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalObservacionPaginado(OportunidadSTDtoRequest request)
        {
            var query = (from oportunidad in context.Set<OportunidadST>()
                         join caso in context.Set<CasoOportunidad>() on oportunidad.IdOportunidad equals caso.IdOportunidad
                         join solicitud in context.Set<TipoSolicitud>() on caso.IdTipoSolicitud equals solicitud.IdTipoSolicitud
                         where ((oportunidad.IdOportunidadSF.Contains(request.IdOportunidadSF) || string.IsNullOrEmpty(request.IdOportunidadSF))
                         && (caso.IdCasoSF.Contains(request.IdCasoSF) || string.IsNullOrEmpty(request.IdCasoSF))
                         && oportunidad.IdEstado == Activo
                         && caso.IdEstado == Activo
                         && solicitud.IdEstado == Activo)
                         orderby oportunidad.FechaCreacion descending
                         select new OportunidadSTDtoResponse
                         {
                             IdOportunidad = oportunidad.IdOportunidad,
                             IdOportunidadSF = oportunidad.IdOportunidadSF,
                             DescripcionOportunidad = oportunidad.Descripcion,
                             IdCaso = caso.IdCaso,
                             IdCasoSF = caso.IdCasoSF,
                             DescripcionCaso = caso.Descripcion,
                             DescripcionSolicitud = solicitud.Descripcion
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new OportunidadSTPaginadoDtoResponse
            {
                ListOportunidadSTDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalRecursoPaginado(OportunidadSTDtoRequest request)
        {
            var query = (from oportunidad in context.Set<OportunidadST>()
                         where ((oportunidad.Descripcion.Contains(request.DescripcionOportunidad) || string.IsNullOrEmpty(request.DescripcionOportunidad))
                         && (oportunidad.IdOportunidadSF.Contains(request.IdOportunidadSF) || string.IsNullOrEmpty(request.IdOportunidadSF))
                         && oportunidad.IdEstado == Activo)
                         orderby oportunidad.FechaCreacion descending
                         select new OportunidadSTDtoResponse
                         {
                             IdOportunidad = oportunidad.IdOportunidad,
                             IdOportunidadSF = oportunidad.IdOportunidadSF,
                             DescripcionOportunidad = oportunidad.Descripcion
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new OportunidadSTPaginadoDtoResponse
            {
                ListOportunidadSTDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTMasivoPaginado(OportunidadSTDtoRequest request)
        {
            var query = (from oportunidad in context.Set<OportunidadST>()
                         join caso in context.Set<CasoOportunidad>() on oportunidad.IdOportunidad equals caso.IdOportunidad
                         join etapa in context.Set<Etapa>() on oportunidad.IdEtapa equals etapa.IdEtapa
                         join cliente in context.Set<Cliente>() on oportunidad.IdCliente equals cliente.IdCliente
                         join estadoCaso in context.Set<EstadoCaso>() on caso.IdEstadoCaso equals estadoCaso.IdEstadoCaso
                         where ((oportunidad.Descripcion.Contains(request.DescripcionOportunidad) || string.IsNullOrEmpty(request.DescripcionOportunidad))
                         && oportunidad.IdEstado == Activo
                         && caso.IdEstado == Activo
                         && etapa.IdEstado == Activo
                         && cliente.IdEstado == Activo
                         && estadoCaso.IdEstado == Activo)
                         orderby oportunidad.FechaCreacion descending
                         select new OportunidadSTDtoResponse
                         {
                             IdOportunidad = oportunidad.IdOportunidad,
                             IdOportunidadSF = oportunidad.IdOportunidadSF,
                             DescripcionOportunidad = oportunidad.Descripcion,
                             IdCasoSF = caso.IdCasoSF,
                             DescripcionCliente = cliente.Descripcion,
                             PorcentajeCierre = oportunidad.PorcentajeCierre,
                             PorcentajeCheckList = oportunidad.PorcentajeCheckList,
                             DescripcionEstadoCaso = estadoCaso.Descripcion,
                             DescripcionEtapa = etapa.Descripcion,
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new OportunidadSTPaginadoDtoResponse
            {
                ListOportunidadSTDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }
        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalObservacionPorCodigoPaginado(OportunidadSTDtoRequest request)
        {
            var query = (from oportunidad in context.Set<OportunidadST>()
                         join caso in context.Set<CasoOportunidad>() on oportunidad.IdOportunidad equals caso.IdOportunidad
                         join solicitud in context.Set<TipoSolicitud>() on caso.IdTipoSolicitud equals solicitud.IdTipoSolicitud
                         where ((oportunidad.IdOportunidadSF.Contains(request.IdOportunidadSF) || string.IsNullOrEmpty(request.IdOportunidadSF))
                         && (caso.IdCasoSF.Contains(request.IdCasoSF) || string.IsNullOrEmpty(request.IdCasoSF))
                         && oportunidad.IdEstado == Activo
                         && caso.IdEstado == Activo
                         && solicitud.IdEstado == Activo)
                         orderby oportunidad.FechaCreacion descending
                         select new OportunidadSTDtoResponse
                         {
                             IdOportunidad = oportunidad.IdOportunidad,
                             IdOportunidadSF = oportunidad.IdOportunidadSF,
                             DescripcionOportunidad = oportunidad.Descripcion,
                             IdCaso = caso.IdCaso,
                             IdCasoSF = caso.IdCasoSF,
                             DescripcionCaso = caso.Descripcion,
                             DescripcionSolicitud = solicitud.Descripcion
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new OportunidadSTPaginadoDtoResponse
            {
                ListOportunidadSTDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public OportunidadSTSeguimientoPreventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPreventaPaginado(OportunidadSTSeguimientoPreventaDtoRequest request)
        {
                var vIdOportunidadSF = (request.IdOportunidadSF == null) ? "0" : request.IdOportunidadSF;
                var vIdCasoSF = (request.IdCasoSF == null) ? "0" : request.IdCasoSF;
                var vDescripcionCliente = (request.DescripcionCliente == null) ? "0" : request.DescripcionCliente;

                var IdOportunidadSF = new SqlParameter { ParameterName = "IdOportunidadSF", Value = vIdOportunidadSF, SqlDbType = SqlDbType.VarChar };
                var IdCasoSF = new SqlParameter { ParameterName = "IdCasoSF", Value = vIdCasoSF, SqlDbType = SqlDbType.VarChar };
                var DescripcionCliente = new SqlParameter { ParameterName = "DescripcionCliente", Value = vDescripcionCliente, SqlDbType = SqlDbType.VarChar };
                var IdSegmentoNegocio = new SqlParameter { ParameterName = "IdSegmentoNegocio", Value = request.IdSegmentoNegocio, SqlDbType = SqlDbType.Int };
                var IdEstadoCaso = new SqlParameter { ParameterName = "IdEstadoCaso", Value = request.IdEstadoCaso, SqlDbType = SqlDbType.Int };
                var IdTipoOportunidad = new SqlParameter { ParameterName = "IdTipoOportunidad", Value = request.IdTipoOportunidad, SqlDbType = SqlDbType.Int };
                var Codigo = new SqlParameter { ParameterName = "Codigo", Value = request.Codigo, SqlDbType = SqlDbType.Int };
                var IdLineaNegocio = new SqlParameter { ParameterName = "IdLineaNegocio", Value = request.IdLineaNegocio, SqlDbType = SqlDbType.Int };
                var IdEtapa = new SqlParameter { ParameterName = "IdEtapa", Value = request.IdEtapa, SqlDbType = SqlDbType.Int };
                var Indice = new SqlParameter { ParameterName = "Indice", Value = request.Indice, SqlDbType = SqlDbType.Int };
                var Tamanio = new SqlParameter { ParameterName = "Tamanio", Value = request.Tamanio, SqlDbType = SqlDbType.Int };
                var UserId = new SqlParameter { ParameterName = "UserId", Value = request.UserId, SqlDbType = SqlDbType.Int };
                var Lider = new SqlParameter { ParameterName = "Lider", Value = request.Lider, SqlDbType = SqlDbType.Bit };

                //var IdOportunidadSF2 = new SqlParameter { ParameterName = "IdOportunidadSF2", Value = vIdOportunidadSF, SqlDbType = SqlDbType.VarChar };
                //var IdCasoSF2 = new SqlParameter { ParameterName = "IdCasoSF2", Value = vIdCasoSF, SqlDbType = SqlDbType.VarChar };
                //var DescripcionCliente2 = new SqlParameter { ParameterName = "DescripcionCliente2", Value = vDescripcionCliente, SqlDbType = SqlDbType.VarChar };
                //var IdSegmentoNegocio2 = new SqlParameter { ParameterName = "IdSegmentoNegocio2", Value = request.IdSegmentoNegocio, SqlDbType = SqlDbType.Int };
                //var IdEstadoCaso2 = new SqlParameter { ParameterName = "IdEstadoCaso2", Value = request.IdEstadoCaso, SqlDbType = SqlDbType.Int };
                //var IdTipoOportunidad2 = new SqlParameter { ParameterName = "IdTipoOportunidad2", Value = request.IdTipoOportunidad, SqlDbType = SqlDbType.Int };
                //var Codigo2 = new SqlParameter { ParameterName = "Codigo2", Value = request.Codigo, SqlDbType = SqlDbType.Int };
                //var IdLineaNegocio2 = new SqlParameter { ParameterName = "IdLineaNegocio2", Value = request.IdLineaNegocio, SqlDbType = SqlDbType.Int };
                //var IdEtapa2 = new SqlParameter { ParameterName = "IdEtapa2", Value = request.IdEtapa, SqlDbType = SqlDbType.Int };
                //var UserId2 = new SqlParameter { ParameterName = "UserId2", Value = request.UserId, SqlDbType = SqlDbType.Int };
                //var Lider2 = new SqlParameter { ParameterName = "Lider2", Value = request.Lider, SqlDbType = SqlDbType.Bit };

                 var lista = context.ExecuteQuery<OportunidadSTSeguimientoPreventaDtoResponse>("[TRAZABILIDAD].[USP_OPORTUNIDADST_SEGUIMIENTO_PREVENTA_PAGINADO_LISTA]  @IdOportunidadSF,@IdCasoSF,@DescripcionCliente,@IdSegmentoNegocio,@IdEstadoCaso,@IdTipoOportunidad,@Codigo,@IdLineaNegocio,@Indice,@Tamanio,@UserId,@Lider", IdOportunidadSF, IdCasoSF, DescripcionCliente, IdSegmentoNegocio, IdEstadoCaso, IdTipoOportunidad, Codigo, IdLineaNegocio, Indice, Tamanio, UserId, Lider).ToList<OportunidadSTSeguimientoPreventaDtoResponse>();
                 //var TotalItemCount = context.ExecuteQuery<int>("[TRAZABILIDAD].[USP_OPORTUNIDADST_SEGUIMIENTO_PREVENTA_PAGINADO_TOTAL]  @IdOportunidadSF2,@IdCasoSF2,@DescripcionCliente2,@IdSegmentoNegocio2,@IdEstadoCaso2,@IdTipoOportunidad2,@Codigo2,@IdLineaNegocio2,@UserId2,@Lider2", IdOportunidadSF2, IdCasoSF2, DescripcionCliente2, IdSegmentoNegocio2, IdEstadoCaso2, IdTipoOportunidad2, Codigo2, IdLineaNegocio2, UserId2, Lider2).FirstOrDefault();

            var datosCarga = new OportunidadSTSeguimientoPreventaPaginadoDtoResponse
            {
                ListOportunidadSTSeguimientoPreventaDto = lista,
                TotalItemCount = 0
            };
            return datosCarga;
        }
        public OportunidadSTSeguimientoPostventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPostventaPaginado(OportunidadSTSeguimientoPostventaDtoRequest request)
        {

            var vIdOportunidadSF = (request.IdOportunidadSF == null) ? "0" : request.IdOportunidadSF;
            var vDescripcionCliente = (request.DescripcionCliente == null) ? "0" : request.DescripcionCliente;
            var vCodSisego = (request.CodSisego == null) ? "0" : request.CodSisego;

            var IdOportunidadSF = new SqlParameter { ParameterName = "IdOportunidadSF", Value = vIdOportunidadSF, SqlDbType = SqlDbType.VarChar };
            var DescripcionCliente = new SqlParameter { ParameterName = "DescripcionCliente", Value = vDescripcionCliente, SqlDbType = SqlDbType.VarChar };
            var IdEtapa = new SqlParameter { ParameterName = "IdEtapa", Value = request.IdEtapa, SqlDbType = SqlDbType.Int };
            var IdEstadoCaso = new SqlParameter { ParameterName = "IdEstadoCaso", Value = request.IdEstadoCaso, SqlDbType = SqlDbType.Int };
            var IdSegmentoNegocio = new SqlParameter { ParameterName = "IdSegmentoNegocio", Value = request.IdSegmentoNegocio, SqlDbType = SqlDbType.Int };
            var IdTipoOportunidad = new SqlParameter { ParameterName = "IdTipoOportunidad", Value = request.IdTipoOportunidad, SqlDbType = SqlDbType.Int };
            var IdComplejidad = new SqlParameter { ParameterName = "IdComplejidad", Value = request.IdComplejidad, SqlDbType = SqlDbType.Int };
            var CodSisego = new SqlParameter { ParameterName = "CodSisego", Value = vCodSisego, SqlDbType = SqlDbType.VarChar };
            var Indice = new SqlParameter { ParameterName = "Indice", Value = request.Indice, SqlDbType = SqlDbType.Int };
            var Tamanio = new SqlParameter { ParameterName = "Tamanio", Value = request.Tamanio, SqlDbType = SqlDbType.Int };

            var IdOportunidadSFp = new SqlParameter { ParameterName = "IdOportunidadSFp", Value = vIdOportunidadSF, SqlDbType = SqlDbType.VarChar };
            var DescripcionClientep = new SqlParameter { ParameterName = "DescripcionClientep", Value = vDescripcionCliente, SqlDbType = SqlDbType.VarChar };
            var IdEtapap = new SqlParameter { ParameterName = "IdEtapap", Value = request.IdEtapa, SqlDbType = SqlDbType.Int };
            var IdEstadoCasop = new SqlParameter { ParameterName = "IdEstadoCasop", Value = request.IdEstadoCaso, SqlDbType = SqlDbType.Int };
            var IdSegmentoNegociop = new SqlParameter { ParameterName = "IdSegmentoNegociop", Value = request.IdSegmentoNegocio, SqlDbType = SqlDbType.Int };
            var IdTipoOportunidadp = new SqlParameter { ParameterName = "IdTipoOportunidadp", Value = request.IdTipoOportunidad, SqlDbType = SqlDbType.Int };
            var IdComplejidadp = new SqlParameter { ParameterName = "IdComplejidadp", Value = request.IdComplejidad, SqlDbType = SqlDbType.Int };
            var CodSisegop = new SqlParameter { ParameterName = "CodSisegop", Value = vCodSisego, SqlDbType = SqlDbType.VarChar };
            var Indicep = new SqlParameter { ParameterName = "Indicep", Value = request.Indice, SqlDbType = SqlDbType.Int };
            var Tamaniop = new SqlParameter { ParameterName = "Tamaniop", Value = request.Tamanio, SqlDbType = SqlDbType.Int };

            
            var TotalItemCount = context.ExecuteQuery<int>("[TRAZABILIDAD].[USP_OPORTUNIDADST_SEGUIMIENTO_POSTVENTA_PAGINADO_TOTAL]  @IdOportunidadSF,@DescripcionCliente,@IdEtapa,@IdEstadoCaso,@IdSegmentoNegocio,@IdTipoOportunidad,@IdComplejidad,@CodSisego,@Indice,@Tamanio", IdOportunidadSF, DescripcionCliente, IdEtapa, IdEstadoCaso, IdSegmentoNegocio, IdTipoOportunidad, IdComplejidad, CodSisego, Indice, Tamanio).FirstOrDefault();
            var lista = context.ExecuteQuery<OportunidadSTSeguimientoPostventaDtoResponse>("[TRAZABILIDAD].[USP_OPORTUNIDADST_SEGUIMIENTO_POSTVENTA_PAGINADO_LISTA]  @IdOportunidadSFp,@DescripcionClientep,@IdEtapap,@IdEstadoCasop,@IdSegmentoNegociop,@IdTipoOportunidadp,@IdComplejidadp,@CodSisegop,@Indicep,@Tamaniop", IdOportunidadSFp, DescripcionClientep, IdEtapap, IdEstadoCasop, IdSegmentoNegociop, IdTipoOportunidadp, IdComplejidadp, CodSisegop, Indicep, Tamaniop).ToList<OportunidadSTSeguimientoPostventaDtoResponse>();

            var datosCarga = new OportunidadSTSeguimientoPostventaPaginadoDtoResponse
            {
                ListOportunidadSTSeguimientoPostventaDto = lista,
                TotalItemCount = TotalItemCount
            };


            return datosCarga;            
        }
    }
}
