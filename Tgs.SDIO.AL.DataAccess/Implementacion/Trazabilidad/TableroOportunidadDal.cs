﻿using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using System.Linq;
using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

//EAAR: PF12
namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
    public class TableroOportunidadDal : Repository<OportunidadTs>, ITableroOportunidadDal
    {
        readonly DioContext context;

        public TableroOportunidadDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public TableroOportunidadMatrizDtoResponse ListarTableroOportunidadMatriz(TableroOportunidadListarDtoRequest request)
        {

            var pIdOportunidad = new SqlParameter { ParameterName = "IdOportunidad", Value = request.IdOportunidad, SqlDbType = SqlDbType.Int };
            var pIdCliente = new SqlParameter { ParameterName = "IdCliente", Value = request.IdCliente, SqlDbType = SqlDbType.Int };
            var pEstadoOportunidad = new SqlParameter { ParameterName = "EstadoOportunidad", Value = request.EstadoOportunidad, SqlDbType = SqlDbType.Int };
            var pFechaInicio = new SqlParameter { ParameterName = "FechaInicio", Value = request.FechaInicio.Trim(), SqlDbType = SqlDbType.VarChar };
            var pFechaFin = new SqlParameter { ParameterName = "FechaFin", Value = request.FechaFin.Trim(), SqlDbType = SqlDbType.VarChar };

            var res = context.ExecuteQuery<TableroOportunidadListarDtoResponse>("[Trazabilidad].[USP_Listar_Oportunidad_Matriz] @IdOportunidad, @IdCliente, @EstadoOportunidad, @FechaInicio, @FechaFin", pIdOportunidad, pIdCliente, pEstadoOportunidad, pFechaInicio, pFechaFin).ToList<TableroOportunidadListarDtoResponse>();

            List<TableroOportunidadDtoResponse> oportunidades = res.GroupBy(x => new {x.IdOportunidad, x.OportunidadDesc, x.IdEstadoOportunidad, x.EstadoOportunidadDesc, x.IdEtapa, x.EtapaDesc, x.IdCliente, x.ClienteDesc })
                .Select(x => new TableroOportunidadDtoResponse
                {
                    IdOportunidad = x.Key.IdOportunidad,
                    OportunidadDesc = x.Key.OportunidadDesc,
                    IdEstadoOportunidad = x.Key.IdEstadoOportunidad,
                    EstadoOportunidadDesc = x.Key.EstadoOportunidadDesc,
                    IdEtapa = x.Key.IdEtapa,
                    EtapaDesc = x.Key.EtapaDesc,
                    IdCliente = x.Key.IdCliente,
                    ClienteDesc = x.Key.ClienteDesc,
                    RecursoNombre = res.Where(a => a.IdOportunidad == x.Key.IdOportunidad && a.RecursoId != -1).FirstOrDefault()==null?String.Empty: res.Where(a => a.IdOportunidad == x.Key.IdOportunidad && a.RecursoId != -1).FirstOrDefault().RecursoNombre.Trim(),
                    Actividades = res.Where(y => y.IdOportunidad == x.Key.IdOportunidad).Select(z=>new TableroOportunidadActividadDtoResponse{
                        IdSeguimiento =z.IdSeguimiento,
                        IdActividad =z.IdActividad,
                        IdEstadoEjecucion = z.IdEstadoEjecucion,
                        FechaInicioReal =z.FechaInicioReal,
                        FechaFinReal =z.FechaFinReal,
                        Tiene=z.Tiene
                    }).ToList()
                }).ToList();
            
            List<TableroActividadDtoResponse> actividades = res.GroupBy(x => new { x.IdActividad, x.ActividadDesc })
                .Select(x => new TableroActividadDtoResponse
                {
                    IdActividad = x.Key.IdActividad,
                    ActividadDesc = x.Key.ActividadDesc
                }).ToList();
             
            return new TableroOportunidadMatrizDtoResponse() { Oportunidades = oportunidades, Actividades = actividades };
        }
    }
}