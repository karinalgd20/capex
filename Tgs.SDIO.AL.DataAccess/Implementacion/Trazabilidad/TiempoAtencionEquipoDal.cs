﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.Entities.Entities.PlantaExterna;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using Tgs.SDIO.Util.Funciones;
namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
   public  class TiempoAtencionEquipoDal : Repository<TiempoAtencionEquipo>, ITiempoAtencionEquipoDal
    {
        readonly DioContext context;
        public TiempoAtencionEquipoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public TiempoAtencionEquipoPaginadoDtoResponse ListadoTiempoAtencionEquipoPaginado(TiempoAtencionEquipoDtoRequest request)
        {
            var query = (from tiempoatencion in context.Set<TiempoAtencionEquipo>()
                         join oportunidad in context.Set<OportunidadST>() on tiempoatencion.IdOportunidad equals oportunidad.IdOportunidad
                         join caso in context.Set<CasoOportunidad>() on oportunidad.IdOportunidad equals caso.IdOportunidad
                         join equipoTrabajo in context.Set<EquipoTrabajo>() on tiempoatencion.IdEquipoTrabajo equals equipoTrabajo.IdEquipoTrabajo

                         where ((oportunidad.IdOportunidadSF.Contains(request.IdOportunidadSF) || string.IsNullOrEmpty(request.IdOportunidadSF))
                         && (caso.IdCasoSF.Contains(request.IdCasoSF) || string.IsNullOrEmpty(request.IdCasoSF))
                         && (tiempoatencion.IdSisegoSW.Contains(request.IdSisegoSW) || string.IsNullOrEmpty(request.IdSisegoSW))
                         && (tiempoatencion.IdEquipoTrabajo == request.IdEquipoTrabajo || request.IdEquipoTrabajo == Seleccione)
                         && oportunidad.IdEstado == Activo
                         && caso.IdEstado == Activo
                         && tiempoatencion.IdEstado == Activo
                         && caso.IdCaso == tiempoatencion.IdCaso
                         )
                         orderby tiempoatencion.FechaCreacion descending
                         select new 
                         {
                             IdTiempoAtencion = tiempoatencion.IdTiempoAtencion,
                             IdOportunidadSF = oportunidad.IdOportunidadSF,
                             IdCasoSF = caso.IdCasoSF,
                             IdSisegoSW = tiempoatencion.IdSisegoSW,
                             DescripcionEquipoTrabajo = equipoTrabajo.Descripcion,
                             FechaInicio = tiempoatencion.FechaInicio,
                             FechaFin = tiempoatencion.FechaFin,
                             DiasGestion = tiempoatencion.DiasGestion,
                             Observaciones = tiempoatencion.Observaciones,
                             Estado = tiempoatencion.IdEstado == Activo ? "Activo" : "Inactivo",
                             UsuarioEdicion = (from recursoEdita in context.Set<Recurso>() where recursoEdita.IdUsuarioRais == tiempoatencion.IdUsuarioEdicion && recursoEdita.IdEstado == Activo select recursoEdita.Nombre).FirstOrDefault(),
                             FechaEdicion = tiempoatencion.FechaEdicion,
                             UsuarioCreacion = (from recursoCreacion in context.Set<Recurso>() where recursoCreacion.IdUsuarioRais == tiempoatencion.IdUsuarioCreacion && recursoCreacion.IdEstado == Activo select recursoCreacion.Nombre).FirstOrDefault(),
                             FechaCreacion = tiempoatencion.FechaCreacion
                         }).ToList()
                         .Select(x => new TiempoAtencionEquipoDtoResponse
                         {
                              IdTiempoAtencion = x.IdTiempoAtencion,
                              IdOportunidadSF = x.IdOportunidadSF,
                              IdCasoSF = x.IdCasoSF,
                              IdSisegoSW = x.IdSisegoSW,
                              DescripcionEquipoTrabajo = x.DescripcionEquipoTrabajo,
                              strFechaInicio = x.FechaInicio != null ? Funciones.FormatoFecha(x.FechaInicio.Value) : "",
                              strFechaFin = x.FechaFin != null ? Funciones.FormatoFecha(x.FechaFin.Value) : "",
                              DiasGestion = x.DiasGestion,
                              Observaciones = x.Observaciones,
                              Estado = x.Estado,
                              UsuarioEdicion = x.UsuarioEdicion,
                              strFechaEdicion = x.FechaEdicion != null ? Funciones.FormatoFecha(x.FechaEdicion.Value) : "",
                              UsuarioCreacion = x.UsuarioCreacion,
                              strFechaCreacion = x.FechaCreacion != null ? Funciones.FormatoFecha(x.FechaCreacion) : ""
                         }).Distinct().OrderBy(x => x.IdTiempoAtencion).AsQueryable().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new TiempoAtencionEquipoPaginadoDtoResponse
            {
                ListTiempoAtencionEquipoDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }
    }
}
