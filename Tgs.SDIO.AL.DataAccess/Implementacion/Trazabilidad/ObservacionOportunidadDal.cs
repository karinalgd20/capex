﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using Tgs.SDIO.Util.Funciones;


namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
   public class ObservacionOportunidadDal : Repository<ObservacionOportunidad>, IObservacionOportunidadDal
    {
        readonly DioContext context;
        public ObservacionOportunidadDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public ObservacionOportunidadPaginadoDtoResponse ListadoObservacionesOportunidadPaginado(ObservacionOportunidadDtoRequest request)
        {
                var query = (from observacion in context.Set<ObservacionOportunidad>()
                             join oportunidad in context.Set<OportunidadST>() on observacion.IdOportunidad equals oportunidad.IdOportunidad
                             join concepto in context.Set<ConceptoSeguimiento>() on observacion.IdConcepto equals concepto.IdConcepto
                             join caso in context.Set<CasoOportunidad>() on observacion.IdOportunidad equals caso.IdOportunidad
                             where (((observacion.IdConcepto == request.IdConcepto) || (request.IdConcepto == Seleccione))
                             && (oportunidad.IdOportunidadSF.Contains(request.IdOportunidadSF) || string.IsNullOrEmpty(request.IdOportunidadSF))
                             && observacion.IdEstado == Activo
                             && oportunidad.IdEstado == Activo
                             && concepto.IdEstado == Activo
                             && caso.IdEstado == Activo
                             && caso.IdCaso == observacion.IdCaso
                             && (observacion.IdEstado == Activo || observacion.IdEstado == Inactivo))
                             orderby observacion.FechaCreacion descending
                             select new 
                             {
                                 IdObservacion = observacion.IdObservacion,
                                 IdOportunidadSF = oportunidad.IdOportunidadSF,
                                 IdCasoSF = caso.IdCasoSF,
                                 FechaSeguimiento = observacion.FechaSeguimiento,
                                 IdSeguimiento = observacion.IdSeguimiento,
                                 DescripcionConcepto = concepto.Descripcion,
                                 Observaciones = observacion.Observaciones,
                                 Estado = observacion.IdEstado ==Activo ? "Activo" : "Inactivo",
                                 UsuarioEdicion = (from recursoEdita in context.Set<Recurso>() where recursoEdita.IdUsuarioRais == observacion.IdUsuarioEdicion && recursoEdita.IdEstado == Activo select recursoEdita.Nombre).FirstOrDefault(),
                                 FechaEdicion = observacion.FechaEdicion,
                                 UsuarioCreacion = (from recursoCreacion in context.Set<Recurso>() where recursoCreacion.IdUsuarioRais == observacion.IdUsuarioCreacion && recursoCreacion.IdEstado == Activo select recursoCreacion.Nombre).FirstOrDefault(),
                                 FechaCreacion = observacion.FechaCreacion

                             }).ToList()
                            .Select(x => new ObservacionOportunidadDtoResponse
                            {
                                IdObservacion = x.IdObservacion,
                                IdOportunidadSF = x.IdOportunidadSF,
                                IdCasoSF = x.IdCasoSF,
                                strFechaSeguimiento = x.FechaSeguimiento != null ? Funciones.FormatoFecha(x.FechaSeguimiento.Value) : "",
                                IdSeguimiento = x.IdSeguimiento,
                                DescripcionConcepto = x.DescripcionConcepto,
                                Observaciones = x.Observaciones,
                                Estado = x.Estado,
                                UsuarioEdicion = x.UsuarioEdicion,
                                strFechaEdicion = x.FechaEdicion != null ? Funciones.FormatoFecha(x.FechaEdicion.Value) : "",
                                UsuarioCreacion = x.UsuarioCreacion,
                                strFechaCreacion = x.FechaCreacion != null ? Funciones.FormatoFecha(x.FechaCreacion) : ""
                            }).Distinct().OrderBy(x => x.IdObservacion).AsQueryable().ToPagedList(request.Indice, request.Tamanio);
                             
                             
                   

                var datosCarga = new ObservacionOportunidadPaginadoDtoResponse
                {
                    ListObservacionOportunidadDto = query.ToList(),
                    TotalItemCount = query.TotalItemCount
                };

                return datosCarga;
        }

    }
}
