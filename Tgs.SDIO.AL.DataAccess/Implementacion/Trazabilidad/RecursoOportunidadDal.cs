﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Comun;

using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.Util.Funciones;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using System.Collections.Generic;


namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
    public class RecursoOportunidadDal : Repository<RecursoOportunidad>, IRecursoOportunidadDal
    {
        readonly DioContext context;
        public RecursoOportunidadDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public RecursoOportunidadPaginadoDtoResponse ListadoRecursosOportunidadPaginado(RecursoOportunidadDtoRequest request)
        {

            var query = (from recursoOportunidad in context.Set<RecursoOportunidad>()
                         join recurso in context.Set<Recurso>() on recursoOportunidad.IdRecurso equals recurso.IdRecurso
                         join rolRecurso in context.Set<RolRecurso>() on recursoOportunidad.IdRol equals rolRecurso.IdRol
                         join oportunidad in context.Set<OportunidadST>() on recursoOportunidad.IdOportunidad equals oportunidad.IdOportunidad
                         where ((oportunidad.IdOportunidadSF.Contains(request.IdOportunidadSF) || string.IsNullOrEmpty(request.IdOportunidadSF))
                         && (recurso.Nombre.Contains(request.DescripcionRecurso) || string.IsNullOrEmpty(request.DescripcionRecurso))
                         && (recursoOportunidad.IdEstado == Activo || recursoOportunidad.IdEstado == Inactivo)
                         && recurso.IdEstado == Activo
                         && rolRecurso.IdEstado == Activo
                         && oportunidad.IdEstado == Activo)
                         orderby recurso.FechaCreacion descending
                         select new
                         {
                             IdAsignacion = recursoOportunidad.IdAsignacion,
                             DescripcionRecurso = recurso.Nombre,
                             DescripcionRolRecurso = rolRecurso.Descripcion,
                             FInicioAsignacion = recursoOportunidad.FInicioAsignacion,
                             FFinAsignacion = recursoOportunidad.FFinAsignacion,
                             PorcentajeDedicacion = recursoOportunidad.PorcentajeDedicacion,
                             Estado = recursoOportunidad.IdEstado == Activo ? "Activo" : "Inactivo",
                             IdUsuarioCreacion = recursoOportunidad.IdUsuarioCreacion,
                             FechaCreacion = recursoOportunidad.FechaCreacion,
                             IdUsuarioEdicion = recursoOportunidad.IdUsuarioEdicion,
                             FechaEdicion = recursoOportunidad.FechaEdicion
                         }).ToList()
                         .Select(x => new RecursoOportunidadDtoResponse
                         {
                             IdAsignacion = x.IdAsignacion,
                             DescripcionRecurso = x.DescripcionRecurso,
                             DescripcionRolRecurso = x.DescripcionRolRecurso,
                             strFInicioAsignacion = x.FInicioAsignacion != null ? Funciones.FormatoFecha(x.FInicioAsignacion.Value) : "",
                             strFFinAsignacion = x.FFinAsignacion != null ? Funciones.FormatoFecha(x.FFinAsignacion.Value) : "",
                             PorcentajeDedicacion = x.PorcentajeDedicacion,
                             Estado = x.Estado,
                             IdUsuarioCreacion = x.IdUsuarioCreacion,
                             FechaCreacion = x.FechaCreacion,
                             IdUsuarioEdicion = x.IdUsuarioEdicion,
                             FechaEdicion = x.FechaEdicion
                         }).Distinct().OrderBy(x => x.IdOportunidad).AsQueryable().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new RecursoOportunidadPaginadoDtoResponse
            {
                ListRecursoOportunidadDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

    }
}
