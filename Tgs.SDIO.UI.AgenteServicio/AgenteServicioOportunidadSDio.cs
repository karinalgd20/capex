﻿using ChannelAdam.ServiceModel;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioOportunidadSDio;

namespace Tgs.SDIO.UI.AgenteServicio
{
    public class AgenteServicioOportunidadSDio  : IDisposable
    {
        private readonly IServiceConsumer<IServicioOportunidadSDio> proxy;

        public AgenteServicioOportunidadSDio()
        {
            proxy = ServiceConsumerFactory.Create<IServicioOportunidadSDio>(() => new ServicioOportunidadSDioClient());
        }
        public T InvocarFuncionAsync<T>(Expression<Func<IServicioOportunidadSDio, T>> func)
        {
            var result = proxy.Consume<T>(func);
            return AgenteUtil.ObtenerResultado<T>(result);
        }

        public async Task InvocarAccionAsync(Expression<Func<IServicioOportunidadSDio, Task>> func)
        {
            var result = await proxy.ConsumeAsync(func);
            AgenteUtil.FinalizarResultado(result);
        }

        public void Dispose()
        {
            if (proxy != null)
                proxy.Dispose();
        }
    }
}
