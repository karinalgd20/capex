﻿using ChannelAdam.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioCapexSDio;

namespace Tgs.SDIO.UI.AgenteServicio
{


    public class AgenteServicioCapexSDio : IDisposable
    {

        private readonly IServiceConsumer<IServicioCapexSDio> proxy;
        public AgenteServicioCapexSDio()
        {

            proxy = ServiceConsumerFactory.Create<IServicioCapexSDio>(() => new ServicioCapexSDioClient());
        }

        public T InvocarFuncionAsync<T>(Expression<Func<IServicioCapexSDio, T>> func)
        {
            var result = proxy.Consume<T>(func);
            return AgenteUtil.ObtenerResultado<T>(result);
        }

        public async Task InvocarAccionAsync(Expression<Func<IServicioCapexSDio, Task>> func)
        {
            var result = await proxy.ConsumeAsync(func);
            AgenteUtil.FinalizarResultado(result);
        }

        public void Dispose()
        {
            if (proxy != null)
                proxy.Dispose();

        }


    }
}
