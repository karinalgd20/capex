﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ChannelAdam.ServiceModel;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioTrazabilidadSDio;

namespace Tgs.SDIO.UI.AgenteServicio
{
    public class AgenteServicioTrazabilidadSDio : IDisposable
    {
        private readonly IServiceConsumer<IServicioTrazabilidadSDio> proxy;
        public AgenteServicioTrazabilidadSDio()
        {
            proxy = ServiceConsumerFactory.Create<IServicioTrazabilidadSDio>(() => new ServicioTrazabilidadSDioClient());
        }

        public T InvocarFuncionAsync<T>(Expression<Func<IServicioTrazabilidadSDio, T>> func)
        {
            var result = proxy.Consume<T>(func);
            return AgenteUtil.ObtenerResultado<T>(result);
        }

        public async Task InvocarAccionAsync(Expression<Func<IServicioTrazabilidadSDio, Task>> func)
        {
            var result = await proxy.ConsumeAsync(func);
            AgenteUtil.FinalizarResultado(result);
        }

        public void Dispose()
        {
            if (proxy != null)
                proxy.Dispose();
        }
    }
}
