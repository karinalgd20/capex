﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ChannelAdam.ServiceModel;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioComunSDio;

namespace Tgs.SDIO.UI.AgenteServicio
{
    public class AgenteServicioComunSDio : IDisposable
    {
        private readonly IServiceConsumer<IServicioComunSDio> proxy;
        public AgenteServicioComunSDio()
        {
            proxy = ServiceConsumerFactory.Create<IServicioComunSDio>(() => new ServicioComunSDioClient());
        }

        public T InvocarFuncionAsync<T>(Expression<Func<IServicioComunSDio, T>> func)
        {
            var result = proxy.Consume<T>(func);
            return AgenteUtil.ObtenerResultado<T>(result);
        }

        public async Task InvocarAccionAsync(Expression<Func<IServicioComunSDio, Task>> func)
        {
            var result = await proxy.ConsumeAsync(func);
            AgenteUtil.FinalizarResultado(result);
        }

        public void Dispose()
        {
            if (proxy != null)
                proxy.Dispose();
        }
    }
}
