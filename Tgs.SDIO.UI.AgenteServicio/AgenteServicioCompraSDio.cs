﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ChannelAdam.ServiceModel;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioCompraSDio;

namespace Tgs.SDIO.UI.AgenteServicio
{
    public class AgenteServicioCompraSDio : IDisposable
    {
        private readonly IServiceConsumer<IServicioCompraSDio> proxy;
        public AgenteServicioCompraSDio()
        {
            proxy = ServiceConsumerFactory.Create<IServicioCompraSDio>(() => new ServicioCompraSDioClient());
        }

        public T InvocarFuncionAsync<T>(Expression<Func<IServicioCompraSDio, T>> func)
        {
            var result = proxy.Consume<T>(func);
            return AgenteUtil.ObtenerResultado<T>(result);
        }

        public async Task InvocarAccionAsync(Expression<Func<IServicioCompraSDio, Task>> func)
        {
            var result = await proxy.ConsumeAsync(func);
            AgenteUtil.FinalizarResultado(result);
        }

        public void Dispose()
        {
            if (proxy != null)
                proxy.Dispose();
        }
    }
}
