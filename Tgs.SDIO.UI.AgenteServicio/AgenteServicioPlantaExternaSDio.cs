﻿using ChannelAdam.ServiceModel;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioPlantaExternaSDio;

namespace Tgs.SDIO.UI.AgenteServicio
{
    public class AgenteServicioPlantaExternaSDio: IDisposable
    {
        private readonly IServiceConsumer<IServicioPlantaExternaSDio> proxy;
        public AgenteServicioPlantaExternaSDio()
        {

            proxy = ServiceConsumerFactory.Create<IServicioPlantaExternaSDio>(() => new ServicioPlantaExternaSDioClient());
        }

        public T InvocarFuncionAsync<T>(Expression<Func<IServicioPlantaExternaSDio, T>> func)
        {
            var result = proxy.Consume<T>(func);
            return AgenteUtil.ObtenerResultado<T>(result);
        }

        public async Task InvocarAccionAsync(Expression<Func<IServicioPlantaExternaSDio, Task>> func)
        {
            var result = await proxy.ConsumeAsync(func);
            AgenteUtil.FinalizarResultado(result);
        }

        public void Dispose()
        {
            if (proxy != null)
                proxy.Dispose();

        }
    }
}
