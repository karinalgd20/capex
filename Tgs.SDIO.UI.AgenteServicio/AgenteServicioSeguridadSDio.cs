﻿using ChannelAdam.ServiceModel;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioSeguridadSDio;

namespace Tgs.SDIO.UI.AgenteServicio
{
    public class AgenteServicioSeguridadSDio : IDisposable
    {
        private readonly IServiceConsumer<IServicioSeguridadSDio> proxy;
        public AgenteServicioSeguridadSDio()
        {
            proxy = ServiceConsumerFactory.Create<IServicioSeguridadSDio>(() => new ServicioSeguridadSDioClient());
        }

        public T InvocarFuncionAsync<T>(Expression<Func<IServicioSeguridadSDio, T>> func)
        {
            var result = proxy.Consume<T>(func);
            return AgenteUtil.ObtenerResultado<T>(result);
        }

        public async Task InvocarAccionAsync(Expression<Func<IServicioSeguridadSDio, Task>> func)
        {
            var result = await proxy.ConsumeAsync(func);
            AgenteUtil.FinalizarResultado(result);
        }

        public void Dispose()
        {
            if (proxy != null)
                proxy.Dispose();
        }

    }
}
