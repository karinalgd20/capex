﻿using ChannelAdam.ServiceModel;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioProyectoSDio;

namespace Tgs.SDIO.UI.AgenteServicio
{
    public class AgenteServicioProyectoSDio  : IDisposable
    {
        private readonly IServiceConsumer<IServicioProyectoSDio> proxy;

        public AgenteServicioProyectoSDio()
        {
            proxy = ServiceConsumerFactory.Create<IServicioProyectoSDio>(() => new ServicioProyectoSDioClient());
        }
        public T InvocarFuncionAsync<T>(Expression<Func<IServicioProyectoSDio, T>> func)
        {
            var result = proxy.Consume<T>(func);
            return AgenteUtil.ObtenerResultado<T>(result);
        }

        public async Task InvocarAccionAsync(Expression<Func<IServicioProyectoSDio, Task>> func)
        {
            var result = await proxy.ConsumeAsync(func);
            AgenteUtil.FinalizarResultado(result);
        }

        public void Dispose()
        {
            if (proxy != null)
                proxy.Dispose();
        }
    }
}
