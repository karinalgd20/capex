﻿using ChannelAdam.ServiceModel;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioNegocioSDio;

namespace Tgs.SDIO.UI.AgenteServicio
{
    public class AgenteServicioNegocioSDio : IDisposable
    {
        private readonly IServiceConsumer<IServicioNegocioSDio> proxy;
        public AgenteServicioNegocioSDio()
        {
            proxy = ServiceConsumerFactory.Create<IServicioNegocioSDio>(() => new ServicioNegocioSDioClient());
        }

        public T InvocarFuncionAsync<T>(Expression<Func<IServicioNegocioSDio, T>> func)
        {
            var result = proxy.Consume<T>(func);
            return AgenteUtil.ObtenerResultado<T>(result);
        }

        public async Task InvocarAccionAsync(Expression<Func<IServicioNegocioSDio, Task>> func)
        {
            var result = await proxy.ConsumeAsync(func);
            AgenteUtil.FinalizarResultado(result);
        }

        public void Dispose()
        {
            if (proxy != null)
                proxy.Dispose();
        }
    }
}
