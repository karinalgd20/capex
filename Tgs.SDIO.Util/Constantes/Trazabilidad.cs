﻿
namespace Tgs.SDIO.Util.Constantes
{
   public static class Trazabilidad
    {

        public class IdRelacionDeMaestra
        {
            public const int TipoRecursos = 125;
            public const int EstadoAuditoria = 1;
        }

        public class EstadosEntidad
        {
            public const int Inactivo = 0;
            public const int Activo = 1;
            public const int Eliminado = 2;
        }
      
    }
}
