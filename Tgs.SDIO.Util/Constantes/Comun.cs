﻿
namespace Tgs.SDIO.Util.Constantes
{
    public static class Comun
    {

        public class IdRelacionDeMaestra
        {
            public const int TipoRecursos = 125;
        }

        public class EstadosEntidad
        {
            public const int Inactivo = 0;
            public const int Activo = 1;
            public const int Eliminado = 2;
        }

        public class Perfiles
        {
            public const string GerentePreventa = "GERENTPREV";
            public const string LiderPreventa = "LIDERPREV";
            public const string Preventa = "PREVENT";
            public const string GerenteRentabilidad = "RENT";            
            public const string coordinadorServicioF4 = "CSF402";

            
        }

        public class Parametro
        {
            public const int TasaDescuentoMensual = 15;
        }

        public class ConceptoFinanciero
        {
            public const int FlujoNeto = 50;
            public const int Ingresos = 2;
            public const int Inversion = 51;
        }
        public class IndicadoresFinancieros
        {
            public const int Van = 9;
            public const int VanIngresos = 10;
            public const int VanFlujoNeto = 15;
            public const int VanInversiones = 11;
            public const int VanFlujoNetoVanIngresos = 12;
            public const int VanFlujoNetoVanInversion = 13;
            public const int PeriodoRecuperoFinanciero = 14;
            public const int VanVai = 16;
            public const int Payback = 17;
            public const int Tir = 8;
            public const int TirAnual = 23;
        }
        public class TipoFicha
        {
            public const int FichaContable = 1;
            public const int FichaFinanciero = 2;
        }

        public class ValoresGo
        {
            public const double Valor1 = 0.14;
            public const int caso1 = 10;
            public const int caso2 = 22;
            public const int caso3 = 24;
            public const int caso4 = 31;
            public const int caso5 = 39;
        }
    }
}
