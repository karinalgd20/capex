using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Util.Constantes
{
    public static class Configuracion
    {
        public static string CodigoAplicacion
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings.Get("CodigoAplicacion"));
            }
        }

        public static int CodigoEmpresa
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings.Get("CodigoEmpresa"));
            }
        }

        public static int IdSistema
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings.Get("IdSistema"));
            }
        }

        public static string CorreoSalida
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("CorreoSalida");
            }
        }

        public static string NombreSistemaCorreoSalida
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("NombreSistema");
            }
        }

        public static bool DummyServicio
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings.Get("DummyServicio"));
            }
        }
        public static string RutaServidorArchivos
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("RutaArchivo");
            }
        }

        public static int IdFasePreImplantacion
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings.Get("IdFasePreImplantacion"));
            }
        }

        public static int IdEtapaPreventa
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings.Get("IdEtapaPreventa"));
            }
        }

        public static int IdEtapaLiderJP
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings.Get("IdEtapaLiderJP"));
            }
        }

        public static int IdEtapaAF
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings.Get("IdEtapaAF"));
            }
        }

        public static int IdEtapaControl
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings.Get("IdEtapaControl"));
            }
        }

        public static int IdEtapaJP
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings.Get("IdEtapaJP"));
            }
        }
    }
}
