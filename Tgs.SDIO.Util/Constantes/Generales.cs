namespace Tgs.SDIO.Util.Constantes
{
    public static class Generales
    {
        public enum Sistemas
        {
            Comun,
            Oportunidad,
            CartaFianza,
            Proyecto,
            Compra
        }
        public class EstadoLogico
        {
            public const bool Verdadero = true;
            public const bool Falso = false;

        }
        public class Numeric
        {
            public const int Cero = 0;
            public const int Uno = 1;
            public const int Dos = 2;
            public const int Tres = 3;
            public const int Cuatro = 4;
            public const int Cinco = 5;
            public const int Seis = 6;
            public const int Siete = 7;
            public const int Ocho = 8;
            public const int Nuevo = 9;
            public const int Diez = 10;


            public const int NegativoUno = -1;
            public const int Tamanio = 10;
        }
        public class Confirmacion
        {
            public const string si = "Si";
            public const string no = "No";
        }
        public class Estados
        {

            public const int Eliminado = 2;
            public const int Inactivo = 0;
            public const int Activo = 1;
            public const int EnProceso = 1;
            public const int registrado = 2;
            public const int EvaluacionEconomica = 3;
            public const int Aprobado = 4;
            public const int Rechazado = 5;
            public const int Ganado = 6;
            public const int Cancelado = 7;
            public const int RechazadoPorCliente = 8;
            public const int Preventa = 1;
            public const int Seleccione = -1;
        }

        public class EstadosSolicitudCapex
        {

            public const int Preventa = 1;
            public const int Producto = 2;
        }
        public class EstadoDescripcion
        {
            public const string Inactivo = "Inactivo";
            public const string Activo = "Activo";
        }
        public class Proceso
        {
            public const int Valido = 0;
            public const int Invalido = 1;
        }
        public class LineasProducto
        {
            public const int Datos = 5;
        }
        public class TablaMaestra
        {
            public const int EstadoBasicos = 1;
            public const int TipoCostos = 4;
            public const int TiposCosto = 4;
            public const int ProyectoTipo = 7;
            public const int Renovacion = 8;
            public const int EstadoProyectos = 11;
            public const int TipoServicio = 14;
            public const int GerenciaProductos = 17;
            public const int TipoProyecto = 19;
            public const int TipoEmpresa = 19;
            public const int Periodos = 28;
            public const int TiposConceptos = 32;
            public const int ConceptosAdicionales = 35;
            public const int OportunidadTipoCosto = 58;
            public const int Agrupador = 66;
            public const int TipoProveedor = 74;
            public const int TipoArchivo = 75;
            public const int Medio = 84;
            public const int TipoEnlace = 89;
            public const int ActivoPasivo = 92;
            public const int Localidad = 95;
            public const int TipoMoneda = 98;
            public const int ValoresFijos = 107;        
            public const int Etapas = 129;
            public const int Madurez = 136;
            public const int SituacionActual = 254;
            public const int TipoRecursos = 125;
            public const int EstadoAuditoria = 1;
            public const int TipoActividad = 146;
            public const int TipoComplejidad = 160;
            public const int TipoEntidadEmpresa = 168;
            public const int TipoProbabilidad = 320;
            public const int TipoImpacto = 326;
            public const int TipoEstado = 316;
            public const int TipoRiesgo = 313;
            public const int TipoComponente = 332;
            public const int OficinaOportunidad = 358;
            public const int ComplejidadProyecto = 361;
            public const int TipoDocumentoAdjuntos = 370; 


        }
        public class TipoConcepto
        {
            public const int Capex = 2;
            public const int Opex = 1;
            public const int Unicos = 3;
            public const int Unicos2 = 4;
            public const int Unicos3 = 5;
            public const int Unicos4 = 6;
            public const int Unicos7 = 7;
        }
        public class TipoEquipo
        {
            public const int areas = 5;
        }
        public class EquipoTrabajo
        {
            public const int sisego = 22;
            public const int preventa = 23;
            public const int gics = 24;
            public const int calidadRed = 25;
            public const int finanzas = 26;
            public const int planeamiento = 27;
            public const int comercial = 28;
            public const int producto = 29;
            public const int jproducto = 30;
            public const int programacionKO = 31;
        }
        public class IdConcepto
        {
            public const int Depreciacion = 119;
            public const int PagoUnico = 114;
            public const int Recurrente = 115;
        }
        public class TipoPerfil
        {
            public const string Administrador = "ADM";
            public const string Gerente = "GRT";
            public const string Analista_FIN = "ANLFIN";
            public const string Product_Manager = "PDM";
            public const string Preventa = "PRSTD1";
            public const string Preventa_Compleja = "PRVC01";
            public const string Coordinador_FIN = "CoFiNa";
            public const string Analista = "Analista";
            public const string PreventaProyecto = "PREVENT";
            public const string LiderJefeProyecto = "SGP01";
            public const string JefeProyecto = "SDIOPY";
            public const string GestorPlantaExterna = "GESTPLANT1";
            public const string NegociosEstandar = "GEN_RQCD";
            public const string EmisorEstandar = "EmiteRQ_CD";
            public const string CapexPreventa = "PrevCapex";

            public const string CapexJefePreventa = "CapexJPrev";
            public const string CapexProducto = "CapexProd";
            public const string CapexJefeProducto = "CapexJProd";

            public const string coordinadorServicioF4 = "CSF402";

            public const string GerentePreventa = "GERENTPREV";
            public const string LiderPreventa = "LIDERPREV";
           // public const string Preventa = "PREVENT";

            public const string GerenteRentabilidad = "RENT";
            public const string UsuarioSolicitante = "SOLICITANT";
 

            public const string Gestor = "COGEST";
            public const string JefeProyectos = "SDIOPY";
        }
        public class Plantilla
        {
            public const int Id = 23;
            public const int Housing = 1;
            public const int Outsourcing = 2;
            public const int Social = 3;
            public const int Atento = 4;
            public const int Datos = 5;
            public const int Caratula = 1;
            public const int ECapex = 2;
        }
        public class Conceptos
        {
            public const int Circuitos = 1;
            public const int Medio3G = 2;
            public const int Opex = 3;
            public const int RouterSonda = 4;
            public const int SeguridadOpex = 5;
            public const int EstudiosEspeciales = 6;
            public const int EquiposEE = 7;
            public const int Routers = 8;
            public const int Modems = 9;
            public const int EquiposSeguridad = 10;
            public const int Hardware = 11;
            public const int Software = 12;
            public const int Gabinetes = 13;
            public const int Ghz = 14;
            public const int SolarWind = 15;
            public const int SmartVPN = 16;
            public const int Satelitales = 17;
            public const int IdRouterSonda = 177;
            public const int IdIngreso = 116;
            public const int IdCostoDirecto = 141;
            public const int IdOibda = 143;
            public const int IdCapex = 144;
        }
        public class PeriodoPlazos
        {
            public const int Anios = 1;
            public const int Diaslaborables = 2;
            public const int Diascalendario = 3;
            public const int Meses = 4;
        }
        public class ProyectoFase
        {
            public const int PreImplantacion = 4;
        }
        public class ProyectoFaseEtapa
        {
            public const int PreVenta = 9;
            public const int LiderJP = 10;
            public const int AnalistaFinanciera = 11;
            public const int Control = 12;
            public const int JefeProyecto = 13;
        }
        public class ProyectoPreVentaEstado
        {
            public const int Registrado = 41;
            public const int EnProceso = 42;
            public const int Procesado = 43;
            public const int Observado = 44;
            public const int Anulado = 45;
        }
        public class ProyectoLiderJPEstado
        {
            public const int Registrado = 41;
            public const int EnProceso = 42;
            public const int Procesado = 43;
            public const int Observado = 44;
            public const int Anulado = 45;
        }
        public class ProyectoAFEstado
        {
            public const int Registrado = 41;
            public const int EnProceso = 42;
            public const int Procesado = 43;
            public const int Observado = 44;
            public const int Anulado = 45;
        }
        public class ProyectoControlEstado
        {
            public const int Registrado = 41;
            public const int EnProceso = 42;
            public const int Procesado = 43;
            public const int Observado = 44;
            public const int Anulado = 45;
        }
        public class ProyectoJPEstado
        {
            public const int Registrado = 41;
            public const int EnProceso = 42;
            public const int Procesado = 43;
            public const int Observado = 44;
            public const int Anulado = 45;
        }

        public class Calculos
        {
            public const int Instalacion = 110;
            public const int Desinstalacion = 111;
        }
        public class MesDepreciacion
        {
            public const int meses1 = 120;
            public const int meses2 = 60;
        }
        public class ColorLinea
        {
            public const string Linea1 = "#fb0201";
            public const string Linea2 = "#fb0201";
            public const string Linea3 = "#fb0201";
            public const string Linea4 = "#fb0201";
            public const string Linea5 = "#fb0201";
        }
        public class TiposMadurez
        {
            public const string N1 = "N1";
            public const string N2 = "N2";
            public const string N3 = "N3";
            public const string N4 = "N4";
            public const string N5 = "N5";
        }
        public class TipoRecurso
        {
            public const int Personal = 1;
        }
        public enum TiposDetalleOfertaAnio
        {
            OportunidadesTrabajadas,
            Ingresos,
            Capex,
            Opex,
            Oidba
        }
        public class Tablas
        {
            public enum Capex
            {
                Topologia = 120
            }
            public enum CartaFianza
            {
            }
            public enum Comun
            {
            }
            public enum Funnel
            {
            }
            public enum Oportunidad
            {
                SituacionActual = 100
            }
            public enum PlantaExterna
            {
            }
            public enum Proyecto
            {
            }
            public enum Trazabilidad
            {
            }
        }

        public class LogEstados
        {
            public const string Estado1 = "Sin Asignar";
            public const string Estado2 = "Pendiente Cotización";
            public const string Estado3 = "Plan Control";
            public const string Estado4 = "Pendiente Asignación";
        }

        public class EstadoSedeInstalacion
        {
            public const int Activo = 1;
            public const int Eliminado = 2;
            public const int SinAsignar = 3;
            public const int PendienteCotizacion = 4;
            public const int PendienteAsignacion = 5;
            public const int PlanControl = 6;
        }

        public class Cargo
        {
            public const int AnalistaFinanciero = 4;
            public const int LiderJefeDeProyecto = 8;
            public const int GerenteCuenta = 3;
            public const int JefeProyecto = 7;
            public const int Preventa = 2;
            public const int Control = 6;
        }

        public class TipoDetalleFinanciero
        {
            public const int Consolidado = 1;
            public const int DetalleCMI = 2;
        }

        public class EtapasOportunidad
        {
            public const int Validacion = 1;
            public const int CierreOferta = 2;
            public const int CreacionRequerimiento = 3;
            public const int EmisionCircuito = 4;
        }

        public class EstadosRPA
        {
            public const int Pendiente = 1;
            public const int EnProceso = 2;
            public const int Expirado = 3;
            public const int Observado = 4;
            public const int Procesado = 5;
            public const int Cancelado = 6;
        }

    }
}