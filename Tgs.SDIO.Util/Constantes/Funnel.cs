﻿using System.Configuration;

namespace Tgs.SDIO.Util.Constantes
{
    public static class Funnel
    {
        public class ExportacionMadurez
        {
            public const string NombreHojaExportacion = "OfertasPorAño";
            public const string FormatoExportacionMontos = "###,###,##0.00";
            public const string CabeceraMes = "Mes";
            public const string CabeceraIdOportunidad = "Id de Oportunidad";
            public const string CabeceraLineaNegocio = "Linea de Negocio";
            public const string CabeceraSector = "Sector";
            public const string CabeceraNombreCliente = "Nombre de Cliente";
            public const string CabeceraCodigoCliente = "Codigo de Cliente";
            public const string CabeceraIngresoTotal = "Ingreso Total";
            public const string CabeceraCapex = "Capex";
            public const string CabeceraOpex = "Opex";
            public const string CabeceraOibda = "Oibda";
            public const string CabeceraMadurez = "Madurez";
            public const string CabeceraDetalleOportunidades = "Oportunidades Trabajadas por Linea Negocio";
            public const string CabeceraDetalleIngresos = "Ingresos por Linea de Negocio";
            public const string CabeceraDetalleCapex = "Capex por Linea Negocio";
            public const string CabeceraDetalleOpex = "Opex por Linea Negocio";
            public const string CabeceraDetalleOidba = "Oidba por Linea Negocio";
            public const string CabeceraDetalleN1 = "N1";
            public const string CabeceraDetalleN2 = "N2";
            public const string CabeceraDetalleN3 = "N3";
            public const string CabeceraDetalleN4 = "N4";
            public const string CabeceraDetalleN5 = "N5";
        }

        public class Extensiones
        {
            public const string xls = "xls";
            public const string xlsx = "xlsx";
        }

        public class Simbolos
        {
            public const string Soles = "S/.";
        }
        public class Montos
        {
            public const string MontoCero = "0.00";
        }

        public class TitulosDetalleOportunidades
        {
            public const string Preventa = "PREVENTA";
            public const string CantidadClientes = "CANTIDAD CLIENTES";
            public const string CantidadOportunidades = "CANTIDAD OPORTUNIDADES";
            public const string WinRateLider = "WIN RATE";
            public const string TiempoEntregaPromedioLider = "TIEMPO ENTREGA OFERTA PROMEDIO";
            public const string PorcentajeCumplimientoLider = "% CUMPLIMIENTO DE ENTREGA DE OFERTAS";
            public const string CarteraClientesLider = "CLIENTE";
            public const string NroOportunidadesLider = "CANTIDAD DE OPORTUNIDADES ABIERTAS";
            public const string WinRateLiderCadena = "CANTIDAD DE OFERTAS VIGENTES";
            public const string TiempoEntregaPromedioLiderCadena = "CANTIDAD DE OPORTUNIDADES TRABAJADAS";
            public const string PorcentajeCumplimientoLiderCadena = "CANTIDAD DE OPORTUNIDADES GANADAS";
            public const string NroOportunidades = "OPORTUNIDAD";
            public const string LineaNegocio = "LINEA DE NEGOCIO";
            public const string DescripcionOportunidad = "DESCRIPCION";
            public const string TipoOportunidad = "TIPO OPORT.";
            public const string CantidadOfertas = "CANT. OFERTAS";
            public const string Fase = "FASE";
            public const string Exito = "% EXITO";
            public const string CierreEstimado = "CIERRE ESTIMADO";
            public const string Oferta = "COD OFERTA";
            public const string AnalistaFinanciero = "ANALISTA FINANCIERO";
            public const string DiasAtencion = "DIAS ATENCION";
            public const string DiasAtencionPreventa = "DIAS ATENCION PREVENTA";
            public const string DiasAtencionFinanzas = "DIAS ATENCION FINANZAS";
            public const string Capex = "CAPEX";
            public const string Opex = "OPEX";
            public const string Ingresos = "INGRESOS";
            public const string Oidba = "OIDBA";
            public const string Fc = "FC";
            public const string Fcv = "FCV";
            public const string Payback = "PAYBACK";
            public const string PagoUnico = "PAGO UNICO";
            public const string PagoRecurrente = "PAGO RECURRENTE";
            public const string Meses = "MESES";
            public const string TipoCambio = "TIPO DE CAMBIO";
            public const string Isp = "ISP";
            public const string GerenteCuenta = "GERENTE DE CUENTA";
            public const string Lider = "LIDER";
            public const string EquipoPreventa = "EQUIPO PREVENTA";
            public const string CarteraClientes = "CARTERA DE CLIENTES";
            public const string Oportunidades = "OPORTUNIDADES";
            public const string EvolutivoOfertas = "EVOLUTIVO DE OFERTAS";
            public const string TotalIngresos = "TOTAL DE INGRESOS";
            public const string TotalCapex = "TOTAL DE CAPEX";
            public const string TotalOpex = "TOTAL DE OPEX";
        }

        public class NivelesDashboardReportes
        {
            public const int Nivel1 = 1;
            public const int Nivel2 = 2;
            public const int Nivel3 = 3;
            public const int Nivel4 = 4;
            public const int Nivel5 = 5;
        }

        public class Controladores
        {
            public const string Principal = "Principal";
            public const string Error = "Error";
            public const string Home = "Home";
            public const string SinAcceso = "SinAcceso";
        }

        public class Acciones
        {
            public const string Index = "Index";
            public const string DashboardFunnelConsolidado = "DashboardFunnelConsolidado";
        }

    }
}
