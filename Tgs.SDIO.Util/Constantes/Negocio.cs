﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Util.Constantes
{
    public static class Negocio
    {
        public class EtapasRPA
        {
            public const int Validacion = 1;
            public const int CierreOferta = 2;
            public const int CreacionRequerimiento = 3;
            public const int EmisionCD = 3;
        }
        public class EstadosRPA
        {
            public const int Inactivo = 0;
            public const int PorEnviar = 1;
            public const int Pendiente = 2;
            public const int EnProceso = 3;
            public const int Expirado = 4;
            public const int Observado = 5;
            public const int Procesado = 6;
            public const int Cancelado = 7;
        }


    }
}
