﻿
namespace Tgs.SDIO.Util.Constantes
{
   public static class Compra
    {

        public class IdRelacionDeMaestra
        {
            public const int TipoDocumentoSolicitante = 433;
            public const int TipoDocumentoGestor = 437;
            public const int EstadoPeticionCompra = 440;
        }

        public class EstadosPeticionCompra
        {
            public const int Registrado = 1;
            public const int Observado = 2;
            public const int Subsanado = 3;
            public const int Aprobado = 4;
        }

        public class Etapas
        {
            public const string GESTOR = "GESTOR";
            public const string CERTIFICADA = "CERTIFICADA";
        }

        public class TipoDocumento
        {
            public const int ActaAceptacion = 6;
        }

        public class TipoCompra
        {
            public const int General = 1;
            public const int DCM = 2;
            public const int Ordinaria = 3;
        }

        public class TipoCosto
        {
            public const int CentroCosto = 1;
            public const int GastoOpex = 2;
            public const int InversionCapex = 3;
        }


    }
}
