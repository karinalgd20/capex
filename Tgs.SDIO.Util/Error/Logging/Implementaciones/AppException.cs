﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Util.Error.Logging.Implementaciones
{
    public class AppException : Exception
    {
        private readonly string accion;
        private readonly TipoException tipo;
        private readonly string codigo;
        private readonly string mensaje;
        private readonly Guid id;

        public AppException(TipoException tipo, string message, string accion, Exception ex) : base(message, ex)
        {
            this.id = Guid.NewGuid();
            this.tipo = tipo;
            this.accion = accion;
            this.mensaje = message;
        }
        public AppException(TipoException tipo, string message, string accion, string codigo) : base(message)
        {
            this.id = Guid.NewGuid();
            this.tipo = tipo;
            this.accion = accion;
            this.codigo = codigo;
            this.mensaje = message;
        }

        public Guid Id
        {
            get
            {
                return id;
            }
        }

        /// <summary>
        /// Contiene el tipo de excepcion que se produjo
        /// </summary>
        public TipoException Tipo
        {
            get
            {
                return tipo;
            }
        }

        /// <summary>
        /// Contiene la acción a realizar en caso ocurra la excepción
        /// </summary>
        public string Accion
        {
            get
            {
                return accion;
            }
        }

        public string Codigo
        {
            get
            {
                return codigo;
            }
        }

        public string Mensaje
        {
            get
            {
                return mensaje;
            }
        }

        /// <summary>
        /// Obtiene el código, mensaje y acción si hubiera en formato cadena para mostrar.
        /// </summary>
        public string MensajeDefault
        {
            get
            {
                var mensaje = string.Empty;
                switch (Tipo)
                {
                    case TipoException.ReglaNegocio:
                        mensaje = !string.IsNullOrWhiteSpace(accion) ?
                            $"Mensaje: {Message}\nAcción: {Accion}\nCódigo: {Codigo}\nId: {Id}"
                            : $"Mensaje: {Message}\nCódigo: {Codigo}\nId: {Id}";
                        break;
                    case TipoException.ValidacionEntidad:
                        mensaje = !string.IsNullOrWhiteSpace(accion) ?
                            $"Mensaje: {Message}\nAcción: {Accion}\nId: {Id}"
                            : $"Mensaje: {Message}\nId: {Id}";
                        break;
                    case TipoException.Otro:
                        mensaje = !string.IsNullOrWhiteSpace(accion) ?
                            $"Mensaje: {Message}\nAcción: {Accion}\nId: {Id}"
                            : $"Mensaje: {Message}\nId: {Id}";
                        break;
                    default:
                        break;
                }
                return mensaje;
            }
        }
    }
}
