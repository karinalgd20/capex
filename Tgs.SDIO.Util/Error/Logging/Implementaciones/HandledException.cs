﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Util.Error.Logging.Implementaciones
{
    public class HandledException : Exception
    {
        public string Mensaje { get; set; }

        public HandledException(string mensaje)
        {
            Mensaje = mensaje;
        }
    }
}
