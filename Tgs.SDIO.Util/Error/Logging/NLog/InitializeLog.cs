﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Tgs.SDIO.Util.Error.Logging.NLog
{
    public static class InitializeLog
    {
        public static void Initialize(string nombre, string ruta)
        {
            var config = new LoggingConfiguration();
            var target = new FileTarget(nombre)
            {
                FileName = string.Concat(ruta, "/", nombre, "_${shortdate}.txt"),
                Layout = string.Concat(
                    "===================================================================================================================================================================================",
                    "${newline}Fecha y Hora: ${longdate}.${newline}Nivel: ${level:upperCase=true}.${newline}Detalle: ${message}.${newline}Source: ${callsite:includeSourcePath=true}.${newline}Linea: ${callsite-linenumber}.${newline}Stacktrace: ${stacktrace:topFrames=10}.${newline}Exception: ${exception:format=ToString}${newline}${newline}")
            };
            var rule = new LoggingRule("*", LogLevel.Debug, target);
            config.LoggingRules.Add(rule);

            LogManager.Configuration = config;
        }
    }
}
