﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Error.Logging.Interfaces;

namespace Tgs.SDIO.Util.Error.Logging.NLog
{
    public class NLogLoggingFactory : ILoggerFactory
    {
        public ILogger Create(string nombre)
        {
            return new NLogLogging(nombre);
        }
    }
}
