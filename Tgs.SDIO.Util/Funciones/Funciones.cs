﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Constantes;
using System.Net.Mime;
using System.Net;

namespace Tgs.SDIO.Util.Funciones
{
    public static class Funciones
    {
        public static string GenerarClaveAleatoria(int numMinusculas, int numMayusculas, int numNumeros)
        {
            string lowers = "abcdefghijklmnopqrstuvwxyz";
            string uppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string number = "0123456789";

            Random random = new Random();

            string generated = "!";
            for (int i = 1; i <= numMinusculas; i++)
                generated = generated.Insert(
                    random.Next(generated.Length),
                    lowers[random.Next(lowers.Length - 1)].ToString()
                );

            for (int i = 1; i <= numMayusculas; i++)
                generated = generated.Insert(
                    random.Next(generated.Length),
                    uppers[random.Next(uppers.Length - 1)].ToString()
                );

            for (int i = 1; i <= numNumeros; i++)
                generated = generated.Insert(
                    random.Next(generated.Length),
                    number[random.Next(number.Length - 1)].ToString()
                );

            return generated.Replace("!", string.Empty);
        }

        public static string FormatoFecha(DateTime fecha)
        {
            CultureInfo Culture = CultureInfo.CreateSpecificCulture("es-PE");
            return String.Format(Culture, "{0:dd/MM/yyyy}", fecha);
        }

        public static string FormatoFechaHora(DateTime fecha)
        {
            CultureInfo Culture = CultureInfo.CreateSpecificCulture("es-PE");
            return String.Format(Culture, "{0:dd/MM/yyyy HH:mm}", fecha);
        }
        public static string FormatoFechaHorams(Nullable<DateTime> fecha)
        {
            CultureInfo Culture = CultureInfo.CreateSpecificCulture("es-PE");
            return String.Format(Culture, "{0:dd/MM/yyyy HH:mm:ss}", fecha);
        }

        public static string FormatoHora(DateTime fecha)
        {
            CultureInfo Culture = CultureInfo.CreateSpecificCulture("es-PE");
            return String.Format(Culture, "{0:T}", fecha);
        }

        public static string GenerarNombreArchivo(string nombreArchivo)
        {
            return DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + "_" + nombreArchivo;
        }

        public static byte[] ConvertirDatosABytes(Stream input)
        {
            byte[] archivo = null;

            using (Stream inputStream = input)
            {
                MemoryStream memoryStream = inputStream as MemoryStream;

                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }

                archivo = memoryStream.ToArray();

                inputStream.Dispose();
            }

            return archivo;
        }

        public static string EnviarCorreo(DatosCorreo datosEnvio)
        {
            string respuesta = ErrorGeneralComun.Ok;

            try
            {
                MailMessage MensajeCliente = new MailMessage();

                MensajeCliente.From = new MailAddress(datosEnvio.CorreoEnvia);

                foreach (string Correo in datosEnvio.Destinatarios.Split(','))
                {
                    MensajeCliente.To.Add(new MailAddress(Correo));
                }

                if(datosEnvio.Copia != null)
                {
                    foreach (string CorreoCopia in datosEnvio.Copia.Split(','))
                    {
                        MensajeCliente.CC.Add(new MailAddress(CorreoCopia));
                    }
                }
                
                if(datosEnvio.Adjuntos != null)
                {
                    foreach (string Adjunto in datosEnvio.Adjuntos)
                    {
                        Attachment data = new Attachment(Adjunto, MediaTypeNames.Application.Octet);


                        // Add time stamp information for the file.
                        ContentDisposition disposition = data.ContentDisposition;
                        disposition.CreationDate = System.IO.File.GetCreationTime(Adjunto);
                        disposition.ModificationDate = System.IO.File.GetLastWriteTime(Adjunto);
                        disposition.ReadDate = System.IO.File.GetLastAccessTime(Adjunto);
                        // Add the file attachment to this email message.

                        MensajeCliente.Attachments.Add(data);
                    }
                }

                MensajeCliente.Subject = datosEnvio.Asunto;
                MensajeCliente.Body = datosEnvio.Cuerpo;
                MensajeCliente.BodyEncoding = System.Text.Encoding.UTF8;
                MensajeCliente.IsBodyHtml = true;

                SmtpClient SMTP = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"], Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]));

                SMTP.Credentials = CredentialCache.DefaultNetworkCredentials;

                SMTP.Send(MensajeCliente);
              //  SMTP.Dispose();
            }
            catch (Exception ex)
            {
                //respuesta = ErrorGeneralComun.Fallo;
                respuesta = ex.Message;
            }

            return respuesta;
        }

        public static int DiferenciaMeses(DateTime FechaFin, DateTime FechaInicio)
        {
            return Math.Abs((FechaFin.Month - FechaInicio.Month) + 12 * (FechaFin.Year - FechaInicio.Year));
        }

        public static string MostrarFormatoMoneda(decimal? monto)
        {
            return monto == null ? Funnel.Simbolos.Soles + Funnel.Montos.MontoCero :
                Funnel.Simbolos.Soles +
                Math.Round(monto.Value, 2, MidpointRounding.AwayFromZero).ToString("###,###,##0.00");
        }

        public static int DiferenciasDias(DateTime FechaIni, DateTime FechaFin)
        {
            TimeSpan ts = FechaFin - FechaIni;
            return ts.Days +1;

        }

        public static string ColoIndicador(int Dias,Dictionary<int, string> indicador)
        {
            string color = null;
            //if(Dias < 0) { color = "black"; }
            foreach (var x in indicador) {
                if (Dias <= x.Key) { color = x.Value; }
            
                //if (Dias < 15) { color = "red"; }
                //else if (Dias >= 16 && Dias < 31) { color = "yellow"; }
                //else if (Dias >= 31 && Dias < 61) { color = "yellow"; }
                //else if (Dias >= 61) { color = "green"; }
            }

            return (color);
        }

        public static string ColoIndicadorRenovacion(int Dias, Dictionary<int, string> indicador)
        {
            string color = null;
            foreach (var x in indicador)
            {
                if (Dias <= x.Key) { color = x.Value; }

            }

            //if(Dias < 0) { color = "red"; }
            //if (Dias >=0 && Dias <= 4) { color = "green"; }
            //else { color = "black"; }
            return (color);
        }



        public static int DiasHabiles(DateTime Fecha_Inicio, DateTime Fecha_Fin)
        {
            int DiasNormales;
            int DiasHabiles;
            int Contador;
            DateTime Aux_Fecha;
            int DiaSemana;

            string Aux_FechaInicio;
            string Aux_FechaFin;

            DiasNormales = 0;
            DiaSemana = 0;
            DiasHabiles = 0;
            Contador = 0;
            DiasNormales = Convert.ToInt32((Fecha_Fin - Fecha_Inicio).TotalDays);
            Aux_FechaInicio = Fecha_Inicio.ToString("dd-MM-yyyy");
            Aux_FechaFin = Fecha_Fin.ToString("dd-MM-yyyy");
            if (Aux_FechaInicio != Aux_FechaFin)
            {
                if (DiasNormales == 2)
                {
                    DiasHabiles = 1;
                }
                else
                {
                    while (Contador < DiasNormales)
                    {
                        Aux_Fecha = Fecha_Inicio.AddDays(Contador);
                        DiaSemana = GetDayWeek(Aux_Fecha);
                        if ((DiaSemana != 5) && (DiaSemana != 6))
                        {
                            DiasHabiles = DiasHabiles + 1;
                        }
                        Contador = Contador + 1;
                    }
                }
            }
            else
            {
                DiasHabiles = 0;
            }
            return DiasHabiles;
        }


        private static int GetDayWeek(DateTime Fecha)
        {
            int Dia = 0;
            switch (Fecha.Date.DayOfWeek.ToString())
            {
                case "Monday":
                    Dia = 0;
                    break;
                case "Tuesday":
                    Dia = 1;
                    break;
                case "Wednesday":
                    Dia = 2;
                    break;
                case "Thursday":
                    Dia = 3;
                    break;
                case "Friday":
                    Dia = 4;
                    break;
                case "Saturday":
                    Dia = 5;
                    break;
                case "Sunday":
                    Dia = 6;
                    break;
            }
            return Dia;
        }
        public static string FormatoFechaIndicadores(DateTime? fecha)
        {
           return fecha == null ? string.Empty :
                       fecha.Value.Day + "-" + fecha.Value.ToString("MMM", CultureInfo.CreateSpecificCulture("es"));
        }

        public static string UppercaseWords(string value)
        {
            char[] array = value.ToCharArray();
            // Handle the first letter in the string.
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            // Scan through the letters, checking for spaces.
            // ... Uppercase the lowercase letters following spaces.
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }
    }
}
