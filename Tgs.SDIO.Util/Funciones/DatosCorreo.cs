﻿using System.Collections.Generic;

namespace Tgs.SDIO.Util.Funciones
{
    public class DatosCorreo
    {
        public string Cuerpo { get; set; }
        public string Destinatarios { get; set; }
        public string Copia { get; set; }
        public string Asunto { get; set; }
        public string CorreoEnvia { get; set; }
        public List<string> Adjuntos { get; set; }
    }
}
