﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetodosFinancieros;

namespace Tgs.SDIO.Util.Funciones
{
    public class Formulas
    {

        public double CalculoTIR(double TasaDescuentoMensual, double[] dIngresos)
        {
            double TIR= 0;
            try
            {
                MetodosFinancieros.MetodosFinancieros ClaseMetodos = new MetodosFinancieros.MetodosFinancieros();

                 TIR = ClaseMetodos.CalculoTIR(ref dIngresos, TasaDescuentoMensual);
              
            }

            catch (Exception e)
            {
                //Console.WriteLine("{0} Exception caught.", e);
            }

            return TIR;

        }                                                                                

        public int CalcularPeriodoRecupero(List<double> dFNeto, int iDuracionOportunidad, double dTasaDescuentoMensual)
        {

            MetodosFinancieros.MetodosFinancieros ClaseMetodos = new MetodosFinancieros.MetodosFinancieros();
            List<double> dMeses = new List<double>(iDuracionOportunidad);
            dMeses = FMeses(iDuracionOportunidad + 1);

            List<double> FNetoLinea1 = new List<double>(iDuracionOportunidad);
            FNetoLinea1.Add(Math.Round(dFNeto[0]));
            for (int i = 1; i < dFNeto.Count(); i++)
            {
                FNetoLinea1.Add(Math.Round(ClaseMetodos.CalculoValorPresente(dTasaDescuentoMensual, i + 1, 0, -dFNeto[i]), 0));
            }

            List<double> FNetoLinea2 = new List<double>(iDuracionOportunidad);
            for (int i = 0; i < FNetoLinea1.Count(); i++)
            {
                if (i == 0) { FNetoLinea2.Add(0); continue; }
                if (i == 1) { FNetoLinea2.Add(FNetoLinea1[i - 1] + FNetoLinea1[i]); continue; }
                FNetoLinea2.Add(FNetoLinea2[i - 1] + FNetoLinea1[i]);
            }

            List<double> FNetoLinea3 = new List<double>(iDuracionOportunidad);
            for (int i = 0; i < FNetoLinea2.Count(); i++)
            {
                if (i == 0) { FNetoLinea3.Add(0); continue; }
                if (FNetoLinea2[i] >= 0 && FNetoLinea2[i - 1] < 0)
                    FNetoLinea3.Add(dMeses[i]);
                else
                    FNetoLinea3.Add(0);
            }

            List<int> FNetoLinea4 = new List<int>(iDuracionOportunidad);
            for (int i = 0; i < FNetoLinea2.Count(); i++)
            {
                if (FNetoLinea2[i] < 0)
                    FNetoLinea4.Add(1);
                else
                    FNetoLinea4.Add(0);
            }

            int iSumatoriaLinea4 = 0;
            iSumatoriaLinea4 = FNetoLinea4.Sum();
            return iSumatoriaLinea4;
        }

        public List<double> FMeses(int iDuracionOportunidad)
        {
            List<double> Meses = new List<double>(iDuracionOportunidad);
            for (int i = 1; i <= iDuracionOportunidad; i++)
            {
                Meses.Add(i);
            }
            return Meses;
        }

        public List<double> Ingresos()
        {
            List<double> Ingreso = new List<double>(61);
            Ingreso.Add(120);
            for (int i = 1; i < 60; i++)
            {
                Ingreso.Add(20);
            }
            return Ingreso;
        }


        public double CalcularVANIngresos(double TasaDescuentoMensual, double[] dIngresos)
        {
            MetodosFinancieros.MetodosFinancieros ClaseMetodos = new MetodosFinancieros.MetodosFinancieros();
           
            double tmpVNA = ClaseMetodos.CalculoValorActualNeto(TasaDescuentoMensual, ref dIngresos);
            return tmpVNA;
        }

    }
}
