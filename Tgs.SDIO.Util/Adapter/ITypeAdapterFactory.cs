﻿namespace Tgs.SDIO.Util.Adapter
{
    public interface ITypeAdapterFactory
    {
        ITypeAdapter Create();
    }
}
