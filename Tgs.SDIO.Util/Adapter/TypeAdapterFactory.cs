﻿namespace Tgs.SDIO.Util.Adapter
{
    public static class TypeAdapterFactory
    {
        static ITypeAdapterFactory typeAdapterFactory;

        public static void SetCurrent(ITypeAdapterFactory typeAdapterFactory)
        {
            TypeAdapterFactory.typeAdapterFactory = typeAdapterFactory;
        }

        public static ITypeAdapter CreateAdapter()
        {
            return typeAdapterFactory.Create();
        }
    }
}
