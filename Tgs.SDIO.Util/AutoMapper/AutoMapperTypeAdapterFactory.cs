﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Tgs.SDIO.Util.Adapter;

namespace Tgs.SDIO.Util.AutoMapper
{
    public class AutoMapperTypeAdapterFactory : ITypeAdapterFactory
    {
        public AutoMapperTypeAdapterFactory()
        {
            var tipos = new List<Type>();

            string[] assembliesAsArray = {"Tgs.SDIO.AL.Host" };

            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(p => assembliesAsArray.Contains(p.GetName().Name)).ToList();
            foreach (var assembly in assemblies)
            {
                foreach (var type in assembly.GetTypes())
                {
                    if (type.BaseType == typeof(Profile)) tipos.Add(type);
                }
            }

            Mapper.Initialize(cfg =>
            {
                foreach (var tipo in tipos)
                {
                    cfg.AddProfile(Activator.CreateInstance(tipo) as Profile); 
                }

            });
        }

        public ITypeAdapter Create()
        {
            return new AutoMapperTypeAdapter();
        }
    }
}
