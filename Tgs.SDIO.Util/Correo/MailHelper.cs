﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Util.Correo
{
    public class MailHelper
    {
        public static void SendMail(string mailFrom, string passwordMailFrom, string mailTo, string subject, string body, bool isBodyHtml, MailPriority priority)
        {
            using (var mailMessage = new MailMessage())
            {
                mailMessage.To.Add(new MailAddress(mailTo));
                mailMessage.From = new MailAddress(mailFrom);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = isBodyHtml;
                mailMessage.Priority = MailPriority.Normal;

                using (var smtp = new SmtpClient() { DeliveryMethod = SmtpDeliveryMethod.Network, UseDefaultCredentials = false })
                {
                    smtp.Host = ConfigurationManager.AppSettings.Get("Host");
                    smtp.Credentials = new NetworkCredential(mailFrom, passwordMailFrom);
                    smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings.Get("Port"));
                    smtp.EnableSsl = true;

                    smtp.Send(mailMessage);
                }
            }

        }
    }
}
