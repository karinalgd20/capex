﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.Entities.Entities.PlantaExterna;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Entities.Entities.Proyecto;

namespace Tgs.SDIO.AL.DataAccess.Core.Context
{
    public class DioContext : DbContext, IQueryableUnitOfWork
    {
        public DioContext() : base("name=SDioDbConnectionString")
        {
            Database.Log = (sql) => Debug.Write(sql);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.AutoDetectChangesEnabled = true;
            Configuration.ValidateOnSaveEnabled = false;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<DioContext>(null);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            #region  - CartaFianza - 
            modelBuilder.Entity<CartaFianzaMaestro>()
                .ToTable("CartaFianza", "CARTAFIANZA");

            modelBuilder.Entity<CartaFianzaColaboradorDetalle>()
                .ToTable("CartaFianzaColaboradorDetalle", "CARTAFIANZA");

            modelBuilder.Entity<CartaFianzaDetalleAccion>()
                .ToTable("CartaFianzaDetalleAccion", "CARTAFIANZA");

            modelBuilder.Entity<CartaFianzaDetalleEstado>()
                .ToTable("CartaFianzaDetalleEstado", "CARTAFIANZA");

            modelBuilder.Entity<CartaFianzaDetalleRecupero>()
                .ToTable("CartaFianzaDetalleRecupero", "CARTAFIANZA");

            modelBuilder.Entity<CartaFianzaDetalleRenovacion>()
                .ToTable("CartaFianzaDetalleRenovacion", "CARTAFIANZA");

            modelBuilder.Entity<CartaFianzaDetalleSeguimiento>()
              .ToTable("CartaFianzaDetalleSeguimiento", "CARTAFIANZA");
            

            #endregion

            #region - Comun -

            modelBuilder.Entity<Colaborador>()
               .ToTable("Colaborador", "COMUN");

            modelBuilder.Entity<SalesForceConsolidadoCabecera>()
                    .ToTable("SalesForceConsolidadoCabecera", "COMUN");
            modelBuilder.Entity<SalesForceConsolidadoDetalle>()
                 .ToTable("SalesForceConsolidadoDetalle", "COMUN");
            modelBuilder.Entity<ServicioCMILineaSubLinea>()
                    .ToTable("ServicioCMILineaSubLinea", "COMUN");

            modelBuilder.Entity<Empresa>()
                  .ToTable("Empresa", "COMUN");

            modelBuilder.Entity<Medio>()
                    .ToTable("Medio", "COMUN");

            modelBuilder.Entity<Servicio>()
                    .ToTable("Servicio", "COMUN");

            modelBuilder.Entity<EquipoTrabajo>()
                   .ToTable("EquipoTrabajo", "COMUN");

        modelBuilder.Entity<SubServicio>()
                    .ToTable("SubServicio", "COMUN");

            modelBuilder.Entity<ServicioSubServicio>()
                    .ToTable("ServicioSubServicio", "COMUN");

            modelBuilder.Entity<Archivo>()
                    .ToTable("Archivo", "COMUN");

            modelBuilder.Entity<BW>()
                    .ToTable("BW", "COMUN");

            modelBuilder.Entity<Concepto>()
                    .ToTable("Concepto", "COMUN");

            modelBuilder.Entity<Depreciacion>()
                    .ToTable("Depreciacion", "COMUN");

            modelBuilder.Entity<DG>()
                    .ToTable("DG", "COMUN");

            modelBuilder.Entity<DireccionComercial>()
                    .ToTable("DireccionComercial", "COMUN");

            modelBuilder.Entity<Fentocelda>()
                    .ToTable("Fentocelda", "COMUN");

            modelBuilder.Entity<Linea>()
                    .ToTable("Linea", "COMUN");

            modelBuilder.Entity<LineaNegocio>()
                    .ToTable("LineaNegocio", "COMUN");

            modelBuilder.Entity<Area>()
                  .ToTable("Area", "COMUN");

            modelBuilder.Entity<Maestra>()
                    .ToTable("Maestra", "COMUN");

            modelBuilder.Entity<Proveedor>()
                    .ToTable("Proveedor", "COMUN");

            modelBuilder.Entity<Sector>()
                    .ToTable("Sector", "COMUN");

            modelBuilder.Entity<ServicioCMI>()
                    .ToTable("ServicioCMI", "COMUN");

            modelBuilder.Entity<LineaProducto>()
                    .ToTable("LineaProducto", "COMUN");

            modelBuilder.Entity<Cliente>()
                    .ToTable("Cliente", "COMUN");

            modelBuilder.Entity<CasoNegocio>()
                    .ToTable("CasoNegocio", "COMUN");

            modelBuilder.Entity<CasoNegocioServicio>()
                    .ToTable("CasoNegocioServicio", "COMUN");

            modelBuilder.Entity<Costo>()
                    .ToTable("Costo", "COMUN");

            modelBuilder.Entity<MedioCosto>()
                    .ToTable("MedioCosto", "COMUN");

            modelBuilder.Entity<GerenciaProducto>()
                    .ToTable("GerenciaProducto", "COMUN");

            modelBuilder.Entity<CentroCosto>()
                    .ToTable("CentroCosto", "COMUN");

            modelBuilder.Entity<SubLinea>()
                    .ToTable("SubLinea", "COMUN");

            modelBuilder.Entity<Area>()
                    .ToTable("Area", "COMUN");

            modelBuilder.Entity<Cargo>()
                    .ToTable("Cargo", "COMUN");

            modelBuilder.Entity<RecursoLineaNegocio>()
                    .ToTable("RecursoLineaNegocio", "COMUN");

            modelBuilder.Entity<Recurso>()
                    .ToTable("Recurso", "COMUN");

            modelBuilder.Entity<RolRecurso>()
                    .ToTable("RolRecurso", "COMUN");

            modelBuilder.Entity<ConceptoSeguimiento>()
                    .ToTable("ConceptoSeguimiento", "COMUN");

            modelBuilder.Entity<Actividad>()
                    .ToTable("Actividad", "COMUN");

            modelBuilder.Entity<Fase>()
                    .ToTable("Fase", "COMUN");

            modelBuilder.Entity<Etapa>()
                    .ToTable("Etapa", "COMUN");

            modelBuilder.Entity<TipoSolucion>()
               .ToTable("TipoSolucion", "COMUN");

            modelBuilder.Entity<TipoSolicitud>()
                    .ToTable("TipoSolicitud", "COMUN");

            modelBuilder.Entity<AreaSeguimiento>()
                    .ToTable("AreaSeguimiento", "COMUN");

            modelBuilder.Entity<SegmentoNegocio>()
                    .ToTable("SegmentoNegocio", "COMUN");

            modelBuilder.Entity<AreaSegmentoNegocio>()
                    .ToTable("AreaSegmentoNegocio", "COMUN");


            modelBuilder.Entity<ActividadSegmentoNegocio>()
                    .ToTable("ActividadSegmentoNegocio", "COMUN");

            modelBuilder.Entity<Ubigeo>()
                    .ToTable("Ubigeo", "COMUN");

            modelBuilder.Entity<RiesgoProyecto>()
                    .ToTable("RiesgoProyecto", "COMUN");

            modelBuilder.Entity<RecursoJefe>()
                 .ToTable("RecursoJefe", "COMUN");

            modelBuilder.Entity<ServicioGrupo>()
                .ToTable("ServicioGrupo", "COMUN");

            modelBuilder.Entity<Parametro>()
             .ToTable("Parametro", "COMUN");
            modelBuilder.Entity<TipoCambio>()
             .ToTable("TipoCambio", "COMUN");
            modelBuilder.Entity<TipoCambioDetalle>()
             .ToTable("TipoCambioDetalle", "COMUN");

			modelBuilder.Entity<ContratoMarco>()
               .ToTable("ContratoMarco", "COMUN");
            #endregion

            #region - Capex -

            modelBuilder.Entity<Solicitud>()
               .ToTable("Solicitud", "CAPEX");

            modelBuilder.Entity<AsignacionCapexTotal>()
              .ToTable("AsignacionCapexTotal", "CAPEX");

            modelBuilder.Entity<AsignacionCapexTotalDetalle>()
              .ToTable("AsignacionCapexTotalDetalle", "CAPEX");

            modelBuilder.Entity<CapexLineaNegocio>()
              .ToTable("CapexLineaNegocio", "CAPEX");

            modelBuilder.Entity<ConceptosCapex>()
              .ToTable("ConceptosCapex", "CAPEX");

            modelBuilder.Entity<Cronograma>()
              .ToTable("Cronograma", "CAPEX");

            modelBuilder.Entity<CronogramaDetalle>()
              .ToTable("CronogramaDetalle", "CAPEX");

            modelBuilder.Entity<Diagrama>()
              .ToTable("Diagrama", "CAPEX");

            modelBuilder.Entity<DiagramaDetalle>()
              .ToTable("DiagramaDetalle", "CAPEX");

            modelBuilder.Entity<EstructuraCosto>()
              .ToTable("EstructuraCosto", "CAPEX");

            modelBuilder.Entity<EstructuraCostoGrupo>()
               .ToTable("EstructuraCostoGrupo", "CAPEX");

            modelBuilder.Entity<EstructuraCostoGrupoDetalle>()
              .ToTable("EstructuraCostoGrupoDetalle", "CAPEX");

            modelBuilder.Entity<Grupo>()
              .ToTable("Grupo", "CAPEX");

            modelBuilder.Entity<Observacion>()
              .ToTable("Observacion", "CAPEX");

            modelBuilder.Entity<Seccion>()
              .ToTable("Seccion", "CAPEX");

            modelBuilder.Entity<SolicitudCapex>()
              .ToTable("SolicitudCapex", "CAPEX");

            modelBuilder.Entity<SolicitudCapexFlujoEstado>()
              .ToTable("SolicitudCapexFlujoEstado", "CAPEX");

            modelBuilder.Entity<Topologia>()
              .ToTable("Topologia", "CAPEX");

            #endregion

            #region - Negocios -

            modelBuilder.Entity<AsociarNroOfertaSisegos>()
                .ToTable("AsociarNroOfertaSisegos", "NEGOCIOS");

            modelBuilder.Entity<AsociarServicioSisegos>()
                .ToTable("AsociarServicioSisegos", "NEGOCIOS");

            modelBuilder.Entity<IsisNroOferta>()
                .ToTable("IsisNroOferta", "NEGOCIOS");

            modelBuilder.Entity<OportunidadGanadora>()
                .ToTable("OportunidadGanadora", "NEGOCIOS");

            modelBuilder.Entity<RPAEquiposServicio>()
                .ToTable("RPAEquiposServicio", "NEGOCIOS");

            modelBuilder.Entity<RPAServiciosxNroOferta>()
                .ToTable("RPAServiciosxNroOferta", "NEGOCIOS");

            modelBuilder.Entity<RPASisegoDatosContacto>()
                .ToTable("RPASisegoDatosContacto", "NEGOCIOS");

            modelBuilder.Entity<RPASisegoDetalle>()
              .ToTable("RPASisegoDetalle", "NEGOCIOS");

            modelBuilder.Entity<SisegoCotizado>()
              .ToTable("SisegoCotizado", "NEGOCIOS");

            #endregion

            #region - Oportunidad -

            modelBuilder.Entity<Formula>()
                    .ToTable("Formula", "OPORTUNIDAD");

            modelBuilder.Entity<FormulaDetalle>()
                    .ToTable("FormulaDetalle", "OPORTUNIDAD");

            modelBuilder.Entity<ConceptoDatosCapex>()
                    .ToTable("ConceptoDatosCapex", "OPORTUNIDAD");

            modelBuilder.Entity<SubServicioDatosCaratula>()
                    .ToTable("SubServicioDatosCaratula", "OPORTUNIDAD");

            modelBuilder.Entity<LineaConceptoCMI>()
                    .ToTable("LineaConceptoCMI", "OPORTUNIDAD");

            modelBuilder.Entity<LineaNegocioCMI>()
                    .ToTable("LineaNegocioCMI", "OPORTUNIDAD");

            modelBuilder.Entity<LineaConceptoGrupo>()
                    .ToTable("LineaConceptoGrupo", "OPORTUNIDAD");

            modelBuilder.Entity<LineaPestana>()
                    .ToTable("LineaPestana", "OPORTUNIDAD");

            modelBuilder.Entity<PestanaGrupo>()
                    .ToTable("PestanaGrupo", "OPORTUNIDAD");

            modelBuilder.Entity<PestanaGrupoTipo>()
                    .ToTable("PestanaGrupoTipo", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadLineaNegocio>()
                    .ToTable("OportunidadLineaNegocio", "OPORTUNIDAD");

            modelBuilder.Entity<ProyectoLineaConceptoProyectado>()
                    .ToTable("ProyectoLineaConceptoProyectado", "OPORTUNIDAD");

            modelBuilder.Entity<ProyectoLineaProducto>()
                    .ToTable("ProyectoLineaProducto", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadServicioCMI>()
                    .ToTable("OportunidadServicioCMI", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadTipoCambio>()
                    .ToTable("OportunidadTipoCambio", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadTipoCambioDetalle>()
                    .ToTable("OportunidadTipoCambioDetalle", "OPORTUNIDAD");

            modelBuilder.Entity<ProyectoServicioCMI>()
                    .ToTable("ProyectoServicioCMI", "OPORTUNIDAD");

            modelBuilder.Entity<ProyectoServicioConcepto>()
                    .ToTable("ProyectoServicioConcepto", "OPORTUNIDAD");

            modelBuilder.Entity<ServicioConceptoDocumento>()
                    .ToTable("ServicioConceptoDocumento", "OPORTUNIDAD");

            modelBuilder.Entity<ServicioConceptoProyectado>()
                    .ToTable("ServicioConceptoProyectado", "OPORTUNIDAD");

            modelBuilder.Entity<LineaProductoCMI>()
                    .ToTable("LineaProductoCMI", "OPORTUNIDAD");

            modelBuilder.Entity<Oportunidad>()
                    .ToTable("Oportunidad", "OPORTUNIDAD");
            modelBuilder.Entity<OportunidadParametro>()
                   .ToTable("OportunidadParametro", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadFlujoCaja>()
                    .ToTable("OportunidadFlujoCaja", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadFlujoCajaConfiguracion>()
                    .ToTable("OportunidadFlujoCajaConfiguracion", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadFlujoCajaDetalle>()
                    .ToTable("OportunidadFlujoCajaDetalle", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadFlujoEstado>()
                    .ToTable("OportunidadFlujoEstado", "OPORTUNIDAD");

            modelBuilder.Entity<SubServicioDatosCapex>()
                    .ToTable("SubServicioDatosCapex", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadDocumento>()
                    .ToTable("OportunidadDocumento", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadCosto>()
                    .ToTable("OportunidadCosto", "OPORTUNIDAD");

            modelBuilder.Entity<SituacionActual>()
                    .ToTable("SituacionActual", "OPORTUNIDAD");

            modelBuilder.Entity<Visita>()
                    .ToTable("Visita", "OPORTUNIDAD");

            modelBuilder.Entity<VisitaDetalle>()
                    .ToTable("VisitaDetalle", "OPORTUNIDAD");

            modelBuilder.Entity<Sede>()
                    .ToTable("Sede", "OPORTUNIDAD");

            modelBuilder.Entity<Contacto>()
                .ToTable("Contacto", "OPORTUNIDAD");

            modelBuilder.Entity<SedeInstalacion>()
                .ToTable("SedeInstalacion", "OPORTUNIDAD");

            modelBuilder.Entity<LogEstadoSisego>()
                .ToTable("LogEstadoSisego", "OPORTUNIDAD");

            modelBuilder.Entity<ServicioSedeInstalacion>()
                .ToTable("ServicioSedeInstalacion", "OPORTUNIDAD");

            modelBuilder.Entity<Estudios>()
                .ToTable("Estudios", "OPORTUNIDAD");
            modelBuilder.Entity<Cotizacion>()
                .ToTable("Cotizacion", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadCalculado>()
                .ToTable("OportunidadCalculado", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadIndicadorFinanciero>()
                .ToTable("OportunidadIndicadorFinanciero", "OPORTUNIDAD");

            modelBuilder.Entity<SedeInstalacion>()
               .ToTable("SedeInstalacion", "OPORTUNIDAD");

            modelBuilder.Entity<PlantillaImplantacion>()
               .ToTable("PlantillaImplantacion", "OPORTUNIDAD");

            #endregion

            #region - Funnel -

            #endregion

            #region - Proyecto -

            modelBuilder.Entity<Proyecto>()
                .ToTable("Proyecto", "PROYECTO");

            modelBuilder.Entity<DetalleSolucion>()
                .ToTable("DetalleSolucion", "PROYECTO");

            modelBuilder.Entity<DetalleFacturar>()
                .ToTable("DetalleFacturar", "PROYECTO");

            modelBuilder.Entity<DetallePlazo>()
                    .ToTable("DetallePlazo", "PROYECTO");
            modelBuilder.Entity<RecursoProyecto>()
                    .ToTable("RecursoProyecto", "PROYECTO");
            modelBuilder.Entity<DocumentacionProyecto>()
                    .ToTable("DocumentacionProyecto", "PROYECTO");
            modelBuilder.Entity<DocumentacionArchivo>()
                    .ToTable("DocumentacionArchivo", "PROYECTO");
            modelBuilder.Entity<DetalleFinanciero>()
                    .ToTable("DetalleFinanciero", "PROYECTO");
            modelBuilder.Entity<ServicioCMIFinanciero>()
                    .ToTable("ServicioCMIFinanciero", "PROYECTO");
            #endregion

			#region - Compra -
            modelBuilder.Entity<AnexosArchivo>()
                .ToTable("AnexosArchivo", "COMPRAS");
            modelBuilder.Entity<AnexosPeticionCompras>()
                .ToTable("AnexosPeticionCompras", "COMPRAS");
            modelBuilder.Entity<CentroCostos>()
                .ToTable("CentroCostos", "COMPRAS");
            modelBuilder.Entity<ConfiguracionEtapa>()
                .ToTable("ConfiguracionEtapa", "COMPRAS");
            modelBuilder.Entity<Cuentas>()
                .ToTable("Cuentas", "COMPRAS");
            modelBuilder.Entity<DetalleClienteProveedor>()
                .ToTable("DetalleClienteProveedor", "COMPRAS");
            modelBuilder.Entity<DetalleCompra>()
                .ToTable("DetalleCompra", "COMPRAS");
            modelBuilder.Entity<EtapaPeticionCompras>()
                .ToTable("EtapaPeticionCompras", "COMPRAS");
            modelBuilder.Entity<GestionPeticionCompras>()
                .ToTable("GestionPeticionCompras", "COMPRAS");
            modelBuilder.Entity<PeticionCompras>()
                .ToTable("PeticionCompras", "COMPRAS");
            modelBuilder.Entity<RecursosCentroCostos>()
                .ToTable("RecursosCentroCostos", "COMPRAS");

            modelBuilder.Entity<PrePeticionCabecera>()
                .ToTable("PrePeticionCabecera", "COMPRAS");
            modelBuilder.Entity<PrePeticionDCMDetalle>()
                .ToTable("PrePeticionDCMDetalle", "COMPRAS");
            modelBuilder.Entity<PrePeticionOrdinariaDetalle>()
                .ToTable("PrePeticionOrdinariaDetalle", "COMPRAS");

            modelBuilder.Entity<PrePeticionDCMDetalle>().Property(p => p.Monto).HasPrecision(18, 6);
            modelBuilder.Entity<PrePeticionOrdinariaDetalle>().Property(p => p.Monto).HasPrecision(18, 6);
            

			#endregion  
            #region - Trazabilidad -

            modelBuilder.Entity<CasoOportunidad>()
                    .ToTable("CasoOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<OportunidadST>()
                    .ToTable("OportunidadST", "TRAZABILIDAD");

            modelBuilder.Entity<ObservacionOportunidad>()
                    .ToTable("ObservacionOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<RecursoEquipoTrabajo>()
                    .ToTable("RecursoEquipoTrabajo", "TRAZABILIDAD");

            modelBuilder.Entity<RecursoOportunidad>()
                    .ToTable("RecursoOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<EstadoCaso>()
                    .ToTable("EstadoCaso", "TRAZABILIDAD");

            modelBuilder.Entity<DetalleActividad>()
                  .ToTable("DetalleActividad", "TRAZABILIDAD");

            modelBuilder.Entity<TipoOportunidad>()
                    .ToTable("TipoOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<DatosPreventaMovil>()
                    .ToTable("DatosPreventaMovil", "TRAZABILIDAD");

            modelBuilder.Entity<MotivoOportunidad>()
                    .ToTable("MotivoOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<FuncionPropietario>()
                    .ToTable("FuncionPropietario", "TRAZABILIDAD");

            modelBuilder.Entity<TiempoAtencionEquipo>()
                   .ToTable("TiempoAtencionEquipo", "TRAZABILIDAD");

            modelBuilder.Entity<EtapaOportunidad>()
                  .ToTable("EtapaOportunidad", "COMUN");

            #endregion

            #region  - PlantaExterna -
            modelBuilder.Entity<NoPeps>()
                .ToTable("NoPeps", "PLANTAEXTERNA");

            modelBuilder.Entity<Sisego>()
                .ToTable("Sisego", "PLANTAEXTERNA");

            modelBuilder.Entity<EstadoSisego>()
                .ToTable("EstadoSisego", "PLANTAEXTERNA");

            modelBuilder.Entity<TipoCambioSisego>()
               .ToTable("TipoCambioSisego", "PLANTAEXTERNA");

            modelBuilder.Entity<AccionEstrategica>()
              .ToTable("AccionEstrategica", "PLANTAEXTERNA");
            #endregion

        }

        #region DbSets

        #region - CartaFianza -
        public virtual DbSet<CartaFianzaMaestro> CartaFianza { get; set; }
        public virtual DbSet<CartaFianzaColaboradorDetalle> CartaFianzaColaboradorDetalle { get; set; }
        public virtual DbSet<CartaFianzaDetalleAccion> CartaFianzaDetalleAccion { get; set; }
        public virtual DbSet<CartaFianzaDetalleEstado> CartaFianzaDetalleEstado { get; set; }
        public virtual DbSet<CartaFianzaDetalleRecupero> CartaFianzaDetalleRecupero { get; set; }
        public virtual DbSet<CartaFianzaDetalleRenovacion> CartaFianzaDetalleRenovacion { get; set; }
        #endregion

        #region - Comun -
        public virtual DbSet<SalesForceConsolidadoCabecera> SalesForceConsolidadoCabecera { get; set; }
        public virtual DbSet<SalesForceConsolidadoDetalle> SalesForceConsolidadoDetalle { get; set; }  
        public virtual DbSet<Colaborador> Colaborador { get; set; }
        public virtual DbSet<Area> Area { get; set; }
        public virtual DbSet<Medio> Medio { get; set; }
        public virtual DbSet<Servicio> Servicio { get; set; }
        public virtual DbSet<ServicioSubServicio> ServicioSubServicio { get; set; }
        public virtual DbSet<SubServicio> SubServicio { get; set; }
        public virtual DbSet<Archivo> Archivo { get; set; }
        public virtual DbSet<Concepto> Concepto { get; set; }
        public virtual DbSet<EquipoTrabajo> EquipoTrabajo { get; set; }
        public virtual DbSet<Parametro> Parametro { get; set; }
        public virtual DbSet<BW> BW { get; set; }
        public virtual DbSet<SubServicioDatosCaratula> SubServicioDatosCaratula { get; set; }
        public virtual DbSet<ConceptoDatosCapex> ConceptoDatosCapex { get; set; }
        public virtual DbSet<LineaConceptoCMI> LineaConceptoCMI { get; set; }
        public virtual DbSet<LineaConceptoGrupo> LineaConceptoGrupo { get; set; }
        public virtual DbSet<LineaProductoCMI> LineaProductoCMI { get; set; }
        public virtual DbSet<PestanaGrupo> PestanaGrupo { get; set; }
        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<PestanaGrupoTipo> PestanaGrupoTipo { get; set; }
        public virtual DbSet<ProyectoLineaConceptoProyectado> ProyectoLineaConceptoProyectado { get; set; }
        public virtual DbSet<ProyectoLineaProducto> ProyectoLineaProducto { get; set; }
        public virtual DbSet<OportunidadServicioCMI> OportunidadServicioCMI { get; set; }
        public virtual DbSet<OportunidadTipoCambio> OportunidadTipoCambio { get; set; }
        public virtual DbSet<OportunidadTipoCambioDetalle> OportunidadTipoCambioDetalle { get; set; }
        public virtual DbSet<ProyectoServicioCMI> ProyectoServicioCMI { get; set; }
        public virtual DbSet<ProyectoServicioConcepto> ProyectoServicioConcepto { get; set; }
        public virtual DbSet<ServicioConceptoDocumento> ServicioConceptoDocumento { get; set; }
        public virtual DbSet<ServicioConceptoProyectado> ServicioConceptoProyectado { get; set; }
        public virtual DbSet<Depreciacion> Depreciacion { get; set; }
        public virtual DbSet<DG> DG { get; set; }
        public virtual DbSet<DireccionComercial> DireccionComercial { get; set; }
        public virtual DbSet<Fentocelda> Fentocelda { get; set; }
        public virtual DbSet<Linea> Linea { get; set; }
        public virtual DbSet<LineaNegocio> LineaNegocio { get; set; }
        public virtual DbSet<LineaProducto> LineaProducto { get; set; }
        public virtual DbSet<Maestra> Maestra { get; set; }
        public virtual DbSet<Proveedor> Proveedor { get; set; }
        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<ServicioCMI> ServicioCMI { get; set; }
        public virtual DbSet<Sector> Sector { get; set; }
        public virtual DbSet<CasoNegocio> CasoNegocio { get; set; }
        public virtual DbSet<CasoNegocioServicio> CasoNegocioServicio { get; set; }
        public virtual DbSet<Costo> Costo { get; set; }
        public virtual DbSet<MedioCosto> MedioCosto { get; set; }
        public virtual DbSet<GerenciaProducto> GerenciaProducto { get; set; }
        public virtual DbSet<CentroCosto> CentroCosto { get; set; }
        public virtual DbSet<SubLinea> SubLinea { get; set; }
        public virtual DbSet<RecursoLineaNegocio> RecursoLineaNegocio { get; set; }
        public virtual DbSet<RecursoOportunidad> RecursoOportunidad { get; set; }
        public virtual DbSet<Actividad> Actividad { get; set; }
        public virtual DbSet<Cargo> Cargo { get; set; }
        public virtual DbSet<ConceptoSeguimiento> ConceptoSeguimiento { get; set; }
        public virtual DbSet<Ubigeo> Ubigeo { get; set; }
        public virtual DbSet<ServicioGrupo> ServicioGrupo { get; set; }
        public virtual DbSet<TipoSolucion> TipoSolucion { get; set; }
        public virtual DbSet<TipoCambio> TipoCambio { get; set; }
        public virtual DbSet<TipoCambioDetalle> TipoCambioDetalle { get; set; }
        
		public virtual DbSet<ContratoMarco> ContratoMarco { get; set; }
		#endregion

        #region - Capex -

        public virtual DbSet<Solicitud> Solicitud { get; set; }

         public virtual DbSet<AsignacionCapexTotal> AsignacionCapexTotal { get; set; }

        public virtual DbSet<AsignacionCapexTotalDetalle> AsignacionCapexTotalDetalle { get; set; }

        public virtual DbSet<CapexLineaNegocio> CapexLineaNegocio { get; set; }

        public virtual DbSet<ConceptosCapex> ConceptosCapex { get; set; }

        public virtual DbSet<Cronograma> Cronograma { get; set; }

        public virtual DbSet<CronogramaDetalle> CronogramaDetalle { get; set; }

        public virtual DbSet<Diagrama> Diagrama { get; set; }

        public virtual DbSet<DiagramaDetalle> DiagramaDetalle { get; set; }

        public virtual DbSet<EstructuraCosto> EstructuraCosto { get; set; }

        public virtual DbSet<EstructuraCostoGrupo> EstructuraCostoGrupo { get; set; }

        public virtual DbSet<EstructuraCostoGrupoDetalle> EstructuraCostoGrupoDetalle { get; set; }

        public virtual DbSet<Grupo> Grupo { get; set; }

        public virtual DbSet<Observacion> Observacion { get; set; }
        public virtual DbSet<Seccion> Seccion { get; set; }
        public virtual DbSet<SolicitudCapex> SolicitudCapex { get; set; }
        public virtual DbSet<SolicitudCapexFlujoEstado> SolicitudCapexFlujoEstado { get; set; }
        public virtual DbSet<Topologia> Topologia { get; set; }
        #endregion
        

        #endregion

        #region - Negocios -
        public virtual DbSet<AsociarNroOfertaSisegos> AsociarNroOfertaSisegos { get; set; }
        public virtual DbSet<AsociarServicioSisegos> AsociarServicioSisegos { get; set; }
        public virtual DbSet<IsisNroOferta> IsisNroOferta { get; set; }
        public virtual DbSet<OportunidadGanadora> OportunidadGanadora { get; set; }
        public virtual DbSet<RPAEquiposServicio> RPAEquiposServicio { get; set; }
        public virtual DbSet<RPAServiciosxNroOferta> RPAServiciosxNroOferta { get; set; }
        public virtual DbSet<RPASisegoDatosContacto> RPASisegoDatosContacto { get; set; }
        public virtual DbSet<RPASisegoDetalle> RPASisegoDetalle { get; set; }
        public virtual DbSet<SisegoCotizado> SisegoCotizado { get; set; }

        #endregion

        #region - Oportunidad -
        public virtual DbSet<Formula> Formula { get; set; }
        public virtual DbSet<FormulaDetalle> FormulaDetalle { get; set; }
        public virtual DbSet<LineaNegocioCMI> LineaNegocioCMI { get; set; }
        public virtual DbSet<LineaPestana> LineaPestana { get; set; }
        public virtual DbSet<Oportunidad> Oportunidad { get; set; }
        public virtual DbSet<OportunidadParametro> OportunidadParametro { get; set; }
        public virtual DbSet<OportunidadFlujoEstado> OportunidadFlujoEstado { get; set; }
        public virtual DbSet<OportunidadLineaNegocio> OportunidadLineaNegocio { get; set; }
        public virtual DbSet<OportunidadCosto> OportunidadCosto { get; set; }
        public virtual DbSet<OportunidadDocumento> OportunidadDocumento { get; set; }
        public virtual DbSet<OportunidadFlujoCaja> OportunidadFlujoCaja { get; set; }
        public virtual DbSet<OportunidadFlujoCajaConfiguracion> OportunidadFlujoCajaConfiguracion { get; set; }
        public virtual DbSet<OportunidadFlujoCajaDetalle> OportunidadFlujoCajaDetalle { get; set; }
        public virtual DbSet<SubServicioDatosCapex> SubServicioDatosCapex { get; set; }
        public virtual DbSet<SituacionActual> SituacionActual { get; set; }
        public virtual DbSet<ServicioCMILineaSubLinea> ServicioCMILineaSubLinea { get; set; }
        public virtual DbSet<Visita> Visita { get; set; }
        public virtual DbSet<VisitaDetalle> VisitaDetalle { get; set; }
        public virtual DbSet<Sede> Sede { get; set; }
        public virtual DbSet<Contacto> Contacto { get; set; }
        public virtual DbSet<ServicioSedeInstalacion> ServicioSedeInstalacion { get; set; }
        public virtual DbSet<Estudios> Estudios { get; set; }
        public virtual DbSet<Cotizacion> Cotizacion { get; set; }
        public virtual DbSet<OportunidadCalculado> OportunidadCalculado { get; set; }
        public virtual DbSet<OportunidadIndicadorFinanciero> OportunidadIndicadorFinanciero { get; set; }
        public virtual DbSet<SedeInstalacion> SedeInstalacion { get; set; }
        public virtual DbSet<PlantillaImplantacion> PlantillaImplantacion { get; set; }
        #endregion

        #region - Proyecto -
        public virtual DbSet<Proyecto> Proyecto { get; set; }
        public virtual DbSet<DetalleSolucion> DetalleSolucion { get; set; }
        public virtual DbSet<DetalleFacturar> DetalleFacturar { get; set; }

        public virtual DbSet<DetallePlazo> DetallePlazo { get; set; }
        public virtual DbSet<RecursoProyecto> RecursoProyecto { get; set; }
        public virtual DbSet<DocumentacionProyecto> DocumentacionProyecto { get; set; }
        public virtual DbSet<DocumentacionArchivo> DocumentacionArchivo { get; set; }
        #endregion

		#region - Compra -
        public virtual DbSet<DetalleClienteProveedor> DetalleClienteProveedor { get; set; }
        #endregion
        #region - Trazabilidad -
        public virtual DbSet<AreaSeguimiento> AreaSeguimiento { get; set; }
        public virtual DbSet<SegmentoNegocio> SegmentoNegocio { get; set; }
        public virtual DbSet<AreaSegmentoNegocio> AreaSegmentoNegocio { get; set; }
        public virtual DbSet<Recurso> Recurso { get; set; }
        public virtual DbSet<RolRecurso> RolRecurso { get; set; }
        public virtual DbSet<Etapa> EtapaOportunidad { get; set; }
        
        public virtual DbSet<ActividadSegmentoNegocio> ActividadSegmentoNegocio { get; set; }
        public virtual DbSet<Fase> Fase { get; set; }
        public virtual DbSet<ObservacionOportunidad> ObservacionOportunidad { get; set; }
        public virtual DbSet<OportunidadST> OportunidadST { get; set; }
        public virtual DbSet<CasoOportunidad> CasoOportunidad { get; set; }
        public virtual DbSet<RecursoEquipoTrabajo> RecursoEquipoTrabajo { get; set; }
        public virtual DbSet<TipoSolicitud> TipoSolicitud { get; set; }
        public virtual DbSet<EstadoCaso> EstadoCaso { get; set; }
        public virtual DbSet<DetalleActividad> DetalleActividad { get; set; }
        public virtual DbSet<TipoOportunidad> TipoOportunidad { get; set; }
        public virtual DbSet<DatosPreventaMovil> DatosPreventaMovil { get; set; }
        public virtual DbSet<MotivoOportunidad> MotivoOportunidad { get; set; }
        public virtual DbSet<FuncionPropietario> FuncionPropietario { get; set; }

        public virtual DbSet<TiempoAtencionEquipo> TiempoAtencionEquipo { get; set; }


        #endregion

        #region - PlantaExterna -
        public virtual DbSet<NoPeps> NoPeps { get; set; }
        public virtual DbSet<Sisego> Sisego { get; set; }

        public virtual DbSet<EstadoSisego> EstadoSisego { get; set; }

        public virtual DbSet<AccionEstrategica> AccionEstrategica { get; set; }

        public virtual DbSet<TipoCambioSisego> TipoCambioSisego { get; set; }

        #endregion
    

        #region IQueryableUnitOfWork
        public DbSet<TEntity> CreateSet<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }
        public void Attach<TEntity>(TEntity item) where TEntity : class
        {
            base.Entry<TEntity>(item).State = EntityState.Unchanged;
        }
        public void SetModified<TEntity>(TEntity item) where TEntity : class
        {
            base.Entry<TEntity>(item).State = EntityState.Modified;
        }
        public void ApplyCurrentValues<TEntity>(TEntity original, TEntity current) where TEntity : class
        {
            base.Entry<TEntity>(original).CurrentValues.SetValues(current);
        }
        public void Commit()
        {
            try
            {
                base.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        public async Task<int> CommitAsync()
        {
            return await base.SaveChangesAsync();
        }
        public void CommitAndRefreshChanges()
        {
            bool saveFailed = false;
            do
            {
                try
                {
                    base.SaveChanges();
                    saveFailed = false;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    ex.Entries.ToList()
                              .ForEach(entry =>
                              {
                                  entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                              });
                }
            } while (saveFailed);
        }
        public void RollbackChanges()
        {
            base.ChangeTracker
                .Entries()
                .ToList()
                .ForEach(entry => entry.State = EntityState.Unchanged);
        }
        public IEnumerable<TEntity> ExecuteQuery<TEntity>(string sqlQuery, params object[] parameters)
        {
            return base.Database.SqlQuery<TEntity>(sqlQuery, parameters);
        }
        public int ExecuteCommand(string sqlCommand, params object[] parameters)
        {
            return base.Database.ExecuteSqlCommand(sqlCommand, parameters);
        }
        public async Task<int> ExecuteCommandAsync(string sqlCommand, params object[] parameters)
        {
            return await base.Database.ExecuteSqlCommandAsync(sqlCommand, parameters);
        }
        public DbContextTransaction BeginTransaction()
        {
            return base.Database.BeginTransaction();
        }

        #endregion

    }

    public abstract class BaseDomainContext : DbContext
    {
        static BaseDomainContext()
        {
            var ensureDllIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}





