﻿using System.Linq;
using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.Web.Models;
using Tgs.SDIO.Util.Web.Funnel;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.Web.Controllers
{
    public class PrincipalController : BaseController
    {
        private readonly AgenteServicioSeguridadSDio _agenteServicioSeguridadSDio = null;
        public PrincipalController(AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            _agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        public ActionResult Index()
        {
            return View();
        }

        [CustomAuthorize]
        public ActionResult VistaPrincipal(string id)
        {
            var valor = SessionFacade.Modulo;
            SessionFacade.Modulo = (string.IsNullOrEmpty(id) ? SessionFacade.Modulo : (id.Equals(Numeric.Cero) ? SessionFacade.Modulo : id));

            var menu = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarOpcionesUsuario(User.IdUsuarioSistema));

            var menuPerfil = menu.Where(x => x.OpcionMenuDto.IdOpcion == SessionFacade.Modulo.ToString()).ToList();
            if (menuPerfil.Count == 0)
            {
                SessionFacade.Modulo = valor;

                menu = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarOpcionesUsuario(User.IdUsuarioSistema));
                menuPerfil = menu.Where(x => x.OpcionMenuDto.IdOpcion == SessionFacade.Modulo.ToString()).ToList();
            }

            var opcionPrincipal = menuPerfil[0].OpcionMenuDtoLista.First().OpcionSubMenuDto.First().UrlPagina.Split('/');
            var areaPrincipal = string.Empty;
            var controller = string.Empty;
            var action = string.Empty;
            var parametro01 = string.Empty;

            if (opcionPrincipal[1] == Funnel.Controladores.Principal ||
                opcionPrincipal[1] == Funnel.Controladores.Error ||
                opcionPrincipal[1] == Funnel.Controladores.Home ||
                opcionPrincipal[1] == Funnel.Controladores.SinAcceso)
            {
                if (opcionPrincipal[1] == Funnel.Controladores.Principal &&
                     opcionPrincipal[2] == Funnel.Acciones.DashboardFunnelConsolidado)
                {
                    controller = opcionPrincipal[1];
                    action = opcionPrincipal[2];
                    parametro01 = opcionPrincipal[3];

                    return RedirectToAction(action, controller, new { area = string.Empty, moduloReporte = parametro01 });

                }
                else
                {
                    controller = opcionPrincipal[1];
                    action = opcionPrincipal[2];

                    return RedirectToAction(action, controller, new { area = string.Empty });
                }
            }
            else
            {
                areaPrincipal = opcionPrincipal[1];
                controller = opcionPrincipal[2];
                action = opcionPrincipal.Length == 4 ? opcionPrincipal[3] : Funnel.Acciones.Index;

                return RedirectToAction(action, controller, new { area = areaPrincipal });
            }

        }

        [CustomAuthorize]
        public ActionResult DashboardCartaFianza(string idOpcion)
        {

            //SessionFacade.Modulo = idOpcion;
            //ViewBag.Titulo = idOpcion.Equals(Cadenas.IdOpcionPreventa) ? Titulos.Titulos_DashboardPreventa : "";
            //ViewBag.CodigosPerfil = User.CodigoPerfil;
            //ViewBag.IdUsuario = User.UserId;
            //ViewBag.IdOpcion = idOpcion;
            //if (Request.IsAuthenticated)
            //    return RedirectToAction("Index", "~/Areas/CartaFianza/Views/DashBoardCartaFianza/Index.cshtml");
            ViewBag.Perfil = SessionFacade.Usuario.CodigoPerfil[0];
            return View();
        }

        [CustomAuthorize]
        public ActionResult DashboardPlantaExterna()
        {

            return View();
        }

        [CustomAuthorize]
        public ActionResult DashboardTrazabilidad()
        {
            return View();
        }

        [CustomAuthorize]
        [Route("~/Principal/DashboardFunnelConsolidado/{moduloReporte}")]
        public ActionResult DashboardFunnelConsolidado(string moduloReporte)
        {
            ViewBag.Titulo = Titulos.Titulos_DashboardPreventa;
            ViewBag.CodigosPerfil = User.CodigoPerfil;
            ViewBag.IdUsuario = User.UserId;
            ViewBag.ModuloReporte = moduloReporte;
            return View();
        }

        public ActionResult PermisoDenegado()
        {
            return View();

        }
        [CustomAuthorize]
        public ActionResult Modulo()
        {
            var menu = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarOpcionesUsuario(User.IdUsuarioSistema));

       
            return PartialView("_Modulo", menu);

        }
        public ActionResult DatosUsuario()
        {
            return PartialView(new DetalleUsuario { Usuario = SessionFacade.Usuario });
        }

        [CustomAuthorize]
        public ActionResult Menu(string id)
        {
            string valor = "";
            valor = SessionFacade.Modulo;
            SessionFacade.Modulo = (string.IsNullOrEmpty(id) ? SessionFacade.Modulo : (id.Equals(Numeric.Cero) ? SessionFacade.Modulo : id) );
            
            var menu = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarOpcionesUsuario(User.IdUsuarioSistema));

            var menuPerfil = menu.Where(x => x.OpcionMenuDto.IdOpcion== SessionFacade.Modulo.ToString()).ToList();
            if (menuPerfil.Count == 0)
            {
                SessionFacade.Modulo = valor;

                menu = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarOpcionesUsuario(User.IdUsuarioSistema));
                 menuPerfil = menu.Where(x => x.OpcionMenuDto.IdOpcion == SessionFacade.Modulo.ToString()).ToList();
            }
            return PartialView("_Menu", menuPerfil);
        }

        [CustomAuthorize]
        public ActionResult PerfilesUsuario()
        {
            var perfilesRais = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerPerfiles(User.IdUsuarioSistema)).Count();

            return PartialView("_Perfil", perfilesRais);
        }
        public ActionResult Oferta()
        {
            return View();
        }

    }
}