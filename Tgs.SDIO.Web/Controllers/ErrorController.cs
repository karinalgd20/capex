﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.Web.Models;

namespace Tgs.SDIO.Web.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index(string mensaje)
        {
            var modelo = new Error
            {
                Mensaje = mensaje
            };

            return View("~/Views/Shared/_Error.cshtml", modelo);
        }
    }
}