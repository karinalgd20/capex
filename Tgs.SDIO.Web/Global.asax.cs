﻿using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using Tgs.SDIO.Web.Models;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            InitializeContainer.Start();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.SuppressIdentityHeuristicChecks = true;
            MvcHandler.DisableMvcResponseHeader = true;

        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                UsuarioLoginSerializeModel serializeModel = JsonConvert.DeserializeObject<UsuarioLoginSerializeModel>(authTicket.UserData);

                UsuarioLogin newUser = new UsuarioLogin(authTicket.Name);
                newUser.UserId = serializeModel.UserId;
                newUser.FirstName = serializeModel.FirstName;
                newUser.Roles = serializeModel.Roles;
                newUser.CodigoPerfil = serializeModel.CodigoPerfil;
                newUser.Login = serializeModel.Login;
                newUser.Cip = serializeModel.Cip;
                newUser.IdUsuarioSistema = serializeModel.IdUsusarioSistema;
                newUser.NombrePreventa= serializeModel.NombrePreventa;
                newUser.LiderJefeProyecto = serializeModel.LiderJefeProyecto;
                newUser.JefeProyecto = serializeModel.JefeProyecto;
                HttpContext.Current.User = newUser;
            }
        }

        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove("Server");
            Response.Headers.Remove("X-AspNet-Version");
        }
    }
}
