﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmContenedor.aspx.cs" Inherits="Tgs.SDIO.Web.Reportes.frmContenedor" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SGV</title> 
    <style type="text/css" > 
        a[onclick*='exportReport(\'IMAGE\');'],
        a[onclick*='exportReport(\'MHTML\');'],
       
        a[onclick*='exportReport(\'PDF\');'],  
        a[onclick*='exportReport(\'XML\');']
        {display:none !important}
        /*
             a[onclick*='exportReport(\'WORD\');'],
            **/
    </style>
    
</head>
<body>
    <form id="form1" runat="server">
    <div xml:lang="es-pe">  
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>  
        <rsweb:ReportViewer ID="rvVisor" ShowRefreshButton="false" ProcessingMode="Remote"
            ShowParameterPrompts="false" InteractiveDeviceInfos="(Collection)"
                         Width="100%" Height="850px" runat="server"  
            ExportContentDisposition="AlwaysAttachment"></rsweb:ReportViewer>
    </div> 
    </form> 
</body>
</html>