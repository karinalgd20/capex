﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Tgs.SDIO.Util.Enumerables;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Funciones;
using System.Globalization;
using System.Web;
using Tgs.SDIO.Web.Models;

namespace Tgs.SDIO.Web.Reportes
{
    public partial class frmContenedor : System.Web.UI.Page
    {
        public static UsuarioLogin UsuarioLogin
        {
            get
            {
                return (HttpContext.Current.Request.IsAuthenticated) ? (UsuarioLogin)HttpContext.Current.User : null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) return;

            var reporte = Request.Params["reporte"];

            if ((string.IsNullOrEmpty(reporte)))
            {
                return;
            }

            var rep = (EnumReportes.Reportes)Enum.Parse(typeof(EnumReportes.Reportes), reporte);
            var reportServerUrl = ConfigurationManager.AppSettings["ReportServer"];

            switch (rep)
            {
                case EnumReportes.Reportes.Prueba:
                    Pruebas(reportServerUrl);
                    break;
                case EnumReportes.Reportes.MadurezPorMes:
                    MadurezPorMes(reportServerUrl);
                    break;
                case EnumReportes.Reportes.ProbabilidadPorMesPopup:
                    ProbabilidadPorMesPopup(reportServerUrl);
                    break;
                case EnumReportes.Reportes.LineaNegocioPeriodo:
                    LineaNegocioPeriodo(reportServerUrl);
                    break;
                case EnumReportes.Reportes.OfertasPorSector:
                    OfertasPorSector(reportServerUrl);
                    break;

                case EnumReportes.Reportes.IngresoLineaNegocioPeriodo:
                    IngresoLineaNegocioPeriodo(reportServerUrl);
                    break;

                case EnumReportes.Reportes.IngresoSectorPeriodo:
                    IngresoSectorPeriodo(reportServerUrl);
                    break;

                case EnumReportes.Reportes.DashboardPrincipal:
                    ProbabilidadPorMes(reportServerUrl);
                    break;

                case EnumReportes.Reportes.OfertaPorEstado:
                    OfertaPorEstadoPeriodo(reportServerUrl);
                    break;
                case EnumReportes.Reportes.EvolutivoOportunidad:
                    EvolutivoOportunidad(reportServerUrl);
                    break;
                case EnumReportes.Reportes.OfertasPorCliente:
                    OfertasPorCliente(reportServerUrl);
                    break;
                case EnumReportes.Reportes.SeguimientoOportunidadesPreventa:
                    SeguimientoOportunidadesPreventa(reportServerUrl);
                    break;
                case EnumReportes.Reportes.CostosOportunidad:
                    CostosOportunidad(reportServerUrl);
                    break;

                case EnumReportes.Reportes.FlujoCajaProyectado:
                    FlujoCajaProyectado(reportServerUrl);
                    break;

                case EnumReportes.Reportes.TrazabilidadOportunidad:
                    TrazabilidadOportunidad(reportServerUrl);
                    break;
                case EnumReportes.Reportes.DashboardFunnelConsolidado:
                    DashboardFunnelConsolidado(reportServerUrl);
                    break;
                case EnumReportes.Reportes.TrazabilidadQuiebresOportunidades:
                    TrazabilidadQuiebresOportunidades(reportServerUrl);
                    break;

                case EnumReportes.Reportes.TrazabilidadQuiebresGrilla:
                    TrazabilidadQuiebresGrilla(reportServerUrl);
                    break;

                case EnumReportes.Reportes.TrazabilidadFCVGrilla:
                    TrazabilidadFCVGrilla(reportServerUrl);
                    break;

                case EnumReportes.Reportes.TrazabilidadOportunidadesAtendidas:
                    TrazabilidadOportunidadesAtendidas(reportServerUrl);
                    break;

                case EnumReportes.Reportes.TrazabilidadOportunidadesAtendidasAcumuladas:
                    TrazabilidadOportunidadesAtendidasAcumuladas(reportServerUrl);
                    break;

                case EnumReportes.Reportes.TrazabilidadFcv2GraficoBarras:
                    TrazabilidadFcv2GraficoBarras(reportServerUrl);
                    break;
                case EnumReportes.Reportes.RptOportunidadSectorGerentePreventa:
                    RptOportunidadSectorGerentePreventa(reportServerUrl);
                    break;
                case EnumReportes.Reportes.ReporteConsolidadoSisegos:
                    ConsultarConsolidadoSisegos(reportServerUrl);
                    break;
                case EnumReportes.Reportes.DashBoardPlantaExterna:
                    ConsultarDashBoardSisegos(reportServerUrl);
                    break;
                case EnumReportes.Reportes.DashboardTrazabilidad:
                    DashboardTrazabilidad(reportServerUrl);
                    break;
                case EnumReportes.Reportes.Indicadores:
                    Indicadores(reportServerUrl);
                    break;
                case EnumReportes.Reportes.CartaFianzaConsolidado:
                    CartaFianzaConsolidado(reportServerUrl);
                    break;
                case EnumReportes.Reportes.CartaFianzaEstatusRecupero:
                    CartaFianzaEstatusRecupero(reportServerUrl);
                    break;
                case EnumReportes.Reportes.CartaFianzaSinRespuesta:
                    CartaFianzaSinRespuesta(reportServerUrl);
                    break;
                case EnumReportes.Reportes.CartaFianzaReporteSolicitudDevolucion:
                    CartaFianzaReporteSolicitudDevolucion(reportServerUrl);
                    break;
                case EnumReportes.Reportes.CartaFianzaRenovacionGarantia:
                    CartaFianzaRenovacionGarantia(reportServerUrl);
                    break;
                case EnumReportes.Reportes.CartaFianzaEstatusTesoreria:
                    CartaFianzaEstatusTesoreria(reportServerUrl);
                    break;
                case EnumReportes.Reportes.TrazabilidadSeguimientoPostventa:
                    TrazabilidadSeguimientoPostventa(reportServerUrl);
                    break;
                case EnumReportes.Reportes.RptIngresosEvolutivos:
                    RptIngresosEvolutivos(reportServerUrl);
                    break;
                case EnumReportes.Reportes.RptReporteOpexCapex:
                    RptReporteOpexCapex(reportServerUrl);
                    break;
                case EnumReportes.Reportes.ReporteCronogramaProyecto:
                    RptReporteCronogramaProyecto(reportServerUrl);
                    break;
                case EnumReportes.Reportes.ReportePayBackFCV:
                    ReportePayBackFCV(reportServerUrl);
                    break;
                case EnumReportes.Reportes.FCVOfertasSector:
                    FCVOfertasSector(reportServerUrl);
                    break;
                case EnumReportes.Reportes.PyC_CargabilidadLineaNegocio:
                    PyC_CargabilidadLineaNegocio(reportServerUrl);
                    break;
                case EnumReportes.Reportes.PyC_PayBackVsFCV:
                    PyC_PayBackVsFCV(reportServerUrl);
                    break;
            }
        }

        private void Pruebas(string reportServerUrl)
        {
            string fechaDerecho = Request.Params["FechaDerecho"].Trim();
            string fechaRol = Request.Params["FechaRol"].Trim();
            string periodo = Request.Params["Periodo"].Trim();
            string rovReprog = Request.Params["RovReprog"].Trim();
            string diasGozado = Request.Params["DiasGozado"].Trim();
            string diasPendienteGoze = Request.Params["DiasPendienteGoze"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/TGS.SGV/ConsultaProgramaciones";

            var parametros = new Dictionary<string, string>
            {
                {"P_FECHADERECHO", fechaDerecho},
                {"P_FECHAROL", fechaRol},
                {"P_PERIODO", periodo},
                {"P_ROVREPROG", rovReprog}
            };

            AgregarParametros(parametros);
        }

        #region ReportesFunnel

        private void MadurezPorMes(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? null : Request.Params["Mes"].Trim();
            var lineaNegocio = Request.Params["LineaNegocio"].Trim() == "0" ? null : Request.Params["LineaNegocio"].Trim();
            var sector = Request.Params["Sector"].Trim() == "0" ? null : Request.Params["Sector"].Trim();
            var probabilidad = Request.Params["Probabilidad"].Trim() == "-1" ? null : Request.Params["Probabilidad"].Trim();
            var etapa = Request.Params["Etapa"].Trim() == "0" ? null : Request.Params["Etapa"].Trim();
            var urlReporte = Request.Params["urlReporte"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptMadurez";
            rvVisor.Height = 770;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"IdLineaNegocio", lineaNegocio},
                {"IdSector", sector},
                {"ProbabilidadExito", probabilidad},
                {"Etapa", etapa},
                {"UrlProbabilidadPopup", urlReporte},
                {"IdMaestra_Madurez", Generales.TablaMaestra.Madurez.ToString()},
                {"IdMaestra_Etapa", Generales.TablaMaestra.Etapas.ToString()},
                {"IdEstado", Generales.Estados.Activo.ToString()}
            };

            AgregarParametros(parametros);
        }

        private void ProbabilidadPorMesPopup(string reportServerUrl)
        {
            var idOportunidad = Request.Params["IdOportunidad"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptProbabilidadPorMesPopup";
            rvVisor.ShowToolBar = false;

            rvVisor.Height = 800;
            rvVisor.Width = 1000;
            var parametros = new Dictionary<string, string>
            {
                {"IdOportunidad", idOportunidad}
            };

            AgregarParametros(parametros);
        }

        private void LineaNegocioPeriodo(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? null : Request.Params["Mes"].Trim();
            var probabilidad = Request.Params["Probabilidad"].Trim() == "-1" ? null : Request.Params["Probabilidad"].Trim();
            var etapa = Request.Params["Etapa"].Trim() == "0" ? null : Request.Params["Etapa"].Trim();
            var madurez = Request.Params["Madurez"].Trim() == "0" ? null : Request.Params["Madurez"].Trim();


            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptLineaNegocioPeriodo";
            rvVisor.Height = 450;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"ProbabilidadExito", probabilidad},
                {"Etapa", etapa},
                {"Madurez", madurez},
                {"IdMaestra_Madurez", Generales.TablaMaestra.Madurez.ToString()},
                {"IdMaestra_Etapa", Generales.TablaMaestra.Etapas.ToString()},
                {"IdEstado", Generales.Estados.Activo.ToString()}
            };

            AgregarParametros(parametros);
        }


        private void OfertasPorSector(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? null : Request.Params["Mes"].Trim();
            var probabilidad = Request.Params["Probabilidad"].Trim() == "-1" ? null : Request.Params["Probabilidad"].Trim();
            var etapa = Request.Params["Etapa"].Trim() == "0" ? null : Request.Params["Etapa"].Trim();
            var madurez = Request.Params["Madurez"].Trim() == "0" ? null : Request.Params["Madurez"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptSectorPeriodo";
            rvVisor.Height = 450;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"ProbabilidadExito", probabilidad},
                {"Etapa", etapa},
                {"Madurez", madurez},
                {"IdMaestra_Madurez", Generales.TablaMaestra.Madurez.ToString()},
                {"IdMaestra_Etapa", Generales.TablaMaestra.Etapas.ToString()},
                {"IdEstado", Generales.Estados.Activo.ToString()}
            };

            AgregarParametros(parametros);
        }

        private void IngresoLineaNegocioPeriodo(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? null : Request.Params["Mes"].Trim();
            var probabilidad = Request.Params["Probabilidad"].Trim() == "-1" ? null : Request.Params["Probabilidad"].Trim();
            var etapa = Request.Params["Etapa"].Trim() == "0" ? null : Request.Params["Etapa"].Trim();
            var madurez = Request.Params["Madurez"].Trim() == "0" ? null : Request.Params["Madurez"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptIngresosPorLineaNegocio";
            rvVisor.Height = 550;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"ProbabilidadExito", probabilidad},
                {"Etapa", etapa},
                {"Madurez", madurez}
            };

            AgregarParametros(parametros);
        }


        private void IngresoSectorPeriodo(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? null : Request.Params["Mes"].Trim();
            var probabilidad = Request.Params["Probabilidad"].Trim() == "-1" ? null : Request.Params["Probabilidad"].Trim();
            var etapa = Request.Params["Etapa"].Trim() == "0" ? null : Request.Params["Etapa"].Trim();
            var madurez = Request.Params["Madurez"].Trim() == "0" ? null : Request.Params["Madurez"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptIngresoSectorPorNegocio";
            rvVisor.Height = 600;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"ProbabilidadExito", probabilidad},
                {"Etapa", etapa},
                {"Madurez", madurez}
            };

            AgregarParametros(parametros);
        }

        private void ProbabilidadPorMes(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var urlReporte = Request.Params["urlReporte"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptPrincipal";
            rvVisor.Height = 800;
            
            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"UrlProbabilidadPopup", urlReporte}
            };

            AgregarParametros(parametros);
        }

        private void OfertaPorEstadoPeriodo(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? null : Request.Params["Mes"].Trim();
            var lineaNegocio = Request.Params["IdLineaNegocio"].Trim() == "0" ? null : Request.Params["IdLineaNegocio"].Trim();
            var sector = Request.Params["Sector"].Trim() == "0" ? null : Request.Params["Sector"].Trim();
            var probabilidad = Request.Params["Probabilidad"].Trim() == "-1" ? null : Request.Params["Probabilidad"].Trim();
            var madurez = Request.Params["Madurez"].Trim() == "0" ? null : Request.Params["Madurez"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptOfertasEstado";
            rvVisor.Height = 550;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"IdLineaNegocio", lineaNegocio},
                {"IdSector", sector},
                {"ProbabilidadExito", probabilidad},
                {"Madurez", madurez},
                {"IdMaestra_Madurez", Generales.TablaMaestra.Madurez.ToString()},
                {"IdEstado", Generales.Estados.Activo.ToString()}
            };

            AgregarParametros(parametros);
        }

        private void EvolutivoOportunidad(string reportServerUrl)
        {
            var idOportunidad = Request.Params["IdOportunidad"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptEvolutivoOportunidad";

            var parametros = new Dictionary<string, string>
            {
                {"IdOportunidad", idOportunidad}
            };

            AgregarParametros(parametros);
        }

        private void CostosOportunidad(string reportServerUrl)
        {
            var idOportunidad = Request.Params["IdOportunidad"].Trim();
            var numeroCaso = Request.Params["NumeroCaso"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptCostosOportunidad";

            var parametros = new Dictionary<string, string>
            {
                {"IdOportunidad", idOportunidad},
                {"NumeroCaso", numeroCaso}
            };

            AgregarParametros(parametros);
        }

        private void OfertasPorCliente(string reportServerUrl)
        {
            throw new NotImplementedException();
        }

        private void SeguimientoOportunidadesPreventa(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var urlReporteEvolutivo = Request.Params["urlReporteEvolutivo"].Trim();
            var urlReporteCostos = Request.Params["urlReporteCostos"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptSeguimientoOportunidadesPreventa";
            rvVisor.Height = 770;
            rvVisor.ShowToolBar = false;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"UrlReporteEvolutivo", urlReporteEvolutivo},
                {"UrlReporteCostos", urlReporteCostos}
            };

            AgregarParametros(parametros);
        }
        private void DashboardFunnelConsolidado(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var idJefe = Request.Params["IdJefe"];
            var idPreventa = Request.Params["IdPreventa"];
            var moduloReporte = Request.Params["ModuloReporte"];
            var tipoReporte = Request.Params["TipoReporte"];

            var modulo = (EnumReportes.ReportesModulo)Enum.Parse(typeof(EnumReportes.ReportesModulo), moduloReporte);

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.Height = 800;
            rvVisor.ShowToolBar = false;

            Dictionary<string, string> parametros = null;

            switch (modulo)
            {
                case EnumReportes.ReportesModulo.Preventa:
                    rvVisor.ServerReport.ReportPath = "/SDIO/rptReportePreventa";

                    idJefe = idJefe == "0" ? null : idJefe;
                    idPreventa = idPreventa == "0" ? null : idPreventa;

                    parametros = new Dictionary<string, string>
                    {
                        {"Anio", anio},
                        {"IdJefe", idJefe},
                        {"IdPreventa", idPreventa},
                        {"TipoReporte", tipoReporte}
                    };
                    break;
                case EnumReportes.ReportesModulo.Rentabilidad:
                    rvVisor.ServerReport.ReportPath = "/SDIO/rptReporteRentabilidad";

                    parametros = new Dictionary<string, string>
                    {
                        {"Anio", anio},
                        {"IdGerente", idJefe}
                    };
                    break;
                case EnumReportes.ReportesModulo.Producto:
                    rvVisor.ServerReport.ReportPath = "/SDIO/Producto_Dashboard";

                    parametros = new Dictionary<string, string>
                    {
                        {"Anio", anio},
                        {"Mes", null}
                    };
                    break;
                default:
                    break;
            }

            AgregarParametros(parametros);
        }

        private void RptOportunidadSectorGerentePreventa(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.Height = 470;
            rvVisor.ShowToolBar = false;

            rvVisor.ServerReport.ReportPath = "/SDIO/rptOfertasPorSector";

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio}
            };

            AgregarParametros(parametros);
        }

        private void RptIngresosEvolutivos(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var idOportunidad = Request.Params["IdOportunidad"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.Height = 470;
            rvVisor.ShowToolBar = false;

            rvVisor.ServerReport.ReportPath = "/SDIO/rptReporteIngresosEvolutivos";

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"IdOportunidad", idOportunidad}
            };

            AgregarParametros(parametros);
        }
        private void RptReporteOpexCapex(string reportServerUrl)
        {
            var idOportunidad = Request.Params["IdOportunidad"].Trim();
            var numeroCaso = Request.Params["NumeroCaso"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.Height = 470;
            rvVisor.ShowToolBar = false;

            rvVisor.ServerReport.ReportPath = "/SDIO/rptReporteOpexCapex";

            var parametros = new Dictionary<string, string>
            {
                {"IdOportunidad", idOportunidad},
                {"NumeroCaso", numeroCaso}
            };

            AgregarParametros(parametros);
        }

        private void RptReporteCronogramaProyecto(string reportServerUrl)
        {
            var idOportunidad = Request.Params["IdOportunidad"].Trim();
            var anio = Request.Params["Anio"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.Height = 470;
            rvVisor.ShowToolBar = false;

            rvVisor.ServerReport.ReportPath = "/SDIO/rptReporteCronogramaProyecto";

            var parametros = new Dictionary<string, string>
            {
                {"IdOportunidad", idOportunidad},
                {"Anio", anio}
            };

            AgregarParametros(parametros);
        }


        private void ReportePayBackFCV(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);

            rvVisor.ServerReport.ReportPath = "/SDIO/rptReportePayBackFCV";

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio}
            };

            AgregarParametros(parametros);
        }

        private void FCVOfertasSector(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Trim();

            mes = string.IsNullOrWhiteSpace(mes) ? null : mes;

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/PyC_OfertasSector";
            rvVisor.Height = 500;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes}
            };

            AgregarParametros(parametros);
        }

        private void PyC_CargabilidadLineaNegocio(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/PyC_CargabilidadLineaNegocio";
            rvVisor.Height = 500;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio}
            };

            AgregarParametros(parametros);
        }

        private void PyC_PayBackVsFCV(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/PyC_PayBackVsFCV";
            rvVisor.Height = 600;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio}
            };

            AgregarParametros(parametros);
        }

        #endregion ReportesFunnel

        #region ReportesCartaFianza
        private void CartaFianzaConsolidado(string reportServerUrl)
        {
            var NumeroOportunidad = Request.Params["NumeroOportunidad"].Trim();
            var NumeroGarantia = Request.Params["NumeroGarantia"].Trim();
            var NombreCliente = Request.Params["NombreCliente"].Trim();
            var Importe = Request.Params["Importe"].Trim();
            var IdEstado = Request.Params["IdEstado"].Trim();
            var IdTipoAccion = Request.Params["IdTipoAccion"].Trim();

            if (NumeroOportunidad == "undefined")
            {
                NumeroOportunidad = null;
            }
            if (NumeroGarantia == "undefined")
            {
                NumeroGarantia = null;
            }
            if (NombreCliente == "undefined")
            {
                NombreCliente = null;
            }
            if (Importe == "undefined")
            {
                Importe = null;
            }

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptCartaFianzaConsolidado";

            var parametros = new Dictionary<string, string>
            {
                {"NumeroOportunidad", NumeroOportunidad},
                {"NumeroGarantia", NumeroGarantia},
                {"NombreCliente", NombreCliente},
                {"Importe", Importe},
                {"IdEstado", IdEstado},
                {"IdTipoAccion", IdTipoAccion}
            };

            AgregarParametros(parametros);
        }

        private void CartaFianzaEstatusRecupero(string reportServerUrl)
        {
            var FechaInicio = Request.Params["FechaInicio"].Trim();
            var FechaFin = Request.Params["FechaFin"].Trim();
            var FechaVencimiento = Request.Params["FechaVencimiento"].Trim();
            var IdCliente = Request.Params["IdCliente"].Trim();
            var IdBanco = Request.Params["IdBanco"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptCartaFianzaEstatusRecupero";

            var parametros = new Dictionary<string, string>
            {
                {"FechaInicio", FechaInicio},
                {"FechaFin", FechaFin},
                {"FechaVencimiento", FechaVencimiento},
                {"IdCliente", IdCliente},
                {"IdBanco", IdBanco}
            };

            AgregarParametros(parametros);
        }

        private void CartaFianzaReporteSolicitudDevolucion(string reportServerUrl)
        {
            var IdCliente = Request.Params["p_IdCliente"].Trim();
            var numeroGarantia = Request.Params["p_numeroGarantia"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptCartaFianzaSolDevolucion";

            var parametros = new Dictionary<string, string>
            {
                {"p_IdCliente", IdCliente},
                 {"p_numeroGarantia", numeroGarantia}
            };

            AgregarParametros(parametros);
        }

        private void CartaFianzaSinRespuesta(string reportServerUrl)
        {
            var Indicador = Request.Params["Indicador"].Trim();
            var IdGerenteCuenta = Request.Params["IdGerenteCuenta"].Trim();
            var FechaVencimiento = Request.Params["FechaVencimiento"].Trim();
            var IdRenovarTm = Request.Params["IdRenovarTm"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptCartaFianzaSinRespuesta";

            var parametros = new Dictionary<string, string>
            {
                {"Indicador", Indicador},
                {"IdGerenteCuenta", IdGerenteCuenta},
                {"FechaVencimiento", FechaVencimiento},
                {"IdRenovarTm", IdRenovarTm}
            };

            AgregarParametros(parametros);

        }

        private void CartaFianzaRenovacionGarantia(string reportServerUrl)
        {
            var p_IdCliente = Request.Params["p_IdCliente"].Trim();
            var p_IdCartaFianza = Request.Params["p_IdCartaFianza"].Trim();
            var p_NumeroGarantia = Request.Params["p_NumeroGarantia"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptCartaFianzaRenovacionGarantia";

            var parametros = new Dictionary<string, string>
            {
                {"p_IdCliente", p_IdCliente},
                {"p_IdCartaFianza", p_IdCartaFianza},
                {"p_NumeroGarantia", p_NumeroGarantia},
            };

            AgregarParametros(parametros);
        }

        private void CartaFianzaEstatusTesoreria(string reportServerUrl)
        {
            var Indicador = Request.Params["Indicador"].Trim();
            var IdSubEstado = Request.Params["IdSubEstado"].Trim();
            var IdTipoAccion = Request.Params["IdTipoAccion"].Trim();
            var FechaCreacion = Request.Params["FechaCreacion"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptCartaFianzaEstatusTesoreria";

            var parametros = new Dictionary<string, string>
            {
                {"Indicador", Indicador},
                {"IdSubEstado", IdSubEstado},
                {"IdTipoAccion", IdTipoAccion},
                {"FechaCreacion", FechaCreacion}
            };

            AgregarParametros(parametros);
        }

        #endregion

        #region Oportunidad
        private void FlujoCajaProyectado(string reportServerUrl)
        {
            var idOportunidadLineaNegocio = Request.Params["IdOportunidadLineaNegocio"].Trim();
            var idEstado = Request.Params["IdEstado"].Trim();
            var tipoFicha = Request.Params["TipoFicha"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptProyectado";

            var parametros = new Dictionary<string, string>
            {
                {"IdOportunidadLineaNegocio", idOportunidadLineaNegocio},
                {"IdEstado", idEstado},
                {"TipoFicha", tipoFicha}
            };

            AgregarParametros(parametros);
        }

        private void Indicadores(string reportServerUrl)
        {
            var idOportunidadLineaNegocio = Request.Params["IdOportunidadLineaNegocio"].Trim();
            var idEstado = Request.Params["IdEstado"].Trim();
            var tipoFicha = Request.Params["TipoFicha"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptIndicadores";

            var parametros = new Dictionary<string, string>
            {
                {"IdOportunidadLineaNegocio", idOportunidadLineaNegocio},
                {"IdEstado", idEstado},
                {"TipoFicha", tipoFicha}
            };

            AgregarParametros(parametros);
        }

        #endregion

        #region Trazabilidad
        private void TrazabilidadOportunidad(string reportServerUrl)
        {
            var DescripcionOportunidad = Request.Params["DescripcionOportunidad"].Trim();
            var DescripcionCliente = Request.Params["DescripcionCliente"].Trim();
            var PorcentajeCierre = Request.Params["PorcentajeCierre"].Trim();
            var IdEtapa = Request.Params["IdEtapa"].Trim();
            var IdEstadoCaso = Request.Params["IdEstadoCaso"].Trim();
            var IdSegmentoNegocio = Request.Params["IdSegmentoNegocio"].Trim();
            var IdTipoOportunidad = Request.Params["IdTipoOportunidad"].Trim();
            var Codigo = Request.Params["Codigo"].Trim();
            if (string.IsNullOrEmpty(DescripcionOportunidad))
            {
                DescripcionOportunidad = null;
            }
            if (string.IsNullOrEmpty(DescripcionCliente))
            {
                DescripcionCliente = null;
            }
            if (string.IsNullOrEmpty(PorcentajeCierre))
            {
                PorcentajeCierre = null;
            }
            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptTrazabilidadOportunidad";
            rvVisor.Height = 1300;
            rvVisor.ShowToolBar = false;
            var parametros = new Dictionary<string, string>
            {
                {"DescripcionOportunidad", DescripcionOportunidad},
                {"DescripcionCliente", DescripcionCliente},
                {"PorcentajeCierre", PorcentajeCierre},
                {"IdEtapa", IdEtapa},
                {"IdEstadoCaso", IdEstadoCaso},
                {"IdSegmentoNegocio", IdSegmentoNegocio},
                {"IdTipoOportunidad", IdTipoOportunidad},
                {"Codigo", Codigo}
            };

            AgregarParametros(parametros);
        }
        private void TrazabilidadQuiebresOportunidades(string reportServerUrl)
        {
            var FechaInicio = Request.Params["FechaInicio"].Trim();
            var FechaFin = Request.Params["FechaFin"].Trim();
            var IdEtapa = Request.Params["IdEtapa"].Trim();
            var IdSegmentoNegocio = Request.Params["IdSegmentoNegocio"].Trim();
            var IdEstadoOportunidad = Request.Params["IdEstadoOportunidad"].Trim();
            var IdNivel = Request.Params["IdNivel"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptTrazabilidadQuiebresOportunidad";
            rvVisor.Height = 1300;
            rvVisor.ShowToolBar = false;
            var parametros = new Dictionary<string, string>
            {
                {"FechaInicio", FechaInicio},
                {"FechaFin", FechaFin},
                {"IdEtapa", IdEtapa},
                {"IdSegmentoNegocio", IdSegmentoNegocio},
                {"IdEstadoOportunidad", IdEstadoOportunidad},
                {"IdNivel", IdNivel}
            };

            AgregarParametros(parametros);
        }
        private void TrazabilidadQuiebresGrilla(string reportServerUrl)
        {
            var FechaInicio = Request.Params["FechaInicio"].Trim();
            var FechaFin = Request.Params["FechaFin"].Trim();
            var IdEtapa = Request.Params["IdEtapa"].Trim();
            var IdSegmentoNegocio = Request.Params["IdSegmentoNegocio"].Trim();
            var IdEstadoOportunidad = Request.Params["IdEstadoOportunidad"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptTrazabilidadQuiebresGrilla";
            rvVisor.Height = 1300;
            rvVisor.ShowToolBar = false;
            var parametros = new Dictionary<string, string>
            {
                {"FechaInicio", FechaInicio},
                {"FechaFin", FechaFin},
                {"IdEtapa", IdEtapa},
                {"IdSegmentoNegocio", IdSegmentoNegocio},
                {"IdEstadoOportunidad", IdEstadoOportunidad}
            };

            AgregarParametros(parametros);
        }

        private void TrazabilidadFCVGrilla(string reportServerUrl)
        {
            var Anio = Request.Params["Anio"].Trim();
            var MesInicio = Request.Params["MesInicio"].Trim();
            var MesFin = Request.Params["MesFin"].Trim();
            var IdEtapa = Request.Params["IdEtapa"].Trim();
            var IdRecurso = Request.Params["IdRecurso"].Trim();
            var IdEstadoOportunidad = Request.Params["IdEstadoOportunidad"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptTrazabilidadFCVGrilla";
            rvVisor.Height = 1300;
            rvVisor.ShowToolBar = false;
            var parametros = new Dictionary<string, string>
            {
                {"Anio", Anio},
                {"MesInicio", MesInicio},
                {"MesFin", MesFin},
                {"IdEtapa", IdEtapa},
                {"IdRecurso", IdRecurso},
                {"IdEstadoOportunidad", IdEstadoOportunidad}
            };

            AgregarParametros(parametros);
        }

        private void TrazabilidadOportunidadesAtendidas(string reportServerUrl)
        {
            var Anio = Request.Params["Anio"].Trim();
            var MesInicio = Request.Params["MesInicio"].Trim();
            var MesFin = Request.Params["MesFin"].Trim();
            var IdEtapa = Request.Params["IdEtapa"].Trim();
            var IdComplejidad = Request.Params["IdComplejidad"].Trim();
            var IdRecurso = Request.Params["IdRecurso"].Trim();
            var IdEstadoOportunidad = Request.Params["IdEstadoOportunidad"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptTrazabilidadOportunidadesAtendidas";
            rvVisor.Height = 1300;
            rvVisor.ShowToolBar = false;
            var parametros = new Dictionary<string, string>
            {
                {"Anio", Anio},
                {"MesInicio", MesInicio},
                {"MesFin", MesFin},
                {"IdEtapa", IdEtapa},
                {"IdComplejidad", IdComplejidad},
                {"IdRecurso", IdRecurso},
                {"IdEstadoOportunidad", IdEstadoOportunidad}
            };

            AgregarParametros(parametros);
        }

        private void TrazabilidadOportunidadesAtendidasAcumuladas(string reportServerUrl)
        {
            var Anio = Request.Params["Anio"].Trim();
            var MesInicio = Request.Params["MesInicio"].Trim();
            var MesFin = Request.Params["MesFin"].Trim();
            var IdEtapa = Request.Params["IdEtapa"].Trim();
            var IdRecurso = Request.Params["IdRecurso"].Trim();
            var IdEstadoOportunidad = Request.Params["IdEstadoOportunidad"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptTrazabilidadOportunidadesAtendidasAcumuladas";
            rvVisor.Height = 1300;
            rvVisor.ShowToolBar = false;
            var parametros = new Dictionary<string, string>
            {
                {"Anio", Anio},
                {"MesInicio", MesInicio},
                {"MesFin", MesFin},
                {"IdEtapa", IdEtapa},
                {"IdRecurso", IdRecurso},
                {"IdEstadoOportunidad", IdEstadoOportunidad}
            };

            AgregarParametros(parametros);
        }

        private void TrazabilidadFcv2GraficoBarras(string reportServerUrl)
        {
            var Anio = Request.Params["Anio"].Trim();
            var MesInicio = Request.Params["MesInicio"].Trim();
            var MesFin = Request.Params["MesFin"].Trim();
            var IdEtapa = Request.Params["IdEtapa"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptTrazabilidadFcv2GraficoBarras";
            rvVisor.Height = 1300;
            rvVisor.ShowToolBar = false;
            var parametros = new Dictionary<string, string>
            {
                {"Anio", Anio},
                {"MesInicio", MesInicio},
                {"MesFin", MesFin},
                {"IdEtapa", IdEtapa}
            };

            AgregarParametros(parametros);
        }


        private void DashboardTrazabilidad(string reportServerUrl)
        {

            var anio = Request.Params["Anio"].Trim();
            var idJefe = Request.Params["IdJefe"];
            var idPreventa = Request.Params["IdPreventa"];
            var tipoReporte = Request.Params["TipoReporte"];


            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.Height = 800;
            rvVisor.ShowToolBar = false;
            rvVisor.ServerReport.ReportPath = "/SDIO/rptTrazabilidadDashboard";

            idJefe = idJefe == "0" ? null : idJefe;
            idPreventa = idPreventa == "0" ? null : idPreventa;

            var parametros = new Dictionary<string, string>
            {
                        {"Anio", anio},
                        {"IdJefe", idJefe},
                        {"IdPreventa", idPreventa},
                        {"TipoReporte", tipoReporte}
            };


            AgregarParametros(parametros);
        }

        private void TrazabilidadSeguimientoPostventa(string reportServerUrl)
        {
            var FechaInicio = Request.Params["FechaInicio"].Trim();
            var FechaFin = Request.Params["FechaFin"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptTrazabilidadSeguimientoPostventa";
            rvVisor.Height = 1300;
            rvVisor.ShowToolBar = false;
            var parametros = new Dictionary<string, string>
            {
                {"FechaInicio", FechaInicio},
                {"FechaFin", FechaFin}
            };

            AgregarParametros(parametros);
        }

        #endregion

        #region PlantaEXterna
        private void ConsultarConsolidadoSisegos(string reportServerUrl)
        {
            var sisego = Request.Params["sisego"].Trim();
            var idproyecto = Request.Params["IdProyecto"].Trim();
            var pep1 = Request.Params["Pep1"].Trim();
            var salesforce = Request.Params["Salesforce"].Trim();
            var liderProyecto = UsuarioLogin.LiderJefeProyecto;
            var jefeProyecto = UsuarioLogin.JefeProyecto;
            var preventa = UsuarioLogin.NombrePreventa;
            var perfil = UsuarioLogin.CodigoPerfil.Any(x => x.Equals(Generales.TipoPerfil.GestorPlantaExterna)).ToString();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);

            rvVisor.ServerReport.ReportPath = "/SDIO/rptPlantaExternaConsolidadoSisego";

            var parametros = new Dictionary<string, string>
            {
                {"sisego", sisego},
                {"IdProyecto", idproyecto},
                {"Pep1", pep1},
                {"Salesforce", salesforce},
                {"Lp", liderProyecto},
                {"Jp", jefeProyecto},
                {"Pv", preventa},
                {"Perfil", perfil}
            };

            AgregarParametros(parametros);
        }

        private void ConsultarDashBoardSisegos(string reportServerUrl)
        {
            var Lp = UsuarioLogin.LiderJefeProyecto;
            var Jp = UsuarioLogin.JefeProyecto;
            var Pv = UsuarioLogin.NombrePreventa;

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptPlantaExternaEstatusPresupuesto";
            rvVisor.Height = 1100;
            rvVisor.ShowToolBar = false;

            var parametros = new Dictionary<string, string>
            {
                {"Lp", Lp},
                {"Jp", Jp},
                {"Pv", Pv}
            };

            AgregarParametros(parametros);
        }

        #endregion

        private void AgregarParametros(Dictionary<string, string> parametros)
        {
            var paramList = parametros.Select(item => new ReportParameter(item.Key, item.Value)).ToList();

            rvVisor.ServerReport.SetParameters(paramList);
            rvVisor.ServerReport.Refresh();
        }

    }
}
