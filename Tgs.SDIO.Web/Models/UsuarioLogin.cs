﻿using System.Linq;
using System.Security.Principal;

namespace Tgs.SDIO.Web.Models
{
    public class UsuarioLogin : IPrincipal
    {
        public UsuarioLogin(string Username)
        {
            this.Identity = new GenericIdentity(Username);
        }

        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role)
        {
            if (Roles.Any(r => role.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        } 

        public int  UserId { get; set; }
        public string FirstName { get; set; }
        public string[] CodigoPerfil { get; set; } 
        public string[] Roles { get; set; }  
        public string CodigoContrata { get; set; }
        public string TipoAsistente { get; set; }
        public int IdUsuarioSistema { get; set; }
        public string Login { get; set; }
        public string Cip { get; set; }
        public string NombrePreventa { get; set; }
        public string JefeProyecto { get; set; }
        public string LiderJefeProyecto { get; set; }
    }

    public class UsuarioLoginSerializeModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string[] CodigoPerfil { get; set; }
        public string[] Roles { get; set; }
        public int IdUsusarioSistema { get; set; }
        public string Login { get; set; }
        public string Cip { get; set; }
        public string EmailUser { get; set; }
        public string Email { get; set; } 
        public string NombrePreventa { get; set; } 
        public string JefeProyecto { get; set; }
        public string LiderJefeProyecto { get; set; }
    }
}