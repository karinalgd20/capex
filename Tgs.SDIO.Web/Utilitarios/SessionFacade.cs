﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;
using Tgs.SDIO.Web.Models;

namespace Tgs.SDIO.Web.Utilitarios
{
    public static class SessionFacade
    {                  
        public static void EliminarSesion()
        {
            HttpContext.Current.Session.Abandon();
            FormsAuthentication.SignOut();
            
            HttpCookie cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(cookie);
                HttpContext.Current.User = null;
            }

        }

        public static void CrearSesion(UsuarioDtoResponse usuarioRais)
        {
            UsuarioLoginSerializeModel serializeModel = new UsuarioLoginSerializeModel();
            serializeModel.FirstName = string.Concat(usuarioRais.Nombres, " ", usuarioRais.Apellidos);
            serializeModel.UserId = usuarioRais.IdUsuario;
            serializeModel.Cip = usuarioRais.CodigoCip;
            serializeModel.Login = usuarioRais.Login;
            serializeModel.IdUsusarioSistema = usuarioRais.IdUsuarioSistema;
            serializeModel.EmailUser = usuarioRais.CorreoElectronico;
            serializeModel.Roles = usuarioRais.PerfilDtoResponseLista.Select(i => i.NombrePerfil).ToArray();
            serializeModel.CodigoPerfil = usuarioRais.PerfilDtoResponseLista.Select(i => i.CodigoPerfil).ToArray();
            serializeModel.Email = usuarioRais.CorreoElectronico;
            serializeModel.JefeProyecto = usuarioRais.JefeProyecto;
            serializeModel.LiderJefeProyecto = usuarioRais.LiderJefeProyecto;
            serializeModel.NombrePreventa = usuarioRais.NombrePreventa;

            string userData = JsonConvert.SerializeObject(serializeModel);
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, usuarioRais.IdUsuario.ToString(), DateTime.Now, DateTime.Now.AddMinutes(40), false, userData);

            string encTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            HttpContext.Current.Response.Cookies.Add(faCookie);
        }

        public static UsuarioLoginSerializeModel Usuario
        {
            get
            {
                if (HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName] == null || HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName].Value == null) return null;
                var usuarioComprimido = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName].Value;
                var encTicket = FormsAuthentication.Decrypt(usuarioComprimido);
                var data = encTicket.UserData;
                var userData = JsonConvert.DeserializeObject<UsuarioLoginSerializeModel>(data);
                return userData;
            }
            set
            {
               
            }
        }
        public static string PerfilSesion;
        public static string Modulo;
        public static int IdSolicitudCapex;
        public static int IdOportunidad;
        public static int IdFlujoCaja;
    }
}