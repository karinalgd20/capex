﻿using System.Web.Mvc;
using System.Collections.Generic;
using Tgs.SDIO.Web.Models;

namespace Tgs.SDIO.Web.Utilitarios
{
    public abstract class BaseController : Controller
    {
        public Dictionary<string, object> ErrorModelState()
        {
            var errors = new Dictionary<string, object>();
            foreach (var key in ModelState.Keys)
            {
                if (ModelState[key].Errors.Count > 0)
                {
                    errors[key] = ModelState[key].Errors;

                    return errors;
                }
            }

            return errors;
        }
    

        protected virtual new UsuarioLogin User
        {
            get { return HttpContext.User as UsuarioLogin; }
        }
          
    }
}