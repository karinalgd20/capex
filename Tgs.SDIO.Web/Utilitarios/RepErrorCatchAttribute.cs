﻿using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using NLog;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;


namespace Tgs.SDIO.Web.Utilitarios
{
    public class RepErrorCatchAttribute : FilterAttribute, IExceptionFilter
    {
        readonly Logger logger = LogManager.GetCurrentClassLogger();

        public bool Enabled { get; set; }

        private RedirectToRouteResult GetRedirectToRouteResult(string mensaje = null)
        {
            var redirectoToErrorPageResult =
            new RedirectToRouteResult(
                new RouteValueDictionary {
                    { "action", "Index" },
                    { "controller", "Error" },
                    { "Area", string.Empty },
                    { "mensaje", string.IsNullOrWhiteSpace(mensaje) ? "" : mensaje}
                });
            return redirectoToErrorPageResult;
        }

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception != null)
            {
                if (filterContext.Exception is HandledException)
                {
                    filterContext.ExceptionHandled = true;
                    var mensaje = ((HandledException)filterContext.Exception).Mensaje;
                    filterContext.HttpContext.Response.Clear();
                    filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        filterContext.Result = new JsonResult()
                        {
                            Data = new
                            {
                                Respuesta = new
                                {
                                    Error = "-1",
                                    Mensaje = string.IsNullOrEmpty(mensaje)
                                        ? ""
                                        : mensaje
                                }
                            },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                    }
                    else
                    {
                        filterContext.Result = GetRedirectToRouteResult(mensaje);
                    }

                    logger.Error(filterContext.Exception, "Error del servicio. " + (string.IsNullOrEmpty(mensaje) ? "" : "Mensaje: " + mensaje));
                }
                else
                {
                    if (!filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        filterContext.ExceptionHandled = true;
                        filterContext.Result = GetRedirectToRouteResult();
                    }

                    logger.Error(filterContext.Exception, "Error de la Web");
                }
            }
        }
    }
}