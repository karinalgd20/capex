﻿using System.Reflection;
using System.Web.Mvc;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web
{
    public static class InitializeContainer
    {
        public static Container Container = new Container();
        public static void Start()
        {
            Container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            //Container.Register<ITypeAdapterFactory, AutoMapperTypeAdapterFactory>(Lifestyle.Singleton);
            //Container.Register<IEntityValidatorFactory, DataAnnotationEntityValidatorFactory>(Lifestyle.Singleton);

            Container.Register<AgenteServicioCartaFianzaSDio, AgenteServicioCartaFianzaSDio>(Lifestyle.Scoped);

            Container.Register<AgenteServicioComunSDio, AgenteServicioComunSDio>(Lifestyle.Scoped);
            Container.Register<AgenteServicioSeguridadSDio, AgenteServicioSeguridadSDio>(Lifestyle.Scoped);
            Container.Register<AgenteServicioTrazabilidadSDio, AgenteServicioTrazabilidadSDio>(Lifestyle.Scoped);
            Container.Register<AgenteServicioOportunidadSDio, AgenteServicioOportunidadSDio>(Lifestyle.Scoped);
            Container.Register<AgenteServicioFunnelSDio, AgenteServicioFunnelSDio>(Lifestyle.Scoped);
            Container.Register<AgenteServicioPlantaExternaSDio, AgenteServicioPlantaExternaSDio>(Lifestyle.Scoped);
            Container.Register<AgenteServicioNegocioSDio, AgenteServicioNegocioSDio>(Lifestyle.Scoped);
            Container.Register<AgenteServicioProyectoSDio, AgenteServicioProyectoSDio>(Lifestyle.Scoped);
             Container.Register<AgenteServicioCapexSDio, AgenteServicioCapexSDio>(Lifestyle.Scoped);
            Container.Register<AgenteServicioCompraSDio, AgenteServicioCompraSDio>(Lifestyle.Scoped);
            Container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            Container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(Container));
            //var automapperTypeAdapterFactory = Container.GetInstance<ITypeAdapterFactory>();
            //TypeAdapterFactory.SetCurrent(automapperTypeAdapterFactory);

            //var dataAnnotationEntityValidatorFactory = Container.GetInstance<IEntityValidatorFactory>();
            //EntityValidatorFactory.SetCurrent(dataAnnotationEntityValidatorFactory);
        }
    }
}