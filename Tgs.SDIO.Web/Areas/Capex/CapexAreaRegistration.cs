﻿using System.Web.Mvc;

namespace Tgs.SDIO.Web.Areas.Capex
{
    public class CapexAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Capex";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Capex_default",
                "Capex/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}