﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Capex.Controllers
{
    public class ProyectoController : Controller
    {
        private readonly AgenteServicioCapexSDio agenteServicioCapexSDio = null;
        private readonly AgenteServicioProyectoSDio agenteServicioProyectoSDio = null;
        public ProyectoController(AgenteServicioCapexSDio AgenteServicioCapexSDio,
                                  AgenteServicioProyectoSDio AgenteServicioProyectoSDio)
        {
            this.agenteServicioCapexSDio = AgenteServicioCapexSDio;
            this.agenteServicioProyectoSDio = AgenteServicioProyectoSDio;
        }
        // GET: Capex/Proyecto
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public JsonResult ListarProyectos(ProyectoDtoRequest proyectoDtoRequest)
        {
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ListarProyecto(proyectoDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


    }
}