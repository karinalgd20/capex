﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;
using Tgs.SDIO.Util.Constantes;


namespace Tgs.SDIO.Web.Areas.Capex.Controllers
{
    public class BandejaEstructuraCostoController : Controller
    {
        // GET: Solicitud/BandejaSolicitud
        private readonly AgenteServicioCapexSDio agenteServicioCapexSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public BandejaEstructuraCostoController(AgenteServicioCapexSDio agenteServicioCapexSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio,
            AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioCapexSDio = agenteServicioCapexSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }


        //[CustomAuthorize]
        //[NoCache]
        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public JsonResult ListaEstructuraCostoGrupoPaginado(EstructuraCostoGrupoDtoRequest request)
        {


            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ListaEstructuraCostoGrupoPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ObtenerCapexLineaNegocioPorIdSolicitud(CapexLineaNegocioDtoRequest request)
        {


            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ObtenerCapexLineaNegocioPorIdSolicitud(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }




    }
}