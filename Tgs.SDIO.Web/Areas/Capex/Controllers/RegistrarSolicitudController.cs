﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Capex.Controllers
{
    public class RegistrarSolicitudController : Controller
    {

        private readonly AgenteServicioCapexSDio agenteServicioCapexSDio = null;
        public RegistrarSolicitudController(AgenteServicioCapexSDio AgenteServicioCapexSDio)
        {
            agenteServicioCapexSDio = AgenteServicioCapexSDio;
        }
        // GET: Capex/RegistrarSolicitud
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Sustento()
        {
            return View("Sustento");
        }

        [HttpPost]
        public JsonResult RegistrarSolicitud(SolicitudCapexDtoRequest solicitudDtoRequest)
        {
            solicitudDtoRequest.IdEstado = Estados.Activo;
            solicitudDtoRequest.FechaCreacion = DateTime.Now;
            solicitudDtoRequest.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.RegistrarSolicitudCapex(solicitudDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarSeccion(SeccionDtoRequest seccionDtoRequest)
        {
            seccionDtoRequest.IdEstado = Estados.Activo;
            seccionDtoRequest.FechaCreacion = DateTime.Now;
            seccionDtoRequest.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.RegistrarSeccion(seccionDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarCapexLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocioDtoRequest)
        {
            capexLineaNegocioDtoRequest.IdEstado = Estados.Activo;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ListarCapexLineaNegocio(capexLineaNegocioDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarCapexLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocioDtoRequest)
        {
            capexLineaNegocioDtoRequest.IdEstado = Estados.Activo;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ActualizarCapexLineaNegocio(capexLineaNegocioDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocioDtoRequest)
        {
            capexLineaNegocioDtoRequest.IdEstado = Estados.Activo;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ListarLineaNegocio(capexLineaNegocioDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerSeccion(SeccionDtoRequest seccionDtoRequest)
        {
            seccionDtoRequest.IdEstado = Estados.Activo;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ObtenerSeccion(seccionDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerSolicitud(SolicitudCapexDtoRequest solicitudDtoRequest)
        {
            solicitudDtoRequest.IdEstado = Estados.Activo;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ObtenerSolicitudCapex(solicitudDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarSeccion(SeccionDtoRequest seccionDtoRequest)
        {
            seccionDtoRequest.IdEstado = Estados.Activo;
            seccionDtoRequest.FechaEdicion = DateTime.Now;
            seccionDtoRequest.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ActualizarSeccion(seccionDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarSolicitud(SolicitudCapexDtoRequest solicitudCapexDtoRequest)
        {
            solicitudCapexDtoRequest.IdEstado = Estados.Activo;
            solicitudCapexDtoRequest.FechaEdicion = DateTime.Now;
            solicitudCapexDtoRequest.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ActualizarSolicitudCapex(solicitudCapexDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest estado)
        {
            estado.FechaCreacion = DateTime.Now;
            estado.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.RegistrarSolicitudCapexFlujoEstado(estado));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarSolicitudCapexEstado(SolicitudCapexDtoRequest solicitud)
        {
            solicitud.FechaEdicion = DateTime.Now;
            solicitud.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ActualizarSolicitudCapexEstado(solicitud));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        
    }
}