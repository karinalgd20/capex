﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.Web.Areas.Capex.Controllers
{
    public class CapexContenedorController : Controller
    {
        private readonly AgenteServicioCapexSDio agenteServicioCapexSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        public CapexContenedorController
            (
            AgenteServicioCapexSDio agenteServicioCapexSDio,
            AgenteServicioSeguridadSDio agenteServicioSeguridadSDio
            )
        {
            this.agenteServicioCapexSDio = agenteServicioCapexSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        // GET: Capex/CapexContenedor
        public ActionResult Index(int Id)
        {
            ViewBag.ObjSolicitudCapex = Id;
            SessionFacade.IdSolicitudCapex = Id;
            return View("Contenedor");
        }

        public ActionResult Detalle()
        {
            return View("Detalle");
        }


        public JsonResult TabPorPerfilCapexDatos()
        {

            CapexAccesoPerfilDtoResponse objAccesoPerfil = new CapexAccesoPerfilDtoResponse();

            //  var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            var IdSolicitudCapex = 0;
            CodPerfil = SessionFacade.PerfilSesion;
            IdSolicitudCapex = SessionFacade.IdSolicitudCapex;

            SolicitudCapexFlujoEstadoDtoRequest objSolicitudFlujoEstado= new SolicitudCapexFlujoEstadoDtoRequest();

            objSolicitudFlujoEstado.IdSolicitudCapex = IdSolicitudCapex;
            var Estado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ObtenerSolicitudCapexFlujoEstadoId(objSolicitudFlujoEstado));
            

            objAccesoPerfil.TabIndicadores = Generales.EstadoLogico.Verdadero;

            MaestraDtoRequest maestra = new MaestraDtoRequest();
            maestra.IdEstado = Generales.Numeric.Uno;
            maestra.IdRelacion = Generales.TablaMaestra.EstadoProyectos;
            maestra.Valor2 = CodPerfil.ToUpper();

            if(CodPerfil == Generales.TipoPerfil.CapexProducto && Generales.EstadosSolicitudCapex.Producto==Estado.IdEstado)
            {
                objAccesoPerfil.TabIndicadores = Generales.EstadoLogico.Falso;

            }
                                                                                                      
           

        

            return Json(objAccesoPerfil, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistroIdSolicitudCapexSession(int Id)
        {

            SessionFacade.IdSolicitudCapex = Id;
            var resultado = SessionFacade.IdSolicitudCapex;
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerIdSolicitudCapexSession()
        {

            var resultado = SessionFacade.IdSolicitudCapex;
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}