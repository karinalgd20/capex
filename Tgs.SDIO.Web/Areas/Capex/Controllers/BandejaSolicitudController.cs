﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;
using Tgs.SDIO.Util.Constantes;


namespace Tgs.SDIO.Web.Areas.Capex.Controllers
{
    public class BandejaSolicitudController : Controller
    {
        // GET: Solicitud/BandejaSolicitud
        private readonly AgenteServicioCapexSDio agenteServicioCapexSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public BandejaSolicitudController(AgenteServicioCapexSDio agenteServicioCapexSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio,
            AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioCapexSDio = agenteServicioCapexSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }


        //[CustomAuthorize]
        //[NoCache]
        public ActionResult Index()
        {
            PerfilSesion();
            return View();
        }

        [HttpPost]
        public JsonResult ListaSolicitudCapexPaginado(SolicitudCapexDtoRequest request)
        {
           
            var CodPerfil = string.Empty;
            CodPerfil = SessionFacade.PerfilSesion;
            if (CodPerfil == Generales.TipoPerfil.CapexPreventa)
            {
                request.IdPreVenta = SessionFacade.Usuario.UserId;
            }

            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ListaSolicitudCapexPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public void PerfilSesion()
        {
            var perfil = SessionFacade.Usuario.CodigoPerfil;


            for (int i = 0; i < perfil.Length; i++)
            {

                SessionFacade.PerfilSesion =
                  (perfil[i] == Generales.TipoPerfil.CapexPreventa) ?
                  perfil[i] : (perfil[i] == Generales.TipoPerfil.CapexPreventa ?
                  perfil[i] : (perfil[i] == Generales.TipoPerfil.CapexPreventa ? perfil[i] : null));


            }


        }

        public JsonResult DetalleSolicitudCapex(SolicitudCapexDtoRequest request)
        {
            string CodSistema = "SDIO";
            int IdEmpresa = Generales.Numeric.Uno;
            string CodPerfilAdmin = Generales.TipoPerfil.CapexPreventa;
            string CodPerfilJefePreventa = Generales.TipoPerfil.CapexJefePreventa;
            UsuarioDtoRequest usuario_Admin = new UsuarioDtoRequest();
            usuario_Admin.CodigoPerfil = CodPerfilAdmin;
            usuario_Admin.CodigoSistema = CodSistema;
            usuario_Admin.IdEmpresa = IdEmpresa;

         


            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ObtenerSolicitudCapex(request));

            resultado.IdPreVenta = SessionFacade.Usuario.UserId;
            var ListPerfilAdmin = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));
            usuario_Admin.CodigoPerfil = CodPerfilJefePreventa;
            var ListPerfilAdmin_2 = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));
            ListPerfilAdmin = ListPerfilAdmin.Where<UsuarioDtoResponse>(p => p.IdUsuario == resultado.IdPreVenta).ToList<UsuarioDtoResponse>();
            resultado.NombrePreventa = ListPerfilAdmin.Count > Generales.Numeric.Cero ? ListPerfilAdmin[0].NombresCompletos : "";
             resultado.NombreJefePreventa = ListPerfilAdmin_2.Count > Generales.Numeric.Cero ? ListPerfilAdmin_2[0].NombresCompletos : "";

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }



    }
}