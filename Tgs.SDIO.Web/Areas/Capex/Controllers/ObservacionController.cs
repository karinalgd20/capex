﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Capex.Controllers
{
    public class ObservacionController : Controller
    {
        private readonly AgenteServicioCapexSDio agenteServicioCapexSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public ObservacionController(AgenteServicioCapexSDio AgenteServicioCapexSDio,
                                     AgenteServicioSeguridadSDio AgenteServicioSeguridadSDio)
        {
            agenteServicioCapexSDio = AgenteServicioCapexSDio;
            agenteServicioSeguridadSDio = AgenteServicioSeguridadSDio;
        }
        // GET: Capex/Observacion
        public ActionResult Index(ObservacionDtoRequest observacion)
        {
            observacion.IdResponsable = SessionFacade.Usuario.UserId;
            ViewBag.ObjSolicitudCapex = SessionFacade.IdSolicitudCapex;
            ViewBag.Observacion = observacion;
            return View();
        }

        [HttpPost]
        public JsonResult RegistrarObservacion(ObservacionDtoRequest observacionDtoRequest)
        {
            observacionDtoRequest.IdEstado = Estados.Activo;
            observacionDtoRequest.FechaCreacion = DateTime.Now;
            observacionDtoRequest.IdResponsable = SessionFacade.Usuario.UserId;
            observacionDtoRequest.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.RegistrarObservacion(observacionDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarObservacion(ObservacionDtoRequest observacionDtoRequest)
        {
            observacionDtoRequest.IdEstado = Estados.Activo;
            observacionDtoRequest.FechaEdicion = DateTime.Now;
            observacionDtoRequest.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.RegistrarObservacion(observacionDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerObservacion(ObservacionDtoRequest observacionDtoRequest)
        {
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ObtenerObservacion(observacionDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest solicitud)
        {
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ObtenerSolicitudCapexFlujoEstado(solicitud));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerUsuario(int IdUsuario)
        {
            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuarioPorId(IdUsuario));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarSeccionObservacion(SeccionDtoRequest seccion)
        {
            var resultado = agenteServicioCapexSDio.InvocarFuncionAsync(o => o.ActualizarSeccionObservacion(seccion));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}