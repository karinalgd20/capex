﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.PlantaExterna.Controllers
{
    public class RegistrarTipoCambioSisegoController : BaseController
    {
        private readonly AgenteServicioPlantaExternaSDio agenteServicioPlantaExternaSDio;

        public RegistrarTipoCambioSisegoController(AgenteServicioPlantaExternaSDio agenteServicioPlantaExterna)
        {
            this.agenteServicioPlantaExternaSDio = agenteServicioPlantaExterna;
        }

        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioSisegoDtoRequest)
        {
            tipoCambioSisegoDtoRequest.IdUsuarioCreacion = User.UserId;
            tipoCambioSisegoDtoRequest.FechaCreacion = DateTime.Now; 

            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.RegistrarTipoCambioSisego(tipoCambioSisegoDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioSisegoDtoRequest)
        {
            tipoCambioSisegoDtoRequest.IdUsuarioEdicion = User.UserId;
            tipoCambioSisegoDtoRequest.FechaEdicion = DateTime.Now;

            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.ActualizarTipoCambioSisego(tipoCambioSisegoDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ObtenerTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioSisegoDtoRequest)
        {
            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.ObtenerTipoCambioSisego(tipoCambioSisegoDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}