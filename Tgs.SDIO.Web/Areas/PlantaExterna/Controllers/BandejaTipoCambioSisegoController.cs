﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.PlantaExterna.Controllers
{
    public class BandejaTipoCambioSisegoController : BaseController
    {
        private readonly AgenteServicioPlantaExternaSDio agenteServicioPlantaExternaSDio;

        public BandejaTipoCambioSisegoController(AgenteServicioPlantaExternaSDio agenteServicioPlantaExterna)
        {
            this.agenteServicioPlantaExternaSDio = agenteServicioPlantaExterna;
        }

        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioSisegoDtoRequest)
        {
            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.ListarTipoCambioSisego(tipoCambioSisegoDtoRequest));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}