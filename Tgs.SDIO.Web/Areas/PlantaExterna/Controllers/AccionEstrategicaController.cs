﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.PlantaExterna.Controllers
{
    public class AccionEstrategicaController : BaseController
    {
        private readonly AgenteServicioPlantaExternaSDio agenteServicioPlantaExternaSDio;

        public AccionEstrategicaController(AgenteServicioPlantaExternaSDio agenteServicioPlantaExterna)
        {
            this.agenteServicioPlantaExternaSDio = agenteServicioPlantaExterna;
        }
        
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarAccionEstrategica(AccionEstrategicaDtoRequest accionDtoRequest)
        { 
            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.ListarAccionEstrategica(accionDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}