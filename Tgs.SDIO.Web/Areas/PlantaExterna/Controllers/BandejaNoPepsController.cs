﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.PlantaExterna.Controllers
{
    public class BandejaNoPepsController : BaseController
    {
        private readonly AgenteServicioPlantaExternaSDio agenteServicioPlantaExternaSDio = null;
        // GET: CartaFianza/RegistroCartaFianza

        public BandejaNoPepsController(AgenteServicioPlantaExternaSDio agenteServicioPlantaExternaSDio)
        {
            this.agenteServicioPlantaExternaSDio = agenteServicioPlantaExternaSDio;
        }
        // GET: NoPeps/NoPeps
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]

        public JsonResult ListarNoPeps(NoPepsDtoRequest noPeps)
        {
            // cartaFianza.IdCartaFianza = 1;
            //SessionFacade.Usuario.EmailUser;
           // NoPeps.Peps = "55";
            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.ListarNoPeps(noPeps));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public JsonResult ObtenerNoPeps(NoPepsDtoRequest noPeps)
        {
            // cartaFianza.IdCartaFianza = 1;
            //SessionFacade.Usuario.EmailUser;
            
            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.ObtenerNoPeps(noPeps));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public JsonResult ActualizarNoPeps(NoPepsDtoRequest NoPeps)
        {
            NoPeps.IdUsuarioEdicion = User.UserId;
            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.ActualizarNoPeps(NoPeps));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public JsonResult RegistrarNoPeps(NoPepsDtoRequest NoPeps)
        {
            NoPeps.IdUsuarioCreacion = User.UserId;
            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.RegistrarNoPeps(NoPeps));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public JsonResult EliminarNoPeps(NoPepsDtoRequest NoPeps)
        {
            
            NoPeps.IdUsuarioEdicion = User.UserId;
            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.EliminarNoPeps(NoPeps));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}