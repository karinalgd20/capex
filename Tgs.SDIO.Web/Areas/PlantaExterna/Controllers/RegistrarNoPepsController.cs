﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.PlantaExterna.Controllers
{
    public class RegistrarNoPepsController : BaseController
    {
        private readonly AgenteServicioPlantaExternaSDio agenteServicioPlantaExternaSDio = null;
       
        public RegistrarNoPepsController(AgenteServicioPlantaExternaSDio agenteServicioPlantaExternaSDio)
        {
            this.agenteServicioPlantaExternaSDio = agenteServicioPlantaExternaSDio;
        }
     
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpPost]

        public JsonResult ObtenerNoPeps(NoPepsDtoRequest noPeps)
        {
            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.ObtenerNoPeps(noPeps));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public JsonResult ActualizarNoPeps(NoPepsDtoRequest NoPeps)
        {
            NoPeps.IdUsuarioEdicion = User.UserId;
            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.ActualizarNoPeps(NoPeps));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public JsonResult RegistrarNoPeps(NoPepsDtoRequest NoPeps)
        {
            NoPeps.IdUsuarioCreacion = User.UserId;
            var resultado = agenteServicioPlantaExternaSDio.InvocarFuncionAsync(o => o.RegistrarNoPeps(NoPeps));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}