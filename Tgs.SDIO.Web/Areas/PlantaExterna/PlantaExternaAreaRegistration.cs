﻿using System.Web.Mvc;

namespace Tgs.SDIO.Web.Areas.PlantaExterna
{
    public class PlantaExternaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PlantaExterna";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PlantaExterna_default",
                "PlantaExterna/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}