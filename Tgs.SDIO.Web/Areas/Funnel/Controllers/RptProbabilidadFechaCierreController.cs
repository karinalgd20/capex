﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.Util.Web.Funnel;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class RptProbabilidadFechaCierreController : Controller
    {
        // GET: Funnel/RptProbabilidadFechaCierre
        public ActionResult Index()
        {
            ViewBag.Titulo = Titulos.Titulo_BurbujaOportunidades;
            return View();
        }
    }
}

