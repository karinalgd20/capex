﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.Util.Web.Funnel;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class RptCargabilidadLineaNegocioController : BaseController
    {
        // GET: Funnel/RptCargabilidadLineaNegocio
        public ActionResult Index()
        {
            ViewBag.Titulos_Cargabilidad = Titulos.Titulos_Cargabilidad;
            return View();
        }
    }
}