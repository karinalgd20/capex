﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class DashboardFunnelConsolidadoController : BaseController
    {
        // GET: Funnel/DashboardFunnelConsolidado
        public ActionResult Index()
        {
            ViewBag.CodigosPerfil = User.CodigoPerfil;
            ViewBag.IdUsuario = User.UserId;
            return View("~/Views/Principal/DashboardFunnelConsolidado.cshtml");
        }
    }
}