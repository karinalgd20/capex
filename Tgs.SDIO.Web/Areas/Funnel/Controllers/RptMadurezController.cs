﻿using Newtonsoft.Json;
using System.IO;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Web.Funnel;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class RptMadurezController : Controller
    {
        private readonly AgenteServicioFunnelSDio agenteServicioFunnelSDio = null;

        // GET: Funnel/RptMadurez

        public RptMadurezController(AgenteServicioFunnelSDio agenteServicioFunnelSDio)
        {
            this.agenteServicioFunnelSDio = agenteServicioFunnelSDio;
        }


        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public FileResult ExportarOfertasAnio(string item)
        {
            var ofertaDto = JsonConvert.DeserializeObject<UI.ProxyServicio.ServiceReferenceServicioFunnelSDio.IndicadorMadurezDtoRequest>(item);

            var ofertasAnios = agenteServicioFunnelSDio.InvocarFuncionAsync(x => x.ListarIndicadorOfertasAnio(ofertaDto));

            return File(ofertasAnios.ToArray(), Cadenas.FormatoExportacionExcel, Cadenas.FormatoExcelMadurez);
            
        }

    }
}

