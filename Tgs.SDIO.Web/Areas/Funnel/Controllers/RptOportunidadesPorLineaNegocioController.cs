﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioFunnelSDio;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class RptOportunidadesPorLineaNegocioController : BaseController
    {
        private readonly AgenteServicioFunnelSDio agenteServicioFunnelSDio = null;

        public RptOportunidadesPorLineaNegocioController(AgenteServicioFunnelSDio agenteServicioFunnelSDio)
        {
            this.agenteServicioFunnelSDio = agenteServicioFunnelSDio;
        }

        // GET: Funnel/RptOportunidadesPorLineaNegocio
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarLineasNegocio(IndicadorLineasNegocioDtoRequest filtro)
        {
            var oportunidades = agenteServicioFunnelSDio.InvocarFuncionAsync(o => o.ListarLineasNegocio(filtro));
            var json = Json(oportunidades);
            json.MaxJsonLength = int.MaxValue;
            return json;
        }

    }
}

