﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioFunnelSDio;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class RptPreventaOportunidadesGerenteController : BaseController
    {
        private readonly AgenteServicioFunnelSDio agenteServicioFunnelSDio = null;
        public RptPreventaOportunidadesGerenteController(AgenteServicioFunnelSDio agenteServicioFunnelSDio)
        {
            this.agenteServicioFunnelSDio = agenteServicioFunnelSDio;
        }

        // GET: Funnel/RptPreventaOportunidadesGerente
        public ActionResult Index()
        {
            ViewBag.IdUsuario = User.UserId;
            return View();
        }

        [HttpPost]
        public JsonResult ListarOportunidades(IndicadorDashboardPreventaConsolidadoDtoRequest request)
        {
            var resultado = agenteServicioFunnelSDio.InvocarFuncionAsync(o => o.ListarSeguimientoOportunidadesPreventaGerente(request));
            var json = Json(resultado);
            json.MaxJsonLength = int.MaxValue;
            return json;
        }
    }
}

