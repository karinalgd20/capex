﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class DashboardController : Controller
    {
        private readonly AgenteServicioFunnelSDio agenteServicioFunnelSDio = null;

        public DashboardController(AgenteServicioFunnelSDio agenteServicioFunnelSDio)
        {
            this.agenteServicioFunnelSDio = agenteServicioFunnelSDio;
        }

        // GET: Funnel/Dashoard
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public JsonResult MostrarDashboard(UI.ProxyServicio.ServiceReferenceServicioFunnelSDio.DashoardDtoRequest request)
        {
            var resultado = agenteServicioFunnelSDio.InvocarFuncionAsync(x => x.MostrarDashboard(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);

        }
    }
}

