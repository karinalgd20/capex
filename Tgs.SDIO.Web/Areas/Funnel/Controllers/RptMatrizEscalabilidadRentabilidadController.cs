﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.Util.Web.Funnel;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    public class RptMatrizEscalabilidadRentabilidadController : BaseController
    {
        // GET: Funnel/RptMatrizEscalabilidadRentabilidad
        public ActionResult Index()
        {
            ViewBag.Titulo = Titulos.Titulo_MatrizEscalabilidad;
            return View();
        }
    }
}

