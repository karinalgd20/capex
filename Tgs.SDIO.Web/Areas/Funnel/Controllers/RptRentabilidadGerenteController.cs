﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioFunnelSDio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class RptRentabilidadGerenteController : BaseController
    {
        private readonly AgenteServicioFunnelSDio agenteServicioFunnelSDio = null;
        // GET: Funnel/RptRentabilidadGerente
        public ActionResult Index()
        {
            ViewBag.IdUsuario = User.UserId;
            return View();
        }

        public RptRentabilidadGerenteController(AgenteServicioFunnelSDio agenteServicioFunnelSDio)
        {
            this.agenteServicioFunnelSDio = agenteServicioFunnelSDio;
        }

        [HttpPost]
        public JsonResult ListarAnalistasFinancieros(IndicadorRentabilidadDtoRequest request)
        {
            var resultado = agenteServicioFunnelSDio.InvocarFuncionAsync(x => x.ListarRentabilidadAnalistasFinancieros(request));
            var json = Json(resultado);
            json.MaxJsonLength = int.MaxValue;
            return json;

        }

    }
}