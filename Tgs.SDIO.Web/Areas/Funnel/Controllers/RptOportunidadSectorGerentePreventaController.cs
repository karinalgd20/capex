﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioFunnelSDio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class RptOportunidadSectorGerentePreventaController : BaseController
    {
        private readonly AgenteServicioFunnelSDio agenteServicioFunnelSDio = null;
        public RptOportunidadSectorGerentePreventaController(AgenteServicioFunnelSDio agenteServicioFunnelSDio)
        {
            this.agenteServicioFunnelSDio = agenteServicioFunnelSDio;
        }

        // GET: Funnel/RptOportunidadSectorGerentePreventa
        public ActionResult Index()
        {
            ViewBag.IdUsuario = User.UserId;
            return View();
        }

        [HttpPost]
        public JsonResult ListarSeguimientoOportunidadesSectorOportunidad(IndicadorDashboardSectorDtoRequest request)
        {
            var oportunidades = agenteServicioFunnelSDio.InvocarFuncionAsync(o => o.ListarSeguimientoOportunidadesSectorOportunidad(request));
            var json = Json(oportunidades);
            json.MaxJsonLength = int.MaxValue;
            return json;
        }

    }
}

