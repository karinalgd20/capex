﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [RepErrorCatch]
    public class ReportesController : Controller
    {
        // GET: Funnel/Reportes

        [CustomAuthorize]
        public ActionResult RptPrincipal()
        {
            return View();
        }


        [AllowAnonymous]
        public ActionResult RptProbabilidadPorMes(string idOportunidad)
        {
            ViewBag.IdOportunidad = idOportunidad;
            return View();
        }

        [CustomAuthorize]
        public ActionResult RptLineaNegocioPeriodo()
        {
            return View();
        }

        [CustomAuthorize]
        public ActionResult RptIngresoLineaNegocioPeriodo()
        {
            return View();
        }

        [CustomAuthorize]
        public ActionResult RptIngresoSectorPeriodo()
        {
            return View();
        }

        [CustomAuthorize]
        public ActionResult RptOfertasPorEstadoPeriodo()
        {
            return View();
        }

        [CustomAuthorize]
        public ActionResult RptEvolutivoOportunidad()
        {
            return View();
        }

        [CustomAuthorize]
        public ActionResult RptOfertasCliente()
        {
            return View();
        }
    }
}

