﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class RptCostosOportunidadController : Controller
    {
        // GET: Funnel/RptCostosOportunidad
        public ActionResult Index(string idOportunidad,string numeroCaso)
        {
            ViewBag.IdOportunidad = idOportunidad;
            ViewBag.NumeroCaso = numeroCaso;
            return View();
        }
    }
}

