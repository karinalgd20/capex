﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    public class RptProbabilidadPorMesController : Controller
    {
        // GET: Funnel/RptProbabilidadPorMes
        public ActionResult Index(string idOportunidad)
        {
            ViewBag.IdOportunidad = idOportunidad;
            return View();
        }
    }
}