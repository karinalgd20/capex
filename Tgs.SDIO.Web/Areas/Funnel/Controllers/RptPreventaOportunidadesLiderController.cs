﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioFunnelSDio;


namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class RptPreventaOportunidadesLiderController : BaseController
    {
        private readonly AgenteServicioFunnelSDio agenteServicioFunnelSDio = null;

        public RptPreventaOportunidadesLiderController(AgenteServicioFunnelSDio agenteServicioFunnelSDio)
        {
            this.agenteServicioFunnelSDio = agenteServicioFunnelSDio;
        }
        // GET: Funnel/RptPreventaOportunidadesLider

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.IdUsuario = User.UserId;
            return View();
        }

        [HttpPost]
        public JsonResult ListarOportunidades(IndicadorDashboardPreventaConsolidadoDtoRequest request)
        {
            var resultado = agenteServicioFunnelSDio.InvocarFuncionAsync(o => o.ListarSeguimientoOportunidadesPreventaLider(request));
            var json = Json(resultado);
            json.MaxJsonLength = int.MaxValue;
            return json;
        }

    }
}

