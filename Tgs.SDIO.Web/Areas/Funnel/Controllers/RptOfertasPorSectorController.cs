﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class RptOfertasPorSectorController : Controller
    {
        // GET: Funnel/RptOfertasPorSector
        public ActionResult Index()
        {
            return View();
        }
    }
}