﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class RptEvolutivoOportunidadController : Controller
    {
        // GET: Funnel/RptEvolutivoOportunidad
        public ActionResult Index(string idOportunidad)
        {
            ViewBag.IdOportunidad = idOportunidad;
            return View();
        }
    }
}