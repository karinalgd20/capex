﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class RptLineaNegocioPeriodoController : Controller
    {
        // GET: Funnel/RptLineaNegocioPeriodo
        public ActionResult Index()
        {
            return View();
        }
    }
}