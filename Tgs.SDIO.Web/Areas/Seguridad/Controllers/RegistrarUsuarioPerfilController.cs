﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Seguridad.Controllers
{
    public class RegistrarUsuarioPerfilController : BaseController
    {
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio;

        public RegistrarUsuarioPerfilController(AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarUsuarioPerfil(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest)
        { 
            usuarioPerfilDtoRequest.IdUsuarioCreacion = User.UserId;
            usuarioPerfilDtoRequest.IdUsuarioEdicion = User.UserId;
            usuarioPerfilDtoRequest.FechaCreacion = DateTime.Now;
            usuarioPerfilDtoRequest.FechaEdicion = DateTime.Now;
            usuarioPerfilDtoRequest.IdEstado = Estados.Activo;
            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.RegistrarUsuarioPerfil(usuarioPerfilDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarPerfilesPorSistema()
        {
            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarPerfilesPorSistema());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarPerfilesAsignados(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest)
        {
            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarPerfilesAsignados(usuarioPerfilDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarEstadoUsuarioPerfil(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest)
        { 
            usuarioPerfilDtoRequest.IdUsuarioCreacion = User.UserId;
            usuarioPerfilDtoRequest.IdUsuarioEdicion = User.UserId;
            usuarioPerfilDtoRequest.FechaCreacion = DateTime.Now;
            usuarioPerfilDtoRequest.FechaEdicion = DateTime.Now;

            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ActualizarEstadoUsuarioPerfil(usuarioPerfilDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}