﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Seguridad.Controllers
{
    public class OlvidoClaveController : Controller
    {

        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio;
        public OlvidoClaveController(AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EnviarClave(UsuarioDtoRequest usuario)
        {
            var usuarioLoginRais = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.EnvioClaveUsuario(usuario));

            return Json(new { Respuesta = new { Mensaje = usuarioLoginRais } });
        }
    }
}