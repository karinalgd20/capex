﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Seguridad.Controllers
{
    public class RegistrarUsuarioController : BaseController
    {
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio;

        public RegistrarUsuarioController(AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();  
        } 

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            usuarioDtoRequest.IdEstado = Estados.Activo;
            usuarioDtoRequest.IdUsuarioCreacion = User.UserId;
            usuarioDtoRequest.TipoUsuario = 1;
            usuarioDtoRequest.TipoRecurso = TipoRecurso.Personal;
            usuarioDtoRequest.FechaCreacion = DateTime.Now;
            usuarioDtoRequest.FechaEdicion = DateTime.Now;            
            usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest = new UsuarioSistemaEmpresaDtoRequest
            {
                IdTurno = 1,
                FechaInicio = DateTime.Now,
                FechaFin = DateTime.Now.AddDays(900),
                EstadoRegistro = Estados.Activo,
                IdUsuarioRegistra = User.UserId,
                FechaRegistro = DateTime.Now
            };

            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.RegistrarUsuario(usuarioDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        { 
            usuarioDtoRequest.IdUsuarioEdicion = User.UserId;
            usuarioDtoRequest.IdEstado = Estados.Activo;
            usuarioDtoRequest.FechaEdicion = DateTime.Now;
            
            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ActualizarUsuario(usuarioDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ObtenerUsuarioPorId(int id)
        {
            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuarioPorId(id));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

       
    }
}