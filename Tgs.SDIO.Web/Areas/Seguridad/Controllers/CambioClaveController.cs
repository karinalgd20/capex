﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Seguridad.Controllers
{
    public class CambioClaveController : Controller
    {  
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio;
        public CambioClaveController(AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CambiarClave(UsuarioDtoRequest usuario)
        {
            var usuarioLoginRais = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.CambiarPassword(usuario));

            return Json(new { Respuesta = new { Error = usuarioLoginRais.CodigoError, Mensaje = usuarioLoginRais.MensajeError } });
        }

    }
}