﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Seguridad.Controllers
{
    public class BandejaUsuarioController : BaseController
    {
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio;

        public BandejaUsuarioController(AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarUsuariosPaginado(UsuarioDtoRequest usuarioDtoRequest)
        {
            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarUsuariosPaginado(usuarioDtoRequest));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarUsuariosFiltro(UsuarioDtoRequest usuarioDtoRequest)
        {
            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarUsuariosFiltro(usuarioDtoRequest));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarEstadoUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            usuarioDtoRequest.IdUsuarioEdicion = User.UserId;
            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ActualizarEstadoUsuario(usuarioDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}