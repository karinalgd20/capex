﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class BandejaServicioController : Controller
    {
        // GET: Comun/BandejaServicio
   
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public BandejaServicioController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;

        }
    
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListarPestanaGrupo(PestanaGrupoDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarPestanaGrupo(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}