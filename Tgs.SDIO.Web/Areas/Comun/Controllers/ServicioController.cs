﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class ServicioController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ServicioController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/Servicio
        public ActionResult Index()
        {
            return View();
        }

        // Servicio
        public JsonResult ListarServicios(ServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarServicios(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActualizarServicio(ServicioDtoRequest request)
        {
            DateTime fecha = DateTime.Now;
            request.FechaEdicion = fecha;
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerServicio(ServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InhabilitarServicio(ServicioDtoRequest request)
        {
            DateTime fecha = DateTime.Now;

            request.FechaEdicion = fecha;
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.InhabilitarServicio(request));
         
            return Json(resultado, JsonRequestBehavior.AllowGet);


        }
        public JsonResult RegistrarServicio(ServicioDtoRequest request)
        {
            DateTime fecha = DateTime.Now;


            request.FechaCreacion = fecha;
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;

            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);

        }
        public JsonResult ListaServicioPaginado(ServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListaServicioPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


    }
}