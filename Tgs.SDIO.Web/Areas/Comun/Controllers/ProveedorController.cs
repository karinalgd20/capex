﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class ProveedorController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ProveedorController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        /*---- Vista Bandeja Mantenimiento Proveedores ----*/

        public ActionResult Index()
        {
            return View();
        }

        #region --- Transaccionales ---
        [HttpPost]
        public JsonResult RegistrarProveedor(ProveedorDtoRequest request)
        {
            DateTime fecha = DateTime.Now;
            request.FechaCreacion = fecha;
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarProveedor(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ActualizarProveedor(ProveedorDtoRequest request)
        {
            DateTime fecha = DateTime.Now;
            request.FechaEdicion = fecha;
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarProveedor(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult InactivarProveedor(ProveedorDtoRequest request)
        {
            DateTime fecha = DateTime.Now;
            request.FechaEdicion = fecha;
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.InactivarProveedor(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerProveedorById(ProveedorDtoRequest request)
        {
            request.IdEstado = Generales.Estados.Activo;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerProveedor(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region -- No Transaccionales --
        [HttpPost]
        public JsonResult ListarProveedor(ProveedorDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarProveedor(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ListarProveedorPaginado(ProveedorDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarProveedorPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}