﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class CasoNegocioServicioController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public CasoNegocioServicioController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/CasoNegocioServicio
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoNegocio)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarCasoNegocioServicio(casoNegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListPaginadoCasoNegocioServicio(CasoNegocioServicioDtoRequest casoNegocio)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListPaginadoCasoNegocioServicio(casoNegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        
    }
}