﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class DepreciacionController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public DepreciacionController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/Depreciacion
        public ActionResult Index()
        {
            return View();
        }
        //Depreciacion
        public JsonResult ListarDepreciacion(DepreciacionDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarDepreciacion(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


    }
}