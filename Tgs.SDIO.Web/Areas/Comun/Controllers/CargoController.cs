﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class CargoController : BaseController
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio;
        public CargoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        [CustomAuthorize]
        public JsonResult ListarCargos(CargoDtoRequest cargoRequest)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarCargos(cargoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        public JsonResult ListarComboCargo()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboCargo());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}