﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class TipoSolucionController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public TipoSolucionController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/SubServicio
        public ActionResult Index()
        {
            return View();
        }
        // SubServicio
        public JsonResult ListarTipoSolucionPorProyecto(DetalleSolucionDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarTipoSolucionByProyecto(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        
    }
}