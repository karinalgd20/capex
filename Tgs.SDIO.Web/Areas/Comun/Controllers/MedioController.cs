﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class MedioController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public MedioController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/Medio
        public ActionResult Index()
        {
            return View();
        }

        // Medio
        public JsonResult ListarMedio(MedioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarMedio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActualizarMedio(MedioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarMedio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerMedio(MedioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerMedio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistrarMedio(MedioDtoRequest request)
        {
            DateTime fecha = DateTime.Now;


            request.FechaCreacion = fecha;
            request.IdUsuarioCreacion = SessionFacade.Usuario.IdUsusarioSistema;


            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarMedio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


    }
}