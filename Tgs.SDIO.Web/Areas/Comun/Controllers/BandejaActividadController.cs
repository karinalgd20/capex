﻿using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class BandejaActividadController : Controller
    {

        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public BandejaActividadController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        // GET: Comun/BandejaActividad
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarActividad(ActividadDtoRequest areaRequest)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListatActividadPaginado(areaRequest));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult EliminarActividad(ActividadDtoRequest areaRequest)
        {
            areaRequest.IdUsuario = SessionFacade.Usuario.UserId;

            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.EliminarActividad(areaRequest));

            return Json(resultado, JsonRequestBehavior.AllowGet);

        }
    }
}