﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class UbigeoController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public UbigeoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        [HttpPost]
        public JsonResult ListarComboUbigeoDepartamento()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboUbigeoDepartamento());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarComboUbigeoProvincia(UbigeoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboUbigeoProvincia(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarComboUbigeoDistrito(UbigeoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboUbigeoDistrito(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ObtenerPorCodigoDistrito(UbigeoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerPorCodigoDistrito(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        

    }
}