﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.Web.Utilitarios;
namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class RolRecursoController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public RolRecursoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        [HttpPost]
        public JsonResult ListarComboRolRecurso()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboRolRecurso());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerRolRecursoPorId(RolRecursoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerRolRecursoPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarRolRecurso(RolRecursoDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarRolRecurso(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarRolRecurso(RolRecursoDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarRolRecurso(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}