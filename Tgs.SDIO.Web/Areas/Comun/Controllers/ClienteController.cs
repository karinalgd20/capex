﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Web.Comun.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class ClienteController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ClienteController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/Cliente
        public ActionResult Index()
        {
            return View();
        }

        //Cliente
        public JsonResult ActualizarCliente(ClienteDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var result = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarCliente(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RegistrarCliente(ClienteDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.IdEstado = General.Estados.Activo;
            var result = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarCliente(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListarClientePorDescripcion(ClienteDtoRequest request)
        {
            request.IdEstado = General.Estados.Activo;
            var result = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarClientePorDescripcion(request));
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        public JsonResult ListarClienteCartaFianza(ClienteDtoRequest request)
        {
            request.IdEstado = General.Estados.Activo;
            var result = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarClienteCartaFianza(request));
            return Json(result, JsonRequestBehavior.AllowGet);

        }

    
        public JsonResult ListarCliente(ClienteDtoRequest request)
        {
            var result = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarCliente(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerCliente(ClienteDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerCliente(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObtenerClientePorCodigo(ClienteDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerClientePorCodigo(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ListarClientePaginado(ClienteDtoRequest cliente)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarClientePaginado(cliente));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarDireccionComercial(DireccionComercialDtoRequest direccionComercial)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarDireccionComercial(direccionComercial));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarComboSector()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboSector());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}