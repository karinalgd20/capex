﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class SegmentoNegocioController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public SegmentoNegocioController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        [HttpPost]
        public JsonResult ListarComboSegmentoNegocio()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboSegmentoNegocio());

            return new JsonResult { Data = resultado, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public JsonResult ListarComboSegmentoNegocioSimple()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboSegmentoNegocioSimple());

            return new JsonResult { Data = resultado, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}