﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class RegistrarProveedorController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;

        public RegistrarProveedorController(AgenteServicioComunSDio AgenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = AgenteServicioComunSDio;
        }
        /// <summary>
        /// Los Metodos se encuentran en el archivo Comun/ProveedorController.cs
        /// </summary>
        /// <returns></returns>

        /*---- Vista Registrar Nuevo Proveedor ----*/
        [HttpGet]
        public ActionResult Index(string IdProveedor)
        {
            var Id = Convert.ToInt16(IdProveedor);
            if (Id > 0)
            {
                ProveedorDtoRequest objProveedor = new ProveedorDtoRequest();

                objProveedor.IdProveedor = Id;
                objProveedor.IdEstado = Generales.Estados.Activo;

                var buscarProveedor = agenteServicioComunSDio.InvocarFuncionAsync(p => p.ObtenerProveedor(objProveedor));

                ViewBag.IdProveedor = Id;

            }

            return View();
        }

        [HttpPost]
        public JsonResult ListarPaisesComboBoxProveedor(ProveedorDtoRequest request)
        {
            //var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarPaisesComboBoxProveedor(request));
            //return Json(resultado, JsonRequestBehavior.AllowGet);
            return null;
        }

    }
}