﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class SubServicioController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public SubServicioController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/SubServicio
        public ActionResult Index()
        {
            return View();
        }
        // SubServicio
        public JsonResult ListarSubServicios(SubServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarSubServicios(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActualizarSubServicio(SubServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarSubServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult InhabilitarSubServicio(SubServicioDtoRequest request)
        {
            
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.InhabilitarSubServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerSubServicio(SubServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerSubServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistrarSubServicio(SubServicioDtoRequest request)
        {

            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;


            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarSubServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListaSubServicioPaginado(SubServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListaSubServicioPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        
       public JsonResult ListarSubServiciosPorIdGrupo(SubServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarSubServiciosPorIdGrupo(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}