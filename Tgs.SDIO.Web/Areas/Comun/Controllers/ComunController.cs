﻿using System;
using System.Linq;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class ComunController : Controller
    {

        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ComunController(
            AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        // GET: Comun/Comun
        public ActionResult Index()
        {
            return View();
        }
       

        public JsonResult ListaSubServicioPaginado(SubServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListaSubServicioPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        // Servicio
        public JsonResult ListarServicios(ServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarServicios(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActualizarServicio(ServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerServicio(ServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistrarServicio(ServicioDtoRequest request)
        {
            DateTime fecha = DateTime.Now;

            request.FechaCreacion = fecha;
            request.IdUsuarioCreacion = SessionFacade.Usuario.IdUsusarioSistema;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListaServicioPaginado(ServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListaServicioPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        //Sector
        public JsonResult ObtenerSector(SectorDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerSector(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        //Cliente
        public JsonResult ListarClientesPorDescripcion(ClienteDtoRequest request)
        {
            var result = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarCliente(request));
            var resultado = result.Where(m => m.IdEstado == request.IdEstado && m.Descripcion.ToLower().Contains(request.Descripcion.ToLower())).ToList();
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListarClientePorDescripcion(ClienteDtoRequest request)
        {
            var result = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarCliente(request));
            var resultado = result.Where(m => m.IdEstado == request.IdEstado && m.Descripcion == request.Descripcion).Single();
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerCliente(ClienteDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerCliente(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        //DireccionComercial
        public JsonResult ObtenerDireccionComercial(DireccionComercialDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerDireccionComercial(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        //Maestra
        public JsonResult ListarMaestraPorIdRelacion(MaestraDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarMaestraPorIdRelacion(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        //Depreciacion
        public JsonResult ListarDepreciacion(DepreciacionDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarDepreciacion(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        //Lista de Probabilidades
        public JsonResult ListarProbabilidades()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarProbabilidades());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}