﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class BandejaRolRecursoController : Controller
    {

        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;

        public BandejaRolRecursoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        // GET: Trazabilidad/RolRecursoBandeja
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarRolRecurso(RolRecursoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListadoRolRecursosPaginado(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult EliminarRolRecurso(RolRecursoDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.EliminarRolRecurso(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);


        }
    }
}