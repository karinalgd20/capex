﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class CostoController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public CostoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarCostos(CostoDtoRequest costo)
        {
            costo.IdEstado = Estados.Activo;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboCosto(costo));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListarCosto(CostoDtoRequest costo)
        {
            costo.IdEstado = Estados.Activo;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarCosto(costo));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

         public JsonResult ObtenerCostoPorCodigo(CostoDtoRequest costo)
        {
            costo.IdEstado = Estados.Activo;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerCostoPorCodigo(costo));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ListarCostoPorMedioServicioGrupo(CostoDtoRequest costo)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarCostoPorMedioServicioGrupo(costo));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ObtenerCosto(CostoDtoRequest costo)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerCosto(costo));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}