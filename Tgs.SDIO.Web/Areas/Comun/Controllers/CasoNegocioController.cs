﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Web.Comun.Constantes.General;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class CasoNegocioController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        DateTime fecha = DateTime.Now;
        public CasoNegocioController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/CasoNegocio
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListarCasoNegocioBandeja(CasoNegocioDtoRequest casoNegocio)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarCasoNegocio(casoNegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EliminarCasoNegocio(CasoNegocioDtoRequest casoNegocio)
        {
            casoNegocio.IdEstado = Estados.Inactivo;
            casoNegocio.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            casoNegocio.FechaEdicion = fecha;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarCasoNegocio(casoNegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListarCasoNegocioDefecto(CasoNegocioDtoRequest casoNegocio)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarCasoNegocioDefecto(casoNegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}