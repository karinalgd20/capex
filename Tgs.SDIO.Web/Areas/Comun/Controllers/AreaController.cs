﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class AreaController : BaseController
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio;
        public AreaController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        [CustomAuthorize]
        public JsonResult ListarAreas(AreaDtoRequest areaRequest)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarAreas(areaRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        public JsonResult ListarComboArea()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboArea());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}