﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class ServicioSubServicioController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ServicioSubServicioController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/ServicioSubServicio
        public ActionResult Index()
        {
            return View();
        }
        // ServicioSubServicio
        public JsonResult ListarServicioSubServicio(ServicioSubServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarServicioSubServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActualizarServicioSubServicio(ServicioSubServicioDtoRequest request)
        {
            DateTime fecha = DateTime.Now;

            request.FechaEdicion = fecha;
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarServicioSubServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult InhabilitarServicioSubServicio(ServicioSubServicioDtoRequest request)

        {
            DateTime fecha = DateTime.Now;

            request.FechaCreacion = fecha;
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.InhabilitarServicioSubServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EliminarServicioSubServicio(ServicioSubServicioDtoRequest request)

        {

            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.EliminarServicioSubServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerServicioSubServicio(ServicioSubServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerServicioSubServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistrarServicioSubServicio(ServicioSubServicioDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarServicioSubServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListaServicioSubServicioPaginado(ServicioSubServicioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListaServicioSubServicioPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListarServicioSubServicioGrupos(ServicioSubServicioDtoRequest request)
        {
            request.IdEstado = Estados.Activo;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarServicioSubServicioGrupos(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}