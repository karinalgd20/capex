﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class FaseController : Controller
    {

        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public FaseController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        [HttpPost]
        public JsonResult ListarComboFases()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboFase());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}