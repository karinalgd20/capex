﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class DireccionComercialController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public DireccionComercialController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/DireccionComercial
        public ActionResult Index()
        {
            return View();
        }

        //DireccionComercial
        public JsonResult ObtenerDireccionComercial(DireccionComercialDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerDireccionComercial(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}