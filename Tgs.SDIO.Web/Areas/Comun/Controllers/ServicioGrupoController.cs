﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Web.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class ServicioGrupoController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ServicioGrupoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        // GET: Comun/ServicioGrupo
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListarServicioGrupo(ServicioGrupoDtoRequest servicioGrupo)
        {
            servicioGrupo.IdEstado = Estados.Activo;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarServicioGrupo(servicioGrupo));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}
