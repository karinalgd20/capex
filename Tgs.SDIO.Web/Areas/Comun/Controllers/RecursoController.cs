﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class RecursoController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public RecursoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        [HttpPost]
        public JsonResult ListaRecursoModalRecursoPaginado(RecursoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListaRecursoModalRecursoPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerRecursoPorId(RecursoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerRecursoPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarRecurso(RecursoDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarRecurso(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarRecurso(RecursoDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarRecurso(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarRecursoPorCargo(RecursoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarRecursoPorCargo(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarRecursoDeUsuario(RecursoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarRecursoDeUsuario(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarRecursoPorCargoPaginado(RecursoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListadoRecursosByCargoPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}