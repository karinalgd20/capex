﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class BandejaConceptoSeguimientoController : Controller
    {

        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;

        public BandejaConceptoSeguimientoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        // GET: Comun/ConceptoSeguimientoBandeja
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {

            return View();
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListadoConceptosSeguimientoPaginado(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult EliminarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.EliminarConceptoSeguimiento(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);


        }
    }
}