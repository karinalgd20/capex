﻿using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Mvc;

using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.Util.Funciones;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class ArchivoController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ArchivoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListarArchivoPaginado(ArchivoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarArchivoPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarArchivo(HttpPostedFileBase[] files, string cargaArchivo)
        {
            byte[] fileArchivo = Funciones.ConvertirDatosABytes(files[0].InputStream);
            ArchivoDtoRequest request = JsonConvert.DeserializeObject<ArchivoDtoRequest>(cargaArchivo);
            request.Nombre = files[0].FileName;
            request.NombreInterno = Funciones.GenerarNombreArchivo(request.Nombre);
            request.IdEstado = 1;
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());

            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarArchivo(request, fileArchivo));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarArchivos(HttpPostedFileBase[] files, string cargaArchivo)
        {
            List<ProcesoResponse> respuesta = new List<ProcesoResponse>();

            foreach (var file in files)
            {
                byte[] fileArchivo = Funciones.ConvertirDatosABytes(file.InputStream);
                ArchivoDtoRequest request = JsonConvert.DeserializeObject<ArchivoDtoRequest>(cargaArchivo);
                request.Nombre = file.FileName;
                request.NombreInterno = Funciones.GenerarNombreArchivo(request.Nombre);
                request.IdEstado = 1;
                request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
                request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());

                var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarArchivo(request, fileArchivo));
                respuesta.Add(resultado);
                respuesta[0].Mensaje = (resultado.TipoRespuesta == 0) ? "Se subieron " + respuesta.Count.ToString() + " archivo(s) correctamente" : resultado.Mensaje;
            }

            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult InactivarArchivo(ArchivoDtoRequest request)
        {
            request.IdEstado = 0;
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Parse(DateTime.Now.ToLongDateString());

            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.InactivarArchivo(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        public FileResult DescargarArchivo(int id)
        {
            var archivo = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerArchivo(new ArchivoDtoRequest { Id = id }));
            return File(archivo.Ruta, "application/force-download", archivo.Nombre);
        }
    }
}