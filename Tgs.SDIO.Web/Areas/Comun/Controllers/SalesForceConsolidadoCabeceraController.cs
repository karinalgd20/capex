﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class SalesForceConsolidadoCabeceraController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public SalesForceConsolidadoCabeceraController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/SalesForceConsolidadoCabecera
        public ActionResult Index()
        {
            return View();
        }
        // SalesForceConsolidadoCabecera
        public JsonResult ListarNumeroSalesForcePorIdOportunidad(SalesForceConsolidadoCabeceraDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarNumeroSalesForcePorIdOportunidad(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
 
    

    }
}