﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class RiesgoProyectoController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public RiesgoProyectoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        #region - Transaccionales -
        [HttpPost]
        public JsonResult RegistrarRiesgoProyecto(RiesgoProyectoDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarRiesgoProyecto(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarRiesgoProyecto(RiesgoProyectoDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarRiesgoProyecto(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EliminarRiesgoProyecto(RiesgoProyectoDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.EliminarRiesgoProyecto(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region - No Transaccionales -
        [HttpPost]
        public JsonResult ListarRiesgoProyectosPaginado(RiesgoProyectoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListadoRiesgoProyectosPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerRiesgoProyectoPorId(RiesgoProyectoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerRiesgoProyectoPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}