﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class RegistrarRecursoJefeController : BaseController
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio; 

        public RegistrarRecursoJefeController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarRecursoJefe(RecursoJefeDtoRequest recursoJefeDtoRequest)
        {
            recursoJefeDtoRequest.IdUsuarioCreacion = User.UserId;
            recursoJefeDtoRequest.FechaCreacion = DateTime.Now;
            recursoJefeDtoRequest.IdEstado = Estados.Activo;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarRecursoJefe(recursoJefeDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarRecursoPorJefePaginado(RecursoJefeDtoRequest recursoJefeDtoRequest)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarRecursoPorJefePaginado(recursoJefeDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarEstadoRecursoJefe(RecursoJefeDtoRequest recursoJefeDtoRequest)
        {
            recursoJefeDtoRequest.IdUsuarioEdicion = User.UserId;
            recursoJefeDtoRequest.FechaEdicion = DateTime.Now;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarEstadoRecursoJefe(recursoJefeDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        } 
    }
}