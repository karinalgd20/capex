﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class SalesForceConsolidadoDetalleController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public SalesForceConsolidadoDetalleController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/SalesForceConsolidadoCabecera
        public ActionResult Index()
        {
            return View();
        }
        // SalesForceConsolidadoCabecera
        public JsonResult ObtenerSalesForceConsolidadoDetalle(SalesForceConsolidadoDetalleDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerSalesForceConsolidadoDetalle(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListaNumerodeCasoPorNumeroSalesForceDetalle(SalesForceConsolidadoDetalleDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListaNumerodeCasoPorNumeroSalesForceDetalle(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        
    }
}