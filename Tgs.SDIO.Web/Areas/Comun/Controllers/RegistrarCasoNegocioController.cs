﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Web.Comun;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Web.Comun.Constantes.General;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class RegistrarCasoNegocioController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        DateTime fecha = DateTime.Now;
        public RegistrarCasoNegocioController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        // GET: Comun/RegistrarCasoNegocio
        public ActionResult Index(CasoNegocioDtoRequest casoNegocio)
        {
            return View();
        }

        [HttpPost]
        public JsonResult RegistrarCasoNegocio(CasoNegocioDtoRequest casoNegocio)
        {
            casoNegocio.IdEstado = Estados.Activo;
            casoNegocio.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            casoNegocio.FechaCreacion = fecha;

            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarCasoNegocio(casoNegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarCasoNegocio(CasoNegocioDtoRequest casoNegocio)
        {
            casoNegocio.IdEstado = Estados.Activo;
            casoNegocio.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            casoNegocio.FechaEdicion = fecha;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarCasoNegocio(casoNegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerCasoNegocio(CasoNegocioDtoRequest casoNegocio)
        {   
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerCasoNegocio(casoNegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoServicio)
        {
            casoServicio.IdEstado = Estados.Activo;
            casoServicio.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            casoServicio.FechaCreacion = fecha;

            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarCasoNegocioServicio(casoServicio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EliminarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoServicio)
        {
            casoServicio.IdEstado = Estados.Inactivo;
            casoServicio.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            casoServicio.FechaEdicion = fecha;

            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarCasoNegocioServicio(casoServicio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}