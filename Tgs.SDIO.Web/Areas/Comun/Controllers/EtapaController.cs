﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class EtapaController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public EtapaController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        [HttpPost]
        public JsonResult ListarComboEtapas()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboEtapaOportunidad());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarComboEtapaOportunidadPorIdFase(FaseDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboEtapaOportunidadPorIdFase(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}