﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class EquipoTrabajoController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public EquipoTrabajoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        [HttpPost]
        public JsonResult ListarComboEquipoTrabajoAreas()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboEquipoTrabajoAreas());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}