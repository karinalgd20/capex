﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Funciones;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class CorreoController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        private readonly AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio = null;

        public CorreoController(AgenteServicioComunSDio agenteServicioComunSDio, AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
            this.agenteServicioCartaFianzaSDio = agenteServicioCartaFianzaSDio;
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult Index(MailResponse mailResponse)
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult EnviarCorreo(MailDto mailDto)
        {
            var resultado = "";//= agenteServicioComunSDio.InvocarFuncionAsync(o => o.EnviarCorreo(mailRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        [CustomAuthorize]
        public ActionResult ObtenerDatosCorreo(MailRequest mailRequest)
        {
            MailResponse respuesta = new MailResponse();
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ObtenerDatosCorreo(mailRequest));
            respuesta = (resultado != null) ? resultado : respuesta;
            respuesta.Body = (resultado != null) ? respuesta.Body.Replace("@NombreUsuario", Funciones.UppercaseWords(SessionFacade.Usuario.FirstName)) : "";
            respuesta.Body = (resultado != null) ? respuesta.Body.Replace("@CorreoUsuario", SessionFacade.Usuario.EmailUser) : "";
            string baseUrl = string.Format("{0}://{1}{2}{3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? string.Empty : ":" + Request.Url.Port, Request.ApplicationPath);
            respuesta.Body = (resultado != null) ? respuesta.Body.Replace("@baseUrlCartaFianza", baseUrl) : "";
            respuesta.From = SessionFacade.Usuario.EmailUser;
            respuesta.IdColaborador = SessionFacade.Usuario.IdUsusarioSistema;
            respuesta.IdUsuarioCreacion = SessionFacade.Usuario.IdUsusarioSistema;
            respuesta.UsuarioEdicion = SessionFacade.Usuario.Login;
            ViewBag.Correo = JsonConvert.SerializeObject(respuesta);
            return View("Index");
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult ObtenerDatosCorreoGC(MailRequest mailRequest)
        {
            MailResponse respuesta = new MailResponse();
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ObtenerDatosCorreoRenovacion(mailRequest));
            respuesta = (resultado != null) ? resultado : respuesta;
            respuesta.Body = (resultado != null) ? respuesta.Body.Replace("@NombreUsuario", Funciones.UppercaseWords(SessionFacade.Usuario.FirstName)) : "";
            respuesta.Body = (resultado != null) ? respuesta.Body.Replace("@CorreoUsuario", SessionFacade.Usuario.EmailUser) : "";
            string baseUrl = string.Format("{0}://{1}{2}{3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? string.Empty : ":" + Request.Url.Port, Request.ApplicationPath);
            respuesta.Body = (resultado != null) ? respuesta.Body.Replace("@baseUrlCartaFianza", baseUrl) : "";
            respuesta.From = SessionFacade.Usuario.EmailUser;
            respuesta.IdColaborador = SessionFacade.Usuario.IdUsusarioSistema;
            respuesta.IdUsuarioCreacion = SessionFacade.Usuario.IdUsusarioSistema;
            respuesta.UsuarioEdicion = SessionFacade.Usuario.Login;
            ViewBag.Correo = JsonConvert.SerializeObject(respuesta);
            return View("Index");
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult EnviarCorreoCartaFianza(MailRequest mailRequest)
        {
            string baseUrl = string.Format("{0}://{1}{2}{3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? string.Empty : ":" + Request.Url.Port, Request.ApplicationPath);
            mailRequest.Body = mailRequest.Body.Replace("../../", baseUrl);
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.EnviarCorreoCartaFianza(mailRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}
