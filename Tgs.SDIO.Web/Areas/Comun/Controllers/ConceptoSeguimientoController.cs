﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class ConceptoSeguimientoController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ConceptoSeguimientoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        [HttpPost]
        public JsonResult ListarComboConceptoSeguimiento()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboConceptoSeguimiento());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarComboConceptoSeguimientoAgrupador()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboConceptoSeguimientoAgrupador());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarComboConceptoSeguimientoNiveles()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboConceptoSeguimientoNiveles());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerConceptoSeguimientoPorId(ConceptoSeguimientoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerConceptoSeguimientoPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarConceptoSeguimiento(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarConceptoSeguimiento(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}