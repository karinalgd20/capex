﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class ServicioCMIController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ServicioCMIController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/ServicioCMI
        public ActionResult Index()
        {
            return View();
        }

        // ServicioCMI
        public JsonResult ListarServicioCMIPorLineaNegocio(ServicioCMIDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarServicioCMIPorLineaNegocio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}