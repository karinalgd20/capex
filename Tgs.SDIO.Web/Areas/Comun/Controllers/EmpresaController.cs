﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class EmpresaController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public EmpresaController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        [HttpPost]
        public JsonResult ListarComboEmpresa()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboEmpresa());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}