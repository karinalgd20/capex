﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.UI.AgenteServicio;


namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class BandejaAreaSeguimientoController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public BandejaAreaSeguimientoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        // GET: Trazabilidad/AreaSeguimientoBandeja
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarAreaSeguimiento(AreaSeguimientoDtoRequest areaRequest)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListadoAreasSeguimientoPaginado(areaRequest));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult EliminarAreaSeguimiento(AreaSeguimientoDtoRequest areaRequest)
        {
            areaRequest.IdUsuario = SessionFacade.Usuario.UserId;

            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.EliminarAreaSeguimiento(areaRequest));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}