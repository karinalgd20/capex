﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class RegistrarRecursoLineaNegocioController : BaseController
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio;
        public RegistrarRecursoLineaNegocioController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarRecursoLineaNegocio(RecursoLineaNegocioDtoRequest recursoLineaNegocioDtoRequest)
        {
            recursoLineaNegocioDtoRequest.IdUsuarioCreacion = User.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarRecursoLineaNegocio(recursoLineaNegocioDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }      

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarRecursoLineaNegocioPaginado(RecursoLineaNegocioDtoRequest recursoLineaNegocioDtoRequest)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarRecursoLineaNegocioPaginado(recursoLineaNegocioDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarEstadoRecursoLineaNegocio(RecursoLineaNegocioDtoRequest recursoLineaNegocioDtoRequest)
        {
            recursoLineaNegocioDtoRequest.IdUsuarioEdicion = User.UserId;
            recursoLineaNegocioDtoRequest.FechaEdicion = DateTime.Now;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarEstadoRecursoLineaNegocio(recursoLineaNegocioDtoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}