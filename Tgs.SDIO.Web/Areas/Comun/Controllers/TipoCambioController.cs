﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class TipoCambioController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public TipoCambioController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/SubServicio
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult ListaTipoCambioPaginado(TipoCambioDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListaTipoCambioPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }



    }
}