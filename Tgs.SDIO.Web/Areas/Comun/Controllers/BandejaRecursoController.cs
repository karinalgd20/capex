﻿using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class BandejaRecursoController : Controller
    {

        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;

        public BandejaRecursoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        // GET: Trazabilidad/RecursoBandeja
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarRecurso(RecursoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListadoRecursosPaginado(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult EliminarRecurso(RecursoDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.EliminarRecurso(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);

        }

    }
}