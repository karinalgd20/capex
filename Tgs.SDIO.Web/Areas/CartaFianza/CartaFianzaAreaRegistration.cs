﻿using System.Web.Mvc;

namespace Tgs.SDIO.Web.Areas.CartaFianza
{
    public class CartaFianzaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "CartaFianza";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "CartaFianza_default",
                "CartaFianza/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}