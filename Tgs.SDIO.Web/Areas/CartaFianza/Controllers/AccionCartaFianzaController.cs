﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.CartaFianza.Controllers
{
    public class AccionCartaFianzaController : Controller
    {
        private readonly AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio = null;

        public AccionCartaFianzaController(AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio)
        {
            this.agenteServicioCartaFianzaSDio = agenteServicioCartaFianzaSDio;
        }


        // GET: CartaFianza/AccionCartaFianza
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListaCartaFianzaDetalleAccionId(CartaFianzaDetalleAccionRequest cartaFianza)
        {
            // cartaFianza.IdCartaFianza = 1;
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ListaCartaFianzaDetalleAccionId(cartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult InsertaCartaFianzaDetalleAccion(CartaFianzaDetalleAccionRequest cartaFianza)
        {
            cartaFianza.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            cartaFianza.IdColaboradorACargo = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.InsertaCartaFianzaDetalleAccion(cartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizaCartaFianzaDetalleAccion(CartaFianzaDetalleAccionRequest cartaFianza)
        {
            cartaFianza.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ActualizaCartaFianzaDetalleAccion(cartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }



    }
}
