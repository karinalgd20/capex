﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.CartaFianza.Controllers
{
    public class RecuperoCartaFianzaController : Controller
    {
        private readonly AgenteServicioCartaFianzaSDio _agenteServicioCartaFianzaSDio = null;

        public RecuperoCartaFianzaController(AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio)
        {
            this._agenteServicioCartaFianzaSDio = agenteServicioCartaFianzaSDio;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarCartaFianzaRecupero(CartaFianzaDetalleRecuperoRequest cartaFianzaDetalleRecuperoRequest)
        {
            var resultado = _agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ListaCartaFianzaDetalleRecupero(cartaFianzaDetalleRecuperoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarCartaFianzaRecupero(CartaFianzaDetalleRecuperoRequest cartaFianzaDetalleRecuperoRequest)
        {
            cartaFianzaDetalleRecuperoRequest.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            cartaFianzaDetalleRecuperoRequest.FechaEdicion = DateTime.Now;
            var resultado = _agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ActualizaCartaFianzaDetalleRecupero(cartaFianzaDetalleRecuperoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListaCartaFianzaDetalleSeguimiento(CartaFianzaDetalleSeguimientoRequest cartaFianzaDetalleSeguimientoRequest)
        {
            var resultado = _agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ListaCartaFianzaDetalleSeguimiento(cartaFianzaDetalleSeguimientoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult InsertaCartaFianzaSeguimiento(CartaFianzaDetalleSeguimientoRequest cartaFianzaDetalleSeguimientoRequest)
        {
            cartaFianzaDetalleSeguimientoRequest.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            cartaFianzaDetalleSeguimientoRequest.FechaCreacion = DateTime.Now;
            var resultado = _agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.InsertaCartaFianzaSeguimiento(cartaFianzaDetalleSeguimientoRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}
