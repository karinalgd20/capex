﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.CartaFianza.Controllers
{
    public class MaestroCartaFianzaController : Controller
    {
        private readonly AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio = null;
        // GET: CartaFianza/RegistroCartaFianza

        public MaestroCartaFianzaController(AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio)
        {
            this.agenteServicioCartaFianzaSDio = agenteServicioCartaFianzaSDio;
        }
        // GET: CartaFianza/CartaFianza
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarCartaFianza(CartaFianzaRequest cartaFianza)
        {
            // cartaFianza.IdCartaFianza = 1;
            //SessionFacade.Usuario.EmailUser;
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ListaCartaFianza(cartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizaARenovacionCartaFianza(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacion)
        {
            cartaFianzaRenovacion.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            cartaFianzaRenovacion.IdColaborador = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ActualizaARenovacionCartaFianza(cartaFianzaRenovacion));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarCartaFianzaRequerida(CartaFianzaRequest cartaFianza)
        {
            cartaFianza.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            cartaFianza.UsuarioEdicion = SessionFacade.Usuario.Login;
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ActualizarCartaFianzaRequerida(cartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarColaborador(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalle)
        {
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ListarColaborador(cartaFianzaColaboradorDetalle));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListaCartaFianzaCombo(CartaFianzaRequest cartaFianza)
        {
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ListaCartaFianzaCombo(cartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult CartasFianzasSinRespuesta(CartaFianzaRequest cartaFianza)
        {
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.CartasFianzasSinRespuesta(cartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult GuardarRespuestaGerenteCuenta(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacion)
        {
            cartaFianzaRenovacion.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.GuardarRespuestaGerenteCuenta(cartaFianzaRenovacion));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult GuardarConfirmacionGerenteCuenta(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacion)
        {
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.GuardarConfirmacionGerenteCuenta(cartaFianzaRenovacion));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}
