﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.CartaFianza.Controllers
{
    public class RegistroCartaFianzaController : Controller
    {

        private readonly AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio = null;
        // GET: CartaFianza/RegistroCartaFianza

        public RegistroCartaFianzaController(AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio)
        {
            this.agenteServicioCartaFianzaSDio = agenteServicioCartaFianzaSDio;
        }
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]  
        public JsonResult ListarCartaFianzaId(CartaFianzaRequest cartaFianza)
        {
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ListaCartaFianzaId(cartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]       
        public JsonResult InsertaCartaFianza(CartaFianzaRequest cartaFianza)
        {
            cartaFianza.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            cartaFianza.UsuarioEdicion = SessionFacade.Usuario.Login;
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.InsertaCartaFianza(cartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]       
        public JsonResult ActualizaCartaFianza(CartaFianzaRequest cartaFianza)
        {
            cartaFianza.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            cartaFianza.UsuarioEdicion = SessionFacade.Usuario.Login;
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ActualizaCartaFianza(cartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

      


    }
}
