﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.CartaFianza.Controllers
{
    public class FormularioGerentesCuentaController : Controller
    {
        private readonly AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;

        public FormularioGerentesCuentaController(AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio, AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioCartaFianzaSDio = agenteServicioCartaFianzaSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        public ActionResult Index()
        {
            var respuesta = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerRecursoPorIdUsuarioRais(SessionFacade.Usuario.UserId));
            ViewBag.IdRecurso = respuesta.IdRecurso;
            return View();
        }
    }
}