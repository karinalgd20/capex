﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.CartaFianza.Controllers
{
    public class RenovacionCartaFianzaController : Controller
    {
        private readonly AgenteServicioCartaFianzaSDio _agenteServicioCartaFianzaSDio = null;

        public RenovacionCartaFianzaController(AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio)
        {
            this._agenteServicioCartaFianzaSDio = agenteServicioCartaFianzaSDio;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarCartaFianzaRenovacion(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacionRequest)
        {
            var resultado = _agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ListaCartaFianzaDetalleRenovacion(cartaFianzaRenovacionRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarCartaFianzaRenovacion(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacionRequest)
        {
            cartaFianzaRenovacionRequest.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            cartaFianzaRenovacionRequest.FechaEdicion = DateTime.Now;
            cartaFianzaRenovacionRequest.UsuarioEdicion = SessionFacade.Usuario.Login;
            var resultado = _agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ActualizaCartaFianzaDetalleRenovacion(cartaFianzaRenovacionRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}
