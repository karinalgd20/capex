﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.CartaFianza.Controllers
{
    public class AsignarColaboradorCartaFianzaController : Controller
    {
        private readonly AgenteServicioCartaFianzaSDio _agenteServicioCartaFianzaSDio = null;

        public AsignarColaboradorCartaFianzaController(AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio)
        {
            this._agenteServicioCartaFianzaSDio = agenteServicioCartaFianzaSDio;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult AsignarSupervisorAGerenteCuenta(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            cartaFianzaColaboradorDetalleRequest.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = _agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.AsignarSupervisorAGerenteCuenta(cartaFianzaColaboradorDetalleRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult AsignarGerenteCuentaACartaFianza(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            cartaFianzaColaboradorDetalleRequest.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = _agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.AsignarGerenteCuentaACartaFianza(cartaFianzaColaboradorDetalleRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarTesorera(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            cartaFianzaColaboradorDetalleRequest.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            var resultado = _agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ActualizarTesorera(cartaFianzaColaboradorDetalleRequest));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}