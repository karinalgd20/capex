﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.CartaFianza.Controllers
{
    public class DashBoardCartaFianzaController : Controller
    {

        private readonly AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio = null;
        // GET: CartaFianza/DashBoardCartaFianza
        public DashBoardCartaFianzaController(AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio)
        {
            this.agenteServicioCartaFianzaSDio = agenteServicioCartaFianzaSDio;
        }
        public ActionResult Index()
        {
            return View(); 
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListaDatos_DashoBoard_CartaFianza(DashBoardCartaFianzaRequest dashBoardcartaFianza)
        {
            // cartaFianza.IdCartaFianza = 1;
            //SessionFacade.Usuario.EmailUser;
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ListaDatos_DashoBoard_CartaFianza(dashBoardcartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListaDatos_DashoBoard_CartaFianza_Pie(DashBoardCartaFianzaRequest dashBoardcartaFianza)
        {
            // cartaFianza.IdCartaFianza = 1;
            //SessionFacade.Usuario.EmailUser;
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ListaDatos_DashoBoard_CartaFianza_Pie(dashBoardcartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


    }
}
