﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Negocio.Controllers
{
    public class NgBandejaOportunidadesController : Controller
    {
        //// GET: Negocio/BandejaOportunidad
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        private readonly AgenteServicioNegocioSDio agenteServicioNegocioSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;

        public NgBandejaOportunidadesController(AgenteServicioNegocioSDio agenteServicioNegocioSDio,
            AgenteServicioSeguridadSDio agenteServicioSeguridadSDio, AgenteServicioComunSDio agenteServicioComunSDio
            )
        {
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
            this.agenteServicioNegocioSDio = agenteServicioNegocioSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        public ActionResult Index()
        {
            OportunidadGanadora oportunidad = new OportunidadGanadora();

            var OptionGics = Generales.EstadoLogico.Falso;

            var perfil = SessionFacade.Usuario.CodigoPerfil;

            var CodPerfil = string.Empty;
            CodPerfil = perfil[0];

            if (CodPerfil == Generales.TipoPerfil.NegociosEstandar)
            {
                oportunidad.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            }

            if (CodPerfil == Generales.TipoPerfil.EmisorEstandar)
            {
                oportunidad.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
                OptionGics = Generales.EstadoLogico.Verdadero;
            }

            ViewBag.vbOptionGics = OptionGics;
            
            return View();
        }
        [HttpPost]
        public JsonResult EliminarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            var resultado = agenteServicioNegocioSDio.InvocarFuncionAsync(o => o.EliminarOportunidadGanadora(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult BuscarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            var resultado = agenteServicioNegocioSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadGanadora(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListaBandejaOportunidad(OportunidadGanadoraDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(o => o.ListarOportunidadGanadora(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}