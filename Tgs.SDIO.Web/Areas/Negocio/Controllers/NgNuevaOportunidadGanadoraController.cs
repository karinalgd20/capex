﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Negocio;

namespace Tgs.SDIO.Web.Areas.Negocio.Controllers
{
    public class NgNuevaOportunidadGanadoraController : Controller
    {
        private readonly AgenteServicioNegocioSDio agenteServicioNegocioSDio = null;

        public NgNuevaOportunidadGanadoraController(AgenteServicioNegocioSDio agenteServicioNegocioSDio)
        {
            this.agenteServicioNegocioSDio = agenteServicioNegocioSDio;
        }

        // GET: Negocio/NgNuevaOportunidadGanadora
        public ActionResult Index(string PER)
        {
            if (PER != null)
            {
                OportunidadGanadoraDtoRequest oportunidad = new OportunidadGanadoraDtoRequest();
                IsisNroOfertaDtoRequest listaOfertas = new IsisNroOfertaDtoRequest();
                SisegoCotizadoDtoRequest listSisegos = new SisegoCotizadoDtoRequest();
                AsociarNroOfertaSisegosDtoRequest listaAsociada = new AsociarNroOfertaSisegosDtoRequest();

                oportunidad.PER = PER;

                var resultOportunidad = agenteServicioNegocioSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadGanadora(oportunidad));

                listaOfertas.Id_OportunidadGanadora = resultOportunidad.Id;

                var resultOfertas = agenteServicioNegocioSDio.InvocarFuncionAsync(x => x.ListarIsisNroOfertaPaginado(listaOfertas));

                listSisegos.Id_OportunidadGanadora = resultOportunidad.Id;

                var resultSisegos = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ListarSisegosCotizadosPaginado(listSisegos));

                listaAsociada.IdOportunidad = resultOportunidad.Id;

                var resultListaAsociar = agenteServicioNegocioSDio.InvocarFuncionAsync(l => l.ListarOfertaSisegosPaginado(listaAsociada));

                ViewBag.vbOportunidadGanadora = resultOportunidad;
                ViewBag.PER = PER;
                SessionFacade.IdOportunidad = resultOportunidad.Id;
            }
            else
            {
                SessionFacade.IdOportunidad = Generales.Numeric.Cero;
                ViewBag.vbOportunidadGanadora = "";
            }
            return View("Index");
        }

        [HttpPost]
        public JsonResult RegistarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            request.IdEtapa = EtapasRPA.Validacion;
            request.IdEstado = EstadosRPA.PorEnviar;
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadGanadora(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ValidarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            request.IdEstado = EstadosRPA.Pendiente;
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(o => o.ValidarOportunidadGanadora(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AgregarIsisNroOferta(IsisNroOfertaDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(o => o.AgregarIsisNroOferta(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarIsisNroOferta(IsisNroOfertaDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(o => o.ListarIsisNroOferta(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EliminarIsisNroOferta(IsisNroOfertaDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(o => o.EliminarIsisNroOferta(request));
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult ListarIsisNroOfertaPaginado(IsisNroOfertaDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(o => o.ListarIsisNroOfertaPaginado(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AgregarSisegoCotizado(SisegoCotizadoDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.AgregarSisegoCotizado(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarSisegoCotizado(SisegoCotizadoDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ListarSisegosCotizados(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EliminarSisegoCotizado(SisegoCotizadoDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.EliminarSisegoCotizado(request));
            return Json(result, JsonRequestBehavior.AllowGet);
            
        }

        [HttpPost]
        public JsonResult ListarSisegoCotizadoPaginado(SisegoCotizadoDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ListarSisegosCotizadosPaginado(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AsociarOfertaSisego(AsociarNroOfertaSisegosDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            request.IdEstado = 1;
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.AgregarOfertaSisegos(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarOfertaSisego(AsociarNroOfertaSisegosDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ListarOfertaSisegos(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EliminarOfertaSisego(AsociarNroOfertaSisegosDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.EliminarOfertaSisego(request));
            return Json(result, JsonRequestBehavior.AllowGet);
            
        }

        [HttpPost]
        public JsonResult ListarOfertaSisegoPaginado(AsociarNroOfertaSisegosDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ListarOfertaSisegosPaginado(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerIdOportunidadGanadora(OportunidadGanadoraDtoRequest respuesta)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ObtenerOportunidadGanadora(respuesta));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerCasoOportunidadGanadora(OportunidadGanadoraDtoRequest consulta)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ObtenerCasoOportunidadGanadora(consulta));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerNroOfertaById(AsociarNroOfertaSisegosDtoRequest requestId)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ObtenerNroOfertaById(requestId));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ObtenerOportunidadGanadora(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}