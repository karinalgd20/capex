﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Negocio.Controllers
{
    public class ProcesarOportunidadGanadoraController : Controller
    {
        private readonly AgenteServicioNegocioSDio agenteServicioNegocioSDio = null;

        public ProcesarOportunidadGanadoraController(AgenteServicioNegocioSDio agenteServicioNegocioSDio)
        {
            this.agenteServicioNegocioSDio = agenteServicioNegocioSDio;
        }

        #region -----Index-----

        public ActionResult Index(int? id)
        {
            var oportunidad = id;
            if (oportunidad > 0)
            {
                ViewData["PER"] = oportunidad;
            }

            OportunidadGanadora op = new OportunidadGanadora();

            var OptionGics = Generales.EstadoLogico.Falso;

            var perfil = SessionFacade.Usuario.CodigoPerfil;

            var CodPerfil = string.Empty;
            CodPerfil = perfil[0];

            if (CodPerfil == Generales.TipoPerfil.NegociosEstandar)
            {
                op.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            }

            if (CodPerfil == Generales.TipoPerfil.EmisorEstandar)
            {
                op.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
                OptionGics = Generales.EstadoLogico.Verdadero;
            }

            ViewBag.vbOptionGics = OptionGics;

            return View();
        }

        [HttpPost]
        public JsonResult AgregarServicioRPA(RPAServiciosxNroOfertaDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            //var result = agenteServicioNegocioSDio.InvocarFuncionAsync(o => o.AgregarIsisNroOferta(request));
            //return Json(result, JsonRequestBehavior.AllowGet);
            return null;
        }

        [HttpPost]
        public JsonResult ListarIsisNroOferta(IsisNroOfertaDtoRequest request)
        {
            request.IdEstado = Generales.Estados.Activo;
            var ListIsisOfertas = agenteServicioNegocioSDio.InvocarFuncionAsync(isis => isis.ListarIsisNroOferta(request));
            return Json(ListIsisOfertas);
        }

        public JsonResult ListarServiciosNroOfertaPaginado(RPAServiciosxNroOfertaDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(o => o.ListarServiciosNroOfertaPaginado(request));
            return Json(result, JsonRequestBehavior.AllowGet);
            //return null;
        }

        [HttpPost]
        public JsonResult AgregarSisegoDetalles(RPASisegoDetalleDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            //var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.AgregarSisegoCotizado(request));
            //return Json(result, JsonRequestBehavior.AllowGet);
            return null;
        }

        [HttpPost]
        public JsonResult ListarSisegoDetallesPaginado(RPASisegoDetalleDtoRequest request)
        {

            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ListarSisegoDetallePaginado(request));
            return Json(result, JsonRequestBehavior.AllowGet);
            //return null;
        }

        [HttpPost]
        public JsonResult AsociarServicioSisego(AsociarServicioSisegosDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            request.IdEstado = 1;

            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.AsociarServicioSisegos(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarServicioSisegoPaginado(AsociarServicioSisegosDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ListarServicioSisegosPaginado(request));
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult ObtenerOportunidadById(OportunidadGanadoraDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ObtenerOportunidadById(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerRucClientePorIdOportunidad(OportunidadGanadoraDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ObtenerRucClientePorIdOportunidad(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarEtapaCierreOportunidad(OportunidadGanadoraDtoRequest request)
        {
            request.IdEtapa = Generales.EtapasOportunidad.CierreOferta;
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ActualizarEtapaCierreOportunidad(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region -----Detalles-----
        public ActionResult Detalle(int? id)
        {
            var IdDetalle = id;
            if (IdDetalle > 0)
            {
                ViewData["Id"] = id;
            }
            return View();
        }

        [HttpPost]
        public JsonResult ListarServicioDetalles(RPAServiciosxNroOfertaDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ListarServiciosNroOferta(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult ListarSisegoDetalles(RPASisegoDetalleDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ListarSisegoDetalle(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ListarDetalleEquipos(RPAEquiposServicioDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ListarEquipoServicioPaginado(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarDetalleContacto(RPASisegoDatosContactoDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ListaDetallesContactoRPAPaginadoDtoResponse(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarContactoServicio(RPASisegoDatosContactoDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.ActualizarContactoServicio(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarContactoServicio(RPASisegoDatosContactoDtoRequest request)
        {
            var result = agenteServicioNegocioSDio.InvocarFuncionAsync(s => s.RegistrarContactoServicio(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}