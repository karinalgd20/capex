﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tgs.SDIO.Web.Areas.Negocio
{
    public class NegocioAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Negocio";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Negocio_default",
                "Negocio/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}