﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class CasoOportunidadController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;
        public CasoOportunidadController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }

        [HttpPost]
        public JsonResult ObtenerCasoOportunidadPorId(CasoOportunidadDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerCasoOportunidadPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarCasoOportunidadModalPreventa(CasoOportunidadDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ActualizarCasoOportunidadModalPreventa(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }

}