﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class BandejaConceptoSeguimientoController : Controller
    {

        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public BandejaConceptoSeguimientoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/ConceptoSeguimientoBandeja
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {

            return View();
        }
        //[HttpPost]
        //[CustomAuthorize]
        //public JsonResult ListarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        //{
        //    var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListadoConceptosSeguimientoPaginado(request));

        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //[CustomAuthorize]
        //public JsonResult EliminarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        //{
        //    request.IdUsuario = SessionFacade.Usuario.UserId;
        //    var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.EliminarConceptoSeguimiento(request));

        //    return Json(resultado, JsonRequestBehavior.AllowGet);


        //}
    }
}