﻿using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class BandejaActividadOportunidadController : Controller
    {

        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;
        public BandejaActividadOportunidadController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/BandejaActividadOportunidad
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {

            return View();
        }

        //[HttpPost]
        //[CustomAuthorize]
        //public JsonResult ListarActividadOportunidad(ActividadOportunidadDtoRequest areaRequest)
        //{
        //    var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListatActividadOportunidadPaginado(areaRequest));

        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //[CustomAuthorize]
        //public JsonResult EliminarActividadOportunidad(ActividadOportunidadDtoRequest areaRequest)
        //{
        //    areaRequest.IdUsuario = SessionFacade.Usuario.UserId;

        //    var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.EliminarActividadOportunidad(areaRequest));

        //    return Json(resultado, JsonRequestBehavior.AllowGet);

        //}
    }
}