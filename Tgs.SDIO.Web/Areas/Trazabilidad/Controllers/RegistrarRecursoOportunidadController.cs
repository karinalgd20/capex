﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class RegistrarRecursoOportunidadController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public RegistrarRecursoOportunidadController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/RecursoOportunidadRegistrar
        [CustomAuthorize]
        public ActionResult Index(int id)
        {
            if (id == 0)
            {
                ViewBag.vbDatoRecursoOportunidad = "";
            }
            else
            {
                var recursoOportunidad = new RecursoOportunidadDtoRequest { IdAsignacion = id };
                var DatoRecursoOportunidad = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerRecursoOportunidadPorId(recursoOportunidad));
                ViewBag.vbDatoRecursoOportunidad = DatoRecursoOportunidad;
            }
            return View();
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarRecursoOportunidad(RecursoOportunidadDtoRequest request)
        {
            if (request.IdAsignacion != 0)
            {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ActualizarRecursoOportunidad(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            else
            {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.RegistrarRecursoOportunidad(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }

        }
    }
}