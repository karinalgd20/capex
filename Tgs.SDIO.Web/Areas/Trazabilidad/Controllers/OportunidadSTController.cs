﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class OportunidadSTController : BaseController
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;
        public OportunidadSTController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }

        [HttpPost]
        public JsonResult ListaOportunidadSTModalObservacionPaginado(OportunidadSTDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListaOportunidadSTModalObservacionPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerOportunidadSTPorId(OportunidadSTDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadSTPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ListaOportunidadSTModalRecursoPaginado(OportunidadSTDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListaOportunidadSTModalRecursoPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListaOportunidadSTMasivoPaginado(OportunidadSTDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListaOportunidadSTMasivoPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListaOportunidadSTSeguimientoPreventaPaginado(OportunidadSTSeguimientoPreventaDtoRequest request)
        {

            var Lider = false;
            foreach (string codigo in User.CodigoPerfil)
            {
                if (codigo == Util.Constantes.Generales.TipoPerfil.coordinadorServicioF4) {
                    Lider = true;
                }
            }
            request.UserId = User.UserId;
            request.Lider = Lider;
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListaOportunidadSTSeguimientoPreventaPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListaOportunidadSTSeguimientoPostventaPaginado(OportunidadSTSeguimientoPostventaDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListaOportunidadSTSeguimientoPostventaPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ActualizarOportunidadSTModalPreventa(OportunidadSTDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidadSTModalPreventa(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}