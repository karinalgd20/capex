﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;


namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class RegistrarTiempoAtencionEquipoController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public RegistrarTiempoAtencionEquipoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }

        [CustomAuthorize]
        public ActionResult Index(int id)
        {
            if (id == 0)
            {
                ViewBag.vbTiempoAtencionEquipo = "";
            }
            else
            {
                var request = new TiempoAtencionEquipoDtoRequest { IdTiempoAtencion = id };
                var DatoTiempoAtencionEquipo = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerTiempoAtencionEquipoPorId(request));
                ViewBag.vbTiempoAtencionEquipo = DatoTiempoAtencionEquipo;
            }
            return View();
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request)
        {
            if (request.IdTiempoAtencion != 0)
            {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ActualizarTiempoAtencionEquipo(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            else
            {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.RegistrarTiempoAtencionEquipo(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }

        }
    }
}