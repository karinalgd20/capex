﻿using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;


namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class BandejaDatosPreventaMovilController : Controller
    {

        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public BandejaDatosPreventaMovilController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/DatosPreventaMovilBandeja
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {

            return View();
        }


        [HttpPost]
        [CustomAuthorize]
        public JsonResult EliminarDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.EliminarDatosPreventaMovil(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);

        }


    }
}