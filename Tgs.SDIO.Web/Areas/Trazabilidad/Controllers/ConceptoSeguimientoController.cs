﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class ConceptoSeguimientoController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;
        public ConceptoSeguimientoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        //[HttpPost]
        //public JsonResult ListarComboConceptoSeguimiento()
        //{
        //    var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListarComboConceptoSeguimiento());
        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult ObtenerConceptoSeguimientoPorId(ConceptoSeguimientoDtoRequest request)
        //{
        //    var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerConceptoSeguimientoPorId(request));
        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult ActualizarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        //{
        //    var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ActualizarConceptoSeguimiento(request));
        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult RegistrarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        //{
        //    var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.RegistrarConceptoSeguimiento(request));
        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}

    }
}