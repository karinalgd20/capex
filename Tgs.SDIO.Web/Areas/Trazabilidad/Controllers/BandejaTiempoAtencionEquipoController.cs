﻿using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class BandejaTiempoAtencionEquipoController : Controller
    {

        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public BandejaTiempoAtencionEquipoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/BandejaObservacionOportunidad
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index(string id)
        {
            if (id == null)
            {
                ViewBag.vbTiempoAtencionEquipo = '0';
            }
            else
            {
                ViewBag.vbTiempoAtencionEquipo = id;
            }
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListadoTiempoAtencionEquipoPaginado(TiempoAtencionEquipoDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListadoTiempoAtencionEquipoPaginado(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult EliminarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.EliminarTiempoAtencionEquipo(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);

        }
    }
}