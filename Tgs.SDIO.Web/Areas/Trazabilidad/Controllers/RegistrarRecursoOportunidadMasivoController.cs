﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class RegistrarRecursoOportunidadMasivoController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public RegistrarRecursoOportunidadMasivoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarRecursoOportunidadMasivo(RecursoOportunidadDtoRequest request)
        {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.RegistrarRecursoOportunidadMasivo(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}