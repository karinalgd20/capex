﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;


namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class RegistrarDatosPreventaMovilController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public RegistrarDatosPreventaMovilController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        public ActionResult Index()
        {

            return View();
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarDatosPreventaMovilModalPreventa(DatosPreventaMovilDtoRequest request)
        {

                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ActualizarDatosPreventaMovilModalPreventa(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {

            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.RegistrarDatosPreventaMovil(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);

        }
    }
}