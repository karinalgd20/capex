﻿using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class BandejaObservacionOportunidadController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public BandejaObservacionOportunidadController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/BandejaObservacionOportunidad
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index(string id)
        {
            if (id == null)
            {
                ViewBag.vbDatosPreventaMovil = '0';
            }
            else {
                ViewBag.vbDatosPreventaMovil = id;
            }
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarObservacionOportunidad(ObservacionOportunidadDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListadoObservacionesOportunidadPaginado(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult EliminarObservacionOportunidad(ObservacionOportunidadDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.EliminarObservacionOportunidad(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);

        }
    }
}