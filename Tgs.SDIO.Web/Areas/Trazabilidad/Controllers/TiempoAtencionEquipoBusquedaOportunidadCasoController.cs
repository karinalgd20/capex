﻿using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;
namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class TiempoAtencionEquipoBusquedaOportunidadCasoController : Controller
    {
        // GET: Comun/TiempoAtencionEquipoBusquedaOportunidadCaso
        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }
    }
}