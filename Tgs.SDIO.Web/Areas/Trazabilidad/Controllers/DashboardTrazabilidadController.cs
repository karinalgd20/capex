﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    [CustomAuthorize]
    [RepErrorCatch]
    public class DashboardTrazabilidadController : BaseController
    {
        // GET: Trazabilidad/DashboardTrazabilidad
        public ActionResult Index()
        {
            ViewBag.CodigosPerfil = User.CodigoPerfil;
            ViewBag.IdUsuario = User.UserId;
            return View("~/Views/Principal/DashboardTrazabilidad.cshtml");
        }
    }
}