﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.Web.Utilitarios;

using Newtonsoft.Json;
using System;
using System.Web;

using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.Util.Funciones;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class DatosPreventaMovilController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public DatosPreventaMovilController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio, AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        [HttpPost]
        public JsonResult ObtenerDatosPreventaMovilPorId(DatosPreventaMovilDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerDatosPreventaMovilPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarArchivo(HttpPostedFileBase[] files, string cargaArchivo)
        {
            byte[] fileArchivo = Funciones.ConvertirDatosABytes(files[0].InputStream);
            ArchivoDtoRequest request = JsonConvert.DeserializeObject<ArchivoDtoRequest>(cargaArchivo);
            request.Nombre = files[0].FileName;
            request.NombreInterno = Funciones.GenerarNombreArchivo(request.Nombre);
            request.IdEstado = 1;
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());

            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarArchivo(request, fileArchivo));
           var datosPreventa = new DatosPreventaMovilDtoRequest { RutaArchivo = @"\\tgpesvlcli1054\SDIO-FILES\" + request.NombreInterno, IdUsuario = SessionFacade.Usuario.UserId };
            var resultado2 = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.CargaExcelDatosPreventaMovil(datosPreventa));



            return Json(resultado2, JsonRequestBehavior.AllowGet);
        }



    }
}