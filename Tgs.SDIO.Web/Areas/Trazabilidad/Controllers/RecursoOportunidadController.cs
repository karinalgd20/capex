﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class RecursoOportunidadController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;
        public RecursoOportunidadController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }

        [HttpPost]
        public JsonResult ObtenerRecursoOportunidadPorId(RecursoOportunidadDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerRecursoOportunidadPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarRecursoOportunidad(RecursoOportunidadDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ActualizarRecursoOportunidad(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarRecursoOportunidad(RecursoOportunidadDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.RegistrarRecursoOportunidad(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}