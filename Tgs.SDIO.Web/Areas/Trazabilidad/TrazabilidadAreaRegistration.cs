﻿using System.Web.Mvc;

namespace Tgs.SDIO.Web.Areas.Trazabilidad
{
    public class TrazabilidadAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Trazabilidad";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Trazabilidad_default",
                "Trazabilidad/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}