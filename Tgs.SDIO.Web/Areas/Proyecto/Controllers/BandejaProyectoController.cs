﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Proyecto.Controllers
{
    public class BandejaProyectoController : Controller
    {
        // GET: Proyecto/BandejaProyecto
        private readonly AgenteServicioProyectoSDio agenteServicioProyectoSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public BandejaProyectoController(AgenteServicioProyectoSDio agenteServicioProyectoSDio)
        {
            this.agenteServicioProyectoSDio = agenteServicioProyectoSDio;
        }

        public ActionResult Index()
        {
            var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            for (int i = 0; i < perfil.Length; i++)
            {
                CodPerfil = (perfil[i]);
            }

            ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Falso;

            if (CodPerfil != Generales.TipoPerfil.PreventaProyecto)
            {
                ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Verdadero;
            }

            return View();
        }

        public JsonResult ListarProyectoPaginado(ProyectoDtoRequest request)
        {
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ListarProyectoCabecera(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObtenerInfoUsuarioActual()
        {
            Models.UsuarioLoginSerializeModel oUsuarioActual = SessionFacade.Usuario;
            return Json(oUsuarioActual, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BandejaLP()
        {
            var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            for (int i = 0; i < perfil.Length; i++)
            {
                CodPerfil = (perfil[i]);
            }

            ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Falso;

            if (CodPerfil != Generales.TipoPerfil.PreventaProyecto)
            {
                ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Verdadero;
            }

            return View();
        }

        public ActionResult BandejaControl()
        {
            return RedirectToAction("BandejaLP");
        }

        public ActionResult ObtenerInformacionPEPCeCo()
        {
            return View("_InfoPEPCeCo");
        }

        public JsonResult GuardarPEPCeCo(ServicioCMIFinancieroDtoRequest request)
        {
            ProcesoResponse respuesta = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.InsertarServicioCMIFinanciero(request));
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObtenerPEPCeCo(ServicioCMIFinancieroDtoRequest request)
        {
            ServicioCMIFinancieroDtoResponse oServicioCMIFinancieroDtoResponse = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ObtenerServicioCMIFinanciero(request));
            return Json(oServicioCMIFinancieroDtoResponse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarJP(RecursoProyectoDtoRequest request)
        {
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.InsertarRecursoProyecto(request));

            // Cambiar de estado a AF
            EtapaProyectoDtoRequest rqEtapa = new EtapaProyectoDtoRequest()
            {
                IdProyecto = request.IdProyecto,
                IdFase = Configuracion.IdFasePreImplantacion,
                IdEtapa = Configuracion.IdEtapaAF,
                IdEstadoEtapa = Generales.ProyectoControlEstado.Registrado,
                Observaciones = "",
                IdUsuarioCreacion = SessionFacade.Usuario.UserId
            };

            var respuesta = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.RegistrarEtapaEstado(rqEtapa));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}