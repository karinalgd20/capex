﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Web.Proyecto.Conversiones;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Proyecto.Controllers
{
    public class RegistrarProyectoController : Controller
    {
        // GET: Proyecto/BandejaProyecto
        private readonly AgenteServicioProyectoSDio agenteServicioProyectoSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        DateTime fecha = DateTime.Now;
        public RegistrarProyectoController(AgenteServicioProyectoSDio agenteServicioProyectoSDio, 
                                            AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioProyectoSDio = agenteServicioProyectoSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        public ActionResult Index(int id = 0)
        {
            ViewBag.IdProyecto = id;

            var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            for (int i = 0; i < perfil.Length; i++)
            {
                CodPerfil = (perfil[i]);
            }

            ViewBag.IdPerfil = CodPerfil;

            ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Falso;

            if (CodPerfil == Generales.TipoPerfil.PreventaProyecto)
            {
                ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Verdadero;
            }

            return View();
        }

        public JsonResult ObtenerOportunidadGanada(string sIdOportunidad)
        {
            OportunidadGanadaDtoRequest request = new OportunidadGanadaDtoRequest
            {
                IdOportunidad = sIdOportunidad
            };

            OportunidadGanadaDtoResponse OportunidadGanadaDtoResponse = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadGanada(request));
            return Json(OportunidadGanadaDtoResponse, JsonRequestBehavior.AllowGet);
        }

        #region "Tab Cliente"
        public ActionResult ObtenerInformacionCliente()
        {
            return View();
        }

        [HttpPost]
        public JsonResult RegistrarProyectoCliente(ProyectoDtoRequest proyecto)
        {
            proyecto.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            proyecto.IdEstado = 0;
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.RegistrarProyecto(proyecto));

            //Cambiar de estado a Preventa.
            EtapaProyectoDtoRequest rqEtapa = new EtapaProyectoDtoRequest()
            {
                IdProyecto = resultado.Id,
                IdFase = Configuracion.IdFasePreImplantacion,
                IdEtapa = Configuracion.IdEtapaPreventa,
                IdEstadoEtapa = Generales.ProyectoControlEstado.Registrado,
                Observaciones = "",
                IdUsuarioCreacion = SessionFacade.Usuario.UserId
            };

            var respuesta = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.RegistrarEtapaEstado(rqEtapa));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarProyectoCliente(ProyectoDtoRequest proyecto)
        {
            proyecto.IdUsuarioModifica = SessionFacade.Usuario.UserId;
            proyecto.FechaModifica = fecha;
            proyecto.IdEstado = 0;
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ActualizarProyecto(proyecto));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerProyectoPorId(ProyectoDtoRequest proyecto)
        {
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ObtenerProyectoByID(proyecto));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Tab Descripcion"
        public ActionResult ObtenerInformacionDescripcion()
        {
            return View("_InfoDescripcion");
        }

        [HttpPost]
        public JsonResult ActualizarProyectoDescripcion(ProyectoDtoRequest request)
        {
            request.IdUsuarioModifica = SessionFacade.Usuario.UserId;
            request.FechaModifica = fecha;
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ActualizarProyectoDescripcion(request));

            if (resultado.TipoRespuesta == 0)
            {
                if (request.ListaTiposNuevo != null)
                {
                    foreach (var detalleSolucion in request.ListaTiposNuevo)
                    {
                        agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.RegistrarDetalleSolucion(detalleSolucion));
                    }
                }

                if (request.ListaTiposEliminado != null)
                {
                    foreach (var detalleSolucion in request.ListaTiposEliminado)
                    {
                        agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.EliminarDetalleSolucion(detalleSolucion));
                    }
                }
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Tab Montos"
        public ActionResult ObtenerInformacionMonto()
        {
            return View("_InfoMonto");
        }

        [HttpPost]
        public JsonResult RegistrarProyectoMontos(DetalleFacturarDtoRequest request)
        {
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.RegistrarDetalleFacturar(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarProyectoMontos(DetalleFacturarDtoRequest request)
        {
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ActualizarDetalleFacturar(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerDetalleFacturaPorIdProyecto(DetalleFacturarDtoRequest request)
        {
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ObtenerDetalleFacturarPorIdProyecto(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Tab Plazos"
        public ActionResult ObtenerInformacionPlazo()
        {
            return View("_InfoPlazo");
        }

        public JsonResult ListarDetallePlazo(int iIdProyecto)
        {
            List<DetallePlazoDtoResponse> ListaDetallePlazo = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ListarDetallePlazo(iIdProyecto));
            return Json(ListaDetallePlazo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GuardarDetallePlazo(List<DetallePlazoDtoRequest> request)
        {
            foreach (DetallePlazoDtoRequest item in request)
            {
                if (item.Cantidad.HasValue)
                {
                    item.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
                    item.IdUsuarioEdicion = SessionFacade.Usuario.UserId;

                    agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.InsertarDetallePlazo(item));
                }
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CalcularPeriodoPlazos(DetallePlazoDtoRequest request)
        {
            double dDiff = 0;
            int dAnios = 0;
            int dMeses = 0;
            int dDias = 0;

            dAnios = (int)Math.Round((request.FechaFin.Value - request.FechaInicio).Value.TotalDays / 365.25, 0);
            dMeses = (int)Math.Round((request.FechaFin.Value - request.FechaInicio).Value.TotalDays / (365.25 / 12), 0);
            dDias = (int)Math.Round((request.FechaFin.Value - request.FechaInicio).Value.TotalDays, 0);

            switch (request.IdUnidadMedida)
            {
                case Generales.PeriodoPlazos.Diaslaborables:
                    dDiff = ObtenerDiasLaborales(request.FechaInicio.Value, request.FechaFin.Value);
                    break;
                case Generales.PeriodoPlazos.Anios:
                    dDiff = (request.FechaFin.Value - request.FechaInicio).Value.TotalDays / 365.25;
                    break;
                case Generales.PeriodoPlazos.Meses:
                    dDiff = (request.FechaFin.Value - request.FechaInicio).Value.TotalDays / (365.25 / 12);
                    break;
                case Generales.PeriodoPlazos.Diascalendario:
                    dDiff = (request.FechaFin.Value - request.FechaInicio).Value.TotalDays;
                    break;
            }

            return Json(new { Cantidad = Math.Round(dDiff, 0), Anios = dAnios, Meses = dMeses, Dias = dDias } , JsonRequestBehavior.AllowGet);
        }

        public static double ObtenerDiasLaborales(DateTime dFechaInicio, DateTime dFechaFin)
        {
            double diasCalculados =
                ((dFechaFin - dFechaInicio).TotalDays * 5 -
                (dFechaInicio.DayOfWeek - dFechaFin.DayOfWeek) * 2) / 7;

            if (dFechaFin.DayOfWeek == DayOfWeek.Saturday) diasCalculados--;
            if (dFechaInicio.DayOfWeek == DayOfWeek.Sunday) diasCalculados--;

            return diasCalculados;
        }
        #endregion

        #region "Tab Equipos"
        public ActionResult ObtenerInformacionEquipo()
        {
            return View("_InfoEquipo");
        }
        public JsonResult ListarRecursoProyecto(int iIdProyecto)
        {
            var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            for (int i = 0; i < perfil.Length; i++)
            {
                CodPerfil = (perfil[i]);
            }

            List<RecursoProyectoDtoResponse> ListaRecursoProyecto = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ListarRecursoProyecto(iIdProyecto));

            if (CodPerfil == Generales.TipoPerfil.PreventaProyecto)
            {
                var jefeProyecto = ListaRecursoProyecto.Find(x => x.IdCargo == Generales.Cargo.JefeProyecto);
                ListaRecursoProyecto.Remove(jefeProyecto);
            }

            return Json(new
            {
                lista = ListaRecursoProyecto,
                NombreUsuarioActual = SessionFacade.Usuario.FirstName,
                EmailUsuarioActual = SessionFacade.Usuario.Email
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GuardarRecursoProyecto(List<RecursoProyectoDtoRequest> request)
        {
            int? idProyecto = 0;
            foreach (RecursoProyectoDtoRequest item in request)
            {
                if (item.Modificado)
                {
                    item.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
                    item.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
                    agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.InsertarRecursoProyecto(item));
                }

                idProyecto = item.IdProyecto;

                if (item.IdRol == 7)
                {
                    // Cambiar de estado a AF
                    EtapaProyectoDtoRequest rqEtapa = new EtapaProyectoDtoRequest()
                    {
                        IdProyecto = idProyecto,
                        IdFase = Configuracion.IdFasePreImplantacion,
                        IdEtapa = Configuracion.IdEtapaAF,
                        IdEstadoEtapa = Generales.ProyectoControlEstado.Registrado,
                        Observaciones = "",
                        IdUsuarioCreacion = SessionFacade.Usuario.UserId
                    };

                    var respuesta = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.RegistrarEtapaEstado(rqEtapa));
                }
            }

            //Solo Pre-venta
            var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            for (int i = 0; i < perfil.Length; i++)
            {
                CodPerfil = (perfil[i]);
            }

            if (CodPerfil == Generales.TipoPerfil.PreventaProyecto)
            {
                var cargo = new RecursoDtoRequest() { IdCargo = 5 };
                var listaRecursoXCargo = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarRecursoPorCargo(cargo));
                var idRecurso = listaRecursoXCargo.Find(x => x.IdUsuarioRais == SessionFacade.Usuario.UserId).IdRecurso;
                RecursoProyectoDtoRequest rqPreventa = new RecursoProyectoDtoRequest()
                {
                    IdRecurso = idRecurso,
                    IdRol = 5,
                    IdUsuarioCreacion = SessionFacade.Usuario.UserId,
                    IdUsuarioEdicion = SessionFacade.Usuario.UserId,
                    IdProyecto = idProyecto,
                };
                var actualizar = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ObtenerRecursoPorProyectoRol(rqPreventa));
                if (actualizar == null)
                {
                    agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.InsertarRecursoProyecto(rqPreventa));
                }
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Tab Documentacion"
        public ActionResult ObtenerInformacionDocumentacion()
        {
            return View("_InfoDocumentacion");
        }

        [HttpPost]
        public ActionResult GuardarDocumentacionProyecto()
        {
            //Request["IdTipoDocumento"]
            if (Request.Files.Count > 0)
            {
                string[] sIdTipoDocumento = Request["IdTipoDocumento"].Split(',');
                int IdProyecto = Convert.ToInt32(Request["IdProyecto"]);

                try
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        HttpPostedFileBase file = Request.Files[i];

                        using (Stream fs = file.InputStream)
                        {
                            using (BinaryReader br = new BinaryReader(fs))
                            {
                                byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                DocumentacionProyectoDtoRequest request = new DocumentacionProyectoDtoRequest();
                                request.ArchivoAdjunto = bytes;
                                request.NombreArchivo = file.FileName;
                                request.IdProyecto = IdProyecto;
                                request.IdDocumento = Convert.ToInt32(sIdTipoDocumento[i]);
                                request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
                                request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;

                                agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.InsertarDocumentacionProyecto(request));
                            }
                        }
                    }

                    // Cambiar de estado a Lider
                    EtapaProyectoDtoRequest rqEtapa = new EtapaProyectoDtoRequest()
                    {
                        IdProyecto = IdProyecto,
                        IdFase = Configuracion.IdFasePreImplantacion,
                        IdEtapa = Configuracion.IdEtapaLiderJP,
                        IdEstadoEtapa = Generales.ProyectoControlEstado.Registrado,
                        Observaciones = "",
                        IdUsuarioCreacion = SessionFacade.Usuario.UserId
                    };

                    var respuesta = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.RegistrarEtapaEstado(rqEtapa));
                    return Json(respuesta, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { TipoRespuesta = Proceso.Invalido, Mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else {
                return Json(new { TipoRespuesta = Proceso.Valido, Mensaje = "Grabado con éxito." }, JsonRequestBehavior.AllowGet);
            }
        }

        private static byte[] GetFileBytes(HttpPostedFileBase file)
        {
            var streamLength = file.InputStream.Length;
            var imageBytes = new byte[streamLength];

            file.InputStream.Read(imageBytes, 0, imageBytes.Length);
            return imageBytes;
        }

        [HttpPost]
        public JsonResult ListarDocumentacionProyecto(int iIdProyecto)
        {
            List<DocumentacionProyectoDtoResponse> ListaDocumentacionProyecto = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ListarDocumentacionProyecto(iIdProyecto));
            return Json(ListaDocumentacionProyecto, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EliminarDocumentacionProyecto(DocumentacionProyectoDtoRequest request)
        {
            agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.EliminarDocumentacionProyecto(request));
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        public FileResult DescargarDocumentacionArchivo(int iIdDocumentoArchivo)
        {
            DocumentacionArchivoDtoResponse oDocumentacionArchivo = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ObtenerDocumentacionArchivo(iIdDocumentoArchivo));
            string sArchivo = Convert.ToBase64String(oDocumentacionArchivo.ArchivoAdjunto);

            return File(oDocumentacionArchivo.ArchivoAdjunto, "application/force-download", oDocumentacionArchivo.NombreArchivo);
        }

        #endregion

        #region "Tab Plantilla financiera"
        public ActionResult ObtenerInformacionFinanciera()
        {
            return View("_infoFinanciera");
        }

        public ActionResult ObtenerInformacionFinancieraPagos()
        {
            return View("_infoFinancieraPagos");
        }

        public ActionResult ObtenerInformacionFinancieraIndicador()
        {
            return View("_infoFinancieraIndicador");
        }

        public ActionResult ObtenerInformacionFinancieraLineas()
        {
            return View("_infoFinancieraLineas");
        }

        public ActionResult ObtenerInformacionFinancieraCostos()
        {
            return View("_infoFinancieraCosto");
        }

        public ActionResult ObtenerInformacionFinancieraCostosConcepto()
        {
            return View("_infoFinancieraCostoConcepto");
        }

        public ActionResult ObtenerInformacionFinancieraServicios()
        {
            return View("_infoFinancieraServicios");
        }

        
        [HttpPost]
        public ActionResult GuardarArchivoFinanciero()
        {
            if (Request.Files.Count > 0)
            {
                int iTipo = Convert.ToInt32(Request["tipoArchivo"]);
                int IdProyecto = Convert.ToInt32(Request["IdProyecto"]);

                HttpPostedFileBase file = Request.Files[0];


                string directorio_virtual = ConfigurationManager.AppSettings["RutaTemporal"];
                if (!Directory.Exists(directorio_virtual))
                {
                    Directory.CreateDirectory(directorio_virtual);
                }

                if (file.ContentLength > 0)
                {
                    DataTable dt = new DataTable();
                    var fileName = Path.GetFileName(file.FileName);
                    var sFileLocation = Path.Combine(directorio_virtual, fileName);
                    var sExtension = Path.GetExtension(sFileLocation);
                    try
                    {
                        if (System.IO.File.Exists(sFileLocation))
                        {
                            System.IO.File.SetAttributes(sFileLocation, FileAttributes.Normal);
                            System.IO.File.Delete(sFileLocation);
                        }

                        file.SaveAs(sFileLocation);

                        switch (sExtension.ToLower())
                        {
                            case ".xlsx":
                                dt = ExcelToDataTable.ReadExcelXLSXToDataTable(sFileLocation, "", iTipo);
                                break;
                        }
                        ProcesoResponse resultadoStage = null;
                        int iFila = 1;
                        foreach (DataRow fila in dt.Rows)
                        {
                            DetalleFinancieroDtoRequest request = new DetalleFinancieroDtoRequest();
                            request = GenerarEntidadConsolidado(request, fila, iTipo);
                            request.Fila = iFila;
                            request.IdProyecto = IdProyecto;
                            if (iTipo == 1)
                            {
                                resultadoStage = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.RegistrarStageFinanciero(request));
                            }
                            else
                            {
                                if (fila[0].ToString() != string.Empty)
                                {
                                    resultadoStage = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.RegistrarStageFinanciero(request));
                                }
                            }
                            iFila++;
                        }

                        if (resultadoStage != null)
                        {
                            if (resultadoStage.TipoRespuesta == 0 && iTipo != 1)
                            {
                                var proyectoDto = new ProyectoDtoRequest() {
                                    IdProyecto = IdProyecto,
                                    IdUsuarioCreacion = SessionFacade.Usuario.UserId
                                };

                                var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ProcesarArchivosFinancieros(proyectoDto));

                                //Validar por lo menos una linea con OPEX > 0
                                
                                var detalleFinanciero = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.BuscarDetalleFinancieroPorProyecto(proyectoDto));
                                var cantidadOpexEncontrados = detalleFinanciero.Count();

                                EtapaProyectoDtoRequest rqEtapa = null;

                                if (cantidadOpexEncontrados > 0)
                                {
                                    // Cambiar de estado a Control
                                    rqEtapa = new EtapaProyectoDtoRequest()
                                    {
                                        IdProyecto = IdProyecto,
                                        IdFase = Configuracion.IdFasePreImplantacion,
                                        IdEtapa = Configuracion.IdEtapaControl,
                                        IdEstadoEtapa = Generales.ProyectoControlEstado.Registrado,
                                        Observaciones = "",
                                        IdUsuarioCreacion = SessionFacade.Usuario.UserId
                                    };
                                }
                                else
                                {
                                    // Cambiar de estado a JP
                                    rqEtapa = new EtapaProyectoDtoRequest()
                                    {
                                        IdProyecto = IdProyecto,
                                        IdFase = Configuracion.IdFasePreImplantacion,
                                        IdEtapa = Configuracion.IdEtapaJP,
                                        IdEstadoEtapa = Generales.ProyectoControlEstado.Registrado,
                                        Observaciones = "SIN_CONTROL",
                                        IdUsuarioCreacion = SessionFacade.Usuario.UserId
                                    };
                                }

                                var respuesta = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.RegistrarEtapaEstado(rqEtapa));

                                if (System.IO.File.Exists(sFileLocation))
                                {
                                    System.IO.File.SetAttributes(sFileLocation, FileAttributes.Normal);
                                    System.IO.File.Delete(sFileLocation);
                                }

                                return Json(resultado, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                if (System.IO.File.Exists(sFileLocation))
                                {
                                    System.IO.File.SetAttributes(sFileLocation, FileAttributes.Normal);
                                    System.IO.File.Delete(sFileLocation);
                                }
                            }
                        }

                        return Json(resultadoStage, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        return Json(new { TipoRespuesta = "1", Mensaje = ex.Message } , JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return View();
        }

        [HttpPost]
        public JsonResult ListarPagos(int iIdProyecto)
        {
            var request = new ProyectoDtoRequest() { IdProyecto = iIdProyecto };
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ListarPagos(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarIndicadores(int iIdProyecto)
        {
            var request = new ProyectoDtoRequest() { IdProyecto = iIdProyecto };
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ListarIndicadores(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarDetalleFinanciero(int iIdProyecto)
        {
            var request = new ProyectoDtoRequest() { IdProyecto = iIdProyecto };
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ListarDetalleFinanciero(iIdProyecto));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarCostos(int iIdProyecto)
        {
            var request = new ProyectoDtoRequest() { IdProyecto = iIdProyecto };
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ListarCostos(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarCostosOpex(int iIdProyecto)
        {
            var request = new ProyectoDtoRequest() { IdProyecto = iIdProyecto };
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ListarCostosOpex(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarCostosCapex(int iIdProyecto)
        {
            var request = new ProyectoDtoRequest() { IdProyecto = iIdProyecto };
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ListarCostosCapex(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarServiciosCMI(int iIdProyecto)
        {
            var request = new ProyectoDtoRequest() { IdProyecto = iIdProyecto };
            var resultado = agenteServicioProyectoSDio.InvocarFuncionAsync(o => o.ListarServiciosCMI(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        private DetalleFinancieroDtoRequest GenerarEntidadConsolidado(DetalleFinancieroDtoRequest request, DataRow fila, int iTipo)
        {
            if (iTipo == 1)
            {
                request.Item = fila[0].ToString();
                request.NumeroDeOportunidad = fila[1].ToString();
                request.NroDeCaso = fila[2].ToString();
                request.TipoDeProyecto = fila[3].ToString();
                request.PagoUnico = fila[4].ToString();
                request.PagoRecurrente = fila[5].ToString();
                request.NroDeMesesDePagoRecurrente = fila[6].ToString();
                request.PagoTotal = fila[7].ToString();
                request.TipoDeCambio = fila[8].ToString();
                request.MargenOibda = fila[9].ToString();
                request.Van = fila[10].ToString();
                request.VanVai = fila[11].ToString();
                request.Oibda = fila[12].ToString();
                request.PaybackMeses = fila[13].ToString();
                request.AnalistaFinancieroEvaluador = fila[14].ToString();
                request.AnalistaFinancieroUsuarioLotus = fila[15].ToString();
                request.AnalistaFinancieroCorreoOutlook = fila[16].ToString();
                request.ImporteDeVentasUss = fila[17].ToString();
                request.CostosOpex = fila[18].ToString();
                request.Capex = fila[19].ToString();
                request.AntenasVsatCapexCapexValorResidual = fila[20].ToString();
                request.AntenasVsatCapexLogInversa = fila[21].ToString();
                request.ClearChannelCapexCapexValorResidual = fila[22].ToString();
                request.ClearChannelCapexLogInversa = fila[23].ToString();
                request.EquiposDeSeguridadCapexCapexValorResidual = fila[24].ToString();
                request.EquiposDeSeguridadCapexLogInversa = fila[25].ToString();
                request.EquiposEeEeCapexCapexValorResidual = fila[26].ToString();
                request.EstudiosEspecialesCapexValorResidual = fila[27].ToString();
                request.GabinetesCapexCapexValorResidual = fila[28].ToString();
                request.GabinetesCapexLogInversa = fila[29].ToString();
                request.HardwareCapexCapexValorResidual = fila[30].ToString();
                request.HardwareCapexLogInversa = fila[31].ToString();
                request.InstalacionEquiposYLicenciasCchnCapexValorResidual = fila[32].ToString();
                request.InstalacionEquiposYLicenciasCchnCapexLogInversa = fila[33].ToString();
                request.InstalacionEquiposYLicenciasVsatCapexValorResidual = fila[34].ToString();
                request.InstalacionEquiposYLicenciasVsatCapexLogInversa = fila[35].ToString();
                request.InstalacionesRoutersCapexValorResidual = fila[36].ToString();
                request.InstalacionesRoutersLogInversa = fila[37].ToString();
                request.Inversion35GhzCapexValorResidual = fila[38].ToString();
                request.Inversion35GhzLogInversa = fila[39].ToString();
                request.ModemsCapexCapexValorResidual = fila[40].ToString();
                request.ModemsCapexLogInversa = fila[41].ToString();
                request.RoutersCapexCapexValorResidual = fila[42].ToString();
                request.RoutersCapexLogInversa = fila[43].ToString();
                request.SmartvpnCapexCapexValorResidual = fila[44].ToString();
                request.SoftwareCapexCapexValorResidual = fila[45].ToString();
                request.SolarwindCapexCapexValorResidual = fila[46].ToString();
                request.ComisionComercial = fila[47].ToString();
                request.ContingenciaDeCostos = fila[48].ToString();
                request.CostosCambium = fila[49].ToString();
                request.CostosCircuitos = fila[50].ToString();
                request.CostosInternosDatacenter = fila[51].ToString();
                request.CostosInternosOutsourcingDesktop = fila[52].ToString();
                request.CostosInternosOutsourcingImpresion = fila[53].ToString();
                request.InstalacionEquiposYLicenciasOpex = fila[54].ToString();
                request.OtrosCostosIndirectos = fila[55].ToString();
                request.PersonalPropioTelefonica = fila[56].ToString();
                request.PersonalPropioTelefonicaMantSop = fila[57].ToString();
                request.SegmentoSatelital = fila[58].ToString();
                request.SoporteYMttoEqTelecomunicaciones = fila[59].ToString();
                request.Telecomunicaciones = fila[60].ToString();
                request.Telecomunicaciones35Ghz = fila[61].ToString();
                request.TelecomunicacionesSmartvpn = fila[62].ToString();
                request.TelecomunicacionesSolarwin = fila[63].ToString();
                request.ViaticosYGastosDePersonalTdp = fila[64].ToString();
                request.AntenasVsatCapexNoStock = fila[65].ToString();
                request.AntenasVsatCapexStock = fila[66].ToString();
                request.ClearChannelCapexNoStock = fila[67].ToString();
                request.ClearChannelCapexStock = fila[68].ToString();
                request.EquiposDeSeguridadCapexNoStock = fila[69].ToString();
                request.EquiposDeSeguridadCapexStock = fila[70].ToString();
                request.EquiposEeEeCapexEeEe = fila[71].ToString();
                request.EstudiosEspecialesEeEe = fila[72].ToString();
                request.GabinetesCapexNoStock = fila[73].ToString();
                request.HardwareCapexNoStock = fila[74].ToString();
                request.InstalacionEquiposYLicenciasCchnNoStock = fila[75].ToString();
                request.InstalacionEquiposYLicenciasCchnStock = fila[76].ToString();
                request.InstalacionEquiposYLicenciasVsatNoStock = fila[77].ToString();
                request.InstalacionEquiposYLicenciasVsatStock = fila[78].ToString();
                request.InstalacionesRoutersNoStock = fila[79].ToString();
                request.InstalacionesRoutersStock = fila[80].ToString();
                request.Inversion35GhzNoStock = fila[81].ToString();
                request.Inversion35GhzStock = fila[82].ToString();
                request.ModemsCapexNoStock = fila[83].ToString();
                request.ModemsCapexStock = fila[84].ToString();
                request.RoutersCapexNoStock = fila[85].ToString();
                request.RoutersCapexStock = fila[86].ToString();
                request.SmartvpnCapexNoStock = fila[87].ToString();
                request.SoftwareCapexNoStock = fila[88].ToString();
                request.SolarwindCapexNoStock = fila[89].ToString();
                request.CableadoYSuministrosDc = fila[90].ToString();
                request.CableadoYSuministrosPe = fila[91].ToString();
                request.ComisionPartnerDc = fila[92].ToString();
                request.ComisionPartnerPe = fila[93].ToString();
                request.GestionDeProyectosDt = fila[94].ToString();
                request.GestionDeProyectosPe = fila[95].ToString();
                request.HardwareCostoDeVentas = fila[96].ToString();
                request.InstalacionEntregaConfiguracionYTransporteDc = fila[97].ToString();
                request.InstalacionEntregaConfiguracionYTransportePe = fila[98].ToString();
                request.MesaDeAyuda = fila[99].ToString();
                request.Penalidades = fila[100].ToString();
                request.PersonalTercerosDatacenter = fila[101].ToString();
                request.PersonalTercerosOutsourcingDeImpresion = fila[102].ToString();
                request.PersonalTercerosRenovacionTecnologica = fila[103].ToString();
                request.PersonalTercerosServiciosTransaccionales = fila[104].ToString();
                request.PlataformaTraficoSeguro = fila[105].ToString();
                request.RentingDeEquiposCcuuYSeguridad = fila[106].ToString();
                request.RentingDeImpresoras = fila[107].ToString();
                request.RentingDeLicencias = fila[108].ToString();
                request.RentingDePcsLaptopsYAccesorios = fila[109].ToString();
                request.RentingDeServidoresYAccesorios = fila[110].ToString();
                request.SalidaInternet = fila[111].ToString();
                request.SegmentoSatelitalIntenet = fila[112].ToString();
                request.SegurosDeEquipos = fila[113].ToString();
                request.ServiciosCloud = fila[114].ToString();
                request.ServiciosDeCapacitacionDc = fila[115].ToString();
                request.ServiciosDeCapacitacionPe = fila[116].ToString();
                request.ServicioDeDisponibilidadTecnologica = fila[117].ToString();
                request.ServiciosParaOutsourcingDeImpresion = fila[118].ToString();
                request.ServiciosParaRenovacionTecnologica = fila[119].ToString();
                request.Simcard = fila[120].ToString();
                request.SoftwareCostosDeVentas = fila[121].ToString();
                request.SoporteYMantenimientoCcuuCentrales = fila[122].ToString();
                request.SoporteYMantenimientoCcuuHwSw = fila[123].ToString();
                request.SoporteYMantenimientoCcuuSsp = fila[124].ToString();
                request.SoporteYMantenimientoTi = fila[125].ToString();
                request.SuministrosDeBackup = fila[126].ToString();
                request.TelecomunicacionesTerceros = fila[127].ToString();
                request.TerminalesAlquiler = fila[128].ToString();
                request.TerminalesVenta = fila[129].ToString();
                request.Tributos = fila[130].ToString();
                request.ViaticosYGastosDePersonalDc = fila[131].ToString();
                request.ViaticosYGastosDePersonalPe = fila[132].ToString();


                request.Linea = string.Empty;
                request.Sublinea = string.Empty;
                request.Servicio = string.Empty;
                request.Producto = string.Empty;
                request.ProductoAf = string.Empty;
                request.AnalistaControlGastosUsuarioLotus = string.Empty;
                request.AnalistaControlGastosCorreoOutlook = string.Empty;
                request.Porcentaje = string.Empty;
                request.CeCo = string.Empty;
            }
            else
            {
                request.Item = fila[0].ToString();
                request.NumeroDeOportunidad = fila[1].ToString();
                request.Linea = fila[2].ToString();
                request.Sublinea = fila[3].ToString();
                request.Servicio = fila[4].ToString();
                request.Producto = fila[5].ToString();
                request.ProductoAf = fila[6].ToString();
                request.AnalistaControlGastosUsuarioLotus = fila[7].ToString();
                request.AnalistaControlGastosCorreoOutlook = fila[8].ToString();
                request.Porcentaje = fila[9].ToString();
                request.PagoUnico = fila[10].ToString();
                request.PagoRecurrente = fila[11].ToString();
                request.NroDeMesesDePagoRecurrente = fila[12].ToString();
                request.PagoTotal = fila[13].ToString();
                request.TipoDeCambio = fila[14].ToString();
                request.MargenOibda = fila[15].ToString();
                request.Van = fila[16].ToString();
                request.VanVai = fila[17].ToString();
                request.Oibda = fila[18].ToString();
                request.PaybackMeses = fila[19].ToString();
                request.AnalistaFinancieroEvaluador = fila[20].ToString();
                request.AnalistaFinancieroUsuarioLotus = fila[21].ToString();
                request.AnalistaFinancieroCorreoOutlook = fila[22].ToString();
                request.ImporteDeVentasUss = fila[23].ToString();
                request.CostosOpex = fila[24].ToString();
                request.Capex = fila[25].ToString();
                request.CeCo = fila[26].ToString();
                request.AntenasVsatCapexCapexValorResidual = fila[27].ToString();
                request.AntenasVsatCapexLogInversa = fila[28].ToString();
                request.ClearChannelCapexCapexValorResidual = fila[29].ToString();
                request.ClearChannelCapexLogInversa = fila[30].ToString();
                request.EquiposDeSeguridadCapexCapexValorResidual = fila[31].ToString();
                request.EquiposDeSeguridadCapexLogInversa = fila[32].ToString();
                request.EquiposEeEeCapexCapexValorResidual = fila[33].ToString();
                request.EstudiosEspecialesCapexValorResidual = fila[34].ToString();
                request.GabinetesCapexCapexValorResidual = fila[35].ToString();
                request.GabinetesCapexLogInversa = fila[36].ToString();
                request.HardwareCapexCapexValorResidual = fila[37].ToString();
                request.HardwareCapexLogInversa = fila[38].ToString();
                request.InstalacionEquiposYLicenciasCchnCapexValorResidual = fila[39].ToString();
                request.InstalacionEquiposYLicenciasCchnCapexLogInversa = fila[40].ToString();
                request.InstalacionEquiposYLicenciasVsatCapexValorResidual = fila[41].ToString();
                request.InstalacionEquiposYLicenciasVsatCapexLogInversa = fila[42].ToString();
                request.InstalacionesRoutersCapexValorResidual = fila[43].ToString();
                request.InstalacionesRoutersLogInversa = fila[44].ToString();
                request.Inversion35GhzCapexValorResidual = fila[45].ToString();
                request.Inversion35GhzLogInversa = fila[46].ToString();
                request.ModemsCapexCapexValorResidual = fila[47].ToString();
                request.ModemsCapexLogInversa = fila[48].ToString();
                request.RoutersCapexCapexValorResidual = fila[49].ToString();
                request.RoutersCapexLogInversa = fila[50].ToString();
                request.SmartvpnCapexCapexValorResidual = fila[51].ToString();
                request.SoftwareCapexCapexValorResidual = fila[52].ToString();
                request.SolarwindCapexCapexValorResidual = fila[53].ToString();
                request.ComisionComercial = fila[54].ToString();
                request.ContingenciaDeCostos = fila[55].ToString();
                request.CostosCambium = fila[56].ToString();
                request.CostosCircuitos = fila[57].ToString();
                request.CostosInternosDatacenter = fila[58].ToString();
                request.CostosInternosOutsourcingDesktop = fila[59].ToString();
                request.CostosInternosOutsourcingImpresion = fila[60].ToString();
                request.InstalacionEquiposYLicenciasOpex = fila[61].ToString();
                request.OtrosCostosIndirectos = fila[62].ToString();
                request.PersonalPropioTelefonica = fila[63].ToString();
                request.PersonalPropioTelefonicaMantSop = fila[64].ToString();
                request.SegmentoSatelital = fila[65].ToString();
                request.SoporteYMttoEqTelecomunicaciones = fila[66].ToString();
                request.Telecomunicaciones = fila[67].ToString();
                request.Telecomunicaciones35Ghz = fila[68].ToString();
                request.TelecomunicacionesSmartvpn = fila[69].ToString();
                request.TelecomunicacionesSolarwin = fila[70].ToString();
                request.ViaticosYGastosDePersonalTdp = fila[71].ToString();
                request.AntenasVsatCapexNoStock = fila[72].ToString();
                request.AntenasVsatCapexStock = fila[73].ToString();
                request.ClearChannelCapexNoStock = fila[74].ToString();
                request.ClearChannelCapexStock = fila[75].ToString();
                request.EquiposDeSeguridadCapexNoStock = fila[76].ToString();
                request.EquiposDeSeguridadCapexStock = fila[77].ToString();
                request.EquiposEeEeCapexEeEe = fila[78].ToString();
                request.EstudiosEspecialesEeEe = fila[79].ToString();
                request.GabinetesCapexNoStock = fila[80].ToString();
                request.HardwareCapexNoStock = fila[81].ToString();
                request.InstalacionEquiposYLicenciasCchnNoStock = fila[82].ToString();
                request.InstalacionEquiposYLicenciasCchnStock = fila[83].ToString();
                request.InstalacionEquiposYLicenciasVsatNoStock = fila[84].ToString();
                request.InstalacionEquiposYLicenciasVsatStock = fila[85].ToString();
                request.InstalacionesRoutersNoStock = fila[86].ToString();
                request.InstalacionesRoutersStock = fila[87].ToString();
                request.Inversion35GhzNoStock = fila[88].ToString();
                request.Inversion35GhzStock = fila[89].ToString();
                request.ModemsCapexNoStock = fila[90].ToString();
                request.ModemsCapexStock = fila[91].ToString();
                request.RoutersCapexNoStock = fila[92].ToString();
                request.RoutersCapexStock = fila[93].ToString();
                request.SmartvpnCapexNoStock = fila[94].ToString();
                request.SoftwareCapexNoStock = fila[95].ToString();
                request.SolarwindCapexNoStock = fila[96].ToString();
                request.CableadoYSuministrosDc = fila[97].ToString();
                request.CableadoYSuministrosPe = fila[98].ToString();
                request.ComisionPartnerDc = fila[99].ToString();
                request.ComisionPartnerPe = fila[100].ToString();
                request.GestionDeProyectosDt = fila[101].ToString();
                request.GestionDeProyectosPe = fila[102].ToString();
                request.HardwareCostoDeVentas = fila[103].ToString();
                request.InstalacionEntregaConfiguracionYTransporteDc = fila[104].ToString();
                request.InstalacionEntregaConfiguracionYTransportePe = fila[105].ToString();
                request.MesaDeAyuda = fila[106].ToString();
                request.Penalidades = fila[107].ToString();
                request.PersonalTercerosDatacenter = fila[108].ToString();
                request.PersonalTercerosOutsourcingDeImpresion = fila[109].ToString();
                request.PersonalTercerosRenovacionTecnologica = fila[110].ToString();
                request.PersonalTercerosServiciosTransaccionales = fila[111].ToString();
                request.PlataformaTraficoSeguro = fila[112].ToString();
                request.RentingDeEquiposCcuuYSeguridad = fila[113].ToString();
                request.RentingDeImpresoras = fila[114].ToString();
                request.RentingDeLicencias = fila[115].ToString();
                request.RentingDePcsLaptopsYAccesorios = fila[116].ToString();
                request.RentingDeServidoresYAccesorios = fila[117].ToString();
                request.SalidaInternet = fila[118].ToString();
                request.SegmentoSatelitalIntenet = fila[119].ToString();
                request.SegurosDeEquipos = fila[120].ToString();
                request.ServiciosCloud = fila[121].ToString();
                request.ServiciosDeCapacitacionDc = fila[122].ToString();
                request.ServiciosDeCapacitacionPe = fila[123].ToString();
                request.ServicioDeDisponibilidadTecnologica = fila[124].ToString();
                request.ServiciosParaOutsourcingDeImpresion = fila[125].ToString();
                request.ServiciosParaRenovacionTecnologica = fila[126].ToString();
                request.Simcard = fila[127].ToString();
                request.SoftwareCostosDeVentas = fila[128].ToString();
                request.SoporteYMantenimientoCcuuCentrales = fila[129].ToString();
                request.SoporteYMantenimientoCcuuHwSw = fila[130].ToString();
                request.SoporteYMantenimientoCcuuSsp = fila[131].ToString();
                request.SoporteYMantenimientoTi = fila[132].ToString();
                request.SuministrosDeBackup = fila[133].ToString();
                request.TelecomunicacionesTerceros = fila[134].ToString();
                request.TerminalesAlquiler = fila[135].ToString();
                request.TerminalesVenta = fila[136].ToString();
                request.Tributos = fila[137].ToString();
                request.ViaticosYGastosDePersonalDc = fila[138].ToString();
                request.ViaticosYGastosDePersonalPe = fila[139].ToString();

                request.NroDeCaso = string.Empty;
                request.TipoDeProyecto = string.Empty;

            }
            request.TipoTabla = iTipo;
            return request;
        }

        #endregion

    }
}