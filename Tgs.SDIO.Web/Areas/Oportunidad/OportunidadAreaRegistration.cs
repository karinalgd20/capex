﻿using System.Web.Mvc;

namespace Tgs.SDIO.Web.Areas.Oportunidad
{
    public class OportunidadAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Oportunidad";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Oportunidad_default",
                "Oportunidad/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}