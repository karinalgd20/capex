﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class RegistrarOportunidadController : Controller
    {
        // GET: Oportunidad/RegistrarOportunidad

        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public RegistrarOportunidadController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }



        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {

            return View();

        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult Preventa()
        {
            var IdOportunidadSession = SessionFacade.IdOportunidad;
            var Preventa = "";

            if (IdOportunidadSession == 0)
            {
              Preventa = SessionFacade.Usuario.FirstName;
            }

            else
            {
                OportunidadDtoRequest objOportunidad = new OportunidadDtoRequest();

                objOportunidad.IdOportunidad = IdOportunidadSession;

              var  responseOportunidad = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidad(objOportunidad));

                var IdUsuario = Convert.ToInt32( responseOportunidad.IdPreVenta);


             var responseUsuario = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuarioPorId(IdUsuario));

                Preventa = responseUsuario.NombresCompletos;
            }


            return Json(Preventa, JsonRequestBehavior.AllowGet);
        }
        


        [HttpPost]
        [CustomAuthorize]
        public JsonResult ObtenerOportunidadFlujoEstadoId(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {

            var estadoOportunidad = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadFlujoEstadoId(oportunidadFlujoEstado));



            return Json(estadoOportunidad, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ObtenerOportunidad(OportunidadDtoRequest oportunidadDtoRequest)
        {

            var estadoOportunidad = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidad(oportunidadDtoRequest));

            return Json(estadoOportunidad, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarUsuariosPorPerfil(UsuarioDtoRequest usuario_Admin)
        {

            string CodSistema = "SDIO";
            int IdEmpresa = 1;
            usuario_Admin.CodigoSistema = CodSistema;
            usuario_Admin.IdEmpresa = IdEmpresa;
            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));


        
            List<ListaDtoResponse> Lista = new List<ListaDtoResponse>();

            foreach (var part in resultado)
            {
                Lista.Add(new ListaDtoResponse()
                {
                    Codigo = part.IdUsuario.ToString(),
                    Descripcion = part.NombresCompletos
                });

            }


            return Json(Lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadFlujoEstado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarOportunidad(OportunidadDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.IdPreVenta = SessionFacade.Usuario.UserId;
            var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            CodPerfil = SessionFacade.PerfilSesion;       
            request.CodPerfil = CodPerfil;
            DateTime fecha = DateTime.Now;
            request.FechaCreacion = fecha;
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidad(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarOportunidad(OportunidadDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
           // var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            CodPerfil = SessionFacade.PerfilSesion;
            request.CodPerfil = CodPerfil;
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidad(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult CargarPerfil(UsuarioDtoRequest usuario)

        {
  

            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarOpcionesUsuario(usuario.IdEmpresa));



            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;


            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadLineaNegocio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;


            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidadLineaNegocio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ObtenerOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest request)
        {

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadLineaNegocio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest request)
        {

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidadFlujoEstado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult NuevaVersion(OportunidadDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.NuevaVersion(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult InsertarConfiguracionFlujo(OportunidadLineaNegocioDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.IdEstado = 1;

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.InsertarConfiguracionFlujo(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}