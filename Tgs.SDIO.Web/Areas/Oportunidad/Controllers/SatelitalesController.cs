﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class SatelitalesController : Controller
    {
        // GET: Oportunidad/Satelitales
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public SatelitalesController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        // GET: Oportunidad/SmartVpn
        public ActionResult Index(ConceptoDatosCapexDtoRequest dto)
        {
            return View();
        }

        public JsonResult CantidadSatelitales(SubServicioDatosCapexDtoRequest dto)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.CantidadSatelitales(dto));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}