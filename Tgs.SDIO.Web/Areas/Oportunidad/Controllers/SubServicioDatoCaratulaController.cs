﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class SubServicioDatoCaratulaController : BaseController
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportunidadSDio = null;
        DateTime fecha = DateTime.Now;
        private object lockObj = new object();

        public SubServicioDatoCaratulaController(AgenteServicioOportunidadSDio agenteServicioOportunidadSDio)
        {
            this.agenteServicioOportunidadSDio = agenteServicioOportunidadSDio;
        }

        // GET: Oportunidad/SubServicioDatoCaratula
        public ActionResult Index()
        {
            return View();
        }

        //[HttpPost]
        //public JsonResult RegistrarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest caratula)
        //{
        //    caratula.IdEstado = Estados.Activo;
        //    caratula.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
        //    caratula.FechaCreacion = fecha;
        //    var resultado = agenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ActualizarSubServicioDatosCaratula(caratula));
        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult ActualizarSubServicioDatoCaratula(SubServicioDatosCaratulaDtoRequest caratula)
        //{
        //    caratula.IdEstado = Estados.Activo;
        //    caratula.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
        //    caratula.FechaEdicion = fecha;
        //    var resultado = agenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ActualizarSubServicioDatosCaratula(caratula));
        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}


    }
}
