﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class VisitaDetalleController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        public VisitaDetalleController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarVisitaDetalle(VisitaDetalleDtoRequest request)
        {
            request.IdEstado = 1;
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarVisitaDetalle(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult InhabilitarVisitaDetalle(VisitaDetalleDtoRequest request)
        {
            request.IdEstado = 0;
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Now;
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.InhabilitarVisitaDetalle(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarVisitaDetallePaginado(VisitaDetalleDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarVisitaDetallePaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}