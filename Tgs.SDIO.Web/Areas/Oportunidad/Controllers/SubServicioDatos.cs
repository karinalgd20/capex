﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class SubServicioDatosController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public SubServicioDatosController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        
        }
        public ActionResult Index()
        {
            return View();
        }

        //[HttpPost]
        //public JsonResult ListarDetalleOportunidadCaratula2(OportunidadFlujoCajaDtoRequest casonegocio)
        //{
        //    casonegocio.IdEstado = Estados.Activo;
        //    var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarDetalleOportunidadCaratula(casonegocio));
        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult ListarDetalleOportunidadEcapex2(OportunidadFlujoCajaDtoRequest casonegocio)
        //{
        //    casonegocio.IdEstado = Estados.Activo;
        //    var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarDetalleOportunidadEcapex(casonegocio));
        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult ObtenerSubServicioDatos(SubServicioDatosCaratulaDtoRequest dto)
        //{
        //    var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarSubServicioDatoCaratula(dto)).FirstOrDefault();
        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult ActualizarSubServicioDatos(SubServicioDatosCaratulaDtoRequest dto)
        //{
        //    var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarSubServicioDatosCaratula(dto));
        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult EliminarSubServicioDatos(SubServicioDatosCaratulaDtoRequest dto)
        //{
        //    var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.EliminarSubServicioDatoCaratula(dto));
        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}

    }
}