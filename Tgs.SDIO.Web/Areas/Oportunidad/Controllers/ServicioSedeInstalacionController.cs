﻿using System;
using System.Web.Mvc;

using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class ServicioSedeInstalacionController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        public ServicioSedeInstalacionController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
        }

        #region - Transaccionales -
        [HttpPost]
        public JsonResult RegistrarServicioSedeInstalacion(ServicioSedeInstalacionDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarServicioSedeInstalacion(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}