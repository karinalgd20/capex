﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class LineaConceptoCmiController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportunidadSDio = null;

        public LineaConceptoCmiController(AgenteServicioOportunidadSDio AgenteServicioOportunidadSDio) {
            agenteServicioOportunidadSDio = AgenteServicioOportunidadSDio;
        }
        // GET: Oportunidad/LineaConceptoCmi
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarConceptosCmi(LineaConceptoCMIDtoRequest dto)
        {
            var lineaConceptoCMIDtoResponse = agenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ListarConceptosPorLinea(dto));
            return Json(lineaConceptoCMIDtoResponse, JsonRequestBehavior.AllowGet);
        }

    }
}