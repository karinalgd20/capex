﻿using System.Collections.Generic;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class OportunidadContenedorController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public OportunidadContenedorController
            (
            AgenteServicioOportunidadSDio agenteServicioOportinodadSDio,
            AgenteServicioSeguridadSDio agenteServicioSeguridadSDio,
            AgenteServicioComunSDio agenteServicioComunSDio
            )
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        public ActionResult Index(int Id)
        {
           // var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            CodPerfil = SessionFacade.PerfilSesion;
            MaestraDtoRequest maestra = new MaestraDtoRequest();
            maestra.IdEstado = Generales.Numeric.Uno;
            maestra.IdRelacion = Generales.TablaMaestra.EstadoProyectos;
            maestra.Valor2 = CodPerfil.ToUpper();

            var estadoPerfil = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarMaestra(maestra));
            if (Id > 0)
            {
                OportunidadDtoRequest oportunidad = new OportunidadDtoRequest();
                OportunidadLineaNegocioDtoRequest lineaNegocio = new OportunidadLineaNegocioDtoRequest();
                OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado = new OportunidadFlujoEstadoDtoRequest();
                oportunidad.IdOportunidad = Id;

                var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidad(oportunidad));

                resultado.IdLineaNegocio = Generales.LineasProducto.Datos;
                resultado.FechaOportunidad = resultado.Fecha.ToString("dd/MM/yyyy");
                resultado.IdOportunidad = Id;

                lineaNegocio.IdOportunidad = Id;

                oportunidadFlujoEstado.IdOportunidad = Id;

                var resultadoLinea = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadLineaNegocio(lineaNegocio));



                var estadoOportunidad = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadFlujoEstadoId(oportunidadFlujoEstado));


                resultado.IdEstado = estadoOportunidad.IdEstado;


                resultado.IdLineaNegocio = resultadoLinea.IdLineaNegocio;
                resultado.IdOportunidadLineaNegocio = resultadoLinea.IdOportunidadLineaNegocio;
                ViewBag.VbOportunidad = resultado;
                SessionFacade.IdOportunidad = Id;
                TabPorPerfilOportunidadDatos();

            }
            else
            {

                SessionFacade.IdOportunidad = Generales.Numeric.Cero;
                TabPorPerfilOportunidadDatos();

                ViewBag.VbOportunidad = "";
            }
            return View("Contenedor");
        }
        public ActionResult Detalle()
        {
            return View("DetalleContenedor");
        }
        public ActionResult PreImplantacion()
        {
            return View("PreImplantacionContenedor");
        }
        public ActionResult SISEGO()
        {
            return View("SISEGOContenedor");
        }
        public ActionResult Indicadores()
        {
            return View("SISEGOContenedor");
        }
        public JsonResult ListarLineaNegocioPorIdOportunidad(OportunidadLineaNegocioDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarLineaNegocioPorIdOportunidad(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistroIdOportunidadSession(int Id)
        {

            SessionFacade.IdOportunidad = Id;
            var resultado = SessionFacade.IdOportunidad;
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerIdOportunidadSession()
        {

            var resultado = SessionFacade.IdOportunidad;
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult TabPorPerfilOportunidadDatos()
        {
            OportunidadAccesoPerfilDtoResponse objAccesoPerfil = new OportunidadAccesoPerfilDtoResponse();
            List<ListaDtoResponse> ListEstado = new List<ListaDtoResponse>();
            var Id = SessionFacade.IdOportunidad;

          //  var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            CodPerfil = SessionFacade.PerfilSesion;

            objAccesoPerfil.TabResumen = Generales.EstadoLogico.Verdadero;

            objAccesoPerfil.AccionGrabar = Generales.EstadoLogico.Falso;

            objAccesoPerfil.OptionAnalistaFin = Generales.EstadoLogico.Falso;
            objAccesoPerfil.OptionCoordinadorFin = Generales.EstadoLogico.Falso;
            objAccesoPerfil.OptionPreVenta = Generales.EstadoLogico.Falso;
            objAccesoPerfil.EditarDato = Generales.EstadoLogico.Falso;
            objAccesoPerfil.EditarDatoTable = Generales.EstadoLogico.Falso;

            objAccesoPerfil.TabIndicadores = Generales.EstadoLogico.Verdadero;
            objAccesoPerfil.TabCAdicional = Generales.EstadoLogico.Verdadero;

            objAccesoPerfil.TabCapex = Generales.EstadoLogico.Verdadero;
            objAccesoPerfil.TabCaratula = Generales.EstadoLogico.Verdadero;



            objAccesoPerfil.TabOCosto = Generales.EstadoLogico.Verdadero;
            objAccesoPerfil.TabFCFinanciero = Generales.EstadoLogico.Verdadero;
            objAccesoPerfil.TabFCContable = Generales.EstadoLogico.Verdadero;

            objAccesoPerfil.TabDocumento = Generales.EstadoLogico.Verdadero;

            MaestraDtoRequest maestra = new MaestraDtoRequest();
            maestra.IdEstado = Generales.Numeric.Uno;
            maestra.IdRelacion = Generales.TablaMaestra.EstadoProyectos;
            maestra.Valor2 = CodPerfil.ToUpper();

            var estadoPerfil = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarMaestra(maestra));
            if (Id > 0)
            {
                OportunidadDtoRequest oportunidad = new OportunidadDtoRequest();
                OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado = new OportunidadFlujoEstadoDtoRequest();
                oportunidad.IdOportunidad = Id;

                oportunidadFlujoEstado.IdOportunidad = Id;


                var estadoOportunidad = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadFlujoEstadoId(oportunidadFlujoEstado));


                ListEstado = estadoPerfil;
                var IdEstado = estadoOportunidad.IdEstado;

                if (estadoOportunidad.IdEstado == Generales.Estados.EnProceso && CodPerfil != Generales.TipoPerfil.Coordinador_FIN && CodPerfil != Generales.TipoPerfil.Analista_FIN)
                {

                    objAccesoPerfil.OptionPreVenta = Generales.EstadoLogico.Verdadero;
                    //estadoPerfil.RemoveRange(2, 1);
                    estadoPerfil.RemoveRange(2, 1);


                    ListEstado = estadoPerfil;
                    objAccesoPerfil.AccionGrabar = Generales.EstadoLogico.Verdadero;
                    objAccesoPerfil.EditarDato = Generales.EstadoLogico.Verdadero;
                    objAccesoPerfil.EditarDatoTable = Generales.EstadoLogico.Verdadero;

                    objAccesoPerfil.TabIndicadores = Generales.EstadoLogico.Falso;
                    objAccesoPerfil.TabCAdicional = Generales.EstadoLogico.Falso;
                    objAccesoPerfil.TabCapex = Generales.EstadoLogico.Falso;
                    objAccesoPerfil.TabCaratula = Generales.EstadoLogico.Falso;

                }
                if (estadoOportunidad.IdEstado == Generales.Estados.registrado)
                {
                    if (CodPerfil == Generales.TipoPerfil.Coordinador_FIN)
                    {
                        objAccesoPerfil.EditarDato = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.OptionCoordinadorFin = Generales.EstadoLogico.Verdadero;
                        ListEstado = estadoPerfil;
                        objAccesoPerfil.AccionGrabar = Generales.EstadoLogico.Verdadero;


                        objAccesoPerfil.TabIndicadores = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabCAdicional = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabCapex = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabCaratula = Generales.EstadoLogico.Falso;

                    }
                    else
                    {

                        objAccesoPerfil.TabIndicadores = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabCAdicional = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabCapex = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabCaratula = Generales.EstadoLogico.Falso;

                    }


                }
                if (estadoOportunidad.IdEstado == Generales.Estados.EvaluacionEconomica)
                {

                    if (CodPerfil == Generales.TipoPerfil.Analista_FIN)
                    {

                        objAccesoPerfil.EditarDato = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.OptionAnalistaFin = Generales.EstadoLogico.Verdadero;
                        ListEstado = estadoPerfil;
                        objAccesoPerfil.AccionGrabar = Generales.EstadoLogico.Verdadero;

                        objAccesoPerfil.TabOCosto = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabFCFinanciero = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabFCContable = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabCAdicional = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabIndicadores = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabCapex = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabCaratula = Generales.EstadoLogico.Falso;

                    }
                    else
                    {

                        objAccesoPerfil.TabIndicadores = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabCAdicional = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabCapex = Generales.EstadoLogico.Falso;
                        objAccesoPerfil.TabCaratula = Generales.EstadoLogico.Falso;

                    }

                }
                if (estadoOportunidad.IdEstado == Generales.Estados.Aprobado && CodPerfil != Generales.TipoPerfil.Coordinador_FIN & CodPerfil != Generales.TipoPerfil.Analista_FIN)
                {
                    // estadoPerfil = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarMaestraPorIdRelacion(maestra));
                    estadoPerfil.RemoveRange(0, 2);
                    ListEstado = estadoPerfil;
                    objAccesoPerfil.AccionGrabar = Generales.EstadoLogico.Verdadero;
                    objAccesoPerfil.EditarDato = Generales.EstadoLogico.Falso;
                    ///  resultado.IdEstado = Generales.Numeric.NegativoUno;

                    IdEstado = estadoOportunidad.IdEstado;

                }
                if (estadoOportunidad.IdEstado == Generales.Estados.Rechazado && CodPerfil != Generales.TipoPerfil.Coordinador_FIN && CodPerfil != Generales.TipoPerfil.Analista_FIN)
                {
                    estadoPerfil = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarMaestraPorIdRelacion(maestra));
                    //   estadoPerfil.RemoveRange(2, 1);
                    ListEstado = estadoPerfil;
                    //resultado.IdEstado = Generales.Numeric.NegativoUno;
                    IdEstado = estadoOportunidad.IdEstado;
                }

            }

            else
            {
                objAccesoPerfil.AccionGrabar = Generales.EstadoLogico.Verdadero;
                objAccesoPerfil.EditarDato = Generales.EstadoLogico.Verdadero;
                objAccesoPerfil.OptionPreVenta = Generales.EstadoLogico.Verdadero;


                estadoPerfil.RemoveRange(2, 1);
                ListEstado = estadoPerfil;
            }


            objAccesoPerfil.ListaAccionPerfil = ListEstado;

            return Json(objAccesoPerfil, JsonRequestBehavior.AllowGet);
        }

        

        
        public JsonResult RegistroIdFlujoCajaSession(int Id)
        {
            SessionFacade.IdFlujoCaja = Id;
            var resultado = SessionFacade.IdFlujoCaja;
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerIdFlujoCajaSession()
        {
            var resultado = SessionFacade.IdFlujoCaja;
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}