﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class PlantillaImplantacionController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        public PlantillaImplantacionController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
        }

        #region - Transaccionales -
        [HttpPost]
        public JsonResult RegistrarPlantillaImplantacion(PlantillaImplantacionDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarPlantillaImplantacion(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InactivarPlantillaImplantacion(PlantillaImplantacionDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.InactivarPlantillaImplantacion(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region - No Transaccionales -
        [HttpPost]
        public JsonResult ListarPlantillaImplantacionPaginado(PlantillaImplantacionDtoRequest filtro)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarPlantillaImplantacionPaginado(filtro));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}