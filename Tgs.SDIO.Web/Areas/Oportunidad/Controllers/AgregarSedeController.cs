﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.Web.Utilitarios;
using System;


namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class AgregarSedeController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        public AgregarSedeController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
        }

        public ActionResult Editar()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarSedeCliente(OportunidadDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarSedeCliente(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerClientePorIdOportunidad(OportunidadDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerClientePorIdOportunidad(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}