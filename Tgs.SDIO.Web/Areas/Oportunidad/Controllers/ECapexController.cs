﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Web.Comun;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class ECapexController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public ECapexController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }
        
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarCapex(ConceptoDatosCapexDtoRequest dto)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarConceptoDatoCapex(dto));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult ModalEstudiosEspeciales(ConceptoDatosCapexDtoRequest dto)
        {
            var resultado = Url.Action("Index", "EstudiosEspeciales", dto);
            return Json(new { Respuesta = new { Mensaje = resultado } });
        }

        public JsonResult CantidadCapex(SubServicioDatosCapexDtoRequest dto)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.CantidadCapex(dto));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListarPestanaGrupoTipo(PestanaGrupoTipoDtoRequest dto)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarPestanaGrupoTipo(dto));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

      
    }
}