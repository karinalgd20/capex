﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class IndicadorController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public IndicadorController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        
        }
        public ActionResult Index()
        {
            return View();
        }

    }
}