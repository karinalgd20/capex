﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class RiesgosController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public RiesgosController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;

        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarSubservicioPaginado(OportunidadFlujoCajaDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarSubservicioPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Editar(SubServicioDtoRequest request)
        {
            return View();
        }

        [HttpPost]
        public JsonResult Obtener(OportunidadFlujoCajaDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerSubServicio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}