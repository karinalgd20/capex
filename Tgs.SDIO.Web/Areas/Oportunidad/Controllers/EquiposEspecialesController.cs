﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;

using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class EquiposEspecialesController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public EquiposEspecialesController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        // GET: Oportunidad/EquiposEspeciales
        public ActionResult Index(ConceptoDatosCapexDtoRequest dto)
        {
            return View();
        }
        
        public JsonResult CantidadEquiposEsp(SubServicioDatosCapexDtoRequest dto)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.CantidadEquiposEsp(dto));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}