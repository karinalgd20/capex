﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class OportunidadServicioCMIController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportunidadSDio = null;
        public OportunidadServicioCMIController(AgenteServicioOportunidadSDio agenteServicioOportunidadSDio) {
            this.agenteServicioOportunidadSDio = agenteServicioOportunidadSDio;
        }
        
        public ActionResult Index()
        {
            return View();
        }

        // ServicioCMI
        public JsonResult ListarServicioCMIPorLineaNegocio(OportunidadServicioCMIDtoRequest request)
        {
            request.IdEstado = Estados.Activo;
            var resultado = agenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ListarOportunidadServicioCMIPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}