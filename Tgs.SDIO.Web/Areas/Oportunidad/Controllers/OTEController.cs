﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class OTEController : Controller
    {
        private readonly AgenteServicioOportunidadSDio _AgenteServicioOportunidadSDio = null;
        DateTime fecha = DateTime.Parse(DateTime.Now.ToLongDateString());

        public OTEController(AgenteServicioOportunidadSDio AgenteServicioOportunidadSDio)
        {
            _AgenteServicioOportunidadSDio = AgenteServicioOportunidadSDio;
        }
        
        public ActionResult Index()
        {
            return View();
        }

    }
}