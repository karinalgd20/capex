﻿using System;
using System.Web.Mvc;

using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class CotizacionController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        public CotizacionController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
        }

        [HttpPost]
        public JsonResult ListarCotizacionPaginado(CotizacionDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarCotizacionPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}