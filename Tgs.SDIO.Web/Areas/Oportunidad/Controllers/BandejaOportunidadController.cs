﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;
using Tgs.SDIO.Util.Constantes;


namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class BandejaOportunidadController : Controller
    {
        // GET: Oportunidad/BandejaOportunidad
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public BandejaOportunidadController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio,
            AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }


        //[CustomAuthorize]
        //[NoCache]
        public ActionResult Index()
        {


            OportunidadDtoRequest oportunidad = new OportunidadDtoRequest();
            LineaNegocioDtoRequest LineaNegocio = new LineaNegocioDtoRequest();
          
            UsuarioDtoRequest usuario = new UsuarioDtoRequest();
            DireccionComercialDtoRequest direccioncomercial = new DireccionComercialDtoRequest();

            oportunidad.IdLineaNegocio = Generales.LineasProducto.Datos;
            oportunidad.IdCliente = Generales.Numeric.Cero;
            oportunidad.Descripcion = "";
            oportunidad.NumeroSalesForce = "";
            oportunidad.IdGerente = Generales.Numeric.Cero;
            oportunidad.IdDireccion = Generales.Numeric.Cero;
            oportunidad.IdEstado = Generales.Estados.Inactivo;
            oportunidad.Tamanio = 10;
            oportunidad.Indice = 1;
          var OptionPreVenta = Generales.EstadoLogico.Falso;
            // var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;

            PerfilSesion();

            // perfil.Where<UsuarioDtoResponse>(p => p.IdUsuario == CodigoPerfil).ToList<UsuarioDtoResponse>();

            //for (int i = 0; i < perfil.Length;)
            //{
            //   
            //}

            CodPerfil = SessionFacade.PerfilSesion;

            LineaNegocio.IdEstado = Generales.Estados.Activo;




            if (CodPerfil == Generales.TipoPerfil.Preventa)
            {
                oportunidad.IdPreVenta = SessionFacade.Usuario.UserId;
                OptionPreVenta = Generales.EstadoLogico.Verdadero;

            }
            if (CodPerfil == Generales.TipoPerfil.Coordinador_FIN)
            {
                oportunidad.IdCoordinadorFinanciero = SessionFacade.Usuario.UserId;

            }

            if (CodPerfil == Generales.TipoPerfil.Analista_FIN)
            {
                oportunidad.IdAnalistaFinanciero = SessionFacade.Usuario.UserId;

            }




            var ListOportunidadCabecera = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarOportunidadCabecera(oportunidad));

            ViewBag.vbListOportunidadCabecera = ListOportunidadCabecera;

            var ListLineaNegocio = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarLineaNegocio(LineaNegocio));

            ViewBag.vbListLineaNegocio = ListLineaNegocio;



            //usuario.CodigoSistema = "SDIO";


            //usuario.CodigoPerfil = Generales.TipoPerfil.Gerente;
            //usuario.IdEmpresa = 1;
            //var ListGerente = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario));

            //ViewBag.vbListGerente = ListGerente;
            //direccioncomercial.IdEstado = Generales.Numeric.Uno;

            //var ListDireccion = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarDireccionComercial(direccioncomercial));

            //ViewBag.vbListDireccion = ListDireccion;
            ViewBag.vbOptionPreVenta = OptionPreVenta;
            return View();
        }

        public void PerfilSesion()
        {
            var perfil = SessionFacade.Usuario.CodigoPerfil;


            for (int i = 0; i < perfil.Length;i++)
            {

                SessionFacade.PerfilSesion =
                  (perfil[i] == Generales.TipoPerfil.Analista_FIN) ?
                  perfil[i] : (perfil[i] == Generales.TipoPerfil.Coordinador_FIN ?
                  perfil[i] : (perfil[i] == Generales.TipoPerfil.Preventa ? perfil[i] : null));
    

            }
    

        }

        //[HttpPost]
        //[CustomAuthorize]
        public JsonResult ListarOportunidadCabecera(OportunidadDtoRequest request)
        {
            //var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            CodPerfil = SessionFacade.PerfilSesion;
            if (CodPerfil == Generales.TipoPerfil.Preventa)
            {
                request.IdPreVenta = SessionFacade.Usuario.UserId;
            }
            if (CodPerfil == Generales.TipoPerfil.Coordinador_FIN)
            {
                request.IdCoordinadorFinanciero = SessionFacade.Usuario.UserId;
            }
            if (CodPerfil == Generales.TipoPerfil.Product_Manager)
            {
                request.IdProductManager = SessionFacade.Usuario.UserId;
            }

            if (CodPerfil == Generales.TipoPerfil.Analista_FIN)
            {
                request.IdAnalistaFinanciero = SessionFacade.Usuario.UserId;
            }





            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarOportunidadCabecera(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObtenerVersiones(OportunidadDtoRequest request)
        {

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerVersiones(request));
            //


            //string CodSistema = Configuracion.CodigoAplicacion;
            //int IdEmpresa = Configuracion.CodigoEmpresa;
            string CodSistema = "SDIO";
            int IdEmpresa = Generales.Numeric.Uno;
            string CodPerfilAdmin = Generales.TipoPerfil.Preventa;
            string CodPerfilProduct = Generales.TipoPerfil.Product_Manager;
            string CodPerfilAnalista = Generales.TipoPerfil.Analista_FIN;
            string Coordinador_FIN = Generales.TipoPerfil.Coordinador_FIN;
            //var perfil = SessionFacade.Usuario.CodigoPerfil;
            //var CodPerfilAdmin = string.Empty;
            //for (int i = 0; i < perfil.Length; )
            //{
            //    CodPerfilAdmin = (perfil[i]);
            //}


            UsuarioDtoRequest usuario_Admin = new UsuarioDtoRequest();
            UsuarioDtoResponse ListPerfilAdmin_2 = new UsuarioDtoResponse();
            //UsuarioDtoRequest usuario_Analista = new UsuarioDtoRequest();
            List<UsuarioDtoResponse> obj = new List<UsuarioDtoResponse>();


            usuario_Admin.CodigoPerfil = CodPerfilAdmin;
            usuario_Admin.CodigoSistema = CodSistema;
            usuario_Admin.IdEmpresa = IdEmpresa;
            ListPerfilAdmin_2.NombresCompletos = "";

            var ListPerfilAdmin = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuarioPorId(resultado[0].IdUsuarioCreacion));
            
            if (request.IdUsuarioEdicion != null)
            {
                ListPerfilAdmin_2 = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuarioPorId((int)resultado[0].IdUsuarioEdicion));
            }
          
            usuario_Admin.CodigoPerfil = CodPerfilProduct;
            var ListPerfilProduct_Manager = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));


            resultado[0].NomCompletProductManager = "";
              resultado[0].NomCompletAnalistaFinanciero = "";
            if (resultado[0].IdCoordinadorFinanciero != null)
            {
                usuario_Admin.CodigoPerfil = Coordinador_FIN;

                var ListPerfilCodPerfilCoordinador_FIN = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));
                ListPerfilCodPerfilCoordinador_FIN = ListPerfilCodPerfilCoordinador_FIN.Where<UsuarioDtoResponse>(p => p.IdUsuario == ((int)resultado[0].IdCoordinadorFinanciero)).ToList<UsuarioDtoResponse>();



                resultado[0].NomCompletProductManager= ListPerfilCodPerfilCoordinador_FIN.Count > Generales.Numeric.Cero?
                ListPerfilCodPerfilCoordinador_FIN[0].NombresCompletos :  "";

             
      
            }
        

            if (resultado[0].IdAnalistaFinanciero!=null)
            {
                usuario_Admin.CodigoPerfil = CodPerfilAnalista;
                var ListPerfilAnalista = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));

                ListPerfilAnalista = ListPerfilAnalista.Where<UsuarioDtoResponse>(p => p.IdUsuario == ((int)resultado[0].IdAnalistaFinanciero)).ToList<UsuarioDtoResponse>();

                resultado[0].NomCompletAnalistaFinanciero = ListPerfilAnalista.Count > Generales.Numeric.Cero ?
                ListPerfilAnalista[0].NombresCompletos : "";
            }

            var idCreacion = resultado[0].IdUsuarioCreacion;
            var idProduct = resultado[0].IdProductManager;
            var idAnalista = resultado[0].IdAnalistaFinanciero;
            var idPreventa = resultado[0].IdPreVenta;
            if (resultado[0].IdUsuarioModifica != null)
            {
                var id = resultado[0].IdUsuarioModifica;
                var resul = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuarioPorId((int)id));

                resultado[0].NomCompletUsuMod = resul!=null?
               resul.NombresCompletos : "";
            }


            //ListPerfilAdmin = ListPerfilAdmin.Where<UsuarioDtoResponse>(p => p.IdUsuario == idCreacion).ToList<UsuarioDtoResponse>();
            //ListPerfilProduct_Manager = ListPerfilProduct_Manager.Where<UsuarioDtoResponse>(p => p.IdUsuario == idProduct).ToList<UsuarioDtoResponse>();
            //ListPerfilAnalista = ListPerfilAnalista.Where<UsuarioDtoResponse>(p => p.IdUsuario == idAnalista).ToList<UsuarioDtoResponse>();
            //ListPerfilAdmin_2 = ListPerfilAdmin_2.Where<UsuarioDtoResponse>(p => p.IdUsuario == idPreventa).ToList<UsuarioDtoResponse>();

            resultado[0].NomCompletUsuCrea = ListPerfilAdmin.NombresCompletos;
        
           
            resultado[0].NomCompletPreventa = ListPerfilAdmin.NombresCompletos;




            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DatosPorVersion(OportunidadDtoRequest request)
        {
            string CodSistema = "SDIO";
            int IdEmpresa = Generales.Numeric.Uno;
            string CodPerfilAdmin = Generales.TipoPerfil.Preventa;
            string CodPerfilProduct = Generales.TipoPerfil.Product_Manager;
            string CodPerfilAnalista = Generales.TipoPerfil.Analista_FIN;
            string Coordinador_FIN = Generales.TipoPerfil.Coordinador_FIN;

            UsuarioDtoRequest usuario_Admin = new UsuarioDtoRequest();
            UsuarioDtoRequest usuario_Product = new UsuarioDtoRequest();
            UsuarioDtoRequest usuario_Analista = new UsuarioDtoRequest();
            List<UsuarioDtoResponse> obj = new List<UsuarioDtoResponse>();
            usuario_Admin.CodigoPerfil = CodPerfilAdmin;
            usuario_Admin.CodigoSistema = CodSistema;
            usuario_Admin.IdEmpresa = IdEmpresa;

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidad(request));



            var ListPerfilAdmin = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));
            var ListPerfilAdmin_2 = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));
            usuario_Admin.CodigoPerfil = CodPerfilProduct;
            var ListPerfilProduct_Manager = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));
      

            if (resultado.IdCoordinadorFinanciero != null)
            {
                usuario_Admin.CodigoPerfil = Coordinador_FIN;

                var ListPerfilCodPerfilCoordinador_FIN = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));
                ListPerfilCodPerfilCoordinador_FIN = ListPerfilCodPerfilCoordinador_FIN.Where<UsuarioDtoResponse>(p => p.IdUsuario == ((int)resultado.IdCoordinadorFinanciero)).ToList<UsuarioDtoResponse>();

                resultado.NomCompletProductManager = ListPerfilCodPerfilCoordinador_FIN.Count > Generales.Numeric.Cero ? ListPerfilCodPerfilCoordinador_FIN[0].NombresCompletos : "";

            }


            if (resultado.IdAnalistaFinanciero != null)
            {
                usuario_Admin.CodigoPerfil = CodPerfilAnalista;
                var ListPerfilAnalista = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));

                ListPerfilAnalista = ListPerfilAnalista.Where<UsuarioDtoResponse>(p => p.IdUsuario == ((int)resultado.IdAnalistaFinanciero)).ToList<UsuarioDtoResponse>();

                resultado.NomCompletAnalistaFinanciero = ListPerfilAnalista.Count> Generales.Numeric.Cero ? ListPerfilAnalista[0].NombresCompletos : "";


            }

            var idCreacion = resultado.IdUsuarioCreacion;
            var idProduct = resultado.IdProductManager;
            var idAnalista = resultado.IdAnalistaFinanciero;
            var idPreventa = resultado.IdPreVenta;
            if (resultado.IdUsuarioModifica != null)
            {
                var id = resultado.IdUsuarioModifica;

                obj = ListPerfilAdmin.Where(p => p.IdUsuario == id).ToList();
                resultado.NomCompletUsuMod = obj.Count > Generales.Numeric.Cero ? obj[0].NombresCompletos : "";     
            }


            //ListPerfilAdmin = ListPerfilAdmin.Where<UsuarioDtoResponse>(p => p.IdUsuario == idCreacion).ToList<UsuarioDtoResponse>();
            //ListPerfilProduct_Manager = ListPerfilProduct_Manager.Where<UsuarioDtoResponse>(p => p.IdUsuario == idProduct).ToList<UsuarioDtoResponse>();
            //ListPerfilAnalista = ListPerfilAnalista.Where<UsuarioDtoResponse>(p => p.IdUsuario == idAnalista).ToList<UsuarioDtoResponse>();
            //ListPerfilAdmin_2 = ListPerfilAdmin_2.Where<UsuarioDtoResponse>(p => p.IdUsuario == idPreventa).ToList<UsuarioDtoResponse>();

            resultado.NomCompletUsuCrea = ListPerfilAdmin[0].NombresCompletos;
            resultado.NomCompletPreventa = ListPerfilAdmin_2[0].NombresCompletos;

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GraficoOibda(ServicioConceptoProyectadoDtoRequest request)
        {
            ServicioConceptoProyectadoDtoResponse resul = new ServicioConceptoProyectadoDtoResponse();
            // var result = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.DatosPorVersion(request));
            resul.IdLineaNegocio = Generales.LineasProducto.Datos;
             LineaNegocioDtoRequest lineaRequest = new LineaNegocioDtoRequest();
            List<ProyectadoOibdaResponse> data = new List<ProyectadoOibdaResponse>();

            lineaRequest.IdEstado = Generales.Estados.Activo;
            //var resultado =0;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarLineaNegocio(lineaRequest));
            //if (result.Count == 0)
            //{
                data.Add(new ProyectadoOibdaResponse()
                {
                    label = "Oibda",
                    value = Generales.Numeric.Uno,
                    Linea = "",
                    Color = Generales.ColorLinea.Linea1
                });
            //}
            //else
            //{
                string[] weekDays = {
                    Generales.ColorLinea.Linea1,
                    Generales.ColorLinea.Linea2,
                    Generales.ColorLinea.Linea3,
                    Generales.ColorLinea.Linea4,
                    Generales.ColorLinea.Linea5};
             //   var i = Generales.Numeric.Cero;
                //foreach (var partlinea in result)
                //{

                   // var nombre = "Datos";


                    //if (resultado[i].Codigo == partlinea.IdLineaNegocio.ToString())
                    //{
                    //    nombre = resultado[i].Descripcion;

                    //}

                    //   var nombre = resultado.Where<LineaNegocioDtoResponse>(p => p.IdLineaNegocio == partlinea.IdLineaNegocio).ToList<LineaNegocioDtoResponse>();

                    //data.Add(new ProyectadoOibdaResponse()
                    //{
                    //    label = "Oibda",
                    //    value=0,
                    //    //value = partlinea.Monto,
                    //    Linea = nombre,
                    //    // Linea = nombre[0].Descripcion,   
                    //    Color = weekDays[5],
                    //   // Color = weekDays[partlinea.IdLineaNegocio - 1],

                    //});
                //}



            //}

            return Json(data, JsonRequestBehavior.AllowGet);

        }

        public JsonResult ObtenerOportunidadDocumento(OportunidadDocumentoDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadDocumento(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistrarOportunidadDocumento(OportunidadDocumentoDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadDocumento(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerOportunidadCosto(OportunidadCostoDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadCosto(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistrarOportunidadCosto(OportunidadCostoDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadCosto(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerOportunidadTipoCambio(OportunidadTipoCambioDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadTipoCambio(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RegistrarOportunidadTipoCambio(OportunidadTipoCambioDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadTipoCambio(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RegistrarOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadTipoCambioDetalle(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObtenerOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadTipoCambioDetalle(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OportunidadGanador(OportunidadDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.OportunidadGanador(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult OportunidadInactivar(OportunidadDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.OportunidadInactivar(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistrarVersionOportunidad(OportunidadDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarVersionOportunidad(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


    }
}