﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class SolarWindController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public SolarWindController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        // GET: Oportunidad/SolarWind
        public ActionResult Index(ConceptoDatosCapexDtoRequest dto)
        {
            return View();
        }

        public JsonResult CantidadSolarWind(SubServicioDatosCapexDtoRequest dto)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.CantidadSolarWindVPN(dto));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
} 