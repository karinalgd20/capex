﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class SubServicioDatosCapexController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportunidadSDio = null;
        DateTime fecha = DateTime.Now;

        public SubServicioDatosCapexController(AgenteServicioOportunidadSDio agenteServicioOportunidadSDio)
        {
            this.agenteServicioOportunidadSDio = agenteServicioOportunidadSDio;
        }
        
        // GET: Oportunidad/SubServicioDatosCapex
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult RegistrarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest flujocaja)
        {
            flujocaja.IdEstado = Estados.Activo;
            flujocaja.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            flujocaja.FechaCreacion = fecha;
            var resultado = agenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.RegistrarSubServicioDatosCapex(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest flujocaja)
        {
            flujocaja.IdEstado = Estados.Activo;
            flujocaja.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            flujocaja.FechaEdicion = fecha;
            var resultado = agenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ActualizarSubServicioDatosCapex(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest flujocaja)
        {
            var resultado = agenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ListarSubServicioDatosCapex(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerSubServicioDatosCapex(SubServicioDatosCapexDtoRequest flujocaja)
        {
            var resultado = agenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ObtenerSubServicioDatosCapex(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}