﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class RegistrarCostoController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public RegistrarCostoController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;

        }
        // GET: Oportunidad/RegistrarCosto
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult RegistrarOportunidadCostoPorLineaNegocio(OportunidadCostoDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadCostoPorLineaNegocio(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActualizarOportunidadCostoPorLineaNegocio(OportunidadCostoDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidadCostoPorLineaNegocio(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerOportunidadCosto(OportunidadCostoDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadCosto(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        
    }
}