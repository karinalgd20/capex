﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class RoutersController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public RoutersController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        // GET: Oportunidad/Routers
        public ActionResult Index(ConceptoDatosCapexDtoRequest dto)
        {
            return View();
        }

        public JsonResult CantidadRouters(SubServicioDatosCapexDtoRequest dto)
        {
           var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.CantidadRouters(dto));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}