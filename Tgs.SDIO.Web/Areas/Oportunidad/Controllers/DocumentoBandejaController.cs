﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class DocumentoBandejaController : Controller
    {
        // GET: Oportunidad/DocumentoBandeja
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;

        public DocumentoBandejaController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {

            //ArchivoDtoRequest archivo = new ArchivoDtoRequest();
            //archivo.IdEstado = 1;
            //ListarArchivo(archivo);
            return View();


        }

        //[HttpPost]
        //[CustomAuthorize]
        //public JsonResult ListarArchivo(ArchivoDtoRequest archivo)
        //{
        //    var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.listarar(archivo));
        //    return Json(resultado, JsonRequestBehavior.AllowGet);
        //}


        [HttpPost]
        public virtual string UploadFiles(object obj)
        {
            var length = Request.ContentLength;
            var bytes = new byte[length];
            Request.InputStream.Read(bytes, 0, length);

            var fileName = Request.Headers["X-File-Name"];
            var fileSize = Request.Headers["X-File-Size"];
            var fileType = Request.Headers["X-File-Type"];


            var saveToFileLoc = "\\\\adcyngctg\\HRMS\\Images\\" + fileName;


            // save the file.
            var fileStream = new FileStream(saveToFileLoc, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write(bytes, 0, length);
            fileStream.Close();

            return string.Format("{0} bytes uploaded", bytes.Length);
        }

    }
}