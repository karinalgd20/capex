﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class SituacionActualController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        public SituacionActualController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ObtenerSituacionActualPorIdOportunidad(SituacionActualDtoRequest request)
        {
            var response = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerSituacionActualPorIdOportunidad(request));
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarSituacionActual(SituacionActualDtoRequest request)
        {
            if (request.Id == 0)
            {
                request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
                request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            }
            else
            {
                request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
                request.FechaEdicion = DateTime.Parse(DateTime.Now.ToLongDateString());
            }

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarSituacionActual(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}