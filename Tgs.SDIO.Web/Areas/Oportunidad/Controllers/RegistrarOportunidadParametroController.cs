﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.Web.Utilitarios;
using System;


namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class RegistrarOportunidadParametroController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        public RegistrarOportunidadParametroController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarOportunidadParametro(OportunidadParametroDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidadParametro(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ObtenerOportunidadParametro(OportunidadParametroDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadParametro(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}
