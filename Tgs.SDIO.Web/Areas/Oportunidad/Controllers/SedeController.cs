﻿using System.Web.Mvc;

using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.Web.Utilitarios;
using System;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class SedeController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        public SedeController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Editar()
        {
            return View();
        }

        [HttpPost]
        public JsonResult RegistrarSede(SedeDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarSede(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarSede(SedeDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Now;
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarSede(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EliminarSede(SedeDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Now;
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.EliminarSede(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EliminarServicios(ServicioSedeInstalacionDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Now;
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.EliminarServicios(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerSedePorId(SedeDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerSedePorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AgregarSedeInstalacion(SedeDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Now;
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.AgregarSedeInstalacion(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerClientePorIdOportunidad(OportunidadDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerClientePorIdOportunidad(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarSedeInstalacion(OportunidadDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarSedeInstalacion(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarSedeServiciosPaginado(SedeDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarSedeServiciosPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DireccionBuscar(SedeDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.DireccionBuscar(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        

        [HttpPost]
        public JsonResult DetalleSedePorId(SedeDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.DetalleSedePorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}