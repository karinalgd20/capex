﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class OportunidadFlujoCajaController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        public OportunidadFlujoCajaController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
        }

        #region - Transaccionales -
        #endregion

        #region - No Transaccionales -
        [HttpPost]
        public JsonResult ListarServiciosAgrupados(OportunidadFlujoCajaDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarServiciosCircuitosPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerServicioPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerServicioPorIdFlujoCaja(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}