﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class RegistrarTipoCambioController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public RegistrarTipoCambioController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;

        }
        // GET: Oportunidad/RegistrarTipoCambio
        public ActionResult Index()
        {
            return View();
        }

        
        public JsonResult ObtenerOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadTipoCambioDetalle(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActualizarOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidadTipoCambioDetalle(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}