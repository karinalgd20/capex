﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class BandejaOportunidadGanadoraController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public BandejaOportunidadGanadoraController
            (
            AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, 
            AgenteServicioSeguridadSDio agenteServicioSeguridadSDio,
            AgenteServicioComunSDio agenteServicioComunSDio
            )
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        public ActionResult Index()
        {
            OportunidadDtoRequest oportunidad = new OportunidadDtoRequest();
            LineaNegocioDtoRequest LineaNegocio = new LineaNegocioDtoRequest();
            UsuarioDtoRequest usuario = new UsuarioDtoRequest();
            DireccionComercialDtoRequest direccioncomercial = new DireccionComercialDtoRequest();

            oportunidad.IdLineaNegocio = Generales.LineasProducto.Datos;
            oportunidad.IdCliente = Generales.Numeric.Cero;
            oportunidad.Descripcion = "";
            oportunidad.NumeroSalesForce = "";
            oportunidad.IdGerente = Generales.Numeric.Cero;
            oportunidad.IdDireccion = Generales.Numeric.Cero;
            oportunidad.IdEstado = Generales.Estados.Ganado;
            oportunidad.Tamanio = 10;
            oportunidad.Indice = 1;

            var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            for (int i = 0; i < perfil.Length; i++)
            {
                CodPerfil = (perfil[i]);
            }

            ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Falso;
            LineaNegocio.IdEstado = Generales.Estados.Activo;

            if (CodPerfil == Generales.TipoPerfil.Preventa)
            {
                oportunidad.IdPreVenta = SessionFacade.Usuario.UserId;
                ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Verdadero;
            }

            if (CodPerfil == Generales.TipoPerfil.Coordinador_FIN)
            {
                oportunidad.IdCoordinadorFinanciero = SessionFacade.Usuario.UserId;
            }

            if (CodPerfil == Generales.TipoPerfil.Analista_FIN)
            {
                oportunidad.IdAnalistaFinanciero = SessionFacade.Usuario.UserId;
            }

            var ListOportunidadCabecera = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarOportunidadCabecera(oportunidad));
            ViewBag.vbListOportunidadCabecera = ListOportunidadCabecera;

            var ListLineaNegocio = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarLineaNegocio(LineaNegocio));
            ViewBag.vbListLineaNegocio = ListLineaNegocio;

            return View();
        }
    }
}