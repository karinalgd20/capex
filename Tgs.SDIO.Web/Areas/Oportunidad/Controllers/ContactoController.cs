﻿using System;
using System.Web.Mvc;

using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class ContactoController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        public ContactoController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
        }

        #region - Transaccionales -
        [HttpPost]
        public JsonResult RegistrarContacto(ContactoDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarContacto(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarContacto(ContactoDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarContacto(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InactivarContacto(ContactoDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = DateTime.Parse(DateTime.Now.ToLongDateString());
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.InactivarContacto(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region - No Transaccionales -
        [HttpPost]
        public JsonResult ListarContactoPaginado(ContactoDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarContactoPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerContactoPorId(ContactoDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerContactoPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}