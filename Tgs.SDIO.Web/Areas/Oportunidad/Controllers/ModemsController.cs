﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Web.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class ModemsController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        
        public ModemsController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }
        // GET: Oportunidad/Modems
        public ActionResult Index(ConceptoDatosCapexDtoRequest dto)
        {
            return View();
        }

        public JsonResult CantidadModems(SubServicioDatosCapexDtoRequest dto)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.CantidadModems(dto));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}