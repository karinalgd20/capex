﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Web.Comun.Constantes.General;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class FlujoCajaController : Controller
    {
        private readonly AgenteServicioOportunidadSDio _AgenteServicioOportunidadSDio = null;
        DateTime fecha = DateTime.Now;

        public FlujoCajaController(AgenteServicioOportunidadSDio AgenteServicioOportunidadSDio)
        {
            _AgenteServicioOportunidadSDio = AgenteServicioOportunidadSDio;
        } 

        // GET: Oportunidad/FlujoCaja
        public ActionResult Index()
        {
            return View();
        }
         public JsonResult RegistrarCasoNegocioServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            servicioSubServicio.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.RegistrarCasoNegocioServicio(servicioSubServicio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistrarServicioComponente(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            servicioSubServicio.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.RegistrarServicioComponente(servicioSubServicio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListarPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
     
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ListarPorIdFlujoCaja(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistrarComponente(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            flujocaja.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.RegistrarComponente(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ListarAnoMesProyecto(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ListaAnoMesProyecto(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            flujocaja.IdEstado = Estados.Activo;
            flujocaja.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            flujocaja.FechaCreacion = fecha;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadFlujoCaja(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {


            flujocaja.IdEstado = Estados.Activo;
            flujocaja.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            flujocaja.FechaEdicion = fecha;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidadFlujoCaja(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult InhabilitarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {

   
            flujocaja.IdEstado = Estados.Activo;
            flujocaja.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            flujocaja.FechaEdicion = fecha;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.InhabilitarOportunidadFlujoCaja(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult InhabilitarOportunidadEcapex(OportunidadFlujoCajaDtoRequest flujocaja)
        {


            flujocaja.IdEstado = Estados.Activo;
            flujocaja.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            flujocaja.FechaEdicion = fecha;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.InhabilitarOportunidadEcapex(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        } 
        [HttpPost]
        public JsonResult ObtenerOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadFlujoCaja(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            flujocaja.IdEstado = Estados.Activo;
            flujocaja.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            flujocaja.FechaCreacion = fecha;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadFlujoCajaConfiguracion(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            flujocaja.IdEstado = Estados.Activo;
            flujocaja.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            flujocaja.FechaEdicion = fecha;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidadFlujoCajaConfiguracion(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            flujocaja.IdEstado = Estados.Activo;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadFlujoCajaConfiguracion(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GeneraCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            casonegocio.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            casonegocio.IdEstado = Estados.Activo;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.GeneraCasoNegocio(casonegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GeneraServicio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            casonegocio.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            casonegocio.IdEstado = Estados.Activo;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.GeneraServicio(casonegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EliminarCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            casonegocio.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            casonegocio.IdEstado = Estados.Inactivo;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.EliminarCasoNegocio(casonegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            casonegocio.IdEstado = Estados.Activo;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ListarDetalleOportunidadCaratula(casonegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            casonegocio.IdEstado = Estados.Activo;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ListarDetalleOportunidadEcapex(casonegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void GenerarFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest request)
        {
            var usuarioLoginRais = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.GeneraProyectadoOportunidadFlujoCaja(request));
        }

        [HttpPost]
        public JsonResult ObtenerDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            casonegocio.IdEstado = Estados.Activo;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ObtenerDetalleOportunidadEcapex(casonegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListaOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            flujocaja.IdEstado = Estados.Activo;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ListaOportunidadFlujoCajaConfiguracion(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarPeriodoOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            flujocaja.IdEstado = Estados.Activo;
            flujocaja.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            flujocaja.FechaEdicion = fecha;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ActualizarPeriodoOportunidadFlujoCaja(flujocaja));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarOportunidadCosto(OportunidadCostoDtoRequest costo)
        {
            costo.IdEstado = Estados.Activo;
            costo.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            costo.FechaCreacion = fecha;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadCosto(costo));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarOportunidadCosto(OportunidadCostoDtoRequest costo)
        {
            costo.IdEstado = Estados.Activo;
            costo.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            costo.FechaEdicion = fecha;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidadCosto(costo));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            casonegocio.IdEstado = Estados.Activo;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ObtenerDetalleOportunidadCaratula(casonegocio));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarSubServicioCaratula(SubServicioDatosCaratulaDtoRequest caratula)
        {
           
            caratula.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            caratula.FechaEdicion = fecha;
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ActualizarSubServicioDatosCaratula(caratula));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListaServicioPorOportunidadLineaNegocio(OportunidadFlujoCajaDtoRequest request)
        {
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.ListaServicioPorOportunidadLineaNegocio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }



    }
}