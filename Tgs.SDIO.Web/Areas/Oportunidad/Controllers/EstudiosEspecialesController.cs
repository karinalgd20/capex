﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Web.Comun;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class EstudiosEspecialesController : Controller
    {
        private readonly AgenteServicioOportunidadSDio _AgenteServicioOportunidadSDio = null;
        public EstudiosEspecialesController(AgenteServicioOportunidadSDio AgenteServicioOportunidadSDio)
        {
            _AgenteServicioOportunidadSDio = AgenteServicioOportunidadSDio;
        }
        // GET: Oportunidad/EstudiosEspeciales
        public ActionResult Index(SubServicioDatosCapexDtoRequest dto)
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarEstudiosEspeciales(SubServicioDatosCapexDtoRequest dto)
        {
            var responseSolicitud = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.RegistrarSubServicioDatosCapex(dto));
            return Json(responseSolicitud, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CantidadEstudiosEsp(SubServicioDatosCapexDtoRequest dto)
        {
            var resultado = _AgenteServicioOportunidadSDio.InvocarFuncionAsync(o => o.CantidadEstudiosEsp(dto));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}