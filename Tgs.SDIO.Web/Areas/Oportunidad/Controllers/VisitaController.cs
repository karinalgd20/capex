﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class VisitaController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        public VisitaController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Editar()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarVisita(VisitaDtoRequest request)
        {
            if (request.Id.Equals(0))
            {
                request.IdEstado = 1;
                request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
                request.FechaCreacion = DateTime.Parse(DateTime.Now.ToLongDateString());
            }
            else
            {
                request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
                request.FechaEdicion = DateTime.Parse(DateTime.Now.ToLongDateString());
            }

            var resultado = request.Id.Equals(0) ?
                agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarVisita(request)) :
                agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarVisita(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ObtenerVisitaPorIdOportunidad(VisitaDtoRequest request)
        {
            var response = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerVisitaPorIdOportunidad(request));
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}