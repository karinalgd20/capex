﻿using System;
using System.Web.Mvc;

using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class EstudiosController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        public EstudiosController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
        }

        [HttpPost]
        public JsonResult ListarEstudiosPaginado(EstudiosDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarEstudiosPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}