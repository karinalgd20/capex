﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class PrePeticionCompraController : Controller
    {
        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;

        public PrePeticionCompraController(AgenteServicioCompraSDio agenteServicioCompraSDio)
        {
            this.agenteServicioCompraSDio = agenteServicioCompraSDio;
        }
        
        public ActionResult Index(int IdProyecto, int IdDetalleFinanciero)
        {
            ViewBag.IdProyecto = IdProyecto;
            ViewBag.IdDetalleFinanciero = IdDetalleFinanciero;

            return View();
        }

        [HttpGet]
        public ActionResult MostrarDetallePrePeticionCompra()
        {
            return RedirectToAction("Index", "DetallePrePeticionCompra", null);
        }

        [HttpGet]
        public ActionResult MostrarDetallePrePeticionCompraOrdinaria()
        {
            return RedirectToAction("Index", "DetallePrePeticionCompraOrdinaria", null);
        }

        [HttpGet]
        public ActionResult ModalBuscarPrePeticion()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ListaPrePeticion(PrePeticionCabeceraDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ListarPrePeticion(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RegistrarPrePeticionCompra(int IdProyecto, int IdDetalleFinanciero, int Id)
        {
            ViewBag.IdCabecera = Id;
            ViewBag.IdProyecto = IdProyecto;
            ViewBag.IdDetalleFinanciero = IdDetalleFinanciero;

            return View();
        }

        [HttpPost]
        public ActionResult RegistrarPrePeticionCompra(PrePeticionCabeceraDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.RegistrarPrePeticionCompraCabecera(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListaPrePeticionDCM(PrePeticionDetalleDCMDetalleDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ListarPrePeticionDCM(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListaPrePeticionOrdinaria(PrePeticionOrdinariaDetalleDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ListarPrePeticionOrdinaria(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ObtenerPrePeticionCabecera(PrePeticionCabeceraDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerPrePeticionCabecera(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ValidarMontoLimite(PrePeticionCabeceraDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ValidacionMontoLimite(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ObtenerSaldo(PrePeticionCabeceraDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerSaldo(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}