﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class DetalleCompraCuentaController : Controller
    {
        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;

        public DetalleCompraCuentaController(AgenteServicioCompraSDio agenteServicioCompraSDio)
        {
            this.agenteServicioCompraSDio = agenteServicioCompraSDio;
        }
        // GET: Compra/DetalleCompraCuenta
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarCuentasPaginado(CuentaDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ListarCuentaPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}