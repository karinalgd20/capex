﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class DatosGeneralesController : Controller
    {

        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;

        public DatosGeneralesController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Compra/DatosGenerales
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult CargarDatosUsuarioRegistra(PeticionCompraDtoRequest request)
        {
            var nombreUsuario = string.Empty;
            var correoUsuario = string.Empty;

            nombreUsuario = SessionFacade.Usuario.FirstName;
            correoUsuario = SessionFacade.Usuario.EmailUser;

            return Json(new
            {
                NombreUsuarioActual = nombreUsuario,
                EmailUsuarioActual = correoUsuario
            }, JsonRequestBehavior.AllowGet);
        }

    }
}