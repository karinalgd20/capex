﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class PeticionCompraResponsableController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;

        public PeticionCompraResponsableController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        // GET: Compra/PeticionCompraResponsable
        public ActionResult Index()
        {
            return View();
        }
    }
}