﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class DetalleCompraController : Controller
    {
        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;
        DateTime fecha = DateTime.Now;
        public DetalleCompraController(AgenteServicioCompraSDio agenteServicioCompraSDio)
        {
            this.agenteServicioCompraSDio = agenteServicioCompraSDio;
        }

        // GET: Compra/DetalleCompra
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListarDetalleCompra(DetalleCompraDtoRequest request)
        {
            List<DetalleCompraDtoResponse> resultado = null;
            if (request.IdPeticionCompra == 0)
            {
                resultado = new List<DetalleCompraDtoResponse>();
            }
            else
            {
                resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ListarDetalleCompra(request));
                foreach (var item in resultado)
                {
                    item.EsNuevo = false;
                }
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarDetalleCompra(List<DetalleCompraDtoRequest> request)
        {
            ProcesoResponse resultado = null;
            var iCantDetalles = request.Count;
            foreach (var detalle in request)
            {
                if (detalle.EsNuevo) detalle.IdDetalleCompra = 0;

                detalle.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
                resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.RegistrarDetalleCompra(detalle));
            }

            if (request.Count > 0)
            {
                var peticionRequest = new PeticionCompraDtoRequest()
                {
                    IdPeticionCompra = request[0].IdPeticionCompra,
                    DetalleSolpe = request[0].DetalleSolpe,
                    CostoTotal = request[0].TotalCosto,
                    IdTipoMoneda = request[0].IdTipoMoneda,
                    SustentoCualitativo = request[0].SustentoCualitativo,
                    IdUsuarioEdicion = SessionFacade.Usuario.UserId,
                    FechaEdicion = fecha
                };
                resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ActualizarPeticionCompraDetalleCompra(peticionRequest));
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult EliminarDetalleCompra(DetalleCompraDtoRequest request)
        {
            agenteServicioCompraSDio.InvocarFuncionAsync(o => o.EliminarDetalleCompra(request));
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}