﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Mensajes.Compra;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class ComentarioAnexoController : Controller
    {
        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ComentarioAnexoController(AgenteServicioCompraSDio agenteServicioCompraSDio,
                                        AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioCompraSDio = agenteServicioCompraSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarAnexos(int IdPeticionCompra)
        {
            List<AnexoDtoResponse> resultado = new List<AnexoDtoResponse>();
            resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ListarAnexos(new AnexoDtoRequest { IdPeticionCompra = IdPeticionCompra }));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EliminarAnexo(int IdPeticionCompra)
        {
            agenteServicioCompraSDio.InvocarFuncionAsync(o => o.EliminarAnexo(new AnexoDtoRequest { IdAnexoPeticionCompra = IdPeticionCompra }));
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        public FileResult DescargarAnexoArchivo(int iIdAnexoPeticionCompra)
        {
            AnexoArchivoDtoResponse oAnexoArchivo = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerAnexoArchivo(iIdAnexoPeticionCompra));
            string sArchivo = Convert.ToBase64String(oAnexoArchivo.ArchivoAdjunto);

            return File(oAnexoArchivo.ArchivoAdjunto, "application/force-download", oAnexoArchivo.NombreArchivo);

        }

        [HttpPost]
        public ActionResult GuardarAnexo()
        {
            string[] sIdTipoDocumento = Request["IdTipoDocumento"].Split(',');
            int IdPeticionCompra = Convert.ToInt32(Request["IdPeticionCompra"]);

            try
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i];

                    using (Stream fs = file.InputStream)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            AnexoDtoRequest request = new AnexoDtoRequest();
                            request.ArchivoAdjunto = bytes;
                            request.NombreArchivo = file.FileName;
                            request.IdPeticionCompra = IdPeticionCompra;
                            request.IdTipoDocumento = Convert.ToInt32(sIdTipoDocumento[i]);
                            request.IdUsuario = SessionFacade.Usuario.UserId;

                            agenteServicioCompraSDio.InvocarFuncionAsync(o => o.RegistrarAnexo(request));
                        }
                    }
                }

                return Json(new { TipoRespuesta = Proceso.Valido, Mensaje = MensajesGeneralCompra.RegistrarAnexos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { TipoRespuesta = Proceso.Invalido, Mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}