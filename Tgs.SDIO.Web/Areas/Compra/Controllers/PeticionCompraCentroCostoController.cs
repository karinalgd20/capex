﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class PeticionCompraCentroCostoController : Controller
    {
        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;

        public PeticionCompraCentroCostoController(AgenteServicioCompraSDio agenteServicioCompraSDio)
        {
            this.agenteServicioCompraSDio = agenteServicioCompraSDio;
        }

        // GET: Compra/PeticionCompraCentroCosto
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarCentroCostoPorAreaPaginado(CentroCostoRecursoDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ListarCentroCostoRecursoPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}