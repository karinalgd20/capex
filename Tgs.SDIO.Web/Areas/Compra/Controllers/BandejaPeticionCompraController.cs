﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class BandejaPeticionCompraController : Controller
    {

        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;

        public BandejaPeticionCompraController(AgenteServicioCompraSDio AgenteServicioCompraSDio)
        {
            this.agenteServicioCompraSDio = AgenteServicioCompraSDio;
        }

        // GET: Compra/BandejaPeticionCompra
        public ActionResult Index()
        {
            EtapaPeticionCompraDtoRequest request = new EtapaPeticionCompraDtoRequest()
            {
                IdUsuario = SessionFacade.Usuario.UserId,
                IdPeticionCompra = 0
            };

            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerEtapaPeticionCompraActual(request));
            if (resultado != null) {
                ViewBag.Orden = resultado.Orden;
            }
            
            return View();
        }

        public JsonResult ListarPeticionCompraPaginado(PeticionCompraDtoRequest request) {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ListarPeticionCompraPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}