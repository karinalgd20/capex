﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class LineasCMIController : Controller
    {

        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;
        private readonly AgenteServicioProyectoSDio agenteServicioProyectoSDio = null;

        public LineasCMIController(AgenteServicioProyectoSDio AgenteServicioProyectoSDio)
        {
            this.agenteServicioProyectoSDio = AgenteServicioProyectoSDio;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult MostrarPrePeticionCompra()
        {
            return RedirectToAction("Index", "PrePeticionCompra", null);
        }
    }
}