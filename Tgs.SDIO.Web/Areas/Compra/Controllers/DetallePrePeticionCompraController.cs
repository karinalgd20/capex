﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class DetallePrePeticionCompraController : Controller
    {
        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;

        public DetallePrePeticionCompraController(AgenteServicioCompraSDio agenteServicioCompraSDio)
        {
            this.agenteServicioCompraSDio = agenteServicioCompraSDio;
        }
        
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RegistrarPrePeticionDCMDetalle(PrePeticionDetalleDCMDetalleDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.RegistrarPrePeticionDCMDetalle(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ObtenerPrePeticionDCMDetalle(PrePeticionDetalleDCMDetalleDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerDetalleDCM(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}