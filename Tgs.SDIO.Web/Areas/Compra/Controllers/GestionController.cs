﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class GestionController : Controller
    {
        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public GestionController(AgenteServicioCompraSDio agenteServicioCompraSDio, AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioCompraSDio = agenteServicioCompraSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Compra/Gestion
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerGestion(GestionDtoRequest request)
        {
            GestionDtoResponse oResponse = agenteServicioCompraSDio.InvocarFuncionAsync(d => d.ObtenerGestion(request));
            return Json(oResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GuardarGestion(GestionDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;

            EtapaPeticionCompraDtoRequest etapaactual = new EtapaPeticionCompraDtoRequest()
            {
                IdUsuario = SessionFacade.Usuario.UserId,
                IdPeticionCompra = request.IdPeticionCompra
            };

            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerEtapaPeticionCompraActual(etapaactual));
            request.IdEstadoEtapa = resultado.IdEstadoEtapa.Value;
            request.IdOrden = resultado.Orden;

            ProcesoResponse oProcesoResponse = agenteServicioCompraSDio.InvocarFuncionAsync(d => d.RegistrarGestion(request));

            return Json(oProcesoResponse, JsonRequestBehavior.AllowGet);
        }
    }
}