﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class SoporteController : Controller
    {
        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;

        public SoporteController(AgenteServicioCompraSDio AgenteServicioCompraSDio)
        {
            this.agenteServicioCompraSDio = AgenteServicioCompraSDio;
        }

        // GET: Compra/Soporte
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListarEtapaPeticionCompraPaginado(EtapaPeticionCompraDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ListarEtapaPeticionCompraPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}