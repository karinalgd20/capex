﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class AprobacionesController : Controller
    {
        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;

        public AprobacionesController(AgenteServicioCompraSDio AgenteServicioCompraSDio)
        {
            this.agenteServicioCompraSDio = AgenteServicioCompraSDio;
        }
        // GET: Compra/Aprobaciones
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListarAprobaciones(PeticionCompraDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ListaAprobaciones(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}