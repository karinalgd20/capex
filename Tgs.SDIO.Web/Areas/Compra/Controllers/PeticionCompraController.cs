﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Correo;
using Tgs.SDIO.Util.Mensajes.Compra;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Compra;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class PeticionCompraController : Controller
    {
        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        DateTime fecha = DateTime.Now;
        public PeticionCompraController(AgenteServicioCompraSDio agenteServicioCompraSDio,
                                        AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioCompraSDio = agenteServicioCompraSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Compra/PeticionCompra
        public ActionResult Index(int id = 0, int Ver = 0)
        {
            ViewBag.IdPeticionCompra = id;
            try
            {
                EtapaPeticionCompraDtoRequest request = new EtapaPeticionCompraDtoRequest()
                {
                    IdUsuario = SessionFacade.Usuario.UserId,
                    IdPeticionCompra = id
                };

                var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerEtapaPeticionCompraActual(request));
                ViewBag.Accion = resultado.Accion;
                ViewBag.Orden = resultado.Orden;
                ViewBag.EstadoEtapa = resultado.IdEstadoEtapa;
                ViewBag.IdEtapaPeticionCompra = resultado.IdEtapaPeticionCompra;
                ViewBag.Observado = resultado.Observado;
                ViewBag.Comentario = resultado.Comentario;
                ViewBag.Etapa = resultado.Etapa;

                ViewBag.IdUsuario = request.IdUsuario;
                ViewBag.Ver = Ver;

                var perfil = SessionFacade.Usuario.CodigoPerfil;
                var CodPerfil = string.Empty;
                for (int i = 0; i < perfil.Length; i++)
                {
                    CodPerfil = (perfil[i]);
                }

                ViewBag.vbOptionUsuarioSol = string.Empty;
                if ((CodPerfil == Generales.TipoPerfil.UsuarioSolicitante || CodPerfil == Generales.TipoPerfil.Gestor || CodPerfil == Generales.TipoPerfil.JefeProyectos)
                    && resultado.Etapa == Etapas.CERTIFICADA)
                {
                    ViewBag.flagActaSubidaUsuario = ValidarActaAceptacion(id);

                    if (resultado.IdRecursoAtiende == SessionFacade.Usuario.UserId)
                    {
                        ViewBag.flagUsuarioAtiende = true;
                    }
                    else
                    {
                        ViewBag.flagUsuarioAtiende = false;
                    }
                } else if (CodPerfil == Generales.TipoPerfil.UsuarioSolicitante || CodPerfil == Generales.TipoPerfil.JefeProyectos) {
                    ViewBag.vbOptionUsuarioSol = Generales.TipoPerfil.UsuarioSolicitante;
                }

            }
            catch (Exception ex)
            {
                return Json(new { TipoRespuesta = Proceso.Invalido, Mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return View();
        }

        public ActionResult Ver(int id = 0, int ver = 0)
        {
            ViewBag.IdPeticionCompra = id;
            try
            {
                EtapaPeticionCompraDtoRequest request = new EtapaPeticionCompraDtoRequest()
                {
                    IdUsuario = SessionFacade.Usuario.UserId,
                    IdPeticionCompra = id
                };

                var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerEtapaPeticionCompraActual(request));
                ViewBag.Accion = resultado.Accion;
                ViewBag.Orden = resultado.Orden;
                ViewBag.EstadoEtapa = resultado.IdEstadoEtapa;
                ViewBag.IdEtapaPeticionCompra = resultado.IdEtapaPeticionCompra;
                ViewBag.Observado = resultado.Observado;
                ViewBag.Comentario = resultado.Comentario;
                ViewBag.Etapa = resultado.Etapa;
            }
            catch (Exception ex)
            {
                return Json(new { TipoRespuesta = Proceso.Invalido, Mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return RedirectToAction("Index", new { id = id, Ver = ver });
        }

        [HttpGet]
        public ActionResult ObtenerInformacionDatosGenerales()
        {
            return RedirectToAction("Index", "DatosGenerales", null);
        }
        [HttpGet]
        public ActionResult ObtenerInformacionClienteProveedor()
        {
            return RedirectToAction("Index", "ClienteProveedor", null);
        }
        [HttpGet]
        public ActionResult ObtenerInformacionDetalleCompra()
        {
            return RedirectToAction("Index", "DetalleCompra", null);
        }
        [HttpGet]
        public ActionResult ObtenerInformacionAprobaciones()
        {
            return RedirectToAction("Index", "Aprobaciones", null);
        }
        [HttpGet]
        public ActionResult ObtenerInformacionComentarioAnexo()
        {
            return RedirectToAction("Index", "ComentarioAnexo", null);
        }
        [HttpGet]
        public ActionResult ObtenerInformacionSoporte()
        {
            return RedirectToAction("Index", "Soporte", null);
        }
        [HttpGet]
        public ActionResult ObtenerInformacionGestion()
        {
            return RedirectToAction("Index", "Gestion", null);
        }
        [HttpGet]
        public ActionResult ObtenerInformacionDespacho()
        {
            return RedirectToAction("Index", "Despacho", null);
        }

        [HttpPost]
        public JsonResult RegistrarPeticionCompra(PeticionCompraDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.RegistrarPeticionCompra(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActualizarPeticionCompra(PeticionCompraDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;
            request.FechaEdicion = fecha;
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ActualizarPeticionCompra(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerPeticionCompraPorId(PeticionCompraDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerPeticionCompraPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RegistrarEtapaPeticionCompra(EtapaPeticionCompraDtoRequest request)
        {
            EtapaPeticionCompraDtoRequest EtapaPeticionCompraRequest = new EtapaPeticionCompraDtoRequest
            {
                IdPeticionCompra = request.IdPeticionCompra,
                IdEstadoEtapa = request.IdEstadoEtapa,
                IdUsuario = SessionFacade.Usuario.UserId,
                IdOrden = request.IdOrden,
                IdOrdenObservar = request.IdOrdenObservar,
                Comentario = string.IsNullOrEmpty(request.Comentario) ? "" : request.Comentario,
                IrSiguienteEtapa = request.IrSiguienteEtapa
            };

            EtapaPeticionCompraDtoRequest oEtapaPeticionCompraDtoRequest = new EtapaPeticionCompraDtoRequest()
            {
                IdUsuario = SessionFacade.Usuario.UserId,
                IdPeticionCompra = request.IdPeticionCompra
            };

            EtapaPeticionCompraDtoResponse oEtapaPeticionCompraDtoResponse = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerEtapaPeticionCompraActual(oEtapaPeticionCompraDtoRequest));
            
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.RegistrarEtapaPeticionCompra(EtapaPeticionCompraRequest));

            //Envio de Notificacion De Gestor a Solicitante.
            if (resultado.TipoRespuesta == 0)
            {
                if (oEtapaPeticionCompraDtoResponse.Etapa == Util.Constantes.Compra.Etapas.GESTOR && request.IdEstadoEtapa == 4)
                {
                    PeticionCompraDtoRequest PeticionCompraRequest = new PeticionCompraDtoRequest()
                    {
                        IdPeticionCompra = request.IdPeticionCompra
                    };

                    var peticionCompra = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerPeticionCompraPorId(PeticionCompraRequest));
                    try
                    {
                        EnviarNotificaion(peticionCompra.CorreoSolicitante, peticionCompra.Solicitante);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }

            if (request.IdEstadoEtapa == 4)
            {
                resultado.Mensaje = MensajesGeneralCompra.AprobacionPeticionCompra;
            }
            else if (request.IdEstadoEtapa == 6)
            {
                resultado.Mensaje = MensajesGeneralCompra.AtenderPeticionCompra;
            }
            else {
                resultado.Mensaje = MensajesGeneralCompra.ActualizarPeticionCompra; 
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarEtapas(ConfiguracionEtapaDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ListarEtapas(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerEtapaActual(int iIdPeticionCompra) {
            EtapaPeticionCompraDtoRequest request = new EtapaPeticionCompraDtoRequest()
            {
                IdUsuario = SessionFacade.Usuario.UserId,
                IdPeticionCompra = iIdPeticionCompra
            };

            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerEtapaPeticionCompraActual(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        private void EnviarNotificaion(string correoDestinatario, string nombreDestinatario) {

            // Configurar envio de correo
            string subject = string.Format("{0}-{1}: {2}", "[SDIO - Compras]", "Notificación de aprobación.", "");
            string mailFrom = ConfigurationManager.AppSettings.Get("MailEmisor");
            string passwordMailEmisor = ConfigurationManager.AppSettings.Get("PasswordMailEmisor");
            StringBuilder buffer = new StringBuilder();

            buffer.Append("Estimado <b>{0}</b> <br /><br />");
            buffer.Append(" Es grato saludarlo e informarle que se ha realizado la aprobación, y se ha validado el registro de cesta y SAP.<br /><br />");
            buffer.Append("<br/><br/>");
            buffer.Append(" Saludos cordiales.");
            buffer.Append("<br/>");
            buffer.Append("<strong> Soporte SDIO - Compras</strong>");
            buffer.Append("<br/><br/>");
            buffer.Append("<i> Nota: Por favor no responder a este correo. <i>");

            if (!string.IsNullOrEmpty(mailFrom))
            {
                MailHelper.SendMail(mailFrom, passwordMailEmisor, correoDestinatario,
                subject, string.Format(buffer.ToString(), nombreDestinatario), true, System.Net.Mail.MailPriority.Normal);
            }
        }

        private bool ValidarActaAceptacion(int idPeticionCompra = 0)
        {
            bool resultado = false;
            List<AnexoDtoResponse> listaDocumentos = new List<AnexoDtoResponse>();
            listaDocumentos = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ListarAnexos(new AnexoDtoRequest { IdPeticionCompra = idPeticionCompra }));

            var listActaAceptacion = listaDocumentos.FindAll(p => p.IdTipoDocumento == TipoDocumento.ActaAceptacion).ToList();

            if (listActaAceptacion.Count() == 1) resultado = true;

            return resultado;
        }

    }
}
