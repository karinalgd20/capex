﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class ClienteProveedorController : Controller
    {
        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ClienteProveedorController(AgenteServicioCompraSDio agenteServicioCompraSDio, AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioCompraSDio = agenteServicioCompraSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Compra/ClienteProveedor
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListaClientePaginado(ClienteDtoRequest request) {
            ClienteDtoResponsePaginado oClienteDtoResponsePaginado = agenteServicioComunSDio.InvocarFuncionAsync(d => d.ListarClientePaginado(request));
            return Json(oClienteDtoResponsePaginado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListaProveedorPaginado(ProveedorDtoRequest request)
        {
            ProveedorPaginadoDtoResponse oProveedorDtoResponsePaginado = agenteServicioComunSDio.InvocarFuncionAsync(d => d.ListarProveedorPaginado(request));
            return Json(oProveedorDtoResponsePaginado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObtenerDetalleClienteProveedor(DetalleClienteProveedorDtoRequest request)
        {
            DetalleClienteProveedorDtoResponse oResponse = agenteServicioCompraSDio.InvocarFuncionAsync(d => d.ObtenerDetalleClienteProveedor(request));
            return Json(oResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GuardarDetalleClienteProveedor(DetalleClienteProveedorDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.IdUsusarioSistema;
            ProcesoResponse oProcesoResponse = agenteServicioCompraSDio.InvocarFuncionAsync(d => d.RegistrarDetalleClienteProveedor(request));
            return Json(oProcesoResponse, JsonRequestBehavior.AllowGet);
        }
    }
}