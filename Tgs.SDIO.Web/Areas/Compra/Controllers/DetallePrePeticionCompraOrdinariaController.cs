﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class DetallePrePeticionCompraOrdinariaController : Controller
    {
        private readonly AgenteServicioCompraSDio agenteServicioCompraSDio = null;

        public DetallePrePeticionCompraOrdinariaController(AgenteServicioCompraSDio agenteServicioCompraSDio)
        {
            this.agenteServicioCompraSDio = agenteServicioCompraSDio;
        }
        
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RegistrarPrePeticionOrdinariaDetalle(PrePeticionOrdinariaDetalleDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.RegistrarPrePeticionOrdinariaDetalle(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ObtenerPrePeticionOrdinariaDetalle(PrePeticionOrdinariaDetalleDtoRequest request)
        {
            var resultado = agenteServicioCompraSDio.InvocarFuncionAsync(o => o.ObtenerDetalleOrdinaria(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}