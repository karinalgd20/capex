﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class PeticionCompraContratoMarcoController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;

        public PeticionCompraContratoMarcoController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Compra/PeticionCompraCentroCosto
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ListarContratoMarcoPaginado(ContratoMarcoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarContratoMarcoPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerContratoMarcoPorId(ContratoMarcoDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerContratoMarcoPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}