﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Compra.Controllers
{
    public class PeticionCompraCompradorController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;

        public PeticionCompraCompradorController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Compra/PeticionCompraComprador
        public ActionResult Index()
        {
            return View();
        }
    }
}