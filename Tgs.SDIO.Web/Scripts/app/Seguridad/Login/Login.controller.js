﻿(function () {
    'use strict',

    angular
    .module('app.Seguridad')
    .controller('LoginController', LoginController);

    LoginController.$inject = ['LoginService', 'CambioClaveService', 'OlvidoClaveService', 'blockUI', '$timeout', 'UtilsFactory', 'MensajesUI', '$scope', '$modal', '$injector'];

    function LoginController(LoginService, CambioClaveService, OlvidoClaveService, blockUI, $timeout, UtilsFactory, $MensajesUI, $scope, $modal, $injector) {
        var vm = this;
        vm.Login = Login;

        vm.TxtUsuario = 'jtorres';
        vm.TxtClave = 'iQ0802';
        

        vm.CargaModalCambioClave = CargaModalCambioClave;
        vm.CargaModalOlvidoClave = CargaModalOlvidoClave;

        function Login() {
             
            var UsuarioDto = {
                Login: vm.TxtUsuario,
                Password: vm.TxtClave
            };

            if (UsuarioDto.Login == '') {
                UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese su usuario", 5);
                blockUI.stop();
                return;
            }

            if (UsuarioDto.Password == '') {
                UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese su clave", 5);
                blockUI.stop();
                return;
            }

            blockUI.start();
            var token = UtilsFactory.TokenAutorizacion();
            var promise = LoginService.LoginDatos(UsuarioDto,token);

            promise.then(function (response) {
              
                if (response.data.Respuesta.Error == "0") {
                    window.location.href = response.data.Respuesta.Mensaje;
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', response.data.Respuesta.Mensaje, 5); 
                }

                $timeout(function () {
                    blockUI.stop();
                }, 2500);

            }, function (response) {

                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', UtilsFactory.GetMensajeError(response), 6);
            });
        };

        function CargaModalCambioClave() {

            var promise = CambioClaveService.ModalCambiarClave();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCambioClave").html(content);
                });

                $('#ModalCambioClave').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaModalOlvidoClave() {

            var promise = OlvidoClaveService.ModalOlvidoClave();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoOlvidoClave").html(content);
                });

                $('#ModalOlvidoClave').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };


    }

})();

