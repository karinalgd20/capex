﻿(function () {
    'use strict',
     angular
    .module('app.Seguridad')
    .service('BandejaUsuarioService', BandejaUsuarioService);

    BandejaUsuarioService.$inject = ['$http', 'URLS'];

    function BandejaUsuarioService($http, $urls) {

        var service = {
            ListarUsuariosPaginado: ListarUsuariosPaginado, 
            ListarUsuariosFiltro: ListarUsuariosFiltro,
            ActualizarEstadoUsuario: ActualizarEstadoUsuario
        };

        return service;

        function ListarUsuariosPaginado(request) {
            return $http({
                url: $urls.ApiSeguridad + "BandejaUsuario/ListarUsuariosPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarUsuariosFiltro(request) {
            return $http({
                url: $urls.ApiSeguridad + "BandejaUsuario/ListarUsuariosFiltro",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


        function ActualizarEstadoUsuario(request) {
            return $http({
                url: $urls.ApiSeguridad + "BandejaUsuario/ActualizarEstadoUsuario",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
         

    }
})();