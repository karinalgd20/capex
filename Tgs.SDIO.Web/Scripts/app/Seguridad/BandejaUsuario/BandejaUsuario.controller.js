﻿(function () {
    'use strict'
    angular
        .module('app.Seguridad')
        .controller('BandejaUsuarioController', BandejaUsuarioController);

    BandejaUsuarioController.$inject = ['BandejaUsuarioService', 'RegistrarUsuarioService','RegistrarRecursoLineaNegocioService', 'RegistrarUsuarioPerfilService','RegistrarRecursoJefeService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function BandejaUsuarioController(BandejaUsuarioService,RegistrarUsuarioService,RegistrarRecursoLineaNegocioService,RegistrarUsuarioPerfilService,RegistrarRecursoJefeService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector)
    {
        var vm = this; 
        vm.ListarUsuariosPaginado = ListarUsuariosPaginado;
        vm.ActualizarEstadoUsuario = ActualizarEstadoUsuario;
        vm.ModalRegistrarUsuario = ModalRegistrarUsuario;
        vm.ModalRegistrarUsuarioPerfil= ModalRegistrarUsuarioPerfil;
        vm.ModalRegistrarRecursoLineaNegocio = ModalRegistrarRecursoLineaNegocio;
        vm.ModalRegistrarRecursoJefe = ModalRegistrarRecursoJefe;
        vm.LimpiarFiltros = LimpiarFiltros;        

        vm.Apellidos = "";
        vm.Nombres = "";
        vm.Login = "";
        vm.IdUsuario = "";
        vm.NombreUsuario = "";
        vm.IdUsuarioSistemaEmpresa = "";
         
        vm.dtInstance = {};
        vm.dtColumns = [
            DTColumnBuilder.newColumn('Login').withTitle('Login').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('Apellidos').withTitle('Apellidos').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('Nombres').withTitle('Nombres').notSortable().withOption('width', '30%'),            
            DTColumnBuilder.newColumn('EstadoRegistro').withTitle('Estado').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];

        LimpiarGrilla();

        function DatosUsuarios(usuario)
        { 
            vm.IdUsuario = usuario.IdUsuario;
            vm.NombreUsuario = usuario.Nombres + ' ' + usuario.Apellidos;
            vm.IdUsuarioSistemaEmpresa = usuario.IdUsuarioSistemaEmpresa;
            
        }

        function ModalRegistrarUsuario(idUsuario) {
            vm.IdUsuario = idUsuario;

            var promise = RegistrarUsuarioService.ModalRegistrarUsuario();
            promise.then(function (response) {
                var respuesta = $(response.data);
                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoDatosUsuario").html(content);
                });

                $('#ModalRegistrarUsuario').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });
        };

        function ModalRegistrarUsuarioPerfil(usuario) {
            DatosUsuarios(usuario);
            var promise = RegistrarUsuarioPerfilService.ModalRegistrarUsuarioPerfil();
            promise.then(function (response) {
                var respuesta = $(response.data);
                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoUsuarioPerfil").html(content);
                });

                $('#ModalRegistrarUsuarioPerfil').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });
        };

        function ModalRegistrarRecursoLineaNegocio(usuario) {
            DatosUsuarios(usuario);
            var promise = RegistrarRecursoLineaNegocioService.ModalRegistrarRecursoLineaNegocio();
            promise.then(function (response) {
                var respuesta = $(response.data);
                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoRecursoLineaNegocio").html(content);
                });

                $('#ModalRegistrarRecursoLineaNegocio').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });
        };


        function ModalRegistrarRecursoJefe(usuario) {
            DatosUsuarios(usuario);
            var promise = RegistrarRecursoJefeService.ModalRegistrarRecursoJefe();
            promise.then(function (response) {
                var respuesta = $(response.data);
                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoRecursoJefe").html(content);
                });

                $('#ModalRegistrarRecursoJefe').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });
        };

         
        function ActualizarEstadoUsuario(id, estado)
        {
            var etiquetaEstado = (estado==1) ? "inactivar" : "activar";
                         
            if (confirm('¿Estas seguro de ' + etiquetaEstado + ' el registro?'))
            {
                blockUI.start();

                var usuarioDtoRequest = {
                    IdEstado: (estado == 1) ? 2 : 1,
                    IdUsuario:id
                };

                var promise = BandejaUsuarioService.ActualizarEstadoUsuario(usuarioDtoRequest);

                promise.then(function (resultado) {
                     
                    var respuesta = resultado.data;

                    if (respuesta.TipoRespuesta == 1) {
                        UtilsFactory.Alerta('#divAlert', 'success', respuesta.Mensaje, 10);
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', respuesta.Mensaje, 10);
                    }

                    ListarUsuariosPaginado();

                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
            }
        }
                
        function ListarUsuariosPaginado() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(ListarUsuarios)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                       .withOption('createdRow', function (row, data, dataIndex) {
                           $compile(angular.element(row).contents())($scope);
                       })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }

        function ListarUsuarios(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            
            var usuarioDtoRequest = {
                Apellidos: vm.Apellidos,
                Nombres: vm.Nombres,
                Login: vm.Login,  
                NroPagina:pageNumber,
                RegistrosPagina:length 
            };

            var promise = BandejaUsuarioService.ListarUsuariosPaginado(usuarioDtoRequest);
             
            promise.then(function (resultado) { 

                var result = {
                    'draw': draw,
                    'recordsTotal': (resultado.data[0] == null) ? 0 : resultado.data[0].TotalRegistros,
                    'recordsFiltered': (resultado.data[0] == null) ? 0 : resultado.data[0].TotalRegistros,
                    'data': resultado.data
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
       
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('paging', false);
        }
        
        function AccionesBusqueda(data, type, full, meta) { 
            return "<div class='col-xs-6 col-sm-12'><a title='Editar' class='btn btn-primary'  id='tab-button' data-toggle='modal' ng-click='vm.ModalRegistrarUsuario(" + data.IdUsuario + ");'>" + "<span class='fa fa-edit fa-lg'></span>" + "</a>  " +
                 "  <a title='Cambiar Estado' class='btn btn-primary'   id='tab-button' class='btn '  ng-click='vm.ActualizarEstadoUsuario(" + data.IdUsuario + "," + data.IdEstadoRegistro + ");'> <span class='fa fa-eye-slash fa-lg'></span></a>  " +
                 "  <a title='Perfiles' class='btn btn-primary'   id='tab-button' class='btn '  ng-click='vm.ModalRegistrarUsuarioPerfil(" + JSON.stringify(data) +");'>" + "<span class='fa fa-user fa-lg'></span>" + "</a>  " +
                 "  <a title='Personal' class='btn btn-primary'   id='tab-button' class='btn '  ng-click='vm.ModalRegistrarRecursoJefe(" + JSON.stringify(data) + ");'>" + "<span class='fa fa-users fa-lg'></span>" + "</a> " +
                 "  <a title='Lineas' class='btn btn-primary'   id='tab-button' class='btn '  ng-click='vm.ModalRegistrarRecursoLineaNegocio(" + JSON.stringify(data) + ");'>" + "<span class='fa fa-tags fa-lg'></span>" + "</a> </div>";
        }
        
        function LimpiarFiltros() {
            vm.Apellidos = '';
            vm.Nombres = '';
            vm.Login = '';
            LimpiarGrilla();
        }
    }
})();