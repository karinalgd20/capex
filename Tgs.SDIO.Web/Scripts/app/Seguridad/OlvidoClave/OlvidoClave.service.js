﻿(function () {
    'use strict',
    angular
    .module('app.Seguridad')
    .service('OlvidoClaveService', OlvidoClaveService);

    OlvidoClaveService.$inject = ['$http', 'URLS'];

    function OlvidoClaveService($http, $urls) {

        var service = { 
            EnviarClave: EnviarClave,
            ModalOlvidoClave: ModalOlvidoClave
        };

        return service;
         
        function EnviarClave(UsuarioDto) {
            return $http({
                url: $urls.ApiSeguridad + "OlvidoClave/EnviarClave",
                method: "POST",
                data: JSON.stringify(UsuarioDto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ModalOlvidoClave(UsuarioDto) {
            return $http({
                url: $urls.ApiSeguridad + "OlvidoClave/Index",
                method: "POST",
                data: JSON.stringify(UsuarioDto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };


      
    }

})();

