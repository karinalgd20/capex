﻿(function () {
    'use strict',
     angular
    .module('app.Seguridad')
    .service('RegistrarUsuarioService', RegistrarUsuarioService);

    RegistrarUsuarioService.$inject = ['$http', 'URLS'];

    function RegistrarUsuarioService($http, $urls) {

        var service = {
            ModalRegistrarUsuario: ModalRegistrarUsuario,
            RegistrarUsuario: RegistrarUsuario,
            ActualizarUsuario: ActualizarUsuario,
            ObtenerUsuarioPorId: ObtenerUsuarioPorId
        };

        return service; 
         
        function ModalRegistrarUsuario() {
            return $http({
                url: $urls.ApiSeguridad + "RegistrarUsuario/Index",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarUsuario(request) {
            return $http({
                url: $urls.ApiSeguridad + "RegistrarUsuario/RegistrarUsuario",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarUsuario(request) {
            return $http({
                url: $urls.ApiSeguridad + "RegistrarUsuario/ActualizarUsuario",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
        function ObtenerUsuarioPorId(request) {
            return $http({
                url: $urls.ApiSeguridad + "RegistrarUsuario/ObtenerUsuarioPorId",
                method: "POST",
                data: { Id: request }
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();