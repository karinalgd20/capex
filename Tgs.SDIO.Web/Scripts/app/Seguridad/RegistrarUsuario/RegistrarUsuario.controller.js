﻿(function () {
    'use strict'
    angular
    .module('app.Seguridad')
    .controller('RegistrarUsuarioController', RegistrarUsuarioController);

    RegistrarUsuarioController.$inject = ['RegistrarUsuarioService', 'AreaService', 'CargoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarUsuarioController(RegistrarUsuarioService, AreaService, CargoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vmregusu = this;
    
        vmregusu.RegistrarUsuario = RegistrarUsuario;  

        vmregusu.ListaArea = [];
        vmregusu.ListaCargo = [];          

        vmregusu.Login = "";
        vmregusu.Email = "";
        vmregusu.Nombre = "";
        vmregusu.Apellido = "";
        vmregusu.IdArea = "-1";
        vmregusu.IdCargo = "-1";
        vmregusu.FechaCaducidad = "";
        vmregusu.IntentosFallidos = "0";
        vmregusu.Clave = "";
        vmregusu.RepetirClave = "";
        vmregusu.CambioClavePeriodo = false;
        vmregusu.IdUsuario = 0;
        vmregusu.MostrarClave = false;
        vmregusu.BloquearCajaLogin = true;
        CargarVista();         

        function ListarAreas() {
            var areaDtoRequest = {
                IdEstado: 1
            };

            var promise = AreaService.ListarAreas(areaDtoRequest);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;
                vmregusu.ListaArea = UtilsFactory.AgregarItemSelect(respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
                 
        function ListarCargos()
        {
            var cargoDtoRequest = {
               IdEstado: 1
            };

            var promise = CargoService.ListarCargos(cargoDtoRequest);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;
                vmregusu.ListaCargo = UtilsFactory.AgregarItemSelect(respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function CargarVista() {

            blockUI.start();

            vmregusu.IdUsuario = $scope.$parent.vm.IdUsuario;
           
            if (vmregusu.IdUsuario > 0) {
              
                var promise = RegistrarUsuarioService.ObtenerUsuarioPorId(vmregusu.IdUsuario);
                promise.then(function (resultado) {
                   
                    var respuesta = resultado.data;

                    vmregusu.Login = respuesta.Login;
                    vmregusu.Email = respuesta.CorreoElectronico;
                    vmregusu.Nombre = respuesta.Nombres;
                    vmregusu.Apellido = respuesta.Apellidos;
                    vmregusu.IdArea = (respuesta.IdArea > 0) ? respuesta.IdArea : "-1";
                    vmregusu.IdCargo = (respuesta.IdCargo > 0) ? respuesta.IdCargo : "-1";
                    vmregusu.CambioClavePeriodo = respuesta.FlagSolicitarCambioClave;
                    vmregusu.FechaCaducidad = moment(respuesta.FechaCaducidad).format("DD/MM/YYYY");
                    vmregusu.IntentosFallidos = respuesta.IntentosFallidos;
                   
                    ListarAreas();
                    ListarCargos(); 
                    blockUI.stop();

                }, function (response) {
                    blockUI.stop();
                }); 
            } else { 
                ListarAreas();
                ListarCargos();
                vmregusu.MostrarClave = true;
                vmregusu.BloquearCajaLogin = false;
            } 
        };
           
        function RegistrarUsuario()
        {
            if ($.trim(vmregusu.Login) == '') {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese un login", 5); 
                return;
            }

            if ($.trim(vmregusu.Email) == '') {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese un email", 5); 
                return;
            }

            if ($.trim(vmregusu.Nombre) == '') {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese un nombre", 5); 
                return;
            }

            if ($.trim(vmregusu.Apellido) == '') {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese un apellido", 5); 
                return;
            }

            if ($.trim(vmregusu.Clave) == '' && vmregusu.IdUsuario == 0) {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese una contraseña", 5); 
                return;
            }

            if (vmregusu.Clave != vmregusu.RepetirClave && vmregusu.IdUsuario == 0) {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "La contraseña y la confirmación no coinciden", 5); 
                return;
            }

            if ($.trim(vmregusu.IdCargo) == '-1') {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese un cargo", 5); 
                return;
            }

            if ($.trim(vmregusu.IdArea) == '-1') {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese un área", 5); 
                return;
            }

            if ($.trim(vmregusu.FechaCaducidad) == "") { 
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese la fecha de caducidad", 5);
                return;
            }

            if (!UtilsFactory.ValidarFecha(vmregusu.FechaCaducidad)) {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese una fecha valida de caducidad", 5);
                return;
            }

            blockUI.start();

            var usuarioDtoRequest = {
                Login: vmregusu.Login,
                Password: vmregusu.Clave,
                IdUsuario: vmregusu.IdUsuario,
                Apellidos: vmregusu.Apellido,
                Nombres: vmregusu.Nombre,
                CorreoElectronico: vmregusu.Email,
                IntentosFallidos: vmregusu.IntentosFallidos,
                FechaCaducidad: vmregusu.FechaCaducidad,
                SolicitaCambioClave: vmregusu.CambioClavePeriodo,
                IdCargo: vmregusu.IdCargo,
                IdArea: vmregusu.IdArea
            };

            var promise = (usuarioDtoRequest.IdUsuario > 0) ? RegistrarUsuarioService.ActualizarUsuario(usuarioDtoRequest) : RegistrarUsuarioService.RegistrarUsuario(usuarioDtoRequest);
            
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;

                vmregusu.IdUsuario = respuesta.Id;

                if (respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlertRegistrar', 'success', respuesta.Mensaje, 10);
                    vmregusu.BloquearCajaLogin = true;
                    $scope.$parent.vm.ListarUsuariosPaginado();
                } else {                    
                    UtilsFactory.Alerta('#divAlertRegistrar', 'danger', respuesta.Mensaje, 10);
                } 
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', MensajesUI.DatosError, 5);
            });
        } 
       
    }

})();