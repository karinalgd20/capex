﻿(function () {
    'use strict'
    angular
    .module('app.Seguridad')
    .controller('RegistrarUsuarioPerfilController', RegistrarUsuarioPerfilController);

    RegistrarUsuarioPerfilController.$inject = ['RegistrarUsuarioPerfilService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarUsuarioPerfilController(RegistrarUsuarioPerfilService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vmper = this;
        vmper.ListaUsuarioPerfil = [];
        vmper.CodigoPerfil = "-1";

        vmper.IdUsuario = $scope.$parent.vm.IdUsuario;
        vmper.NombreUsuario = $scope.$parent.vm.NombreUsuario;
        vmper.IdUsuarioSistemaEmpresa = $scope.$parent.vm.IdUsuarioSistemaEmpresa; 
        vmper.ListarPerfilesPorSistema = ListarPerfilesPorSistema;
        vmper.RegistrarUsuarioPerfil = RegistrarUsuarioPerfil;
        vmper.ActualizarEstadoUsuarioPerfil = ActualizarEstadoUsuarioPerfil;
        vmper.ListarUsuarioPerfilPaginado = ListarUsuarioPerfilPaginado; 

        LimpiarGrilla();
        ListarPerfilesPorSistema();
        ListarUsuarioPerfilPaginado();

        vmper.dtInstanceUsuarioPerfil = {};
        vmper.dtColumnsUsuarioPerfil = [
            DTColumnBuilder.newColumn('Perfil').withTitle('Perfil').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('Estado').withTitle('Estado').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ]; 

        function ListarPerfilesPorSistema() { 

            var promise = RegistrarUsuarioPerfilService.ListarPerfilesPorSistema();

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;
                vmper.ListaUsuarioPerfil = UtilsFactory.AgregarItemSelect(respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function AccionesBusqueda(data, type, full, meta) {
            return "<a title='Cambiar Estado' class='btn btn-primary'  id='tab-button' class='btn '  ng-click='vmper.ActualizarEstadoUsuarioPerfil(" + data.IdPerfil + "," + data.IdEstadoRegistro + ");'> <span class='fa fa-eye-slash fa-lg'></span></a>  ";
        } 

        function RegistrarUsuarioPerfil()
        {
            if (vmper.CodigoPerfil == '-1') {
                UtilsFactory.Alerta('#divAlertUsuarioPerfil', 'danger', "Seleccione un perfil", 5);
                blockUI.stop();
                return;
            }

            blockUI.start();

            var usuarioPerfilDtoRequest = {
                IdUsuarioSistemaEmpresa: vmper.IdUsuarioSistemaEmpresa,
                IdUsuarioAsignado: vmper.IdUsuario ,
                IdPerfil: vmper.CodigoPerfil
            };

            var promise = RegistrarUsuarioPerfilService.RegistrarUsuarioPerfil(usuarioPerfilDtoRequest);

            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data; 

                if (respuesta.TipoRespuesta == 1) { 
                    UtilsFactory.Alerta('#divAlertUsuarioPerfil', 'success', respuesta.Mensaje, 10);                   
                    ListarUsuarioPerfilPaginado();
                } else {
                    UtilsFactory.Alerta('#divAlertUsuarioPerfil', 'danger', respuesta.Mensaje, 10);
                } 
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertUsuarioPerfil', 'danger', MensajesUI.DatosError, 5);
            });
        }

        function ActualizarEstadoUsuarioPerfil(idPerfil, estado)
        {
            var etiquetaEstado = (estado == 1) ? "inactivar" : "activar";

            if (confirm('¿Estas seguro de ' + etiquetaEstado + ' el registro?')) {
                blockUI.start();

                var usuarioDtoRequest = {
                    IdEstado: (estado == 1) ? 2 : 1,
                    IdUsuarioSistemaEmpresa: vmper.IdUsuarioSistemaEmpresa,
                    IdPerfil: idPerfil
                };

                var promise = RegistrarUsuarioPerfilService.ActualizarEstadoUsuarioPerfil(usuarioDtoRequest);

                promise.then(function (resultado) {

                    var respuesta = resultado.data;

                    if (respuesta.TipoRespuesta == 1) {
                        UtilsFactory.Alerta('#divAlertUsuarioPerfil', 'success', respuesta.Mensaje, 10);
                    } else {
                        UtilsFactory.Alerta('#divAlertUsuarioPerfil', 'danger', respuesta.Mensaje, 10);
                    }

                    ListarUsuarioPerfilPaginado();

                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertUsuarioPerfil', 'danger', MensajesUI.DatosError, 5);
                });
            }
        }

        function ListarUsuarioPerfilPaginado() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vmper.dtOptionsUsuarioPerfil = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(ListarUsuarioPerfil)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                       .withOption('createdRow', function (row, data, dataIndex) {
                           $compile(angular.element(row).contents())($scope);
                       })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }

        function ListarUsuarioPerfil(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
             
            var usuarioDtoRequest = {
                IdUsuarioAsignado: vmper.IdUsuario,
                IdEstadoRegistro: 0, 
                NroPagina: pageNumber,
                RegistrosPagina: length
            };

            var promise = RegistrarUsuarioPerfilService.ListarPerfilesAsignados(usuarioDtoRequest);

            promise.then(function (resultado) {

                var result = {
                    'draw': draw,
                    'recordsTotal': (resultado.data[0]==null)? 0: resultado.data[0].TotalRegistros,
                    'recordsFiltered': (resultado.data[0] == null) ? 0 : resultado.data[0].TotalRegistros,
                    'data': resultado.data
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function LimpiarGrilla() {
            vmper.dtOptionsUsuarioPerfil = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('paging', false);
        }

    }

})();