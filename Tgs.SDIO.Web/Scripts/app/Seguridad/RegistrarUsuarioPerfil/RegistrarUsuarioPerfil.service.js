﻿(function () {
    'use strict',
     angular
    .module('app.Seguridad')
    .service('RegistrarUsuarioPerfilService', RegistrarUsuarioPerfilService);

    RegistrarUsuarioPerfilService.$inject = ['$http', 'URLS'];

    function RegistrarUsuarioPerfilService($http, $urls) {

        var service = {
            RegistrarUsuarioPerfil: RegistrarUsuarioPerfil,
            ListarPerfilesPorSistema: ListarPerfilesPorSistema,
            ListarPerfilesAsignados: ListarPerfilesAsignados,
            ActualizarEstadoUsuarioPerfil: ActualizarEstadoUsuarioPerfil,
            ModalRegistrarUsuarioPerfil: ModalRegistrarUsuarioPerfil
        };

        return service; 
         
        function ModalRegistrarUsuarioPerfil() {
            return $http({
                url: $urls.ApiSeguridad + "RegistrarUsuarioPerfil/Index",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarUsuarioPerfil(request) {
            return $http({
                url: $urls.ApiSeguridad + "RegistrarUsuarioPerfil/RegistrarUsuarioPerfil",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
         
        function ListarPerfilesPorSistema() {
            return $http({
                url: $urls.ApiSeguridad + "RegistrarUsuarioPerfil/ListarPerfilesPorSistema",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarPerfilesAsignados(request) {
            return $http({
                url: $urls.ApiSeguridad + "RegistrarUsuarioPerfil/ListarPerfilesAsignados",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarEstadoUsuarioPerfil(request) {
            return $http({
                url: $urls.ApiSeguridad + "RegistrarUsuarioPerfil/ActualizarEstadoUsuarioPerfil",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();