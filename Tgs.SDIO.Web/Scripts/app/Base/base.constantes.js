﻿(function () {
    'use strict';

    angular
        .module('app.Base')
        .constant('URLS', { 
            ConsolidadoService: "http://localhost:43180/SalesForce/",
            ApiSeguridad: "http://localhost:43180/Seguridad/",
            ApiPrincipal: "http://localhost:43180/Principal/",
            ApiTrazabilidad: "http://localhost:43180/Trazabilidad/",
            ApiOportunidad: "http://localhost:43180/Oportunidad/",
            ApiComun: "http://localhost:43180/Comun/",
            ApiFunnel: "http://localhost:43180/Funnel/",
            ApiReportes: "/Reportes/frmContenedor.aspx?",
            ApiCartaFianza: "http://localhost:43180/CartaFianza/",
            ApiPlantaExterna: "http://localhost:43180/PlantaExterna/",
            ApiNegocio: "http://localhost:43180/Negocio/",
            ApiProyecto: "http://localhost:43180/Proyecto/",
            ApiCapex: "http://localhost:43180/Capex/",
			ApiCompra: "http://localhost:43180/Compra/",
        })
        .constant('MensajesUI', {
            DatosOk: "Los datos fueron grabados correctamente",
            DatosOkCorreo: "El correo fue enviado correctamente",
            DatosDeleteOk: "Los datos fueron Eliminados correctamente",
            DatosError: "No se pudo realizar la acción, consulte con el administrador",
            DetalleGeneradoOk: "Registros generados correctamente",
            DetalleGeneradoError: "Error al generara el proyectado"
        });
})();
