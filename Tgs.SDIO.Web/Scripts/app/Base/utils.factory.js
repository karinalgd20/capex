﻿(function () {
    'use strict';
    angular
    .module('app.Base')
    .factory('UtilsFactory', UtilsFactory);
     
    UtilsFactory.$inject = ['$alert', '$timeout', 'MensajesUI'];

    function UtilsFactory($alert, $timeout, $MensajesUI) {
        var vm = this;

        vm.alert = "";

        var factory = {
            AgregarItemSelect: AgregarItemSelect,
            Alerta: Alerta,
            CerrarAlerta: CerrarAlerta,
            InputBorderColor: InputBorderColor,
            HabilitarElemento: HabilitarElemento,
            DeshabilitarElemento: DeshabilitarElemento,
            LimpiarLista: LimpiarLista,
            ValidarFecha: ValidarFecha,
            AgregarItemSelectTodos: AgregarItemSelectTodos,
            AgregarItemSelectMaestra:AgregarItemSelectMaestra,
            AgregarItemSelectLabel:AgregarItemSelectLabel,
            ObtenerExtensionArchivo: ObtenerExtensionArchivo,
            ValidarNumero: ValidarNumero,
            ValidarEmail: ValidarEmail,
            ValidarVacio: ValidarVacio,
            ValidarSoloLetras: ValidarSoloLetras,
            OcultarElemento: OcultarElemento,
            MostrarElemento: MostrarElemento,
            CompararFechas: CompararFechas,
            ObtenerNroDiasFechas: ObtenerNroDiasFechas,
            CompletarCadena: CompletarCadena,
            ObtenerFechaActual: ObtenerFechaActual,
            AgregarDias: AgregarDias,
            TokenAutorizacion: TokenAutorizacion,
            GetUrlAbsoluta: GetUrlAbsoluta,
            GetMensajeError: GetMensajeError,
            ToJavaScriptDate: ToJavaScriptDate,
            AgregarItemSelectCosto: AgregarItemSelectCosto
        }; 


        return factory;

        function AgregarItemSelect(data) {
            var lista = data;            
            var item = { "Codigo": "-1", "Descripcion": "--Seleccione--" };
           lista.splice(0, 0, item);
            return lista;
        };
        function AgregarItemSelectMaestra(data) {
            var lista = data;
            var item = { "Valor": "-1", "Descripcion": "--Seleccione--" };
            lista.splice(0, 0, item);
            return lista;
        };
        function AgregarItemSelectTodos(data) {
            var lista = data;
            var item = { "Codigo": "0", "Descripcion": "--Todos--" };
            lista.splice(0, 0, item);
            return lista;
        };
        function AgregarItemSelectLabel(data) {
            var lista = data;
            var item = { "id": "-1", "label": "--Seleccione--" };
            lista.splice(0, 0, item);
            return lista;
        };
        function AgregarItemSelectCosto(data) {
            var lista = data;
            var item = { "IdCosto": -1, "Descripcion": "--Seleccione--" };
            lista.splice(0, 0, item);
            return lista;
        };
        function Alerta(elemento, tipoMsj, mensaje, duracion) { 
             
            var titulo = "";
            if (tipoMsj == "danger") {
                titulo = "Error:";
            } else if (tipoMsj == "info") {
                titulo = " Info:";
            } else if (tipoMsj == "warning") {
                titulo = " Advertencia:";
            }
            else {
                titulo = "Correcto:";
            }
              
            vm.alert= $alert({
                title: titulo,
                content: mensaje,
                container: elemento ,  
                type: tipoMsj ,       
                dismissable: true,
                duration: duracion,
                show: false,
                animation: 'am-fade'
            });
             
            $timeout(function () {
                vm.alert.$promise.then(function () {
                    vm.alert.show();
                });
            }, 1000);            
        };

    
        function CerrarAlerta() {
            if (vm.alert!= "")
            {
                vm.alert.$promise.then(function () {
                    vm.alert.hide();
                });
            } 
        }

        function InputBorderColor(InputId,Color){
            var FailedCss = { "border": '2px solid red' };
            var SuccessCss = { "border": '2px solid green' };
            var nothingCss = { "border": '' };
            if (Color == 'Rojo') {
                $(InputId).css(FailedCss);
            }
            if (Color == 'Verde') {
                $(InputId).css(SuccessCss);
            }
            if (Color == 'Ninguno') {
                $(InputId).css(nothingCss);
            }
            if (Color == 'combo') {
                $(InputId).css(nothingCss);
            }
        }

        function LimpiarLista(objetoLista) {
            return objetoLista.filter(function (item) {
                return !!item;
            });
        }
         

        function HabilitarElemento(IdItem) {
           $(IdItem).prop("disabled", "");
        }

        function DeshabilitarElemento(IdItem) {
           $(IdItem).prop("disabled", "disabled");
        }

        function ValidarFecha(fecha) {
            var expReg = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;
            return expReg.test(fecha);
        } 

        function ObtenerExtensionArchivo(filename) {
            var ext = /^.+\.([^.]+)$/.exec(filename);
            return ext == null ? "" : ext[1];
        }

        function ValidarNumero(Value) {
            var expReg = new RegExp('^[0-9]+$');
            return expReg.test(Value);
        }

        function ValidarEmail(Value) {
            var expReg = new RegExp('^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$');
            return expReg.test(Value);
        }

        function ValidarVacio(Value) {
            var expReg = new RegExp('\S+');
            return expReg.test(Value);
        } 

        function ValidarSoloLetras(Value) {
            var expReg = new RegExp("^[A-Za-z ]+$");
            return expReg.test(Value);
        }

        function OcultarElemento(item) {
            $(item).hide();
        }

        function MostrarElemento(item) {
            $(item).show();
        }

        function CompararFechas(fechaInicio, fechaFin)
        {
            var xMonth = fechaInicio.substring(3, 5);
            var xDay = fechaInicio.substring(0, 2);
            var xYear = fechaInicio.substring(6, 10);
            var yMonth = fechaFin.substring(3, 5);
            var yDay = fechaFin.substring(0, 2);
            var yYear = fechaFin.substring(6, 10);

            if (xYear > yYear) {
                return (true)
            }
            else {
                if (xYear == yYear) {
                    if (xMonth > yMonth) {
                        return (true)
                    }
                    else {
                        if (xMonth == yMonth) {
                            if (xDay > yDay)
                                return (true);
                            else
                                return (false);
                        }
                        else
                            return (false);
                    }
                }
                else
                    return (false);
            }
        }

        function ObtenerNroDiasFechas(fechaInicio, fechaFin)
        {
            var FechaFin = new Date(fechaFin.substring(fechaFin.indexOf("/") + 1, fechaFin.lastIndexOf("/")) + "/" + fechaFin.substring(0, fechaFin.indexOf("/")) + "/" + fechaFin.substring(fechaFin.lastIndexOf("/") + 1, fechaFin.length));
            var FechaInicio = new Date(fechaInicio.substring(fechaInicio.indexOf("/") + 1, fechaInicio.lastIndexOf("/")) + "/" + fechaInicio.substring(0, fechaInicio.indexOf("/")) + "/" + fechaInicio.substring(fechaInicio.lastIndexOf("/") + 1, fechaInicio.length));

            FechaResta = FechaFin - FechaInicio;
            NroDias = (((FechaResta / 1000) / 60) / 60) / 24;

            return NroDias;
            
        }

        function CompletarCadena(cadena, nroCaracteres) {
            if (cadena.length > 0) {
                var cadena = cadena.toString();
                while (cadena.length < nroCaracteres) {
                    cadena = "0" + cadena;
                }                
            }
            return cadena;
        }

        function ObtenerFechaActual() {
            return moment().format("DD/MM/YYYY");
        }

        function AgregarDias(fecha,dias) {
            var nuevaFecha = moment(fecha, "DD/MM/YYYY");
            return nuevaFecha.add(dias, 'days').format('DD/MM/YYYY');
        }

        function TokenAutorizacion() {
            return $("input[name='__RequestVerificationToken']").val();
        }
        
        function GetUrlAbsoluta() {
            if (siteFullUrl[siteFullUrl.length - 1] == '/') {
                return siteFullUrl;
            } else {
                return siteFullUrl + '/';
            }
        }

        function GetMensajeError(response) {
            var mensajeError = '';
            if (response.data.Respuesta == undefined) {
                mensajeError = $MensajesUI.DatosError;
            } else {
                mensajeError = response.data.Respuesta.Mensaje;
                if (mensajeError == null || mensajeError == '') {
                    mensajeError = $MensajesUI.DatosError;
                }
            }
            return mensajeError;
        }

        function ToJavaScriptDate(value) { //To Parse Date from the Returned Parsed Date
            var pattern = /Date\(([^)]+)\)/;
            var results = pattern.exec(value);
            var dt = new Date(parseFloat(results[1]));

            var dia = dt.getDate()<10?'0'+dt.getDate():dt.getDate();
            var mes = dt.getMonth() < 9 ? '0' + (dt.getMonth()+1) : (dt.getMonth()+1);

            return dia + "/" + mes + "/" + dt.getFullYear();
        }
    }

})();
