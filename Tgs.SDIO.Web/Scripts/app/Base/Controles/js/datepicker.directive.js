﻿(function () {
    'use strict' 

    angular
    .module('app.Base')
    .directive('dateTimePicker', dateTimePicker);

    function dateTimePicker() {
        var directive = {
            restrict: "A",
            require: "ngModel",
            link: function (scope, element, attrs, ngModelCtrl) {
                var parent = $(element).parent();

                var mindateParam = attrs.minDate;
                var mindate;

                if (mindateParam != undefined && mindateParam.trim() != "") {
                    if (mindateParam == "today") {
                        mindate = new Date();
                    }else{
                        mindate = new Date(mindateParam);
                    }
                }

                var dtp;

                if (mindate != undefined) {
                    dtp = parent.datetimepicker({
                        format: "DD/MM/YYYY",
                        minDate: mindate,
                        showTodayButton: true
                    });
                } else {
                    dtp = parent.datetimepicker({
                        format: "DD/MM/YYYY",
                        showTodayButton: true
                    });
                }
                
                dtp.on("dp.change", function (e) {
                    ngModelCtrl.$setViewValue(moment(e.date).format("DD/MM/YYYY"));
                    scope.$apply();
                });
            }
        };

        return directive; 
    }

})();

 