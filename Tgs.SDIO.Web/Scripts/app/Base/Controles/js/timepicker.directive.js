﻿(function () {
    'use strict' 

    angular
    .module('app.Base')
    .directive('timePicker', timePicker);

    function timePicker() {
        var directive = {
            restrict: "A",
            require: "ngModel",
            link: function (scope, element, attrs, ngModelCtrl) {
                var parent = $(element).parent();
                var dtp = parent.datetimepicker({
                    format: "HH:mm"
                });
                dtp.on("dp.change", function (e) {
                    ngModelCtrl.$setViewValue(moment(e.date).format("HH:mm"));
                    scope.$apply();
                });
            }
        };

        return directive; 
    }

})();

 