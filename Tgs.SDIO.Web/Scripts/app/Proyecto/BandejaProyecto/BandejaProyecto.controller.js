(function () {
    'use strict'
    angular
        .module('app.Proyecto')
        .controller('BandejaProyectoController', BandejaProyectoController);

        BandejaProyectoController.$inject = ['BandejaProyectoService',
        'RegistroProyectoService', 'PrePeticionCompraService', 'EtapaOportunidadService', 'RecursoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function BandejaProyectoController(BandejaProyectoService,
        RegistroProyectoService, PrePeticionCompraService, EtapaOportunidadService, RecursoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector)
    {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiProyecto + "RegistrarProyecto/Index/";
        vm.BuscarProyecto = BuscarProyecto;
        vm.ActualizarJP = ActualizarJP;
        vm.GuardarPEPCeCo = GuardarPEPCeCo;
        vm.ObtenerPEPCeCo = ObtenerPEPCeCo;
        vm.MensajeDeAlerta = "";
        
        vm.LineasCMI = 0;
        vm.MostrarPrePeticionCompra = MostrarPrePeticionCompra;

        vm.ListaJefes = [];
        vm.Apellidos = "";
        vm.Nombres = "";
        vm.Login = "";
        vm.IdEtapa = -1;
        vm.IdProyectoLKM = "";
        vm.Cliente = "";
        vm.FechaInicio = "";
        vm.FechaFin = "";
        vm.IdProyecto = "";

        vm.UsuarioActual = {};
        vm.CodigoPerfilControl = "CONTROL";
        vm.CodigoPerfilLiderJP = "LIDERPROY";
        ObtenerInfoUsuarioActual();
        vm.OptionPreVenta = jsonOptionPreVenta;

        ListarJefes();
        LimpiarGrilla();
        BuscarProyecto();
        ListarEtapas();

        vm.pepceco = {
            ElementoPEP: "",
            CentroGestor: "",
            CeCo: ""
        };

        vm.selected = {};

        vm.dtInstance = {};
        vm.dtColumns = [
            DTColumnBuilder.newColumn('IdProyectoLKM').withTitle('Código de Proyecto').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('DescripcionEtapa').withTitle('Etapa').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('ResposanbleEtapa').withTitle('ResponsableActual').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn('NombreCliente').withTitle('Cliente').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('JefeProyecto').withTitle('JP').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('IdProyectoSF').withTitle('Oportunidad').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ]; 

        vm.dtColumnsLP = [
            DTColumnBuilder.newColumn(null).withTitle('Ver Detalle').notSortable().withOption('className', 'text-center details-control').renderWith(IconExpandCollapse).withOption('width', '10%'),
            DTColumnBuilder.newColumn('IdProyectoLKM').withTitle('Código de Proyecto').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('DescripcionEtapa').withTitle('Etapa').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('ResposanbleEtapa').withTitle('ResponsableActual').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn('NombreCliente').withTitle('Cliente').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('JefeProyecto').withTitle('JP').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Asignar Jefe').notSortable().renderWith(SeleccionarJefe).withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ]; 

        vm.dtColumnsLineasCMI = [
            DTColumnBuilder.newColumn(null).withTitle('Ver Detalle').notSortable().withOption('className', 'text-center details-control').renderWith(IconExpandCollapse).withOption('width', '10%'),
            DTColumnBuilder.newColumn('IdProyectoLKM').withTitle('Código de Proyecto').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('DescripcionEtapa').withTitle('Etapa').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('ResposanbleEtapa').withTitle('ResponsableActual').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn('NombreCliente').withTitle('Cliente').notSortable().withOption('width', '10%')
        ]; 

        var HtmlLineasCMI = function (d, html) {
           if (!d) return ''
        
           var HtmlColumnaPerfilControl = "";

           if (vm.UsuarioActual.CodigoPerfil[0] == vm.CodigoPerfilControl || LineasCMI == 1){
                HtmlColumnaPerfilControl = '<th class="text-center sorting_disabled"><strong>Acciones</strong></th>';
           }
           
           return '<div class="table-responsive" style="padding:5px"><table cellpadding="5" cellspacing="0" border="0" style="width:100%;">'+
               '<thead class="cf">'+
                  '<th class="text-center sorting_disabled"><strong>Código</strong></th>'+
                  '<th class="text-center sorting_disabled"><strong>Linea</strong></th>'+
                  '<th class="text-center sorting_disabled"><strong>Sublinea</strong></th>'+
                  '<th class="text-center sorting_disabled"><strong>Codigo Servicio CMI</strong></th>'+
                  '<th class="text-center sorting_disabled"><strong>Servicio CMI</strong></th>' +
                  HtmlColumnaPerfilControl +
               '</thead><tbody>' + html + '</tbody>' +
               '</table></div>';
         }

        $('body').on('click', '.details-control', function() {
            var tr = $(this).closest('tr');
            var fila = vm.dtInstance.DataTable.row(tr);

            fila.child.hide();
            tr.removeClass('shown');
            
            ListarServiciosCMI(fila, tr);
        });
        
        $('body').on('click', '.ingresos', function() {
            var tr = $(this).closest('tr');
            
            var celdas = tr[0].childNodes;
            var ValorCelda = celdas[3].innerHTML;
            var iIdDetalleFinanciero = celdas[0].innerHTML;
            var iIdProyecto = celdas[1].innerHTML;
            /*celdas.forEach(celda => {
                console.log(celda.innerHTML)
            });*/

            MostrarPEPCeCo(ValorCelda, iIdDetalleFinanciero, iIdProyecto);
        });

        $('body').on('click', '.prepeticion', function() {
            var tr = $(this).closest('tr');
            
            var celdas = tr[0].childNodes;
            var ValorCelda = celdas[3].innerHTML;
            var iIdDetalleFinanciero = celdas[0].innerHTML;
            var iIdProyecto = celdas[1].innerHTML;
            /*celdas.forEach(celda => {
                console.log(celda.innerHTML)
            });*/

            MostrarPrePeticionCompra(iIdDetalleFinanciero, iIdProyecto);
        });

        function ObtenerInfoUsuarioActual(){
            blockUI.start();

            var promise = BandejaProyectoService.ObtenerInfoUsuarioActual();
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.UsuarioActual = Respuesta;

            }, function (response) {
                blockUI.stop();
            });
        }

        function SeleccionarJefe(data, type, full, meta) {
            let html = "<option value='0'>--Seleccione--</option>";
            vm.ListaJefes.forEach(e => {
                html += "<option value='" + e.IdRecurso + "'>" + e.Nombre + "</option>"
            });
            
            vm.selected[full.IdProyecto] = full.IdJefeProyecto;

            return '<select id="ddlJefe" class="form-control" ng-model="vm.selected[\'' + full.IdProyecto + '\']" ng-change="vm.ActualizarJP(' + full.IdProyecto + ',' + full.IdAsignacionJP + ')" >' + html + '</select>'
        }

        function IconExpandCollapse(data, type, full, meta) {
            return "<a><i class='fa fa-chevron-circle-right iec_" + full.IdProyecto + "' aria-hidden='true'></i></a>"
        }

        function AccionesBusqueda(data, type, full, meta) {
            return "<div class='col-xs-6 col-sm-6'><a title='Editar' class='btn btn-primary'  id='tab-button' href='" + vm.EnlaceRegistrar + data.IdProyecto + "' >" + "<span class='fa fa-edit fa-lg'></span>" + "</a></div>  "
        }

        function ListarServiciosCMI(fila, tr) {
            var RowData = fila.data();

            var promise = RegistroProyectoService.ListarServicioCMI(RowData.IdProyecto);
            promise.then(function (response) {
                var Respuesta = response.data;
                var HtmlLinea = "<tr><td colspan='6'>No hay data para mostrar.</td></tr>";
                
                if (Respuesta.length > 0){
                    Respuesta = Respuesta.filter(r => r.CostoOpex > 0);

                    HtmlLinea = "";
                    Respuesta.forEach(e => {
                        HtmlLinea += "<tr>";
                        HtmlLinea += "<td>" + e.IdDetalleFinanciero + "</td>";
                        HtmlLinea += "<td style='display:none'>" + RowData.IdProyecto + "</td>";
                        HtmlLinea += "<td>" + e.Linea;
                        HtmlLinea += "</td>";
                        HtmlLinea += "<td>" + e.SubLinea;
                        HtmlLinea += "</td>";
                        HtmlLinea += "<td>" + e.CodigoCMI;
                        HtmlLinea += "</td>";
                        HtmlLinea += "<td>" + e.Servicio;
                        HtmlLinea += "</td>";
                        
                        if (vm.UsuarioActual.CodigoPerfil[0] == vm.CodigoPerfilControl){
                            HtmlLinea += "<td>" + "<button class='btn btn-primary ingresos' ><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button>";
                            HtmlLinea += "</td>";
                        }
                        if (LineasCMI == 1){
                            HtmlLinea += "<td>" + "<button class='btn btn-primary prepeticion' ><i class='fa fa-align-justify' aria-hidden='true'></i></button>";
                            HtmlLinea += "</td>";
                        }

                        HtmlLinea += "</tr>";
                    });
                }

                var sNombreClase = $(".iec_" + RowData.IdProyecto).attr("class")

                if (sNombreClase.indexOf("fa-chevron-circle-down") > 0) {
                    fila.child.hide();
                    tr.removeClass('shown');
    
                    $(".iec_" + RowData.IdProyecto).removeClass("fa-chevron-circle-down");
                    $(".iec_" + RowData.IdProyecto).addClass("fa-chevron-circle-right");
    
                }else{
                    $(".iec_" + RowData.IdProyecto).addClass("fa-chevron-circle-down");
                    $(".iec_" + RowData.IdProyecto).removeClass("fa-chevron-circle-right");
    
                    fila.child(HtmlLineasCMI(RowData, HtmlLinea)).show();
                    tr.addClass('shown');
                }
            }, function (response) {
                blockUI.stop();
            });
        }

        function MostrarPEPCeCo(ValorCeldaLinea, iIdDetalleFinanciero, iIdProyecto) {
            blockUI.start();

            var promise = BandejaProyectoService.MostrarInfoPEPCeCo();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoPEPCeCo").html(content);
                });

                vm.Linea = ValorCeldaLinea;
                
                ObtenerPEPCeCo(iIdDetalleFinanciero, iIdProyecto);
                
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        };

        function ValidarPEPCeCo(){
            if (vm.pepceco.ElementoPEP == null){
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_PEPCeCo', 'danger', "Debe ingresar un Elemento PEP.", 5);
                return false;
            }

            if (vm.pepceco.CentroGestor == null){
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_PEPCeCo', 'danger', "Debe ingresar un Centro Gestor.", 5);
                return false;
            }

            if (vm.pepceco.CeCo == null){
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_PEPCeCo', 'danger', "Debe ingresar un CeCo.", 5);
                return false;
            }

            return true;
        }

        function GuardarPEPCeCo(){
            blockUI.start();

            if (!ValidarPEPCeCo()) return;

            let request = vm.pepceco;
            
            var promise = BandejaProyectoService.GuardarPEPCeCo(request);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0){
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_PEPCeCo', 'success', Respuesta.Mensaje, 5);
                    
                    setTimeout(() => {
                        var modalpopupConfirm = angular.element(document.getElementById("IdPEPCeCoModal"));
                        modalpopupConfirm.modal('hide'); 
                    }, 2000);
                }
            }, function (response) {
                blockUI.stop();
            });
        }

        function ObtenerPEPCeCo(iIdDetalleFinanciero, iIdProyecto){
            let request = {IdDetalleFinanciero: iIdDetalleFinanciero};
            
            var promise = BandejaProyectoService.ObtenerPEPCeCo(request);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.IdServicioFinanciero != 0){
                    blockUI.stop();
                    vm.pepceco = Respuesta;
                }else{
                    vm.pepceco = {};
                    vm.pepceco.IdDetalleFinanciero = iIdDetalleFinanciero;
                }

                vm.pepceco.IdProyecto = iIdProyecto;

                
                var modalpopupConfirm = angular.element(document.getElementById("IdPEPCeCoModal"));
                modalpopupConfirm.modal('show');

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarEtapas() {
            var fase = {
                IdFase: 4
            };
            var promise = EtapaOportunidadService.ListarComboEtapaOportunidadPorIdFase(fase);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaEtapas = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }
        
        function ListarJefes(){
            let request = {IdCargo: 7};
            
            var promise = RecursoService.ListarRecursoPorCargo(request);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaJefes = Respuesta;
            }, function (response) {
                blockUI.stop();
            });
        }

        function ActualizarJP(iIdProyecto, iIdAsignacion){
            let request = {
                    IdCargo: 7, 
                    IdRol: 7,
                    Modificado: true,
                    IdAsignacion: iIdAsignacion,
                    IdProyecto: iIdProyecto, 
                    IdRecurso: vm.selected[iIdProyecto]
                };
            
            var promise = BandejaProyectoService.ActualizarJP(request);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
            }, function (response) {
                blockUI.stop();
            });
        }
        
        function BuscarProyecto(){
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(ListarProyecto)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withLanguage({"sEmptyTable" : "No hay datos disponibles en la tabla"})
                    .withOption('destroy', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }

        function ListarProyecto(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            
            var proyectoDtoRequest = {
                IdProyectoLKM: vm.IdProyectoLKM,
                Cliente: vm.Cliente,
                IdEtapaActual: vm.IdEtapa,
                FechaInicio: vm.FechaInicio,
                FechaFin: vm.FechaFin,
                Indice: pageNumber,
                Tamanio: length 
            };

            var promise = BandejaProyectoService.ListarProyectoPaginado(proyectoDtoRequest);
            promise.then(function (resultado) {
                var data = resultado.data;

                if (LineasCMI == 1){
                    data = data.filter(d => d.TieneLineaCMI == 1);
                }

                var result = {
                    'draw': draw,
                    'recordsTotal': (resultado.data.length > 0 ? resultado.data[0].TotalRegistros : 0),
                    'recordsFiltered': (resultado.data.length > 0 ? resultado.data[0].TotalRegistros : 0),
                    'data': data
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
       
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('paging', false);
        }
        
        function LimpiarFiltros() {
            vm.IdProyectoLKM = '';
            vm.Cliente = '';
            vm.IdEtapa = -1;
            vm.FechaInicio = '';
            vm.FechaFin = '';
            LimpiarGrilla();
        }

        function MostrarPrePeticionCompra(iIdDetalleFinanciero, iIdProyecto){
            window.location = $urls.ApiCompra + "prepeticioncompra?IdProyecto=" + iIdProyecto + "&IdDetalleFinanciero=" + iIdDetalleFinanciero;
            /*blockUI.start();

            var promise = PrePeticionCompraService.MostrarPrePeticionCompra();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoPrePeticionCompra").html(content);
                });

                var modalpopupConfirm = angular.element(document.getElementById("IdPrePeticionCompra"));
                modalpopupConfirm.modal('show');

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });*/
        }
    }
})();