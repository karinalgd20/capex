(function () {
    'use strict',
     angular
    .module('app.Proyecto')
    .service('BandejaProyectoService', BandejaProyectoService);

    BandejaProyectoService.$inject = ['$http', 'URLS'];

    function BandejaProyectoService($http, $urls) {

        var service = {
            ListarProyectoPaginado: ListarProyectoPaginado,
            EliminarAreaSeguimiento: EliminarAreaSeguimiento,
            ModalAreaSeguimiento: ModalAreaSeguimiento,
            ActualizarJP: ActualizarJP,
            MostrarInfoPEPCeCo: MostrarInfoPEPCeCo,
            GuardarPEPCeCo: GuardarPEPCeCo,
            ObtenerPEPCeCo: ObtenerPEPCeCo,
            ObtenerInfoUsuarioActual: ObtenerInfoUsuarioActual
        };

        return service;

        function ListarProyectoPaginado(area) {
            return $http({
                url: $urls.ApiProyecto + "BandejaProyecto/ListarProyectoPaginado",
                method: "POST",
                data: JSON.stringify(area)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarAreaSeguimiento(area) {
            return $http({
                url: $urls.ApiSeguridad + "BandejaAreaSeguimiento/EliminarAreaSeguimiento",
                method: "POST",
                data: JSON.stringify(area)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalAreaSeguimiento(area) {
            return $http({
                url: $urls.ApiSeguridad + "RegistrarAreaSeguimiento/Index",
                method: "POST",
                data: JSON.stringify(area)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarJP(request) {
            return $http({
                url: $urls.ApiProyecto + "BandejaProyecto/ActualizarJP",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoPEPCeCo() {
            return $http({
                url: $urls.ApiProyecto + "BandejaProyecto/ObtenerInformacionPEPCeCo",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function GuardarPEPCeCo(request) {
            return $http({
                url: $urls.ApiProyecto + "BandejaProyecto/GuardarPEPCeCo",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerPEPCeCo(request) {
            return $http({
                url: $urls.ApiProyecto + "BandejaProyecto/ObtenerPEPCeCo",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerInfoUsuarioActual() {
            return $http({
                url: $urls.ApiProyecto + "BandejaProyecto/ObtenerInfoUsuarioActual",
                method: "POST",
                data: {}
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();