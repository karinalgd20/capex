(function () {
    'use strict',
     angular
    .module('app.Proyecto')
    .service('RegistroProyectoService', RegistroProyectoService);

    RegistroProyectoService.$inject = ['$http', 'URLS'];

    function RegistroProyectoService($http, $urls) {

        var service = {
            MostrarInfoCliente: MostrarInfoCliente,
            MostrarInfoDescripcion: MostrarInfoDescripcion,
            MostrarInfoMonto: MostrarInfoMonto,
            MostrarInfoPlazo: MostrarInfoPlazo,
            MostrarInfoEquipo: MostrarInfoEquipo,
            MostrarInfoDocumentacion: MostrarInfoDocumentacion,
            ObtenerOportunidadGanadaPorId: ObtenerOportunidadGanadaPorId,
            RegistrarProyectoCliente: RegistrarProyectoCliente,
            ActualizarProyectoCliente: ActualizarProyectoCliente,
            ObtenerProyectoById: ObtenerProyectoById,
            ActualizarProyectoDescripcion: ActualizarProyectoDescripcion,
            RegistrarProyectoMontos: RegistrarProyectoMontos,
            ActualizarProyectoMontos: ActualizarProyectoMontos,
            ObtenerMontosByIdProyecto: ObtenerMontosByIdProyecto,
            CalcularPeriodoPlazos: CalcularPeriodoPlazos,
            GuardarDetallePlazo: GuardarDetallePlazo,
            ListarDetallePlazo: ListarDetallePlazo,
            GuardarRecursoProyecto: GuardarRecursoProyecto,
            ListarRecursoProyecto: ListarRecursoProyecto,
            GuardarDocumentacionProyecto: GuardarDocumentacionProyecto,
            ListarDocumentacionProyecto: ListarDocumentacionProyecto,
            DescargarDocumentacionArchivo: DescargarDocumentacionArchivo,
            MostrarInfoPlantillafinanciera: MostrarInfoPlantillafinanciera,

            MostrarInfoFinacieraPagos: MostrarInfoFinacieraPagos,
            MostrarInfoFinacieraIndicadores: MostrarInfoFinacieraIndicadores,
            MostrarInfoFinacieraLineas: MostrarInfoFinacieraLineas,
            MostrarInfoFinacieraCostos: MostrarInfoFinacieraCostos,
            MostrarInfoFinacieraCostosConcepto: MostrarInfoFinacieraCostosConcepto,
            MostrarInfoFinacieraServicios: MostrarInfoFinacieraServicios,
            GuardarArchivoFinanciero: GuardarArchivoFinanciero,
            ListarServicioCMI: ListarServicioCMI,
            ListarPagos: ListarPagos,
            ListarIndicadores: ListarIndicadores,
            ListarDetalleFinanciero: ListarDetalleFinanciero,
            ListarCostos: ListarCostos,
            ListarCostosOpex: ListarCostosOpex,
            ListarCostosCapex: ListarCostosCapex,
            
            DescargarDocumentacionArchivo: DescargarDocumentacionArchivo,
            EliminarDocumentacionProyecto: EliminarDocumentacionProyecto
        };

        return service;

        function MostrarInfoCliente() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionCliente", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoDescripcion() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionDescripcion", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoMonto() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionMonto", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoPlazo() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionPlazo", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoEquipo() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionEquipo", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoDocumentacion() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionDocumentacion", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerOportunidadGanadaPorId(sIdOportunidad) {
            var data = {sIdOportunidad: sIdOportunidad}
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerOportunidadGanada",
                method: "POST",
                data: JSON.stringify(data)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarProyectoCliente(dto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/RegistrarProyectoCliente",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarProyectoCliente(dto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ActualizarProyectoCliente",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerProyectoById(dto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerProyectoPorId",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ActualizarProyectoDescripcion(dto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ActualizarProyectoDescripcion",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarProyectoMontos(dto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/RegistrarProyectoMontos",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarProyectoMontos(dto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ActualizarProyectoMontos",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function CalcularPeriodoPlazos(DetallePlazo) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/CalcularPeriodoPlazos",
                method: "POST",
                data: JSON.stringify(DetallePlazo)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
        function GuardarDetallePlazo(DetallePlazos) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/GuardarDetallePlazo",
                method: "POST",
                data: JSON.stringify(DetallePlazos)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarDetallePlazo(IdProyecto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ListarDetallePlazo",
                method: "POST",
                data: JSON.stringify({iIdProyecto: IdProyecto})
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function GuardarRecursoProyecto(RecursoProyecto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/GuardarRecursoProyecto",
                method: "POST",
                data: JSON.stringify(RecursoProyecto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarRecursoProyecto(IdProyecto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ListarRecursoProyecto",
                method: "POST",
                data: JSON.stringify({iIdProyecto: IdProyecto})
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function GuardarDocumentacionProyecto(Documentacion, iIdProyecto){
            
            var datax = new FormData();
            var sTipoDocumento = [];

            console.log(Documentacion);

            Documentacion.forEach(documento => {
                sTipoDocumento.push(documento.IdTipoDocumento);
                datax.append("file", documento.Archivo);
            });
            datax.append("IdTipoDocumento", sTipoDocumento.join(","));
            datax.append("IdProyecto", iIdProyecto);

            var resultado =$.ajax({
                type: "POST",
                url: $urls.ApiProyecto + "Registrarproyecto/GuardarDocumentacionProyecto",  
                dataType: 'json',
                cache: false,
                processData: false,
                contentType: false, 
                data: datax
            }); 

            /*return $http({
                url: ,
                method: "POST",
                data: JSON.stringify(Documentacion)
            }).then(DatosCompletados);*/

            /*function DatosCompletados(resultado) {
                return resultado;
            }*/

            return resultado;
        }

        function ListarDocumentacionProyecto(IdProyecto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ListarDocumentacionProyecto",
                method: "POST",
                data: JSON.stringify({iIdProyecto: IdProyecto})
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ObtenerMontosByIdProyecto(dto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerDetalleFacturaPorIdProyecto",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function EliminarDocumentacionProyecto(iIdDocumentoProyecto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/EliminarDocumentacionProyecto",
                method: "POST",
                data: JSON.stringify({IdDocumentoProyecto: iIdDocumentoProyecto })
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function DescargarDocumentacionArchivo(iIdDocumentoArchivo) {
            window.location.href = $urls.ApiProyecto + "RegistrarProyecto/DescargarDocumentacionArchivo?iIdDocumentoArchivo=" + iIdDocumentoArchivo;
            /*return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/DescargarDocumentacionArchivo",
                method: "POST",
                data: JSON.stringify({iIdDocumentoArchivo: iIdDocumentoArchivo, sNombreArchivo: sNombreArchivo })
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }*/
        };

        function ListarDetalleDescripcion(IdProyecto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ListarDetalleDescripcion",
                method: "POST",
                data: JSON.stringify({ idProyecto: IdProyecto })
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarDetalleMontos(IdProyecto) {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ListarDetalleMontos",
                method: "POST",
                data: JSON.stringify({ idProyecto: IdProyecto })
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
        function MostrarInfoPlantillafinanciera() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionFinanciera",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoFinacieraPagos() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionFinancieraPagos",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoFinacieraIndicadores() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionFinancieraIndicador",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoFinacieraLineas() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionFinancieraLineas",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoFinacieraCostos() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionFinancieraCostos",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoFinacieraCostosConcepto() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionFinancieraCostosConcepto",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoFinacieraServicios() {
            return $http({
                url: $urls.ApiProyecto + "RegistrarProyecto/ObtenerInformacionFinancieraServicios",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function GuardarArchivoFinanciero(Documentacion, iIdProyecto, tipo) {

            var datax = new FormData();

            datax.append("file", Documentacion.Archivo);
            datax.append("IdProyecto", iIdProyecto);
            datax.append("tipoArchivo", tipo);

            var resultado = $.ajax({
                type: "POST",
                url: $urls.ApiProyecto + "Registrarproyecto/GuardarArchivoFinanciero",
                cache: false,
                processData: false,
                contentType: false,
                data: datax,
                success: function (response) {
                    return response;
                },
                error: function (error) {

                }
            });

            return resultado;
        };

        function ListarServicioCMI(IdProyecto) {
            return $http({
                url: $urls.ApiProyecto + "Registrarproyecto/ListarServiciosCMI",
                method: "POST",
                data: JSON.stringify({ iIdProyecto: IdProyecto })
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarPagos(IdProyecto) {
            return $http({
                url: $urls.ApiProyecto + "Registrarproyecto/ListarPagos",
                method: "POST",
                data: JSON.stringify({ iIdProyecto: IdProyecto })
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarIndicadores(IdProyecto) {
            return $http({
                url: $urls.ApiProyecto + "Registrarproyecto/ListarIndicadores",
                method: "POST",
                data: JSON.stringify({ iIdProyecto: IdProyecto })
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarDetalleFinanciero(IdProyecto) {
            return $http({
                url: $urls.ApiProyecto + "Registrarproyecto/ListarDetalleFinanciero",
                method: "POST",
                data: JSON.stringify({ iIdProyecto: IdProyecto })
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarCostos(IdProyecto) {
            return $http({
                url: $urls.ApiProyecto + "Registrarproyecto/ListarCostos",
                method: "POST",
                data: JSON.stringify({ iIdProyecto: IdProyecto })
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarCostosOpex(IdProyecto) {
            return $http({
                url: $urls.ApiProyecto + "Registrarproyecto/ListarCostosOpex",
                method: "POST",
                data: JSON.stringify({ iIdProyecto: IdProyecto })
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarCostosCapex(IdProyecto) {
            return $http({
                url: $urls.ApiProyecto + "Registrarproyecto/ListarCostosCapex",
                method: "POST",
                data: JSON.stringify({ iIdProyecto: IdProyecto })
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();