(function () {
    'use strict'
    angular
        .module('app.Proyecto')
        .controller('RegistroProyectoController', RegistroProyectoController);
    
        RegistroProyectoController.$inject = ['RegistroProyectoService',
        'MaestraService', 'TipoSolucionService', 'RecursoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$filter'];

    function RegistroProyectoController(RegistroProyectoService,
        MaestraService, TipoSolucionService, RecursoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $filter)
    {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiProyecto + "RegistrarProyecto/Index/";

        vm.MostrarInfoCliente = MostrarInfoCliente;
        vm.MostrarInfoDescripcion = MostrarInfoDescripcion;
        vm.MostrarInfoMonto = MostrarInfoMonto;
        vm.MostrarInfoPlazo = MostrarInfoPlazo;
        vm.MostrarInfoEquipo = MostrarInfoEquipo;
        vm.MostrarInfoDocumentacion = MostrarInfoDocumentacion;
        vm.ObtenerOportunidadGanadaPorId = ObtenerOportunidadGanadaPorId;
        vm.CalcularPeriodoPlazos = CalcularPeriodoPlazos;
        vm.GuardarDetallePlazo = GuardarDetallePlazo;
        vm.ListarDetallePlazo = ListarDetallePlazo;
        vm.SeleccionarUsuarioEquipo = SeleccionarUsuarioEquipo;
        vm.GuardarRecursoProyecto = GuardarRecursoProyecto;
        vm.ListarRecursoProyecto = ListarRecursoProyecto;
        vm.GuardarDocumento = GuardarDocumento;
        vm.EliminarDocumento = EliminarDocumento;
        vm.SeleccionarTipoDocumento = SeleccionarTipoDocumento;
        vm.GuardarDocumentacion = GuardarDocumentacion;
        vm.DescargarDocumentacionArchivo = DescargarDocumentacionArchivo;
        vm.ListaDocumentos = [];
        vm.GrabarProyectoCliente = GrabarProyectoCliente;
        vm.GrabarProyectoDescripcion = GrabarProyectoDescripcion;
        vm.GrabarProyectoMontos = GrabarProyectoMontos;
        vm.ListarDetalleMontos = ListarDetalleMontos;
        vm.MostrarPlantillaFinanciera = MostrarPlantillaFinanciera;


        vm.MostrarFinacieraPagos = MostrarFinacieraPagos;
        vm.MostrarFinacieraIndicadores = MostrarFinacieraIndicadores;
        vm.MostrarFinacieraLineas = MostrarFinacieraLineas;
        vm.MostrarFinacieraCostos = MostrarFinacieraCostos;
        vm.MostrarFinacieraCostosConcepto = MostrarFinacieraCostosConcepto;
        vm.MostrarFinacieraServicios = MostrarFinacieraServicios;
        vm.GuardarConsolidado = GuardarConsolidado;
        vm.GuardarCMI = GuardarCMI;
        vm.ListaServiciosCMI = [];
        vm.ListaPagos = [];
        vm.ListaIndicadores = [];
        vm.ListaDetalleFinanciero = [];
        vm.ListaCostos = [];
        vm.ListaCostosOpex = [];
        vm.ListaCostosCapex = [];
        
        vm.OportunidadGanada = {
            IdOportunidad: "",
            Cliente: "",
            IdCliente: ""
        }

        vm.Proyecto = {
            IdProyecto: 0,
            FechaCrea: new Date().toLocaleDateString(),
            CodigoProyectoExtendido: "",
            IdProyectoLKM: "",
            FechaModifica: "",
            Estado: ""
        }

        
        vm.TipoDocumentoValor2 = "";

        vm.IdRecursoAF = 0;
        vm.IdRecursoJP = 0;
        vm.DetallePlazoVentaDirecta = true;
        
        vm.EmailAF = "";
        vm.EmailJP = "";               

        var proyectoMontos = {
            IdDetalleFacturar: 0,
            IdProyecto: 0,
            IdMonedaPagoUnico: null,
            MonedaPagoUnico: null,
            IdMonedaRecurrenteMensual: null,
            MonedaRecurrenteMensual: null,
            MontoPagoUnico: null,
            MontoRecurrenteMensual: null
        };

            
        vm.IdEtapaActual = 9;
        vm.FechaGanada = "";
        vm.IdComplejidad = -1;
        vm.IdOficinaOportunidad = -1;
        vm.TiposSelected = [];
        vm.TiposNoSelected = [];
        vm.IdDetalleFacturar = 0;

        vm.MensajeDeAlerta = "";

        vm.TipoDocumentoFinancieros = "xlsx";
        vm.DocumentoFinan = null;
        vm.DocumentoFinanCMI = null;
        vm.habilitarCMI = true;
        vm.AdjuntoCorrecto = false;
        vm.HabilitarEditar = false;
        vm.OptionPreVenta = !jsonOptionPreVenta;

        vm.MostrarTabPF = true;
        vm.MostrarAF = true;
        vm.MostrarANFI = true;
        vm.CodigoPerfilControl = "CONTROL";
        vm.CodigoPerfilLiderJP = "LIDERPROY";
        vm.CodigoPerfilPreventa = "PREVENT";
        vm.CodigoPerfilAF = "ANLFIN";

        angular.element(document).ready(function () {

            if (IdProyecto !== "0") {
                blockUI.start();

                var proyecto = {
                    IdProyecto: IdProyecto,
                };

                var promise = RegistroProyectoService.ObtenerProyectoById(proyecto);
                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                    var dFechaGanada = new Date(parseInt(Respuesta.FechaOportunidadGanada.substr(6)));
                    
                    vm.Proyecto = Respuesta;
                    vm.OportunidadGanada.IdOportunidad = Respuesta.IdProyectoSF;
                    vm.OportunidadGanada.Cliente = Respuesta.NombreCliente;
                    vm.OportunidadGanada.IdCliente = Respuesta.IdCliente;
                    vm.FechaGanada = $filter('date')(dFechaGanada,'dd/MM/yyyy');
                    vm.Descripcion = Respuesta.Descripcion;
                    vm.IdOficinaOportunidad = Respuesta.IdOficinaOportunidad;
                    vm.IdComplejidad = Respuesta.IdComplejidad;
                    vm.HabilitarEditar = true;
                    vm.IdEtapaActual = Respuesta.IdEtapaActual;
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 5);
                });

            }

            if (IdPerfil == vm.CodigoPerfilPreventa || IdPerfil == vm.CodigoPerfilLiderJP) {
                vm.MostrarTabPF = true;
                vm.MostrarAF = false;
            } else {
                vm.MostrarTabPF = false;
                vm.MostrarAF = true;
            }

            if (IdPerfil == vm.CodigoPerfilAF) {
                vm.MostrarANFI = false;
            } else {
                vm.MostrarANFI = true;
            }


        });

        function MostrarInfoCliente() {
            
            blockUI.start();

            //var promise = RegistroProyectoService.MostrarInfoCliente();
            //promise.then(function (response) {
            //    var respuesta = $(response.data);

            //    $injector.invoke(function ($compile) {
            //        var div = $compile(respuesta);
            //        var content = div($scope);
            //        $("#ContenidoInfoCliente").html(content);
            //    });

            //}, function (response) {
            //    blockUI.stop();
            //});
            blockUI.stop();
        };
        
        function MostrarInfoDescripcion() {
            if (vm.Proyecto.IdProyecto == 0) return;

            blockUI.start();

            var promise = RegistroProyectoService.MostrarInfoDescripcion();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoDescripcion").html(content);
                });

                ListarOficinaOportunidad();
                ListarComplejidadProyecto();
                ListarTipoSolucion();

            }, function (response) {
                blockUI.stop();
            });
        };

        function ListarOficinaOportunidad() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 358
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaOficinaOportunidad = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarComplejidadProyecto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 361
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaComplejidadProyecto = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarTipoSolucion() {

            var DetalleSolucionDto = {
                IdProyecto: vm.Proyecto.IdProyecto
            };

            var promise = TipoSolucionService.ListarTipoSolucion(DetalleSolucionDto);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaTipoSolucion = Respuesta;
            }, function (response) {
                blockUI.stop();
            });
        }

        function MostrarInfoMonto() {

            if (vm.Proyecto.IdProyecto == 0) return;

            blockUI.start();

            var promise = RegistroProyectoService.MostrarInfoMonto();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoMonto").html(content);
                });

                ListarDetalleMontos();

            }, function (response) {
                blockUI.stop();
            });
        };

        //#region "Tab Equipo"
        function MostrarInfoEquipo() {
            if (vm.Proyecto.IdProyecto == 0) return;

            blockUI.start();
            
            var promise = RegistroProyectoService.MostrarInfoEquipo();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoEquipo").html(content);

                    ListarRecursoAF();
                    ListarRecursoJP();
                    ListarRecursoGC();
                    ListarRecursoJP2();
                    ListarRecursoProyecto();
                });
            }, function (response) {
                blockUI.stop();
            });
        };

        function ListarRecursoAF() {
            let request = {IdCargo: 4};

            var promise = RecursoService.ListarRecursoPorCargo(request);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaRecursoAF = Respuesta;
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarRecursoJP() {
            let request = {IdCargo: 8};

            var promise = RecursoService.ListarRecursoPorCargo(request);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaRecursoJP = Respuesta;
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarRecursoGC() {
            let request = {IdCargo: 3};

            var promise = RecursoService.ListarRecursoPorCargo(request);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaRecursoGC = Respuesta;
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarRecursoJP2() {
            let request = {IdCargo: 7};

            var promise = RecursoService.ListarRecursoPorCargo(request);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaRecursoJP2 = Respuesta;
            }, function (response) {
                blockUI.stop();
            });
        }

        function SeleccionarUsuarioEquipo(RecursoProyecto){
            switch (RecursoProyecto.IdCargo) {
                case 4:
                    let oRecursoAF = vm.ListaRecursoAF.find(raf => raf.IdRecurso == RecursoProyecto.IdRecurso);
                    if (oRecursoAF != null){
                        RecursoProyecto.Email = oRecursoAF.Email;
                    }
                    break;
                case 8:
                    let oRecursoJP = vm.ListaRecursoJP.find(raf => raf.IdRecurso == RecursoProyecto.IdRecurso);
                    if (oRecursoJP != null){
                        RecursoProyecto.Email = oRecursoJP.Email;
                    }
                    break;
                case 3:
                    let oRecursoGC = vm.ListaRecursoGC.find(raf => raf.IdRecurso == RecursoProyecto.IdRecurso);
                    if (oRecursoGC != null){
                        RecursoProyecto.Email = oRecursoGC.Email;
                    }
                    break;
                case 7:
                    let oRecursoJP2 = vm.ListaRecursoJP2.find(raf => raf.IdRecurso == RecursoProyecto.IdRecurso);
                    if (oRecursoJP2 != null){
                        RecursoProyecto.Email = oRecursoJP2.Email;
                    }
                    break;
                default:
                    break;
            }

            RecursoProyecto.Modificado = true;
        }

        function GuardarRecursoProyecto(){
            let iCantidadRecursos = vm.RecursoProyecto.filter(dp => dp.IdRecurso != 0).length;
            if (iCantidadRecursos == 0){
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', "Debe ingresar al menos un recurso.", 5);
                blockUI.stop();
                return;
            }

            blockUI.start();

            vm.RecursoProyecto.forEach(detalle => {
                detalle.IdProyecto = vm.Proyecto.IdProyecto
            });

            var promise = RegistroProyectoService.GuardarRecursoProyecto(vm.RecursoProyecto);
            promise.then(function (response) {
                var Respuesta = response.data;
                if (Respuesta){
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'success', "Se registro la información satisfactoriamente.", 5);
                    blockUI.stop();
                    return;
                }
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarRecursoProyecto(){
            var promise = RegistroProyectoService.ListarRecursoProyecto(vm.Proyecto.IdProyecto);
            promise.then(function (response) {
                vm.RecursoProyecto = response.data.lista; 
                vm.NombreUsuarioActual = response.data.NombreUsuarioActual;
                vm.EmailUsuarioActual = response.data.EmailUsuarioActual;

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }
        //#endregion
        
        //#region  "Tab Documentacion"
        function MostrarInfoDocumentacion() {
            if (vm.Proyecto.IdProyecto == 0) return;
            
            blockUI.start();

            NuevoDocumento();

            var promise = RegistroProyectoService.MostrarInfoDocumentacion();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoDocumentacion").html(content);

                    ListarTipoDocumento();
                    ListarDocumentacionProyecto()
                });
            }, function (response) {
                blockUI.stop();
            });
        };

        function NuevoDocumento(){
            vm.Documento = {
                IdTipoDocumento: -1,
                DesTipoDocumento: "",
                Archivo: "",
                NombreArchivo: "",
                EsNuevo: true
            }
        }

        function ListarTipoDocumento(){
            var maestra = {
                IdEstado: 1,
                IdRelacion: 370
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion2(maestra);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                let ListaTipoDocumento = [];
                Respuesta.forEach(element => {
                    ListaTipoDocumento.push({Codigo: element.Valor, Valor2: element.Valor2, Descripcion: element.Descripcion + (element.Valor2 != null ? " (" + element.Valor2 + ")" : "")});
                });

                vm.ListaTipoDocumento = UtilsFactory.AgregarItemSelect(ListaTipoDocumento);
                blockUI.stop();
            }, function (response) {
            });
        }

        $scope.UploadFiles = function (files) {
            let archivo = files[0];
            vm.Documento.Archivo = archivo
            vm.Documento.NombreArchivo = archivo.name;
        }

        function SeleccionarTipoDocumento(){
            let oTipoDocumento = vm.ListaTipoDocumento.find(td => td.Codigo == vm.Documento.IdTipoDocumento);
            vm.Documento.DesTipoDocumento = oTipoDocumento.Descripcion;
            vm.TipoDocumentoValor2 = oTipoDocumento.Valor2;
        }

        function GuardarDocumento() {

            let Documentos = vm.ListaDocumentos.filter(d => d.NombreArchivo == vm.Documento.NombreArchivo);

            if (vm.Documento.IdTipoDocumento == -1) {
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', "Debe seleccionar el tipo de documento.", 5);
                return;
            } else if (vm.Documento.NombreArchivo == "") {
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', "Debe de seleccionar el archivo a adjuntar.", 5);
                return;
            } else if (Documentos.length > 0) {
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', "El archivo ya fue adjuntado seleccione otro.", 5);
                return;
            }
                
            vm.ListaDocumentos.push(vm.Documento);
            NuevoDocumento();
        }

        function EliminarDocumento(Documento){
            if (Documento.IdDocumentoProyecto == 0 || (Documento.IdDocumentoProyecto == undefined && Documento.EsNuevo)) {
                let idx = vm.ListaDocumentos.findIndex(d => d.IdTipoDocumento === Documento.IdTipoDocumento && d.NombreArchivo === Documento.NombreArchivo);
                vm.ListaDocumentos.splice(idx, 1);
            }else{
                var promise = RegistroProyectoService.EliminarDocumentacionProyecto(Documento.IdDocumentoProyecto).then(function (response) {
                    var Respuesta = response.data;
                    if (Respuesta) {
                        let idx = vm.ListaDocumentos.findIndex(d => d.IdDocumentoProyecto == Documento.IdDocumentoProyecto);
                        vm.ListaDocumentos.splice(idx, 1);

                        UtilsFactory.Alerta('#lblAlerta_Proyecto', 'success', "Se elimino la información satisfactoriamente.", 5);
                    }
    
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                });
            }
        }

        function GuardarDocumentacion(){
            if (vm.ListaDocumentos.length == 0){
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', "Debe ingresar al menos un documento.", 5);
                blockUI.stop();
            }
            
            let Documentos = vm.ListaDocumentos.filter(d => d.EsNuevo);
            
            blockUI.start();
            var promise = RegistroProyectoService.GuardarDocumentacionProyecto(Documentos, vm.Proyecto.IdProyecto).then(function (response) {
                var Respuesta = response;
                if (Respuesta.TipoRespuesta == 0){
                    ListarDocumentacionProyecto();
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'success', "Se registro la información satisfactoriamente.", 5);
                    //UtilsFactory.Alerta('#lblAlerta_Proyecto', 'success', Respuesta.Mensaje, 5);
                } else {
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', Respuesta.Mensaje, 20);
                }

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarDocumentacionProyecto(){
            var promise = RegistroProyectoService.ListarDocumentacionProyecto(vm.Proyecto.IdProyecto);
            promise.then(function (response) {
                var Respuesta = response.data;
  
                vm.ListaDocumentos = Respuesta;

                vm.ListaDocumentos.forEach(documento => {
                    documento.EsNuevo = false;
                });

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }
        //#endregion

        function ObtenerOportunidadGanadaPorId() {
            let sIdOportunidad = vm.OportunidadGanada.IdOportunidad;
            
            blockUI.start();

            if (sIdOportunidad.length == 0){
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', 'Debe ingresar un Código Oportunidad.', 5);
                blockUI.stop();
                return;
            }

            var promise = RegistroProyectoService.ObtenerOportunidadGanadaPorId(sIdOportunidad);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;

                if (Respuesta.TipoRespuesta == 0) {
                    vm.OportunidadGanada = Respuesta;
                    vm.Descripcion = Respuesta.Descripcion;
                } else {
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', Respuesta.Mensaje, 5);
                }
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function DescargarDocumentacionArchivo(Documento){
            var promise = RegistroProyectoService.DescargarDocumentacionArchivo(Documento.IdDocumentoArchivo, Documento.NombreArchivo);
            promise.then(function (response) {
                var Respuesta = response.data;
                
                
                var blob = new Blob([Respuesta], { type: 'application/octet-stream' });
                
                var url = window.URL.createObjectURL(blob);
                
                var linkElement = document.createElement('a');

                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", Documento.NombreArchivo);
     
                var clickEvent = new MouseEvent("click", {
                    "view": window,
                    "bubbles": true,
                    "cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        //#region Tab Plazos
        function MostrarInfoPlazo() {
            if (vm.Proyecto.IdProyecto == 0) return;

            blockUI.start();
            
            var promise = RegistroProyectoService.MostrarInfoPlazo();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoPlazo").html(content);

                    ListarPeriodoPlazos();
                    ListarDetallePlazo();
                });

            }, function (response) {
                blockUI.stop();
            });
        };

        function GuardarDetallePlazo(){
            let iCantidadDetallePlazo = vm.DetallePlazo.filter(dp => dp.Modificado > 0).length;
            
            if (iCantidadDetallePlazo == 0){
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', "Debe ingresar al menos un detalle.", 5);
                blockUI.stop();
                return;
            }

            vm.DetallePlazo.forEach(detalle => {
                detalle.IdProyecto = vm.Proyecto.IdProyecto,
                detalle.VentaDirecta = vm.DetallePlazoVentaDirecta
            });

            var promise = RegistroProyectoService.GuardarDetallePlazo(vm.DetallePlazo);
            promise.then(function (response) {
                var Respuesta = response.data;
                if (Respuesta){
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'success', "Se registro la información satisfactoriamente.", 5);
                    return;
                }
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarDetallePlazo(){
            var promise = RegistroProyectoService.ListarDetallePlazo(vm.Proyecto.IdProyecto);
            promise.then(function (response) {
                var Respuesta = response.data;
                vm.DetallePlazo = Respuesta;

                vm.DetallePlazo.forEach(detalleplazo => {
                        detalleplazo.FechaInicio = detalleplazo.FechaInicio != null ? new Date(parseInt(detalleplazo.FechaInicio.substr(6))) : "",
                        detalleplazo.FechaFin = detalleplazo.FechaFin != null ? new Date(parseInt(detalleplazo.FechaFin.substr(6))) : ""
                    }
                );

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarPeriodoPlazos() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 365
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaPeriodoPlazos = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function CalcularPeriodoPlazos(DetallePlazo){
            window.setTimeout(() => {
                blockUI.start();
            
                if (DetallePlazo.FechaInicio == null){
                    alert("Debe ingresar una Fecha de Inicio para " + DetallePlazo.DesActividad + ".");
                    DetallePlazo.IdUnidadMedida = -1;
                    blockUI.stop();
                    return;
                }
    
                if (DetallePlazo.FechaFin == null){
                    alert("Debe ingresar una Fecha de Fin para " + DetallePlazo.DesActividad + ".");
                    DetallePlazo.IdUnidadMedida = -1;
                    blockUI.stop();
                    return;
                }
                
                var promise = RegistroProyectoService.CalcularPeriodoPlazos(DetallePlazo);
    
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
    
                    let oItemCalculado = vm.DetallePlazo.find(dp => dp.IdActividad == DetallePlazo.IdActividad);
                    oItemCalculado.Modificado = 1;
                    oItemCalculado.Cantidad = Respuesta.Cantidad;
                    oItemCalculado.Anios = Respuesta.Anios;
                    oItemCalculado.Meses = Respuesta.Meses;
                    oItemCalculado.Dias = Respuesta.Dias;
                }, function (response) {
                    blockUI.stop();
                }); 
            }, 0);
        }

        function GrabarProyectoCliente() {
            var Id = $scope.vm.Proyecto.IdProyecto;

            if ($.trim(vm.FechaGanada) == "") {
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', 'Seleccione una fecha ganada', 5);
                return;
            }

            if (vm.OportunidadGanada.IdCliente == '') {
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', 'Seleccione una oportunidad', 5);
                return;
            }

            var proyectoClienteDto = {
                IdProyecto: Id,
                IdProyectoSF: vm.OportunidadGanada.IdOportunidad,
                IdCliente: vm.OportunidadGanada.IdCliente,
                FechaOportunidadGanada: vm.FechaGanada,
            };

            blockUI.start();

            var promise = (Id > 0) ? RegistroProyectoService.ActualizarProyectoCliente(proyectoClienteDto) : RegistroProyectoService.RegistrarProyectoCliente(proyectoClienteDto);
            promise.then(function (response) {
                var Respuesta = response.data;
                if (Respuesta.TipoRespuesta != 0) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 20);
                } else {
                    //Resfrescar Datos en Pantalla.
                    var proyecto = {
                        IdProyecto: Respuesta.Id,
                    };

                    var promise = RegistroProyectoService.ObtenerProyectoById(proyecto);
                    promise.then(function (resultado) {
                        var Respuesta = resultado.data;
                        vm.Proyecto = Respuesta;

                        blockUI.stop();
                    }, function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 5);
                    });

                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'success', Respuesta.Mensaje, 10);
                }
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 5);
            });
        };
        
        function GrabarProyectoDescripcion() {
            
            blockUI.start();

            var Id = $scope.vm.Proyecto.IdProyecto;

            if (Id == 0) {
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', 'Grabar un proyecto antes de continuar.', 5);
                return;
            }

            var ListaTiposNuevo = [];
            var ListaTiposEliminado = [];
            vm.TiposSelected.forEach(function (element) {
                var tipo = {
                    IdTipoSolucion: element,
                    IdProyecto: Id
                };

                ListaTiposNuevo.push(tipo);
            });

            vm.TiposNoSelected.forEach(function (element) {
                var tipo = {
                    IdTipoSolucion: element,
                    IdProyecto: Id
                };

                ListaTiposEliminado.push(tipo);
            });

            var proyectoClienteDto = {
                IdProyecto: Id,
                IdComplejidad: vm.IdComplejidad,
                IdOficinaOportunidad: vm.IdOficinaOportunidad,
                Descripcion: vm.Descripcion,
                ListaTiposNuevo: ListaTiposNuevo,
                ListaTiposEliminado: ListaTiposEliminado
            };

            var promise = RegistroProyectoService.ActualizarProyectoDescripcion(proyectoClienteDto);
            promise.then(function (response) {

                var Respuesta = response.data;
                if (Respuesta.TipoRespuesta != 0) {
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 20);
                } else {
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'success', Respuesta.Mensaje, 10);
                }
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 5);
            });
        };

        function GrabarProyectoMontos() {

            blockUI.start();
            var IdProyecto = vm.Proyecto.IdProyecto;
            var IdDetalleFacturar = vm.IdDetalleFacturar;

            if (vm.OportunidadGanada.IdCliente == '') {
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', 'Seleccione una oportunidad', 5);
                return;
            }

            var proyectoMontosDto = {
                IdDetalleFacturar: vm.IdDetalleFacturar,
                IdProyecto: IdProyecto,
                IdMonedaPagoUnico: vm.proyectoMontos.IdMonedaPagoUnico,
                IdMonedaRecurrenteMensual: vm.proyectoMontos.IdMonedaRecurrenteMensual,
                MontoPagoUnico: vm.proyectoMontos.MontoPagoUnico,
                MontoRecurrenteMensual: vm.proyectoMontos.MontoRecurrenteMensual
            };
            
            var promise = (IdDetalleFacturar > 0) ? RegistroProyectoService.ActualizarProyectoMontos(proyectoMontosDto) : RegistroProyectoService.RegistrarProyectoMontos(proyectoMontosDto);
            promise.then(function (response) {
                var Respuesta = response.data;
                if (Respuesta.TipoRespuesta != 0) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 20);
                } else {
                    //Resfrescar Datos en Pantalla.
                    vm.IdDetalleFacturar = Respuesta.Id;
                    ListarDetalleMontos();
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'success', Respuesta.Mensaje, 10);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 5);
            });
        };

        function ListarDetalleMontos() {
            var proyectoMontos = {
                IdProyecto: vm.Proyecto.IdProyecto
            };

            var promise = RegistroProyectoService.ObtenerMontosByIdProyecto(proyectoMontos);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.proyectoMontos = Respuesta;
                vm.IdDetalleFacturar = Respuesta.IdDetalleFacturar;

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 5);
            });
        }

        $scope.ActualizarSeleccion = function (_event) {

            var data = vm.ListaTipoSolucion;
            vm.TiposSelected = [];
            vm.TiposNoSelected = [];

            angular.forEach(data, function (_tipoSol) {
                var idTipoSol = 0;
                if (_tipoSol.Activo === true) {
                    idTipoSol = _tipoSol.IdTipoSolucion;
                    vm.TiposSelected.push(angular.copy(idTipoSol));
                } else if (_tipoSol.Activo === false) {
                    idTipoSol = _tipoSol.IdTipoSolucion;
                    vm.TiposNoSelected.push(angular.copy(idTipoSol));
                }

                if (_tipoSol.IdTipoSolucion == 8 && _tipoSol.Activo === true) {
                    vm.IdOficinaOportunidad = 1;
                } else if (_tipoSol.IdTipoSolucion != 8) {
                    vm.IdOficinaOportunidad = 2;
                } else if (vm.TiposSelected.length == 0) {
                    vm.IdOficinaOportunidad = -1;
                }

            });
        }

        $scope.CheckTipoMoneda = function (s, i) {

            vm.proyectoMontos.MonedaPagoUnico = s;
            vm.proyectoMontos.MonedaRecurrenteMensual = s;
            vm.proyectoMontos.IdMonedaPagoUnico = i;
            vm.proyectoMontos.IdMonedaRecurrenteMensual = i

        };

        //INICIO: Tab Plantilla Financiero

        function MostrarPlantillaFinanciera() {
            if (vm.Proyecto.IdProyecto == 0) return;

            blockUI.start();

            NuevoDocumentosFinanciero();

            var promise = RegistroProyectoService.MostrarInfoPlantillafinanciera();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoPlantillaFinanciera").html(content);
                });

                MostrarFinacieraPagos(1);
            }, function (response) {
                blockUI.stop();
            });
        };

        function MostrarFinacieraPagos(loading) {
            if (vm.Proyecto.IdProyecto == 0) return;

            if (loading == 0) {
                blockUI.start();
            }
            
            var promise = RegistroProyectoService.MostrarInfoFinacieraPagos();
            promise.then(function (response) {
                var respuesta = $(response.data);

                

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoPagosFinaciero").html(content);
                });
                ListarPagos();
            }, function (response) {
                blockUI.stop();
            });
        };

        function MostrarFinacieraIndicadores() {
            if (vm.Proyecto.IdProyecto == 0) return;

            blockUI.start();

            var promise = RegistroProyectoService.MostrarInfoFinacieraIndicadores();
            promise.then(function (response) {
                var respuesta = $(response.data);

                

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoIndicadores").html(content);
                });
                ListarIndicadores();
            }, function (response) {
                blockUI.stop();
            });
        };

        function MostrarFinacieraLineas() {
            if (vm.Proyecto.IdProyecto == 0) return;

            blockUI.start();

            var promise = RegistroProyectoService.MostrarInfoFinacieraLineas();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoLineasFinanciero").html(content);
                });
                ListarDetalleFinanciero();
            }, function (response) {
                blockUI.stop();
            });
        };

        function MostrarFinacieraCostos() {
            if (vm.Proyecto.IdProyecto == 0) return;

            blockUI.start();

            var promise = RegistroProyectoService.MostrarInfoFinacieraCostos();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoCostosFinanciero").html(content);
                });
                ListarCostos();
            }, function (response) {
                blockUI.stop();
            });
        };

        function MostrarFinacieraCostosConcepto() {
            if (vm.Proyecto.IdProyecto == 0) return;

            blockUI.start();

            var promise = RegistroProyectoService.MostrarInfoFinacieraCostosConcepto();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoCostoConceptoFinanciero").html(content);
                });

                ListarCostosOpex();
                ListarCostosCapex();
            }, function (response) {
                blockUI.stop();
            });
        };

        function MostrarFinacieraServicios() {
            if (vm.Proyecto.IdProyecto == 0) return;

            blockUI.start();

            var promise = RegistroProyectoService.MostrarInfoFinacieraServicios();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoServiciosFinanciero").html(content);
                });

                ListarServiciosCMI();

            }, function (response) {
                blockUI.stop();
            });
        };

        function NuevoDocumentosFinanciero() {

            vm.DocumentoFinan = {
                Archivo: "",
                NombreArchivo: ""
            }

        }

        $scope.UploadFilesFinan = function (files) {
            for (let i = 0; i < files.length; i++) {
                let archivo = files[i];
                vm.DocumentoFinan.Archivo = archivo
                vm.DocumentoFinan.NombreArchivo = archivo.name;
            }
        }

        $scope.UploadFilesCMI = function (files) {
            for (let i = 0; i < files.length; i++) {
                let archivo = files[i];
                vm.DocumentoFinanCMI.Archivo = archivo
                vm.DocumentoFinanCMI.NombreArchivo = archivo.name;
            }
        }

        function GuardarConsolidado() {
            //let Documentos = vm.ListaDocumentos.filter(d => d.EsNuevo);
            
            let Documento = vm.DocumentoFinan;
            //vm.AdjuntoCorrecto = true;
            vm.DocumentoFinanCMI = {
                Archivo: "",
                NombreArchivo: ""
            }

            var promise = RegistroProyectoService.GuardarArchivoFinanciero(Documento, vm.Proyecto.IdProyecto, 1).then(function (response) {
                var Respuesta = response;
                if (Respuesta.TipoRespuesta == 0) {

                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'success', Respuesta.Mensaje, 10);
                    vm.AdjuntoCorrecto = true;
                    return;
                } else {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 5);
                }
            }, function (response) {
                blockUI.stop();
            });
        }

        function GuardarCMI() {
            //let Documentos = vm.ListaDocumentos.filter(d => d.EsNuevo);
            if (vm.Proyecto.IdProyecto == 0) return;

            blockUI.start();

            let Documento = vm.DocumentoFinanCMI;
            var promise = RegistroProyectoService.GuardarArchivoFinanciero(Documento, vm.Proyecto.IdProyecto, 2).then(function (response) {
                var Respuesta = response;
                if (Respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'success', Respuesta.Mensaje, 10);
                    blockUI.stop();
                    //MostrarFinacieraServicios();
                    MostrarFinacieraPagos();
                    return;
                } else {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 5);
                }
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarPagos() {
            var promise = RegistroProyectoService.ListarPagos(vm.Proyecto.IdProyecto);
            promise.then(function (response) {
                var Respuesta = response.data;

                vm.ListarPagos = Respuesta;

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarIndicadores() {
            var promise = RegistroProyectoService.ListarIndicadores(vm.Proyecto.IdProyecto);
            promise.then(function (response) {
                var Respuesta = response.data;

                vm.ListaIndicadores = Respuesta;

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarDetalleFinanciero() {

            var promise = RegistroProyectoService.ListarDetalleFinanciero(vm.Proyecto.IdProyecto);
            promise.then(function (response) {
                var Respuesta = response.data;

                vm.ListaDetalleFinanciero = Respuesta;

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarCostos() {
            var promise = RegistroProyectoService.ListarCostos(vm.Proyecto.IdProyecto);
            promise.then(function (response) {
                var Respuesta = response.data;
                
                vm.ListaCostos = Respuesta;

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarCostosOpex() {

            var promise = RegistroProyectoService.ListarCostosOpex(vm.Proyecto.IdProyecto);
            promise.then(function (response) {
                var Respuesta = response.data;
                
                vm.ListaCostosOpex = Respuesta;

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarCostosCapex() {

            var promise = RegistroProyectoService.ListarCostosCapex(vm.Proyecto.IdProyecto);
            promise.then(function (response) {
                var Respuesta = response.data;
                
                vm.ListaCostosCapex = Respuesta;

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarServiciosCMI() {
            var promise = RegistroProyectoService.ListarServicioCMI(vm.Proyecto.IdProyecto);
            promise.then(function (response) {
                var Respuesta = response.data;

                vm.ListaServiciosCMI = Respuesta;

                vm.ListaServiciosCMI.forEach(servicio => {
                   var dFechaCreacion = new Date(parseInt(servicio.FechaCreacion.substr(6)));
                    servicio.FechaCreacion = $filter('date')(dFechaCreacion,'dd/MM/yyyy');
                });
                
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        $scope.ActualizarConformidad = function () {

            if (vm.DocumentoFinan.NombreArchivo == "" || !vm.AdjuntoCorrecto) {
                UtilsFactory.Alerta('#lblAlerta_Proyecto', 'warning', 'Adjunte el archivo consolidado antes de dar la conformidad.', 5);
                vm.ConformidadConsolidado = false;
                return;
            }

            if (vm.ConformidadConsolidado && vm.AdjuntoCorrecto) {
                vm.habilitarCMI = false;
            } else {
                vm.habilitarCMI = true;   
            }
        }

        //FIN: Tab Plantilla Financiero
    }
})();