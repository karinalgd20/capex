﻿(function () {
    'use strict'

    angular
    .module('app.Funnel')
    .controller('RptMatrizEscalabilidadRentabilidadController', RptMatrizEscalabilidadRentabilidadController);

    RptMatrizEscalabilidadRentabilidadController.$inject = [
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];


    function RptMatrizEscalabilidadRentabilidadController(
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.MostrarReporte = MostrarReporte;

        function MostrarReporte()
        {
            var fechaActual = new Date();
            var anioActual = fechaActual.getFullYear();

            var parametros = "reporte=ReportePayBackFCV&Anio=" + anioActual;

            blockUI.start();

            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);

            $timeout(function () {
                blockUI.stop();
            }, 5000);
        }
        
    }
})();

