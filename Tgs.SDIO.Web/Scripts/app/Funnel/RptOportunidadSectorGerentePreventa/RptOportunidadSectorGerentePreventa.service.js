﻿(function () {
    'use strict',
     angular
    .module('app.Funnel')
    .service('RptOportunidadSectorGerentePreventaService', RptOportunidadSectorGerentePreventaService);

    RptOportunidadSectorGerentePreventaService.$inject = ['$http', 'URLS'];

    function RptOportunidadSectorGerentePreventaService($http, $urls) {

        var service = {
            ListarOportunidades: ListarOportunidades
        };

        return service;
        function ListarOportunidades(indicadorDashboardSectorDtoRequest) {

            return $http({
                url: $urls.ApiFunnel + "RptOportunidadSectorGerentePreventa/ListarSeguimientoOportunidadesSectorOportunidad",
                method: "POST",
                data: JSON.stringify(indicadorDashboardSectorDtoRequest)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();

