﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('RptOportunidadSectorGerentePreventaController', RptOportunidadSectorGerentePreventaController);

    RptOportunidadSectorGerentePreventaController.$inject = ['RptOportunidadSectorGerentePreventaService', 'blockUI', '$timeout', 'UtilsFactory', 'URLS', 'DTOptionsBuilder', 'DTColumnBuilder', '$compile','$scope'];

    function RptOportunidadSectorGerentePreventaController(RptOportunidadSectorGerentePreventaService, blockUI, $timeout, UtilsFactory, $urls, DTOptionsBuilder, DTColumnBuilder,$compile,  $scope) {
        var vm = this;

        vm.pageNumber = 0;
        vm.tamanioPagina = 0;

        vm.ListaSectorIncluir = [];
        vm.ListaSectorExcluir = [];

        vm.ListaLiderIncluir = [];
        vm.ListaLiderExcluir = [];

        vm.ListaPreventaIncluir = [];
        vm.ListaPreventaExcluir = [];

        vm.ListaClientesIncluir = [];
        vm.ListaClientesExcluir = [];

        vm.MostrarReporte = MostrarReporte;

        vm.BuscarOportunidades = BuscarOportunidades;
        vm.SeleccionarSector = SeleccionarSector;
        vm.SeleccionarLider = SeleccionarLider;
        vm.SeleccionarPreventa = SeleccionarPreventa;
        vm.SeleccionarCliente = SeleccionarCliente;

        function MostrarReporte() {
            var fechaActual = new Date();
            var anioActual = fechaActual.getFullYear();
           

            var parametros =
                    "reporte=RptOportunidadSectorGerentePreventa&Anio=" + anioActual;

            blockUI.start();

            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);


            BuscarOportunidades();
        }

        vm.dtColumns = [
        //DTColumnBuilder.newColumn('Orden').withTitle('Orden').notSortable(),
        //DTColumnBuilder.newColumn('OrdenMostrar').withTitle('OrdenMostrar').notSortable(),
        DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaSector).withClass('widthSelectorGrilla'),
        DTColumnBuilder.newColumn('Sector').withTitle('SECTOR').notSortable(),
        DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaLider).withClass('widthSelectorGrilla'),
        DTColumnBuilder.newColumn('OportunidadesCadena').withTitle('OPORTUNIDADES').notSortable(),
        DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaPreventa).withClass('widthSelectorGrilla'),
        DTColumnBuilder.newColumn('EquipoPreventaCadena').withTitle('').notSortable(),
        DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaClientes).withClass('widthSelectorGrilla'),
        DTColumnBuilder.newColumn('CarteraClientesCadena').withTitle('').notSortable(),
        DTColumnBuilder.newColumn('NroOportunidadesLiderCadena').withTitle('').notSortable(),
        DTColumnBuilder.newColumn('WinRateLiderCadena').withTitle('').notSortable(),
        DTColumnBuilder.newColumn('TiempoEntregaOfertaCadena').withTitle('').notSortable(),
        DTColumnBuilder.newColumn('PorcentajeCumplimientoEntregaOfertaCadena').withTitle('').notSortable(),
        DTColumnBuilder.newColumn('WinRateClienteCadena').withTitle('').notSortable(),
        DTColumnBuilder.newColumn('TiempoEntregaPromedioClienteCadena').withTitle('').notSortable(),
        DTColumnBuilder.newColumn('PorcentajeCumplimientoClienteCadena').withTitle('').notSortable(),
        DTColumnBuilder.newColumn('FaseOportunidad').withTitle('').notSortable(),
        DTColumnBuilder.newColumn('ProbabilidadExito').withTitle('').notSortable()
        
        ];
        
        function AccionesBusquedaSector(data, type, full, meta) {
            if (data.Nivel == '1') {
             
              return "<div class='col-xs-6 col-sm-6'><a title='" + (data.Desplegado == '1' ? 'Ocultar Detalle Sector' : 'Mostrar Detalle Sector') + "' class='btn btn-sm' onclick='SeleccionarSector(2," + data.IdSector + "," + data.Desplegado + ");'  id='tab-buttonNivel1' >" + "<span class='" + (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') + "'></span>" + "</a></div> ";

            }
            else {
                return "";
            }
        }
        
        function AccionesBusquedaLider(data, type, full, meta) {
            if (data.Nivel == '2' && data.OportunidadesCadena != 'LIDER') {

                return "<div class='col-xs-6 col-sm-6'><a title='" + (data.Desplegado == '1' ? 'Ocultar Detalle Lider' : 'Mostrar Detalle Lider') + "' class='btn btn-sm' onclick='SeleccionarLider(3," + data.IdSector + "," + data.IdLider + "," + data.Desplegado + ");'  id='tab-buttonNivel2' >" + "<span class='" + (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') + "'></span>" + "</a></div> ";
            }
            else {
                return "";
            }
        }

        function AccionesBusquedaPreventa(data, type, full, meta) {
            if (data.Nivel == '3' && data.EquipoPreventaCadena != 'PREVENTA') {

                return "<div class='col-xs-6 col-sm-6'><a title='" + (data.Desplegado == '1' ? 'Ocultar Detalle Preventa' : 'Mostrar Detalle Preventa') + "' class='btn btn-sm' onclick='SeleccionarPreventa(4," + data.IdSector + "," + data.IdLider + "," + data.IdPreventa + "," + data.Desplegado + ");'  id='tab-buttonNivel3' >" + "<span class='" + (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') + "'></span>" + "</a></div> ";
            }
            else {
                return "";
            }
        }

        function AccionesBusquedaClientes(data, type, full, meta) {

            if (data.Nivel == '4' && data.CarteraClientesCadena != 'CLIENTE') {

                return "<div class='col-xs-6 col-sm-6'><a title='" + (data.Desplegado == '1' ? 'Ocultar Detalle Cliente' : 'Mostrar Detalle Cliente') + "' class='btn btn-sm' onclick='SeleccionarCliente(5," + data.IdSector + "," + data.IdLider + "," + data.IdPreventa + "," + data.IdCliente + "," + data.Desplegado + ");'  id='tab-buttonNivel4' >" + "<span class='" + (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') + "'></span>" + "</a></div> ";
            }
            else {
                return "";
            }
        }

        function BuscarOportuniadesComun(sSource, aoData, fnCallback, oSettings, request) {
            var fechaActual = new Date();
            var anioActual = fechaActual.getFullYear()

            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            request.Anio = anioActual;
            request.IdJefe = idUsuario;
            
            if (pageNumber != vm.pageNumber || length != vm.tamanioPagina)
            {
                vm.ListaSectorIncluir = [];
                vm.ListaSectorExcluir = [];
                vm.ListaLiderIncluir = [];
                vm.ListaLiderExcluir = [];
                vm.ListaPreventaIncluir = [];
                vm.ListaPreventaExcluir = [];
                vm.ListaClientesIncluir = [];
                vm.ListaClientesExcluir = [];

                request.Nivel = 1;
                request.ListaSectorIncluir = JSON.stringify(vm.ListaSectorIncluir);
                request.ListaSectorExcluir = JSON.stringify(vm.ListaSectorExcluir);
                request.ListaLiderIncluir = JSON.stringify(vm.ListaLiderIncluir);
                request.ListaLiderExcluir = JSON.stringify(vm.ListaLiderExcluir);
                request.ListaPreventaIncluir = JSON.stringify(vm.ListaPreventaIncluir);
                request.ListaPreventaExcluir = JSON.stringify(vm.ListaPreventaExcluir);
                request.ListaClientesIncluir = JSON.stringify(vm.ListaClientesIncluir);
                request.ListaClientesExcluir = JSON.stringify(vm.ListaClientesExcluir);
            }

            vm.pageNumber = pageNumber;
            vm.tamanioPagina = length;

            request.Paginacion =
                {
                    Indice: pageNumber,
                    Tamanio: length
                };

            var promise = RptOportunidadSectorGerentePreventaService.ListarOportunidades(request);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.Total,
                    'recordsFiltered': resultado.data.Total,
                    'data': resultado.data.ListaOportunidades
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
            });
        }


         function BuscarOportunidadesPaginado(sSource, aoData, fnCallback, oSettings) {

             var request = {
                 Nivel: 1
            };

             BuscarOportuniadesComun(sSource, aoData, fnCallback, oSettings, request);
            
         }

         function LimpiarGrilla() {
             vm.dtOptions = DTOptionsBuilder
             .newOptions()
             //.withOption('data', [])
             .withOption('bFilter', false)
             .withOption('responsive', false)
             .withOption('destroy', true)
             .withOption('order', [])
             .withDisplayLength(0)
             .withOption('paging', false);
         }

        function BuscarOportunidades() {
           
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('stateSave', true)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarOportunidadesPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }

        function BuscarOportunidadesPaginadoNivel(sSource, aoData, fnCallback, oSettings) {

            var request = {
                    Nivel: 0,
                    ListaSectorIncluir: JSON.stringify(vm.ListaSectorIncluir),
                    ListaSectorExcluir: JSON.stringify(vm.ListaSectorExcluir),
                    ListaLiderIncluir: JSON.stringify(vm.ListaLiderIncluir),
                    ListaLiderExcluir: JSON.stringify(vm.ListaLiderExcluir),
                    ListaPreventaIncluir: JSON.stringify(vm.ListaPreventaIncluir),
                    ListaPreventaExcluir: JSON.stringify(vm.ListaPreventaExcluir),
                    ListaClientesIncluir: JSON.stringify(vm.ListaClientesIncluir),
                    ListaClientesExcluir: JSON.stringify(vm.ListaClientesExcluir)
            };

            BuscarOportuniadesComun(sSource, aoData, fnCallback, oSettings, request);
        }


        function SeleccionarComun() {
           LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('stateSave', true)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarOportunidadesPaginadoNivel)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {

                    if (data.OportunidadesCadena == 'LIDER' || data.EquipoPreventaCadena == 'PREVENTA' || data.CarteraClientesCadena == 'CLIENTE' || data.NroOportunidadesLiderCadena == "OPORTUNIDAD") {
                        $('td', row).addClass('header_pagining');
                        $compile(angular.element(row).contents())($scope);
                    }

                })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }

        function SeleccionarSector(nivel, _idSector, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idSector: _idSector,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaSectorIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaSectorExcluir.push(objeto);
                }
            }

            SeleccionarComun();

        }

        function SeleccionarLider(nivel, _idSector, _idLider, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idSector: _idSector,
                    idLider: _idLider,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaLiderIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaLiderExcluir.push(objeto);
                }
            }

            SeleccionarComun();

        }

        function SeleccionarPreventa(nivel,_idSector, _idLider, _idPreventa, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idSector: _idSector,
                    idLider: _idLider,
                    idPreventa: _idPreventa,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaPreventaIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaPreventaExcluir.push(objeto);
                }
            }

            SeleccionarComun();

        }

        function SeleccionarCliente(nivel, _idSector, _idLider, _idPreventa, _idCliente, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idSector: _idSector,
                    idLider: _idLider,
                    idPreventa: _idPreventa,
                    IdCliente: _idCliente,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaClientesIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaClientesExcluir.push(objeto);
                }
            }

            SeleccionarComun();

        }


    }
})();
