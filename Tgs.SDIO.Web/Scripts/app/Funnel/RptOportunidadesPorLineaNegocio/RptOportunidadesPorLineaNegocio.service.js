﻿(function () {
    'use strict',
     angular
    .module('app.Funnel')
    .service('RptOportunidadesPorLineaNegocioService', RptOportunidadesPorLineaNegocioService);

    RptOportunidadesPorLineaNegocioService.$inject = ['$http', 'URLS'];

    function RptOportunidadesPorLineaNegocioService($http, $urls) {

        var service = {
            
            ListarLineasNegocios: LineasNegocios
        };

        return service;
        function LineasNegocios(filtroLineasNegocio) {

            return $http({
                url: $urls.ApiFunnel + "RptOportunidadesPorLineaNegocio/ListarLineasNegocio",
                method: "POST",
                data: JSON.stringify(filtroLineasNegocio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();

