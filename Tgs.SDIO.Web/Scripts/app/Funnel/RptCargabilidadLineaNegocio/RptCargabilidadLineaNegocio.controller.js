﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('CargabilidadLineaNegocioController', CargabilidadLineaNegocioController);

    CargabilidadLineaNegocioController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS'];

    function CargabilidadLineaNegocioController(blockUI, $timeout, UtilsFactory, $urls) {
        var vm = this;
        vm.ConsultarReporte = ConsultarReporte;

        var fechaActual = new Date();
        var anioActual = fechaActual.getFullYear();

        function ConsultarReporte() {

            var parametros = "reporte=PyC_CargabilidadLineaNegocio&Anio=" + anioActual;

            blockUI.start();

            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);

            $timeout(function () {
                blockUI.stop();
            }, 5000);

        }
    }

})();

