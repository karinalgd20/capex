﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('ReportMadurezController', ReportMadurezController);

    ReportMadurezController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS', 'LineaNegocioService','SalesForceConsolidadoCabeceraService','MaestraService','SectorService'];

    function ReportMadurezController(blockUI, $timeout, UtilsFactory, $urls, LineaNegocioService,SalesForceConsolidadoCabeceraService,MaestraService,SectorService) {
        var vm = this;
        vm.ConsultarReporte = ConsultarReporte;
        vm.LimpiarModelo = LimpiarModelo;
        vm.ExportarReporte = ExportarReporte;


        vm.ListarLineaNegocio = ListarLineaNegocio;
        vm.ListarSector = ListarSector;
        vm.ListarProbabilidades = ListarProbabilidades;
        vm.ListarEtapa = ListarEtapa;
        vm.IdLineaNegocio = "0";
        vm.IdSector = "0";
        vm.IdProbabilidad = -1;
        vm.IdEtapa = "0";
        
        vm.ListaAnios = '2018';
        vm.ListaMeses = '';
        vm.ListLineaNegocio = [];
        vm.ListSector = [];
        vm.ListProbabilidades = [];
        vm.ListEtapa = [];


        function LimpiarModelo() {
            vm.ListaAnios = '2018';
            vm.ListaMeses = '';
            vm.IdLineaNegocio = "0";
            vm.IdSector = "0";
            vm.IdProbabilidad = -1;
            vm.IdEtapa = "0";
            $("[id$='FReporte']").contents().find('body').html('');
        };
        
        function ListarLineaNegocio()
        {
            var lineaNegocio = {
                IdEstado : 1
            };

            var promise = LineaNegocioService.ListarLineaNegocios(lineaNegocio);
                
            promise.then(function (resultado) {
                blockUI.stop();

                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelectTodos(resultado.data);


            }, function (response) {
                blockUI.stop();

            });

        }

         function ListarSector()
            {
                var sector = {
                    IdEstado : 1
                };

                var promise = SectorService.ListarSector(sector);

                promise.then(function (resultado) {
                    blockUI.stop();

                    vm.ListSector = UtilsFactory.AgregarItemSelectTodos(resultado.data);


                }, function (response) {
                    blockUI.stop();

                });

         }

         function ListarProbabilidades()
            {
                var promise = SalesForceConsolidadoCabeceraService.ListarProbabilidades();

                promise.then(function (resultado) {
                    blockUI.stop();

                    vm.ListProbabilidades = resultado.data;


                }, function (response) {
                    blockUI.stop();

                });
         }


         function ListarEtapa()
         {
             var maestra = {
                 IdEstado: 1,
                 IdRelacion: 129

             };
             var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

             promise.then(function (resultado) {
                 blockUI.stop();

                 vm.ListEtapa = UtilsFactory.AgregarItemSelectTodos(resultado.data);

             }, function (response) {
                 blockUI.stop();

             });

         }


        function ConsultarReporte() {

            var meses = '0';
            if (vm.ListaMeses == '' || vm.ListaMeses == null) {
                meses = '0';
            }
            else {
                meses = vm.ListaMeses;
            }

            var parametros =
                    "reporte=MadurezPorMes&Anio=" + vm.ListaAnios + '&Mes=' + meses + '&LineaNegocio=' + vm.IdLineaNegocio +
                    "&Sector=" + vm.IdSector + '&Probabilidad=' + vm.IdProbabilidad + '&Etapa=' + vm.IdEtapa + 
                    "&urlReporte=" + encodeURIComponent(UtilsFactory.GetUrlAbsoluta() + "Funnel/RptProbabilidadPorMes"); 

            blockUI.start();

            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);

            $timeout(function () {
                blockUI.stop();
            }, 2000);

        };
        
        function ExportarReporte()
        {

            var meses = '0';
            if (vm.ListaMeses == '' || vm.ListaMeses == null) {
                meses = '0';
            }
            else
            {
                meses = vm.ListaMeses;
            }

            var item = {
                Anio: vm.ListaAnios,
                Mes: meses,
                IdLineaNegocio: vm.IdLineaNegocio,
                IdSector: vm.IdSector,
                ProbabilidadExito: vm.IdProbabilidad,
                Etapa: vm.IdEtapa
            };


            var json = JSON.stringify(item);

            window.location.href = $urls.ApiFunnel + "RptMadurez/ExportarOfertasAnio?item=" + json;
        }

    }
})();

