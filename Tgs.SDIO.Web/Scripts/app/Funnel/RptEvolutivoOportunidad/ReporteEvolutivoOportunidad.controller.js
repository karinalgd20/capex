﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('ReporteEvolutivoOportunidadController', ReporteEvolutivoOportunidadController);

    ReporteEvolutivoOportunidadController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS'];

    function ReporteEvolutivoOportunidadController(blockUI, $timeout, UtilsFactory, $urls) {
        var vm = this;

        angular.element(document).ready(function () {

            if (IdOportunidad !== '' || IdOportunidad !== undefined) {

                var parametros = "reporte=EvolutivoOportunidad&IdOportunidad=" + IdOportunidad;

                blockUI.start();

                $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);

                $timeout(function () {
                    blockUI.stop();
                }, 2000);

            }

        });

    }
})();

