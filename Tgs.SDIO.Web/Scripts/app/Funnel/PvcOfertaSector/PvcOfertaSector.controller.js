﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('PvcOfertaSectorController', PvcOfertaSectorController);

    PvcOfertaSectorController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS'];

    function PvcOfertaSectorController(blockUI, $timeout, UtilsFactory, $urls) {
        var vm = this;
        vm.ConsultarReporte = ConsultarReporte;
        vm.LimpiarModelo = LimpiarModelo;
         
        var fechaActual = new Date();
        var anioActual = fechaActual.getFullYear();

        vm.ListaAnios = anioActual;
        vm.ListaMeses = '';

        function ConsultarReporte() {

            var parametros = "reporte=FCVOfertasSector&Anio=" + vm.ListaAnios + "&Mes=" + vm.ListaMeses;

            blockUI.start();

            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);

            $timeout(function () {
                blockUI.stop();

            }, 5000);

        }

        function LimpiarModelo() {

            vm.ListaAnios = anioActual;
            vm.ListaMeses = '';

            $("[id$='FReporte']").contents().find('body').html('');
        };

    }

})();

