﻿(function () {
    'use strict'

    angular
    .module('app.Funnel')
    .controller('RptProbabilidadFechaCierreController', RptProbabilidadFechaCierreController);

    RptProbabilidadFechaCierreController.$inject = [
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];


    function RptProbabilidadFechaCierreController(
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.MostrarReporte = MostrarReporte;

        function MostrarReporte()
        {
            var fechaActual = new Date();
            var anioActual = fechaActual.getFullYear();

            var parametros = "reporte=DashboardPrincipal&Anio=" + anioActual +
             "&urlReporte=" + encodeURIComponent(UtilsFactory.GetUrlAbsoluta() + "Funnel/RptProbabilidadPorMesPopup");

            blockUI.start();

            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);

            $timeout(function () {
                blockUI.stop();
            }, 5000);
        }
        
    }
})();

