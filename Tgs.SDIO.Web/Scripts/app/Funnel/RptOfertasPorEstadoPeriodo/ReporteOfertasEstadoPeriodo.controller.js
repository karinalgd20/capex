﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('ReporteOfertaEstadoPeriodoController', ReporteOfertaEstadoPeriodoController);

    ReporteOfertaEstadoPeriodoController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS', 'LineaNegocioService', 'SectorService','SalesForceConsolidadoCabeceraService','MaestraService'];

    function ReporteOfertaEstadoPeriodoController(blockUI, $timeout, UtilsFactory, $urls, LineaNegocioService, SectorService, SalesForceConsolidadoCabeceraService,MaestraService) {
        var vm = this;
        vm.ConsultarReporte = ConsultarReporte;
        vm.LimpiarModelo = LimpiarModelo;

        vm.ListarLineaNegocio = ListarLineaNegocio;
        vm.ListarSector = ListarSector;
        vm.ListarProbabilidades = ListarProbabilidades;
        vm.ListarMadurez = ListarMadurez;
        
        vm.ListaAnios = '2018';
        vm.ListaMeses = '';
        vm.IdLineaNegocio = "0";
        vm.IdSector = "0";
        vm.IdProbabilidad = -1;
         vm.IdMadurez = "0";


        vm.ListLineaNegocio = [];
        vm.ListSector = [];
        vm.ListProbabilidades = [];
        vm.ListMadurez = [];


        function ListarLineaNegocio()
        {
            var lineaNegocio = {
                IdEstado : 1
            };

            var promise = LineaNegocioService.ListarLineaNegocios(lineaNegocio);
                
            promise.then(function (resultado) {
                blockUI.stop();

                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelectTodos(resultado.data);


            }, function (response) {
                blockUI.stop();

            });

        }

        function ListarSector() {
            var sector = {
                IdEstado: 1
            };

            var promise = SectorService.ListarSector(sector);

            promise.then(function (resultado) {
                blockUI.stop();

                vm.ListSector = UtilsFactory.AgregarItemSelectTodos(resultado.data);


            }, function (response) {
                blockUI.stop();

            });

        }

        function ListarProbabilidades() {
            var promise = SalesForceConsolidadoCabeceraService.ListarProbabilidades();

            promise.then(function (resultado) {
                blockUI.stop();

                vm.ListProbabilidades = resultado.data;


            }, function (response) {
                blockUI.stop();

            });

        }

        function ListarMadurez() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 136

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                vm.ListMadurez = UtilsFactory.AgregarItemSelectTodos(resultado.data);

            }, function (response) {
                blockUI.stop();

            });

        }

        function LimpiarModelo() {
            
            vm.ListaAnios = '2018';
            vm.ListaMeses = '';
            vm.IdLineaNegocio = "0";
            vm.IdSector = "0";
            vm.IdProbabilidad = -1;
            vm.IdMadurez = "0"; 

            $("[id$='FReporte']").contents().find('body').html('');
            
        };

        function ConsultarReporte() {

            var meses = '0';
            if (vm.ListaMeses == '' || vm.ListaMeses == null) {
                meses = '0';
            }
            else {
                meses = vm.ListaMeses;
            }

           var parametros =
                    "reporte=OfertaPorEstado&Anio=" + vm.ListaAnios +
                    '&Mes=' + meses + '&IdLineaNegocio=' + vm.IdLineaNegocio +
                    "&Sector=" + vm.IdSector + 
                    '&Probabilidad=' + vm.IdProbabilidad + '&Madurez=' + vm.IdMadurez;;

            blockUI.start();
            
            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
            
            $timeout(function () {
                blockUI.stop();
            },2000);

        };
    }
})();

