﻿(function () {
    'use strict',
     angular
    .module('app.Funnel')
    .service('RptPreventaOportunidadesPreventaService', RptPreventaOportunidadesPreventaService);

    RptPreventaOportunidadesPreventaService.$inject = ['$http', 'URLS'];

    function RptPreventaOportunidadesPreventaService($http, $urls) {

        var service = {
            //SubServicios
            ListarOportunidades: ListarOportunidades
        };

        return service;
        function ListarOportunidades(oportunidad) {

            return $http({
                url: $urls.ApiFunnel + "RptPreventaOportunidadesPreventa/ListarOportunidades",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();


