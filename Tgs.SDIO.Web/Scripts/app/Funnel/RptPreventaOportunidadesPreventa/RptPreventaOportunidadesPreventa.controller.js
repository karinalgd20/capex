﻿(function () {
    'use strict'

    angular
    .module('app.Funnel')
    .controller('RptPreventaOportunidadesPreventaController', RptPreventaOportunidadesPreventaController);

    RptPreventaOportunidadesPreventaController.$inject = ['RptPreventaOportunidadesPreventaService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];


    function RptPreventaOportunidadesPreventaController(RptPreventaOportunidadesPreventaService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.pageNumber = 0;
        vm.tamanioPagina = 0;

        vm.ListaClientesIncluir = [];
        vm.ListaClientesExcluir = [];

        vm.BuscarOportunidades = BuscarOportunidades;
        vm.SeleccionarCliente = SeleccionarCliente;

        vm.dtColumns = [
         //DTColumnBuilder.newColumn('Orden').withTitle('Orden').notSortable(),
         //DTColumnBuilder.newColumn('OrdenMostrar').withTitle('OrdenMostrar').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaClientes).withClass('widthSelectorGrilla'),
         DTColumnBuilder.newColumn('Cliente').withTitle('CLIENTE').notSortable(),
         DTColumnBuilder.newColumn('OportunidadesAbiertasClienteCadena').withTitle('CANTIDAD DE OPORTUNIDADES ABIERTAS').notSortable(),
         DTColumnBuilder.newColumn('OfertasTrabajadasClienteCadena').withTitle('CANTIDAD DE OPORTUNIDADES TRABAJADAS').notSortable(),
         DTColumnBuilder.newColumn('OfertasGanadasClienteCadena').withTitle('CANTIDAD DE OPORTUNIDADES GANADAS').notSortable(),
         DTColumnBuilder.newColumn('WinRateClienteCadena').withTitle('WIN RATE').notSortable(),
         DTColumnBuilder.newColumn('TiempoEntregaPromedioClienteCadena').withTitle('TIEMPO ENTREGA OFERTA PROMEDIO').notSortable(),
         DTColumnBuilder.newColumn('PorcentajeCumplimientoClienteCadena').withTitle('% CUMPLIMIENTO DE ENTREGA DE OFERTAS').notSortable(),
         DTColumnBuilder.newColumn('IspClienteCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('FechaCierreEstimadaCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('NumeroDelCaso').withTitle('').notSortable(),
        ];
        
        function AccionesBusquedaClientes(data, type, full, meta) {

            if (data.Nivel == '1') {

                return "<div class='col-xs-6 col-sm-6'><a title='" + (data.Desplegado == '1' ? 'Ocultar Detalle Cliente' : 'Mostrar Detalle Cliente') + "' class='btn btn-sm' onclick='SeleccionarCliente(2," +data.IdCliente + "," +data.Desplegado + ");'  id='tab-buttonNivel3' >" + "<span class='" +(data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') +"'></span>" + "</a></div> ";
            }
            else {
                return "";
            }
        }

        function SeleccionarComun()
        {
            //blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('stateSave', true)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarOportunidadesPaginadoNivel)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {

                    if (data.OportunidadesAbiertasClienteCadena == 'OPORTUNIDAD') {
                        $('td', row).addClass('header_pagining');
                        $compile(angular.element(row).contents())($scope);
                    }

                })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }


        function SeleccionarCliente(nivel, _idCliente, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    IdCliente: _idCliente,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaClientesIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaClientesExcluir.push(objeto);
                }
            }

            SeleccionarComun();
        }


        function BuscarOportunidades() {
            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('stateSave', true)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarOportunidadesPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }

        function BuscarOportuniadesComun(sSource, aoData, fnCallback, oSettings, request)
        {
            var fechaActual = new Date();
            var anioActual = fechaActual.getFullYear()

            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            request.oportunidadesFiltro.Anio = anioActual;
            request.oportunidadesFiltro.IdJefe = idUsuario;

            if (pageNumber != vm.pageNumber || length != vm.tamanioPagina) {

                vm.ListaClientesIncluir = [];
                vm.ListaClientesExcluir = [];

                request.Nivel = 1;

                request.oportunidadesFiltro.ListaClientesIncluir = JSON.stringify(vm.ListaClientesIncluir);
                request.oportunidadesFiltro.ListaClientesExcluir = JSON.stringify(vm.ListaClientesExcluir);
            }

            vm.pageNumber = pageNumber;
            vm.tamanioPagina = length;


            request.oportunidadesFiltro.Paginacion =
                {
                    Indice: pageNumber,
                    Tamanio: length
                };

            var promise = RptPreventaOportunidadesPreventaService.ListarOportunidades(request);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.Total,
                    'recordsFiltered': resultado.data.Total,
                    'data': resultado.data.ListaOportunidades
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
            });
        }


        function BuscarOportunidadesPaginado(sSource, aoData, fnCallback, oSettings) {

             var request = {
                oportunidadesFiltro :
                {
                    Nivel: 1
                }
            };

             BuscarOportuniadesComun(sSource, aoData, fnCallback, oSettings, request); 
        }
        
        function BuscarOportunidadesPaginadoNivel(sSource, aoData, fnCallback, oSettings) {

            var request = {
                oportunidadesFiltro:
                {
                    Nivel: 0,
                    ListaClientesIncluir: JSON.stringify(vm.ListaClientesIncluir),
                    ListaClientesExcluir: JSON.stringify(vm.ListaClientesExcluir),
                }
            };

            BuscarOportuniadesComun(sSource, aoData, fnCallback, oSettings, request);
        }


        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            //.withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        
    }
})();

 