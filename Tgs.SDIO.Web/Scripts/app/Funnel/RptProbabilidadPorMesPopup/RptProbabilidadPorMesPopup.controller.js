﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('RptProbabilidadPorMesPopupController', RptProbabilidadPorMesPopupController);

    RptProbabilidadPorMesPopupController.$inject = ['UtilsFactory', 'URLS'];

    function RptProbabilidadPorMesPopupController(UtilsFactory, $urls) {
        var vm = this;

        angular.element(document).ready(function () {
            
            if (IdOportunidad != '' || IdOportunidad != undefined) {

                var parametros =
                    "reporte=ProbabilidadPorMesPopup&IdOportunidad=" + IdOportunidad;
                $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
            }
        });
    }
})();

