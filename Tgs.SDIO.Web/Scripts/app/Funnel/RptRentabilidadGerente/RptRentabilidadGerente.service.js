﻿(function () {
    'use strict',
     angular
    .module('app.Funnel')
    .service('RptRentabilidadGerenteService', RptRentabilidadGerenteService);

    RptRentabilidadGerenteService.$inject = ['$http', 'URLS'];

    function RptRentabilidadGerenteService($http, $urls) {

        var service = {
            
            ListarAnalistasFinancieros: AnalistasFinancieros
        };

        return service;
        function AnalistasFinancieros(oportunidad) {

            return $http({
                url: $urls.ApiFunnel + "RptRentabilidadGerente/ListarAnalistasFinancieros",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();


