﻿(function () {
    'use strict'

    angular
    .module('app.Funnel')
    .controller('RptRentabilidadGerenteController', RptRentabilidadGerenteController);

    RptRentabilidadGerenteController.$inject = ['RptRentabilidadGerenteService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];


    function RptRentabilidadGerenteController(RptRentabilidadGerenteService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.pageNumber = 0;
        vm.tamanioPagina = 0;

        vm.ListaAnalistaFinancierosIncluir = [];
        vm.ListaAnalistaFinancierosExcluir = [];

        vm.ListaClientesIncluir = [];
        vm.ListaClientesExcluir = [];

        vm.ListaOportunidadesIncluir = [];
        vm.ListaOportunidadesExcluir = [];

        vm.ListaCasosIncluir = [];
        vm.ListaCasosExcluir = [];


        vm.BuscarOportunidades = BuscarOportunidades;
        vm.SeleccionarAnalista = SeleccionarAnalista;
        vm.SeleccionarCliente = SeleccionarCliente;
        vm.SeleccionarOportunidad = SeleccionarOportunidad;
        vm.SeleccionarOferta = SeleccionarOferta;


        vm.dtColumns = [
         //DTColumnBuilder.newColumn('Orden').withTitle('Orden').notSortable(),
         //DTColumnBuilder.newColumn('OrdenMostrar').withTitle('OrdenMostrar').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaAnalistas).withClass('widthSelectorGrilla'),
         DTColumnBuilder.newColumn('AnalistaFinanciero').withTitle('ANALISTA').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaClientes).withClass('widthSelectorGrilla'),
         DTColumnBuilder.newColumn('CarteraClientesLiderCadena').withTitle('CARTERA CLIENTES').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaOportunidades).withClass('widthSelectorGrilla'),
         DTColumnBuilder.newColumn('OportunidadesCadena').withTitle('OPORTUNIDADES').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaCaso).withClass('widthSelectorGrilla'),
         DTColumnBuilder.newColumn('TiempoEntregaPromedioCadena').withTitle('TIEMPO ENTREGA').notSortable(),
         DTColumnBuilder.newColumn('PorcentajeCumplimientoCadena').withTitle('% CUMPLIMIENTO').notSortable(),
         DTColumnBuilder.newColumn('TiempoEntregaPromedioClienteCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('CumplimientoEntregaClienteCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('CantidadOfertasClientes').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('FaseOportunidad').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('ProbabilidadExitoCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('FechaCierreEstimadaCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('OibdaCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('FlujoCajaCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('FullContractValueNetoCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('PayBack').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('PagoUnicoDolares').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('PagoRecurrenteDolares').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('MesesPagoRecurrenteCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('TipoCambioCadena').withTitle('').notSortable()
        ];
        

        function AccionesBusquedaAnalistas(data, type, full, meta) {
            if (data.Nivel == '1') {
                return "<div class='col-xs-6 col-sm-6'><a title='" + (data.Desplegado == '1' ? 'Ocultar Detalle Analista' : 'Mostrar Detalle Analista') + "' class='btn btn-sm' onclick='SeleccionarAnalista(2," + data.IdAnalistaFinanciero + "," + data.Desplegado + ");'  id='tab-buttonNivel1' >" + "<span class='" + (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') + "'></span>" + "</a></div> ";
            }
            else {
                return "";
            }
        }


        function AccionesBusquedaClientes(data, type, full, meta) {
            if (data.Nivel == '2' && data.CarteraClientesLiderCadena != "CLIENTE") {
                return "<div class='col-xs-6 col-sm-6'><a title='" + (data.Desplegado == '1' ? 'Ocultar Detalle Cliente' : 'Mostrar Detalle Cliente') + "' class='btn btn-sm' onclick='SeleccionarCliente(3," + data.IdAnalistaFinanciero + "," + data.IdCliente + "," + data.Desplegado + ");'  id='tab-buttonNivel2' >" + "<span class='" + (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') + "'></span>" + "</a></div> ";
            }
            else {
                return "";
            }
        }
        
        function AccionesBusquedaOportunidades(data, type, full, meta) {
            if (data.Nivel == '3' && data.OportunidadesCadena != "OPORTUNIDAD") {
                return "<div class='col-xs-6 col-sm-6'><a title='" + (data.Desplegado == '1' ? 'Ocultar Detalle Oportunidad' : 'Mostrar Detalle Oportunidad') + "' class='btn btn-sm' onclick='SeleccionarOportunidad(4," + data.IdAnalistaFinanciero + "," + data.IdCliente + ",\"" + data.IdOportunidad + "\"," + data.Desplegado + ");'  id='tab-buttonNivel3' >" + "<span class='" + (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') + "'></span>" + "</a></div> ";
            }
            else {
                return "";
            }
        }
        
        
        function AccionesBusquedaCaso(data, type, full, meta) {
            if (data.Nivel == '4' && data.TiempoEntregaPromedioCadena != "COD OFERTA" && data.TiempoEntregaPromedioCadena != "EVOLUTIVO DE OFERTAS" && data.GraficoEvolutivo == 0) {
                return "<div class='col-xs-6 col-sm-6'><a title='" + (data.Desplegado == '1' ? 'Ocultar Detalle Oferta' : 'Mostrar Detalle Oferta') + "' class='btn btn-sm' onclick='SeleccionarOferta(5," + data.IdAnalistaFinanciero + "," + data.IdCliente + ",\"" + data.IdOportunidad + "\"," + data.NumeroCaso + "," + data.Desplegado + ");'  id='tab-buttonNivel4' >" + "<span class='" + (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') + "'></span>" + "</a></div> ";
            }
            else {
                var fechaActual = new Date();
                var anioActual = fechaActual.getFullYear();

                if (data.Nivel == '4' && data.GraficoEvolutivo == 1)
                {
                    
                    var parametros1 =
                       "reporte=RptIngresosEvolutivos" +
                       "&Anio=" + anioActual +
                       "&IdOportunidad=" + data.IdOportunidad;

                    $('#FReporteEvolutivo_' + data.IdOportunidad).attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros1);


                    return "<iframe id='FReporteEvolutivo_" + data.IdOportunidad + "' style='width: 800px;height: 320px;margin-left: 100px;' class='iframe-style'></iframe>"
                }
                else
                {
                    if (data.Nivel == '5') {
                        var parametros2 =
                           "reporte=RptReporteOpexCapex" +
                           "&IdOportunidad=" + data.IdOportunidad +
                           "&NumeroCaso=" + data.NumeroCaso;

                        $('#FReporteIngresos_' + data.IdOportunidad + data.NumeroCaso).attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros2);


                        var parametros3 =
                          "reporte=ReporteCronogramaProyecto" +
                          "&IdOportunidad=" + data.IdOportunidad +
                          "&Anio=" + anioActual;

                        $('#FReporteCronograma_' + data.IdOportunidad + anioActual).attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros3);

                        return "<iframe id='FReporteIngresos_" + data.IdOportunidad + data.NumeroCaso + "' style='width: 800px;height: 300px;margin-left: 100px;' class='iframe-style'></iframe><br/>" +
                                "<iframe id='FReporteCronograma_" + data.IdOportunidad + anioActual + "' style='width: 800px;height: 300px;margin-left: 100px;' class='iframe-style'></iframe>";

                    }

                    else
                        return '';
                }
            }
        }


        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            //.withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }

        function BuscarOportunidadesPaginadoNivel(sSource, aoData, fnCallback, oSettings) {

            var request = {
                Nivel: 0,
                ListaAnalistaFinancierosIncluir: JSON.stringify(vm.ListaAnalistaFinancierosIncluir),
                ListaAnalistaFinancierosExcluir: JSON.stringify(vm.ListaAnalistaFinancierosExcluir),
                ListaClientesIncluir: JSON.stringify(vm.ListaClientesIncluir),
                ListaClientesExcluir: JSON.stringify(vm.ListaClientesExcluir),
                ListaOportunidadesIncluir: JSON.stringify(vm.ListaOportunidadesIncluir),
                ListaOportunidadesExcluir: JSON.stringify(vm.ListaOportunidadesExcluir),
                ListaCasosIncluir: JSON.stringify(vm.ListaCasosIncluir),
                ListaCasosExcluir: JSON.stringify(vm.ListaCasosExcluir)
            };

            BuscarOportunidadesComun(sSource, aoData, fnCallback, oSettings, request);
        }

        function SeleccionarComun() {
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('stateSave', true)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarOportunidadesPaginadoNivel)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {

                    if (data.CarteraClientesLiderCadena == 'CLIENTE' || data.OportunidadesCadena == 'OPORTUNIDAD' || data.TiempoEntregaPromedioCadena == 'COD OFERTA' || data.TiempoEntregaPromedioCadena == 'EVOLUTIVO DE OFERTAS') {
                        $('td', row).addClass('header_pagining');
                        $compile(angular.element(row).contents())($scope);
                    }

                })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }


        function SeleccionarAnalista(nivel, _idAnalistaFinanciero, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idAnalista: _idAnalistaFinanciero,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaAnalistaFinancierosIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaAnalistaFinancierosExcluir.push(objeto);
                }
            }

            SeleccionarComun();

        }

        function SeleccionarCliente(nivel,_idAnalistaFinanciero, _idCliente, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idAnalista: _idAnalistaFinanciero,
                    idCliente: _idCliente,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaClientesIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaClientesExcluir.push(objeto);
                }
            }

            SeleccionarComun();

        }
        
        function SeleccionarOportunidad(nivel, _idAnalistaFinanciero, _idCliente,_idOportunidad, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idAnalista: _idAnalistaFinanciero,
                    idCliente: _idCliente,
                    IdOportunidad: _idOportunidad,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaOportunidadesIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaOportunidadesExcluir.push(objeto);
                }
            }

            SeleccionarComun();

        }
        

        function SeleccionarOferta(nivel, _idAnalistaFinanciero, _idCliente, _idOportunidad,_numeroCaso, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idAnalista: _idAnalistaFinanciero,
                    idCliente: _idCliente,
                    IdOportunidad: _idOportunidad,
                    NumeroCaso: _numeroCaso,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaCasosIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaCasosExcluir.push(objeto);
                }
            }

            SeleccionarComun();
        }

        function BuscarOportunidadesComun(sSource, aoData, fnCallback, oSettings, request) {
            var fechaActual = new Date();
            var anioActual = fechaActual.getFullYear()

            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            request.Anio = anioActual;
            request.IdGerente = idUsuario;

            if (pageNumber != vm.pageNumber || length != vm.tamanioPagina) {

                vm.ListaAnalistaFinancierosIncluir = [];
                vm.ListaAnalistaFinancierosExcluir = [];
                vm.ListaClientesIncluir = [];
                vm.ListaClientesExcluir = [];
                vm.ListaOportunidadesIncluir = [];
                vm.ListaOportunidadesExcluir = [];
                vm.ListaCasosIncluir = [];
                vm.ListaCasosExcluir = [];

                request.Nivel = 1;
                request.ListaAnalistaFinancierosIncluir = JSON.stringify(vm.ListaAnalistaFinancierosIncluir);
                request.ListaAnalistaFinancierosExcluir = JSON.stringify(vm.ListaAnalistaFinancierosExcluir);
                request.ListaClientesIncluir = JSON.stringify(vm.ListaClientesIncluir);
                request.ListaClientesExcluir = JSON.stringify(vm.ListaClientesExcluir);
                request.ListaOportunidadesIncluir = JSON.stringify(vm.ListaOportunidadesIncluir);
                request.ListaOportunidadesExcluir = JSON.stringify(vm.ListaOportunidadesExcluir);
                request.ListaCasosIncluir = JSON.stringify(vm.ListaCasosIncluir);
                request.ListaCasosExcluir = JSON.stringify(vm.ListaCasosExcluir);

            }

            vm.pageNumber = pageNumber;
            vm.tamanioPagina = length;


            request.Indice =  pageNumber;
            request.Tamanio = length;

            var promise = RptRentabilidadGerenteService.ListarAnalistasFinancieros(request);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.Total,
                    'recordsFiltered': resultado.data.Total,
                    'data': resultado.data.ListadoOportunidades
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
            });
        }
        

        function BuscarOportunidadesPaginado(sSource, aoData, fnCallback, oSettings) {

            var request = {
                Nivel: 1
            };

            BuscarOportunidadesComun(sSource, aoData, fnCallback, oSettings, request);

        }


         function BuscarOportunidades() {
            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('stateSave', true)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarOportunidadesPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

         }

    }
})();

 