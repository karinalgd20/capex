﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('DashboardFunnelConsolidadoController', DashboardFunnelConsolidadoController);

    DashboardFunnelConsolidadoController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS','RecursoService'];

    function DashboardFunnelConsolidadoController(blockUI, $timeout, UtilsFactory, $urls, RecursoService) {
        var vm = this;
        vm.MostrarDashboard = MostrarDashboard;
        var idJefe = 0;

        function MostrarDashboard() {
            var fechaActual = new Date();
            var anioActual = fechaActual.getFullYear();
            var tipoReporte = 1;
            var idPreventa = 0;
            var perfiles = JSON.parse(codigosPerfiles);

            if (moduloReporte == 'Preventa')
            {
                 for (var i = 0; i <= perfiles.length - 1 ; i++)
                 {
                        if (perfiles[i] == gerentePreventa) {
                            tipoReporte = 1;
                            idJefe = idUsuario;
                            break;
                        }
                        else {
                            if (perfiles[i] == liderPreventa) {
                                tipoReporte = 2;
                                idJefe = idUsuario;
                                break;
                            }
                            else {
                                if (perfiles[i] == preventa) {
                                    tipoReporte = 3;
                                    idPreventa = idUsuario;
                                    break;
                                }
                            }
                        }
                 }

                 var parametros =
                         "reporte=DashboardFunnelConsolidado&Anio=" + anioActual +
                         "&IdJefe=" + idJefe +
                         "&IdPreventa=" + idPreventa +
                         "&TipoReporte=" + tipoReporte +
                         "&ModuloReporte=" + moduloReporte;

                 blockUI.start();

                 $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);

                 $timeout(function () {
                     blockUI.stop();
                 }, 5000);

            }
            else
            {
                if (moduloReporte == 'Rentabilidad')
                {
                        var request = {
                            IdUsuarioRais: idUsuario
                        }

                        var promise = RecursoService.ListarRecursoDeUsuario(request);

                        promise.then(function (resultado) {
                       
                        var idRecurso = resultado.data.IdRecurso;

                        if (idRecurso != 0) {
                            for (var i = 0; i <= perfiles.length - 1 ; i++) {
                                if (perfiles[i] == gerenteRentabilidad) {
                                    idJefe = idRecurso;
                                    break;
                                }
                            }
                        }
                        

                        if (idRecurso != 0) {
                            var parametros =
                                "reporte=DashboardFunnelConsolidado&Anio=" + anioActual +
                                "&IdJefe=" + idJefe +
                                "&IdPreventa=" + idPreventa +
                                "&TipoReporte=" + tipoReporte +
                                "&ModuloReporte=" + moduloReporte;

                            blockUI.start();

                            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);

                            $timeout(function () {
                                blockUI.stop();
                            }, 5000);
                        }
                        else {
                            UtilsFactory.Alerta('#divAlertRegistro', 'danger', "Ocurrio un error al Mostrar el Dashboard", 5);
                        }                       

                    }, function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlertRegistro', 'danger', "Ocurrio un error al Mostrar el Dashboard", 5);

                    });
                     
                }
                else
                {
                    if (moduloReporte == 'Producto')
                    {
                         var parametros =
                                "reporte=DashboardFunnelConsolidado&Anio=" + anioActual +
                                "&ModuloReporte=" + moduloReporte;

                            blockUI.start();

                            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);

                            $timeout(function () {
                                blockUI.stop();
                            }, 5000);
                    }
                }

            }

        }
        
    }
})();

