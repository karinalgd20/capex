﻿(function () {
    'use strict',
     angular
    .module('app.Funnel')
    .service('RptPreventaOportunidadesLiderService', RptPreventaOportunidadesLiderService);

    RptPreventaOportunidadesLiderService.$inject = ['$http', 'URLS'];

    function RptPreventaOportunidadesLiderService($http, $urls) {

        var service = {
            //SubServicios
            ListarOportunidades: ListarOportunidades
        };

        return service;
        function ListarOportunidades(oportunidad) {

            return $http({
                url: $urls.ApiFunnel + "RptPreventaOportunidadesLider/ListarOportunidades",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();


