﻿(function () {
    'use strict'

    angular
    .module('app.Funnel')
    .controller('RptPreventaOportunidadesLiderController', RptPreventaOportunidadesLiderController);

    RptPreventaOportunidadesLiderController.$inject = ['RptPreventaOportunidadesLiderService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];


    function RptPreventaOportunidadesLiderController(RptPreventaOportunidadesLiderService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.pageNumber = 0;
        vm.tamanioPagina = 0;


        vm.ListaPreventaIncluir = [];
        vm.ListaPreventaExcluir = [];

        vm.ListaClientesIncluir = [];
        vm.ListaClientesExcluir = [];


        vm.BuscarOportunidades = BuscarOportunidades;
        vm.SeleccionarPreventa = SeleccionarPreventa;
        vm.SeleccionarCliente = SeleccionarCliente;

        vm.dtColumns = [
         //DTColumnBuilder.newColumn('Orden').withTitle('Orden').notSortable(),
         //DTColumnBuilder.newColumn('OrdenMostrar').withTitle('OrdenMostrar').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaPreventa).withClass('widthSelectorGrilla'),
         DTColumnBuilder.newColumn('Preventa').withTitle('PREVENTA').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaClientes).withClass('widthSelectorGrilla'),
         DTColumnBuilder.newColumn('CantidadClientesPreventaCadena').withTitle('CANTIDAD DE CLIENTES').notSortable(),
         DTColumnBuilder.newColumn('OportunidadesPreventaCadena').withTitle('CANTIDAD OPORTUNIDADES').notSortable(),
         DTColumnBuilder.newColumn('WinRatePreventaCadena').withTitle('WIN RATE').notSortable(),
         DTColumnBuilder.newColumn('TiempoEntregaPromedioPreventaCadena').withTitle('TIEMPO ENTREGA OFERTA PROMEDIO').notSortable(),
         DTColumnBuilder.newColumn('PorcentajeCumplimientoPreventaCadena').withTitle('% CUMPLIMIENTO DE ENTREGA DE OFERTAS').notSortable(),
         DTColumnBuilder.newColumn('IspPreventaCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('TiempoEntregaPromedioClienteCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('PorcentajeCumplimientoClienteCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('IspClienteCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('FechaCierreEstimadaCadena').withTitle('').notSortable(),
        ];
        

        function AccionesBusquedaPreventa(data, type, full, meta)
        {
            if (data.Nivel == '1')
            {
                  return "<div class='col-xs-6 col-sm-6'><a title='"+ (data.Desplegado == '1' ? 'Ocultar Detalle Preventa' : 'Mostrar Detalle Preventa') +"' class='btn btn-sm' onclick='SeleccionarPreventa(2," + data.IdPreventa + "," + data.Desplegado + ");'  id='tab-buttonNivel2' >" + "<span class='" + (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') +"'></span>" + "</a></div> ";

            }
            else
            {
                return "";
            }
        }


        function AccionesBusquedaClientes(data, type, full, meta) {

            if (data.Nivel == '2' && data.CantidadClientesPreventaCadena != "CLIENTE") {

                  return  "<div class='col-xs-6 col-sm-6'><a title='" + (data.Desplegado == '1' ? 'Ocultar Detalle Cliente' : 'Mostrar Detalle Cliente') +"' class='btn btn-sm' onclick='SeleccionarCliente(3," + data.IdPreventa + "," + data.IdCliente + "," + data.Desplegado + ");'  id='tab-buttonNivel3' >" + "<span class='" + (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') +"'></span>" + "</a></div> ";
            }
            else {
                return "";
            }
        }

        function SeleccionarComun()
        {
            //blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('stateSave', true)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarOportunidadesPaginadoNivel)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {

                    if (data.CantidadClientesPreventaCadena == 'CLIENTE' || data.OportunidadesPreventaCadena == 'OPORTUNIDAD') {
                        $('td', row).addClass('header_pagining');
                        $compile(angular.element(row).contents())($scope);
                    }

                })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }

        function SeleccionarPreventa(nivel, _idPreventa, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idPreventa: _idPreventa,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaPreventaIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaPreventaExcluir.push(objeto);
                }
            }

            SeleccionarComun();

        }

        function SeleccionarCliente(nivel, _idPreventa, _idCliente, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idPreventa: _idPreventa,
                    IdCliente: _idCliente,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaClientesIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaClientesExcluir.push(objeto);
                }
            }

            SeleccionarComun();
        }


        function BuscarOportunidades() {
            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('stateSave', true)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarOportunidadesPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }

        function BuscarOportuniadesComun(sSource, aoData, fnCallback, oSettings, request)
        {
            var fechaActual = new Date();
            var anioActual = fechaActual.getFullYear()

            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            request.oportunidadesFiltro.Anio = anioActual;
            request.oportunidadesFiltro.IdJefe = idUsuario;

            if (pageNumber != vm.pageNumber || length != vm.tamanioPagina) {

                vm.ListaPreventaIncluir = [];
                vm.ListaPreventaExcluir = [];
                vm.ListaClientesIncluir = [];
                vm.ListaClientesExcluir = [];

                request.Nivel = 1;

                request.oportunidadesFiltro.ListaPreventaIncluir = JSON.stringify(vm.ListaPreventaIncluir);
                request.oportunidadesFiltro.ListaPreventaExcluir = JSON.stringify(vm.ListaPreventaExcluir);
                request.oportunidadesFiltro.ListaClientesIncluir = JSON.stringify(vm.ListaClientesIncluir);
                request.oportunidadesFiltro.ListaClientesExcluir = JSON.stringify(vm.ListaClientesExcluir);
            }

            vm.pageNumber = pageNumber;
            vm.tamanioPagina = length;


            request.oportunidadesFiltro.Paginacion =
                {
                    Indice: pageNumber,
                    Tamanio: length
                };

            var promise = RptPreventaOportunidadesLiderService.ListarOportunidades(request);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.Total,
                    'recordsFiltered': resultado.data.Total,
                    'data': resultado.data.ListaOportunidades
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
            });
        }


        function BuscarOportunidadesPaginado(sSource, aoData, fnCallback, oSettings) {

             var request = {
                oportunidadesFiltro :
                {
                    Nivel: 1
                }
            };

             BuscarOportuniadesComun(sSource, aoData, fnCallback, oSettings, request); 
        }
        
        function BuscarOportunidadesPaginadoNivel(sSource, aoData, fnCallback, oSettings) {

            var request = {
                oportunidadesFiltro:
                {
                    Nivel: 0,
                    ListaPreventaIncluir: JSON.stringify(vm.ListaPreventaIncluir),
                    ListaPreventaExcluir: JSON.stringify(vm.ListaPreventaExcluir),
                    ListaClientesIncluir: JSON.stringify(vm.ListaClientesIncluir),
                    ListaClientesExcluir: JSON.stringify(vm.ListaClientesExcluir),
                }
            };

            BuscarOportuniadesComun(sSource, aoData, fnCallback, oSettings, request);
        }


        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            //.withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        
    }
})();

 