﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('ReportIngresoLineaNegocioPeriodoController', ReportIngresoLineaNegocioPeriodoController);

    ReportIngresoLineaNegocioPeriodoController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS', 'MaestraService','SalesForceConsolidadoCabeceraService'];

    function ReportIngresoLineaNegocioPeriodoController(blockUI, $timeout, UtilsFactory, $urls,MaestraService,SalesForceConsolidadoCabeceraService) {
        var vm = this;
        vm.ConsultarReporte = ConsultarReporte;
        vm.LimpiarModelo = LimpiarModelo;
        

        vm.ListaAnios = '2018';
        vm.ListaMeses = '';

        vm.ListarEtapa = ListarEtapa;
        vm.ListarProbabilidades = ListarProbabilidades;
        vm.ListarMadurez = ListarMadurez;

        vm.IdEtapa = "0";
        vm.IdProbabilidad = -1;
        vm.IdMadurez = "0";

        vm.ListEtapa = [];
        vm.ListProbabilidades = [];
        vm.ListMadurez = [];
        

        function ListarEtapa() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 129

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                vm.ListEtapa = UtilsFactory.AgregarItemSelectTodos(resultado.data);

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarProbabilidades() {
            var promise = SalesForceConsolidadoCabeceraService.ListarProbabilidades();

            promise.then(function (resultado) {
                blockUI.stop();

                vm.ListProbabilidades = resultado.data;


            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarMadurez() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 136

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                vm.ListMadurez = UtilsFactory.AgregarItemSelectTodos(resultado.data);

            }, function (response) {
                blockUI.stop();

            });

        }


        function LimpiarModelo() {
            
            vm.ListaAnios = '2018';
            vm.ListaMeses = '';
            vm.IdEtapa = "0";
            vm.IdProbabilidad = -1;
            vm.IdMadurez = "0";


            $("[id$='FReporte']").contents().find('body').html('');

        };

        function ConsultarReporte() {

            var meses = '0';
            if (vm.ListaMeses == '' || vm.ListaMeses == null) {
                meses = '0';
            }
            else {
                meses = vm.ListaMeses;
            }

           var parametros =
                    "reporte=IngresoLineaNegocioPeriodo&Anio=" + vm.ListaAnios + '&Mes=' + meses +
               '&Etapa=' + vm.IdEtapa + '&Probabilidad=' + vm.IdProbabilidad + '&Madurez=' + vm.IdMadurez;

            blockUI.start();
            
            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
            
            $timeout(function () {
                blockUI.stop();
            },2000);

        };
    }
})();

