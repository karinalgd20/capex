﻿(function () {
    'use strict',
     angular
    .module('app.Funnel')
    .service('RptPreventaOportunidadesGerenteService', RptPreventaOportunidadesGerenteService);

    RptPreventaOportunidadesGerenteService.$inject = ['$http', 'URLS'];

    function RptPreventaOportunidadesGerenteService($http, $urls) {

        var service = {
            //SubServicios
            ListarOportunidades: ListarOportunidades
        };

        return service;
        function ListarOportunidades(subServicio) {

            return $http({
                url: $urls.ApiFunnel + "RptPreventaOportunidadesGerente/ListarOportunidades",
                method: "POST",
                data: JSON.stringify(subServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();

