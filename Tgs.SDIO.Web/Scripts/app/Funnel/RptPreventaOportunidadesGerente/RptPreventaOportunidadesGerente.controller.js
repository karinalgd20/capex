﻿(function () {
    'use strict'

    angular
    .module('app.Funnel')
    .controller('RptPreventaOportunidadesGerenteController', RptPreventaOportunidadesGerenteController);

    RptPreventaOportunidadesGerenteController.$inject = ['RptPreventaOportunidadesGerenteService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];


    function RptPreventaOportunidadesGerenteController(RptPreventaOportunidadesGerenteService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.pageNumber = 0;
        vm.tamanioPagina = 0;

        vm.ListaLiderIncluir = [];
        vm.ListaLiderExcluir = [];

        vm.ListaPreventaIncluir = [];
        vm.ListaPreventaExcluir = [];

        vm.ListaClientesIncluir = [];
        vm.ListaClientesExcluir = [];

        //vm.ListaOportunidadesIncluir = [];
        //vm.ListaOportunidadesExcluir = [];

        vm.BuscarOportunidades = BuscarOportunidades;
        vm.SeleccionarLider = SeleccionarLider;
        vm.SeleccionarPreventa = SeleccionarPreventa;
        vm.SeleccionarCliente = SeleccionarCliente;
        //vm.SeleccionarOportunidad = SeleccionarOportunidad;

        vm.dtColumns = [
         //DTColumnBuilder.newColumn('Orden').withTitle('Orden').notSortable(),
         //DTColumnBuilder.newColumn('OrdenMostrar').withTitle('OrdenMostrar').notSortable(),
         //DTColumnBuilder.newColumn('IdLider').withTitle('IdLider').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaLider).withClass('widthSelectorGrilla'),
         DTColumnBuilder.newColumn('Lider').withTitle('LIDER').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaPreventa).withClass('widthSelectorGrilla'),
         DTColumnBuilder.newColumn('EquipoPreventaLiderCadena').withTitle('EQUIPO PREVENTA').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaClientes).withClass('widthSelectorGrilla'),
         DTColumnBuilder.newColumn('CarteraClientesLiderCadena').withTitle('CARTERA CLIENTES').notSortable(),
         //DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesBusquedaOportunidades).withOption('width', '2%'),
         DTColumnBuilder.newColumn('NroOportunidadesLiderCadena').withTitle('OPORTUNIDADES').notSortable(),
         DTColumnBuilder.newColumn('WinRateLiderCadena').withTitle('WIN RATE').notSortable(),
         DTColumnBuilder.newColumn('TiempoEntregaPromedioLiderCadena').withTitle('TIEMPO ENTREGA OFERTA PROMEDIO').notSortable(),
         DTColumnBuilder.newColumn('PorcentajeCumplimientoLiderCadena').withTitle('% CUMPLIMIENTO DE ENTREGA DE OFERTAS').notSortable(),
         DTColumnBuilder.newColumn('WinRateClienteCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('TiempoEntregaPromedioClienteCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('PorcentajeCumplimientoClienteCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('FechaCierreEstimadaCadena').withTitle('').notSortable(),
         DTColumnBuilder.newColumn('Payback').withTitle('').notSortable().withOption('width', '5%')
         //DTColumnBuilder.newColumn('PagoUnicoDolaresCadena').withTitle('').notSortable().withOption('width', '5%'),
         //DTColumnBuilder.newColumn('RecurrenteDolaresCadena').withTitle('').notSortable().withOption('width', '5%'),
         //DTColumnBuilder.newColumn('PeriodoMesesCadena').withTitle('').notSortable().withOption('width', '5%'),
         //DTColumnBuilder.newColumn('TipoCambioCadena').withTitle('').notSortable().withOption('width', '5%')
        ];
        

        function AccionesBusquedaLider(data, type, full, meta) {
            if (data.Nivel == '1')
            {               
               return "<div class='col-xs-6 col-sm-6'><a title='" + (data.Desplegado == '1' ? 'Ocultar Detalle Lider' : 'Mostrar Detalle Lider') +"' class='btn btn-sm' onclick='SeleccionarLider(2," + data.IdLider + ","+ data.Desplegado +");'  id='tab-buttonNivel1' >" + "<span class='"+ (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') +"'></span>" + "</a></div> " ;
            }
            else
            {
                return "";
            }
        }

        function AccionesBusquedaPreventa(data, type, full, meta)
        {
            if (data.Nivel == '2' && data.EquipoPreventaLiderCadena != "PREVENTA")
            {
                return "<div class='col-xs-6 col-sm-6'><a title='"+ (data.Desplegado == '1' ? 'Ocultar Detalle Preventa' : 'Mostrar Detalle Preventa') +"' class='btn btn-sm' onclick='SeleccionarPreventa(3," + data.IdLider + "," + data.IdPreventa + "," + data.Desplegado + ");'  id='tab-buttonNivel2' >" + "<span class='" + (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg') +"'></span>" + "</a></div> " ;
            }
            else
            {
                return "";
            }
        }


        function AccionesBusquedaClientes(data, type, full, meta) {

            if (data.Nivel == '3' && data.CarteraClientesLiderCadena != "CLIENTE") {

                  return "<div class='col-xs-6 col-sm-6'><a title='" + (data.Desplegado == '1' ? 'Ocultar Detalle Cliente' : 'Mostrar Detalle Cliente') + "' class='btn btn-sm' onclick='SeleccionarCliente(4," + data.IdLider + "," + data.IdPreventa + "," + data.IdCliente + "," + data.Desplegado + ");'  id='tab-buttonNivel3' >" + "<span class='" + (data.Desplegado == '1' ? 'fa fa-caret-down fa-lg' : 'fa fa-caret-right fa-lg' ) + "'></span>" + "</a></div> " ;
            }
            else {
                return "";
            }
        }

        //function AccionesBusquedaOportunidades(data, type, full, meta)
        //{
        //    if (data.Nivel == '4' && data.NroOportunidadesLiderCadena != "OPORTUNIDAD") {

        //        if (data.IdOportunidad != undefined)
        //        {
        //            var oportunidad = data.IdOportunidad.substring(4);
        //            var longitud = oportunidad.length;

        //            return data.Desplegado == '1' ?
        //              "<div class='col-xs-6 col-sm-6'><a class='btn btn-sm' onclick='SeleccionarOportunidad(5," + data.IdLider + "," + data.IdPreventa + "," + data.IdCliente + "," + oportunidad + "," + data.Desplegado + "," + longitud + ");'  id='tab-buttonNivel4' >" + "<span class='fa fa-caret-down fa-lg'></span>" + "</a></div> " :
        //              "<div class='col-xs-6 col-sm-6'><a class='btn btn-sm' onclick='SeleccionarOportunidad(5," + data.IdLider + "," + data.IdPreventa + "," + data.IdCliente + "," + oportunidad + "," + data.Desplegado + "," + longitud + ");'  id='tab-buttonNivel4' >" + "<span class='fa fa-caret-right fa-lg'></span>" + "</a></div> ";
        //        }
        //        else
        //        {
        //            return "";
        //        }

        //    }
        //    else {
        //        return "";
        //    }
        //}

        function SeleccionarComun()
        {
            //blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('stateSave', true)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarOportunidadesPaginadoNivel)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {

                    if (data.EquipoPreventaLiderCadena == 'PREVENTA' || data.CarteraClientesLiderCadena == 'CLIENTE' || data.NroOportunidadesLiderCadena == 'OPORTUNIDAD' || data.WinRateLiderCadena == "COD OFERTA") {
                        $('td', row).addClass('header_pagining');
                        $compile(angular.element(row).contents())($scope);
                    }

                })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }


        function SeleccionarLider(nivel, _idLider, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idLider: _idLider,
                    FechaHora: tiempo
                };

            if (desplegado == '0')
            {
                vm.ListaLiderIncluir.push(objeto);
            }
            else
            {
                if (desplegado == '1')
                {
                    vm.ListaLiderExcluir.push(objeto);
                } 
            }

            SeleccionarComun();
         
        }

        function SeleccionarPreventa(nivel, _idLider, _idPreventa, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idLider: _idLider,
                    idPreventa: _idPreventa,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaPreventaIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaPreventaExcluir.push(objeto);
                }
            }

            SeleccionarComun();

        }

        function SeleccionarCliente(nivel, _idLider, _idPreventa, _idCliente, desplegado) {

            var fecha = new Date();
            var tiempo = fecha.getTime();

            var objeto =
                {
                    Level: nivel,
                    idLider: _idLider,
                    idPreventa: _idPreventa,
                    IdCliente: _idCliente,
                    FechaHora: tiempo
                };

            if (desplegado == '0') {
                vm.ListaClientesIncluir.push(objeto);
            }
            else {
                if (desplegado == '1') {
                    vm.ListaClientesExcluir.push(objeto);
                }
            }

            SeleccionarComun();

        }

        //function SeleccionarOportunidad(nivel, _idLider, _idPreventa, _idCliente, _oportunidad ,desplegado, longitud)
        //{
        //    var fecha = new Date();
        //    var tiempo = fecha.getTime();

        //    var perCompleto = '0'.repeat(longitud - _oportunidad.toString().length) + _oportunidad.toString();

        //    var objeto =
        //        {
        //            Level: nivel,
        //            idLider: _idLider,
        //            idPreventa: _idPreventa,
        //            IdCliente: _idCliente,
        //            IdOportunidad: 'PER-' + perCompleto,
        //            FechaHora: tiempo
        //        };

        //    if (desplegado == '0') {
        //        vm.ListaOportunidadesIncluir.push(objeto);
        //    }
        //    else {
        //        if (desplegado == '1') {
        //            vm.ListaOportunidadesExcluir.push(objeto);
        //        }
        //    }

        //    SeleccionarComun();

        //}

        function BuscarOportunidades() {
            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('stateSave', true)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarOportunidadesPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }

        function BuscarOportuniadesComun(sSource, aoData, fnCallback, oSettings, request)
        {
            var fechaActual = new Date();
            var anioActual = fechaActual.getFullYear()

            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            request.oportunidadesFiltro.Anio = anioActual;
            request.oportunidadesFiltro.IdJefe = idUsuario;

            if (pageNumber != vm.pageNumber || length != vm.tamanioPagina) {
                
                vm.ListaLiderIncluir = [];
                vm.ListaLiderExcluir = [];
                vm.ListaPreventaIncluir = [];
                vm.ListaPreventaExcluir = [];
                vm.ListaClientesIncluir = [];
                vm.ListaClientesExcluir = [];

                request.oportunidadesFiltro.Nivel = 1;
                request.oportunidadesFiltro.ListaLiderIncluir = JSON.stringify(vm.ListaLiderIncluir);
                request.oportunidadesFiltro.ListaLiderExcluir = JSON.stringify(vm.ListaLiderExcluir);
                request.oportunidadesFiltro.ListaPreventaIncluir = JSON.stringify(vm.ListaPreventaIncluir);
                request.oportunidadesFiltro.ListaPreventaExcluir = JSON.stringify(vm.ListaPreventaExcluir);
                request.oportunidadesFiltro.ListaClientesIncluir = JSON.stringify(vm.ListaClientesIncluir);
                request.oportunidadesFiltro.ListaClientesExcluir = JSON.stringify(vm.ListaClientesExcluir);
            }

            vm.pageNumber = pageNumber;
            vm.tamanioPagina = length;


            request.oportunidadesFiltro.Paginacion =
                {
                    Indice: pageNumber,
                    Tamanio: length
                };

            var promise = RptPreventaOportunidadesGerenteService.ListarOportunidades(request);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.Total,
                    'recordsFiltered': resultado.data.Total,
                    'data': resultado.data.ListaOportunidades
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
            });
        }


        function BuscarOportunidadesPaginado(sSource, aoData, fnCallback, oSettings) {

             var request = {
                oportunidadesFiltro :
                {
                    Nivel: 1
                }
            };

             BuscarOportuniadesComun(sSource, aoData, fnCallback, oSettings, request);
            
        }
        
        function BuscarOportunidadesPaginadoNivel(sSource, aoData, fnCallback, oSettings) {

            var request = {
                oportunidadesFiltro:
                {
                    Nivel: 0,
                    ListaLiderIncluir: JSON.stringify(vm.ListaLiderIncluir),
                    ListaLiderExcluir: JSON.stringify(vm.ListaLiderExcluir),
                    ListaPreventaIncluir: JSON.stringify(vm.ListaPreventaIncluir),
                    ListaPreventaExcluir: JSON.stringify(vm.ListaPreventaExcluir),
                    ListaClientesIncluir: JSON.stringify(vm.ListaClientesIncluir),
                    ListaClientesExcluir: JSON.stringify(vm.ListaClientesExcluir)
                    //ListaOportunidadesIncluir: JSON.stringify(vm.ListaOportunidadesIncluir),
                    //ListaOportunidadesExcluir: JSON.stringify(vm.ListaOportunidadesExcluir),
                }
            };

            BuscarOportuniadesComun(sSource, aoData, fnCallback, oSettings, request);
        }


        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            //.withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        
    }
})();



