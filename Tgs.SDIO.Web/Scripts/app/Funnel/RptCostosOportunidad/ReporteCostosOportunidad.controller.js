﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('ReporteCostosOportunidadController', ReporteCostosOportunidadController);

    ReporteCostosOportunidadController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS'];

    function ReporteCostosOportunidadController(blockUI, $timeout, UtilsFactory, $urls) {
        var vm = this;

        angular.element(document).ready(function () {

            if (IdOportunidad !== '' || IdOportunidad !== undefined ||
                NumeroCaso !== '' || NumeroCaso !== undefined) {

                var parametros = "reporte=CostosOportunidad&IdOportunidad=" + IdOportunidad +
                    "&NumeroCaso=" + NumeroCaso;

                blockUI.start();

                $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);

                $timeout(function () {
                    blockUI.stop();
                }, 2000);

            }
        });
    }
})();

