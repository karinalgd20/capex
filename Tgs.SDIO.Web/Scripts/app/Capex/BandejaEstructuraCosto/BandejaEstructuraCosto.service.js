﻿(function () {
    'use strict',
     angular
    .module('app.Capex')
    .service('BandejaEstructuraCostoService', BandejaEstructuraCostoService);

    BandejaEstructuraCostoService.$inject = ['$http', 'URLS'];

    function BandejaEstructuraCostoService($http, $urls) {

        var service = {
            //EstructuraCostos
            ListarEstructuraCostos: ListarEstructuraCostos,
            ObtenerEstructuraCosto: ObtenerEstructuraCosto,
            RegistrarEstructuraCosto: RegistrarEstructuraCosto,
            ActualizarEstructuraCosto: ActualizarEstructuraCosto,
            ListaEstructuraCostoGrupoPaginado: ListaEstructuraCostoGrupoPaginado,
            InhabilitarEstructuraCosto: InhabilitarEstructuraCosto,
            ObtenerCapexLineaNegocioPorIdSolicitud: ObtenerCapexLineaNegocioPorIdSolicitud
        };

        return service;

        function ObtenerCapexLineaNegocioPorIdSolicitud(estructuraCosto) {

            return $http({
                url: $urls.ApiCapex + "BandejaEstructuraCosto/ObtenerCapexLineaNegocioPorIdSolicitud",
                method: "POST",
                data: JSON.stringify(estructuraCosto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function InhabilitarEstructuraCosto(estructuraCosto) {

            return $http({
                url: $urls.ApiCapex + "BandejaEstructuraCosto/InhabilitarEstructuraCosto",
                method: "POST",
                data: JSON.stringify(estructuraCosto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        //EstructuraCostos
        function ListarEstructuraCostos(estructuraCosto) {

            return $http({
                url: $urls.ApiCapex + "BandejaEstructuraCosto/ListarEstructuraCostos",
                method: "POST",
                data: JSON.stringify(estructuraCosto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ObtenerEstructuraCosto(estructuraCosto) {

            return $http({
                url: $urls.ApiCapex + "BandejaEstructuraCosto/ObtenerEstructuraCosto",
                method: "POST",
                data: JSON.stringify(estructuraCosto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function RegistrarEstructuraCosto(estructuraCosto) {

            return $http({
                url: $urls.ApiCapex + "BandejaEstructuraCosto/RegistrarEstructuraCosto",
                method: "POST",
                data: JSON.stringify(estructuraCosto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ActualizarEstructuraCosto(estructuraCosto) {

            return $http({
                url: $urls.ApiCapex + "BandejaEstructuraCosto/ActualizarEstructuraCosto",
                method: "POST",
                data: JSON.stringify(estructuraCosto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListaEstructuraCostoGrupoPaginado(estructuraCosto) {

            return $http({
                url: $urls.ApiCapex + "BandejaEstructuraCosto/ListaEstructuraCostoGrupoPaginado",
                method: "POST",
                data: JSON.stringify(estructuraCosto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }

})();