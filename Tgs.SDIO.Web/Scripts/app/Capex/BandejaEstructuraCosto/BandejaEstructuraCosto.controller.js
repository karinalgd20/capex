﻿(function () {
    'use strict'

    angular
    .module('app.Comun')
    .controller('BandejaEstructuraCosto', BandejaEstructuraCosto);

    BandejaEstructuraCosto.$inject = ['BandejaEstructuraCostoService', 'RegistrarEstructuraCostoService', 'MaestraService', 'CapexContenedorService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function BandejaEstructuraCosto(BandejaEstructuraCostoService, RegistrarEstructuraCostoService, MaestraService,CapexContenedorService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.IdEstructuraCostoGrupo = 0;
        vm.IdSolicitudCapex = 0;
        vm.IdCapexLineaNegocio = 0;
   



        ObtenerIdSolicitudCapexSession();
        function ObtenerIdSolicitudCapexSession() {

          
            var promise = CapexContenedorService.ObtenerIdSolicitudCapexSession();

            promise.then(function (resultado) {
                vm.IdSolicitudCapex =     resultado.data;

                if (vm.IdSolicitudCapex > 0) {
                  

                    vm.IdCapexLineaNegocio = 1;
                    BuscarEstructuraCostoSISEGO();
                    BuscarEstructuraCostoServicio();
                    BuscarEstructuraCostoConcepto();
                    BuscarEstructuraCostoEquipo();
                 
                }
                
            }, function (response) {
                blockUI.stop();
            });
        }


        /* Modal Registar */

    
        function ObtenerCapexLineaNegocioPorIdSolicitud() {

            var solicitudLinea = {
                IdSolicitudCapex: vm.IdSolicitudCapex,

            };
            var promise = BandejaEstructuraCostoService.ObtenerCapexLineaNegocioPorIdSolicitud(solicitudLinea);

            promise.then(function (resultado) {


                var Respuesta = resultado.data;
                vm.IdCapexLineaNegocio = Respuesta.IdCapexLineaNegocio;

           

            }, function (response) {
                blockUI.stop();
            });
        }

        /* Modal Registar */
        vm.ModalEstructuraCosto = ModalEstructuraCosto;

        function ModalEstructuraCosto(IdEstructuraCostoGrupo) {
            vm.IdEstructuraCostoGrupo = IdEstructuraCostoGrupo;
            if (IdEstructuraCostoGrupo == undefined) {
                vm.IdEstructuraCostoGrupo = 0;
            }


            debugger
            var EstructuraCosto = {
                IdEstructuraCostoGrupo: vm.IdEstructuraCostoGrupo
            };

            var promise = RegistrarEstructuraCostoService.ModalEstructuraCosto(EstructuraCosto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoEstructuraCosto").html(content);
                });

                $('#ModalRegistrarEstructuraCosto').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };


        /*--------------------------------------*/


        vm.BuscarEstructuraCostoSISEGO = BuscarEstructuraCostoSISEGO;
        vm.LimpiarGrillaSISEGO = LimpiarGrillaSISEGO;
        

        vm.BuscarEstructuraCostoServicio = BuscarEstructuraCostoServicio;
        vm.LimpiarGrillaSISEGO = LimpiarGrillaServicio;
       
       
        vm.BuscarEstructuraCostoConcepto = BuscarEstructuraCostoConcepto;
        vm.LimpiarGrillaConcepto = LimpiarGrillaConcepto;
        

        vm.BuscarEstructuraCostoEquipo = BuscarEstructuraCostoEquipo;
        vm.LimpiarGrillaEquipo = LimpiarGrillaEquipo;
       


        vm.ListTipoEstructuraCosto = [];


        LimpiarGrillaSISEGO();
        LimpiarGrillaServicio();
        LimpiarGrillaConcepto();
        LimpiarGrillaEquipo();

        vm.DescripcionNuevo = "";
        vm.CostoInstalacion = "";
        vm.IdtipoEstructuraCosto = "-1";
        vm.IdDepreciacion = "-1";
        vm.IdtipoEstructuraCosto = "-1";


        vm.Descripcion = '';

        vm.MensajeDeAlerta = "";


        vm.dtInstanceSISEGO = {};
        vm.dtInstanceServicio = {};
        vm.dtInstanceConcepto = {};
        vm.dtInstanceEquipo = {};

        vm.dtColumnsSISEGO = [
        // DTColumnBuilder.newColumn('Sede').withTitle('Sede').notSortable(),
         DTColumnBuilder.newColumn('CuNuevoDolar').withTitle('CU Dolar').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('CAPEX USD').notSortable(),
         DTColumnBuilder.newColumn('CapexTotalSoles').withTitle('CAPEX S/').notSortable(),
         DTColumnBuilder.newColumn('Asignacion').withTitle('Asignacion').notSortable(),
         DTColumnBuilder.newColumn('Certificacion').withTitle('Certificacion').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaEstructuraCosto)
        ];
        function AccionesBusquedaEstructuraCosto(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn ' data-toggle='modal' data-target='#ModalRegistrarEstructuraCosto'  ng-click='vm.ModalEstructuraCosto(" + data.IdEstructuraCostoGrupo+ ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> "
               + "<div class='col-xs-4 col-sm-1'><a title='Inactivar' class='btn btn-sm'  id='tab-button' class='btn '  onclick='EliminarEstructuraCosto(" + data.IdEstructuraCostoGrupo+ ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
        }

        vm.dtColumnsServicio = [
         DTColumnBuilder.newColumn('Sede').withTitle('Sede').notSortable(),
         //DTColumnBuilder.newColumn('Tipo').withTitle('Tipo').notSortable(),
         DTColumnBuilder.newColumn('Servicio').withTitle('Servicio').notSortable(),
         DTColumnBuilder.newColumn('Modelo').withTitle('Modelo').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CuNuevoDolar').withTitle('CU Dolar').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('CAPEX USD').notSortable(),
         DTColumnBuilder.newColumn('CapexTotalSoles').withTitle('CAPEX S/').notSortable(),
         DTColumnBuilder.newColumn('Asignacion').withTitle('Asignacion').notSortable(),
         DTColumnBuilder.newColumn('Certificacion').withTitle('Certificacion').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaEstructuraCosto)
        ];
        function AccionesBusquedaEstructuraCosto(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn ' data-toggle='modal' data-target='#ModalRegistrarEstructuraCosto'  ng-click='vm.ModalEstructuraCosto(" + data.IdEstructuraCostoGrupo+ ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> "
             + "<div class='col-xs-4 col-sm-1'><a title='Inactivar' class='btn btn-sm'  id='tab-button' class='btn '  onclick='EliminarEstructuraCosto(" + data.IdEstructuraCostoGrupo+ ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
        }
        vm.dtColumnsConcepto = [
        DTColumnBuilder.newColumn('Sede').withTitle('Sede').notSortable(),
         //DTColumnBuilder.newColumn('Tipo').withTitle('Tipo').notSortable(),
         DTColumnBuilder.newColumn('Concepto').withTitle('Concepto').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CuNuevoDolar').withTitle('CU Dolar').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('CAPEX USD').notSortable(),
         DTColumnBuilder.newColumn('CapexTotalSoles').withTitle('CAPEX S/').notSortable(),
         DTColumnBuilder.newColumn('Asignacion').withTitle('Asignacion').notSortable(),
         DTColumnBuilder.newColumn('Certificacion').withTitle('Certificacion').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaEstructuraCosto)
        ];
        function AccionesBusquedaEstructuraCosto(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn ' data-toggle='modal' data-target='#ModalRegistrarEstructuraCosto'  ng-click='vm.ModalEstructuraCosto(" + data.IdEstructuraCostoGrupo+ ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> "
             + "<div class='col-xs-4 col-sm-1'><a title='Inactivar' class='btn btn-sm'  id='tab-button' class='btn '  onclick='EliminarEstructuraCosto(" + data.IdEstructuraCostoGrupo+ ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
        }

        vm.dtColumnsEquipo = [
         DTColumnBuilder.newColumn('Sede').withTitle('Sede').notSortable(),
         //DTColumnBuilder.newColumn('Tipo').withTitle('Tipo').notSortable(),
         //DTColumnBuilder.newColumn('Modelo').withTitle('Modelo').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CuNuevoDolar').withTitle('CU Dolar').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('CAPEX USD').notSortable(),
         DTColumnBuilder.newColumn('CapexTotalSoles').withTitle('CAPEX S/').notSortable(),
         DTColumnBuilder.newColumn('Asignacion').withTitle('Asignacion').notSortable(),
         DTColumnBuilder.newColumn('Certificacion').withTitle('Certificacion').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaEstructuraCosto)
        ];
        function AccionesBusquedaEstructuraCosto(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn ' data-toggle='modal' data-target='#ModalRegistrarEstructuraCosto'  ng-click='vm.ModalEstructuraCosto(" + data.IdEstructuraCostoGrupo+ ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> "
             + "<div class='col-xs-4 col-sm-1'><a title='Inactivar' class='btn btn-sm'  id='tab-button' class='btn '  onclick='EliminarEstructuraCosto(" + data.IdEstructuraCostoGrupo+ ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
        }


        function BuscarEstructuraCostoSISEGO() {
            blockUI.start();
            LimpiarGrillaSISEGO();

            $timeout(function () {
                vm.dtOptionsSISEGO = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarEstructuraCostoPaginadoSISEGO)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }
        function BuscarEstructuraCostoPaginadoSISEGO(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var EstructuraCosto = {
                IdCapexLineaNegocio: vm.IdCapexLineaNegocio,

                IdGrupo: 1,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = BandejaEstructuraCostoService.ListaEstructuraCostoGrupoPaginado(EstructuraCosto);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListEstructuraCostoGrupoDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaSISEGO();
            });
        }

        function BuscarEstructuraCostoServicio() {
            blockUI.start();
            LimpiarGrillaServicio();

            $timeout(function () {
                vm.dtOptionsServicio = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarEstructuraCostoPaginadoServicio)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }
        function BuscarEstructuraCostoPaginadoServicio(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var EstructuraCosto = {
                IdCapexLineaNegocio: vm.IdCapexLineaNegocio,

                IdGrupo: 2,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = BandejaEstructuraCostoService.ListaEstructuraCostoGrupoPaginado(EstructuraCosto);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListEstructuraCostoGrupoDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaServicio();
            });
        }

        function BuscarEstructuraCostoConcepto() {
            blockUI.start();
            LimpiarGrillaConcepto();

            $timeout(function () {
                vm.dtOptionsConcepto = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarEstructuraCostoPaginadoConcepto)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }
        function BuscarEstructuraCostoPaginadoConcepto(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var EstructuraCosto = {
                IdCapexLineaNegocio: vm.IdCapexLineaNegocio,

                IdGrupo: 3,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = BandejaEstructuraCostoService.ListaEstructuraCostoGrupoPaginado(EstructuraCosto);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListEstructuraCostoGrupoDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaConcepto();
            });
        }

        function BuscarEstructuraCostoEquipo() {
            blockUI.start();
            LimpiarGrillaEquipo();

            $timeout(function () {
                vm.dtOptionsEquipo = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarEstructuraCostoPaginadoEquipo)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }
        function BuscarEstructuraCostoPaginadoEquipo(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var EstructuraCosto = {
                IdCapexLineaNegocio: vm.IdCapexLineaNegocio,

                IdGrupo: 4,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = BandejaEstructuraCostoService.ListaEstructuraCostoGrupoPaginado(EstructuraCosto);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListEstructuraCostoGrupoDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaEquipo();
            });
        }

        function LimpiarGrillaSISEGO() {
            vm.dtOptionsSISEGO = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }

        
        function LimpiarGrillaServicio() {
            vm.dtOptionsServicio = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        function LimpiarGrillaConcepto() {
            vm.dtOptionsConcepto = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        function LimpiarGrillaEquipo() {
            vm.dtOptionsEquipo = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }



        function LimpiarFiltros() {
            vm.Descripcion = '';
            vm.IdtipoEstructuraCosto = "-1";

        }


    }
})();








