﻿(function () {
    'use strict',
     angular
    .module('app.Capex')
    .service('ProyectoService', ProyectoService);

    ProyectoService.$inject = ['$http', 'URLS'];

    function ProyectoService($http, $urls) {

        var service = {
            VistaSolicitud: VistaSolicitud,
            ConsultaProyecto: ConsultaProyecto,
            VistaEstructuraCosto: VistaEstructuraCosto,
            VistaDocumentoTopologia: VistaDocumentoTopologia
        };

        return service;
        function VistaDocumentoTopologia() {
            return $http({
                url: $urls.ApiCapex + "RegistrarTopologia/Index",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function VistaEstructuraCosto() {
            return $http({
                url: $urls.ApiCapex + "BandejaEstructuraCosto/Index",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function VistaSolicitud() {
            return $http({
                url: $urls.ApiCapex + "RegistrarSolicitud/Index",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ConsultaProyecto(proyecto) {
            return $http({
                url: $urls.ApiCapex + "Proyecto/ListarProyectos",
                method: "POST",
                data: JSON.stringify(proyecto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }

})();