﻿(function () {
    'use strict'

    angular
    .module('app.Capex')
    .controller('ProyectoController', ProyectoController);

    ProyectoController.$inject = ['ProyectoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function ProyectoController(ProyectoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.correo = {};
        vm.CargaSolicitud = CargaSolicitud;
        vm.CargarEstructuraCosto = CargarEstructuraCosto;
        vm.CargarDocumentoTopologia = CargarDocumentoTopologia;
        
        /******************************************* Tabla *******************************************/


        /******************************************* Metodos *******************************************/
        function CargaSolicitud() {

            var promise = ProyectoService.VistaSolicitud();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#datoSolicitud").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };

        function CargarEstructuraCosto() {

            var promise = ProyectoService.VistaEstructuraCosto();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#EstructuraCosto").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };

        function CargarDocumentoTopologia() {

            var promise = ProyectoService.VistaDocumentoTopologia();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#DocumentoTopologia").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };
        /******************************************* Load *******************************************/

    }

})();