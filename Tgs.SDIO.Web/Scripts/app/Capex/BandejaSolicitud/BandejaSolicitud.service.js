﻿(function () {
    'use strict',
     angular
    .module('app.Capex')
    .service('BandejaSolicitudService', BandejaSolicitudService);

    BandejaSolicitudService.$inject = ['$http', 'URLS'];

    function BandejaSolicitudService($http, $urls) {

        var service = {
            ListaSolicitudCapexPaginado: ListaSolicitudCapexPaginado,
            DetalleSolicitudCapex: DetalleSolicitudCapex
        };

        return service;
        function DetalleSolicitudCapex(request) {
            return $http({
                url: $urls.ApiCapex + "BandejaSolicitud/DetalleSolicitudCapex",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListaSolicitudCapexPaginado(request) {
            return $http({
                url: $urls.ApiCapex + "BandejaSolicitud/ListaSolicitudCapexPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }

})();