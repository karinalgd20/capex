﻿(function () {
    'use strict'

    angular
    .module('app.Capex')
    .controller('BandejaSolicitudController', BandejaSolicitudController);

    BandejaSolicitudController.$inject = ['BandejaSolicitudService', 'LineaNegocioService', 'MaestraService','ClienteService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function BandejaSolicitudController(BandejaSolicitudService,LineaNegocioService,MaestraService,ClienteService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.ListSolicitudCapexDtoResponse = [];
        vm.DetalleSolicitudCapex = DetalleSolicitudCapex;
        vm.VerDetalle=true;
        vm.BuscarSolicitud = BuscarSolicitud;
        vm.SelectCliente = SelectCliente;
        vm.fillTextbox = fillTextbox;
        vm.Nuevo = $urls.ApiCapex + "CapexContenedor/Index/";
        vm.Tamanio = 10;
        vm.Indice = 1;
        var total = 0;
        var totalPage = 0;



         vm.FechaCreacion = "";
        vm.FechaEdicion = "";
        vm.IdLineaNegocio = "-1";

        vm.IdEstadoCapex = "-1";
        vm.NomCompletUsuCrea = "";

        vm.numSalesForce = "";
        vm.setPage = setPage;
        vm.pageChanged = pageChanged;
        vm.setItemsPerPage = setItemsPerPage;

        vm.ListLineaNegocio = [];
        vm.ListMaestra = [];
        vm.ListCliente = [];
        //acciones

        listarEstadoCapex();

        listarLineaNegocio();


        $scope.DoCtrlPagingAct = function (text, page, pageSize, total) {

            // vm.Tamanio = pageSize;
            vm.Tamanio = 10;
            vm.Indice = page;

            BuscarSolicitud();
        };

        function setPage(pageNo) {
            vm.currentPage = pageNo;
        }
        function pageChanged() {
            //console.log('Page changed to: ' + vm.currentPage);
        }
        function setItemsPerPage(num) {
            vm.itemsPerPage = num;
            vm.currentPage = parseInt(vm.totalItems / 10); //reset to first page
        }
        BuscarSolicitud();
        /******************************************* Tabla *******************************************/
        function BuscarSolicitud() {
            var IdEstado = "";
            IdEstado = vm.IdEstadoCapex;
            var IdLineaNegocio = "";
            IdLineaNegocio = vm.IdLineaNegocio;
            if (vm.IdEstadoCapex == "-1") {
                IdEstado = null;
            }
            if (vm.IdLineaNegocio == "-1") {
                IdLineaNegocio = 5;
            }
            //if (vm.IdUsuario == "-1") {
            //    vm.IdUsuario = 0;
            //}
            if (vm.Descripcion == "") {
                vm.IdCliente = null;
            }
            if (vm.NumeroSalesForce == "") {
                vm.NumeroSalesForce = null;
            }
            if (vm.FechaCreacion == "") {
                vm.FechaCreacion = null;
            }
            if (vm.FechaEdicion == "") {
                vm.FechaEdicion = null;
            }
            var capex = {
                IdLineaNegocio: IdLineaNegocio,
                IdCliente: vm.IdCliente,
                FechaCreacion: vm.FechaCreacion,
                FechaEdicion: vm.FechaEdicion,
                NumeroSalesForce: vm.NumeroSalesForce,
                Tamanio: vm.Tamanio,
                Indice: vm.Indice,
                IdEstado: IdEstado
            };
            var promise = BandejaSolicitudService.ListaSolicitudCapexPaginado(capex);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListSolicitudCapexDtoResponse = Respuesta.ListSolicitudCapexDtoResponse;

                var totalPage = Respuesta.TotalItemCount % 10;
                var total = Math.trunc(Respuesta.TotalItemCount / 10);

                if (total > 0) {
                    $scope.total = total;
                    if (totalPage > 0) {
                        $scope.total = $scope.total + 1;
                    }
                }
                else {
                    $scope.total = 1;
                }

            }, function (response) {


            });
        }

        /******************************************* Metodos *******************************************/

        function SelectCliente() {
            if (vm.Descripcion.length > 5) {
                ListarCliente();
            }


        }
        function ListarCliente() {

            var cliente = {
                IdEstado: 1,
                Descripcion: vm.Descripcion,

            };
            var promise = ClienteService.ListarClientePorDescripcion(cliente);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;



                var output = [];
                angular.forEach(Respuesta, function (cliente) {

                    output.push(cliente);

                });
                vm.ListCliente = output;


            }, function (response) {
                blockUI.stop();

            });
        }
        function fillTextbox(string, Id) {

            vm.Descripcion = string;
            vm.IdCliente = Id;
            vm.ListCliente = null;

        }
        function ListarMaestraPorValor(Id) {
            var maestra = {
                IdEstado: 1,
                Valor: Id,
                IdRelacion: 1

            };
            var promise = MaestraService.ListarMaestraPorValor(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.DescripcionEstado = Respuesta[0].Descripcion;
            }, function (response) {
                blockUI.stop();

            });
        }

        function listarEstadoCapex() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 565

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListMaestra = UtilsFactory.AgregarItemSelect(Respuesta);
              
            }, function (response) {
                blockUI.stop();
            });
        }


        function listarLineaNegocio() {

            var lineaNegocio = {
                IdEstado: 1,
              

            };
            var promise = LineaNegocioService.ListarLineaNegocios(lineaNegocio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);
                vm.IdLineaNegocio = "-1";
            }, function (response) {
                blockUI.stop();
            });
        }
        function DetalleSolicitudCapex(IdSolicitudCapex) {

            var solicitud = {
                IdSolicitudCapex: IdSolicitudCapex,
                IdEstado: 1


            };
            var promise = BandejaSolicitudService.DetalleSolicitudCapex(solicitud);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.VerDetalle=false;

                vm.NomCompletoPreventa = Respuesta.NombrePreventa;
                vm.NomCompletoJefePreventa = Respuesta.NombreJefePreventa;
                vm.IdSolicitudCapex = IdSolicitudCapex;

            }, function (response) {
                blockUI.stop();
            });
        }
        /******************************************* Load *******************************************/

    }

})();