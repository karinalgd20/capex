﻿(function () {
    'use strict',
     angular
    .module('app.Capex')
    .service('RegistrarSolicitudService', RegistrarSolicitudService);

    RegistrarSolicitudService.$inject = ['$http', 'URLS'];

    function RegistrarSolicitudService($http, $urls) {

        var service = {
            RegistrarSolicitud: RegistrarSolicitud,
            RegistrarSeccion: RegistrarSeccion,
            ListarCapexLineaNegocio: ListarCapexLineaNegocio,
            ListarLineaNegocio: ListarLineaNegocio,
            ActualizarCapexLineaNegocio: ActualizarCapexLineaNegocio,
            ObtenerSeccion: ObtenerSeccion,
            ObtenerSolicitud: ObtenerSolicitud,
            ActualizarSeccion: ActualizarSeccion,
            ActualizarSolicitud: ActualizarSolicitud
        };

        function RegistrarSolicitud(solicitud) {
            return $http({
                url: $urls.ApiCapex + "RegistrarSolicitud/RegistrarSolicitud",
                method: "POST",
                data: JSON.stringify(solicitud)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarSeccion(seccion) {
            return $http({
                url: $urls.ApiCapex + "RegistrarSolicitud/RegistrarSeccion",
                method: "POST",
                data: JSON.stringify(seccion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarCapexLineaNegocio(capex) {
            return $http({
                url: $urls.ApiCapex + "RegistrarSolicitud/ListarCapexLineaNegocio",
                method: "POST",
                data: JSON.stringify(capex)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarCapexLineaNegocio(capex) {
            return $http({
                url: $urls.ApiCapex + "RegistrarSolicitud/ActualizarCapexLineaNegocio",
                method: "POST",
                data: JSON.stringify(capex)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarLineaNegocio(linea) {
            return $http({
                url: $urls.ApiCapex + "RegistrarSolicitud/ListarLineaNegocio",
                method: "POST",
                data: JSON.stringify(linea)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


        function ActualizarSolicitud(solicitud) {
            return $http({
                url: $urls.ApiCapex + "RegistrarSolicitud/ActualizarSolicitud",
                method: "POST",
                data: JSON.stringify(solicitud)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarSeccion(seccion) {
            return $http({
                url: $urls.ApiCapex + "RegistrarSolicitud/ActualizarSeccion",
                method: "POST",
                data: JSON.stringify(seccion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerSolicitud(solicitud) {
            return $http({
                url: $urls.ApiCapex + "RegistrarSolicitud/ObtenerSolicitud",
                method: "POST",
                data: JSON.stringify(solicitud)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


        function ObtenerSeccion(seccion) {
            return $http({
                url: $urls.ApiCapex + "RegistrarSolicitud/ObtenerSeccion",
                method: "POST",
                data: JSON.stringify(seccion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        return service;


    }

})();