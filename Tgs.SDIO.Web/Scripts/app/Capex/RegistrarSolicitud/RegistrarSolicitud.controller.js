﻿(function () {
    'use strict'

    angular
    .module('app.Capex')
    .controller('RegistrarSolicitudController', RegistrarSolicitudController);

    RegistrarSolicitudController.$inject = ['RegistrarSolicitudService', 'ProyectoService','CapexContenedorService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarSolicitudController(RegistrarSolicitudService, ProyectoService,CapexContenedorService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.correo = {};
        vm.proyectos = {};
        vm.CargarProyectos = CargarProyectos;
        vm.IdProyectoSF = '';
        vm.RegistrarSolicitud = RegistrarSolicitud;
        vm.IdCliente = 0;
        vm.GeneralProyecto = '';
        vm.ResumenSolicitud = '';
        vm.ObtendraSolicitud = '';
        vm.SustentoCuantitativo = '';
        vm.SustentoCualitativo = '';
        vm.RegistrarSeccion = RegistrarSeccion;
        vm.IdProyecto = 0;
        vm.Entidad = '';
        vm.CodigoPmo = '';
        vm.ListarLineasNegocio = ListarLineasNegocio;
        vm.ListCapexLinea = [];
        vm.Fc = 0;
        vm.IdCapexLineaNegocio = -1;
        vm.BuscarLineas = BuscarLineas;
        vm.ObtenerProyecto = ObtenerProyecto;
        vm.ObtenerSeccion = ObtenerSeccion;
        vm.IdSolicitudCapex = 0;
        vm.IdSeccion1 = 0;
        vm.IdSeccion2 = 0;
        vm.IdSeccion3 = 0;
        vm.IdSeccion4 = 0;
        vm.IdSeccion5 = 0;
        vm.ObjCapex = [];
        vm.ObjCapex = jsonCapex;
        vm.TipoEntidad = 1;
        vm.IdTipoProyecto = 1;
        vm.EditarIndicador = EditarIndicador;
        vm.ActualizarCapex = ActualizarCapex;


        
        /******************************************* Tabla *******************************************/

        vm.TabIndicadores = true;

        TabPorPerfilCapexDatos();
        function TabPorPerfilCapexDatos() {

            var promise = CapexContenedorService.TabPorPerfilCapexDatos();
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.TabIndicadores = Respuesta.TabIndicadores;


            });


        };


        // CargarProyectos();
        ListaProyectos();
        /******************************************* Metodos *******************************************/


        function ListaProyectos() {

            var promise = ProyectoService.ConsultaProyecto();

            var selector = "IdCodigoSalesForce";
            promise.then(
                function (response) {
                    var Respuesta = response.data;
                    vm.proyectos = Respuesta;

                    //$("#" + selector).select2({
                    //    data: Respuesta,
                    //    placeholder: '--Seleccione--',
                    //    allowClear: true
                    //});
                });
        };

        function CargarProyectos() {

                var selector = "IdCodigoSalesForce";
                let idsValidos = vm.proyectos.filter((datos) => {
                    return datos.IdProyectoSF == vm.IdProyectoSF;
                });

                let idsRespuestas = idsValidos.map((datos) => {
                    vm.Proyecto = datos.Descripcion;
                    vm.Cliente = datos.NombreCliente;
                    vm.IdCliente = datos.IdCliente;
                    vm.IdProyecto = datos.IdProyecto;
                    vm.Entidad = datos.Entidad;
                    vm.CodigoPmo = datos.IdProyectoLKM;
                    vm.TipoEntidad = datos.TipoEntidad;
                    return datos.IdProyectoSF;
                });
                //idsRespuestas = (vm.IdProyectoSF == -1 || vm.IdProyectoSF == null) ? vm.IdProyectoSF : idsRespuestas;
                //$("#" + selector).val(idsRespuestas).change();
        };

        function RegistrarSolicitud() {

            var dto = {
                Descripcion: vm.Proyecto,
                IdCliente: vm.IdCliente,
                NumeroSalesForce: vm.IdProyectoSF,
                IdProyecto: vm.IdProyecto,
                IdTipoEntidad: vm.TipoEntidad,
                IdTipoProyecto: vm.IdTipoProyecto,
                CodigoPmo: vm.CodigoPmo,
                IdSolicitudCapex: vm.IdSolicitudCapex
            };

            var promise = (vm.IdSolicitudCapex == 0) ? RegistrarSolicitudService.RegistrarSolicitud(dto) : RegistrarSolicitudService.ActualizarSolicitud(dto);


            promise.then(
                function (response) {
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#alertSolicitud', 'danger', Respuesta.Mensaje, 20);
                    } else {
                        vm.GeneralProyecto = tinymce.get('GeneralProyecto').getContent();
                        vm.ResumenSolicitud = tinymce.get('ResumenSolicitud').getContent();
                        vm.ObtendraSolicitud = tinymce.get('ObtendraSolicitud').getContent();
                        vm.SustentoCuantitativo = tinymce.get('SustentoCuantitativo').getContent();
                        vm.SustentoCualitativo = tinymce.get('SustentoCualitativo').getContent();
                        if (Respuesta.Id != 0)
                          
                        RegistrarSeccion(Respuesta.Id, vm.GeneralProyecto, 1);
                        RegistrarSeccion(Respuesta.Id, vm.ResumenSolicitud, 2);
                        RegistrarSeccion(Respuesta.Id, vm.ObtendraSolicitud, 3);
                        RegistrarSeccion(Respuesta.Id, vm.SustentoCuantitativo, 4);
                        RegistrarSeccion(Respuesta.Id, vm.SustentoCualitativo, 5);

                        vm.IdSolicitudCapex = Respuesta.Id;

                        CapexContenedorService.RegistroIdSolicitudCapexSession(vm.IdSolicitudCapex)


                        ListarLineasNegocio();
                        CargaIndicadores();
                        UtilsFactory.Alerta('#alertSolicitud', 'success', Respuesta.Mensaje, 10);
                    }

                });

        };

        function RegistrarSeccion(Id, Descripcion, Tipo) {

            switch (Tipo) {
                case 1: vm.IdSeccion = vm.IdSeccion1; break;
                case 2: vm.IdSeccion = vm.IdSeccion2; break;
                case 3: vm.IdSeccion = vm.IdSeccion3; break;
                case 4: vm.IdSeccion = vm.IdSeccion4; break;
                case 5: vm.IdSeccion = vm.IdSeccion5; break;
            }

            var Seccion = {
                IdSolicitudCapex: Id,
                Descripcion: Descripcion,
                IdTipoDescripcion: Tipo,
                IdSeccion: vm.IdSeccion
            };

            var promise = (vm.IdSeccion == 0) ? RegistrarSolicitudService.RegistrarSeccion(Seccion) : RegistrarSolicitudService.ActualizarSeccion(Seccion);

            promise.then(
                function (response) {
                    var Respuesta = response.data;
                    UtilsFactory.Alerta('#alertSolicitud', 'success', Respuesta.Mensaje, 5);
                    
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#alertSolicitud', 'danger', MensajesUI.DatosError, 5);
                });

        };

        function ListarLineasNegocio() {

            var linea = {
                IdSolicitudCapex: vm.IdSolicitudCapex
            };

            var promise = RegistrarSolicitudService.ListarCapexLineaNegocio(linea);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListCapexLinea = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {

                blockUI.stop();

            });
        };

        function ActualizarCapex() {

            var capex = {
                IdSolicitudCapex: vm.IdSolicitudCapex,
                IdCapexLineaNegocio: vm.IdCapexLineaNegocio,
                Fc: vm.Fc,
                Van: vm.Van,
                PayBack: vm.PayBack
            };

            var promise = RegistrarSolicitudService.ActualizarCapexLineaNegocio(capex);

            promise.then(
                function (response) {
                    var Respuesta = response.data;
                    UtilsFactory.Alerta('#alertSolicitud', 'sucess', Respuesta.Mensaje, 5);

                    vm.IdCapexLineaNegocio = 0;
                    vm.PayBack = 0;
                    vm.Van = 0;
                    vm.Fc = 0;
                    BuscarLineas();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#alertSolicitud', 'danger', MensajesUI.DatosError, 5);
                });

        };

        vm.dtInstance = {};
        vm.dtColumns = [
               DTColumnBuilder.newColumn('IdCapexLineaNegocio').notVisible(),
               DTColumnBuilder.newColumn('IdLineaNegocio').notVisible(),
               DTColumnBuilder.newColumn('LineaNegocio').withTitle('Linea Negocio').notSortable(),
               DTColumnBuilder.newColumn('Van').withTitle('Van').notSortable(),
               DTColumnBuilder.newColumn('Fc').withTitle('Fc').notSortable(),
               DTColumnBuilder.newColumn('PayBack').withTitle('PayBack').notSortable(),
               DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesLinea)
        ];
        function cargarGrilla() {

        };

        function AccionesLinea(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-2'><a title='Editar' class='btn btn-sm'  id='tab-button' ng-click='vm.EditarIndicador(" + data.IdCapexLineaNegocio + "," + data.Van + "," + data.Fc + "," + data.PayBack + ");' >" + "<span class='fa fa-edit'></span></a></div>";
        }

        function EditarIndicador(IdCapexLineaNegocio, Van, Fc, PayBack) {
            
            vm.IdCapexLineaNegocio = IdCapexLineaNegocio;
            vm.Van = Van;
            vm.Fc = Fc;
            vm.PayBack = PayBack;

        };

        function BuscarLineas() {
            
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarLineasPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }
        function BuscarLineasPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var lineas = {
                IdSolicitudCapex: vm.IdSolicitudCapex,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = RegistrarSolicitudService.ListarLineaNegocio(lineas);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListCapexLineaNegocioDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }


        function ObtenerProyecto() {
            if (vm.ObjCapex != 0) {
                vm.IdSolicitudCapex = vm.ObjCapex;

                var proyecto = {
                    IdSolicitudCapex: vm.IdSolicitudCapex
                };
                var promise = RegistrarSolicitudService.ObtenerSolicitud(proyecto);

                promise.then(
                    function (response) {
                        var Respuesta = response.data;
                        vm.CodigoSalesForce = Respuesta.NumeroSalesForce;
                        vm.Proyecto = Respuesta.Descripcion;
                        vm.Cliente = Respuesta.NombreCliente;
                        vm.CodigoPmo = Respuesta.CodigoPmo;
                        vm.Entidad = Respuesta.Entidad;
                        vm.IdProyectoSF = Respuesta.NumeroSalesForce;
                        ObtenerSeccion(1);
                        ObtenerSeccion(2);
                        ObtenerSeccion(3);
                        ObtenerSeccion(4);
                        ObtenerSeccion(5);
                    });
            }
        };


        function ObtenerSeccion(IdTipo) {

            var proyecto = {
                IdSolicitudCapex: vm.IdSolicitudCapex,
                IdTipoDescripcion: IdTipo
            };
            var promise = RegistrarSolicitudService.ObtenerSeccion(proyecto);

            promise.then(
                function (response) {
                    var Respuesta = response.data;

                    switch (IdTipo) {
                        case 1:
                            tinymce.get('GeneralProyecto').setContent(Respuesta.Descripcion);
                            vm.GeneralProyecto = tinymce.get('GeneralProyecto').getContent();
                            vm.IdSeccion1 = Respuesta.IdSeccion; break;
                        case 2:
                            tinymce.get('ResumenSolicitud').setContent(Respuesta.Descripcion);
                            vm.ResumenSolicitud = tinymce.get('ResumenSolicitud').getContent();
                            vm.IdSeccion2 = Respuesta.IdSeccion; break;
                        case 3:
                            tinymce.get('ObtendraSolicitud').setContent(Respuesta.Descripcion);
                            vm.ObtendraSolicitud = tinymce.get('ObtendraSolicitud').getContent();
                            vm.IdSeccion3 = Respuesta.IdSeccion; break;
                        case 4:
                            tinymce.get('SustentoCuantitativo').setContent(Respuesta.Descripcion);
                            vm.SustentoCuantitativo = tinymce.get('SustentoCuantitativo').getContent();
                            vm.IdSeccion4 = Respuesta.IdSeccion; break;
                        case 5:
                            tinymce.get('SustentoCualitativo').setContent(Respuesta.Descripcion);
                            vm.SustentoCualitativo = tinymce.get('SustentoCualitativo').getContent();
                            vm.IdSeccion5 = Respuesta.IdSeccion; break;
                    }
                });

        };


        function CargaIndicadores() {
            ListarLineasNegocio();
            BuscarLineas();
            if (vm.IdSolicitudCapex == 0) {
                UtilsFactory.OcultarElemento("#sectionIndicadores");
            } else {
                UtilsFactory.MostrarElemento("#sectionIndicadores");
            }
        };

        /******************************************* Load *******************************************/
        
        cargarGrilla();
        ObtenerProyecto();
        CargaIndicadores();
    }

})();