﻿(function () {
    'use strict'

    angular
    .module('app.Capex')
    .controller('CapexContenedorController', CapexContenedorController);

    CapexContenedorController.$inject = ['CapexContenedorService', 'MaestraService', 'RegistrarSolicitudService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function CapexContenedorController(CapexContenedorService, MaestraService, RegistrarSolicitudService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.CargaDetalle = CargaDetalle;
        vm.ListEstado = [];
        vm.CargarEstados = CargarEstados;
        vm.ObtieneEstado = ObtieneEstado;
        vm.ObjCapex = jsonCapex;
        

        vm.IdSolicitudCapex = (vm.ObjCapex != 0) ? vm.ObjCapex : 0;

      
        /******************************************* Tabla *******************************************/

        Init();


        vm.TabIndicadores = true;

        TabPorPerfilCapexDatos();
        function TabPorPerfilCapexDatos() {
            
            var promise = CapexContenedorService.TabPorPerfilCapexDatos();
                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                    vm.TabIndicadores = Respuesta.TabIndicadores;


                });

            
        };



        /******************************************* Metodos *******************************************/
        /******************************** Librerias Iniciales **********************************/
        function Init() {
            tinymce.init({
                selector: 'textarea.texto-enriquecido',
                theme: 'modern',
                height: 300,
                plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
                image_advtab: true,
                templates: [
                  { title: 'Test template 1', content: 'Test 1' },
                  { title: 'Test template 2', content: 'Test 2' }
                ],
                content_css: [
                  '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                  '//www.tinymce.com/css/codepen.min.css'
                ]
            });


        }


        function CargaDetalle() {

            var promise = CapexContenedorService.VistaDetalle();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#detalle").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };

        function CargarEstados() {
            var filtroEstado = {
                IdRelacion: 565
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroEstado);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListEstado = Respuesta;
            }, function (response) {
                blockUI.stop();
            });
        };


        function ObtieneEstado() {
            if (vm.IdSolicitudCapex == 0) {
                vm.IdEstado = 1;
            } else {

                var solicitud = {
                    IdSolicitudCapex: vm.IdSolicitudCapex
                };

                var promise = RegistrarSolicitudService.ObtenerSolicitud(solicitud);
                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                    vm.IdEstado = Respuesta.IdEstado;
                });

            }
        };

        function setGetLocal() {
            localStorage.setItem("IdEstado", "Parra");
            vm.IdEstado = localStorage.getItem("IdEstado");
            return localStorage.getItem("IdEstado");
        }

        /******************************************* Load *******************************************/
        ObtieneEstado();
        CargarEstados();

    }

})();