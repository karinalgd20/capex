﻿(function () {
    'use strict',
     angular
    .module('app.Capex')
    .service('CapexContenedorService', CapexContenedorService);

    CapexContenedorService.$inject = ['$http', 'URLS'];

    function CapexContenedorService($http, $urls) {

        var service = {
            VistaDetalle: VistaDetalle,
            RegistroIdSolicitudCapexSession: RegistroIdSolicitudCapexSession,
            ObtenerIdSolicitudCapexSession: ObtenerIdSolicitudCapexSession,
            TabPorPerfilCapexDatos: TabPorPerfilCapexDatos
        };

        return service;

        function RegistroIdSolicitudCapexSession(Id) {
            return $http({
                url: $urls.ApiCapex + "CapexContenedor/RegistroIdSolicitudCapexSession",
                method: "POST",
                params: { Id: Id }

            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function TabPorPerfilCapexDatos() {
            return $http({
                url: $urls.ApiCapex + "CapexContenedor/TabPorPerfilCapexDatos",
                method: "POST"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ObtenerIdSolicitudCapexSession() {
            return $http({
                url: $urls.ApiCapex + "CapexContenedor/ObtenerIdSolicitudCapexSession",
                method: "POST"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function VistaDetalle() {
            return $http({
                url: $urls.ApiCapex + "CapexContenedor/Detalle",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function VistaEstructuraCosto() {
            return $http({
                url: $urls.ApiCapex + "BandejaEstructuraCosto/Index",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();