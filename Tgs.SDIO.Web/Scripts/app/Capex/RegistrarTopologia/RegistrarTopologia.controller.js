﻿(function () {
    'use strict'
    angular
        .module('app.Capex')
        .controller('RegistrarTopologia', RegistrarTopologia);

    RegistrarTopologia.$inject =
        [
            'RegistrarTopologiaService',
            'ArchivoService',
            'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$injector'
        ];

    function RegistrarTopologia
        (
        RegistrarTopologiaService,
        ArchivoService,
        MaestraService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile,
        $injector
        ) {

        var vm = this;
        vm.ListTipoRegistrarTopologia = [];

        vm.Id = 0;
        vm.IdCapexLineaNegocio = 1;
      //  vm.IdCapexLineaNegocio = $scope.$parent.vm.IdCapexLineaNegocio;
        vm.IdTipo = '-1';
        vm.Descripcion = '';
        vm.DescripcionArchivo = '';

        vm.TxtArchivos = [];

        vm.dtColumns =
            [
            DTColumnBuilder.newColumn('Id').notVisible(),
            DTColumnBuilder.newColumn('Nombre').withTitle('Nombre').notSortable(),
            DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(Acciones)
            ];


     

        vm.RegistrarArchivo = RegistrarArchivo;
        vm.InactivarArchivo = InactivarArchivo;
        vm.BuscarArchivos = BuscarArchivos;
        vm.DesacargarArchivo = DesacargarArchivo;

        LimpiarGrilla();

 

     
    
        function BuscarArchivos() {

            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarArchivoPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('paging', true)
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }

        function BuscarArchivoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var archivo = {
                CodigoTabla: 120,
                IdEntidad:100,
                IdCategoria:100,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = ArchivoService.ListarArchivoPaginado(archivo);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaArchivos
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function Acciones(data, type, full, meta) {
            //return "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn ' data-toggle='modal'   ng-click='vm.ModalSubServicio(" + data.IdSubServicio + ");'>" + "<span class='fa fa-edit'></span></a></div> " +
            //"<div class='col-xs-4 col-sm-1'><a title='Inactivar' class='btn btn-sm'  id='tab-button' class='btn '  onclick='EliminarSubServicio(" + data.IdSubServicio + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return "<div class='col-xs-2 col-sm-1'><a id='tab-button' title='Inactivar' class='btn btn-sm' ng-click='vm.InactivarArchivo(" + data.Id + ");'>" + "<span class='fa fa-trash-o fa-lg'></span></a></div>" +
            "<div class='col-xs-4 col-sm-1'><a title='Descargar' class='btn btn-sm' id='tab-button' ng-click='vm.DesacargarArchivo(" + data.Id + ");'>" + "<span class='fa fa-edit'></span></a></div>";
        }

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', false);
        }

        function RegistrarArchivo() {
          

            //if (vm.Id == 0 || vm.Id == undefined) {
            //    UtilsFactory.Alerta('#lblAlertArchivo', 'warning', 'El tipo de requerimiento no existe', 5);
            //    return;
            //}

            if ($.trim(vm.TxtArchivos.length) == 0) {
                UtilsFactory.Alerta('#lblAlertArchivo', 'warning', 'Seleccione un archivo', 5);
                return;
            }

            if (vm.DescripcionArchivo == '' || vm.DescripcionArchivo == undefined) {
                UtilsFactory.Alerta('#lblAlertArchivo', 'warning', 'Ingrese la descripción del archivo', 5);
                return;
            }

            var archivo = {
                CodigoTabla: 120,
                IdEntidad: 100,
                IdCategoria: 100,
                Descripcion: vm.DescripcionArchivo
            };

            var formData = new FormData();
            formData.append('cargaArchivo', angular.toJson(archivo));
            angular.forEach(vm.TxtArchivos, function (file) {
                formData.append('files', file);
            });

            var promise = ArchivoService.RegistrarArchivo(formData);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                BuscarArchivos();
                vm.DescripcionArchivo = '';
                UtilsFactory.Alerta('#lblAleraRegistrarTopologia', 'success', Respuesta.Mensaje, 5);

            }, function (response) {
                blockUI.stop();
            });
        }

        function InactivarArchivo(Id) {
            var archivo = { Id: Id };
            blockUI.start();

            var promise = ArchivoService.InactivarArchivo(archivo);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                UtilsFactory.Alerta('#lblAleraRegistrarTopologia', 'success', Respuesta.Mensaje, 5);
                BuscarArchivos();

            }, function (response) {
                blockUI.stop();
            });
        }

        function DesacargarArchivo(Id) {
            window.location.href = $urls.ApiComun + "Archivo/DescargarArchivo/" + Id;
        }
    }
})();