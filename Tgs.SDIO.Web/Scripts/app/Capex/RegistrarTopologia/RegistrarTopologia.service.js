﻿(function () {
    'use strict',
     angular
        .module('app.Capex')
        .service('RegistrarTopologiaService', RegistrarTopologiaService);

    RegistrarTopologiaService.$inject = ['$http', 'URLS'];

    function RegistrarTopologiaService($http, $urls) {
        var service =
        {
            RegistrarRegistrarTopologia: RegistrarRegistrarTopologia,
            ObtenerRegistrarTopologiaPorIdOportunidad: ObtenerRegistrarTopologiaPorIdOportunidad
        };

        return service;

        function RegistrarRegistrarTopologia(request) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarTopologia/RegistrarRegistrarTopologia",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerRegistrarTopologiaPorIdOportunidad(request) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarTopologia/ObtenerRegistrarTopologiaPorIdOportunidad",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();