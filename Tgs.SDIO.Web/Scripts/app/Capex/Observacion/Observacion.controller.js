﻿(function () {
    'use strict'

    angular
    .module('app.Capex')
    .controller('ObservacionController', ObservacionController);

    ObservacionController.$inject = ['ObservacionService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function ObservacionController(ObservacionService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.CargarEstados = CargarEstados;
        vm.IdAsignado = 0;
        vm.IdResponsable = (jsonObservacion != null) ? jsonObservacion.IdResponsable : 0;
        vm.IdObservacion = (jsonObservacion != null) ? jsonObservacion.IdObservacion : 0;
        vm.IdSeccion = (jsonObservacion != null) ? jsonObservacion.IdSeccion : 0;
        vm.ObjSolicitud = jsonSolicitud;
        vm.IdSolicitudCapex = (vm.ObjSolicitud != 0) ? vm.ObjSolicitud : 0;
        vm.Asignado = "";
        vm.Responsable = "";
        vm.ObtieneObservacion = ObtieneObservacion;
        vm.CargaObservacion = CargaObservacion;
        /********************************* Funciones ******************************************/

        function CargaObservacion() {

            if (vm.IdObservacion == 0) {
                ObtenerAsignado();
                vm.Responsable = ObtenerUsuario(vm.IdResponsable);
            } else {
                ObtieneObservacion();
            }
        };

        function ObtieneObservacion() {

            var observacion = {
                IdObservacion: vm.IdObservacion
            };
            var promise = ObservacionService.ObtenerObservacion(observacion);
            promise.then(function (resultado) {

                var Respuesta = resultado.data;
                vm.IdAsignado = Respuesta.IdUsuarioCreacion;
                vm.IdResponsable = Respuesta.IdResponsable;
                vm.Descripcion = Respuesta.Descripcion;
                vm.IdSeccion = Respuesta.IdSeccion;
                vm.Comentario = Respuesta.Respuesta;
                vm.IdEstado = Respuesta.IdEstado;
                vm.Asignado = ObtenerUsuario(vm.IdAsignado);
                vm.Responsable = ObtenerUsuario(vm.IdResponsable);
            }, function (response) {
                blockUI.stop();
            });

        };

        function ObtenerAsignado() {

            var solicitud = {
                IdSolicitudCapex: vm.IdSolicitudCapex
            };
            var promise = ObservacionService.ObtenerUsuario(solicitud);
            promise.then(function (resultado) {

                var Respuesta = resultado.data;
                vm.IdAsignado = Respuesta.IdUsuarioCreacion;
                vm.Asignado = ObtenerUsuario(vm.IdAsignado);

            }, function (response) {
                blockUI.stop();
            });
        };

        function ObtenerUsuario(IdUsuario) {

            var promise = ObservacionService.ObtenerUsuario(filtroEstado);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                return Respuesta.NombresCompletos;
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarEstados() {
            var filtroEstado = {
                IdRelacion: 558
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroEstado);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListEstado = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function GrabarObservacion() {

            blockUI.start();

            var observacion = {
                IdObservacion: vm.IdObservacion,
                IdResponsable: vm.IdResponsable,
                Descripcion: vm.Descripcion,
                IdAsignado: vm.IdAsignado,
                IdSeccion: vm.IdSeccion,
                Respuesta: vm.Comentario,
                IdEstado: vm.IdEstado
            };

            var promise = (vm.IdObservacion == 0) ? ObservacionService.RegistrarObservacion(observacion) : ObservacionService.ActualizarObservacion(observacion);

            promise.then(
            function (response) {
                var Respuesta = response.data;
                var mensaje = Respuesta.Mensaje;

                vm.IdObservacion = Respuesta.Id;
                ActualizaSeccion();

                UtilsFactory.Alerta('#alertObservacion', 'sucess', mensaje, 5);

            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#alertObservacion', 'danger', MensajesUI.DatosError, 5);
            });
        };

        function ActualizaSeccion() {
            var seccion = {
                IdSolicitudCapex: vm.IdSolicitudCapex,
                IdSeccion: vm.IdSeccion,
                IdObservacion: vm.IdObservacion
            };
            var promise = ObservacionService.ActualizarSeccionObservacion();
        };

        /********************************* Load ******************************************/
        CargarEstados();
        CargaObservacion();

    }

})();