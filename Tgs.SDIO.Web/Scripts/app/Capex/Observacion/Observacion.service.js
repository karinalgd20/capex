﻿(function () {
    'use strict',
     angular
    .module('app.Capex')
    .service('ObservacionService', ObservacionService);

    ObservacionService.$inject = ['$http', 'URLS'];

    function ObservacionService($http, $urls) {

        var service = {
            RegistrarObservacion: RegistrarObservacion,
            ActualizarObservacion: ActualizarObservacion,
            ObtenerObservacion: ObtenerObservacion,
            VistaObservacion: VistaObservacion,
            ObtenerSolicitudCapexFlujoEstado: ObtenerSolicitudCapexFlujoEstado,
            ObtenerUsuario: ObtenerUsuario,
            ActualizarSeccionObservacion: ActualizarSeccionObservacion
        };

        function RegistrarObservacion(observacion) {
            return $http({
                url: $urls.ApiCapex + "Observacion/RegistrarObservacion",
                method: "POST",
                data: JSON.stringify(observacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarObservacion(observacion) {
            return $http({
                url: $urls.ApiCapex + "Observacion/ActualizarObservacion",
                method: "POST",
                data: JSON.stringify(observacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerObservacion(observacion) {
            return $http({
                url: $urls.ApiCapex + "Observacion/ObtenerObservacion",
                method: "POST",
                data: JSON.stringify(observacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function VistaObservacion() {
            return $http({
                url: $urls.ApiCapex + "Observacion/Index",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerSolicitudCapexFlujoEstado(observacion) {
            return $http({
                url: $urls.ApiCapex + "Observacion/ObtenerSolicitudCapexFlujoEstado",
                method: "POST",
                data: JSON.stringify(observacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerUsuario(usuario) {
            return $http({
                url: $urls.ApiCapex + "Observacion/ObtenerUsuario",
                method: "POST",
                data: JSON.stringify(usuario)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarSeccionObservacion(observacion) {
            return $http({
                url: $urls.ApiCapex + "Observacion/ActualizarSeccionObservacion",
                method: "POST",
                data: JSON.stringify(observacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        return service;
    }

})();