﻿(function () {
    'use strict',
     angular
    .module('app.Capex')
    .service('RegistrarEstructuraCostoService', RegistrarEstructuraCostoService);

    RegistrarEstructuraCostoService.$inject = ['$http', 'URLS'];

    function RegistrarEstructuraCostoService($http, $urls) {

        var service = {
      
            ModalEstructuraCosto: ModalEstructuraCosto,
            ListarGrupo: ListarGrupo,
            ListarConceptosCapex: ListarConceptosCapex,
            ObtenerEstructuraCostoGrupo: ObtenerEstructuraCostoGrupo,
            RegistrarEstructuraCostoGrupo: RegistrarEstructuraCostoGrupo,
            ActualizarEstructuraCostoGrupo: ActualizarEstructuraCostoGrupo

        };

        return service;
        function ActualizarEstructuraCostoGrupo(request) {

            return $http({
                url: $urls.ApiCapex + "RegistrarEstructuraCosto/ActualizarEstructuraCostoGrupo",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarEstructuraCostoGrupo(request) {

            return $http({
                url: $urls.ApiCapex + "RegistrarEstructuraCosto/RegistrarEstructuraCostoGrupo",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ObtenerEstructuraCostoGrupo(request) {

            return $http({
                url: $urls.ApiCapex + "RegistrarEstructuraCosto/ObtenerEstructuraCostoGrupo",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListarConceptosCapex(request) {

            return $http({
                url: $urls.ApiCapex + "RegistrarEstructuraCosto/ListarConceptosCapex",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListarGrupo(request) {

            return $http({
                url: $urls.ApiCapex + "RegistrarEstructuraCosto/ListarGrupo",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ModalEstructuraCosto(request) {

            return $http({
                url: $urls.ApiCapex + "RegistrarEstructuraCosto/Index",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();