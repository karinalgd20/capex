﻿(function () {
    'use strict'

    angular
    .module('app.Capex')
    .controller('RegistrarEstructuraCosto', RegistrarEstructuraCosto);

    RegistrarEstructuraCosto.$inject = ['RegistrarEstructuraCostoService', 'MaestraService', 'ServicioGrupoService', 'UbigeoService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarEstructuraCosto(RegistrarEstructuraCostoService,MaestraService, ServicioGrupoService,UbigeoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.InputConcepto = true;
        vm.InputDescripcion = true;
        vm.SelectServicio = true;
        vm.SelectMedio = true;
        vm.InputModelo = true;
        vm.SelectUbigeo = true;
        vm.TipoPedido = true;

        vm.IdCapexLineaNegocio = 1;

        vm.ListarGrupo = ListarGrupo;
        vm.RegistrarEstructuraCosto = RegistrarEstructuraCosto;
        vm.ObtenerEstructuraCosto = ObtenerEstructuraCosto;
        vm.CargaModalEstructuraCosto = CargaModalEstructuraCosto;
        vm.ListarConceptosCapex = ListarConceptosCapex;

        vm.CargarProvincia = CargarProvincia;
        vm.CargarDistrito = CargarDistrito;
        vm.CargarDepartamento = CargarDepartamento;

        vm.ListGrupo = [];
        vm.IdGrupo = "-1";

        vm.ListConceptoCapex = [];
        vm.ListConceptoCapex = UtilsFactory.AgregarItemSelect(vm.ListConceptoCapex);
        vm.IdConceptoCapex = "-1";

        vm.ListServicio = [],
        vm.IdServicio = "-1";

        vm.ListTipoPedido = [];
        vm.IdTipoPedido = "-1";

        vm.ListMedio = [];
        vm.IdMedio = "-1";

        vm.Descripcion="";



        vm.ListEstado = [];

        vm.DescripcionNuevo = "";
        vm.CostoInstalacion = "";
        vm.IdtipoServicio = "-1";
        vm.IdtipoEstructuraCosto = "-1";
        vm.IdEstado="-1";
        vm.IdEstructuraCosto = 0;
        vm.IdEstructuraCostoGrupo = 0;


        vm.MensajeDeAlerta = "";



        
        CargaModalEstructuraCosto();




        function CargaModalEstructuraCosto() {
            vm.IdEstructuraCostoGrupo = $scope.$parent.vm.IdEstructuraCostoGrupo;
            (vm.IdEstructuraCostoGrupo > 0) ? ObtenerEstructuraCosto() : CargaModal();
         

        }
        function CargaModal() {
            
            ListarGrupo();
            ListarServicios();
            TipoPedido();
            ListarMedio();
        }

        
        function ListarServicios() {

            var servicio = {
                IdEstado: 1,

            };
            var promise = ServicioGrupoService.ListarServicioGrupo(servicio);

            promise.then(function (resultado) {
             
                var Respuesta = resultado.data;


                vm.ListServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

   
        function LimpiarInput(IdGrupo) {

            vm.InputConcepto = true;
            vm.InputDescripcion = true;
            vm.SelectServicio = true;
            vm.SelectMedio = true;
            vm.InputModelo = true;
            vm.SelectUbigeo = true;
            vm.TipoPedido = true;
            if (IdGrupo == 1) {
                vm.InputDescripcion = false;
            }
            if (IdGrupo == 2) {


                vm.SelectMedio = false;
                vm.InputModelo = false;
                vm.SelectUbigeo = false;
                vm.TipoPedido = false;
            }
            if (IdGrupo == 3) {
                vm.InputConcepto = false;
                vm.SelectMedio = false;
                vm.InputModelo = false;
                vm.SelectUbigeo = false;
                vm.TipoPedido = false;
            }
            if (IdGrupo == 4) {
                vm.InputDescripcion = false;
                vm.InputConcepto = false;
                vm.SelectUbigeo = false;
                vm.TipoPedido = false;

            }
        }
        LimpiarInput();
        function ListarConceptosCapex(IdGrupo) {
            LimpiarInput(IdGrupo);
            //if (IdGrupo == "-1") {
            //    vm.InputConcepto = true;
            //    vm.InputDescripcion = true;
            //}
           
            if (IdGrupo != 1) {


                vm.ListConceptoCapex = [];
                vm.ListConceptoCapex = UtilsFactory.AgregarItemSelect(vm.ListConceptoCapex);


                vm.IdConceptoCapex = "-1";


                var conceptoCapex = {
                    IdGrupo: IdGrupo,

                };
                var promise = RegistrarEstructuraCostoService.ListarConceptosCapex(conceptoCapex);

                promise.then(function (resultado) {


                    var Respuesta = resultado.data;


                    vm.ListConceptoCapex = UtilsFactory.AgregarItemSelect(Respuesta);

                }, function (response) {
                    blockUI.stop();
                });

            }
           
        }
        function ListarMedio() {

            var maestra = {
                IdRelacion: 84,
                IdEstado:1

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {


                var Respuesta = resultado.data;


                vm.ListMedio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function TipoPedido() {

            var maestra = {
                IdRelacion: 562,
                IdEstado: 1

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {


                var Respuesta = resultado.data;


                vm.ListTipoPedido = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }
        function ListarGrupo() {

        

            var grupo = {
                IdGrupo: 1,

            };
            var promise = RegistrarEstructuraCostoService.ListarGrupo(grupo);

            promise.then(function (resultado) {


                var Respuesta = resultado.data;


                vm.ListGrupo = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }
  
        function RegistrarEstructuraCosto() {


            blockUI.start();
           var confirmacion=true;
           var CodigoDistrito = vm.CodigoDistrito;
           var IdServicio = vm.IdServicio;
           var IdConceptoCapex = vm.IdConceptoCapex;
           var IdMedio = vm.IdMedio;
            if(vm.IdGrupo=="-1"){
            confirmacion=false;
            UtilsFactory.Alerta('#divAlert_Registrar', 'warning', "Seleccione Grupo", 5);
             }
           
            if (CodigoDistrito == "-1") {
      
               // UtilsFactory.Alerta('#divAlert_Registrar', 'warning', "Seleccione Distrito", 5);
                CodigoDistrito = null;
            }
            if (IdServicio == "-1") {
     
                // UtilsFactory.Alerta('#divAlert_Registrar', 'warning', "Seleccione Distrito", 5);
                IdServicio = null;
            }
            if (IdMedio == "-1") {
       
                // UtilsFactory.Alerta('#divAlert_Registrar', 'warning', "Seleccione Distrito", 5);
                IdMedio = null;
            }
            if (IdConceptoCapex == "-1") {
            
                // UtilsFactory.Alerta('#divAlert_Registrar', 'warning', "Seleccione Distrito", 5);
                IdConceptoCapex = null;
            }



            if(confirmacion){
                var EstructuraCosto = {
                IdCapexLineaNegocio:vm.IdCapexLineaNegocio,
                IdEstructuraCostoGrupo: vm.IdEstructuraCostoGrupo,
                IdGrupo: vm.IdGrupo,
                Asignacion: vm.Asignacion,
                Cantidad: vm.Cantidad,
                CapexDolares: vm.Dolares,
                CapexSoles: vm.Soles,
                CapexTotalSoles: vm.TotalSoles,
                Certificacion: vm.Certificacion,
                Descripcion: vm.Descripcion,
                IdConceptoCapex: vm.IdConceptoCapex,
                IdMedioCosto: vm.IdMedio,
                IdServicio: IdServicio,
              //  IdTipoGrupo: vm.IdTipoGrupo,
                IdUbigeo: CodigoDistrito,


            };
                var promise = (vm.IdEstructuraCostoGrupo > 0) ? RegistrarEstructuraCostoService.ActualizarEstructuraCostoGrupo(EstructuraCosto) : RegistrarEstructuraCostoService.RegistrarEstructuraCostoGrupo(EstructuraCosto);
            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
     
                    vm.IdEstructuraCostoGrupo = Respuesta.Id;
          
                
        
                UtilsFactory.Alerta('#divAlert_Registrar_EstructuraCosto', 'success',"Se Registro Correctamente", 5);


            }, function (response) {
                blockUI.stop();

            });
            }
        
                blockUI.stop();
          
              
          
        }
        function ObtenerEstructuraCosto() {
            blockUI.start();

            var EstructuraCosto = {
                IdEstructuraCostoGrupo: vm.IdEstructuraCostoGrupo
            }

            var promise = RegistrarEstructuraCostoService.ObtenerEstructuraCostoGrupo(EstructuraCosto);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                CargaModal();
                LimpiarInput(Respuesta.IdGrupo);

                vm.IdGrupo = Respuesta.IdGrupo;
                vm.Asignacion = Respuesta.Asignacion;
                vm.Cantidad = Respuesta.Cantidad;
                
                vm.Dolares=Respuesta.CapexDolares;
                vm.Soles=Respuesta.CapexSoles;
                vm.TotalSoles = Respuesta.CapexTotalSoles;
                
                vm.Certificacion = Respuesta.Certificacion;
                vm.Descripcion = Respuesta.Descripcion;
                vm.IdConceptoCapex = Respuesta.IdConcepto == 0 ? "-1" : Respuesta.IdConcepto;

                vm.IdEstructuraCostoGrupo = Respuesta.IdEstructuraCostoGrupo;
                vm.IdEstructuraCostoGrupoDetalle = Respuesta.IdEstructuraCostoGrupoDetalle;
                vm.IdMedio = Respuesta.IdMedioCosto == 0 ? "-1" : Respuesta.IdMedioCosto;


                vm.IdServicio = Respuesta.IdServicio == 0 ? "-1" : Respuesta.IdServicio;
                vm.IdTipoGrupo = Respuesta.IdTipoGrupo;
          
                if (Respuesta.IdUbigeo.length>1) {
                    vm.CodigoDistrito = Respuesta.IdUbigeo;
                    ObtenerPorCodigoDistrito(vm.CodigoDistrito);

                   
                }
              

            }, function (response) {
                blockUI.stop();

            });
        }

        
        function ObtenerPorCodigoDistrito(CodigoDepartamento) {
            vm.ListDepartamento = [];
            vm.ListProvincia = [];
            vm.ListDistrito = [];

            vm.ListDepartamento = UtilsFactory.AgregarItemSelect(vm.ListDepartamento);
            vm.ListProvincia = UtilsFactory.AgregarItemSelect(vm.ListProvincia);
            vm.ListDistrito = UtilsFactory.AgregarItemSelect(vm.ListDistrito);
                var departamento = {
                    CodigoDistrito: CodigoDepartamento
                };
                var promise = UbigeoService.ObtenerPorCodigoDistrito(departamento);
                promise.then(function (resultado) {

                    var Respuesta = resultado.data;
                    CargarDepartamento();
                   
                    CargarProvincia(Respuesta.CodigoDepartamento);
                    CargarDistrito(Respuesta.CodigoProvincia);

                    vm.CodigoDepartamento = Respuesta.CodigoDepartamento;
                    vm.CodigoProvincia = Respuesta.CodigoProvincia;
                    vm.CodigoDistrito = Respuesta.CodigoDistrito;

                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
           
        

        }

        vm.ListDepartamento = [];
        vm.ListProvincia = [];
        vm.ListDistrito = [];


        vm.ListProvincia = UtilsFactory.AgregarItemSelect(vm.ListProvincia);
        vm.ListDistrito = UtilsFactory.AgregarItemSelect(vm.ListDistrito);

        vm.CodigoDepartamento = "-1";

        vm.CodigoProvincia = "-1";
        vm.CodigoDistrito = "-1";

        CargarDepartamento();

   
            function CargarProvinciaLimpiar() {  
                vm.ListProvincia = UtilsFactory.AgregarItemSelect(vm.ListProvincia);
                vm.CodigoProvincia = "-1";
            }
            function CargarDistritoLimpiar() {  
                vm.ListDistrito = UtilsFactory.AgregarItemSelect(vm.ListDistrito);
                vm.CodigoDistrito = "-1";
            }
         function CargarDepartamento() {
            var promise = UbigeoService.ListarComboUbigeoDepartamento();
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListDepartamento = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarProvincia(CodigoDepartamento) {

            var codDepartamento = zeroFill(CodigoDepartamento, 2);
            if (codDepartamento != "-1") {

                CargarProvinciaLimpiar();
                CargarDistritoLimpiar();
                var departamento = {
                    CodigoDepartamento: codDepartamento
                };
                var promise = UbigeoService.ListarComboUbigeoProvincia(departamento);
                promise.then(function (resultado) {
               
                        var Respuesta = resultado.data;
                        vm.ListProvincia = UtilsFactory.AgregarItemSelect(Respuesta);
                      
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
            }
         

        }

        function CargarDistrito(CodigoProvincia) {


            

            var codProvincia = zeroFill(CodigoProvincia, 4);
            if (codProvincia != "-1") {

            
                var provincia = {
                    CodigoProvincia: codProvincia
                };
                var promise = UbigeoService.ListarComboUbigeoDistrito(provincia);
                promise.then(function (resultado) {
                  
                        var Respuesta = resultado.data;
                        vm.ListDistrito = UtilsFactory.AgregarItemSelect(Respuesta);
                      
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
            }
     
          

        }

        function zeroFill(number, width) {
            width -= number.toString().length;
            if (width > 0) {
                return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
            }
            return number + ""; // siempre devuelve tipo cadena
        }
    }
})();








