﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('RiesgoProyectoService', RiesgoProyectoService);

    RiesgoProyectoService.$inject = ['$http', 'URLS'];

    function RiesgoProyectoService($http, $urls) {
        var service = {
            VistaRiesgoProyecto : VistaRiesgoProyecto,
            RegistrarRiesgoProyecto: RegistrarRiesgoProyecto,
            ActualizarRiesgoProyecto: ActualizarRiesgoProyecto,
            EliminarRiesgoProyecto: EliminarRiesgoProyecto,
            ListarRiesgoProyectosPaginado: ListarRiesgoProyectosPaginado,
            ObtenerRiesgoProyectoPorId: ObtenerRiesgoProyectoPorId
        };

        return service;

        function VistaRiesgoProyecto() {
            return $http({
                url: $urls.ApiComun + "BandejaRiesgoProyecto/Index",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarRiesgoProyecto(riesgoProyecto) {
            return $http({
                url: $urls.ApiComun + "RiesgoProyecto/RegistrarRiesgoProyecto",
                method: "POST",
                data: JSON.stringify(riesgoProyecto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarRiesgoProyecto(riesgoProyecto) {
            return $http({
                url: $urls.ApiComun + "RiesgoProyecto/ActualizarRiesgoProyecto",
                method: "POST",
                data: JSON.stringify(riesgoProyecto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarRiesgoProyecto(riesgoProyecto) {
            return $http({
                url: $urls.ApiComun + "RiesgoProyecto/EliminarRiesgoProyecto",
                method: "POST",
                data: JSON.stringify(riesgoProyecto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarRiesgoProyectosPaginado(riesgoProyecto) {
            return $http({
                url: $urls.ApiComun + "RiesgoProyecto/ListarRiesgoProyectosPaginado",
                method: "POST",
                data: JSON.stringify(riesgoProyecto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerRiesgoProyectoPorId(riesgoProyecto) {
            return $http({
                url: $urls.ApiComun + "RiesgoProyecto/ObtenerRiesgoProyectoPorId",
                method: "POST",
                data: JSON.stringify(riesgoProyecto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();