﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.Comun')
    .controller('MaestraController', MaestraController);

    MaestraController.$inject =
        [
            'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile'
        ];

    function MaestraController
        (
        MaestraService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile
        )
    {

        vm = this;

    }

})();