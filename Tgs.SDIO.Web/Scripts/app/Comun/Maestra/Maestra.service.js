﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('MaestraService', MaestraService);

    MaestraService.$inject = ['$http', 'URLS'];

    function MaestraService($http, $urls) {

        var service = {
          
            ListarMaestraPorIdRelacion: ListarMaestraPorIdRelacion,
            ListarMaestraPorValor:ListarMaestraPorValor,
            ObtenerMaestra: ObtenerMaestra,
            RegistrarMaestra: RegistrarMaestra,
            ActualizarMaestra: ActualizarMaestra,
            ListarMaestraPorIdRelacion2: ListarMaestraPorIdRelacion2,
            ListarMaestra: ListarMaestra
        };

        return service;

        //Maestra 
        function ListarMaestraPorIdRelacion(maestra) {

            return $http({
                url: $urls.ApiComun + "Maestra/ListarMaestraPorIdRelacion",
                method: "POST",
                data: JSON.stringify(maestra)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarMaestraPorValor(maestra) {

            return $http({
                url: $urls.ApiComun + "Maestra/ListarMaestraPorValor",
                method: "POST",
                data: JSON.stringify(maestra)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerMaestra(maestra) {

            return $http({
                url: $urls.ApiComun + "Maestra/ObtenerMaestra",
                method: "POST",
                data: JSON.stringify(maestra)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarMaestra(maestra) {

            return $http({
                url: $urls.ApiComun + "Maestra/ActualizarMaestra",
                method: "POST",
                data: JSON.stringify(maestra)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarMaestra(maestra) {

            return $http({
                url: $urls.ApiComun + "Maestra/RegistrarMaestra",
                method: "POST",
                data: JSON.stringify(maestra)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarMaestraPorIdRelacion2(maestra) {

            return $http({
                url: $urls.ApiComun + "Maestra/ListarMaestraPorIdRelacion2",
                method: "POST",
                data: JSON.stringify(maestra)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarMaestra(maestra) {

            return $http({
                url: $urls.ApiComun + "Maestra/ListarMaestra",
                method: "POST",
                data: JSON.stringify(maestra)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }

})();