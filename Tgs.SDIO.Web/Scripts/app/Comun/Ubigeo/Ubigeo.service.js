﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('UbigeoService', UbigeoService);

    UbigeoService.$inject = ['$http', 'URLS'];

    function UbigeoService($http, $urls) {

        var service = {
            ListarComboUbigeoDepartamento: ListarComboUbigeoDepartamento,
            ListarComboUbigeoProvincia: ListarComboUbigeoProvincia,
            ListarComboUbigeoDistrito: ListarComboUbigeoDistrito,
            ObtenerPorCodigoDistrito: ObtenerPorCodigoDistrito

        };

        return service;

        function ListarComboUbigeoDepartamento() {
            return $http({
                url: $urls.ApiComun + "Ubigeo/ListarComboUbigeoDepartamento",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
        function ObtenerPorCodigoDistrito(ubigeo) {
            return $http({
                url: $urls.ApiComun + "Ubigeo/ObtenerPorCodigoDistrito",
                method: "POST",
                data: JSON.stringify(ubigeo)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListarComboUbigeoProvincia(ubigeo) {
            return $http({
                url: $urls.ApiComun + "Ubigeo/ListarComboUbigeoProvincia",
                method: "POST",
                data: JSON.stringify(ubigeo)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarComboUbigeoDistrito(ubigeo) {
            return $http({
                url: $urls.ApiComun + "Ubigeo/ListarComboUbigeoDistrito",
                method: "POST",
                data: JSON.stringify(ubigeo)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();