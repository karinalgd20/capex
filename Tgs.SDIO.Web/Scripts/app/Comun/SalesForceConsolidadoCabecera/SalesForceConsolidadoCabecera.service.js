﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('SalesForceConsolidadoCabeceraService', SalesForceConsolidadoCabeceraService);

    SalesForceConsolidadoCabeceraService.$inject = ['$http', 'URLS'];

    function SalesForceConsolidadoCabeceraService($http, $urls) {

        var service = {
            //Servicios
            ListarProbabilidades: ListarProbabilidades,
            ListarNumeroSalesForcePorIdOportunidad: ListarNumeroSalesForcePorIdOportunidad

        };

        return service;


      //Probabilidades
        function ListarProbabilidades() {

            return $http({
                url: $urls.ApiComun + "Comun/ListarProbabilidades",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                for (index = 0; index <= resultado.data.length -1 ; ++index) {
                    if (resultado.data[index].ProbabilidadExito == null)
                    {
                        resultado.data[index].ProbabilidadExito = -1;
                        break;
                    }
                }

                return resultado;
            }
        };
     
          function ListarNumeroSalesForcePorIdOportunidad(request) {

            return $http({
                url: $urls.ApiComun + "SalesForceConsolidadoCabecera/ListarNumeroSalesForcePorIdOportunidad",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        
    }

})();


