﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('TipoCambioService', TipoCambioService);

    TipoCambioService.$inject = ['$http', 'URLS'];

    function TipoCambioService($http, $urls) {

        var service = {
            ListaTipoCambioPaginado: ListaTipoCambioPaginado,

        };

        return service;

        function ListaTipoCambioPaginado() {
            return $http({
                url: $urls.ApiComun + "TipoCambio/ListaTipoCambioPaginado",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    

    }
})();