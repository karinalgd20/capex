﻿var vm;
(function () {
	'use strict',

    angular
    .module('app.Comun')
    .controller('RegistrarProveedorController', RegistrarProveedorController);

	RegistrarProveedorController.$inject =
        [
            'RegistrarProveedorService',
            'ProveedorService',
            'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile'
        ];

	function RegistrarProveedorController
        (
        RegistrarProveedorService,
        ProveedorService,
        MaestraService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile
        )
	{

	    vm = this;

	    /*---- Se definen e inicializan variables del Proveedor ----*/

	    vm.DescripcionProveedor = "";
	    vm.EstadoProveedor = "";
		vm.TipoProveedor = "";
		vm.RazonSocial = "";
		vm.CodigoProveedor = "";
		vm.RUC = "";
		vm.IdSecuencia = "";
		vm.NombrePersonaContacto = "";
		vm.TelefonoPrincipal = "";
		vm.CorreoPrincipal = "";
		vm.TelefonoSecundario = "";
		vm.CorreoSecundario = "";
		vm.CodigoSRM = "";
		
		vm.IdProveedor = parseInt($("#IdProveedor").val());
		vm.RegistrarProveedor = RegistrarProveedor;
		vm.ActualizarProveedor = ActualizarProveedor;

		/*--------- Registrar/Actualizar Proveedor ---------*/
		function RegistrarProveedor() {
			
			if (confirm('¿Estas seguro de grabar el registro ?')) {
				blockUI.start();

				var proveedor = {
					IdProveedor: vm.IdProveedor,
					Descripcion: vm.DescripcionProveedor,
					IdEstado: vm.EstadoProveedor,
					TipoProveedor: vm.TipoProveedor,
					RazonSocial: vm.RazonSocial,
					CodigoProveedor: vm.CodigoProveedor,
					RUC: vm.RUC,
					Pais: vm.Pais,
					IdSecuencia: vm.IdSecuencia,
					NombrePersonaContacto: vm.NombrePersonaContacto,
					TelefonoContactoPrincipal: vm.TelefonoPrincipal,
					CorreoContactoPrincipal: vm.CorreoPrincipal,
					TelefonoContactoSecundario: vm.TelefonoSecundario,
					CorreoContactoSecundario: vm.CorreoSecundario,
					CodigoSRM: vm.CodigoSRM
				};

				var promise = ProveedorService.RegistrarProveedor(proveedor);
				promise.then(function (resultado) {
					var Respuesta = resultado.data;
					vm.IdProveedor = Respuesta.Id;
					UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
				}, function (response) {
					blockUI.stop();
					UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
				});
			}
		}

		function ObtenerProveedorById() {
		    //let IdProveedor = parseInt($("#IdProveedor").val());
		    if (vm.IdProveedor > 0) {
		        $('#btnActualizar').css('display', 'inline')
		        $('#btnRegistrar').css('display', 'none')
		       var Proveedor = {
		           IdProveedor: vm.IdProveedor
		       }
		        var promise = ProveedorService.ObtenerProveedorById(Proveedor);
		        promise.then(function (resultado) {
		            if (resultado.data != null && resultado.data != '') {
		                //vm.IdProveedor = vm.IdProveedor;
		                vm.DescripcionProveedor = resultado.data.Descripcion;
		                vm.EstadoProveedor = resultado.data.IdEstado;
		                vm.TipoProveedor = resultado.data.TipoProveedor;
		                vm.RazonSocial = resultado.data.RazonSocial;
		                vm.CodigoProveedor = resultado.data.CodigoProveedor;
		                vm.RUC = resultado.data.RUC;
		                vm.Pais = resultado.data.Pais;
		                vm.IdSecuencia = resultado.data.IdSecuencia;
		                vm.NombrePersonaContacto = resultado.data.NombrePersonaContacto;
		                vm.TelefonoPrincipal = resultado.data.TelefonoContactoPrincipal;
		                vm.CorreoPrincipal = resultado.data.CorreoContactoPrincipal;
		                vm.TelefonoSecundario = resultado.data.TelefonoContactoSecundario;
		                vm.CorreoSecundario = resultado.data.CorreoContactoSecundario;
		                vm.CodigoSRM = resultado.data.CodigoSRM;
		            }
		        }, function (response) {
		            blockUI.stop();
		        });
		    }

		}
		
		function ActualizarProveedor() {
		  
		    if (confirm('¿Estas seguro de Actualizar el Proveedor ?')) {
		        blockUI.start();

		        var proveedor = {
		            IdProveedor: vm.IdProveedor,
		            Descripcion: vm.DescripcionProveedor,
		            IdEstado: vm.EstadoProveedor,
		            TipoProveedor: vm.TipoProveedor,
		            RazonSocial: vm.RazonSocial,
		            CodigoProveedor: vm.CodigoProveedor,
		            RUC: vm.RUC,
		            Pais: vm.Pais,
		            IdSecuencia: vm.IdSecuencia,
		            NombrePersonaContacto: vm.NombrePersonaContacto,
		            TelefonoContactoPrincipal: vm.TelefonoPrincipal,
		            CorreoContactoPrincipal: vm.CorreoPrincipal,
		            TelefonoContactoSecundario: vm.TelefonoSecundario,
		            CorreoContactoSecundario: vm.CorreoSecundario,
		            CodigoSRM: vm.CodigoSRM
		        };

		        var promise = ProveedorService.ActualizarProveedor(proveedor);
		        promise.then(function (resultado) {
		            var Respuesta = resultado.data;
		            vm.IdProveedor = Respuesta.Id;
		            UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
		        }, function (response) {
		            blockUI.stop();
		            UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
		        });
		    }
		}

		/*--------- Inactivar Proveedor ---------*/

	    /*--------- Cargar ComboBox ---------*/

		function CargarEstadoProveedor() {
		    var filtro = {
		        IdRelacion: 1
		    };
		    var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
		    promise.then(function (resultado) {
		        var Respuesta = resultado.data;
		        vm.ListaEstados = UtilsFactory.AgregarItemSelect(Respuesta);
		    }, function (response) {
		        blockUI.stop();
		    });
		};

		function CargarTipoProveedor() {
		    var filtro = {
		        IdRelacion: 74
		    };
		    var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
		    promise.then(function (resultado) {
		        var Respuesta = resultado.data;
		        vm.ListaTipoProveedor = UtilsFactory.AgregarItemSelect(Respuesta);
		    }, function (response) {
		        blockUI.stop();
		    });
		};

		function CargarComboBoxPais() {
		    var promise = RegistrarProveedorService.ListarPaisesComboBoxProveedor(filtro);
		    promise.then(function (resultado) {
		        var Respuesta = resultado.data;
		        vm.ListaEstados = UtilsFactory.AgregarItemSelect(Respuesta);
		    }, function (response) {
		        blockUI.stop();
		    });
		}

		vm.EstadoProveedor = -1;
		vm.TipoProveedor = -1;

	    /*--------- Inicializar Funciones ---------*/
		CargarEstadoProveedor();
		CargarTipoProveedor();
		ObtenerProveedorById();

		
		
	}

})();