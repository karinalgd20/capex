﻿(function () {
    'use strict',
    angular
    .module('app.Comun')
    .service('RegistrarProveedorService', RegistrarProveedorService)

    RegistrarProveedorService.$inject = ['$http', 'URLS'];

    function RegistrarProveedorService($http, $urls)
    {
        var service = {

            ListarPaisesComboBoxProveedor: ListarPaisesComboBoxProveedor

        };

        return service;

        function ListarPaisesComboBoxProveedor(request) {
            return $http({
                url: $urls.ApiComun + "RegistrarProveedor/ListarPaisesComboBoxProveedor",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

    }

    
})();