﻿(function () {
    'use strict'
    angular
        .module('app.Comun')
        .controller('BandejaActividad', BandejaActividad);
    BandejaActividad.$inject = ['BandejaActividadService', 'EtapaService', 'AreaSeguimientoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function BandejaActividad(BandejaActividadService, EtapaService, AreaSeguimientoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.BuscarActividad = BuscarActividad;
        vm.EliminarActividad = EliminarActividad;
        vm.ListarComboAreas = ListarComboAreas;
        vm.ListarComboEtapas = ListarComboEtapas;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.Descripcion = "";
        vm.IdEtapa = "-1";
        vm.IdAreaSeguimiento = "-1";
        LimpiarGrilla();
        ListarComboAreas();
        ListarComboEtapas();
        vm.dtColumns = [
            DTColumnBuilder.newColumn('IdActividad').withTitle('Código').notSortable().withOption('width', '9%'),
            DTColumnBuilder.newColumn('IdActividadPadre').withTitle('Código Agrupador').notSortable().withOption('width', '9%'),
            DTColumnBuilder.newColumn('Descripcion').withTitle('Descripción').notSortable().renderWith().withOption('width', '10%'),
            DTColumnBuilder.newColumn('Predecesoras').withTitle('Predecesoras').notSortable().withOption('width', '9%'),
            DTColumnBuilder.newColumn('DescripcionArea').withTitle('Area/Sistema').notSortable().withOption('width', '9%'),
            DTColumnBuilder.newColumn('DescripcionTipoActividad').withTitle('Tipo Actividad').notSortable().withOption('width', '9%'),
            DTColumnBuilder.newColumn(null).withTitle('Asegurar Oferta').notSortable().renderWith(CheckboxAsegurarOferta).withOption('width', '9%'),
            DTColumnBuilder.newColumn('DescripcionFase').withTitle('Fase').notSortable().withOption('width', '8%'),
            DTColumnBuilder.newColumn(null).withTitle('Capex Mayor').notSortable().renderWith(CheckboxFlgCapexMayor).withOption('width', '9%'),
            DTColumnBuilder.newColumn(null).withTitle('Capex Menor').notSortable().renderWith(CheckboxFlgCapexMenor).withOption('width', '9%'),
            //DTColumnBuilder.newColumn('NumeroDiaCapexMenor').withTitle('N° Dia Capex Menor').notSortable().withOption('width', '10%'),
            //DTColumnBuilder.newColumn('CantidadDiasCapexMenor').withTitle('Cantidad Dias Capex Menor').notSortable().withOption('width', '11%'),
            //DTColumnBuilder.newColumn('NumeroDiaCapexMayor').withTitle('N° Dia Capex Mayor').notSortable().withOption('width', '10%'),
            //DTColumnBuilder.newColumn('CantidadDiasCapexMayor').withTitle('Cantidad Dias Capex Mayor').notSortable().withOption('width', '11%'),
            DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '9%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaAreaSeguimiento).withOption('width', '9%')
        ];
        //Modal registrar

        vm.ModalActividad = ModalActividad;

        function ModalActividad(IdActividad) {
            vm.IdActividad = IdActividad;
            var actividad = {
                IdActividad: IdActividad
            };

            var promise = BandejaActividadService.ModalActividad(actividad);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoActividad").html(content);
                });

                $('#ModalRegistrarActividad').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };
        //Lista los IdAreaSeguimiento y Descripciones de la tabla [Comun].[AreaSeguimiento] que esten Activos.
        function ListarComboAreas() {
            blockUI.start();
            var promise = AreaSeguimientoService.ListarComboAreas();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Areas", 5);
                } else {
                    vm.listAreas = UtilsFactory.AgregarItemSelect(respuesta);
                    vm.IdAreaSeguimiento = "-1";
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [Comun].[EtapaOportunidad] que esten Activos.
        function ListarComboEtapas() {
            blockUI.start();
            var promise = EtapaService.ListarComboEtapas();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Etapas", 5);
                } else {
                    vm.listEtapas = UtilsFactory.AgregarItemSelect(respuesta);
                    vm.IdEtapa = "-1";
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Eliminacion logica de la tabla [Comun].[Actividad] 
        function EliminarActividad(IdActividad) {
            blockUI.start();
            if (confirm('¿Estas seguro que desea eliminar?')) {

                var actividad = {
                    IdActividad: IdActividad
                }
                var promise = BandejaActividadService.EliminarActividad(actividad);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                        BuscarActividad()
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                    }
                    modalpopupConfirm.modal('hide');
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            }
            else {
                blockUI.stop();
            }
        }        
        //Filtro de la  tabla [Comun].[Actividad] por Descripcion, Area y Etapa.
        function BuscarActividad() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarActividadPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }
        function BuscarActividadPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var actividad = {
                Descripcion: vm.Descripcion,
                IdAreaSeguimiento: vm.IdAreaSeguimiento,
                IdEtapa: vm.IdEtapa,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = BandejaActividadService.ListarActividadPaginado(actividad);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListActividadDto
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('destroy', true)
                .withOption('scrollCollapse', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('scrollX', true)
                .withOption('paging', false);
        }
        //Inicializa  html de la columna Accion y checkbox.
        function AccionesBusquedaAreaSeguimiento(data, type, full, meta) {
             var respuesta = "";
             respuesta = respuesta + " <a title='Editar'   ng-click='vm.ModalActividad(" + data.IdActividad + ");'>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
             respuesta = respuesta + "<a title='Eliminar'  onclick='EliminarActividad(" + data.IdActividad + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6; margin-left: 1px;'></span></a>";
            return respuesta;
        }
        function CheckboxFlgCapexMayor(data, type, full, meta) {
            if (data.FlgCapexMayor == true) {
                return '<input type="checkbox" checked="checked" disabled="disabled">';
            } else {
                return '<input type="checkbox" disabled="disabled">';
            }
        }
        function CheckboxFlgCapexMenor(data, type, full, meta) {
            if (data.FlgCapexMenor == true) {
                return '<input type="checkbox" checked="checked" disabled="disabled">';
            } else {
                return '<input type="checkbox" disabled="disabled">';
            }
        }
        function CheckboxAsegurarOferta(data, type, full, meta) {
            if (data.AsegurarOferta == true) {
                return '<input type="checkbox" checked="checked" disabled="disabled">';
            } else {
                return '<input type="checkbox" disabled="disabled">';
            }
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.Descripcion = "";
            vm.TipoEtapas = "";
            vm.TipoAreas = "";
            LimpiarGrilla();
        }


        //Mostrar Resultado
        //vm.promiseActividad = promiseActividad;
        //function MostrarAlerta() {
        //    var RespuestaActividad = promiseActividad.data;
        //        if (RespuestaActividad.TipoRespuesta == 0) {
        //            UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
        //        } else {
        //            UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar la Actividad.", 5);
        //        }
        //}
    }
})();