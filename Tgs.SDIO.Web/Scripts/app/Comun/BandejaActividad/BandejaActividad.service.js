﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('BandejaActividadService', BandejaActividadService);

    BandejaActividadService.$inject = ['$http', 'URLS'];

    function BandejaActividadService($http, $urls) {

        var service = {
            ListarActividadPaginado: ListarActividadPaginado,
            EliminarActividad: EliminarActividad,
            ModalActividad: ModalActividad
        };

        return service;

        function ListarActividadPaginado(actividad) {
            debugger;
            return $http({
                url: $urls.ApiComun + "BandejaActividad/ListarActividad",
                method: "POST",
                data: JSON.stringify(actividad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarActividad(actividad) {
            return $http({
                url: $urls.ApiComun + "BandejaActividad/EliminarActividad",
                method: "POST",
                data: JSON.stringify(actividad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalActividad(actividad) {
            return $http({
                url: $urls.ApiComun + "RegistrarActividad/Index",
                method: "POST",
                data: JSON.stringify(actividad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();