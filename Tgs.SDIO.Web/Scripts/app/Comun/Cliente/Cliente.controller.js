﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.Comun')
    .controller('ClienteController', ClienteController);

    ClienteController.$inject = [
            'ClienteService',
            'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile'];

    function ClienteController(
            ClienteService,
            MaestraService,
            blockUI,
            UtilsFactory,
            DTOptionsBuilder,
            DTColumnBuilder,
            $timeout,
            MensajesUI,
            $urls, 
            $scope, 
            $compile) {

        vm = this;

        vm.BuscarCliente = BuscarCliente;
        vm.CargarTipoEntidad = CargarTipoEntidad;
        vm.CargarTipoIdentificadorFiscal = CargarTipoIdentificadorFiscal;
        vm.CargarSector = CargarSector;
        vm.CargarDireccionComercial = CargarDireccionComercial;
        vm.NuevoCliente = NuevoCliente;
        vm.EditarCliente = EditarCliente;
        vm.EliminarCliente = EliminarCliente;
        vm.LimpiarFormulario = LimpiarFormulario;
        vm.GuardarCliente = GuardarCliente;
        vm.CerrarPopup = CerrarPopup;
        vm.LimpiarFiltros = LimpiarFiltros;

        vm.search = {};
        vm.cliente = {};

        /******************************************* Tabla - Registro de Carta Fianza *******************************************/
        vm.dtInstanceCliente = {};

        vm.dtColumnsCliente = [

             DTColumnBuilder.newColumn('IdCliente').notVisible(),
             DTColumnBuilder.newColumn('IdSector').notVisible(),
             DTColumnBuilder.newColumn('IdDireccionComercial').notVisible(),
             DTColumnBuilder.newColumn('IdTipoIdentificadorFiscalTm').notVisible(),
             DTColumnBuilder.newColumn('IdTipoEntidad').notVisible(),

             DTColumnBuilder.newColumn('CodigoCliente').withTitle('Código'),
             DTColumnBuilder.newColumn('Descripcion').withTitle('Nombre'),
             DTColumnBuilder.newColumn('GerenteComercial').withTitle('Gerente Comercial'),
             DTColumnBuilder.newColumn('NumeroIdentificadorFiscal').withTitle('RUC'),
             DTColumnBuilder.newColumn('Email').withTitle('Email'),
             DTColumnBuilder.newColumn('Direccion').withTitle('Dirección'),

             DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCliente)
        ];

        function AccionesCliente(data, type, full, meta) {
            let NumeroFila = meta.row;
            var respuesta = "";
            respuesta = respuesta + "<center><div class='form-group'>";
            respuesta = respuesta + " <a title='Editar' ng-click='vm.EditarCliente(\"" + NumeroFila + "\");'>" + "<span class='glyphicon fa fa-pencil' style='color: #3949ab; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + " <a title='Eliminar' ng-click='vm.EliminarCliente(\"" + NumeroFila + "\");'>" + "<span class='glyphicon fa fa-trash-o' style='color:#3949ab; margin-left: 1px;'></span></a>";
            respuesta = respuesta + "</div></center>";
            return respuesta;
        };//

        /***********************Carga Incial de Cliente *************************/
        function BuscarCliente() {
            blockUI.start();
            vm.selected = {};
            LimpiarGrillaCliente();
            $timeout(function () {
                vm.dtOptionsCliente = DTOptionsBuilder
                     .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withFnServerData(BuscarClientePaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)

                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })

                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarClientePaginado(sSource, aoData, fnCallback, oSettings) {

            var draw = aoData[0].value;
            var start = aoData[3].value;
            var size = aoData[4].value;
            var pageNumber = (start + size) / size;

            var columnNumber = aoData[2].value[0].column;
            var orderType = aoData[2].value[0].dir;
            var columnName = aoData[1].value[columnNumber].data

            vm.search.Indice = pageNumber;
            vm.search.Tamanio = size;
            vm.search.ColumnName = (columnName == null) ? "IdCliente" : columnName;
            vm.search.OrderType = orderType;
            vm.search.IdEstado = 1;

            var cliente = vm.search;
            vm.selected = {};

            var promise = ClienteService.ListarClientePaginado(cliente);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListCliente
                };
                blockUI.stop();
                fnCallback(records);
                $('#tablaCliente').find('tbody td:last').css('width', '5%');
            }, function (response) {
                blockUI.stop();
                LimpiarGrillaCliente();
            });
            $("#tbCliente").removeClass("dtr-inline");
        };

        function LimpiarGrillaCliente() {
            vm.dtOptionsCliente = DTOptionsBuilder
            .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('destroy', true)
                .withOption('scrollCollapse', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('scrollX', true)
                .withOption('paging', false);

        };

        function LimpiarFiltros() {
            vm.search = {};
            vm.search.IdTipoEntidad = '-1';
            vm.search.IdTipoIdentificadorFiscalTm = '-1';
            vm.search.IdSector = '-1';
            vm.search.IdDireccionComercial = '-1';
        }

        /****************************** Combos Box *********************************/
        function CargarTipoEntidad() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 168
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaTipoEntidad = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarTipoIdentificadorFiscal() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 183
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaTipoIdentificadorFiscal = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarSector() {

            var promise = ClienteService.ListarComboSector();

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaSector = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarDireccionComercial() {
            var maestra = {
                IdEstado: 1
            };
            var promise = ClienteService.ListarDireccionComercial(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaDireccionComercial = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        /****************************** Mantenimiento *********************************/
        function NuevoCliente() {
            vm.LimpiarFormulario();
            $("#titleModal").val("NUEVO");
            $('#ModalCliente').modal({
                keyboard: false
            });

        }

        function EditarCliente(NumeroFila) {
            let that = this;
            let cliente = that.dtInstanceCliente.dataTable.fnGetData(NumeroFila);

            vm.LimpiarFormulario();
            $("#titleModal").val("ACTUALIZAR");
            cliente.CodigoCliente = parseInt(cliente.CodigoCliente);
            cliente.NumeroIdentificadorFiscal = parseInt(cliente.NumeroIdentificadorFiscal);
            vm.cliente = cliente;
            vm.cliente.IdEstado = 1;

            $('#ModalCliente').modal({
                keyboard: false
            });

        }

        function EliminarCliente(NumeroFila) {
            let that = this;
            let cliente = that.dtInstanceCliente.dataTable.fnGetData(NumeroFila);

            vm.delete = cliente;
            vm.delete.IdEstado = 0;

            $('#ModalEliminiarCliente').modal({
                keyboard: false
            });
        }

        function LimpiarFormulario() {
            vm.cliente = {};
            vm.cliente.IdCliente = 0;
            vm.cliente.IdTipoEntidad = '-1';
            vm.cliente.IdTipoIdentificadorFiscalTm = '-1';
            vm.cliente.IdSector = '-1';
            vm.cliente.IdDireccionComercial = '-1';
        }

        function GuardarCliente(cliente, divAlerta) {
            if ($scope.formularioCliente.$valid) {
                blockUI.start();
                var promise = null;

                if (cliente.IdCliente > 0) {
                    promise = ClienteService.ActualizarCliente(cliente);
                } else {
                    promise = ClienteService.RegistrarCliente(cliente);
                }
                 
                promise.then(
                    //Success:
                    function (response) {
                        blockUI.stop();
                        var Respuesta = response.data;

                        if (Respuesta.TipoRespuesta != 0) {
                            UtilsFactory.Alerta('#' + divAlerta, 'danger', Respuesta.Mensaje, 5);
                        } else {
                            UtilsFactory.Alerta('#' + divAlerta, 'success', Respuesta.Mensaje, 3);
                            vm.BuscarCliente();
                            setTimeout(function () { vm.CerrarPopup(); }, 3000);

                        }
                    },
                    //Error:
                    function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#' + divAlerta, 'danger', MensajesUI.DatosError, 5);
                    });
            }
        }

        /*********************** Otras funciones *************************/
        function CerrarPopup() {
            $('#ModalCliente').modal('hide');
            $('#ModalEliminiarCliente').modal('hide');
        };

        vm.BuscarCliente();
        vm.CargarTipoEntidad();
        vm.CargarTipoIdentificadorFiscal();
        vm.CargarSector();
        vm.CargarDireccionComercial();
        vm.search.IdTipoEntidad = '-1';
        vm.search.IdTipoIdentificadorFiscalTm = '-1';
        vm.search.IdSector = '-1';
        vm.search.IdDireccionComercial = '-1';
    }

})();