﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('ClienteService', ClienteService);

    ClienteService.$inject = ['$http', 'URLS'];

    function ClienteService($http, $urls) {

        var service = {

            ObtenerCliente: ObtenerCliente,
            ListarClientePorDescripcion: ListarClientePorDescripcion,
            ListarCliente: ListarCliente,
            ActualizarClienteModalPreventa: ActualizarClienteModalPreventa,
            ListarClientePaginado: ListarClientePaginado,
            ObtenerClientePorCodigo: ObtenerClientePorCodigo,
            ListarClienteCartaFianza: ListarClienteCartaFianza,
            ListarClientePorDescripcion: ListarClientePorDescripcion,
            ListarDireccionComercial: ListarDireccionComercial,
            ListarComboSector: ListarComboSector,
            ActualizarCliente: ActualizarCliente,
            RegistrarCliente: RegistrarCliente
        };

        return service;


        function ObtenerCliente(cliente) {

            return $http({
                url: $urls.ApiComun + "Cliente/ObtenerCliente",
                method: "POST",
                data: JSON.stringify(cliente)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarClientePorDescripcion(cliente) {

            return $http({
                url: $urls.ApiComun + "Cliente/ListarClientePorDescripcion",
                method: "POST",
                data: JSON.stringify(cliente)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarClienteCartaFianza(cliente) {

            return $http({
                url: $urls.ApiComun + "Cliente/ListarClienteCartaFianza",
                method: "POST",
                data: JSON.stringify(cliente)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarCliente(cliente) {

            return $http({
                url: $urls.ApiComun + "Cliente/ListarCliente",
                method: "POST",
                data: JSON.stringify(cliente)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarClienteModalPreventa(cliente) {
            debugger;
            return $http({
                url: $urls.ApiComun + "Cliente/ActualizarClienteModalPreventa",
                method: "POST",
                data: JSON.stringify(cliente)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarClientePaginado(cliente) {

            return $http({
                url: $urls.ApiComun + "Cliente/ListarClientePaginado",
                method: "POST",
                data: JSON.stringify(cliente)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerClientePorCodigo(cliente) {

            return $http({
                url: $urls.ApiComun + "Cliente/ObtenerClientePorCodigo",
                method: "POST",
                data: JSON.stringify(cliente)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarDireccionComercial(direccionComercial) {

            return $http({
                url: $urls.ApiComun + "Cliente/ListarDireccionComercial",
                method: "POST",
                data: JSON.stringify(direccionComercial)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarComboSector() {

            return $http({
                url: $urls.ApiComun + "Cliente/ListarComboSector",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarCliente(cliente) {

            return $http({
                url: $urls.ApiComun + "Cliente/ActualizarCliente",
                method: "POST",
                data: JSON.stringify(cliente)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarCliente(cliente) {

            return $http({
                url: $urls.ApiComun + "Cliente/RegistrarCliente",
                method: "POST",
                data: JSON.stringify(cliente)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();