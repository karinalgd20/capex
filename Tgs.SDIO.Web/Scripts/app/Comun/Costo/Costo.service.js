﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('CostoService', CostoService);

    CostoService.$inject = ['$http', 'URLS'];

    function CostoService($http, $urls) {
        var service = {
            ListarCostos: ListarCostos,
            ListarCostoPorMedioServicioGrupo: ListarCostoPorMedioServicioGrupo,
            ListarCosto: ListarCosto,
            ObtenerCostoPorCodigo: ObtenerCostoPorCodigo,
            ObtenerCosto:ObtenerCosto
        };

        return service;

         function ObtenerCosto(dto) {
            return $http({
                url: $urls.ApiComun + "Costo/ObtenerCosto",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ObtenerCostoPorCodigo(dto) {
            return $http({
                url: $urls.ApiComun + "Costo/ObtenerCostoPorCodigo",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarCosto(dto) {
            return $http({
                url: $urls.ApiComun + "Costo/ListarCosto",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListarCostos(dto) {
            return $http({
                url: $urls.ApiComun + "Costo/ListarCostos",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarCostoPorMedioServicioGrupo(dto) {
            return $http({
                url: $urls.ApiComun + "Costo/ListarCostoPorMedioServicioGrupo",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();