﻿(function () {
    'use strict'

    angular
    .module('app.Comun')
    .controller('RegistrarServicioController', RegistrarServicioController);

    RegistrarServicioController.$inject = ['RegistrarServicioControllerService', 'BandejaServicioService', 'MedioService', 'LineaNegocioService', 'SubServicioService', 'ServicioSubServicioService', 'ProveedorService', 'MaestraService', 'ServicioService', 'DepreciacionService'
        , 'ServicioCMIService', 'ServicioGrupoService'
        , 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];


    function RegistrarServicioController(RegistrarServicioControllerService, BandejaServicioService, MedioService, LineaNegocioService, SubServicioService, ServicioSubServicioService, ProveedorService, MaestraService, ServicioService, DepreciacionService,
        ServicioCMIService, ServicioGrupoService
       , blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

          vm.AccionGrabar =true;

        vm.BuscarSubServicio = BuscarSubServicio;
        vm.EliminarSubServicio = EliminarSubServicio;
        vm.CargaModalServicio = CargaModalServicio;

        /**/
        
        vm.ListarMaestraPorIdRelacion = ListarMaestraPorIdRelacion;
        vm.ListarDepreciacion = ListarDepreciacion;
        vm.RegistrarServicio = RegistrarServicio;
        vm.RegistrarSubServicioPorServicio = RegistrarSubServicioPorServicio;
        vm.ActualizarServicio = ActualizarServicio;
        vm.ObtenerServicio = ObtenerServicio;
        vm.ObtenerSubServicio = ObtenerSubServicio;
        vm.ListarLineaNegocios=ListarLineaNegocios;



        vm.ListarPestanaGrupoServicio = ListarPestanaGrupoServicio;
        vm.ListGrupoServicio = [];
        vm.IdGrupoServicio = "-1";

        vm.ListarPestanaGrupoSubServicio = ListarPestanaGrupoSubServicio;
        vm.ListGrupoSubServicio = [];
        vm.IdGrupoSubServicio = "-1";

        vm.ListarLineaNegocios = ListarLineaNegocios;
        vm.ListLineaNegocio = [];
        vm.IdLineaNegocio = "-1";


        vm.ListarEnlace = ListarEnlace;
        vm.ListEnlace = [];
        vm.IdEnlace = "-1";

        vm.ListarLocalidad = ListarLocalidad;
        vm.ListLocalidad = [];
        vm.IdLocalidad = "-1";

        vm.ListarMoneda = ListarMoneda;
        vm.ListMoneda = [];
        vm.IdMoneda = "-1";


        vm.ListarEstado = ListarEstado;
        vm.ListEstado = [];
        vm.IdEstado = "-1";

        vm.ListarProveedor = ListarProveedor;
        vm.ListProveedor = [];
        vm.IdProveedor = "-1";

        vm.ListarMedio = ListarMedio;
        vm.ListMedio = [];
        vm.IdMedio = "-1";


        vm.ListarTipoCosto = ListarTipoCosto;
        vm.ListTipoCosto = [];
        vm.IdTipoCosto = "-1";

        vm.ListarPeriodo = ListarPeriodo;
        vm.ListPeriodo = [];
        vm.IdPeriodo = "-1";

        vm.ListarServicioCMI = ListarServicioCMI;
        vm.ListServicioCMI = [];
        vm.IdServicioCMI = "-1";

  
        vm.IdLineaNegocioBandeja = "-1";



        vm.ListSubServicio = [];

        vm.IdSisego = "-1";
        vm.IdServicioSubServicio = 0;
        vm.IdServicioSubServicioPorservicio = 0;
        vm.ListTipoServicio = [];
        vm.ListDepreciacion = [];

   
        vm.DescripcionNuevo = "";
        vm.CostoInstalacion = "";
        vm.DescripcionContratoMarco = "";


        vm.Ponderado = 100;
        vm.Inicio = 1;
        vm.ContratoMarco = "";


        vm.IdtipoServicio = "-1";
        vm.IdDepreciacion = "-1";
        vm.IdtipoServicio = "-1";

        vm.IdServicio = 0;
        vm.IdSubServicio = "-1";


        vm.Descripcion = '';

        vm.MensajeDeAlerta = "";
       
    
        LimpiarGrillaSub();

        ListarLineaNegocios();
        


        vm.ListarEstadoServicio = ListarEstadoServicio;
        vm.ListEstadoServicio = [];
        vm.IdEstadoServicio = "-1";


        vm.MensajeDeAlerta = "";

        vm.IdServicio = 0;
        vm.ListarServicioGrupo = ListarServicioGrupo;
        vm.ListaServiciosGrupo = [];
        vm.IdListaServicioGrupo  = "-1";
        CargaModal();
        function CargaModal() {
            vm.IdServicio = $scope.$parent.vm.IdServicio;
            (vm.IdServicio > 0) ? ObtenerServicio(vm.IdServicio) : CargaModalServicio();


        }

        /**/



        vm.dtInstanceServicio = {};

        vm.dtColumnsServicio = [

        DTColumnBuilder.newColumn('DescripcionSubServicio').withTitle('Componente').notSortable(),
        DTColumnBuilder.newColumn('DescripcionTipoCosto').withTitle('Tipo Costo').notSortable(),
        //DTColumnBuilder.newColumn('DescripcionMoneda').withTitle('DescripcionMoneda').notSortable(),
        //DTColumnBuilder.newColumn('DescripcionPeriodo').withTitle('DescripcionPeriodo').notSortable(),
        DTColumnBuilder.newColumn('DescripcionGrupo').withTitle('Grupo').notSortable(),
        //DTColumnBuilder.newColumn('DescripcionProveedor').withTitle('Proveedor').notSortable(),
        //DTColumnBuilder.newColumn('DescripcionFlagSISEGO').withTitle('SISEGO').notSortable(),
        //DTColumnBuilder.newColumn('ContratoMarco').withTitle('Contrato Marco').notSortable(),
        //DTColumnBuilder.newColumn('Inicio').withTitle('Inicio').notSortable(),
        //DTColumnBuilder.newColumn('Ponderacion').withTitle('Ponderacion').notSortable(),



         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaSubServicio)

        ];

        function AccionesBusquedaSubServicio(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn '  ng-click='vm.ObtenerSubServicio(" + data.IdServicioSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> " +
       "<div class='col-xs-4 col-sm-1'><a title='Eliminar' class='btn btn-sm'  id='tab-button' class='btn '  onclick='EliminarSubServicio(" + data.IdServicioSubServicio + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";

        };


        function ObtenerSubServicio(IdServicioSubServicio) {


            blockUI.start();
            var subServicio = {
                IdServicioSubServicio: IdServicioSubServicio,
                IdEstado: 1
            };
            var promise = ServicioSubServicioService.ObtenerServicioSubServicio(subServicio);

            promise.then(function (resultado) {
                blockUI.stop();
                debugger
                var Respuesta = resultado.data;

                ListarPestanaGrupoSubServicio();
                if (Respuesta.IdGrupo == null) {
                    Respuesta.IdGrupo = "-1";
                }
                if (Respuesta.IdSubServicio == null) {
                    Respuesta.IdSubServicio = "-1";
                }
                if (Respuesta.FlagSISEGO == null) {
                    Respuesta.FlagSISEGO = "-1";
                }
                if (Respuesta.IdTipoCosto == null) {
                    Respuesta.IdTipoCosto = "-1";
                }
               
                vm.IdServicioSubServicio = IdServicioSubServicio;

                vm.IdGrupoSubServicio = Respuesta.IdGrupo;
                vm.IdSubServicio = Respuesta.IdSubServicio;
                //   Respuesta.IdLineaNegocio
                vm.IdSisego = Respuesta.FlagSISEGO;
                vm.IdTipoCosto = Respuesta.IdTipoCosto;

                vm.IdProveedor = Respuesta.IdProveedor;
                vm.ContratoMarco = Respuesta.ContratoMarco;

                vm.IdPeriodo = Respuesta.IdPeriodos;
                vm.Inicio = Respuesta.Inicio;

                vm.Ponderado = Respuesta.Ponderacion;
                vm.IdMoneda = Respuesta.IdMoneda;




            }, function (response) {
                blockUI.stop();

            });
        };

        function BuscarSubServicio() {

            blockUI.start();
            LimpiarGrillaSub()
            $timeout(function () {
                vm.dtOptionsServicio = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarSubServicioPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        };

        function BuscarSubServicioPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var Servicio = {
                IdServicio: vm.IdServicio,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ServicioSubServicioService.ListaServicioSubServicioPaginado(Servicio);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListServicioSubServicioDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaSub()
            });
        };



    function LimpiarGrillaSub() {


        vm.dtOptionsServicio = DTOptionsBuilder
       .newOptions()
       .withOption('data', [])
       .withOption('bFilter', false)
       .withOption('responsive', false)
       .withOption('destroy', true)
       .withOption('order', [])
       .withDisplayLength(0)
       .withOption('paging', false);
    };
        //

    function ListarServicioGrupo() {

        var grupo = {};

        var promise = ServicioGrupoService.ListarServicioGrupo(grupo);
        promise.then(function (resultado) {
            blockUI.stop();

            var Respuesta = resultado.data;
            vm.ListaServiciosGrupo = UtilsFactory.AgregarItemSelect(Respuesta);
        }, function (response) {
            blockUI.stop();
        });
    }
    function ListarEstadoServicio() {

        var maestra = {
            IdEstado: 1,
            IdRelacion: 1

        };
        var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

        promise.then(function (resultado) {
            blockUI.stop();

            var Respuesta = resultado.data;


            vm.ListEstadoServicio = UtilsFactory.AgregarItemSelect(Respuesta);

        }, function (response) {
            blockUI.stop();
        });
    }
    function LimpiarModalServicioSubServicio() {

        vm.ContratoMarco = "";
        vm.IdSubServicio = "-1";
        vm.IdGrupoSubServicio = "-1";
        vm.IdSisego = "-1";
        vm.IdMoneda = "-1";
        vm.IdProveedor = "-1";
        vm.IdTipoCosto = "-1";
        vm.IdPeriodo = "-1";
        vm.Inicio = 1;
        vm.Ponderado = 100;
        vm.IdServicioSubServicio = 0;


    }

    function CargaModalServicio() {

        vm.AccionGrabar = true;
        vm.Servicio = "";
        vm.IdLineaNegocio = "-1";

        vm.DescripcionNuevo = "";
        vm.IdSubServicio = "-1";
        vm.ListGrupoServicio = [];
        vm.IdGrupoServicio = "-1";

        vm.ListGrupoSubServicio = [];
        vm.IdGrupoSubServicio = "-1";

        vm.ListEnlace = [];
        vm.IdEnlace = "-1";

        vm.ListLocalidad = [];
        vm.IdLocalidad = "-1";

        vm.ListMoneda = [];
        vm.IdMoneda = "-1";

        vm.ListEstado = [];
        vm.IdEstado = "-1";

        vm.ListEstadoServicio = [];
        vm.IdEstadoServicio = "-1";

        vm.ListProveedor = [];
        vm.IdProveedor = "-1";


        vm.ListMedio = [];
        vm.IdMedio = "-1";

        vm.ListTipoCosto = [];
        vm.IdTipoCosto = "-1";

        vm.ListPeriodo = [];
        vm.IdPeriodo = "-1";

        vm.ListSubServicio = [];
        vm.IdServicio = 0;
        vm.IdServicioSubServicio = 0;
        vm.IdServicioSubServicioPorservicio = 0;
        ListarSubServicios();
        ListarEnlace();
        ListarMedio();
        ListarProveedor();
        ListarLocalidad();
        ListarMoneda();
        ListarEstado();
        ListarEstadoServicio();
        ListarPeriodo();
        ListarTipoCosto();
        ListarPestanaGrupoServicio();
        ListarPestanaGrupoSubServicio();
        ListarServicioCMI();
        BuscarSubServicio();
        ListarLineaNegocios();
        ListarServicioGrupo();

    }
    function EliminarSubServicio(IdServicioSubServicio) {

        var subServicio = {
            IdServicioSubServicio: IdServicioSubServicio
        }
        if (confirm('¿Estas seguro que desea eliminar?')) {
        debugger
        var promise = ServicioSubServicioService.EliminarServicioSubServicio(subServicio);

        promise.then(function (resultado) {
            blockUI.stop();

            UtilsFactory.Alerta('#divAlert_2', 'success', "Se elimino  el componente con exito.", 5);
            BuscarSubServicio();
        }, function (response) {

            blockUI.stop();
        });
        }
        else {
            blockUI.stop();
        }
    }
          function ListarSubServicios() {

            var subServicio = {
                IdEstado: 1

            };
            var promise = SubServicioService.ListarSubServicios(subServicio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListSubServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }


        
        function ListarProveedor() {

            var proveedor = {
                IdEstado: 1

            };
            var promise = ProveedorService.ListarProveedor(proveedor);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListProveedor = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarMedio() {
            debugger
            var medio = {
                IdEstado: 1

            };
            var promise = MedioService.ListarMedio(medio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListMedio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarPestanaGrupoServicio() {

            var pestanaGrupo = {
                IdEstado: 1,
                FlagServicio: 1
            };
            debugger
            var promise = BandejaServicioService.ListarPestanaGrupo(pestanaGrupo);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListGrupoServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function ListarPestanaGrupoSubServicio() {

            var pestanaGrupo = {
                IdEstado: 1,
                FlagServicio: 0
            };
            debugger
            var promise = BandejaServicioService.ListarPestanaGrupo(pestanaGrupo);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListGrupoSubServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarServicioCMI() {

            var servicioCMI = {
                IdEstado: 1,
                IdLineaNegocio: 5

            };
            var promise = ServicioCMIService.ListarServicioCMIPorLineaNegocio(servicioCMI);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListServicioCMI = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }


        function ListarMaestraPorIdRelacion() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 14

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListTipoServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function ListarEnlace() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 89

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListEnlace = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function ListarLocalidad() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 95
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListLocalidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        function ListarMoneda() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 98
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListMoneda = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarEstado() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 92
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListEstado = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        function ListarTipoCosto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 153
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListTipoCosto = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        function ListarPeriodo() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 28
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListPeriodo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }


        function RegistrarServicio() {
            blockUI.start();
            var IdEstadoServicio = vm.IdEstadoServicio;
            if (IdEstadoServicio =="-1") {
                IdEstadoServicio = 0;
            }



            var estado = vm.IdSevicio;

            var servicio = {
                IdServicio: vm.IdServicio,
                IdLineaNegocio: vm.IdLineaNegocio,
                Descripcion: vm.Servicio,
                IdGrupo: vm.IdGrupoServicio,
                IdMedio: vm.IdMedio,
                IdTipoEnlace: vm.IdEnlace,
               // IdPeriodo: vm.IdPeriodo,
                IdServicioCMI: vm.IdServicioCMI,
                IdEstado: IdEstadoServicio,
                IdServicioGrupo: vm.IdListaServicioGrupo
            };

            var promise = (vm.IdServicio == 0) ? ServicioService.RegistrarServicio(servicio) : ServicioService.ActualizarServicio(servicio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                debugger
                vm.IdServicio = Respuesta.Id;


                RegistrarServicioDetalle();

            }, function (response) {
                blockUI.stop();

            });
        }
           function RegistrarServicioDetalle() {
            blockUI.start();
       
            var serviciosubservicio = {

                IdServicioSubServicio: vm.IdServicioSubServicioPorservicio,
                IdServicio: vm.IdServicio,
                IdGrupo: vm.IdGrupoServicio

            };

            var promise = (vm.IdServicioSubServicioPorservicio > 0) ? ServicioSubServicioService.ActualizarServicioSubServicio(serviciosubservicio) : ServicioSubServicioService.RegistrarServicioSubServicio(serviciosubservicio);

            promise.then(function (resultado) {
                blockUI.stop();
                debugger
                var Respuesta = resultado.data;
                vm.AccionGrabar = false;
                UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);

                BuscarSubServicio();
                //BuscarServicio();
            }, function (response) {
                blockUI.stop();

            });
        }





           function RegistrarSubServicioPorServicio() {

            blockUI.start();

            var confirmacion = true;

            var IdProveedor = vm.IdProveedor;
            //var Ponderado = vm.Ponderado;
            if (vm.IdGrupoSubServicio=="-1") {

                confirmacion = false;
                UtilsFactory.Alerta('#divAlert_2', 'warning', "Seleccione grupo", 5);
            }
            if(vm.IdSubServicio=="-1"){

                confirmacion = false;
                 UtilsFactory.Alerta('#divAlert_2', 'warning', "Seleccione subServicio", 5);
            }
            //if(vm.IdSisego=="-1"){
            //    confirmacion = false;
            //    UtilsFactory.Alerta('#divAlert_2', 'warning', "Seleccione SiSego", 5);
            //}
            if (vm.IdTipoCosto == "-1") {
                confirmacion = false;
                UtilsFactory.Alerta('#divAlert_2', 'warning', "Seleccione tipo costo", 5);
            }
            //if (vm.IdPeriodo == "-1") {
            //    confirmacion = false;
            //    UtilsFactory.Alerta('#divAlert_2', 'warning', "Seleccione periodo", 5);
            //}
            if (vm.IdMoneda == "-1") {
                confirmacion = false;
                UtilsFactory.Alerta('#divAlert_2', 'warning', "Seleccione moneda", 5);
            }
            //if (vm.Ponderado == "") {
            //    confirmacion = false;
            //    UtilsFactory.Alerta('#divAlert_2', 'warning', "Ingrese Ponderado", 5);
            //}
            //if (vm.Inicio == "") {
            //    confirmacion = false;
            //    UtilsFactory.Alerta('#divAlert_2', 'warning', "Ingrese Inicio", 5);
            //}
            //if (IdProveedor == "-1") {

            //    IdProveedor = null;
            //}
            //if (Ponderado == "-1") {
            //    Ponderado = 0;
            //}

            if (confirmacion) {
                var serviciosubservicio = {

                    IdServicioSubServicio: vm.IdServicioSubServicio,
                    IdServicio: vm.IdServicio,
                    IdSubServicio: vm.IdSubServicio,
                    IdTipoCosto: vm.IdTipoCosto,
                    //IdProveedor: IdProveedor,
                   // ContratoMarco: vm.ContratoMarco,
                   // IdPeriodos: vm.IdPeriodo,
                   // Inicio: vm.Inicio,
                   // Ponderacion: Ponderado,
                    IdGrupo: vm.IdGrupoSubServicio,
                    IdMoneda: vm.IdMoneda
                    //,
                   // FlagSISEGO: vm.IdSisego



                };

                var promise = (vm.IdServicioSubServicio > 0) ? ServicioSubServicioService.ActualizarServicioSubServicio(serviciosubservicio) : ServicioSubServicioService.RegistrarServicioSubServicio(serviciosubservicio);

                promise.then(function (resultado) {
                    blockUI.stop();
                    debugger
                    var Respuesta = resultado.data;

                    //   vm.IdServicio = Respuesta.IdServicio;
                    LimpiarModalServicioSubServicio();

                    UtilsFactory.Alerta('#divAlert_2', 'success', Respuesta.Mensaje, 5);


                    BuscarSubServicio();
                }, function (response) {
                    blockUI.stop();

                });
            }
            else {
                blockUI.stop();
                 BuscarSubServicio();
            }
        }

        
        function ObtenerServicio(IdServicio) {
            blockUI.start();
            var Servicio = {
                IdServicio: IdServicio

            };
            var promise = ServicioService.ObtenerServicio(Servicio);

            promise.then(function (resultado) {
                blockUI.stop();
                debugger
                var Respuesta = resultado.data;
                CargaModalServicio();

                if (Respuesta.IdGrupo == null) {
                    Respuesta.IdGrupo = "-1";
                }
                if (Respuesta.IdMedio == null) {
                    Respuesta.IdMedio = "-1";
                }
                if (Respuesta.IdTipoEnlace == null) {
                    Respuesta.IdTipoEnlace = "-1";
                }
                if (Respuesta.IdServicioCMI == null) {
                    Respuesta.IdServicioCMI = "-1";
                }
                if (Respuesta.IdLineaNegocio == null) {
                    Respuesta.IdLineaNegocio = "-1";
                }
                if (Respuesta.IdServicioGrupo == null) {
                    Respuesta.IdServicioGrupo = "-1";
                }
                vm.IdServicioSubServicioPorservicio = Respuesta.IdServicioSubServicio;
                vm.IdServicio = Respuesta.IdServicio;
                vm.Servicio = Respuesta.Descripcion;
                vm.IdGrupoServicio = Respuesta.IdGrupo.toString();
                vm.IdLineaNegocio = Respuesta.IdLineaNegocio;
                vm.IdEstadoServicio = Respuesta.IdEstado.toString();
                //   Respuesta.IdLineaNegocio
                vm.IdMedio = Respuesta.IdMedio;
                vm.IdEnlace = Respuesta.IdTipoEnlace;
                vm.IdServicioCMI = Respuesta.IdServicioCMI;
                vm.IdListaServicioGrupo = Respuesta.IdServicioGrupo;
                vm.AccionGrabar = false;
                BuscarSubServicio();

            }, function (response) {
                blockUI.stop();

            });
        }

        function ActualizarServicio() {

            var Servicio = {
                IdServicio: vm.IdServicio,
                CostoInstalacion: vm.CostoInstalacion,
                Descripcion: vm.DescripcionNuevo,
                IdTipoServicio: vm.IdtipoServicio,
                IdDepreciacion: vm.IdDepreciacion,
                IdListaServicioGrupo: vm.IdListaServicioGrupo
            };
            var promise = ServicioService.ActualizarServicio(Servicio);

            promise.then(function (resultado) {
                blockUI.stop();


                var Respuesta = resultado.data;
                vm.IdServicio = Respuesta.Id,
                UtilsFactory.Alerta('#divAlert', 'success', "Se actualizo el servicio con exito.", 5);
                debugger
                //BuscarServicio();
                BuscarSubServicio();

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarDepreciacion() {

            var depreciacion = {
                IdEstado: 1

            };
            var promise = DepreciacionService.ListarDepreciacion(depreciacion);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListDepreciacion = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function LimpiarModal() {
            var ddlEstado = angular.element(document.getElementById("ddlEstado"));
            vm.txtDescripcion = '';
            vm.txtOrdenVisual = '';
            vm.btnActualizar = false;
            vm.btnAgregar = true;
        }
        function ListarLineaNegocios() {

            var lineaNegocio = {
                IdEstado: 1

            };
            var promise = LineaNegocioService.ListarLineaNegocios(lineaNegocio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }


    }
})();




