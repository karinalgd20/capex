﻿(function () {
	'use strict',
     angular
    .module('app.Comun')
    .service('RegistrarServicioControllerService', RegistrarServicioControllerService);

	RegistrarServicioControllerService.$inject = ['$http', 'URLS'];

	function RegistrarServicioControllerService($http, $urls) {

	  var service = {
      
            ModalServicio: ModalServicio
        };

        return service;

        function ModalServicio(servicio) {

            return $http({
                url: $urls.ApiComun + "RegistrarServicio/Index",
                method: "POST",
                data: JSON.stringify(servicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

	}

})();