﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('AreaSeguimientoService', AreaSeguimientoService);

    AreaSeguimientoService.$inject = ['$http', 'URLS'];

    function AreaSeguimientoService($http, $urls) {

        var service = {
            ListarComboAreas: ListarComboAreas,
            ObtenerAreaSeguimientoPorId: ObtenerAreaSeguimientoPorId,
            ActualizarAreaSeguimiento: ActualizarAreaSeguimiento,
            RegistrarAreaSeguimiento: RegistrarAreaSeguimiento
        };

        return service;

     function ListarComboAreas() {
            return $http({
                url: $urls.ApiComun + "AreaSeguimiento/ListarComboAreas",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
     };

     function ObtenerAreaSeguimientoPorId(areaseguimiento) {
            return $http({
                url: $urls.ApiComun + "AreaSeguimiento/ObtenerAreaSeguimientoPorId",
                method: "POST",
                data: JSON.stringify(areaseguimiento)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

     function ActualizarAreaSeguimiento(areaseguimiento) {
            return $http({
                url: $urls.ApiComun + "AreaSeguimiento/ActualizarAreaSeguimiento",
                method: "POST",
                data: JSON.stringify(areaseguimiento)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

     function RegistrarAreaSeguimiento(areaseguimiento) {
            return $http({
                url: $urls.ApiComun + "AreaSeguimiento/RegistrarAreaSeguimiento",
                method: "POST",
                data: JSON.stringify(areaseguimiento)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();