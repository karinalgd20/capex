﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('CorreoService', CorreoService);

    CorreoService.$inject = ['$http', 'URLS'];

    function CorreoService($http, $urls) {

        var service = {
            ObtenerDatosCorreo: ObtenerDatosCorreo,
            ObtenerDatosCorreoGC: ObtenerDatosCorreoGC,
            EnviarCorreoCartaFianza: EnviarCorreoCartaFianza
        };

        return service;


        function ObtenerDatosCorreo(cartaFianza) {

            return $http({
                url: $urls.ApiComun + "Correo/ObtenerDatosCorreo",
                method: "POST",
                data: JSON.stringify(cartaFianza)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerDatosCorreoGC(cartaFianza) {

            return $http({
                url: $urls.ApiComun + "Correo/ObtenerDatosCorreoGC",
                method: "POST",
                data: JSON.stringify(cartaFianza)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EnviarCorreoCartaFianza(cartaFianza) {

            return $http({
                url: $urls.ApiComun + "Correo/EnviarCorreoCartaFianza",
                method: "POST",
                data: JSON.stringify(cartaFianza)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();