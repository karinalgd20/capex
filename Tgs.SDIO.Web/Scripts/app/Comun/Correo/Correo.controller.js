﻿(function () {
    'use strict'
    angular
        .module('app.Comun')
        .controller('CorreoController', CorreoController);

    CorreoController.$inject =
        [
            'CorreoService',
            'ArchivoService',
            'MaestroCartaFianzaService',
            'blockUI',
            'UtilsFactory',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile'
        ];

    function CorreoController
        (
        CorreoService,
        ArchivoService,
        MaestroCartaFianzaService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile
        ) {

        var vm = this;

        vm.CerrarPopup = CerrarPopup;
        vm.Init = Init;

        vm.DescartarCorreoCartaFianza = DescartarCorreoCartaFianza;
        vm.LimpiarFormularioCorreo = LimpiarFormularioCorreo;
        vm.EnviarCorreoCartaFianza = EnviarCorreoCartaFianza;
        vm.SubirArchivosAdjuntos = SubirArchivosAdjuntos;

        vm.correo = {};

        function CerrarPopup() {
            $('#ModalEnviarCorreoCartaFianza').modal('hide');
        };

        /****************************** Envio de Correo *********************************/

        function EnviarCorreoCartaFianza(datosCorreo) {
            if (datosCorreo.To && datosCorreo.To != null && datosCorreo.To != '') {
                blockUI.start();
                datosCorreo.Body = tinymce.get('Body').getContent();
                var promiseArchivo = SubirArchivosAdjuntos();

                promiseArchivo.then(
                    //Success:
                    function (response) {
                        var responseArchivo = response.data;

                        if (responseArchivo[0].Id > 0 && responseArchivo[0].Detalle != '') {

                            if (responseArchivo[0].TipoRespuesta == 0) {
                                datosCorreo.Attachment = responseArchivo.map((archivo) => {
                                    return archivo.Detalle;
                                });

                            } else {
                                blockUI.stop();
                                UtilsFactory.Alerta('#divAlertEnviarCorreoCartaFianza', 'danger', responseArchivo[0].Mensaje, 5);
                            }
                        }

                        var promise = CorreoService.EnviarCorreoCartaFianza(datosCorreo);

                        promise.then(
                            //Success:
                            function (response) {
                                blockUI.stop();
                                var Respuesta = response.data;
                                if (Respuesta.TipoRespuesta != 0) {
                                    UtilsFactory.Alerta('#divAlertEnviarCorreoCartaFianza', 'danger', Respuesta.Mensaje + responseArchivo.Mensaje, 20);

                                } else {
                                    UtilsFactory.Alerta('#divAlertEnviarCorreoCartaFianza', 'success', Respuesta.Mensaje, 5);


                                    if ((vm.correo.IdCartaFianza != null || vm.correo.IdCartaFianza != '') && (vm.correo.IdTipoAccionTm == 1 || vm.correo.IdTipoAccionTm == 2)) {
                                        $scope.$parent.vm.BuscarCartaFianza();
                                        $scope.$parent.vm.CerrarPopup();
                                    } else if ((vm.correo.IdCartaFianza != null || vm.correo.IdCartaFianza != '') && (vm.correo.IdTipoAccionTm == 3)) {
                                        $scope.$parent.vm.BuscarCartaFianzaRenovacion();
                                        $scope.$parent.vm.CerrarPopup();
                                    }
                                    setTimeout(function () { vm.CerrarPopup(); }, 5000);

                                }
                            },
                            //Error:
                            function (response) {
                                blockUI.stop();
                                UtilsFactory.Alerta('#divAlertEnviarCorreoCartaFianza', 'danger', response.Mensaje, 5);
                            });

                    },
                    //Error:
                    function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlertEnviarCorreoCartaFianza', 'danger', response.Mensaje, 5);
                    });

            } else {
                UtilsFactory.Alerta('#divAlertEnviarCorreoCartaFianza', 'warning', 'Debe ingresar un destinatario', 5);
            }
        }

        function LimpiarFormularioCorreo() {
            vm.correo = {};
        }

        function DescartarCorreoCartaFianza() {
            vm.LimpiarFormularioCorreo();
            vm.CerrarPopup();
        }

        /****************************** Subir archivo *********************************/
        function SubirArchivosAdjuntos() {
            vm.correo.Attachments = $("#Adjuntos").fileinput('getFileStack');
            if (!vm.correo.Attachments || vm.correo.Attachments.length == 0) {
                //UtilsFactory.Alerta('#divAlertEnviarCorreoCartaFianza', 'danger', 'Seleccione un archivo valido', 5);
                return new Promise((resolve) => {
                    let respuesta = { data: { 0: { Id: 0, Detalle: '' } } };
                    return resolve(respuesta);
                });
            }

            if (vm.correo.Attachments.length > 0) {
                let totalSize = 0;
                vm.correo.Attachments.forEach((file) => { totalSize += file.size });
                if (totalSize > 1048576) {
                    return new Promise(() => {
                        let respuesta = { Mensaje: 'El total de los archivos adjuntos supera el límite máximo permitido.' };
                        throw respuesta;
                    });
                } else {
                    var archivo = {
                        CodigoTabla: 101,
                        IdEntidad: vm.correo.IdCartaFianza,
                        Descripcion: vm.correo.Subject
                    };

                    var formData = new FormData();
                    formData.append('cargaArchivo', angular.toJson(archivo));
                    angular.forEach(vm.correo.Attachments, function (file) {
                        formData.append('files', file);
                    });

                    return ArchivoService.RegistrarArchivos(formData);
                }
            }
        }

        /******************************** Librerias Iniciales **********************************/
        function Init() {
            tinymce.init({
                selector: 'textarea.texto-enriquecido',
                plugins: 'textcolor',
                height: 300,
                menubar: false,
                toolbar: 'undo redo | formatselect | bold italic | forecolor | backcolor | alignleft aligncenter alignright alignjustify | outdent indent | removeformat ',
            });

            $('#Adjuntos').fileinput({
                theme: 'fa',
                language: 'es',
                uploadUrl: '#',
                browseClass: "btn btn-primary btn-lg",
                removeClass: "btn btn-danger",
                dropZoneEnabled: false,
                showUpload: false,
                showCancel: false,
                showCaption: false,
                maxFileCount: 2,
                maxFileSize: 1024,
                hideThumbnailContent: true,
                fileActionSettings: {
                    showRemove: true,
                    removeClass: "btn btn-danger",
                    showUpload: false,
                    showZoom: false,
                    showDrag: true
                },
            });

        }


    }
})();