﻿(function () {
    'use strict'
    angular
        .module('app.Comun')
        .controller('BandejaRiesgoProyecto', BandejaRiesgoProyecto);

    BandejaRiesgoProyecto.$inject =
        [
        'MaestraService',
        'RiesgoProyectoService',
        'RegistrarRiesgoProyectoService',
        'blockUI',
        'UtilsFactory',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        '$timeout',
        'MensajesUI',
        'URLS',
        '$scope',
        '$compile',
        '$modal',
        '$injector'
        ];

    function BandejaRiesgoProyecto
        (
        MaestraService,
        RiesgoProyectoService,
        RegistrarRiesgoProyectoService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile,
        $modal,
        $injector
        ) {

        var vm = this;
        vm.Id = 0;
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;
        vm.IdTipo = -1;
        vm.IdEstado = -1;
        vm.ListTipoRiesgoProyecto = [];
        vm.ListEstadoRiesgoProyecto = [];
        vm.dtColumns =
            [
            DTColumnBuilder.newColumn('Id').notVisible(),
            DTColumnBuilder.newColumn('DescripcionTipoRiesgo').withTitle('Tipo').notSortable(),
            DTColumnBuilder.newColumn('FechaDeteccion').withTitle('Fecha Detección').notSortable(),
            DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
            DTColumnBuilder.newColumn('Consecuencias').withTitle('Consecuencias').notSortable(),
            DTColumnBuilder.newColumn('DescripcionProbabilidad').withTitle('Probabilidad').notSortable(),
            DTColumnBuilder.newColumn('DescripcionImpacto').withTitle('Impacto').notSortable(),
            DTColumnBuilder.newColumn('Responsable').withTitle('Responsable').notSortable(),
            DTColumnBuilder.newColumn('PlanAccion').withTitle('Plan Acción').notSortable(),
            DTColumnBuilder.newColumn('Estado').withTitle('Estatus').notSortable(),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(Acciones)
            ];

        vm.ListarRiesgoProyecto = ListarRiesgoProyecto;
        vm.EliminarRiesgoProyecto = EliminarRiesgoProyecto;
        vm.ModalRiesgoProyecto = ModalRiesgoProyecto;

        ListarTipoRiesgoProyecto();
        ListarEstadoRiesgoProyecto();
        ListarRiesgoProyecto();
        LimpiarGrilla();

        function ListarTipoRiesgoProyecto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 313
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoRiesgoProyecto = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarEstadoRiesgoProyecto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 316
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListEstadoRiesgoProyecto = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', false);
        }

        function ListarRiesgoProyecto() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(ListarRiesgoProyectoPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('paging', true)
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }

        function ListarRiesgoProyectoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var riesgoProyecto = {
                IdOportunidad: vm.IdOportunidad,
                IdTipo: vm.IdTipo,
                IdEstado: vm.IdEstado,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = RiesgoProyectoService.ListarRiesgoProyectosPaginado(riesgoProyecto);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListRiesgoProyecto
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function Acciones(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'><a title='Editar' class='btn btn-sm' data-toggle='modal' data-target='#ModalRegistrarServicio' ng-click='vm.ModalRiesgoProyecto(" + data.Id + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div>" +
            "<div class='col-xs-4 col-sm-1'><a title='Eliminar' class='btn btn-sm' id='tab-button' ng-click='vm.EliminarRiesgoProyecto(" + data.Id + ");'>" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
        }

        function EliminarRiesgoProyecto(Id) {
            var riesgoProyecto = { Id: Id };
            blockUI.start();

            var promise = RiesgoProyectoService.EliminarRiesgoProyecto(riesgoProyecto);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                UtilsFactory.Alerta('#lblAlerta', 'success', Respuesta.Mensaje, 5);
                ListarRiesgoProyecto();

            }, function (response) {
                blockUI.stop();
            });
        }

        function ModalRiesgoProyecto(Id) {
            vm.Id = Id;
            var riesgoProyecto = {
                Id: Id
            };

            var promise = RegistrarRiesgoProyectoService.ModalRiesgoProyecto(riesgoProyecto);
            promise.then(function (response) {
                var respuesta = $(response.data);
                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoRiesgoProyecto").html(content);
                });

                $('#ModalRegistrarRiesgoProyecto').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });
        }
    }
})();