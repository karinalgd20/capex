﻿(function () {
    'use strict'
    angular
    .module('app.Comun')
    .controller('RegistrarRolRecurso', RegistrarRolRecurso);
    RegistrarRolRecurso.$inject = ['RegistrarRolRecursoService', 'RolRecursoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RegistrarRolRecurso(RegistrarRolRecursoService, RolRecursoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.RegistraRolRecurso = RegistraRolRecurso;
        vm.IdRol = "";
        vm.Descripcion = "";
        vm.Nivel = "";
        vm.IdEstado = "1";
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        vm.CargaModalRolRecurso = CargaModalRolRecurso;
        CargaModalRolRecurso();
        //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si es existente.
        function CargaModalRolRecurso() {
            vm.IdRol = $scope.$parent.vm.IdRol;
            if (vm.IdRol > 0) {
                ObtenerRolRecurso();
            } else {
                CargaModal();
            }
        }

        function ObtenerRolRecurso() {
            blockUI.start();
            var rol = {
                IdRol: vm.IdRol
            }
            var promise = RolRecursoService.ObtenerRolRecursoPorId(rol);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdRol = Respuesta.IdRol;
                vm.Descripcion = Respuesta.Descripcion;
                vm.Nivel = Respuesta.Nivel;
                vm.IdEstado = Respuesta.IdEstado;
            }, function (response) {
                blockUI.stop();

            });
        }

        function CargaModal() {
            vm.IdRol = "";
            vm.Descripcion = "";
            vm.Nivel = "";
            vm.IdEstado = "1";
        }
        //Registra en  la tabla [COMUN].[Recurso] 
        vm.RegistraRolRecurso = RegistraRolRecurso;
        function RegistraRolRecurso() {
            var rol = {
                IdRol: vm.IdRol,
                Descripcion: vm.Descripcion,
                Nivel: vm.Nivel,
                IdEstado: vm.IdEstado
            }
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promisePreventa = (vm.IdRol > 0) ? RolRecursoService.ActualizarRolRecurso(rol) : RolRecursoService.RegistrarRolRecurso(rol);

                promisePreventa.then(function (resultadoRol) {
                    var RespuestaRol = resultadoRol.data;
                    if (RespuestaRol.TipoRespuesta == 0) {
                        $scope.$parent.vm.BuscarRolRecurso();
                        $('#ModalRegistrarRolRecurso').modal('hide');
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar el rol de Recurso.", 5);
                    }

                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            } else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.Descripcion) == "") {
                mensaje = mensaje + "Ingrese Descripcion" + '<br/>';
                UtilsFactory.InputBorderColor('#txtDescripcion', 'Rojo');
            }
            if ($.trim(vm.Nivel) == "") {
                mensaje = mensaje + "Ingrese Nivel" + '<br/>';
                UtilsFactory.InputBorderColor('#txtNivel', 'Rojo');
            }
            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }
            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#txtDescripcion', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtNivel', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
        }
    }
})();