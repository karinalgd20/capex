﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('ServicioService', ServicioService);

    ServicioService.$inject = ['$http', 'URLS'];

    function ServicioService($http, $urls) {

        var service = {
            //Servicios
            ListarServicios: ListarServicios,
            ObtenerServicio: ObtenerServicio,
            RegistrarServicio: RegistrarServicio,
            ActualizarServicio: ActualizarServicio,
            ListaServicioPaginado: ListaServicioPaginado,
            InhabilitarServicio:InhabilitarServicio
        };

        return service;
        //Servicios


        function InhabilitarServicio(servicio) {

            return $http({
                url: $urls.ApiComun + "Servicio/InhabilitarServicio",
                method: "POST",
                data: JSON.stringify(servicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListarServicios(servicio) {

            return $http({
                url: $urls.ApiComun + "Servicio/ListarServicios",
                method: "POST",
                data: JSON.stringify(servicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ObtenerServicio(servicio) {

            return $http({
                url: $urls.ApiComun + "Servicio/ObtenerServicio",
                method: "POST",
                data: JSON.stringify(servicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function RegistrarServicio(servicio) {

            return $http({
                url: $urls.ApiComun + "Servicio/RegistrarServicio",
                method: "POST",
                data: JSON.stringify(servicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ActualizarServicio(servicio) {

            return $http({
                url: $urls.ApiComun + "Servicio/ActualizarServicio",
                method: "POST",
                data: JSON.stringify(servicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListaServicioPaginado(servicio) {

            return $http({
                url: $urls.ApiComun + "Servicio/ListaServicioPaginado",
                method: "POST",
                data: JSON.stringify(servicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();