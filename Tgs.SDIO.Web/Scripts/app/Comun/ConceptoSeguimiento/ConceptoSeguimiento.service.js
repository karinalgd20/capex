﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('ConceptoSeguimientoService', ConceptoSeguimientoService);

    ConceptoSeguimientoService.$inject = ['$http', 'URLS'];

    function ConceptoSeguimientoService($http, $urls) {

        var service = {
            ListarComboConceptoSeguimiento: ListarComboConceptoSeguimiento,
            ObtenerConceptoSeguimientoPorId: ObtenerConceptoSeguimientoPorId,
            ActualizarConceptoSeguimiento: ActualizarConceptoSeguimiento,
            RegistrarConceptoSeguimiento: RegistrarConceptoSeguimiento,
            ListarComboConceptoSeguimientoAgrupador: ListarComboConceptoSeguimientoAgrupador,
            ListarComboConceptoSeguimientoNiveles: ListarComboConceptoSeguimientoNiveles            
        };

        return service;

        function ListarComboConceptoSeguimiento() {
            return $http({
                url: $urls.ApiComun + "ConceptoSeguimiento/ListarComboConceptoSeguimiento",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarComboConceptoSeguimientoAgrupador() {
            return $http({
                url: $urls.ApiComun + "ConceptoSeguimiento/ListarComboConceptoSeguimientoAgrupador",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarComboConceptoSeguimientoNiveles() {
            return $http({
                url: $urls.ApiComun + "ConceptoSeguimiento/ListarComboConceptoSeguimientoNiveles",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerConceptoSeguimientoPorId(conceptoseguimiento) {
            return $http({
                url: $urls.ApiComun + "ConceptoSeguimiento/ObtenerConceptoSeguimientoPorId",
                method: "POST",
                data: JSON.stringify(conceptoseguimiento)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarConceptoSeguimiento(conceptoseguimiento) {
            return $http({
                url: $urls.ApiComun + "ConceptoSeguimiento/ActualizarConceptoSeguimiento",
                method: "POST",
                data: JSON.stringify(conceptoseguimiento)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarConceptoSeguimiento(conceptoseguimiento) {
            return $http({
                url: $urls.ApiComun + "ConceptoSeguimiento/RegistrarConceptoSeguimiento",
                method: "POST",
                data: JSON.stringify(conceptoseguimiento)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();