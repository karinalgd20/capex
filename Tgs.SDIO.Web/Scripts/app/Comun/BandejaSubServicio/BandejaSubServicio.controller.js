﻿(function () {
    'use strict'

    angular
    .module('app.Comun')
    .controller('BandejaSubServicio', BandejaSubServicio);

    BandejaSubServicio.$inject = ['BandejaSubServicioService', 'RegistrarSubServicioService', 'MaestraService', 'SubServicioService', 'DepreciacionService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function BandejaSubServicio(BandejaSubServicioService, RegistrarSubServicioService, MaestraService, SubServicioService, DepreciacionService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        /* Modal Registar */
        vm.ModalSubServicio = ModalSubServicio;

        function ModalSubServicio(IdSubServicio) {
            vm.IdSubServicio = IdSubServicio;
            debugger
            var subServicio = {
                IdSubServicio: IdSubServicio
            };

            var promise = RegistrarSubServicioService.ModalSubServicio(subServicio);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoSubServicio").html(content);
                });

                $('#ModalRegistrarServicio').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };


        /*--------------------------------------*/


        vm.BuscarSubServicio = BuscarSubServicio;
        vm.EliminarSubServicio = EliminarSubServicio;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.ListarMaestraPorIdRelacion = ListarMaestraPorIdRelacion;




        vm.ListTipoServicio = [];
        vm.ListDepreciacion = [];

        vm.DescripcionNuevo = "";
        vm.CostoInstalacion = "";
        vm.IdtipoServicio = "-1";
        vm.IdDepreciacion = "-1";
        vm.IdtipoSubServicio = "-1";
        vm.IdSubServicio = 0;

        vm.Descripcion = '';
        vm.IdSubServicio = "";
        vm.MensajeDeAlerta = "";
        LimpiarGrilla();

        vm.dtColumns = [
         DTColumnBuilder.newColumn('IdSubServicio').withTitle('Codigo').notSortable(),
         DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),

         DTColumnBuilder.newColumn('Estado').withTitle('Estado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaServicio)
        ];
        function AccionesBusquedaServicio(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn ' data-toggle='modal' data-target='#ModalRegistrarServicio'  ng-click='vm.ModalSubServicio(" + data.IdSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> " +
            "<div class='col-xs-4 col-sm-1'><a title='Inactivar' class='btn btn-sm'  id='tab-button' class='btn '  onclick='EliminarSubServicio(" + data.IdSubServicio + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
        }



        function convertToPounds(str) {
            var n = Number.parseFloat(str);
            if (!str || isNaN(n) || n < 0) return 0;
            return n.toFixed(2);
        }


        function ListarMaestraPorIdRelacion() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 32

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListTipoServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }


        function EliminarSubServicio(IdSubServicio) {
            blockUI.start();
            var subServicio = {
                IdSubServicio: IdSubServicio


            };
            var promise = SubServicioService.InhabilitarSubServicio(subServicio);

            promise.then(function (resultado) {

                blockUI.stop();

                UtilsFactory.Alerta('#divAlert_Eliminar', 'success', "Se Inactivo con exito registro.", 5);

                BuscarSubServicio();

            }, function (response) {
                blockUI.stop();

            });
        }



        function LimpiarModal() {
            var ddlEstado = angular.element(document.getElementById("ddlEstado"));
            vm.txtDescripcion = '';
            vm.txtOrdenVisual = '';
            vm.btnActualizar = false;
            vm.btnAgregar = true;
        }


        function BuscarSubServicio() {
            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarSubServicioPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }
        function BuscarSubServicioPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var subServicio = {
                Descripcion: vm.Descripcion,
                IdTipoSubServicio: vm.IdtipoSubServicio,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = SubServicioService.ListaSubServicioPaginado(subServicio);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListSubServicioDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }



        function LimpiarFiltros() {
            vm.Descripcion = '';
            vm.IdtipoSubServicio = "-1";
            LimpiarGrilla();
        }


    }
})();








