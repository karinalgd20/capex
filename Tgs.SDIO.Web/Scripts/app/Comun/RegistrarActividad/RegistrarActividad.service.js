﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('RegistrarActividadService', RegistrarActividadService);

    RegistrarActividadService.$inject = ['$http', 'URLS'];

    function RegistrarActividadService($http, $urls) {

        var service = {
            RegistrarActividad: RegistrarActividad,
        };

        return service;

        function RegistrarActividad(actividad) {
            return $http({
                url: $urls.ApiComun + "RegistrarActividad/RegistrarActividad",
                method: "POST",
                data: JSON.stringify(actividad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();