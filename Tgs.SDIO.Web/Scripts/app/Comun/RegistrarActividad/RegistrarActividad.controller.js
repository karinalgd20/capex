﻿(function () {
    'use strict'
    angular
        .module('app.Comun')
        .controller('RegistrarActividad', RegistrarActividad);
    RegistrarActividad.$inject = ['RegistrarActividadService', 'ActividadService', 'MaestraService', 'EtapaService', 'AreaSeguimientoService', 'FaseService', 'SegmentosNegocioService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RegistrarActividad(RegistrarActividadService, ActividadService, MaestraService, EtapaService, AreaSeguimientoService, FaseService, SegmentosNegocioService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.ListarComboFases = ListarComboFases;
        vm.ListarMaestraPorIdRelacion = ListarMaestraPorIdRelacion;
        vm.ListarComboAreas = ListarComboAreas;
        vm.RegistraActividad = RegistraActividad;
        vm.ObtenerEtapa = ObtenerEtapa;
        vm.IdActividad = "";
        vm.IdActividadPadre = "";
        vm.Descripcion = "";
        vm.Predecesoras = "";
        vm.IdFase = "-1";
        vm.IdEtapa = "-1";
        vm.FlgCapexMayor = false;
        vm.FlgCapexMenor = false;
        vm.IdAreaSeguimiento = "-1";
        vm.NumeroDiaCapexMenor = "";
        vm.CantidadDiasCapexMenor = "";
        vm.NumeroDiaCapexMayor = "";
        vm.CantidadDiasCapexMayor = "";
        vm.IdTipoActividad = "-1";
        vm.AsegurarOferta = false;
        vm.IdEstado = "1";
        vm.SegmentoNegocioModel = [];
        ListarComboFases();
        ListarMaestraPorIdRelacion();
        ListarComboAreas();
        ListarComboSegmentos();
        ListarComboActividadPadres();
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        vm.CargaModalActividad = CargaModalActividad;
        CargaModalActividad();

        //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si es existente.
        function CargaModalActividad() {
            vm.IdActividad = $scope.$parent.vm.IdActividad;
            if (vm.IdActividad > 0) {
                ObtenerActividad();
            } else {
                CargaModal();
            }
        }

        function ObtenerActividad() {
            blockUI.start();
            var actividad = {
                IdActividad: vm.IdActividad
            }
            var promise = ActividadService.ObtenerActividadPorId(actividad);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdActividad = Respuesta.IdActividad;
                debugger;
                if (Respuesta.IdActividadPadre == null) {
                    vm.IdActividadPadre = -1;
                }else{
                    vm.IdActividadPadre = Respuesta.IdActividadPadre;
                }
                vm.Descripcion = Respuesta.Descripcion;
                vm.Predecesoras = Respuesta.Predecesoras;
                vm.IdFase = Respuesta.IdFase;
                ObtenerEtapa(vm.IdFase);
                vm.IdEtapa = Respuesta.IdEtapa;
                vm.FlgCapexMayor = Respuesta.FlgCapexMayor;
                vm.FlgCapexMenor = Respuesta.FlgCapexMenor;
                vm.IdAreaSeguimiento = Respuesta.IdAreaSeguimiento;
                vm.NumeroDiaCapexMenor = Respuesta.NumeroDiaCapexMenor;
                vm.CantidadDiasCapexMenor = Respuesta.CantidadDiasCapexMenor;
                vm.NumeroDiaCapexMayor = Respuesta.NumeroDiaCapexMayor;
                vm.CantidadDiasCapexMayor = Respuesta.CantidadDiasCapexMayor;
                vm.IdTipoActividad = Respuesta.IdTipoActividad;
                vm.AsegurarOferta = Respuesta.AsegurarOferta;
                vm.IdEstado = Respuesta.IdEstado;
                vm.SegmentoNegocioModel = Respuesta.ListSegmentoNegocio;
            }, function (response) {
                blockUI.stop();

            });
        }

        function CargaModal() {
            vm.IdActividad = "";
            vm.IdActividadPadre = "-1";
            vm.Descripcion = "";
            vm.Predecesoras = "";
            vm.IdFase = "-1";
            vm.IdEtapa = "-1";
            vm.FlgCapexMayor = false;
            vm.FlgCapexMenor = false;
            vm.IdAreaSeguimiento = "-1";
            vm.NumeroDiaCapexMenor = "";
            vm.CantidadDiasCapexMenor = "";
            vm.NumeroDiaCapexMayor = "";
            vm.CantidadDiasCapexMayor = "";
            vm.IdTipoActividad = "-1";
            vm.AsegurarOferta = false;
            vm.IdEstado = "1";
        }
        //Registra en  la tabla [Comun].[Actividad] 
        vm.RegistraActividad = RegistraActividad;
        function RegistraActividad() {
            debugger;
            var IdActividad = null;
            if (vm.IdActividadPadre != "-1") {
                IdActividad = vm.IdActividadPadre;
            }
            var actividad = {
                IdActividad: vm.IdActividad,
                IdActividadPadre: IdActividad,
                Descripcion: vm.Descripcion,
                Predecesoras: vm.Predecesoras,
                IdFase: vm.IdFase,
                IdEtapa: vm.IdEtapa,
                FlgCapexMayor: vm.FlgCapexMayor,
                FlgCapexMenor: vm.FlgCapexMenor,
                IdAreaSeguimiento: vm.IdAreaSeguimiento,
                NumeroDiaCapexMenor: vm.NumeroDiaCapexMenor,
                CantidadDiasCapexMenor: vm.CantidadDiasCapexMenor,
                NumeroDiaCapexMayor: vm.NumeroDiaCapexMayor,
                CantidadDiasCapexMayor: vm.CantidadDiasCapexMayor,
                IdTipoActividad: vm.IdTipoActividad,
                AsegurarOferta: vm.AsegurarOferta,
                IdEstado: vm.IdEstado,
                ListSegmentoNegocio: vm.SegmentoNegocioModel
            }
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promiseActividad = (vm.IdActividad > 0) ? ActividadService.ActualizarActividad(actividad) : ActividadService.RegistrarActividad(actividad);

                promiseActividad.then(function (resultadoActividad) {

                    var RespuestaActividad = resultadoActividad.data;
                    if (RespuestaActividad.TipoRespuesta == 0) {
                        
                        $('#ModalRegistrarActividad').modal('hide');
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar la Actividad.", 5);
                    }

                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            } else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Lista los IdEtapa y Descripciones de la tabla [Comun].[EtapaOportunidad] que esten Activos.
        function ObtenerEtapa(IdFase) {
            if (IdFase != "-1") {
                var fase = {
                    IdFase: IdFase
                };
                var promise = EtapaService.ListarComboEtapaOportunidadPorIdFase(fase);
                promise.then(function (response) {
                    if (response == "") {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Etapas", 5);
                    } else {
                        vm.listEtapas = UtilsFactory.AgregarItemSelect(response.data);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
            }
        }
        //Lista los IdFase y Descripciones de la tabla [Comun].[Fase] que esten Activos.
        function ListarComboFases() {
            blockUI.start();
            var promise = FaseService.ListarComboFases();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Fases", 5);
                } else {
                    vm.listFases = UtilsFactory.AgregarItemSelect(respuesta);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarMaestraPorIdRelacion() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 146
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listActividades = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Lista los IdAreaSeguimiento y Descripciones de la tabla [Comun].[AreaSeguimiento] que esten Activos.
        function ListarComboAreas() {
            blockUI.start();
            var promise = AreaSeguimientoService.ListarComboAreas();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Areas", 5);
                } else {
                    vm.listAreas = UtilsFactory.AgregarItemSelect(respuesta);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdActividad y Descripciones de la tabla [Comun].[Actividad]  que esten Activos y el IdActividadPadre sea null.
        function ListarComboActividadPadres() {
            blockUI.start();
            var promise = ActividadService.ListarComboActividadPadres();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Actividades Padres", 5);
                } else {
                    vm.listActividadesPadre = UtilsFactory.AgregarItemSelect(respuesta);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [Comun].[SegmentoNegocio] que esten Activos.
        function ListarComboSegmentos() {
            vm.SegmentoNegocioSettings = {
                scrollableHeight: '200px',
                scrollable: true,
                enableSearch: false
            };
            var promise = SegmentosNegocioService.ListarComboSegmentoNegocio();
            promise.then(function (resultado) {
                vm.SegmentoNegocioData = resultado.data;
            });
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {

            var mensaje = "";
            if ($.trim(vm.Descripcion) == "") {
                mensaje = mensaje + "Ingrese Descripcion" + '<br/>';
                UtilsFactory.InputBorderColor('#txtDescripcion', 'Rojo');
            }
            if ($.trim(vm.IdAreaSeguimiento) == "-1") {
                mensaje = mensaje + "Seleccione Area" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdAreaSeguimiento', 'Rojo');
            }
            if ($.trim(vm.SegmentoNegocioModel) == "") {
                mensaje = mensaje + "Elija SegmentoNegocio" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlAreaSegmentoNegocio', 'Rojo');
            }
            if ($.trim(vm.FlgCapexMayor) == "false" && $.trim(vm.FlgCapexMenor) == "false") {
                mensaje = mensaje + "Seleccione check de Capex Mayor o Mengor" + '<br/>';
                UtilsFactory.InputBorderColor('#chckFlgCapexMayor', 'Rojo');
                UtilsFactory.InputBorderColor('#chckFlgCapexMenor', 'Rojo');
            }
            if ($.trim(vm.NumeroDiaCapexMenor) == "" || !UtilsFactory.ValidarNumero(vm.NumeroDiaCapexMenor)) {
                mensaje = mensaje + "Ingrese Numero de dia de Capex menor" + '<br/>';
                UtilsFactory.InputBorderColor('#txtNumeroDiaCapexMenor', 'Rojo');
            }
            if ($.trim(vm.CantidadDiasCapexMenor) == "") {
                mensaje = mensaje + "Ingrese Cantidad de dia de Capex menor" + '<br/>';
                UtilsFactory.InputBorderColor('#txtCantidadDiasCapexMenor', 'Rojo');
            }
            if ($.trim(vm.NumeroDiaCapexMayor) == "") {
                mensaje = mensaje + "Ingrese Numero de dia de Capex mayor" + '<br/>';
                UtilsFactory.InputBorderColor('#txtNumeroDiaCapexMayor', 'Rojo');
            }
            if ($.trim(vm.CantidadDiasCapexMayor) == "") {
                mensaje = mensaje + "Ingrese Cantidad de dia de Capex mayor" + '<br/>';
                UtilsFactory.InputBorderColor('#txtCantidadDiasCapexMayor', 'Rojo');
            }
            if (!UtilsFactory.ValidarNumero(vm.NumeroDiaCapexMenor)) {
                mensaje = mensaje + "El Numero de dia de Capex menor debe ser numerica" + '<br/>';
                UtilsFactory.InputBorderColor('#txtNumeroDiaCapexMenor', 'Rojo');
            }
            if (!UtilsFactory.ValidarNumero(vm.CantidadDiasCapexMenor)) {
                mensaje = mensaje + "la Cantidad de dia de Capex menor debe de ser numerica" + '<br/>';
                UtilsFactory.InputBorderColor('#txtCantidadDiasCapexMenor', 'Rojo');
            }
            if (!UtilsFactory.ValidarNumero(vm.NumeroDiaCapexMayor)) {
                mensaje = mensaje + "El Numero de dia de Capex mayor debe de ser numerica" + '<br/>';
                UtilsFactory.InputBorderColor('#txtNumeroDiaCapexMayor', 'Rojo');
            }
            if (!UtilsFactory.ValidarNumero(vm.CantidadDiasCapexMayor)) {
                mensaje = mensaje + "la Cantidad de dia de Capex mayor debe ser numerica" + '<br/>';
                UtilsFactory.InputBorderColor('#txtCantidadDiasCapexMayor', 'Rojo');
            }
            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }
            if ($.trim(vm.IdTipoActividad) == "-1") {
                mensaje = mensaje + "Elija Tipo Actividad" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdTipoActividad', 'Rojo');
            }
            if ($.trim(vm.IdFase) == "-1") {
                mensaje = mensaje + "Elija Fase" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdFase', 'Rojo');
            }
            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {

            UtilsFactory.InputBorderColor('#ddlIdAreaSeguimiento', 'Ninguno');
            UtilsFactory.InputBorderColor('#chckFlgCapexMayor', 'Ninguno');
            UtilsFactory.InputBorderColor('#chckFlgCapexMenor', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtNumeroDiaCapexMenor', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtCantidadDiasCapexMenor', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtNumeroDiaCapexMayor', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtCantidadDiasCapexMayor', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdTipoActividad', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtIdActividad', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtDescripcion', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlAreaSegmentoNegocio', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdFase', 'Ninguno');
        }

    }

})();