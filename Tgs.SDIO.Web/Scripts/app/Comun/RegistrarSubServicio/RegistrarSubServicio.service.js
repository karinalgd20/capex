﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('RegistrarSubServicioService', RegistrarSubServicioService);

    RegistrarSubServicioService.$inject = ['$http', 'URLS'];

    function RegistrarSubServicioService($http, $urls) {

        var service = {
      
            ModalSubServicio: ModalSubServicio
        };

        return service;

        function ModalSubServicio(subServicio) {

            return $http({
                url: $urls.ApiComun + "RegistrarSubServicio/Index",
                method: "POST",
                data: JSON.stringify(subServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();