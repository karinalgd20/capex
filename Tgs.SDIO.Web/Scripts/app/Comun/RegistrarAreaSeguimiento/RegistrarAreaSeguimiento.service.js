﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('RegistrarAreaSeguimientoService', RegistrarAreaSeguimientoService);

    RegistrarAreaSeguimientoService.$inject = ['$http', 'URLS'];

    function RegistrarAreaSeguimientoService($http, $urls) {

        var service = {


            RegistrarAreaSeguimiento: RegistrarAreaSeguimiento      
        };

        return service;

        function RegistrarAreaSeguimiento(area) {
            return $http({
                url: $urls.ApiComun + "RegistrarAreaSeguimiento/RegistrarAreaSeguimiento",
                method: "POST",
                data: JSON.stringify(area)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();