﻿(function() {
    'use strict'
    angular
        .module('app.Comun')
        .controller('RegistrarAreaSeguimiento', RegistrarAreaSeguimiento);
    RegistrarAreaSeguimiento.$inject = ['RegistrarAreaSeguimientoService', 'SegmentosNegocioService', 'AreaSeguimientoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RegistrarAreaSeguimiento(RegistrarAreaSeguimientoService, SegmentosNegocioService, AreaSeguimientoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.RegistraAreaSeguimiento = RegistraAreaSeguimiento;
        vm.EnlaceBandeja = $urls.ApiTrazabilidad + "BandejaAreaSeguimiento/Index/";
        vm.IdAreaSeguimiento = "";
        vm.Descripcion = "";
        vm.AreaSegmentoNegocio = "";
        vm.OrdenVisual = "";
        vm.IdEstado = "1";
        vm.SegmentoNegocioModel = [];
        ListarComboSegmentos();
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        vm.CargaModalAreaSeguimiento = CargaModalAreaSeguimiento;
        CargaModalAreaSeguimiento();

        //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si es existente.
        function CargaModalAreaSeguimiento() {
            vm.IdAreaSeguimiento = $scope.$parent.vm.IdAreaSeguimiento;
            if (vm.IdAreaSeguimiento > 0) {
                ObtenerAreaSeguimiento();
            } else {
                CargaModal();
            }
        }

        function ObtenerAreaSeguimiento() {
            blockUI.start();
            var area = {
                IdAreaSeguimiento: vm.IdAreaSeguimiento
            }
            var promise = AreaSeguimientoService.ObtenerAreaSeguimientoPorId(area);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdAreaSeguimiento = Respuesta.IdAreaSeguimiento;
                vm.Descripcion = Respuesta.Descripcion;
                vm.AreaSegmentoNegocio = Respuesta.AreaSegmentoNegocio;
                vm.OrdenVisual = Respuesta.OrdenVisual;
                vm.SegmentoNegocioModel = Respuesta.ListSegmentoNegocio;
                vm.IdEstado = Respuesta.IdEstado;
            }, function (response) {
                blockUI.stop();

            });
        }

        function CargaModal() {
            vm.IdAreaSeguimiento = "";
            vm.Descripcion = "";
            vm.AreaSegmentoNegocio = "";
            vm.OrdenVisual = "";
            vm.IdEstado = "1";
        }
        //Registra en  la tabla [TRAZABILIDAD].[AreaSeguimiento] 
        vm.RegistraAreaSeguimiento = RegistraAreaSeguimiento;
        function RegistraAreaSeguimiento() {
            var area = {
                IdAreaSeguimiento: vm.IdAreaSeguimiento,
                Descripcion: vm.Descripcion,
                OrdenVisual: vm.OrdenVisual,
                IdEstado: vm.IdEstado,
                ListSegmentoNegocio: vm.SegmentoNegocioModel
            }
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promiseAreaSeguimiento = (vm.IdAreaSeguimiento > 0) ? AreaSeguimientoService.ActualizarAreaSeguimiento(area) : AreaSeguimientoService.RegistrarAreaSeguimiento(area);

                promiseAreaSeguimiento.then(function (resultadoAreaSeguimiento) {
                    var RespuestaAreaSeguimiento = resultadoAreaSeguimiento.data;
                    if (RespuestaAreaSeguimiento.TipoRespuesta == 0) {
                        $scope.$parent.vm.BuscarAreaSeguimiento();
                        $('#ModalRegistrarAreaSeguimiento').modal('hide');
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar el Area.", 5);
                    }

                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            } else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[SegmentoNegocio] que esten Activos.
        function ListarComboSegmentos() {
            vm.SegmentoNegocioSettings = {
                scrollableHeight: '200px',
                scrollable: true,
                enableSearch: false
            };
            var promise = SegmentosNegocioService.ListarComboSegmentoNegocio();
            promise.then(function (resultado) {
                vm.SegmentoNegocioData = resultado.data;
            });
        }    
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.Descripcion) == "") {
                mensaje = mensaje + "Ingrese Descripcion" + '<br/>';
                UtilsFactory.InputBorderColor('#txtDescripcion', 'Rojo');
            }
            if ($.trim(vm.SegmentoNegocioModel) == "") {
                mensaje = mensaje + "Elija SegmentoNegocio" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlAreaSegmentoNegocio', 'Rojo');
            }
            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }

            if (!UtilsFactory.ValidarNumero(vm.OrdenVisual)) {
                mensaje = mensaje + "El Numero de orden visual debe ser numerico" + '<br/>';
                UtilsFactory.InputBorderColor('#txtOrdenVisual', 'Rojo');
            }
            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#txtDescripcion', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlAreaSegmentoNegocio', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtOrdenVisual', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
        }
    }
})();