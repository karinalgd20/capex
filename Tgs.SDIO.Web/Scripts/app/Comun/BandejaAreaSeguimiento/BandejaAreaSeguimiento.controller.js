﻿(function () {
    'use strict'
    angular
        .module('app.Comun')
        .controller('BandejaAreaSeguimiento', BandejaAreaSeguimiento);
    BandejaAreaSeguimiento.$inject = ['BandejaAreaSeguimientoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function BandejaAreaSeguimiento(BandejaAreaSeguimientoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiTrazabilidad + "RegistrarAreaSeguimiento/Index/";
        vm.BuscarAreaSeguimiento = BuscarAreaSeguimiento;
        vm.EliminarAreaSeguimiento = EliminarAreaSeguimiento;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.DescripcionSegmento = "";
        vm.DescripcionArea = "";
        vm.IdAreaSeguimiento = "";
        vm.MensajeDeAlerta = "";
        LimpiarGrilla();
        vm.dtColumns = [
            DTColumnBuilder.newColumn('IdAreaSeguimiento').withTitle('Codigo').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion Area').notSortable().withOption('width', '60%'),
            DTColumnBuilder.newColumn('OrdenVisual').withTitle('Orden Visual').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaAreaSeguimiento).withOption('width', '10%')
        ];
        //Modal registrar

        vm.ModalAreaSeguimiento = ModalAreaSeguimiento;

        function ModalAreaSeguimiento(IdAreaSeguimiento) {
            vm.IdAreaSeguimiento = IdAreaSeguimiento;
            var area = {
                IdAreaSeguimiento: IdAreaSeguimiento
            };

            var promise = BandejaAreaSeguimientoService.ModalAreaSeguimiento(area);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoAreaSeguimiento").html(content);
                });

                $('#ModalRegistrarAreaSeguimiento').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };
        //Eliminacion logica de la tabla [TRAZABILIDAD].[AreaSeguimiento]
        function EliminarAreaSeguimiento(IdAreaSeguimiento) {
            blockUI.start();
            if (confirm('¿Estas seguro que desea eliminar?')) {

                var area = {
                    IdAreaSeguimiento: IdAreaSeguimiento
                }   
                var promise = BandejaAreaSeguimientoService.EliminarAreaSeguimiento(area);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                        BuscarAreaSeguimiento()
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                    }
                    modalpopupConfirm.modal('hide');
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            }
            else {
                blockUI.stop();
            }
        }       
        //Filtro de la  tabla [TRAZABILIDAD].[AreaSeguimiento] por Descripcion de area y Descripcion de segmento de negocio.
        function BuscarAreaSeguimiento() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarAreaSeguimientoPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                       .withOption('createdRow', function (row, data, dataIndex) {
                           $compile(angular.element(row).contents())($scope);
                       })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }
        function BuscarAreaSeguimientoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var area = {
                Descripcion: vm.DescripcionArea,
                DescripcionSegmento: vm.DescripcionSegmento,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = BandejaAreaSeguimientoService.ListarAreaSeguimientoPaginado(area);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListAreaSeguimientoDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }                   
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('paging', false);
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusquedaAreaSeguimiento(data, type, full, meta) {
            var respuesta = "";
            respuesta = respuesta + " <a title='Editar'   ng-click='vm.ModalAreaSeguimiento(" + data.IdAreaSeguimiento + ");'>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + "<a title='Eliminar'  onclick='EliminarAreaSeguimiento(" + data.IdAreaSeguimiento + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6; margin-left: 1px;'></span></a>";
            return respuesta;
        }
         //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.DescripcionSegmento = '';
            vm.DescripcionArea = '';
            LimpiarGrilla();
        }
    }
})();