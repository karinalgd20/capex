﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('BandejaAreaSeguimientoService', BandejaAreaSeguimientoService);

    BandejaAreaSeguimientoService.$inject = ['$http', 'URLS'];

    function BandejaAreaSeguimientoService($http, $urls) {

        var service = {
            ListarAreaSeguimientoPaginado: ListarAreaSeguimientoPaginado,
            EliminarAreaSeguimiento: EliminarAreaSeguimiento,
            ModalAreaSeguimiento: ModalAreaSeguimiento
        };

        return service;

        function ListarAreaSeguimientoPaginado(area) {
            return $http({
                url: $urls.ApiComun + "BandejaAreaSeguimiento/ListarAreaSeguimiento",
                method: "POST",
                data: JSON.stringify(area)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarAreaSeguimiento(area) {
            return $http({
                url: $urls.ApiComun + "BandejaAreaSeguimiento/EliminarAreaSeguimiento",
                method: "POST",
                data: JSON.stringify(area)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalAreaSeguimiento(area) {
            return $http({
                url: $urls.ApiComun + "RegistrarAreaSeguimiento/Index",
                method: "POST",
                data: JSON.stringify(area)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();