﻿(function () {
    'use strict'

    angular
    .module('app.Comun')
    .controller('BandejaServicio', BandejaServicio);

    BandejaServicio.$inject = ['BandejaServicioService','RegistrarServicioControllerService', 'MedioService', 'LineaNegocioService', 'SubServicioService', 'ServicioSubServicioService', 'ProveedorService', 'MaestraService', 'ServicioService', 'DepreciacionService'
        , 'ServicioCMIService'
        , 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function BandejaServicio(BandejaServicioService,RegistrarServicioControllerService, MedioService, LineaNegocioService, SubServicioService, ServicioSubServicioService, ProveedorService, MaestraService, ServicioService, DepreciacionService,
        ServicioCMIService
       , blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

               /* Modal Registar */
        vm.ModalServicio = ModalServicio;

        function ModalServicio(IdServicio) {
            vm.IdServicio = IdServicio;
            debugger
            var servicio = {
                IdServicio: IdServicio
            };

            var promise = RegistrarServicioControllerService.ModalServicio(servicio);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoServicio").html(content);
                });

                $('#ModalRegistrarServicio').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };


        /*--------------------------------------*/

        vm.BuscarServicio = BuscarServicio;
        vm.EliminarServicio = EliminarServicio;
        vm.AceptarEliminarServicio = AceptarEliminarServicio;
        vm.LimpiarFiltros = LimpiarFiltros;




        vm.ListarLineaNegocios = ListarLineaNegocios;
        vm.ListLineaNegocio = [];

  
        vm.IdLineaNegocioBandeja = "-1";

        vm.Descripcion = '';
        LimpiarGrilla();
        ListarLineaNegocios();
        vm.dtInstance= {};
        vm.dtColumns = [

           DTColumnBuilder.newColumn('IdServicio').withTitle('Codigo').notSortable(),
           DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
           DTColumnBuilder.newColumn('Estado').withTitle('Estado').notSortable(),
           DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaServicio)

        ];

        function AccionesBusquedaServicio(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn ' data-toggle='modal' data-target='#ModalRegistrarServicio'   ng-click='vm.ModalServicio(" + data.IdServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> " +
        "<div class='col-xs-4 col-sm-1'><a title='Cancelar' class='btn btn-sm'  id='tab-button' class='btn '  onclick='EliminarServicio(" + data.IdServicio + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";

        };

        function BuscarServicio() {

            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarServicioPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        };

        function BuscarServicioPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var Servicio = {
                Descripcion: vm.Descripcion,
                IdLineaNegocio:vm.IdLineaNegocioBandeja,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ServicioService.ListaServicioPaginado(Servicio);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListServicioDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };


     

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);

        };
      

        
 

      

        function EliminarServicio(IdServicio) {

            if (confirm('¿Estas seguro que desea eliminar?')) {


            var servicio = {
                IdServicio: IdServicio
            }

            debugger
            var promise = ServicioService.InhabilitarServicio(servicio);

            promise.then(function (resultado) {
                blockUI.stop();

                UtilsFactory.Alerta('#divAlert_Eliminar', 'success', "Se Inhabilito  el registro.", 5);
                BuscarServicio();
            }, function (response) {

                blockUI.stop();
            });
             }
            else {
                blockUI.stop();
            }

        }

        function AceptarEliminarServicio() {
            var ddlIdServicio = angular.element(document.getElementById("idServicio"));
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            var Servicio = {
                IdServicio: ddlIdServicio.val()
            }
            blockUI.start();
            var promise = BandejaServicioService.EliminarServicio(Servicio);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                    BuscarServicio()
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                }
                modalpopupConfirm.modal('hide');
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                blockUI.stop();
            });

        }
       
        function ListarLineaNegocios() {

            var lineaNegocio = {
                IdEstado: 1

            };
            var promise = LineaNegocioService.ListarLineaNegocios(lineaNegocio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

      



        function LimpiarFiltros() {
            vm.IdLineaNegocioBandeja = "-1";
            vm.Descripcion = '';
            vm.IdtipoServicio = "-1";
            LimpiarGrilla();
        }

    }
})();



