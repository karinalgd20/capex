﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('BandejaServicioService', BandejaServicioService);

    BandejaServicioService.$inject = ['$http', 'URLS'];

    function BandejaServicioService($http, $urls) {

        var service = {

            ListarPestanaGrupo: ListarPestanaGrupo,

        };

        return service;




        function ListarPestanaGrupo(pestanaGrupo) {

            return $http({
                url: $urls.ApiComun + "BandejaServicio/ListarPestanaGrupo",
                method: "POST",
                data: JSON.stringify(pestanaGrupo)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };



    }
})();