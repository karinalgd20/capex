﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('EmpresaService', EmpresaService);

    EmpresaService.$inject = ['$http', 'URLS'];

    function EmpresaService($http, $urls) {

        var service = {
            ListarComboEmpresa: ListarComboEmpresa
        };

        return service;

        function ListarComboEmpresa() {
            return $http({
                url: $urls.ApiComun + "Empresa/ListarComboEmpresa",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };       

    }
})();