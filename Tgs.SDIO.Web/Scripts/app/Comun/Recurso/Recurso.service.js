﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('RecursoService', RecursoService);

    RecursoService.$inject = ['$http', 'URLS'];

    function RecursoService($http, $urls) {

        var service = {
            ListaRecursoModalRecursoPaginado: ListaRecursoModalRecursoPaginado,
            ObtenerRecursoPorId: ObtenerRecursoPorId,
            ActualizarRecurso: ActualizarRecurso,
            RegistrarRecurso: RegistrarRecurso,
            ListarRecursoPorCargo: ListarRecursoPorCargo,
            ListarRecursoDeUsuario: ListarRecursoDeUsuario,
            ListarRecursoPorCargoPaginado: ListarRecursoPorCargoPaginado
        };

        return service;

        function ListaRecursoModalRecursoPaginado(recurso) {
            return $http({
                url: $urls.ApiComun + "Recurso/ListaRecursoModalRecursoPaginado",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerRecursoPorId(recurso) {
            return $http({
                url: $urls.ApiComun + "Recurso/ObtenerRecursoPorId",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarRecurso(recurso) {
            return $http({
                url: $urls.ApiComun + "Recurso/ActualizarRecurso",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarRecurso(recurso) {
            return $http({
                url: $urls.ApiComun + "Recurso/RegistrarRecurso",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarRecursoPorCargo(recurso) {
            return $http({
                url: $urls.ApiComun + "Recurso/ListarRecursoPorCargo",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

         function ListarRecursoDeUsuario(recurso) {
            return $http({
                url: $urls.ApiComun + "Recurso/ListarRecursoDeUsuario",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
         };

         function ListarRecursoPorCargoPaginado(recurso) {
             return $http({
                 url: $urls.ApiComun + "Recurso/ListarRecursoPorCargoPaginado",
                 method: "POST",
                 data: JSON.stringify(recurso)
             }).then(DatosCompletados);

             function DatosCompletados(resultado) {
                 return resultado;
             }
         };
    }
})();