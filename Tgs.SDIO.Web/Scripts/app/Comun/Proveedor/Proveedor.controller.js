﻿var vm;
(function () {
    'use strict',

    angular
    .module('app.Comun')
    .controller('ProveedorController', ProveedorController);

    ProveedorController.$inject =
        [
            'ProveedorService',
            'blockUI',
            'UtilsFactory',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile'
        ];

    function ProveedorController(
        ProveedorService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile
        )
    {
        vm = this;
        vm.IdProveedor = "";
        vm.Descripcion = "";
        vm.RazonSocial = "";
        vm.RUC = "";
        vm.CodigoProveedor = "";
        vm.CambioDescripcion = CambioDescripcion;
        //vm.CambioRazonSocial = CambioRazonSocial;
        //vm.CambioRUC = CambioRUC;
        //vm.CambioCodProveedor = CambioCodProveedor;
        vm.btnFiltrarProveedor = FiltrarProveedor;
        vm.EnlaceRegistrar = $urls.ApiComun + "RegistrarProveedor/Index/"
        vm.dtInstanceTablaProveedor = {};

        vm.EliminarProveedor = EliminarProveedor;

        function FiltrarProveedor() {

            

        }

        vm.dtColumnsProveedor =
            [
                DTColumnBuilder.newColumn('IdProveedor').notVisible(),
                DTColumnBuilder.newColumn('Descripcion').withTitle('Proveedor').notSortable(),
                DTColumnBuilder.newColumn('RazonSocial').withTitle('Razon Social').notSortable(),
                DTColumnBuilder.newColumn('RUC').withTitle('Identificador Fiscal').notSortable(),
                DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesProveedor)
            ];

        function AccionesProveedor(data, type, full, meta) {
                var respuesta = "";
                respuesta = "<center><a title='Editar Proveedor' href='../RegistrarProveedor/Index?IdProveedor=" + data.IdProveedor + "')>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6; margin-right:.8em'></span></a>" +
                "<a title='Eliminar Proveedor'  ng-click='vm.EliminarProveedor(" + data.IdProveedor + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6;'></span></a></center>";
                return respuesta;
            }

        function LimpiarGrillaProveedor() {
            vm.dtOptionsProveedor = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            //.withOption('scrollY', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        };

        function BuscarProveedor() {

            blockUI.start();
            LimpiarGrillaProveedor()
            $timeout(function () {
                vm.dtOptionsProveedor = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                //.withOption('scrollY', 500)
                .withFnServerData(BuscarProveedorPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        };

        function BuscarProveedorPaginado(sSource, aoData, fnCallback, oSettings) {
                var draw = aoData[0].value;
                var start = aoData[3].value;
                var length = aoData[4].value;
                var pageNumber = (start + length) / length;
                var Proveedor = {
                    Id: vm.IdProveedor,
                    Indice: pageNumber,
                    Tamanio: length
                };

                var promise = ProveedorService.ListarProveedorPaginado(Proveedor);
                promise.then(function (resultado) {
                    var result = {
                        'draw': draw,
                        'recordsTotal': resultado.data.TotalItemCount,
                        'recordsFiltered': resultado.data.TotalItemCount,
                        'data': resultado.data.ListaProveedorPaginadoDtoResponse
                    };
                    blockUI.stop();
                    fnCallback(result)

                }, function (response) {
                    blockUI.stop();
                    LimpiarGrillaProveedor()
                });
        };
       
        function CambioDescripcion() {
            if (vm.Descripcion == "" || vm.Descripcion == null) {
                HabilitarCampos();
                return;
            }
            DesHabilitarCampos();
        }
        function HabilitarCampos() {
            $('#RazonSocial').removeAttr('disabled', 'disabled')
            $('#RUC').removeAttr('disabled', 'disabled')
            $('#RazonSocial').removeAttr('disabled', 'disabled')
            $('#CodigoProveedor').removeAttr('disabled', 'disabled')
        }
        function DesHabilitarCampos() {
            $('#RazonSocial').attr('disabled', 'disabled')
            $('#RUC').attr('disabled', 'disabled')
            $('#RazonSocial').attr('disabled', 'disabled')
            $('#CodigoProveedor').attr('disabled', 'disabled')
            
        }

        function EliminarProveedor(IdProveedor) {
            if (confirm('¿Estas Seguro que Desea Eliminar Proveedor')) {
                var proveedor =
                {
                    IdProveedor: IdProveedor
                }
                var promise = ProveedorService.InactivarProveedor(proveedor);
                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                        BuscarProveedor();
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    }
                })

            }
        }

        /*------------------------Inicializar Funciones ---------------------------*/
        LimpiarGrillaProveedor();
        BuscarProveedor();
        HabilitarCampos();
    }

})();