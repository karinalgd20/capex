﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('ProveedorService', ProveedorService);

    ProveedorService.$inject = ['$http', 'URLS'];

    function ProveedorService($http, $urls) {

        var service = {

            RegistrarProveedor: RegistrarProveedor,
            ActualizarProveedor: ActualizarProveedor,
            InactivarProveedor: InactivarProveedor,
            ListarProveedorPaginado: ListarProveedorPaginado,
            ObtenerProveedorById: ObtenerProveedorById,
            ListarProveedor: ListarProveedor
        };

        return service;

        function RegistrarProveedor(proveedor) {

            return $http({
                url: $urls.ApiComun + "Proveedor/RegistrarProveedor",
                method: "POST",
                data: JSON.stringify(proveedor)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarProveedor(proveedor) {

            return $http({
                url: $urls.ApiComun + "Proveedor/ActualizarProveedor",
                method: "POST",
                data: JSON.stringify(proveedor)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function InactivarProveedor(proveedor) {

            return $http({
                url: $urls.ApiComun + "Proveedor/InactivarProveedor",
                method: "POST",
                data: JSON.stringify(proveedor)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

       

        function ObtenerProveedorById(proveedor) {

            return $http({
                url: $urls.ApiComun + "Proveedor/ObtenerProveedorById",
                method: "POST",
                data: JSON.stringify(proveedor)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarProveedor(proveedor) {

            return $http({
                url: $urls.ApiComun + "Proveedor/ListarProveedor",
                method: "POST",
                data: JSON.stringify(proveedor)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarProveedorPaginado(proveedor) {

            return $http({
                url: $urls.ApiComun + "Proveedor/ListarProveedorPaginado",
                method: "POST",
                data: JSON.stringify(proveedor)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();