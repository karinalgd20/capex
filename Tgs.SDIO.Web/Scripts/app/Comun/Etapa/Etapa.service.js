﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('EtapaService', EtapaService);

    EtapaService.$inject = ['$http', 'URLS'];

    function EtapaService($http, $urls) {

        var service = {
            ListarComboEtapaOportunidadPorIdFase: ListarComboEtapaOportunidadPorIdFase,
            ListarComboEtapas: ListarComboEtapas,
        };

        return service;

        function ListarComboEtapaOportunidadPorIdFase(fase) {
            return $http({
                url: $urls.ApiComun + "Etapa/ListarComboEtapaOportunidadPorIdFase",
                method: "POST",
                data: JSON.stringify(fase)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

         function ListarComboEtapas() {
            return $http({
                url: $urls.ApiComun + "Etapa/ListarComboEtapas",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();