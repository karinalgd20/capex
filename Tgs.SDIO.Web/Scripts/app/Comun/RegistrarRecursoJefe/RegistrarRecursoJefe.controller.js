﻿(function () {
    'use strict'
    angular
    .module('app.Comun')
    .controller('RegistrarRecursoJefeController', RegistrarRecursoJefeController);

    RegistrarRecursoJefeController.$inject = ['RegistrarRecursoJefeService', 'BandejaUsuarioService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarRecursoJefeController(RegistrarRecursoJefeService, BandejaUsuarioService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vmjef = this;
        vmjef.ListarRecursoPorJefePaginado = ListarRecursoPorJefePaginado; 
        vmjef.RegistrarRecursoJefe = RegistrarRecursoJefe;
        vmjef.ActualizarEstadoRecursoJefe = ActualizarEstadoRecursoJefe;
        vmjef.BuscarPersonal = BuscarPersonal;

        vmjef.IdUsuario = $scope.$parent.vm.IdUsuario;
        vmjef.NombreUsuario = $scope.$parent.vm.NombreUsuario;
        vmjef.Persona = "";

        vmjef.dtInstancePersona = {};
        vmjef.dtColumnsPersona = [
            DTColumnBuilder.newColumn('NombreRecurso').withTitle('NombreRecurso').notSortable().withOption('width', '50%'),
            DTColumnBuilder.newColumn('Estado').withTitle('Estado').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];

        LimpiarGrilla();
        ListarRecursoPorJefePaginado();

        function AccionesBusqueda(data, type, full, meta) {
            return "<a title='Cambiar Estado' class='btn btn-primary'  id='tab-button' class='btn '  ng-click='vmjef.ActualizarEstadoRecursoJefe(" + data.IdRecursoJefe + "," + data.IdEstado + ");'> <span class='fa fa-eye-slash fa-lg'></span></a>  ";
        }

        function ListarRecursoPorJefePaginado() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vmjef.dtOptionsPersona = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(ListarRecursoPorJefe)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                       .withOption('createdRow', function (row, data, dataIndex) {
                           $compile(angular.element(row).contents())($scope);
                       })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }

        function ListarRecursoPorJefe(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var recursoJefeRequest = {
                IdUsuarioRais: vmjef.IdUsuario,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = RegistrarRecursoJefeService.ListarRecursoPorJefePaginado(recursoJefeRequest);

            promise.then(function (resultado) {

                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaRecursoJefeDtoResponse
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function LimpiarGrilla() {
            vmjef.dtOptionsPersona = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('paging', false);
        }

        function ActualizarEstadoRecursoJefe(id, estado) {
            var etiquetaEstado = (estado == 1) ? "inactivar" : "activar";

            if (confirm('¿Estas seguro de ' + etiquetaEstado + ' el registro?')) {
                blockUI.start();

                var recursoJefeRequest = {
                    IdEstado: (estado == 1) ? 0 : 1,
                    Id: id
                };

                var promise = RegistrarRecursoJefeService.ActualizarEstadoRecursoJefe(recursoJefeRequest);

                promise.then(function (resultado) {

                    var respuesta = resultado.data;

                    if (respuesta.TipoRespuesta == 0) {
                        UtilsFactory.Alerta('#divAlertRecursoJefe', 'success', respuesta.Mensaje, 10);
                        ListarRecursoPorJefePaginado();
                    } else {
                        UtilsFactory.Alerta('#divAlertRecursoJefe', 'danger', respuesta.Mensaje, 10);
                    }
                     
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertRecursoJefe', 'danger', MensajesUI.DatosError, 5);
                });
            }
        }

        function RegistrarRecursoJefe() {
            if (vmjef.Persona.Codigo == 'undefined' || vmjef.Persona.Codigo == null) {
                UtilsFactory.Alerta('#divAlertRecursoJefe', 'danger', "ingrese una persona", 5);
                blockUI.stop();
                return;
            }

            blockUI.start();

            var recursoJefeRequest = {
                IdUsuarioRais: vmjef.Persona.Codigo,
                IdJefe: vmjef.IdUsuario
            };

            var promise = RegistrarRecursoJefeService.RegistrarRecursoJefe(recursoJefeRequest);

            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;

                if (respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlertRecursoJefe', 'success', respuesta.Mensaje, 10);
                } else {
                    UtilsFactory.Alerta('#divAlertRecursoJefe', 'danger', respuesta.Mensaje, 10);
                }

                ListarRecursoPorJefePaginado();

            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRecursoJefe', 'danger', MensajesUI.DatosError, 5);
            });
        }


        function BuscarPersonal(trabajador) {
            if (parseInt(trabajador.length) < 3) {
                return false;
            }
            var trabajadorDto = {
                Nombres: trabajador
            };
            return BandejaUsuarioService.ListarUsuariosFiltro(trabajadorDto).then(function (response) {
                return response.data;
            });
        };

    }

})();