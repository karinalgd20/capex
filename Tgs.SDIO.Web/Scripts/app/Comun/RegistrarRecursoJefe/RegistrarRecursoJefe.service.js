﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('RegistrarRecursoJefeService', RegistrarRecursoJefeService);

    RegistrarRecursoJefeService.$inject = ['$http', 'URLS'];

    function RegistrarRecursoJefeService($http, $urls) {

        var service = {
            ModalRegistrarRecursoJefe:ModalRegistrarRecursoJefe,
            RegistrarRecursoJefe: RegistrarRecursoJefe,
            ListarRecursoPorJefePaginado: ListarRecursoPorJefePaginado,
            ActualizarEstadoRecursoJefe:ActualizarEstadoRecursoJefe
        };

        return service;

        function ModalRegistrarRecursoJefe() {
            return $http({
                url: $urls.ApiComun + "RegistrarRecursoJefe/Index",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarRecursoJefe(request) {
            return $http({
                url: $urls.ApiComun + "RegistrarRecursoJefe/RegistrarRecursoJefe",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarRecursoPorJefePaginado(request) {
            return $http({
                url: $urls.ApiComun + "RegistrarRecursoJefe/ListarRecursoPorJefePaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarEstadoRecursoJefe(request) {
            return $http({
                url: $urls.ApiComun + "RegistrarRecursoJefe/ActualizarEstadoRecursoJefe",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();