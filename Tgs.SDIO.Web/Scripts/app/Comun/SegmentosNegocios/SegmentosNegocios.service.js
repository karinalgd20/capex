﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('SegmentosNegocioService', SegmentosNegocioService);

    SegmentosNegocioService.$inject = ['$http', 'URLS'];

    function SegmentosNegocioService($http, $urls) {

        var service = {
            ListarComboSegmentoNegocio: ListarComboSegmentoNegocio,
            ListarComboSegmentoNegocioSimple: ListarComboSegmentoNegocioSimple
        };

        return service;

        function ListarComboSegmentoNegocio() {
            return $http({
                url: $urls.ApiComun + "SegmentoNegocio/ListarComboSegmentoNegocio",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarComboSegmentoNegocioSimple() {
            return $http({
                url: $urls.ApiComun + "SegmentoNegocio/ListarComboSegmentoNegocioSimple",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();