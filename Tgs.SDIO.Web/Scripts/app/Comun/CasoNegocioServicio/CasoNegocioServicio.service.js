﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('CasoNegocioServicioService', CasoNegocioServicioService);

    CasoNegocioServicioService.$inject = ['$http', 'URLS'];

    function CasoNegocioServicioService($http, $urls) {

        var service = {
            ListarCasoNegocioServicio : ListarCasoNegocioServicio,
            ListPaginadoCasoNegocioServicio:ListPaginadoCasoNegocioServicio
        };

        return service;
        
        function ListPaginadoCasoNegocioServicio(casoServicio) {

            return $http({
                url: $urls.ApiComun + "CasoNegocioServicio/ListPaginadoCasoNegocioServicio",
                method: "POST",
                data: JSON.stringify(casoServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListarCasoNegocioServicio(casoServicio) {

            return $http({
                url: $urls.ApiComun + "CasoNegocioServicio/ListarCasoNegocioServicio",
                method: "POST",
                data: JSON.stringify(casoServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };



    }

})();