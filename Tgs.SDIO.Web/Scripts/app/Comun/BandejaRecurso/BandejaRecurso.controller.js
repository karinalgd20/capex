﻿(function () {
    'use strict'
    angular
    .module('app.Comun')
    .controller('BandejaRecurso', BandejaRecurso);
    BandejaRecurso.$inject = ['BandejaRecursoService', 'MaestraService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function BandejaRecurso(BandejaRecursoService, MaestraService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.BuscarRecurso = BuscarRecurso;
        vm.EliminarRecurso = EliminarRecurso;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.Nombre = "";
        vm.IdTipoRecurso = "-1";
        LimpiarGrilla();
        ListarTipoRecurso();
        vm.listRecursos = [];
        vm.dtColumns = [
          DTColumnBuilder.newColumn('IdRecurso').withTitle('Código').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Nombre').withTitle('Nombre Completo').notSortable().withOption('width', '40%'),
          DTColumnBuilder.newColumn('TipoRecurso').withTitle('Tipo Recurso').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('DNI').withTitle('DNI').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('UserNameSF').withTitle('User Name SF').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('UsuarioEdicion').withTitle('Usuario Edicion').notSortable().withOption('width', '6%'),
          DTColumnBuilder.newColumn('strFechaEdicion').withTitle('Fecha Edicion').notSortable().withOption('width', '6%'),
          DTColumnBuilder.newColumn('UsuarioCreacion').withTitle('Usuario Creacion').notSortable().withOption('width', '6%'),
          DTColumnBuilder.newColumn('strFechaCreacion').withTitle('Fecha Creacion').notSortable().withOption('width', '6%'),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];
        //Modal registrar

        vm.ModalRecurso = ModalRecurso;

        function ModalRecurso(IdRecurso) {
            vm.IdRecurso = IdRecurso;
            var recurso = {
                IdRecurso: IdRecurso
            };

            var promise = BandejaRecursoService.ModalRecurso(recurso);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoRecurso").html(content);
                });

                $('#ModalRegistrarRecurso').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };
        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarTipoRecurso() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 125
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listRecursos = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Eliminacion logica de la tabla [Comun].[Recurso]
        function EliminarRecurso(IdRecurso) {
              blockUI.start();
            if (confirm('¿Estas seguro que desea eliminar?')) {
                    blockUI.start();
                    var objConcepto = {
                        IdRecurso: IdRecurso
                    }
                    var promise = BandejaRecursoService.EliminarRecurso(objConcepto);
                    promise.then(function (resultado) {
                        blockUI.stop();
                        var Respuesta = resultado.data;
                        if (Respuesta.TipoRespuesta == 0) {
                            UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                            BuscarRecurso()
                        } else {
                            UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                        }
                        modalpopupConfirm.modal('hide');
                    }, function (response) {
                        UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                        blockUI.stop();
                    });
            }
            else {
                blockUI.stop();
            }
        }
        //Filtro de la  tabla [Comun].[Recurso] por Descripcion.
        function BuscarRecurso() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarRecursoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                   .withOption('createdRow', function (row, data, dataIndex) {
                       $compile(angular.element(row).contents())($scope);
                   })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }

        function BuscarRecursoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var recurso = {
                Nombre: vm.Nombre,
                TipoRecurso: vm.IdTipoRecurso,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = BandejaRecursoService.ListarRecursoPaginado(recurso);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListRecursoDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusqueda(data, type, full, meta) {
            var respuesta = "";
            respuesta = respuesta + " <a title='Editar'   ng-click='vm.ModalRecurso(" + data.IdRecurso + ");'>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + "<a title='Eliminar'  onclick='EliminarRecurso(" + data.IdRecurso + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6; margin-left: 1px;'></span></a>";
            return respuesta;
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.Nombre = "";
            vm.IdTipoRecurso = "-1";
            LimpiarGrilla();
        }
    }
})();