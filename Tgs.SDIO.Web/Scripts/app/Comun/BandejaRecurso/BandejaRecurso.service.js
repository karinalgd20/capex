﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('BandejaRecursoService', BandejaRecursoService);

    BandejaRecursoService.$inject = ['$http', 'URLS'];

    function BandejaRecursoService($http, $urls) {

        var service = {
            ListarRecursoPaginado: ListarRecursoPaginado,
            EliminarRecurso: EliminarRecurso,
            ModalRecurso: ModalRecurso
            
        };

        return service;

      
        function ListarRecursoPaginado(recurso) {
            return $http({
                url: $urls.ApiComun + "BandejaRecurso/ListarRecurso",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarRecurso(recurso) {
            return $http({
                url: $urls.ApiComun + "BandejaRecurso/EliminarRecurso",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalRecurso(recurso) {
            return $http({
                url: $urls.ApiComun + "RegistrarRecurso/Index",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();