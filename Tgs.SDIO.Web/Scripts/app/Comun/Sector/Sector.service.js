﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('SectorService', SectorService);

    SectorService.$inject = ['$http', 'URLS'];

    function SectorService($http, $urls) {

        var service = {
            ObtenerSector: ObtenerSector,
            ListarSector: ListarSector,
            ListarComboSector: ListarComboSector
        };

        return service;

        //Sector 
        function ObtenerSector(sector) {

            return $http({
                url: $urls.ApiComun + "Sector/ObtenerSector",
                method: "POST",
                data: JSON.stringify(sector)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


        function ListarSector(sector) {

            return $http({
                url: $urls.ApiComun + "Sector/ListarSector",
                method: "POST",
                data: JSON.stringify(sector)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarComboSector() {

            return $http({
                url: $urls.ApiComun + "Sector/ListarComboSector",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();