﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('FaseService', FaseService);

    FaseService.$inject = ['$http', 'URLS'];

    function FaseService($http, $urls) {

        var service = {
             ListarComboFases: ListarComboFases
        };

        return service;

      function ListarComboFases() {
            return $http({
                url: $urls.ApiComun + "Fase/ListarComboFases",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();