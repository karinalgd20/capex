﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('DireccionComercialService', DireccionComercialService);

    DireccionComercialService.$inject = ['$http', 'URLS'];

    function DireccionComercialService($http, $urls) {

        var service = {
            ObtenerDireccionComercial: ObtenerDireccionComercial
        };

        return service;

        //DireccionComercial
        function ObtenerDireccionComercial(direccionComercial) {

            return $http({
                url: $urls.ApiComun + "DireccionComercial/ObtenerDireccionComercial",
                method: "POST",
                data: JSON.stringify(direccionComercial)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();