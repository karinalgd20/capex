﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('CasoNegocioService', CasoNegocioService);

    CasoNegocioService.$inject = ['$http', 'URLS'];

    function CasoNegocioService($http, $urls) {

        var service = {
            ListarCasoNegocioBandeja: ListarCasoNegocioBandeja,
            EliminarCasoNegocioBandeja: EliminarCasoNegocioBandeja,
            ListarCasoNegocioDefecto: ListarCasoNegocioDefecto
        };

        return service;

        function ListarCasoNegocioBandeja(casonegocio) {

            return $http({
                url: $urls.ApiComun + "CasoNegocio/ListarCasoNegocioBandeja",
                method: "POST",
                data: JSON.stringify(casonegocio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarCasoNegocioBandeja(casonegocio) {

            return $http({
                url: $urls.ApiComun + "CasoNegocio/EliminarCasoNegocio",
                method: "POST",
                data: JSON.stringify(casonegocio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarCasoNegocioDefecto(casonegocio) {

            return $http({
                url: $urls.ApiComun + "CasoNegocio/ListarCasoNegocioDefecto",
                method: "POST",
                data: JSON.stringify(casonegocio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();