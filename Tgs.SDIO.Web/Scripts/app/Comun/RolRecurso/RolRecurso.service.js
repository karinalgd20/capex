﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('RolRecursoService', RolRecursoService);

    RolRecursoService.$inject = ['$http', 'URLS'];

    function RolRecursoService($http, $urls) {

        var service = {
            ListarComboRolRecurso: ListarComboRolRecurso,
            ObtenerRolRecursoPorId: ObtenerRolRecursoPorId,
            ActualizarRolRecurso: ActualizarRolRecurso,
            RegistrarRolRecurso: RegistrarRolRecurso

        };

        return service;

      function ListarComboRolRecurso() {
            return $http({
                url: $urls.ApiComun + "RolRecurso/ListarComboRolRecurso",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

      function ObtenerRolRecursoPorId(rolrecurso) {
            return $http({
                url: $urls.ApiComun + "RolRecurso/ObtenerRolRecursoPorId",
                method: "POST",
                data: JSON.stringify(rolrecurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

      function ActualizarRolRecurso(rolrecurso) {
            return $http({
                url: $urls.ApiComun + "RolRecurso/ActualizarRolRecurso",
                method: "POST",
                data: JSON.stringify(rolrecurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

      function RegistrarRolRecurso(rolrecurso) {
            return $http({
                url: $urls.ApiComun + "RolRecurso/RegistrarRolRecurso",
                method: "POST",
                data: JSON.stringify(rolrecurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();