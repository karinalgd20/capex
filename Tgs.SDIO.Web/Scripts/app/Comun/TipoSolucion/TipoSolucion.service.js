﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('TipoSolucionService', TipoSolucionService);

    TipoSolucionService.$inject = ['$http', 'URLS'];

    function TipoSolucionService($http, $urls) {

        var service = {
            ListarTipoSolucion: ListarTipoSolucion
        };

        return service;
        
        //TipoSolucion
        function ListarTipoSolucion(dto) {

            return $http({
                url: $urls.ApiComun + "TipoSolucion/ListarTipoSolucionPorProyecto",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
    }

})();