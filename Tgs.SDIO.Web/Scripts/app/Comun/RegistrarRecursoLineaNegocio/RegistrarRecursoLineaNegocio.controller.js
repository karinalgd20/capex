﻿(function () {
    'use strict'
    angular
    .module('app.Seguridad')
    .controller('RegistrarRecursoLineaNegocioController', RegistrarRecursoLineaNegocioController);

    RegistrarRecursoLineaNegocioController.$inject = ['RegistrarRecursoLineaNegocioService', 'LineaNegocioService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarRecursoLineaNegocioController(RegistrarRecursoLineaNegocioService,LineaNegocioService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vmreclinea = this;
        vmreclinea.ListarRecursoLineaNegocioPaginado = ListarRecursoLineaNegocioPaginado;
        vmreclinea.ListarLineaNegocios = ListarLineaNegocios;
        vmreclinea.RegistrarRecursoLineaNegocio = RegistrarRecursoLineaNegocio;
        vmreclinea.ActualizarEstadoRecursoLineaNegocio = ActualizarEstadoRecursoLineaNegocio;

        vmreclinea.IdLineaNegocio = '-1'; 
        vmreclinea.ListaLineaNegocio = [];

        vmreclinea.IdUsuario = $scope.$parent.vm.IdUsuario;
        vmreclinea.NombreUsuario = $scope.$parent.vm.NombreUsuario;
          
        vmreclinea.dtInstanceLineaNegocio = {};
        vmreclinea.dtColumnsLineaNegocio = [ 
            DTColumnBuilder.newColumn('DescripcionLinea').withTitle('DescripcionLinea').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('Estado').withTitle('Estado').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ]; 

        LimpiarGrilla();
        ListarLineaNegocios();
        ListarRecursoLineaNegocioPaginado();

        function ListarLineaNegocios() {

            var lineaNegocio = {
                IdEstado:1
            };

            var promise = LineaNegocioService.ListarLineaNegocios(lineaNegocio);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;
                vmreclinea.ListaLineaNegocio = UtilsFactory.AgregarItemSelect(respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function AccionesBusqueda(data, type, full, meta) {
            return "<a title='Cambiar Estado' class='btn btn-primary'  id='tab-button' class='btn '  ng-click='vmreclinea.ActualizarEstadoRecursoLineaNegocio(" + data.IdRecursoLineaNegocio + "," + data.IdEstado + ");'> <span class='fa fa-eye-slash fa-lg'></span></a>  ";
        }

        function ListarRecursoLineaNegocioPaginado() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vmreclinea.dtOptionsLineaNegocio = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(ListarRecursoLineaNegocio)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                       .withOption('createdRow', function (row, data, dataIndex) {
                           $compile(angular.element(row).contents())($scope);
                       })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }

        function ListarRecursoLineaNegocio(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var usuarioDtoRequest = {
                IdUsuarioRais: vmreclinea.IdUsuario,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = RegistrarRecursoLineaNegocioService.ListarRecursoLineaNegocioPaginado(usuarioDtoRequest);

            promise.then(function (resultado) {

                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaRecursoLineaNegocio
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function LimpiarGrilla() {
            vmreclinea.dtOptionsLineaNegocio = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('paging', false);
        }

        function ActualizarEstadoRecursoLineaNegocio(id, estado) {
            var etiquetaEstado = (estado == 1) ? "inactivar" : "activar";

            if (confirm('¿Estas seguro de ' + etiquetaEstado + ' el registro?')) {
                blockUI.start();

                var recursoLineaNegocioDtoRequest = {
                    IdEstado: (estado == 1) ? 0 : 1,
                    Id: id
                };

                var promise = RegistrarRecursoLineaNegocioService.ActualizarEstadoRecursoLineaNegocio(recursoLineaNegocioDtoRequest);

                promise.then(function (resultado) {

                    var respuesta = resultado.data;

                    if (respuesta.TipoRespuesta == 0) {
                        UtilsFactory.Alerta('#divAlertLineaNegocio', 'success', respuesta.Mensaje, 10);
                    } else {
                        UtilsFactory.Alerta('#divAlertLineaNegocio', 'danger', respuesta.Mensaje, 10);
                    }

                    ListarRecursoLineaNegocioPaginado();

                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertLineaNegocio', 'danger', MensajesUI.DatosError, 5);
                });
            }
        }

        function RegistrarRecursoLineaNegocio() {
            if (vmreclinea.IdLineaNegocio == '-1') {
                UtilsFactory.Alerta('#divAlertLineaNegocio', 'danger', "Seleccione una linea de negocio", 5);
                blockUI.stop();
                return;
            }

            blockUI.start();

            var recursoLineaNegocioDtoRequest = {
                IdLineaNegocio: vmreclinea.IdLineaNegocio,
                IdUsuarioRais: vmreclinea.IdUsuario
            };

            var promise = RegistrarRecursoLineaNegocioService.RegistrarRecursoLineaNegocio(recursoLineaNegocioDtoRequest);

            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;

                if (respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlertLineaNegocio', 'success', respuesta.Mensaje, 10);
                    ListarRecursoLineaNegocioPaginado();
                } else {
                    UtilsFactory.Alerta('#divAlertLineaNegocio', 'danger', respuesta.Mensaje, 10);
                } 

            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertLineaNegocio', 'danger', MensajesUI.DatosError, 5);
            });
        }

    }

})();