﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('RegistrarRecursoLineaNegocioService', RegistrarRecursoLineaNegocioService);

    RegistrarRecursoLineaNegocioService.$inject = ['$http', 'URLS'];

    function RegistrarRecursoLineaNegocioService($http, $urls) {

        var service = {
            ModalRegistrarRecursoLineaNegocio: ModalRegistrarRecursoLineaNegocio,
            RegistrarRecursoLineaNegocio: RegistrarRecursoLineaNegocio,
            ListarRecursoLineaNegocioPaginado: ListarRecursoLineaNegocioPaginado,
            ActualizarEstadoRecursoLineaNegocio: ActualizarEstadoRecursoLineaNegocio
        };

        return service;

        function ModalRegistrarRecursoLineaNegocio() {
            return $http({
                url: $urls.ApiComun + "RegistrarRecursoLineaNegocio/Index",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarRecursoLineaNegocio(reuqest) {
            return $http({
                url: $urls.ApiComun + "RegistrarRecursoLineaNegocio/RegistrarRecursoLineaNegocio",
                method: "POST",
                data: JSON.stringify(reuqest)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarRecursoLineaNegocioPaginado(reuqest) {
            return $http({
                url: $urls.ApiComun + "RegistrarRecursoLineaNegocio/ListarRecursoLineaNegocioPaginado",
                method: "POST",
                data: JSON.stringify(reuqest)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


        function ActualizarEstadoRecursoLineaNegocio(reuqest) {
            return $http({
                url: $urls.ApiComun + "RegistrarRecursoLineaNegocio/ActualizarEstadoRecursoLineaNegocio",
                method: "POST",
                data: JSON.stringify(reuqest)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }; 
    }
})();