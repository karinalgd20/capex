﻿(function () {
    'use strict',
    angular
        .module('app.Comun')
        .service('ArchivoService', ArchivoService);

    ArchivoService.$inject = ['$http', 'URLS'];

    function ArchivoService($http, $urls) {
        var service = {
            ListarArchivoPaginado: ListarArchivoPaginado,
            RegistrarArchivo: RegistrarArchivo,
            InactivarArchivo: InactivarArchivo,
            RegistrarArchivos: RegistrarArchivos
        };

        return service;

        function ListarArchivoPaginado(request) {
            return $http({
                url: $urls.ApiComun + "Archivo/ListarArchivoPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarArchivo(request) {
            return $http({
                url: $urls.ApiComun + "Archivo/RegistrarArchivo",
                method: "POST",
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined },
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarArchivos(request) {
            return $http({
                url: $urls.ApiComun + "Archivo/RegistrarArchivos",
                method: "POST",
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined },
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function InactivarArchivo(request) {
            return $http({
                url: $urls.ApiComun + "Archivo/InactivarArchivo",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();