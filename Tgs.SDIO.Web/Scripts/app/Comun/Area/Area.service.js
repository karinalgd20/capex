﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('AreaService', AreaService);

    AreaService.$inject = ['$http', 'URLS'];

    function AreaService($http, $urls) {

        var service = {
            ListarAreas: ListarAreas,
            ListarComboArea: ListarComboArea
        };

        return service;

        function ListarAreas(areaDtoRequest) {
            return $http({
                url: $urls.ApiComun + "Area/ListarAreas",
                method: "POST",
                data: JSON.stringify(areaDtoRequest)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarComboArea() {
            return $http({
                url: $urls.ApiComun + "Area/ListarComboArea",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();