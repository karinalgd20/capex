﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('EquipoTrabajoService', EquipoTrabajoService);

    EquipoTrabajoService.$inject = ['$http', 'URLS'];

    function EquipoTrabajoService($http, $urls) {

        var service = {
            ListarComboEquipoTrabajoAreas: ListarComboEquipoTrabajoAreas
        };

        return service;


        function ListarComboEquipoTrabajoAreas() {
            return $http({
                url: $urls.ApiComun + "EquipoTrabajo/ListarComboEquipoTrabajoAreas",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();