﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('CargoService', CargoService);

    CargoService.$inject = ['$http', 'URLS'];

    function CargoService($http, $urls) {

        var service = {
            ListarCargos: ListarCargos,
            ListarComboCargo: ListarComboCargo
            
        };

        return service;

        function ListarCargos(cargoDtoRequest) {
            return $http({
                url: $urls.ApiComun + "Cargo/ListarCargos",
                method: "POST",
                data: JSON.stringify(cargoDtoRequest)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarComboCargo() {
            return $http({
                url: $urls.ApiComun + "Cargo/ListarComboCargo",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();