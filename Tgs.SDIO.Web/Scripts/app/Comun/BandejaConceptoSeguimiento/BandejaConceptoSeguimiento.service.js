﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('BandejaConceptoSeguimientoService', BandejaConceptoSeguimientoService);

    BandejaConceptoSeguimientoService.$inject = ['$http', 'URLS'];

    function BandejaConceptoSeguimientoService($http, $urls) {

        var service = {
            ListarConceptoSeguimientoPaginado: ListarConceptoSeguimientoPaginado,
            EliminarConceptoSeguimiento: EliminarConceptoSeguimiento,
            ModalConceptoSeguimiento: ModalConceptoSeguimiento
        };

        return service;


        function ListarConceptoSeguimientoPaginado(concepto) {
            return $http({
                url: $urls.ApiComun + "BandejaConceptoSeguimiento/ListarConceptoSeguimiento",
                method: "POST",
                data: JSON.stringify(concepto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarConceptoSeguimiento(concepto) {
            return $http({
                url: $urls.ApiComun + "BandejaConceptoSeguimiento/EliminarConceptoSeguimiento",
                method: "POST",
                data: JSON.stringify(concepto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalConceptoSeguimiento(concepto) {
            return $http({
                url: $urls.ApiComun + "RegistrarConceptoSeguimiento/Index",
                method: "POST",
                data: JSON.stringify(concepto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();