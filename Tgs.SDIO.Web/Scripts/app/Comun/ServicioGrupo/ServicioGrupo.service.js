﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('ServicioGrupoService', ServicioGrupoService);

    ServicioGrupoService.$inject = ['$http', 'URLS'];

    function ServicioGrupoService($http, $urls) {

        var service = {
            ListarServicioGrupo : ListarServicioGrupo
        };

        return service;

        function ListarServicioGrupo(servicio) {

            return $http({
                url: $urls.ApiComun + "ServicioGrupo/ListarServicioGrupo",
                method: "POST",
                data: JSON.stringify(servicio)
            }).then(DatosCompletados);ListarServicioGrupo

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();