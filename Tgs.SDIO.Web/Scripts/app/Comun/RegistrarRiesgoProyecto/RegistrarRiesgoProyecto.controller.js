﻿(function () {
    'use strict'
    angular
        .module('app.Comun')
        .controller('RegistrarRiesgoProyecto', RegistrarRiesgoProyecto);

    RegistrarRiesgoProyecto.$inject =
        [
            'MaestraService',
            'RiesgoProyectoService',
            'blockUI',
            'UtilsFactory',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'
        ];

    function RegistrarRiesgoProyecto
        (
        MaestraService,
        RiesgoProyectoService,
        blockUI,
        UtilsFactory,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile,
        $modal,
        $injector
        ) {

        var vm = this;

        vm.Id = 0;
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;
        vm.IdTipo = '-1';
        vm.FechaDeteccion = null;
        vm.Descripcion = null;
        vm.Consecuencias = null;
        vm.PlanAccion = null;
        vm.Responsable = null;
        vm.IdProbabilidad = '-1';
        vm.IdImpacto = '-1';
        vm.IdNivel = 0;
        vm.FechaCierre = null;
        vm.IdEstado = '-1';
        vm.disabledIdEstado = true;

        vm.ListTipoRiesgoProyecto = [];
        vm.ListProbabilidad = [];
        vm.ListImpacto = [];
        vm.ListEstado = [];

        ListarTipoRiesgoProyecto();
        ListarEstadoRiesgoProyecto();
        ListarProbabilidad();
        ListarImpacto();
        CargaModalSubServicio();

        vm.RegistrarRiesgoProyecto = RegistrarRiesgoProyecto;
        vm.IdEstado_SelectedIndexChanged = IdEstado_SelectedIndexChanged;

        function ListarTipoRiesgoProyecto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 313
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListTipoRiesgoProyecto = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarProbabilidad() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 320
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListProbabilidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarImpacto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 326
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListImpacto = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarEstadoRiesgoProyecto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 316
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListEstado = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function ValidarCampos() {
            if (vm.IdOportunidad == 0) {
                UtilsFactory.Alerta('#divAlertRegistrarRiesgoProyecto', 'warning', 'Seleccione la oportunidad', 5);
                return;
            }

            if (vm.IdTipo == '-1') {
                UtilsFactory.Alerta('#divAlertRegistrarRiesgoProyecto', 'warning', 'Seleccione el tipo', 5);
                return;
            }

            if (vm.FechaDeteccion == undefined || vm.FechaDeteccion.trim() == '') {
                UtilsFactory.Alerta('#divAlertRegistrarRiesgoProyecto', 'warning', 'Ingrese la fecha de detección', 5);
                return;
            }

            if (vm.Descripcion == undefined || vm.Descripcion.trim() == '') {
                UtilsFactory.Alerta('#divAlertRegistrarRiesgoProyecto', 'warning', 'Ingrese la descripcion', 5);
                return;
            }

            if (vm.Consecuencias == undefined || vm.Consecuencias.trim() == '') {
                UtilsFactory.Alerta('#divAlertRegistrarRiesgoProyecto', 'warning', 'Ingrese la consecuencia', 5);
                return;
            }

            if (vm.PlanAccion == undefined || vm.PlanAccion.trim() == '') {
                UtilsFactory.Alerta('#divAlertRegistrarRiesgoProyecto', 'warning', 'Ingrese el plan de accion', 5);
                return;
            }

            if (vm.Responsable == undefined || vm.Responsable.trim() == '') {
                UtilsFactory.Alerta('#divAlertRegistrarRiesgoProyecto', 'warning', 'Ingrese el responsable', 5);
                return;
            }

            if (vm.IdProbabilidad == '-1') {
                UtilsFactory.Alerta('#divAlertRegistrarRiesgoProyecto', 'warning', 'Seleccione la probabilidad', 5);
                return;
            }

            if (vm.IdImpacto == '-1') {
                UtilsFactory.Alerta('#divAlertRegistrarRiesgoProyecto', 'warning', 'Seleccione el impacto', 5);
                return;
            }

            if (vm.IdEstado == '-1') {
                UtilsFactory.Alerta('#divAlertRegistrarRiesgoProyecto', 'warning', 'Seleccione el estado', 5);
                return;
            } else {
                if (vm.IdEstado == '302' && !UtilsFactory.ValidarFecha(vm.FechaCierre)) {
                    UtilsFactory.Alerta('#divAlertRegistrarRiesgoProyecto', 'warning', 'Ingrese la fecha de cierre', 5);
                    return;
                }
            }

            return true;
        }

        function RegistrarRiesgoProyecto() {
            if (!ValidarCampos())
                return;

            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();

                var riesgoProyecto = {
                    Id: vm.Id,
                    IdOportunidad: vm.IdOportunidad,
                    IdTipo: vm.IdTipo,
                    FechaDeteccion: vm.FechaDeteccion,
                    Descripcion: vm.Descripcion,
                    Consecuencias: vm.Consecuencias,
                    PlanAccion: vm.PlanAccion,
                    Responsable: vm.Responsable,
                    IdProbabilidad: vm.IdProbabilidad,
                    IdImpacto: vm.IdImpacto,
                    IdNivel: 0,
                    FechaCierre: vm.FechaCierre,
                    IdEstado: vm.IdEstado
                };

                var promise = (vm.Id == 0) ?
                    RiesgoProyectoService.RegistrarRiesgoProyecto(riesgoProyecto) :
                    RiesgoProyectoService.ActualizarRiesgoProyecto(riesgoProyecto);

                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    vm.Id = Respuesta.Id;
                    UtilsFactory.Alerta('#divAlertRegistrarRiesgoProyecto', 'success', Respuesta.Mensaje, 5);
                    $scope.$parent.vm.ListarRiesgoProyecto();
                    $("#ModalRegistrarRiesgoProyecto").modal('hide');

                }, function (response) {
                    blockUI.stop();
                });
            }
        }

        function CargaModalSubServicio() {
            vm.Id = $scope.$parent.vm.Id;
            if (vm.Id > 0) ObtenerRiesgoProyectoPorId();
        }

        function ObtenerRiesgoProyectoPorId() {
            blockUI.start();

            var riesgoProyecto = { Id: vm.Id };
            var promise = RiesgoProyectoService.ObtenerRiesgoProyectoPorId(riesgoProyecto);

            promise.then(function (resultado) {
                blockUI.stop();

                var riesgoProyecto = resultado.data;
                vm.IdTipo = riesgoProyecto.IdTipo;
                vm.FechaDeteccion = riesgoProyecto.FechaDeteccion;
                vm.Descripcion = riesgoProyecto.Descripcion;
                vm.Consecuencias = riesgoProyecto.Consecuencias;
                vm.PlanAccion = riesgoProyecto.PlanAccion;
                vm.Responsable = riesgoProyecto.Responsable;
                vm.IdProbabilidad = riesgoProyecto.IdProbabilidad;
                vm.IdImpacto = riesgoProyecto.IdImpacto;
                vm.FechaCierre = riesgoProyecto.FechaCierre;
                vm.IdEstado = riesgoProyecto.IdEstado;
                
            }, function (response) {
                blockUI.stop();
            });
        }

        function CalcularNivel() {

        }

        function IdEstado_SelectedIndexChanged() {
            if (vm.IdEstado != 302) {
                vm.FechaCierre = null;
                vm.disabledIdEstado = true;

            } else {
                vm.disabledIdEstado = false;
            }
        }
    }
})();