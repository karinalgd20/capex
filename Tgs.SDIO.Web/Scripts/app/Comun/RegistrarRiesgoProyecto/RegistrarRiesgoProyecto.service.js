﻿(function () {
    'use strict',
    angular
        .module('app.Comun')
        .service('RegistrarRiesgoProyectoService', RegistrarRiesgoProyectoService);

    RegistrarRiesgoProyectoService.$inject = ['$http', 'URLS'];

    function RegistrarRiesgoProyectoService($http, $urls) {
        var service = {
            ModalRiesgoProyecto: ModalRiesgoProyecto
        };

        return service;

        function ModalRiesgoProyecto(riesgoProyecto) {
            return $http({
                url: $urls.ApiComun + "RegistrarRiesgoProyecto/Index",
                method: "POST",
                data: JSON.stringify(riesgoProyecto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();