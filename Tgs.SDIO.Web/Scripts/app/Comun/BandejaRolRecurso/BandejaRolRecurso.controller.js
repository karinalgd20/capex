﻿(function () {
    'use strict'
    angular
    .module('app.Comun')
    .controller('BandejaRolRecurso', BandejaRolRecurso);
    BandejaRolRecurso.$inject = ['BandejaRolRecursoService', 'RolRecursoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function BandejaRolRecurso(BandejaRolRecursoService, RolRecursoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiComun + "RegistrarRolRecurso/Index/";
        vm.BuscarRolRecurso = BuscarRolRecurso;
        vm.EliminarRolRecurso = EliminarRolRecurso;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.Descripcion = "";
        vm.IdRol = "";
        LimpiarGrilla();
        vm.dtColumns = [
          DTColumnBuilder.newColumn('IdRol').withTitle('Código').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Descripcion').withTitle('Descripción').notSortable().withOption('width', '60%'),
          DTColumnBuilder.newColumn('Nivel').withTitle('Nivel').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];
        //Modal registrar

        vm.ModalRolRecurso = ModalRolRecurso;

        function ModalRolRecurso(IdRol) {
            vm.IdRol = IdRol;
            var rol = {
                IdRol: IdRol
            };

            var promise = BandejaRolRecursoService.ModalRolRecurso(rol);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoRolRecurso").html(content);
                });

                $('#ModalRegistrarRolRecurso').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };
         //Eliminacion logica de la tabla [TRAZABILIDAD].[[RolRecurso]] 
        function EliminarRolRecurso(IdRol) {
            blockUI.start();
            if (confirm('¿Estas seguro que desea eliminar?')) {
                var objRol = {
                    IdRol: IdRol
                }
                blockUI.start();
                var promise = BandejaRolRecursoService.EliminarRolRecurso(objRol);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                        BuscarRolRecurso()
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                    }
                    modalpopupConfirm.modal('hide');
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            }
            else {
                blockUI.stop();
            }
        }      
         //Filtro de la  tabla [TRAZABILIDAD].[[RolRecurso]] por Descripcion.
        function BuscarRolRecurso() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarRolRecursoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                  .withOption('createdRow', function (row, data, dataIndex) {
                      $compile(angular.element(row).contents())($scope);
                  })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }
        function BuscarRolRecursoPaginado(sSource, aoData, fnCallback, oSettings) { 
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start+length)/length;
            var rol = {
                Descripcion: vm.Descripcion,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = BandejaRolRecursoService.ListarRolRecursoPaginado(rol);         
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListRolRecursoDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
         //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
         //Inicializa  html de la columna Accion.
        function AccionesBusqueda(data, type, full, meta) {
            var respuesta = "";
            respuesta = respuesta + " <a title='Editar'   ng-click='vm.ModalRolRecurso(" + data.IdRol + ");'>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + "<a title='Eliminar'  onclick='EliminarRolRecurso(" + data.IdRol + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6; margin-left: 1px;'></span></a>";
            return respuesta;
        }
         //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.Descripcion = "";
            LimpiarGrilla();
        }
    }
})();