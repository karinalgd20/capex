﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('BandejaRolRecursoService', BandejaRolRecursoService);

    BandejaRolRecursoService.$inject = ['$http', 'URLS'];

    function BandejaRolRecursoService($http, $urls) {

        var service = {
            ListarRolRecursoPaginado: ListarRolRecursoPaginado,
            EliminarRolRecurso: EliminarRolRecurso,
            ModalRolRecurso: ModalRolRecurso
            
        };

        return service;


        function ListarRolRecursoPaginado(rol) {
            return $http({
                url: $urls.ApiComun + "BandejaRolRecurso/ListarRolRecurso",
                method: "POST",
                data: JSON.stringify(rol)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarRolRecurso(rol) {
            return $http({
                url: $urls.ApiComun + "BandejaRolRecurso/EliminarRolRecurso",
                method: "POST",
                data: JSON.stringify(rol)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalRolRecurso(rol) {
            return $http({
                url: $urls.ApiComun + "RegistrarRolRecurso/Index",
                method: "POST",
                data: JSON.stringify(rol)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();