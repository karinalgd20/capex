﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('SalesForceConsolidadoDetalleService', SalesForceConsolidadoDetalleService);

    SalesForceConsolidadoDetalleService.$inject = ['$http', 'URLS'];

    function SalesForceConsolidadoDetalleService($http, $urls) {

        var service = {
            //Servicios
      ObtenerSalesForceConsolidadoDetalle:ObtenerSalesForceConsolidadoDetalle,
      ListaNumerodeCasoPorNumeroSalesForceDetalle:ListaNumerodeCasoPorNumeroSalesForceDetalle
        };

        return service;


 
        function ObtenerSalesForceConsolidadoDetalle(request) {

            return $http({
                url: $urls.ApiComun + "SalesForceConsolidadoDetalle/ObtenerSalesForceConsolidadoDetalle",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

         function ListaNumerodeCasoPorNumeroSalesForceDetalle(request) {

            return $http({
                url: $urls.ApiComun + "SalesForceConsolidadoDetalle/ListaNumerodeCasoPorNumeroSalesForceDetalle",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();


