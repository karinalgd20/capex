﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('ActividadService', ActividadService);

    ActividadService.$inject = ['$http', 'URLS'];

    function ActividadService($http, $urls) {

        var service = {
            ListarComboActividadPadres: ListarComboActividadPadres,
            ObtenerActividadPorId: ObtenerActividadPorId,
            ActualizarActividad: ActualizarActividad,
            RegistrarActividad: RegistrarActividad
        };

        return service;

        function ListarComboActividadPadres() {
            return $http({
                url: $urls.ApiComun + "Actividad/ListarComboActividadPadres",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerActividadPorId(Actividad) {
            return $http({
                url: $urls.ApiComun + "Actividad/ObtenerActividadPorId",
                method: "POST",
                data: JSON.stringify(Actividad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarActividad(Actividad) {
            debugger;
            return $http({
                url: $urls.ApiComun + "Actividad/ActualizarActividad",
                method: "POST",
                data: JSON.stringify(Actividad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarActividad(Actividad) {
            return $http({
                url: $urls.ApiComun + "Actividad/RegistrarActividad",
                method: "POST",
                data: JSON.stringify(Actividad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();