﻿(function () {
    'use strict'
    angular
    .module('app.Comun')
    .controller('RegistrarRecurso', RegistrarRecurso);
    RegistrarRecurso.$inject = ['RegistrarRecursoService', 'MaestraService', 'RecursoService','RolRecursoService','EmpresaService','CargoService','AreaService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RegistrarRecurso(RegistrarRecursoService, MaestraService, RecursoService,RolRecursoService,EmpresaService,CargoService,AreaService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        //vm.CargarDatos = CargarDatos;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.IdRecurso = "";
        vm.Nombre = "";
        vm.IdTipoRecurso = "-1";

        vm.IdArea = "-1";
        vm.IdEmpresa = "-1";
        vm.IdCargo = "-1";
        vm.IdRolDefecto = "-1";

        vm.IdEstado = "1";
        ListarTipoRecurso();
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        vm.CargaModalRecurso = CargaModalRecurso;
        CargaModalRecurso();
        ListarComboRolRecurso();
        ListarComboArea();
        ListarComboCargoRecurso();
        ListarComboEmpresa();
        //Lista los IdFase y Descripciones de la tabla [TRAZABILIDAD].[RolRecurso] que esten Activos.
        function ListarComboRolRecurso() {
            blockUI.start();
            var promise = RolRecursoService.ListarComboRolRecurso();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Roles", 5);
                } else {
                    vm.listRolRecurso = UtilsFactory.AgregarItemSelect(respuesta);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdFase y Descripciones de la tabla [TRAZABILIDAD].[Area] que esten Activos.
        function ListarComboArea() {
            blockUI.start();
            var promise = AreaService.ListarComboArea();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Areas", 5);
                } else {
                    vm.listArea = UtilsFactory.AgregarItemSelect(respuesta);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdFase y Descripciones de la tabla [TRAZABILIDAD].[Cargo] que esten Activos.
        function ListarComboCargoRecurso() {
            blockUI.start();
            var promise = CargoService.ListarComboCargo();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Cargos", 5);
                } else {
                    vm.listCargo = UtilsFactory.AgregarItemSelect(respuesta);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdFase y Descripciones de la tabla [TRAZABILIDAD].[Empresa] que esten Activos.
        function ListarComboEmpresa() {
            blockUI.start();
            var promise = EmpresaService.ListarComboEmpresa();
            promise.then(function (response) {
                debugger;
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Empresas", 5);
                } else {
                    vm.listEmpresa = UtilsFactory.AgregarItemSelect(respuesta);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si es existente.
        function CargaModalRecurso() {
            vm.IdRecurso = $scope.$parent.vm.IdRecurso;
            if (vm.IdRecurso > 0) {
                ObtenerRecurso();
            } else {
                CargaModal();
            }
        }

        function ObtenerRecurso() {
            blockUI.start();
            var recurso = {
                IdRecurso: vm.IdRecurso
            }
            var promise = RecursoService.ObtenerRecursoPorId(recurso);

            promise.then(function (resultado) {
                debugger;
                blockUI.stop();
                var Respuesta = resultado.data;
                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdRecurso = Respuesta.IdRecurso;
                vm.Nombre = Respuesta.Nombre;
                vm.IdTipoRecurso = Respuesta.TipoRecurso;
                vm.IdEstado = Respuesta.IdEstado;
                vm.UserNameSF = Respuesta.UserNameSF;
                vm.DNI = Respuesta.DNI;
                vm.Email = Respuesta.Email;
                if (Respuesta.IdRolDefecto == null || Respuesta.IdRolDefecto == 0) {
                    vm.IdRolDefecto = "-1";
                }else{
                    vm.IdRolDefecto = Respuesta.IdRolDefecto;
                }
                if (Respuesta.IdCargo == null || Respuesta.IdCargo==0) {
                    vm.IdCargo = "-1";
                } else {
                    vm.IdCargo = Respuesta.IdCargo;
                }
                if (Respuesta.IdEmpresa == null || Respuesta.IdEmpresa==0) {
                    vm.IdEmpresa = "-1";
                } else {
                    vm.IdEmpresa = Respuesta.IdEmpresa;
                }
                if (Respuesta.IdArea == null || Respuesta.IdArea == 0) {
                    vm.IdArea = "-1";
                } else {
                    vm.IdArea = Respuesta.IdArea;
                }
            }, function (response) {
                blockUI.stop();

            });
        }

        function CargaModal() {
                    vm.IdRecurso = "";
                    vm.Nombre = "";
                    vm.IdTipoRecurso = "-1";
                    vm.IdEstado = "1";
        }
        //Registra en  la tabla [TRAZABILIDAD].[Recurso] 
        vm.RegistrarRecurso = RegistrarRecurso;
        function RegistrarRecurso() {
            var IdRolDefecto = null;
            var IdCargo = null;
            var IdEmpresa = null;
            if (vm.IdRolDefecto != "-1") {
                IdRolDefecto = vm.IdRolDefecto;
            }
            if (vm.IdCargo != "-1") {
                IdCargo = vm.IdCargo;
            }
            if (vm.IdEmpresa != "-1") {
                IdEmpresa = vm.IdEmpresa;
            }
            var recurso = {
               IdRecurso: vm.IdRecurso,
               Nombre: vm.Nombre,
               TipoRecurso: vm.IdTipoRecurso,
               IdEstado: vm.IdEstado,
               UserNameSF: vm.UserNameSF,
               DNI: vm.DNI,
               Email: vm.Email,
               IdRolDefecto: IdRolDefecto,
               IdCargo: IdCargo,
               IdEmpresa: IdEmpresa,
               IdArea: vm.IdArea
            }
            debugger;
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promisePreventa = (vm.IdRecurso > 0) ? RecursoService.ActualizarRecurso(recurso) : RecursoService.RegistrarRecurso(recurso);

                promisePreventa.then(function (resultadoPreventa) {
                    var RespuestaPreventa = resultadoPreventa.data;
                    if (RespuestaPreventa.TipoRespuesta == 0) {
                           $scope.$parent.vm.BuscarRecurso();
                           $('#ModalRegistrarRecurso').modal('hide');
                           blockUI.stop();
                           UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar el Recurso.", 5);
                    }

                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            } else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarTipoRecurso() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 125
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listRecursos = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        //Limpia los controles del formulacion a sus valores por defecto.
        function LimpiarCampos() {
            vm.IdRecurso = "";
            vm.Nombre = "";
            vm.IdTipoRecurso = "-1";
            vm.IdEstado = "-1";
            vm.IdArea = "-1";
            vm.IdEmpresa = "-1";
            vm.IdCargo = "-1";
            vm.IdRolDefecto = "-1";
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.Nombre) == "") {
                mensaje = mensaje + "Ingrese Nombre" + '<br/>';
                UtilsFactory.InputBorderColor('#txtNombre', 'Rojo');
            }
            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }
            if ($.trim(vm.IdTipoRecurso) == "-1") {
                mensaje = mensaje + "Elija Tipo Recruso" + '<br/>';
                UtilsFactory.InputBorderColor('#IdTipoRecurso', 'Rojo');
            }
            if ($.trim(vm.IdArea) == "-1") {
                mensaje = mensaje + "Elija Area" + '<br/>';
                UtilsFactory.InputBorderColor('#IdArea', 'Rojo');
            }
            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#txtNombre', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdTipoRecurso', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdArea', 'Ninguno');
        }
    }
})();