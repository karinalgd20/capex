﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('RegistrarRecursoService', RegistrarRecursoService);

    RegistrarRecursoService.$inject = ['$http', 'URLS'];

    function RegistrarRecursoService($http, $urls) {

        var service = {
            RegistrarRecurso: RegistrarRecurso
        };

        return service;


        function RegistrarRecurso(recurso) {
            return $http({
                url: $urls.ApiComun + "RegistrarRecurso/RegistrarRecurso",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };    

    }
})();