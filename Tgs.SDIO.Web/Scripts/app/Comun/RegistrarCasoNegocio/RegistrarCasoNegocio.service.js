﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('RegistrarCasoNegocioService', RegistrarCasoNegocioService);

    RegistrarCasoNegocioService.$inject = ['$http', 'URLS'];

    function RegistrarCasoNegocioService($http, $urls) {

        var service = {
            ModalCasoNegocio: ModalCasoNegocio,
            RegistrarCaso: RegistrarCaso,
            ActualizarCaso: ActualizarCaso,
            ObtenerCaso: ObtenerCaso,
            RegistrarCasoNegocioServicio: RegistrarCasoNegocioServicio,
            EliminarCasoNegocioServicio: EliminarCasoNegocioServicio
        };

        return service;


        function ModalCasoNegocio(dto) {
            return $http({
                url: $urls.ApiComun + "RegistrarCasoNegocio/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
        

        function RegistrarCaso(casoNegocio) {
            return $http({
                url: $urls.ApiComun + "RegistrarCasoNegocio/RegistrarCasoNegocio",
                method: "POST",
                data: JSON.stringify(casoNegocio)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };


        function ActualizarCaso(casoNegocio) {
            return $http({
                url: $urls.ApiComun + "RegistrarCasoNegocio/ActualizarCasoNegocio",
                method: "POST",
                data: JSON.stringify(casoNegocio)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ObtenerCaso(casoNegocio) {
            return $http({
                url: $urls.ApiComun + "RegistrarCasoNegocio/ObtenerCasoNegocio",
                method: "POST",
                data: JSON.stringify(casoNegocio)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function RegistrarCasoNegocioServicio(casoServicio) {
            return $http({
                url: $urls.ApiComun + "RegistrarCasoNegocio/RegistrarCasoNegocioServicio",
                method: "POST",
                data: JSON.stringify(casoServicio)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function EliminarCasoNegocioServicio(casoServicio) {
            return $http({
                url: $urls.ApiComun + "RegistrarCasoNegocio/EliminarCasoNegocioServicio",
                method: "POST",
                data: JSON.stringify(casoServicio)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

    }

})();