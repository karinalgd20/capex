﻿(function () {
    'use strict'

    angular
    .module('app.Comun')
    .controller('RegistrarCasoNegocioController', RegistrarCasoNegocioController);


    RegistrarCasoNegocioController.$inject = ['RegistrarCasoNegocioService', 'ServicioService', 'LineaNegocioService', 'CasoNegocioServicioService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarCasoNegocioController(RegistrarCasoNegocioService, ServicioService, LineaNegocioService, CasoNegocioServicioService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.BuscarServicios = BuscarServicios;
        vm.GrabarCasoNegocioServicio = GrabarCasoNegocioServicio;
        vm.GrabarCasoNegocio = GrabarCasoNegocio;
        vm.EliminarServicios = EliminarServicios;
        vm.ListarServicio = ListarServicio;
        vm.ListarLineaNegocio = ListarLineaNegocio;
        vm.CargarVista = CargarVista;

        vm.ListServicios = [];
        vm.ListLineaNegocio = [];
        vm.Codigo = '-1';
        vm.Id = 0;
        vm.entidad = null;
        vm.CasoUso = '';
        vm.Predeterminada = '0';
        vm.IdLineaNegocio = '-1';
        vm.IdServicios = '-1';

        vm.AccionGrabar = true;
        /******************************************* Tabla - Servicios *******************************************/
        vm.dataMasiva = [];
        vm.v = {};
        vm.dtColumnsServicios = [
         DTColumnBuilder.newColumn('IdCasoNegocioServicio').notVisible(),
         DTColumnBuilder.newColumn('IdServicio').notVisible(),
         DTColumnBuilder.newColumn('Servicio').withTitle('Descripcion').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesServicios)

        ];

        function AccionesServicios(data, type, full, meta) {

            var respuesta = "<div class='col-xs-4 col-sm-1'><a title='Cancelar' class='btn btn-sm' id='btnEliminarServicio' ng-click='vm.EliminarServicios(\"" + data.IdCasoNegocioServicio + "\",\"" + data.IdServicio + "\");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarServicios() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsServicios = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarServiciosPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarServiciosPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;

            var casoServicio = {
                IdCasoNegocio: vm.Id,
                IdEstado:1
            };

            var promise = CasoNegocioServicioService.ListarCasoNegocioServicio(casoServicio);
            promise.then(function (response) {

                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };
                blockUI.stop();
                fnCallback(records);
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsServicios = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        };

        /******************************************* Metodos *******************************************/

        function ListarServicio() {

            var servicio = {
                IdEstado: 1
            };

            var promise = ServicioService.ListarServicios(servicio);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListServicios = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();

            });
        }


        function ListarLineaNegocio() {

            var linea = {
                IdEstado: 1
            };

            var promise = LineaNegocioService.ListarLineaNegocios(linea);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);
                
            }, function (response) {
                blockUI.stop();

            });
        }


        function GrabarCasoNegocio() {

            var casoNegocio = {
                IdCasoNegocio: vm.Id,
                IdLineaNegocio: vm.IdLineaNegocio,
                FlagDefecto: vm.Predeterminada,
                Descripcion: vm.CasoUso
            };
            var promise = (vm.Id > 0) ? RegistrarCasoNegocioService.ActualizarCaso(casoNegocio) : RegistrarCasoNegocioService.RegistrarCaso(casoNegocio);

            promise.then(function (response) {
                blockUI.stop();
                var Respuesta = response.data;

                if (Respuesta.TipoRespuesta != 0) {
                    UtilsFactory.Alerta('#divAlertRegistrar', 'danger', Respuesta.Mensaje, 20);
                } else {
                    vm.Id = Respuesta.Id;
                    if (vm.Id != 0) {
                        InactivarCampos();
                        vm.AccionGrabar = false;
                    }
                    UtilsFactory.Alerta('#divAlertRegistrar', 'success', Respuesta.Mensaje, 5);
                    $scope.$parent.vm.BuscarCasoNegocio();
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', MensajesUI.DatosError, 5);
            });

        };

        function GrabarCasoNegocioServicio() {

            var casoServicio = {
                IdCasoNegocio: vm.Id,
                IdServicio: vm.IdServicios
            };
            var promise = RegistrarCasoNegocioService.RegistrarCasoNegocioServicio(casoServicio);

            promise.then(function (response) {
                blockUI.stop();
                var Respuesta = response.data;
                if (Respuesta.TipoRespuesta != 0) {
                    UtilsFactory.Alerta('#divAlertDetalle', 'danger', Respuesta.Mensaje, 20);
                } else {

                    UtilsFactory.Alerta('#divAlertDetalle', 'success', Respuesta.Mensaje, 10);
                    BuscarServicios(); 
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertDetalle', 'danger', MensajesUI.DatosError, 5);
            });

        };
        
        function EliminarServicios(IdCasoNegocioServicio, IdServicio) {

            var casoServicio = {
                IdCasoNegocio: vm.Id,
                IdCasoNegocioServicio: IdCasoNegocioServicio,   
                IdServicio: IdServicio
            };
            var promise = RegistrarCasoNegocioService.EliminarCasoNegocioServicio(casoServicio);

            promise.then(function (response) {
                blockUI.stop();
                var Respuesta = response.data;
                if (Respuesta.TipoRespuesta != 0) {
                    UtilsFactory.Alerta('#divAlertRegistrar', 'danger', Respuesta.Mensaje, 20);
                } else {

                    UtilsFactory.Alerta('#divAlertRegistrar', 'success', Respuesta.Mensaje, 10);
                    BuscarServicios();
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', MensajesUI.DatosError, 5);
            });

           
        };

        function InactivarCampos() {
            UtilsFactory.DeshabilitarElemento(txtCasoUso);
        };

        function LimpiarCampos() {

        };

        function CargarVista() {
            vm.Id = $scope.$parent.vm.IdCasoNegocio;
            
            if (vm.Id > 0) {
                vm.AccionGrabar =false;
                vm.IdLineaNegocio = $scope.$parent.vm.IdLineaNegocio;
                console.log($scope.$parent.vm.IdCasoNegocio);
                CargarCasoNegocio();
                BuscarServicios();
            }
        };

        function CargarCasoNegocio() {

            var casoNegocio = {
                IdCasoNegocio: vm.Id,
                IdLineaNegocio: vm.IdLineaNegocio
            };

            var promise = RegistrarCasoNegocioService.ObtenerCaso(casoNegocio);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.CasoUso = Respuesta.Descripcion;
                vm.Predeterminada = Respuesta.FlagDefecto;
            }, function (response) {
                blockUI.stop();

            });
        }


        /******************************************* Load *******************************************/
        CargarVista();
        BuscarServicios();
        ListarServicio();
        ListarLineaNegocio();
    }

})();