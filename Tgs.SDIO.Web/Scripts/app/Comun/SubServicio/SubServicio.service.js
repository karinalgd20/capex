﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('SubServicioService', SubServicioService);

    SubServicioService.$inject = ['$http', 'URLS'];

    function SubServicioService($http, $urls) {

        var service = {
            //SubServicios
            ListarSubServicios: ListarSubServicios,
            ObtenerSubServicio: ObtenerSubServicio,
            RegistrarSubServicio: RegistrarSubServicio,
            ActualizarSubServicio: ActualizarSubServicio,
            ListaSubServicioPaginado: ListaSubServicioPaginado,
            InhabilitarSubServicio:InhabilitarSubServicio,
            ListarSubServiciosPorIdGrupo :ListarSubServiciosPorIdGrupo
        };

        return service;
        function ListarSubServiciosPorIdGrupo(subServicio) {

            return $http({
                url: $urls.ApiComun + "SubServicio/ListarSubServiciosPorIdGrupo",
                method: "POST",
                data: JSON.stringify(subServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function InhabilitarSubServicio(subServicio) {

            return $http({
                url: $urls.ApiComun + "SubServicio/InhabilitarSubServicio",
                method: "POST",
                data: JSON.stringify(subServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        //SubServicios
        function ListarSubServicios(subServicio) {

            return $http({
                url: $urls.ApiComun + "SubServicio/ListarSubServicios",
                method: "POST",
                data: JSON.stringify(subServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ObtenerSubServicio(subServicio) {

            return $http({
                url: $urls.ApiComun + "SubServicio/ObtenerSubServicio",
                method: "POST",
                data: JSON.stringify(subServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function RegistrarSubServicio(subServicio) {

            return $http({
                url: $urls.ApiComun + "SubServicio/RegistrarSubServicio",
                method: "POST",
                data: JSON.stringify(subServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ActualizarSubServicio(subServicio) {

            return $http({
                url: $urls.ApiComun + "SubServicio/ActualizarSubServicio",
                method: "POST",
                data: JSON.stringify(subServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListaSubServicioPaginado(subServicio) {

            return $http({
                url: $urls.ApiComun + "SubServicio/ListaSubServicioPaginado",
                method: "POST",
                data: JSON.stringify(subServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }

})();