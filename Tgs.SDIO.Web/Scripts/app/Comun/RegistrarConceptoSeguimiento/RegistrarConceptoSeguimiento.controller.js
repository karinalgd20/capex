﻿(function () {
    'use strict'
    angular
    .module('app.Comun')
    .controller('RegistrarConceptoSeguimiento', RegistrarConceptoSeguimiento);
    RegistrarConceptoSeguimiento.$inject = ['RegistrarConceptoSeguimientoService', 'ConceptoSeguimientoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RegistrarConceptoSeguimiento(RegistrarConceptoSeguimientoService, ConceptoSeguimientoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.RegistraConceptoSeguimiento = RegistraConceptoSeguimiento;
        vm.EnlaceBandeja = $urls.ApiComun + "BandejaConceptoSeguimiento/Index/";
        vm.IdConcepto = "";
        vm.Descripcion = "";
        vm.OrdenVisual = "";
        vm.IdConceptoPadre = "-1";
        vm.IdEstado = "1";
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        vm.CargaModalConceptoSeguimiento = CargaModalConceptoSeguimiento;
        CargaModalConceptoSeguimiento();
        ListarComboConceptoSeguimientoPadre();
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [Comun].[SegmentoNegocio] que esten Activos.
        function ListarComboConceptoSeguimientoPadre() {
            blockUI.start();
            var promise = ConceptoSeguimientoService.ListarComboConceptoSeguimientoAgrupador();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Conceptos", 5);
                } else {
                    vm.listConceptoPadre = UtilsFactory.AgregarItemSelect(respuesta);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si es existente.
        function CargaModalConceptoSeguimiento() {
            vm.IdConcepto = $scope.$parent.vm.IdConcepto;
            if (vm.IdConcepto > 0) {
                ObtenerConceptoSeguimiento();
            } else {
                CargaModal();
            }
        }

        function ObtenerConceptoSeguimiento() {
            blockUI.start();
            var recurso = {
                IdConcepto: vm.IdConcepto
            }
            var promise = ConceptoSeguimientoService.ObtenerConceptoSeguimientoPorId(recurso);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdConcepto = Respuesta.IdConcepto;
                vm.Descripcion = Respuesta.Descripcion;
                vm.OrdenVisual = Respuesta.OrdenVisual;
                vm.IdEstado = Respuesta.IdEstado;
                if (Respuesta.IdConceptoPadre == null) {
                    vm.IdConceptoPadre = "-1";
                } else {
                    vm.IdConceptoPadre = Respuesta.IdConceptoPadre;
                }

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargaModal() {
            vm.IdConcepto = "";
            vm.Descripcion = "";
            vm.OrdenVisual = "";
            vm.IdEstado = "1";
            vm.IdConceptoPadre = "-1";
        }
        //Registra en  la tabla [Comun].[ConceptoSeguimiento] 
        vm.RegistraConceptoSeguimiento = RegistraConceptoSeguimiento;
        function RegistraConceptoSeguimiento() {
            var IdConceptoPadre = null;
            if (vm.IdConceptoPadre != "-1") {
                IdConceptoPadre = vm.IdConceptoPadre;
            }
            var concepto = {
                IdConcepto: vm.IdConcepto,
                Descripcion: vm.Descripcion,
                OrdenVisual: vm.OrdenVisual,
                IdEstado: vm.IdEstado,
                IdConceptoPadre: IdConceptoPadre
            }
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promiseConcepto = (vm.IdConcepto > 0) ? ConceptoSeguimientoService.ActualizarConceptoSeguimiento(concepto) : ConceptoSeguimientoService.RegistrarConceptoSeguimiento(concepto);

                promiseConcepto.then(function (resultadoConcepto) {
                    var RespuestaConcepto = resultadoConcepto.data;
                    if (RespuestaConcepto.TipoRespuesta == 0) {
                        $scope.$parent.vm.BuscarConceptoSeguimiento();
                        $('#ModalRegistrarConceptoSeguimiento').modal('hide');
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar el Concepto.", 5);
                    }

                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            } else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }        
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.Descripcion) == "") {
                mensaje = mensaje + "Ingrese Descripcion" + '<br/>';
                UtilsFactory.InputBorderColor('#txtDescripcion', 'Rojo');
            }
            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }
            if (!UtilsFactory.ValidarNumero(vm.OrdenVisual)) {
                mensaje = mensaje + "el OrdenVisual debe ser numerico </br>";
                UtilsFactory.InputBorderColor('#txtOrdenVisual', 'Rojo');
            }

            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#txtDescripcion', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtOrdenVisual', 'Ninguno');
        }
    }
})();