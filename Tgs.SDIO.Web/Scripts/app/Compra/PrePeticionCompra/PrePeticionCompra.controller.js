(function () {
    'use strict'
    angular
        .module('app.Compra')
        .controller('PrePeticionCompraController', PrePeticionCompraController);

    PrePeticionCompraController.$inject = ['PrePeticionCompraService', 'BuscadorPeticionCompraService',
        'MaestraService', 'ClienteProveedorService', 'RegistroProyectoService', 'ClienteService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$filter'];

    function PrePeticionCompraController(PrePeticionCompraService, BuscadorPeticionCompraService,
        MaestraService, ClienteProveedorService, RegistroProyectoService, ClienteService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $filter)
    {
        var vm = this;
        vm.BuscarCliente = BuscarCliente;
        vm.SeleccionarCliente = SeleccionarCliente;
        vm.EditarDetalle = EditarDetalle;
        vm.CerrarPrePeticionCompra = CerrarPrePeticionCompra;
        vm.ListarPrePeticionDetalle = ListarPrePeticionDetalle;
        vm.BuscarProveedor = BuscarProveedor;
        vm.LimpiarFiltrosProveedor = LimpiarFiltrosProveedor;
        vm.SeleccionarProveedor = SeleccionarProveedor;
        vm.SeleccionarPrePeticion = SeleccionarPrePeticion;
        vm.BuscarPrePeticion = BuscarPrePeticion;
        vm.BuscarPrePeticionCabecera = BuscarPrePeticionCabecera;
        vm.EditarPrePeticion = EditarPrePeticion;

        vm.EnlaceRegistrar = $urls.ApiCompra + "PrePeticionCompra/RegistrarPrePeticionCompra/";

        vm.FiltrosPreCompra = {
            DescripcionProyecto: "",
            DescripcionPreCompra: "",
            CMI: ""
        }
        vm.Filtros = {};
        vm.Proyecto = {};
        vm.PrePeticionCompra = {
            Tipo: "-1",
            IdCliente: 0,
            IdProyecto: IdProyecto,
            IdLineaCMI: IdDetalleFinanciero
        };
        vm.IdDetalle;
        vm.ClienteSeleccionado = {};
        vm.ListaPrePeticionDetalleDCM = [];
        vm.ProveedorSeleccionado = {};

        vm.dtInstanceCliente = {};
        vm.dtOptionsCliente = [];
        vm.dtColumnsCliente = [
            DTColumnBuilder.newColumn("CodigoCliente").withTitle('Código de Cliente').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn("Descripcion").withTitle('Razón Social').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("NumeroIdentificadorFiscal").withTitle('RUC').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn("GerenteComercial").withClass('hiddencol'),
            DTColumnBuilder.newColumn("DescripcionSector").withClass('hiddencol'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionSeleccionarCliente).withOption('width', '10%')
        ]; 

        
        vm.dtInstanceDCM = {};
        vm.dtOptionsDCM = [];
        vm.dtColumnsDCM = [
            DTColumnBuilder.newColumn("Id").notVisible(),
            DTColumnBuilder.newColumn("DescripcionTipoCosto").withTitle('Tipo de Costo').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn("DescripcionLineaProducto").withTitle('Línea Negocio').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("NumContrato").withTitle('N° de Contrato Marco SRM').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn("MonedaCompra").withTitle('Moneda de la Compra').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn("Monto").withTitle('Monto sin IGV').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionEditarDetalle).withOption('width', '10%')
        ]; 

        vm.dtInstanceOrdinaria = {};
        vm.dtOptionsOrdinaria = [];
        vm.dtColumnsOrdinaria = [
            DTColumnBuilder.newColumn("Id").notVisible(),
            DTColumnBuilder.newColumn("DescripcionTipoCosto").withTitle('Tipo de Costo').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn("DescripcionLineaProducto").withTitle('Línea Negocio').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("RazonSocialProveedor").withTitle('Proveedor').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn("Moneda").withTitle('Moneda').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn("Monto").withTitle('Monto').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionEditarDetalle).withOption('width', '10%')
        ]; 
        
        vm.dtInstanceProveedor = {};
        vm.dtOptionsProveedor = [];
        vm.dtColumnsProveedor = [
            DTColumnBuilder.newColumn('NombrePersonaContacto').notVisible(),
            DTColumnBuilder.newColumn('TelefonoContactoPrincipal').notVisible(),
            DTColumnBuilder.newColumn('CorreoContactoPrincipal').notVisible(),
            DTColumnBuilder.newColumn('CodigoSRM').notVisible(),
            DTColumnBuilder.newColumn('Descripcion').withTitle('Razón Social de Proveedor').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('CodigoProveedor').withTitle('Código de Proveedor').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('Pais').withTitle('País').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('RUC').withTitle('RUC').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('IdSecuencia').withTitle('Secuencia').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionSeleccionarProveedor).withOption('width', '10%')
        ];

        vm.dtInstancePrePeticion = {};
        vm.dtOptionsPrePeticion = [];
        vm.dtColumnsPrePeticion = [
            DTColumnBuilder.newColumn('Id').notVisible(),
            DTColumnBuilder.newColumn('IdProyecto').notVisible(),
            DTColumnBuilder.newColumn('IdLineaCMI').notVisible(),
            DTColumnBuilder.newColumn('Tipo').notVisible(),
            DTColumnBuilder.newColumn('Linea').notVisible(),
            DTColumnBuilder.newColumn('SubLinea').notVisible(),
            DTColumnBuilder.newColumn('DescripcionProyecto').withTitle('Proyecto').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('Servicio').withTitle('Linea CMI').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('DescripcionTipoCompra').withTitle('Tipo Compra').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('DescripcionTipoCosto').withTitle('Tipo Costo').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("DescripcionPrePeticion").withTitle('Precompra').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionSeleccionarPrePeticion).withOption('width', '10%')
        ];

        vm.dtInstancePrePeticionCabecera = {};
        vm.dtOptionsPrePeticionCabecera = [];
        vm.dtColumnsPrePeticionCabecera = [
            DTColumnBuilder.newColumn('Id').notVisible(),
            DTColumnBuilder.newColumn('DescripcionCliente').withTitle('Cliente').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('DescripcionProyecto').withTitle('Proyecto').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('TipoCompra').withTitle('Tipo Compra').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('Servicio').withTitle('CMI').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Fecha Inicio Proyecto').notSortable().renderWith(FechaInicioProy).withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];

        
        ObtenerProyecto();
        ObtenerPrePeticionCabecera();
        ListarTipoCompra();
        BuscarPrePeticionCabecera();

        function AccionesBusqueda(data, type, full, meta) {
            return "<div class='col-xs-6 col-sm-6'><a title='Editar' class='btn btn-primary'  id='tab-button' ng-click='vm.EditarPrePeticion(" + + data.Id + ")'>" + "<span class='fa fa-edit fa-lg'></span>" + "</a></div>  "
        }

        function FechaInicioProy(data, type, full, meta) {
            if (typeof (data.FechaInicioProyecto) == "undefined" || data.FechaInicioProyecto == null) {
                return "";
            }
            var dFechaInicio = new Date(parseInt(data.FechaInicioProyecto.substr(6)));
            return $filter('date')(dFechaInicio,'dd/MM/yyyy');
        }

        function EditarPrePeticion(Id) {
            window.location = vm.EnlaceRegistrar + "?IdProyecto=" + IdProyecto + "&IdDetalleFinanciero=" + IdDetalleFinanciero + "&Id=" + Id;
        }

        function ObtenerProyecto(){
            if (MostrarBusquedaPrePeticion == 1){
                BuscarPrePeticion();
            }
            
            var proyecto = {
                IdProyecto: IdProyecto,
            };

            var promise = RegistroProyectoService.ObtenerProyectoById(proyecto);
                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                    vm.Proyecto = Respuesta;
                    
                    if (vm.Proyecto != "") {
                        ObtenerCliente(vm.Proyecto.IdCliente);
                    }
                    
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 5);
                });
        }

        function ObtenerCliente(IdCliente) {
            blockUI.start();
            var cliente = {
                IdCliente: IdCliente,
                IdEstado: 1
            }
            var promise = ClienteService.ObtenerCliente(cliente);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.PrePeticionCompra.IdCliente = IdCliente;
            
                vm.ClienteSeleccionado.RazonSocialCliente = (Respuesta.Descripcion == "null" ? "" : Respuesta.Descripcion);
                vm.ClienteSeleccionado.GerenteComercial = (Respuesta.GerenteComercial == "null" ? "" : Respuesta.GerenteComercial);
                vm.ClienteSeleccionado.Sector = (Respuesta.DescripcionSector == "null" ? "" : Respuesta.DescripcionSector);
            }, function (response) {
                blockUI.stop();

            });
        }

        function ObtenerPrePeticionCabecera() {
            var request = {
                Id: IdCabecera
            };

            var promise = PrePeticionCompraService.ObtenerPrePeticionCabecera(request);
                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                    
                    if (Respuesta.Id > 0){
                        vm.PrePeticionCompra = Respuesta;

                        if (Respuesta.FechaInicioProyecto != null){
                            vm.PrePeticionCompra.FechaInicioProyecto = new Date(parseInt(Respuesta.FechaInicioProyecto.substr(6)));
                        }
                        
                        vm.ClienteSeleccionado.RazonSocialCliente = vm.PrePeticionCompra.DescripcionCliente;
                        vm.ClienteSeleccionado.GerenteComercial = vm.PrePeticionCompra.GerenteComercialCliente;
                        vm.ClienteSeleccionado.Sector = vm.PrePeticionCompra.DescripcionSector;
                    }
                    
                    ListarTipoCompra();
                    ListarPrePeticionDetalle();

                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 5);
                });
        }

        function ListarTipoCompra() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 481
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                Respuesta = Respuesta.filter(tc => tc.Codigo > 1);

                vm.ListarTipoCompra = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        
        function LimpiarGrillaCliente() {
            vm.dtOptionsCliente = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarCliente(){
            LimpiarGrillaCliente();

            $timeout(function () {
                vm.dtOptionsCliente = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withOption('lengthChange', false)
                    .withFnServerData(ListarCliente)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);

            var modalpopupConfirm = angular.element(document.getElementById("IdClienteModal"));
            modalpopupConfirm.modal('show');
        }

        function AccionSeleccionarCliente(data, type, full, meta) {
            return "<center><a title='Selecionar'  data-dismiss='modal' ng-click='vm.SeleccionarCliente(\"" + data.IdCliente + "\",\"" + data.Descripcion + "\",\"" + data.GerenteComercial + "\",\"" + data.DescripcionSector + "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
        }

        function ListarCliente(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            
            var clienteDtoRequest = {
                IdEstado: 1,
                NumeroIdentificadorFiscal: vm.Filtros.Ruc,
                Descripcion: vm.Filtros.RazonSocial,
                Indice: pageNumber,
                Tamanio: length 
            };
            
            var promise = ClienteProveedorService.ListaClientePaginado(clienteDtoRequest);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListCliente
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
            });
        }

        function LimpiarGrillaPrePeticionDetalle() {
            vm.FiltrosPreCompra = {
                DescripcionProyecto: "",
                DescripcionPreCompra: "",
                CMI: ""
            }

            vm.dtOptionsDCM = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

           vm.dtOptionsOrdinaria = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function ListarPrePeticionDetalle(){
            LimpiarGrillaPrePeticionDetalle();

            $timeout(function () {
                if (vm.PrePeticionCompra.Tipo == "2"){
                    vm.dtOptionsDCM = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withOption('lengthChange', false)
                    .withFnServerData(ListarPrePeticionDetallePaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
                }else{
                    vm.dtOptionsOrdinaria = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withOption('lengthChange', false)
                    .withFnServerData(ListarPrePeticionDetallePaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
                }
            }, 500);

        }

        function AccionSeleccionarCliente(data, type, full, meta) {
            return "<center><a title='Selecionar'  data-dismiss='modal' ng-click='vm.SeleccionarCliente(\"" + data.IdCliente + "\",\"" + data.Descripcion + "\",\"" + data.GerenteComercial + "\",\"" + data.DescripcionSector + "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
        }

        function AccionEditarDetalle(data, type, full, meta) {
            return "<div class='col-xs-6 col-sm-6'><a title='Editar' class='btn btn-primary'  id='tab-button' ng-click='vm.EditarDetalle(\"" + data.Id + "\",\"" + 1 + "\");'>" + "<span class='fa fa-edit fa-lg'></span>" + "</a></div>  "
        }

        function ListarPrePeticionDetallePaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            
            var request = {
                IdCabecera: vm.PrePeticionCompra.Id,
                TipoCompra: vm.PrePeticionCompra.Tipo,
                Indice: pageNumber,
                Tamanio: length 
            };
            
            var promise = PrePeticionCompraService.ListaPrePeticionDetalle(request);
            promise.then(function (resultado) {
                vm.ListaPrePeticionDetalleDCM = resultado.data;

                let iTotalRegistros = 0;

                if (resultado.data.length > 0){
                    iTotalRegistros = resultado.data[0].TotalRegistros;
                }

                var result = {
                    'draw': draw,
                    'recordsTotal': iTotalRegistros,
                    'recordsFiltered': iTotalRegistros,
                    'data': resultado.data
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
            });
        }

        function SeleccionarCliente(IdCliente, Descripcion, GerenteComercial, DescripcionSector){
            vm.PrePeticionCompra.IdCliente = IdCliente;
            
            vm.ClienteSeleccionado.RazonSocialCliente = (Descripcion == "null" ? "" : Descripcion);
            vm.ClienteSeleccionado.GerenteComercial = (GerenteComercial == "null" ? "" : GerenteComercial);
            vm.ClienteSeleccionado.Sector = (DescripcionSector == "null" ? "" : DescripcionSector);
            
            var modalpopupConfirm = angular.element(document.getElementById("IdClienteModal"));
            modalpopupConfirm.modal('hide');
        }

        function ValidarDatos(Detalle){
            if (vm.PrePeticionCompra.Tipo == "-1"){
                UtilsFactory.Alerta('#lblAlerta_PrePeticionCompra', 'warning', 'Debe ingresar una Tipo de compra.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.PrePeticionCompra.IdCliente == 0){
                UtilsFactory.Alerta('#lblAlerta_PrePeticionCompra', 'warning', 'Debe seleccionar un Cliente.', 5);
                blockUI.stop();
                return false;
            }

            if (Detalle == 0){
                if (vm.ListaPrePeticionDetalleDCM.length == 0){
                    UtilsFactory.Alerta('#lblAlerta_PrePeticionCompra', 'warning', 'Debe ingresar al menos un Detalle.', 5);
                    blockUI.stop();
                    return false;
                }
            }

            return true;
        } 

        function EditarDetalle(Id, Detalle){
            vm.IdDetalle = Id;
            
            if (!ValidarDatos(Detalle)){
                return;
            }else{
                RegistrarPrePeticionCompra(Detalle);
            }
        }

        function RegistrarPrePeticionCompra(Detalle) {
            
            var promise = PrePeticionCompraService.RegistrarPrePeticionCabecera(vm.PrePeticionCompra);

            promise.then(function (resultado) {
                blockUI.stop();
                
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0){
                    UtilsFactory.Alerta('#lblAlerta_PrePeticionCompra', 'success', Respuesta.Mensaje, 5);

                    vm.PrePeticionCompra.Id = Respuesta.Id;

                    if (Detalle == 1) {
                        DetallePrePeticionCompra();
                    } else {
                        history.back();
                    }
                }
            }, function (response) {
                return 0;
                blockUI.stop();
            });
        }

        function DetallePrePeticionCompra(){
            if (vm.PrePeticionCompra.Id > 0){
                var promise = PrePeticionCompraService.MostrarDetallePrePeticionCompra(vm.PrePeticionCompra.Tipo);
                promise.then(function (response) {
                    var respuesta = $(response.data);
    
                    $injector.invoke(function ($compile) {
                        var div = $compile(respuesta);
                        var content = div($scope);
                        $("#DetalleContenidoPrePeticionCompra").html(content);
                    });
    
                    var modalpopupConfirm = angular.element(document.getElementById("IdDetallePrePeticionCompra"));
                    modalpopupConfirm.modal({ backdrop: 'static', keyboard: false });
    
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                });
            }
        }

        function CerrarPrePeticionCompra(){
            $('#IdDetallePrePeticionCompra').modal('hide');
        }

        function LimpiarFiltrosProveedor(){
            vm.Filtros = {
                Ruc: "",
                RazonSocial: ""
            };
            BuscarProveedor();
        }

        function LimpiarGrillaProveedor() {
            vm.dtOptionsProveedor = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarProveedor(){
            blockUI.start();
            
            LimpiarGrillaProveedor();

            $timeout(function () {
                vm.dtOptionsProveedor = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withOption('lengthChange', false)
                    .withFnServerData(ListarProveedor)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);

            var modalpopupConfirm = angular.element(document.getElementById("IdProveedorModal"));
            modalpopupConfirm.modal('show');
        }

        function ListarProveedor(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            
            var proveedorDtoRequest = {
                IdEstado: 1,
                Ruc: vm.Filtros.Ruc,
                Descripcion: vm.Filtros.RazonSocial,
                Indice: pageNumber,
                Tamanio: length 
            };
            
            var promise = ClienteProveedorService.ListaProveedorPaginado(proveedorDtoRequest);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaProveedorPaginadoDtoResponse
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
            });
        }

        function SeleccionarProveedor(IdProveedor, Descripcion, CodigoProveedor, Ruc, Nombre, Telefono, Correo){
            vm.ProveedorSeleccionado.IdProveedor = IdProveedor;
            
            vm.ProveedorSeleccionado.RazonSocialProveedor = Descripcion;
            vm.ProveedorSeleccionado.CodigoProveedor = (CodigoProveedor == "null" ? "" : CodigoProveedor);
            vm.ProveedorSeleccionado.RUC = (Ruc == "null" ? "" : Ruc);
            vm.ProveedorSeleccionado.NombreContacto = (Nombre == "null" ? "" : Nombre);
            vm.ProveedorSeleccionado.TelefonoContacto = (Telefono == "null" ? "" : Telefono);
            vm.ProveedorSeleccionado.EmailContacto = (Correo == "null" ? "" : Correo);
            
            var modalpopupConfirm = angular.element(document.getElementById("IdClienteModal"));
            modalpopupConfirm.modal('hide');
        }

        function AccionSeleccionarProveedor(data, type, full, meta) {
            return "<center><a title='Selecionar'  data-dismiss='modal' ng-click='vm.SeleccionarProveedor(\"" + data.IdProveedor + "\",\"" + data.Descripcion + "\",\"" + data.CodigoProveedor + "\",\"" + data.RUC + "\",\"" + data.NombrePersonaContacto + "\",\"" + data.TelefonoContactoPrincipal + "\",\"" + data.CorreoContactoPrincipal +  "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
        }

        function LimpiarGrillaPrePeticionCompra() {
            
            vm.dtOptionsPrePeticion = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarPrePeticion(){
            blockUI.start();
            
            LimpiarGrillaPrePeticionCompra();

            $timeout(function () {
                vm.dtOptionsPrePeticion = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withOption('lengthChange', false)
                    .withFnServerData(ListarPrePeticion)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(5);
            }, 500);

            var modalpopupConfirm = angular.element(document.getElementById("IdProveedorModal"));
            modalpopupConfirm.modal('show');
        }

        function ListarPrePeticion(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            
            var request = {
                IdEstado: 1,
                DescripcionProyecto: vm.FiltrosPreCompra.DescripcionProyecto,
                DescripcionPrePeticion: vm.FiltrosPreCompra.DescripcionPrePeticion,
                Servicio: vm.FiltrosPreCompra.CMI,
                Indice: pageNumber,
                Tamanio: length 
            };

            var promise = PrePeticionCompraService.ListaPrePeticion(request);
            promise.then(function (resultado) {
                let iTotalRegistros = 0;

                if (resultado.data.length > 0){
                    iTotalRegistros = resultado.data[0].TotalRegistros;
                }

                var result = {
                    'draw': draw,
                    'recordsTotal': iTotalRegistros,
                    'recordsFiltered': iTotalRegistros,
                    'data': resultado.data
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
            });
        }

        function SeleccionarPrePeticion(Id, IdProyecto, IdLineaCMI, Tipo, TipoCosto, Descripcion, Linea, SubLinea, Servicio, IdContratoMarco, CotizacionAdjunta, elementoPEP, IdCliente, IdProveedor) {
            
            $scope.$parent.vm.PeticionCompra.IdPrePeticionDetalle = Id;
            $scope.$parent.vm.SeleccionarPrePeticion.IdProyecto = IdProyecto;
            $scope.$parent.vm.SeleccionarPrePeticion.IdLineaCMI = IdLineaCMI;
            $scope.$parent.vm.PeticionCompra.Descripcion = Descripcion;

            $scope.$parent.vm.SeleccionarPrePeticion.Linea = Linea;
            $scope.$parent.vm.SeleccionarPrePeticion.SubLinea = SubLinea;
            $scope.$parent.vm.SeleccionarPrePeticion.Servicio = Servicio;
            $scope.$parent.vm.ContratoMarco.IdContratoMarco = IdContratoMarco;
            $scope.$parent.vm.PeticionCompra.AdjuntaCotizacion = CotizacionAdjunta;
            $scope.$parent.vm.IdTipoCompra = Tipo;
            $scope.$parent.vm.IdTipoCosto = TipoCosto;
            $scope.$parent.vm.TiposDisabled = true;
            $scope.$parent.vm.PeticionCompra.ElementoPEP = elementoPEP;

            $scope.$parent.vm.IdClienteAP = IdCliente;
            $scope.$parent.vm.IdProveedorAP = IdProveedor;

            if (Tipo == 3 && TipoCosto == 3) {
                $scope.$parent.vm.FlagAPOrdinariaInv = true;
                $scope.$parent.vm.TiposDisabled = true;
            } else if (Tipo == 3 && TipoCosto == 2) {
                $scope.$parent.vm.PeticionCompraAPOrdinariaOpex = true;
                $scope.$parent.vm.TiposDisabled = true;
            } else {
                $scope.$parent.vm.FlagAPOrdinariaInv = false;
                $scope.$parent.vm.PeticionCompraAPOrdinariaOpex = false;
            }

            var contratoMarco = { IdContratoMarco: IdContratoMarco };

            var promise = BuscadorPeticionCompraService.ObtenerContratoMarcoPorId(contratoMarco);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                $scope.$parent.vm.ContratoMarco.NumContrato = Respuesta.NumContrato;
            }, function (response) {
                blockUI.stop();
            });

            var proyecto = {
                IdProyecto: IdProyecto,
            };

            var promise = RegistroProyectoService.ObtenerProyectoById(proyecto);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                $scope.$parent.vm.PrePeticionProyecto = Respuesta;
                $scope.$parent.vm.CerrarPopupPrePeticion();
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_Compras', 'danger', MensajesUI.DatosError, 5);
            });
        }

        function AccionSeleccionarPrePeticion(data, type, full, meta) {
            return "<center><a title='Selecionar'  data-dismiss='modal' ng-click='vm.SeleccionarPrePeticion(\"" + data.Id + "\",\"" + data.IdProyecto + "\",\""
                + data.IdLineaCMI + "\",\"" + data.Tipo + "\",\"" + data.TipoCosto + "\",\"" + data.DescripcionPrePeticion + "\",\"" + data.Linea + "\",\""
                + data.SubLinea + "\",\"" + data.Servicio + "\",\"" + data.IdContratoMarco + "\",\"" + data.CotizacionAdjunta + "\",\"" + data.ElementoPEP + "\",\""
                + data.IdCliente + "\",\"" + data.IdProveedor +
                "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
        }

        function LimpiarGrillaPrePeticionCompraCabecera() {

            vm.dtOptionsPrePeticionCabecera = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarPrePeticionCabecera() {
            blockUI.start();

            LimpiarGrillaPrePeticionCompraCabecera();

            $timeout(function () {
                vm.dtOptionsPrePeticionCabecera = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withOption('lengthChange', false)
                    .withFnServerData(ListarPrePeticionCabecera)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withLanguage({ "sEmptyTable": "No hay datos disponibles en la tabla" })
                    .withPaginationType('full_numbers')
                    .withDisplayLength(5);
            }, 500);

        }

        function ListarPrePeticionCabecera(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var request = {
                IdEstado: 1,
                IdProyecto: IdProyecto,
                IdLineaCMI: IdDetalleFinanciero,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = PrePeticionCompraService.ListarPrePeticionCabecera(request);
            promise.then(function (resultado) {
                let iTotalRegistros = 0;

                if (resultado.data.length > 0) {
                    iTotalRegistros = resultado.data[0].TotalRegistros;
                }

                var result = {
                    'draw': draw,
                    'recordsTotal': iTotalRegistros,
                    'recordsFiltered': iTotalRegistros,
                    'data': resultado.data
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
            });
        }
    }
})();