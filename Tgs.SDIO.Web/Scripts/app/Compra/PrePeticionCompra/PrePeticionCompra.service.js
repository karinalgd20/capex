(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('PrePeticionCompraService', PrePeticionCompraService);

    PrePeticionCompraService.$inject = ['$http', 'URLS'];

    function PrePeticionCompraService($http, $urls) {

        var service = {
            MostrarPrePeticionCompra: MostrarPrePeticionCompra,
            MostrarDetallePrePeticionCompra: MostrarDetallePrePeticionCompra,
            RegistrarPrePeticionCabecera: RegistrarPrePeticionCabecera,
            ListaPrePeticionDetalle: ListaPrePeticionDetalle,
            ListaPrePeticion: ListaPrePeticion,
            ListarPrePeticionCabecera: ListarPrePeticionCabecera,
            ObtenerPrePeticionCabecera: ObtenerPrePeticionCabecera,
            ValidarMontoLimite: ValidarMontoLimite,
            ModalBuscarPrePeticion: ModalBuscarPrePeticion,
            ObtenerSaldo: ObtenerSaldo
        };

        return service;

        function MostrarPrePeticionCompra() {
            return $http({
                url: $urls.ApiCompra + "LineasCMI/MostrarPrePeticionCompra",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarDetallePrePeticionCompra(TipoCompra) {
            let accion = "";
            if (TipoCompra == "2"){
                accion = "MostrarDetallePrePeticionCompra";
            }else if (TipoCompra == "3"){
                accion = "MostrarDetallePrePeticionCompraOrdinaria";
            }

            return $http({
                url: $urls.ApiCompra + "PrePeticionCompra/" + accion,
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarPrePeticionCabecera(request) {
            return $http({
                url: $urls.ApiCompra + "PrePeticionCompra/RegistrarPrePeticionCompra", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalBuscarPrePeticion(request) {
            return $http({
                url: $urls.ApiCompra + "PrePeticionCompra/ModalBuscarPrePeticion",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerPrePeticionCabecera(request) {
            return $http({
                url: $urls.ApiCompra + "PrePeticionCompra/ObtenerPrePeticionCabecera", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ValidarMontoLimite(request) {
            return $http({
                url: $urls.ApiCompra + "PrePeticionCompra/ValidarMontoLimite", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerSaldo(request) {
            return $http({
                url: $urls.ApiCompra + "PrePeticionCompra/ObtenerSaldo",
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaPrePeticion(request) {
            return $http({
                url: $urls.ApiCompra + "PrePeticionCompra/ListaPrePeticion", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarPrePeticionCabecera(request) {
            return $http({
                url: $urls.ApiCompra + "BandejaPrePeticionCompra/ListaPrePeticionCabecera",
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaPrePeticionDetalle(request) {
            
            let accion = "";
            if (request.TipoCompra == "2"){
                accion = "ListaPrePeticionDCM";
            }else if (request.TipoCompra == "3"){
                accion = "ListaPrePeticionOrdinaria";
            }

            return $http({
                url: $urls.ApiCompra + "PrePeticionCompra/" + accion, 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarAnexo(iIdPeticionCompra) {
            return $http({
                url: $urls.ApiCompra + "ComentarioAnexo/EliminarAnexo", 
                method: "POST",
                data: {IdPeticionCompra: iIdPeticionCompra}
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function DescargarAnexoArchivo(iIdAnexoArchivo) {
            window.location.href = $urls.ApiCompra + "ComentarioAnexo/DescargarAnexoArchivo?iIdAnexoArchivo=" + iIdAnexoArchivo;
           
        };

        function GuardarAnexos(Anexos, iIdPeticionCompra) {
            var datax = new FormData();
            var sTipoDocumento = [];

            Anexos.forEach(anexo => {
                sTipoDocumento.push(anexo.IdTipoDocumento);
                datax.append("file", anexo.Archivo);
            });
            datax.append("IdTipoDocumento", sTipoDocumento.join(","));
            datax.append("IdPeticionCompra", iIdPeticionCompra);

            var resultado =$.ajax({
                type: "POST",
                url: $urls.ApiCompra + "ComentarioAnexo/GuardarAnexo",  
                dataType: 'json',
                cache: false,
                processData: false,
                contentType: false, 
                data: datax
            }); 

            return resultado;

        };
    }
})();