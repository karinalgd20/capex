(function () {
    'use strict'
    angular
        .module('app.Compra')
        .controller('ClienteProveedorController', ClienteProveedorController);

        ClienteProveedorController.$inject = ['ClienteProveedorService', 'ClienteService', 'ProveedorService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function ClienteProveedorController(ClienteProveedorService, ClienteService, ProveedorService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector)
    {
        var vm = this;
        vm.AbrirBusquedaClienteProveedor = AbrirBusquedaClienteProveedor;
        vm.SeleccionarCliente = SeleccionarCliente;
        vm.SeleccionarProveedor = SeleccionarProveedor;
        vm.Buscar = Buscar;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.GuardarDetalleClienteProveedor = GuardarDetalleClienteProveedor;
        vm.TipoModal = 0;

        vm.TituloModal = "";

        vm.DetalleClienteProveedor = {
            IdCliente: 0,
            IdProveedor: 0,
            IdPeticionCompra: $scope.$parent.vm.PeticionCompra.IdPeticionCompra
        };

        vm.Ver = $scope.$parent.vm.Ver;
        vm.PeticionCompraDCM = $scope.$parent.vm.PeticionCompraDCM;
        vm.Filtros = {};
        vm.ClienteSeleccionado = {};
        vm.ProveedorSeleccionado = {};
        
        vm.dtInstanceCliente = {};
        vm.dtOptionsCliente = [];
        vm.dtColumnsCliente = [
            DTColumnBuilder.newColumn("CodigoCliente").withTitle('Código de Cliente').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn("Descripcion").withTitle('Razón Social').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("NumeroIdentificadorFiscal").withTitle('RUC').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionSeleccionarCliente).withOption('width', '10%')
        ]; 

        vm.dtInstanceProveedor = {};
        vm.dtOptionsProveedor = [];

        vm.dtColumnsProveedor = [
            DTColumnBuilder.newColumn('NombrePersonaContacto').notVisible(),
            DTColumnBuilder.newColumn('TelefonoContactoPrincipal').notVisible(),
            DTColumnBuilder.newColumn('CorreoContactoPrincipal').notVisible(),
            DTColumnBuilder.newColumn('CodigoSRM').notVisible(),
            DTColumnBuilder.newColumn('Descripcion').withTitle('Razón Social de Proveedor').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('CodigoProveedor').withTitle('Código de Proveedor').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('Pais').withTitle('País').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('RUC').withTitle('RUC').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('IdSecuencia').withTitle('Secuencia').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionSeleccionarProveedor).withOption('width', '10%')
        ];
        
        ObtenerDetalleClienteProveedor();
        
        function ObtenerDetalleClienteProveedor() {
            blockUI.start();
            if ($scope.$parent.vm.PeticionCompraDCM){
                
                vm.DetalleClienteProveedor.IdProveedor = $scope.$parent.vm.ContratoMarco.IdProveedor;
                vm.ProveedorSeleccionado.RazonSocialProveedor = $scope.$parent.vm.ContratoMarco.RazonSocialProveedor;
                vm.ProveedorSeleccionado.CodigoProveedor = $scope.$parent.vm.ContratoMarco.CodigoProveedor;
                vm.ProveedorSeleccionado.RUC = $scope.$parent.vm.ContratoMarco.RUC;

                vm.DetalleClienteProveedor.NombreContacto = $scope.$parent.vm.ContratoMarco.NombreContacto;
                vm.DetalleClienteProveedor.TelefonoContacto = $scope.$parent.vm.ContratoMarco.TelefonoContacto;
                vm.DetalleClienteProveedor.EmailContacto = $scope.$parent.vm.ContratoMarco.EmailContacto;
                vm.DetalleClienteProveedor.CodigoSrm = $scope.$parent.vm.ContratoMarco.CodigoProveedor;
                vm.DetalleClienteProveedor.CodigoSap = $scope.$parent.vm.ContratoMarco.CodigoSap;
            }

            vm.DetalleClienteProveedor.IdPeticionCompra = $scope.$parent.vm.PeticionCompra.IdPeticionCompra;
            var promise = ClienteProveedorService.ObtenerDetalleClienteProveedor(vm.DetalleClienteProveedor);
            promise.then(function (resultado) {
                let oRespuesta = resultado.data;

                if (oRespuesta != ""){
                    vm.DetalleClienteProveedor = oRespuesta; 
                    vm.ClienteSeleccionado.CodigoCliente = oRespuesta.CodigoCliente;
                    vm.ClienteSeleccionado.RazonSocialCliente = oRespuesta.RazonSocialCliente;
                    vm.ProveedorSeleccionado.CodigoProveedor = oRespuesta.CodigoProveedor;
                    vm.ProveedorSeleccionado.RazonSocialProveedor = oRespuesta.RazonSocialProveedor;
                    vm.ProveedorSeleccionado.RUC = oRespuesta.RucProveedor;
                } else {

                    if ($scope.$parent.vm.IdClienteAP != 0) {
                        var clienteDto = { IdCliente: $scope.$parent.vm.IdClienteAP, IdEstado: 1 };
                        var promise = ClienteService.ObtenerCliente(clienteDto);
                        promise.then(function (resultado) {
                            let oRespuesta = resultado.data;
                            if (oRespuesta != "") {
                                vm.DetalleClienteProveedor.IdCliente = oRespuesta.IdCliente;
                                vm.ClienteSeleccionado.CodigoCliente = oRespuesta.CodigoCliente;
                                vm.ClienteSeleccionado.RazonSocialCliente = oRespuesta.Descripcion;
                            }
                        }, function (response) {
                            blockUI.stop();
                        });
                    }
                    
                    if ($scope.$parent.vm.IdProveedorAP != 0) {
                        var proveedorDto = { IdProveedor: $scope.$parent.vm.IdProveedorAP, IdEstado: 1 };
                        var promise = ProveedorService.ObtenerProveedorById(proveedorDto);
                        promise.then(function (resultado) {
                            let oRespuesta = resultado.data;
                            if (oRespuesta != "") {
                                vm.DetalleClienteProveedor.IdProveedor = oRespuesta.IdProveedor;
                                vm.ProveedorSeleccionado.CodigoProveedor = oRespuesta.CodigoProveedor;
                                vm.DetalleClienteProveedor.CodigoSap = oRespuesta.CodigoProveedor;
                                vm.ProveedorSeleccionado.RazonSocialProveedor = oRespuesta.RazonSocial;
                                vm.ProveedorSeleccionado.RUC = oRespuesta.RUC;
                                vm.DetalleClienteProveedor.CodigoSrm = oRespuesta.CodigoSRM;
                                vm.DetalleClienteProveedor.NombreContacto = oRespuesta.NombrePersonaContacto;
                                vm.DetalleClienteProveedor.EmailContacto = oRespuesta.CorreoContactoPrincipal;
                                vm.DetalleClienteProveedor.TelefonoContacto = oRespuesta.TelefonoContactoPrincipal;
                            }
                        }, function (response) {
                            blockUI.stop();
                        });
                    }
                }
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });

            $timeout(function () {
                blockUI.stop();
            }, 500);
        }

        function AbrirBusquedaClienteProveedor(TipoModal) {
            LimpiarFiltros();

            vm.TipoModal = TipoModal;
            vm.TituloModal = (TipoModal == 1 ? "Cliente" : "Proveedor");

            setTimeout(function () {
                Buscar();
            }, 100)
        }

        function Buscar() {
            if (vm.TipoModal == 0) return;

            if (vm.TipoModal == 1){
                BuscarCliente();
            }else{
                BuscarProveedor();
            }
        }
        
        function LimpiarFiltros(){
            vm.Filtros = {};
            Buscar();
        }

        function ValidarDatos(){
            if (vm.DetalleClienteProveedor.IdCliente == 0){
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Debe seleccionar un Cliente.', 5);
                $scope.$parent.vm.blockUI.stop();
                return false;
            }

            if (vm.DetalleClienteProveedor.IdProveedor == 0){
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Debe seleccionar un Proveedor.', 5);
                $scope.$parent.vm.blockUI.stop();
                return false;
            }

            return true;
        }

        function GuardarDetalleClienteProveedor(){
            $scope.$parent.vm.blockUI.start();

            if (!ValidarDatos()){
                return;
            }
            
            vm.DetalleClienteProveedor.IdPeticionCompra = $scope.$parent.vm.PeticionCompra.IdPeticionCompra;
            
            var promise = ClienteProveedorService.GuardarDetalleClienteProveedor(vm.DetalleClienteProveedor);
            promise.then(function (resultado) {
                let oRespuesta = resultado.data;
                if (oRespuesta.TipoRespuesta == 0){
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'success', oRespuesta.Mensaje, 5);
                    ObtenerDetalleClienteProveedor();
                }

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });

            $timeout(function () {
                blockUI.stop();
            }, 500);
        }

        function LimpiarGrillaCliente() {
            vm.dtOptionsCliente = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarCliente(){
            LimpiarGrillaCliente();

            $timeout(function () {
                vm.dtOptionsCliente = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withOption('lengthChange', false)
                    .withFnServerData(ListarCliente)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);

            var modalpopupConfirm = angular.element(document.getElementById("IdClienteModal"));
            modalpopupConfirm.modal('show');
        }

        function ListarCliente(sSource, aoData, fnCallback, oSettings) {
            
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            
            var clienteDtoRequest = {
                IdEstado: 1,
                ColumnName: "IdCliente",
                NumeroIdentificadorFiscal: vm.Filtros.Ruc,
                Descripcion: vm.Filtros.RazonSocial,
                Indice: pageNumber,
                Tamanio: length 
            };
            
            var promise = ClienteProveedorService.ListaClientePaginado(clienteDtoRequest);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListCliente
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
            });
        }

        function SeleccionarCliente(IdCliente, Descripcion, CodigoCliente){
            vm.DetalleClienteProveedor.IdCliente = IdCliente;

            vm.ClienteSeleccionado.RazonSocialCliente = (Descripcion == "null" ? "" : Descripcion);
            vm.ClienteSeleccionado.CodigoCliente = (CodigoCliente == "null" ? "" : CodigoCliente);

            var modalpopupConfirm = angular.element(document.getElementById("IdClienteModal"));
            modalpopupConfirm.modal('hide');
        }

        function AccionSeleccionarCliente(data, type, full, meta) {
            return "<center><a title='Selecionar'  data-dismiss='modal' ng-click='vm.SeleccionarCliente(\"" + data.IdCliente + "\",\"" + data.Descripcion + "\",\"" + data.CodigoCliente + "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
        }

        function LimpiarGrillaProveedor() {
            vm.dtOptionsProveedor = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };


        function BuscarProveedor(){
            blockUI.start();

            LimpiarGrillaProveedor();

            $timeout(function () {
                vm.dtOptionsProveedor = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withOption('lengthChange', false)
                    .withFnServerData(ListarProveedor)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);

            var modalpopupConfirm = angular.element(document.getElementById("IdClienteModal"));
            modalpopupConfirm.modal('show');
        }

        function ListarProveedor(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            
            var proveedorDtoRequest = {
                IdEstado: 1,
                Ruc: vm.Filtros.Ruc,
                Descripcion: vm.Filtros.RazonSocial,
                Indice: pageNumber,
                Tamanio: length 
            };
            
            var promise = ClienteProveedorService.ListaProveedorPaginado(proveedorDtoRequest);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaProveedorPaginadoDtoResponse
                };

                blockUI.stop();
                blockUI.stop();

                fnCallback(result)
            }, function (response) {
                blockUI.stop();
            });
        }

        function SeleccionarProveedor(IdProveedor, Descripcion, CodigoProveedor, Ruc, EsDCM, Nombre, Telefono, Correo, CodigoSRM){
            vm.DetalleClienteProveedor.IdProveedor = IdProveedor;
            
            vm.ProveedorSeleccionado.RazonSocialProveedor = Descripcion;
            vm.ProveedorSeleccionado.CodigoProveedor = (CodigoProveedor == "null" ? "" : CodigoProveedor);
            vm.ProveedorSeleccionado.RUC = (Ruc == "null" ? "" : Ruc);
            
            if (EsDCM == 1){
                vm.DetalleClienteProveedor.NombreContacto = (Nombre == "null" ? "" : Nombre);
                vm.DetalleClienteProveedor.TelefonoContacto = (Telefono == "null" ? "" : Telefono);
                vm.DetalleClienteProveedor.EmailContacto = (Correo == "null" ? "" : Correo);
                vm.DetalleClienteProveedor.CodigoSrm = (CodigoSRM == "null" ? "" : CodigoSRM);
                vm.DetalleClienteProveedor.CodigoSap = (CodigoProveedor == "null" ? "" : CodigoProveedor);
            }
            
            var modalpopupConfirm = angular.element(document.getElementById("IdClienteModal"));
            modalpopupConfirm.modal('hide');
        }

        function AccionSeleccionarProveedor(data, type, full, meta) {
            if (!vm.PeticionCompraDCM){
                return "<center><a title='Selecionar'  data-dismiss='modal' ng-click='vm.SeleccionarProveedor(\"" + data.IdProveedor + "\",\"" + data.Descripcion + "\",\"" + data.CodigoProveedor + "\",\"" + data.RUC + "\",\"" + 0 + "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
            }else{
                return "<center><a title='Selecionar'  data-dismiss='modal' ng-click='vm.SeleccionarProveedor(\"" + data.IdProveedor + "\",\"" + data.Descripcion + "\",\"" + data.CodigoProveedor + "\",\"" + data.RUC + "\",\"" + 1 + "\",\"" + data.NombrePersonaContacto + "\",\"" + data.TelefonoContactoPrincipal + "\",\"" + data.CorreoContactoPrincipal + "\",\"" + data.CodigoSRM + "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
            }
        }
    }
})();