(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('ClienteProveedorService', ClienteProveedorService);

    ClienteProveedorService.$inject = ['$http', 'URLS'];

    function ClienteProveedorService($http, $urls) {

        var service = {
            MostrarListaCliente: MostrarListaCliente,
            ListaClientePaginado: ListaClientePaginado,
            ListaProveedorPaginado: ListaProveedorPaginado,
            ObtenerDetalleClienteProveedor: ObtenerDetalleClienteProveedor,
            GuardarDetalleClienteProveedor: GuardarDetalleClienteProveedor
        };

        return service;

        function MostrarListaCliente() {
            return $http({
                url: $urls.ApiCompra + "ClienteProveedor/ListaCliente", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
        function ListaClientePaginado(request) {
            return $http({
                url: $urls.ApiCompra + "ClienteProveedor/ListaClientePaginado", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaProveedorPaginado(request) {
            return $http({
                url: $urls.ApiCompra + "ClienteProveedor/ListaProveedorPaginado", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerDetalleClienteProveedor(request) {
            return $http({
                url: $urls.ApiCompra + "ClienteProveedor/ObtenerDetalleClienteProveedor", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function GuardarDetalleClienteProveedor(request) {
            return $http({
                url: $urls.ApiCompra + "ClienteProveedor/GuardarDetalleClienteProveedor", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();