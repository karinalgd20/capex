(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('AnexoService', AnexoService);

    AnexoService.$inject = ['$http', 'URLS'];

    function AnexoService($http, $urls) {

        var service = {
            ObtenerGestion: ObtenerGestion,
            GuardarAnexos: GuardarAnexos,
            ListarAnexos: ListarAnexos,
            EliminarAnexo: EliminarAnexo,
            DescargarAnexoArchivo: DescargarAnexoArchivo
        };

        return service;

        function ObtenerGestion(request) {
            return $http({
                url: $urls.ApiCompra + "Gestion/ObtenerGestion", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarAnexos(iIdPeticionCompra) {
            return $http({
                url: $urls.ApiCompra + "ComentarioAnexo/ListarAnexos", 
                method: "POST",
                data: {IdPeticionCompra: iIdPeticionCompra}
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarAnexo(iIdPeticionCompra) {
            return $http({
                url: $urls.ApiCompra + "ComentarioAnexo/EliminarAnexo", 
                method: "POST",
                data: {IdPeticionCompra: iIdPeticionCompra}
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function DescargarAnexoArchivo(iIdAnexoPeticionCompra) {
            window.location.href = $urls.ApiCompra + "ComentarioAnexo/DescargarAnexoArchivo?iIdAnexoPeticionCompra=" + iIdAnexoPeticionCompra;
           
        };

        function GuardarAnexos(Anexos, iIdPeticionCompra) {
            
            var datax = new FormData();
            var sTipoDocumento = [];

            Anexos.forEach(anexo => {
                sTipoDocumento.push(anexo.IdTipoDocumento);
                datax.append("file", anexo.Archivo);
            });
            datax.append("IdTipoDocumento", sTipoDocumento.join(","));
            datax.append("IdPeticionCompra", iIdPeticionCompra);

            var resultado =$.ajax({
                type: "POST",
                url: $urls.ApiCompra + "ComentarioAnexo/GuardarAnexo",  
                dataType: 'json',
                cache: false,
                processData: false,
                contentType: false, 
                data: datax
            }); 

            return resultado;

        };
    }
})();