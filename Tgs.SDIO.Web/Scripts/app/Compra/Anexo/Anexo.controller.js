(function () {
    'use strict'
    angular
        .module('app.Compra')
        .controller('AnexoController', AnexoController);

        AnexoController.$inject = ['AnexoService',
        'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$filter'];

    function AnexoController(AnexoService,
        MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $filter)
    {
        var vm = this;
        vm.GuardarAnexo = GuardarAnexo;
        vm.GuardarAnexos = GuardarAnexos;
        vm.EliminarAnexo = EliminarAnexo;
        vm.DescargarAnexoArchivo = DescargarAnexoArchivo;
        vm.PermitirAcciones = PermitirAcciones;
        vm.Ver = $scope.$parent.vm.Ver;
        vm.ArchivoPesoLimite = 5242880; // 5MB


        vm.SeleccionarTipoDocumento = SeleccionarTipoDocumento;
        vm.ListaAnexo = [];
        vm.Anexo = {
            IdTipoDocumento: -1,
            IdPeticionCompra: $scope.$parent.vm.PeticionCompra.IdPeticionCompra
        };
        vm.TipoDocumentoValor2 = "";
        vm.CodigoPeticionCompraExtendido = $scope.$parent.vm.PeticionCompra.CodigoPeticionCompraExtendido;
        
        vm.IdOrdenEtapa = $scope.$parent.vm.IdOrdenEtapa;
        vm.PeticionCompraDCM = $scope.$parent.vm.PeticionCompraDCM;
        vm.PeticionCompraOrdinaria = $scope.$parent.vm.PeticionCompraOrdinaria;
        vm.AsociadaProyecto = $scope.$parent.vm.AsociadoProyecto;
        vm.PeticionCompraAPOrdinariaOpex = $scope.$parent.vm.PeticionCompraAPOrdinariaOpex;
        vm.FlagAPOrdinariaInv = $scope.$parent.vm.FlagAPOrdinariaInv;
        vm.Etapa = $scope.$parent.vm.Etapa;
        vm.IdUsuario = $scope.$parent.vm.IdUsuario;

        ListarTipoDocumento();
        ListarAnexos();

        function GuardarAnexo() {
            $scope.$parent.vm.blockUI.start();
            
            let Anexos = vm.ListaAnexo.filter(d => d.NombreArchivo == vm.Anexo.NombreArchivo);

            if (vm.Anexo.PesoArchivo > vm.ArchivoPesoLimite) {
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "El tamaño del archivo es demasiado grande. El tamaño máximo permitido es de 5 MB.", 5);
                $scope.$parent.vm.blockUI.stop();
                return;
            }

            if (vm.Anexo.IdTipoDocumento == -1) {
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe seleccionar el tipo de documento.", 5);
                $scope.$parent.vm.blockUI.stop();
                return;
            }else if (vm.Anexo.Archivo == null) {
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe adjuntar un archivo.", 5);
                $scope.$parent.vm.blockUI.stop();
                return;
            } else if (vm.Anexo.NombreArchivo == "") {
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe de seleccionar el archivo a adjuntar.", 5);
                $scope.$parent.vm.blockUI.stop();
                return;
            } else if (Anexos.length > 0) {
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "El archivo ya fue adjuntado seleccione otro.", 5);
                $scope.$parent.vm.blockUI.stop();
                return;
            }

            vm.Anexo.EsNuevo = true;
            vm.ListaAnexo.push(vm.Anexo);
            NuevoDocumento();

            $scope.$parent.vm.blockUI.stop();
        }

        function PermitirAcciones(){
            return (vm.IdOrdenEtapa == 1 || vm.Etapa == "GESTOR") && vm.Ver == 0;
        }

        $scope.UploadFiles = function (files) {
            if (files.length == 0) return;

            let archivo = files[0];

            vm.Anexo.Archivo = archivo
            vm.Anexo.NombreArchivo = archivo.name;
            vm.Anexo.PesoArchivo = archivo.size;
        }

        function EliminarAnexo(Anexo){
            if (Anexo.IdAnexoPeticionCompra == 0 || (Anexo.IdAnexoPeticionCompra == undefined && Anexo.EsNuevo)) {
                let idx = vm.ListaAnexo.findIndex(d => d.IdTipoDocumento === Anexo.IdTipoDocumento && d.NombreArchivo === Anexo.NombreArchivo);
                vm.ListaAnexo.splice(idx, 1);
            }else{
                var promise = AnexoService.EliminarAnexo(Anexo.IdAnexoPeticionCompra).then(function (response) {
                    var Respuesta = response.data;
                    if (Respuesta) {
                        let idx = vm.ListaAnexo.findIndex(d => d.IdAnexoPeticionCompra == Anexo.IdAnexoPeticionCompra);
                        vm.ListaAnexo.splice(idx, 1);

                        UtilsFactory.Alerta('#lblAlerta_Compras', 'success', "Se elimino la información satisfactoriamente.", 5);
                    }
    
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                });
            }
        }

        function GuardarAnexos(){
            $scope.$parent.vm.blockUI.start();
            $scope.$parent.vm.blockUI.start();

            let IdOrdenEtapa = $scope.$parent.vm.IdOrdenEtapa;
            
            if (vm.ListaAnexo.length == 0){
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe ingresar al menos un documento.", 5);
                $scope.$parent.vm.blockUI.stop();
                $scope.$parent.vm.blockUI.stop();
                return;
            }

            let AnexosTipoSolicitud = vm.ListaAnexo.filter(d => d.IdTipoDocumento == 1);
            let AnexosTipoRFP = vm.ListaAnexo.filter(d => d.IdTipoDocumento == 2);
            let AnexosTipoCotizacion = vm.ListaAnexo.filter(d => d.IdTipoDocumento == 3);
            let AnexosCotizacion = vm.ListaAnexo.filter(d => d.IdTipoDocumento == 3);

            if (vm.AsociadaProyecto && vm.PeticionCompraDCM) {
                if (IdOrdenEtapa == 1) {
                    if (AnexosCotizacion.length == 0) {
                        UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe ingresar un documento Cotización.", 5);
                        $scope.$parent.vm.blockUI.stop();
                        $scope.$parent.vm.blockUI.stop();
                        return;
                    }
                }
            } else if (vm.PeticionCompraDCM || vm.AsociadaProyecto) {
                if (IdOrdenEtapa == 1) {
                    if (vm.PeticionCompraAPOrdinariaOpex || vm.FlagAPOrdinariaInv) {
                        if (AnexosCotizacion.length == 0) {
                            UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe ingresar un documento Cotización.", 5);
                            $scope.$parent.vm.blockUI.stop();
                            $scope.$parent.vm.blockUI.stop();
                            return;
                        }

                        if (AnexosTipoRFP.length == 0) {
                            UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe ingresar un documento RFP.", 5);
                            $scope.$parent.vm.blockUI.stop();
                            $scope.$parent.vm.blockUI.stop();
                            return;
                        }
                    } else {

                        if (AnexosCotizacion.length == 0) {
                            UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe ingresar un documento Cotización.", 5);
                            $scope.$parent.vm.blockUI.stop();
                            $scope.$parent.vm.blockUI.stop();
                            return;
                        }
                    }
                }
            } else {
                if (IdOrdenEtapa == 1) {
                    if (AnexosTipoSolicitud.length == 0 || AnexosTipoRFP.length == 0 || AnexosTipoCotizacion.length == 0) {
                        UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe ingresar documentos por cada tipo.", 5);
                        $scope.$parent.vm.blockUI.stop();
                        $scope.$parent.vm.blockUI.stop();
                        return;
                    }
                }
            }
            
            let Anexos = vm.ListaAnexo.filter(d => d.EsNuevo);
            
            
            var promise = AnexoService.GuardarAnexos(Anexos, $scope.$parent.vm.PeticionCompra.IdPeticionCompra).then(function (response) {
                var Respuesta = response;
                if (Respuesta.TipoRespuesta == 0){
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'success', Respuesta.Mensaje, 5);

                    ListarAnexos();
                } else {
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'danger', Respuesta.Mensaje, 20);
                }

                $scope.$parent.vm.blockUI.stop();
                $scope.$parent.vm.blockUI.stop();
            }, function (response) {
                $scope.$parent.vm.blockUI.stop();
                $scope.$parent.vm.blockUI.stop();
            });
        }

        function ListarAnexos() {
            $scope.$parent.vm.blockUI.start();
            $scope.$parent.vm.blockUI.start();

            var promise = AnexoService.ListarAnexos($scope.$parent.vm.PeticionCompra.IdPeticionCompra);
            promise.then(function (response) {
                var Respuesta = response.data;
  
                vm.ListaAnexo = Respuesta.filter(d => d.IdTipoDocumento != 6);

                vm.ListaAnexo.forEach(anexo => {
                    anexo.EsNuevo = false;
                });

                let AnexosTipoCotizacion = vm.ListaAnexo.filter(d => d.IdTipoDocumento == 3);
                if (AnexosTipoCotizacion.length > 0) {
                    $scope.$parent.vm.AdjuntoCotizacion = true;
                } else {
                    $scope.$parent.vm.AdjuntoCotizacion = false;
                }

                $scope.$parent.vm.blockUI.stop();
                $scope.$parent.vm.blockUI.stop();
            }, function (response) {
                $scope.$parent.vm.blockUI.stop();
                $scope.$parent.vm.blockUI.stop();
            });

            $timeout(function () {
                blockUI.stop();
            }, 500);
        }

        function DescargarAnexoArchivo(Anexo){
           AnexoService.DescargarAnexoArchivo(Anexo.IdAnexoPeticionCompra);
        }

        function NuevoDocumento(){
            vm.Anexo = {
                IdTipoDocumento: -1,
                DesTipoDocumento: "",
                Archivo: "",
                NombreArchivo: "",
                EsNuevo: true
            }
        }

        function SeleccionarTipoDocumento(){
            let oTipoDocumento = vm.ListaTipoDocumento.find(td => td.Codigo == vm.Anexo.IdTipoDocumento);
            
            vm.Anexo.DesTipoDocumento = oTipoDocumento.Descripcion;
            vm.Anexo.IdTipoDocumento = oTipoDocumento.Codigo;
            vm.TipoDocumentoValor2 = oTipoDocumento.Valor2;
        }

        function ListarTipoDocumento() {
            
            let IdOrdenEtapa = $scope.$parent.vm.IdOrdenEtapa;
            let iIdRelacion = 0;
            let Etapa = $scope.$parent.vm.Etapa;

            if (IdOrdenEtapa == 1){
                iIdRelacion = 499
            }else if (vm.Etapa == "GESTOR"){
                iIdRelacion = 503
            }
            
            var maestra = {
                IdEstado: 1,
                IdRelacion: iIdRelacion
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion2(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                
                let oListaTipoDocumento = [];
                Respuesta.forEach(element => {
                    oListaTipoDocumento.push({Codigo: element.Valor, Valor2: element.Valor2, Descripcion: element.Descripcion + (element.Valor2 != null ? " (" + element.Valor2 + ")" : "")});
                });
                
                vm.ListaTipoDocumento = UtilsFactory.AgregarItemSelect(oListaTipoDocumento);

            }, function (response) {
                blockUI.stop();
            });
        }
    }
})();