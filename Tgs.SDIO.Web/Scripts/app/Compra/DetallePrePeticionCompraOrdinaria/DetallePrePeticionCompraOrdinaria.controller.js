(function () {
    'use strict'
    angular
        .module('app.Compra')
        .controller('DetallePrePeticionCompraOrdinariaController', DetallePrePeticionCompraOrdinariaController);

        DetallePrePeticionCompraOrdinariaController.$inject = ['DetallePrePeticionCompraOrdinariaService',
        'MaestraService', 'BuscadorPeticionCompraService', 'PrePeticionCompraService', 'ProveedorService', 'RecursoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$filter'];

    function DetallePrePeticionCompraOrdinariaController(DetallePrePeticionCompraOrdinariaService,
        MaestraService, BuscadorPeticionCompraService, PrePeticionCompraService, ProveedorService, RecursoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $filter)
    {
        var vm = this;
        vm.GuardarPrePeticionOrdinariaDetalle = GuardarPrePeticionOrdinariaDetalle;

        vm.BuscarProveedor = BuscarProveedor;
        vm.MontoLimiteValido = 1;

        vm.Filtros = {};
        vm.Proyecto = {};

        LimpiarFormulario();

        vm.ClienteSeleccionado = {};
        vm.ProveedorSeleccionado = {};
        vm.ContratoMarco = {};
        vm.ObtenerSaldo = ObtenerSaldo;
        
        //ObtenerProyecto();
        //ListarTipoCompra();
        
        ListarMoneda();
        ListarCatalogado();
        ListarTipoCosto();
        ListarLineaProducto();
        ListarTipoEntrega();

        ObtenerPrePeticionOrdinariaDetalle();

        $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });

        function LimpiarFormulario(){
            vm.DetallePrePeticionCompraOrdinaria = {
                IdCabecera: $scope.$parent.vm.PrePeticionCompra.Id,
                Id: 0,
                TipoCosto: "-1",
                IdLineaProducto: "-1",
                GrupoCompra: "-1",
                PQAdjudicado: "-1",
                PliegoTecnico: "-1",
                CotizacionAdjunta: "-1",
                PosicionesOC: "-1",
    
                Descripcion: "",
                ProductManager: "",
                Moneda: "-1",
                Monto: "",
                CoordinadorCompras: "",
                CompradorAsignado: "",
                GrupoCompraConfirmado: "-1",
                FechaEntregaOC: "",
                Observaciones: ""
            };
        }

        function ObtenerPrePeticionOrdinariaDetalle(){
            var request = {
                Id: $scope.$parent.vm.IdDetalle
            };

            var promise = DetallePrePeticionCompraOrdinariaService.ObtenerPrePeticionOrdinariaDetalle(request);
                promise.then(function (resultado) {
                    var Respuesta = resultado.data;

                    if (Respuesta.Id > 0){
                        vm.DetallePrePeticionCompraOrdinaria = Respuesta;

                        if (Respuesta.FechaEntregaOC != null){
                            vm.DetallePrePeticionCompraOrdinaria.FechaEntregaOC = new Date(parseInt(Respuesta.FechaEntregaOC.substr(6)));
                        }

                        ObtenerProveedor(Respuesta.IdProveedor);
                    }else{
                        LimpiarFormulario();
                    }
                    
                    ObtenerSaldo();

                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 5);
                });
        }

        function ObtenerProveedor(iIdProveedor) {
            let request = {
                IdEstado: 1,
                IdProveedor: iIdProveedor
            };
            var promise = ProveedorService.ObtenerProveedor(request);
            promise.then(function (resultado) {
                let oRespuesta = resultado.data;

                $scope.$parent.vm.ProveedorSeleccionado.IdProveedor = oRespuesta.IdProveedor;
                $scope.$parent.vm.ProveedorSeleccionado.RazonSocialProveedor = oRespuesta.Descripcion;
                $scope.$parent.vm.ProveedorSeleccionado.RUC = oRespuesta.RUC;
                $scope.$parent.vm.ProveedorSeleccionado.NombreContacto = oRespuesta.NombrePersonaContacto;
                $scope.$parent.vm.ProveedorSeleccionado.TelefonoContacto = oRespuesta.TelefonoContactoPrincipal;
                $scope.$parent.vm.ProveedorSeleccionado.EmailContacto = oRespuesta.CorreoContactoPrincipal;
                $scope.$parent.vm.ProveedorSeleccionado.CodigoSrm = oRespuesta.CodigoProveedor;
                $scope.$parent.vm.ProveedorSeleccionado.CodigoSap = oRespuesta.CodigoSap;
                $scope.$parent.vm.ProveedorSeleccionado.CodigoProveedor = oRespuesta.CodigoProveedor;

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ObtenerSaldo() {
            let request = {
                Id: vm.DetallePrePeticionCompraOrdinaria.IdCabecera,
                TipoCosto: vm.DetallePrePeticionCompraOrdinaria.TipoCosto
            };
            var promise = PrePeticionCompraService.ObtenerSaldo(request);
            promise.then(function (resultado) {
                let oRespuesta = resultado.data;
                vm.MensajeSaldo = oRespuesta;
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarMoneda() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 98
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion2(maestra);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;

                let oListaMoneda = [];
                Respuesta.forEach(element => {
                    oListaMoneda.push({Codigo: element.Valor, Valor2: element.Valor2, Descripcion: element.Descripcion});
                });
                
                vm.ListaMonedas = UtilsFactory.AgregarItemSelect(oListaMoneda);
                
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarCatalogado() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 537
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListaCatalogado = UtilsFactory.AgregarItemSelect(Respuesta);
                
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarTipoCosto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 485
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;

                Respuesta = Respuesta.filter(tc => tc.Codigo > 1);

                vm.ListaTipoCosto = UtilsFactory.AgregarItemSelect(Respuesta);
                
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarLineaProducto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 489
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaLineaProducto = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarTipoEntrega() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 540
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaTipoEntrega = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function BuscarProveedor() {
            $scope.$parent.vm.BuscarProveedor();
        };

        
        function CerrarPopupContratoMarco() {
            $('#ModalCompraContratoMarco').modal('hide');
        };

        function ValidarMontoLimite(){
            var request = {
                Id: vm.DetallePrePeticionCompraOrdinaria.IdCabecera,
                IdDetalle: vm.DetallePrePeticionCompraOrdinaria.Id,
                TipoCosto: vm.DetallePrePeticionCompraOrdinaria.TipoCosto,
                Tipo: $scope.$parent.vm.PrePeticionCompra.Tipo,
                Monto: vm.DetallePrePeticionCompraOrdinaria.Monto
            };

            var promise = PrePeticionCompraService.ValidarMontoLimite(request);
                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                    if (Respuesta == 1){
                        let DesTipoCosto = ""
                        if (vm.DetallePrePeticionCompraOrdinaria.TipoCosto == "2"){
                            DesTipoCosto = "Gasto (Opex)";
                        }else if (vm.DetallePrePeticionCompraOrdinaria.TipoCosto == "3"){
                            DesTipoCosto = "Inversión (Capex)";
                        }
                        
                        UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'danger', "Ha excedido del Monto Limite para el Tipo de Costo: " + DesTipoCosto, 5);
                        blockUI.stop();
                        vm.MontoLimiteValido = 0;
                        return;
                    }else{
                        var promise = DetallePrePeticionCompraOrdinariaService.RegistrarPrePeticionOrdinariaDetalle(vm.DetallePrePeticionCompraOrdinaria);
                        promise.then(function (resultado) {
                            blockUI.stop();
                            
                            var Respuesta = resultado.data;
                            if (Respuesta.TipoRespuesta == 0){
                                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'success', Respuesta.Mensaje, 5);
                                vm.DetallePrePeticionCompraOrdinaria.Id = Respuesta.Id;
                                $scope.$parent.vm.CerrarPrePeticionCompra();
                                $scope.$parent.vm.ListarPrePeticionDetalle();
                            }
                        }, function (response) {
                            return 0;
                            blockUI.stop();
                        });
                    }
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'danger', MensajesUI.DatosError, 5);
                });
        }

        function ValidarDatos(){
            if ($scope.$parent.vm.ProveedorSeleccionado.IdProveedor == null) {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar un Proveedor.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.TipoCosto == "-1"){
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar un Tipo de Costo.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.IdLineaProducto == "-1"){
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar una Línea Negocio.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.Descripcion == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar una Descripción de Compra.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.ProductManager == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar un Product Manager.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.Moneda == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar una Moneda.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.Monto == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar un monto', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.CoordinadorCompras == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar un Coordinado con Dirección de Compras de Telefónica.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.CompradorAsignado == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar un Comprador asignado.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.GrupoCompra == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar un Grupo de Compras.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.GrupoCompraConfirmado == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar un Grupo de Compras confirmado por Comprador.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.PQAdjudicado == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar un P X Q.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.PliegoTecnico == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar un Pliego técnico de la compra.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.CotizacionAdjunta == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar una Cotización de Proveedor.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.PosicionesOC == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar una Posiciones en OC.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.FechaEntregaOC == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar una Fecha de necesidad de entrega de Orden de Compra al Proveedor.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompraOrdinaria.Observaciones == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar Observaciones.', 5);
                blockUI.stop();
                return false;
            }

            return true;
        }

        function GuardarPrePeticionOrdinariaDetalle() {
            if (!ValidarDatos()){
                return;
            }

            vm.DetallePrePeticionCompraOrdinaria.IdCabecera = $scope.$parent.vm.PrePeticionCompra.Id;
            vm.DetallePrePeticionCompraOrdinaria.IdProveedor = $scope.$parent.vm.ProveedorSeleccionado.IdProveedor;
            
            ValidarMontoLimite();
        }
        
    }
})();