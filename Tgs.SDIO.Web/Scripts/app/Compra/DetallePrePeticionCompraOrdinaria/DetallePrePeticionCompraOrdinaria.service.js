(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('DetallePrePeticionCompraOrdinariaService', DetallePrePeticionCompraOrdinariaService);

    DetallePrePeticionCompraOrdinariaService.$inject = ['$http', 'URLS'];

    function DetallePrePeticionCompraOrdinariaService($http, $urls) {

        var service = {
            RegistrarPrePeticionOrdinariaDetalle: RegistrarPrePeticionOrdinariaDetalle,
            MostrarPrePeticionCompra: MostrarPrePeticionCompra,
            MostrarDetallePrePeticionCompra: MostrarDetallePrePeticionCompra,
            ObtenerPrePeticionOrdinariaDetalle: ObtenerPrePeticionOrdinariaDetalle
        };

        return service;

        function RegistrarPrePeticionOrdinariaDetalle(request) {
            return $http({
                url: $urls.ApiCompra + "DetallePrePeticionCompraOrdinaria/RegistrarPrePeticionOrdinariaDetalle", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerPrePeticionOrdinariaDetalle(request) {
            return $http({
                url: $urls.ApiCompra + "DetallePrePeticionCompraOrdinaria/ObtenerPrePeticionOrdinariaDetalle", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarPrePeticionCompra() {
            return $http({
                url: $urls.ApiCompra + "LineasCMI/MostrarPrePeticionCompra",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarDetallePrePeticionCompra() {
            return $http({
                url: $urls.ApiCompra + "PrePeticionCompra/MostrarDetallePrePeticionCompra",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarAnexos(iIdPeticionCompra) {
            return $http({
                url: $urls.ApiCompra + "ComentarioAnexo/ListarAnexos", 
                method: "POST",
                data: {IdPeticionCompra: iIdPeticionCompra}
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarAnexo(iIdPeticionCompra) {
            return $http({
                url: $urls.ApiCompra + "ComentarioAnexo/EliminarAnexo", 
                method: "POST",
                data: {IdPeticionCompra: iIdPeticionCompra}
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function DescargarAnexoArchivo(iIdAnexoArchivo) {
            window.location.href = $urls.ApiCompra + "ComentarioAnexo/DescargarAnexoArchivo?iIdAnexoArchivo=" + iIdAnexoArchivo;
           
        };

        function GuardarAnexos(Anexos, iIdPeticionCompra) {
            var datax = new FormData();
            var sTipoDocumento = [];

            Anexos.forEach(anexo => {
                sTipoDocumento.push(anexo.IdTipoDocumento);
                datax.append("file", anexo.Archivo);
            });
            datax.append("IdTipoDocumento", sTipoDocumento.join(","));
            datax.append("IdPeticionCompra", iIdPeticionCompra);

            var resultado =$.ajax({
                type: "POST",
                url: $urls.ApiCompra + "ComentarioAnexo/GuardarAnexo",  
                dataType: 'json',
                cache: false,
                processData: false,
                contentType: false, 
                data: datax
            }); 

            return resultado;

        };
    }
})();