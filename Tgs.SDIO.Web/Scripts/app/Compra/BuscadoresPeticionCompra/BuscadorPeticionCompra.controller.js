﻿(function () {
    'use strict'
    angular
    .module('app.Compra')
    .controller('BuscadorPeticionCompraController', BuscadorPeticionCompraController);

    BuscadorPeticionCompraController.$inject = ['RecursoService', 'BuscadorPeticionCompraService',
        'ProveedorService', 'blockUI', 'UtilsFactory', 'DTColumnDefBuilder', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$filter'];

    function BuscadorPeticionCompraController(RecursoService, BuscadorPeticionCompraService,
        ProveedorService, blockUI, UtilsFactory, DTColumnDefBuilder, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $filter) {

        var vm = this;

        //Inicio :: Variables Modal de Buscador Comprador
        vm.BuscarCompradorPeticionCompra = BuscarCompradorPeticionCompra;
        vm.SelecionarCompradorId = SelecionarCompradorId;
        vm.LimpiarFiltrosComprador = LimpiarFiltrosComprador;

        vm.CorreoComprador = '';
        vm.NombreComprador = '';
        vm.IdComprador = 0;
        //Fin :: Variables Modal de Buscador Comprador

        //Inicio :: Variables Modal de Buscador CeCo
        vm.BuscarCentroCostoPeticionCompra = BuscarCentroCostoPeticionCompra;
        vm.SelecionarCeCoId = SelecionarCeCoId;
        vm.LimpiarFiltrosCeCo = LimpiarFiltrosCeCo;

        vm.IdAreaSolicitante = -1;
        vm.IdCentroCosto = 0;
        vm.CodigoCeCo = '';
        vm.DescripcionCeCo = '';
        vm.Gerencia = '';
        vm.Direccion = '';
        //Fin :: Variables Modal de Buscador CeCo
        

        //Inicio :: Variables Modal de Buscador Responsable
        vm.CorreoResponsable = '';
        vm.NombreResponsable = '';
        vm.IdResponable = 0;

        vm.BuscarResponsablePeticionCompra = BuscarResponsablePeticionCompra;
        vm.SelecionarResponsableId = SelecionarResponsableId;
        vm.LimpiarFiltrosResponsable = LimpiarFiltrosResponsable;

        //Fin :: Variables Modal de Buscador Responsable

        //Inicio :: Variables Modal de Buscador Contrato Marco
        
        vm.ContratoMarco = {
            IdContratoMarco: 0,
            NumContrato: "",
            Descripcion: ""
        }

        vm.BuscarContratoMarcoPeticionCompra = BuscarContratoMarcoPeticionCompra;
        vm.SeleccionarContratoMarcoId = SeleccionarContratoMarcoId;
        vm.LimpiarFiltrosContratoMarco = LimpiarFiltrosContratoMarco;
        //Fin :: Variables Modal de Buscador Contrato Marco
        
        //Inicio :: Variables Modal de Buscador Cuentas

        vm.IdCuenta = 0;
        vm.Cuenta = "";
        vm.DescCuenta = "";
        vm.bCodigoCuenta = "";
        vm.bCuentaDescripcion = "";

        vm.BuscarCuentaDetalleCompra = BuscarCuentaDetalleCompra;
        vm.SelecionarCuentaId = SelecionarCuentaId;
        vm.LimpiarFiltrosCuenta = LimpiarFiltrosCuenta;
        //Fin :: Variables Modal de Buscador Cuentas

        vm.IdEstado = 1;

        vm.dtBuscarCompradorInstancePeticionCompra = {};
        vm.dtBuscarCompradorColumnsPeticionCompra = [
            DTColumnBuilder.newColumn('IdRecurso').notVisible(),
            DTColumnBuilder.newColumn('Nombre').withTitle('Comprador').withOption('width', '90%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCompradorPeticionCompra).withOption('width', '6%')
        ];

        vm.dtBuscarCentroCostoInstancePeticionCompra = {};
        vm.dtBuscarCentroCostoColumnsPeticionCompra = [
            DTColumnBuilder.newColumn('IdCentroCosto').notVisible(),
            DTColumnBuilder.newColumn('Descripcion').withTitle('Descripción Ce. Co.').notSortable().withOption('width', '5%'),
            DTColumnBuilder.newColumn('CodigoCeCo').withTitle('# Ce.Co.').notSortable().withOption('width', '3%'),
            DTColumnBuilder.newColumn('Cebe').withTitle('CeBe').notSortable().withOption('width', '2%'),
            DTColumnBuilder.newColumn('Ar').withTitle('AR').notSortable().withOption('width', '5%'),
            DTColumnBuilder.newColumn('ResponsableCeCo').withTitle('Responsable Ce.Co.').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('Gerencia').withTitle('Gerencia (Nivel 2)').notSortable().withOption('width', '15%'),
            DTColumnBuilder.newColumn('Gerente').withTitle('Gerente').notSortable().withOption('width', '15%'),
            DTColumnBuilder.newColumn('Direccion').withTitle('Direccion (Nivel 1)').notSortable().withOption('width', '15%'),
            DTColumnBuilder.newColumn('Director').withTitle('Director').notSortable().withOption('width', '15%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCentroCostoPeticionCompra).withOption('width', '5%')
        ];

        vm.dtBuscarResponsableInstancePeticionCompra = {};
        vm.dtBuscarResponsableColumnsPeticionCompra = [
            DTColumnBuilder.newColumn('IdRecurso').notVisible(),
            DTColumnBuilder.newColumn('Nombre').withTitle('Responsable').withOption('width', '90%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesResponsablePeticionCompra).withOption('width', '6%')
        ];

        vm.dtBuscarCuentaInstanceDetalleCompra = {};
        vm.dtBuscarCuentaColumnsDetalleCompra = [
            DTColumnBuilder.newColumn('IdCuenta').notVisible(),
            DTColumnBuilder.newColumn('Cuenta').withTitle('Cuenta').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable().withOption('width', '80%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCuentaDetalleCompra).withOption('width', '6%')
        ];

        vm.dtBuscarContratoMarcoInstance = {};
        vm.dtBuscarContratoMarcoColumns = [
            DTColumnBuilder.newColumn('IdContratoMarco').notVisible(),
            DTColumnBuilder.newColumn('ImporteContrato').notVisible(),
            DTColumnBuilder.newColumn('NumContrato').withTitle('Código CM').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('RazonSocialProveedor').withTitle('Proveedor').notSortable().withOption('width', '3%'),
            DTColumnBuilder.newColumn('RucProveedor').withTitle('RUC Proveedor').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('ProcesoAdjudicacion').withTitle('Proceso Adjudicación').notSortable().withOption('width', '5%'),
            DTColumnBuilder.newColumn('Moneda').withTitle('Moneda').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('ProductoGerencia').withTitle('Producto Gerencia').notSortable().withOption('width', '5%'),
            DTColumnBuilder.newColumn('FechaInicio').withTitle('Fecha Inicio').notSortable().withOption('width', '10%').renderWith(function(data, type) {
                return $filter('date')(new Date(parseInt(data.substr(6))), 'dd/MM/yyyy')
            }),
            DTColumnBuilder.newColumn('FechaFin').withTitle('Fecha Fin').notSortable().withOption('width', '10%').renderWith(function(data, type) {
                return $filter('date')(new Date(parseInt(data.substr(6))), 'dd/MM/yyyy')
            }),
            DTColumnBuilder.newColumn('Catalogado').withTitle('Catalogado').notSortable().withOption('width', '5%'),
            DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(AccionesContratoMarcoPeticionCompra).withOption('width', '3%')
        ];

        function AccionesContratoMarcoPeticionCompra(data, type, full, meta) {
            var respuesta = "<center><a title='Selecionar' data-dismiss='modal' ng-click='vm.SeleccionarContratoMarcoId(\"" + data.IdContratoMarco + "\",\"" + data.IdProveedor + "\",\"" + data.NumContrato + "\",\"" + data.Descripcion + "\",\"" + data.RazonSocialProveedor + "\",\"" + data.FechaFin + "\",\"" + data.Catalogado + "\",\"" + data.ImporteContrato + "\",\"" + data.Moneda + "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
            return respuesta;
        };

        function AccionesCompradorPeticionCompra(data, type, full, meta) {
            var respuesta = "<center><a title='Selecionar' data-dismiss='modal' ng-click='vm.SelecionarCompradorId(\"" + data.IdRecurso + "\",\"" + data.Nombre + "\",\"" + data.Email + "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
            return respuesta;
        };

        function AccionesCentroCostoPeticionCompra(data, type, full, meta) {
            var respuesta = "<center><a title='Selecionar' data-dismiss='modal' ng-click='vm.SelecionarCeCoId(\"" + data.IdCentroCosto + "\",\"" + data.CodigoCeCo + "\",\"" + data.Descripcion + "\",\"" + data.Gerencia + "\",\"" + data.Direccion + "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
            return respuesta;
        };

        function AccionesResponsablePeticionCompra(data, type, full, meta) {
            var respuesta = "<center><a title='Selecionar' data-dismiss='modal' ng-click='vm.SelecionarResponsableId(\"" + data.IdRecurso + "\",\"" + data.Nombre + "\",\"" + data.Email + "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
            return respuesta;
        };

        function AccionesCuentaDetalleCompra(data, type, full, meta) {
            var respuesta = "<center><a title='Selecionar' data-dismiss='modal' ng-click='vm.SelecionarCuentaId(\"" + data.IdCuenta + "\",\"" + data.Cuenta + "\",\"" + data.Descripcion + "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
            return respuesta;
        };

        //Inicio : Modal Comprador

        function BuscarCompradorPeticionCompra() {

            LimpiarGrillaComprador();

            $timeout(function () {
                vm.dtOptionsComPeticionCompra = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarPeticionCompraCompradorPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarPeticionCompraCompradorPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            let request = {
                Nombre: vm.bCompradorNombreComprador,
                IdEstado: vm.IdEstado,
                TipoRecurso: -1,
                IdCargo: 26,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = RecursoService.ListarRecursoPorCargoPaginado(request);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListRecursoDto
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaComprador();
            });
        };

        function LimpiarGrillaComprador() {
            vm.dtOptionsComPeticionCompra = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
        };

        BuscarCompradorPeticionCompra();

        function SelecionarCompradorId(idRecurso, nombre, correo) {

            vm.CorreoComprador = (correo == 'null' ? '' : correo);
            vm.NombreComprador = nombre;
            vm.IdComprador = idRecurso;

            $scope.$parent.vm.BComprador = vm.NombreComprador;
            $scope.$parent.vm.BCorreoComprador = vm.CorreoComprador,
            $scope.$parent.vm.PeticionCompra.IdComprador = vm.IdComprador;

            UtilsFactory.Alerta('#divAlertBuscarComprador', 'success', "Comprador selecionado: " + vm.NombreComprador, 5);

            $scope.$parent.vm.CerrarPopupComprador();
        }

        function LimpiarFiltrosComprador() {
            vm.bCompradorNombreComprador = null;
        }

        //Fin : Modal Comprador

        //Inicio : Modal Centro Costo

        function BuscarCentroCostoPeticionCompra() {

            LimpiarGrillaCeCo();

            $timeout(function () {
                vm.dtOptionsCeCoPeticionCompra = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarPeticionCompraCeCoPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarPeticionCompraCeCoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            let request = {
                IdAreaSolicitante: $scope.$parent.vm.IdAreaSolicitante,
                Descripcion: vm.bCentroCostoDescripcion,
                IdEstado: vm.IdEstado,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = BuscadorPeticionCompraService.ListarCentroCostoPorAreaSolPaginado(request);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListCeCoDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaCeCo();
            });
        };

        function LimpiarGrillaCeCo() {
            vm.dtOptionsCeCoPeticionCompra = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
        };

        BuscarCentroCostoPeticionCompra();

        function SelecionarCeCoId(idCentroCosto, codigo, descripcion, gerencia, direccion) {

            vm.IdCentroCosto = idCentroCosto;
            vm.CodigoCeCo = codigo;
            vm.DescripcionCeCo = descripcion;
            vm.Direccion = direccion;
            vm.Gerencia = gerencia;

            $scope.$parent.vm.BIdCentroCosto = vm.IdCentroCosto;
            $scope.$parent.vm.BUnidadSol = vm.DescripcionCeCo,
            $scope.$parent.vm.BCeCo = vm.CodigoCeCo;
            $scope.$parent.vm.BDireccionSol = vm.Direccion,
            $scope.$parent.vm.BGerenciaSol = vm.Gerencia;

            UtilsFactory.Alerta('#divAlertBuscarCentroCosto', 'success', "Centro de Costo selecionado: " + vm.CodigoCeCo, 5);

            $scope.$parent.vm.CerrarPopupCeCo();
        }

        function LimpiarFiltrosCeCo() {
            vm.bCentroCostoDescripcion = null;
        }

        //Fin : Modal Centro Costo

        //Inicio : Modal Responsable

        function BuscarResponsablePeticionCompra() {

            LimpiarGrillaResponsable();

            $timeout(function () {
                vm.dtOptionsRespPeticionCompra = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarPeticionCompraResponsablePaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarPeticionCompraResponsablePaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            let request = {
                Nombre: vm.bResponsableNombreResponsable,
                IdEstado: vm.IdEstado,
                TipoRecurso: -1,
                IdCargo: 22,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = RecursoService.ListarRecursoPorCargoPaginado(request);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListRecursoDto
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaComprador();
            });
        };

        function LimpiarGrillaResponsable() {
            vm.dtOptionsRespPeticionCompra = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
        };

        BuscarResponsablePeticionCompra();

        function SelecionarResponsableId(idRecurso, nombre, correo) {

            vm.CorreoResponsable = (correo == 'null' ? '' : correo);
            vm.NombreResponsable= nombre;
            vm.IdResponable = idRecurso;

            $scope.$parent.vm.bResponsablePostventa = vm.NombreResponsable;
            $scope.$parent.vm.bCorreoResponsable = vm.CorreoResponsable,
            $scope.$parent.vm.PeticionCompra.IdResponsablePostventa = vm.IdResponable;

            UtilsFactory.Alerta('#divAlertBuscarResponsable', 'success', "Responsable postventa selecionado: " + vm.NombreComprador, 5);

            $scope.$parent.vm.CerrarPopupComprador();
        }

        function LimpiarFiltrosResponsable() {
            vm.bResponsableNombreResponsable = null;
        }

        //Fin : Modal Responsable

        //Inicio : Modal Cuentas

        function BuscarCuentaDetalleCompra() {

            LimpiarGrillaCuentas();

            $timeout(function () {
                vm.dtOptionsCuentaDetalleCompra = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarDetalleCompraCuentaPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withLanguage({"sEmptyTable" : "No hay datos disponibles en la tabla"})
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarDetalleCompraCuentaPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            let request = {
                Cuenta: vm.bCodigoCuenta,
                Descripcion: vm.bCuentaDescripcion,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = BuscadorPeticionCompraService.ListarCuentaPaginado(request);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListaCuentaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaCeCo();
            });
        };

        function LimpiarGrillaCuentas() {
            vm.dtOptionsCuentaDetalleCompra = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
        };

        BuscarCuentaDetalleCompra();

        function SelecionarCuentaId(idCuenta, cuenta, desc) {

            vm.IdCuenta = idCuenta;
            vm.Cuenta = cuenta;
            vm.DescCuenta = desc;

            let listDetalleCompra = $scope.$parent.vm.listaDetalleCompra;

            let oDetalleCompra = listDetalleCompra.find(dp => dp.IdDetalleCompra == $scope.$parent.vm.IdDetalleCompraActual);
            oDetalleCompra.IdCuenta = vm.IdCuenta;
            oDetalleCompra.Cuenta = vm.Cuenta;

            $scope.$parent.vm.listaDetalleCompra = listDetalleCompra;

            UtilsFactory.Alerta('#divAlertBuscarCuenta', 'success', "Cuenta selecionada: " + vm.DescCuenta, 5);

            $scope.$parent.vm.CerrarPopupCuenta();
        }

        function LimpiarFiltrosCuenta() {
            vm.bCuentaDescripcion = null;
        }
        //Fin : Modal Cuentas

        //Inicio : Modal Contrato Marco

        function BuscarContratoMarcoPeticionCompra() {

            LimpiarGrillaContratoMarco();

            $timeout(function () {
                vm.dtOptionsCoMaPeticionCompra = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarPeticionCompraContratoMarcoPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarPeticionCompraContratoMarcoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            
            let request = {
                NumContrato: vm.ContratoMarco.NumContrato,
                Proveedor: vm.ContratoMarco.Proveedor,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = BuscadorPeticionCompraService.ListarContratoMarcoPaginado(request);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListaContratoMarco
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaContratoMarco();
            });
        };

        function LimpiarGrillaContratoMarco() {
            vm.dtOptionsCoMaPeticionCompra = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
        };

        BuscarContratoMarcoPeticionCompra();

        function SeleccionarContratoMarcoId(IdContratoMarco, IdProveedor, NumContrato, Descripcion, Proveedor, FechaFin, ContratoCatolagado, Importe, Moneda) {
            var contratoMarco = { IdContratoMarco: IdContratoMarco };
            var promise = BuscadorPeticionCompraService.ObtenerContratoMarcoPorId(contratoMarco);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;

                $scope.$parent.vm.ContratoMarco.IdContratoMarco = IdContratoMarco;
                $scope.$parent.vm.ContratoMarco.NumContrato = Respuesta.NumContrato;
                $scope.$parent.vm.ContratoMarco.Descripcion = Respuesta.Descripcion;

                let ContratoCatolagado = "-1";
                if (Respuesta.ContratoCatolagado != "null") {
                    ContratoCatolagado = (Respuesta.ContratoCatolagado == "SI" ? 0 : 1)
                }else{
                    ContratoCatolagado = "-1";
                }    
                
                $scope.$parent.vm.ContratoMarco.IdProveedor = Respuesta.IdProveedor;
                $scope.$parent.vm.ContratoMarco.FechaFin = $filter('date')(new Date(Respuesta.FechaFin.match(/\d+/)[0] * 1), 'dd/MM/yyyy');
                $scope.$parent.vm.ContratoMarco.ContratoCatolagado = ContratoCatolagado;
                $scope.$parent.vm.ContratoMarco.Importe = Respuesta.ImporteContrato;
                $scope.$parent.vm.ContratoMarco.Moneda = Respuesta.Moneda;

                if (!$scope.$parent.vm.PeticionCompraDCM){
                    $scope.$parent.vm.DetallePrePeticionCompra.MonedaCompra = Respuesta.Moneda
                }
            
                ObtenerProveedor(IdProveedor);

                UtilsFactory.Alerta('#divAlertBuscarComprador', 'success', "Contrato Marco selecionado: " + vm.NombreComprador, 5);

                $scope.$parent.vm.CerrarPopupContratoMarco();
            }, function (response) {
                blockUI.stop();
            });
            
        }
        
        function LimpiarFiltrosContratoMarco() {
            vm.ContratoMarco = {};
        }

        function ObtenerProveedor(iIdProveedor){
            let request = {
                IdEstado: 1,
                IdProveedor: iIdProveedor
            };
            var promise = ProveedorService.ObtenerProveedorById(request);
            promise.then(function (resultado) {
                let oRespuesta = resultado.data;

                $scope.$parent.vm.ContratoMarco.Proveedor = oRespuesta.Descripcion;

                $scope.$parent.vm.ContratoMarco.IdProveedor = oRespuesta.IdProveedor;
                $scope.$parent.vm.ContratoMarco.RazonSocialProveedor = oRespuesta.Descripcion;
                $scope.$parent.vm.ContratoMarco.RUC = oRespuesta.RUC;
                $scope.$parent.vm.ContratoMarco.NombreContacto = oRespuesta.NombrePersonaContacto;
                $scope.$parent.vm.ContratoMarco.TelefonoContacto = oRespuesta.TelefonoContactoPrincipal;
                $scope.$parent.vm.ContratoMarco.EmailContacto = oRespuesta.CorreoContactoPrincipal;
                $scope.$parent.vm.ContratoMarco.CodigoSrm = oRespuesta.CodigoProveedor;
                $scope.$parent.vm.ContratoMarco.CodigoSap = oRespuesta.CodigoSap;
                $scope.$parent.vm.ContratoMarco.CodigoProveedor = oRespuesta.CodigoProveedor;
                
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }
        
        //Fin : Modal Contrato Marco
    }

})();