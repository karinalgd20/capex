﻿(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('BuscadorPeticionCompraService', BuscadorPeticionCompraService);

    BuscadorPeticionCompraService.$inject = ['$http', 'URLS'];


    function BuscadorPeticionCompraService($http, $urls) {
        
        var service = {
            ModalPeticionCompraBuscarComprador: ModalPeticionCompraBuscarComprador,
            ModalPeticionCompraBuscarCentroCosto: ModalPeticionCompraBuscarCentroCosto,
            ModalPeticionCompraBuscarResponsable: ModalPeticionCompraBuscarResponsable,
            ModalDetalleCompraBuscarCuenta: ModalDetalleCompraBuscarCuenta,
            ModalPeticionCompraBuscarContratoMarco: ModalPeticionCompraBuscarContratoMarco,
            ListarCentroCostoPorAreaSolPaginado: ListarCentroCostoPorAreaSolPaginado,
            ListarCuentaPaginado: ListarCuentaPaginado,
            ListarContratoMarcoPaginado: ListarContratoMarcoPaginado,
            ObtenerContratoMarcoPorId: ObtenerContratoMarcoPorId
        };
        return service;

        function ModalPeticionCompraBuscarComprador(dto) {
            return $http({
                url: $urls.ApiCompra + "PeticionCompraComprador/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ModalPeticionCompraBuscarCentroCosto(dto) {
            return $http({
                url: $urls.ApiCompra + "PeticionCompraCentroCosto/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ModalPeticionCompraBuscarResponsable(dto) {
            return $http({
                url: $urls.ApiCompra + "PeticionCompraResponsable/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ModalPeticionCompraBuscarContratoMarco(dto) {
            return $http({
                url: $urls.ApiCompra + "PeticionCompraContratoMarco/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
        
        function ListarCentroCostoPorAreaSolPaginado(dto) {
            return $http({
                url: $urls.ApiCompra + "PeticionCompraCentroCosto/ListarCentroCostoPorAreaPaginado",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalDetalleCompraBuscarCuenta(dto) {
            return $http({
                url: $urls.ApiCompra + "DetalleCompraCuenta/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ListarCuentaPaginado(dto) {
            return $http({
                url: $urls.ApiCompra + "DetalleCompraCuenta/ListarCuentasPaginado",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarContratoMarcoPaginado(dto) {
            return $http({
                url: $urls.ApiCompra + "PeticionCompraContratoMarco/ListarContratoMarcoPaginado",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerContratoMarcoPorId(dto) {
            return $http({
                url: $urls.ApiCompra + "PeticionCompraContratoMarco/ObtenerContratoMarcoPorId",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();