(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('SoporteService', SoporteService);

    SoporteService.$inject = ['$http', 'URLS'];

    function SoporteService($http, $urls) {

        var service = {
            MostrarInfoDatosGenerales: MostrarInfoDatosGenerales,
            MostrarInfoClienteProveedor: MostrarInfoClienteProveedor,
            MostrarInfoDetalleCompra: MostrarInfoDetalleCompra,
            ListarEtapaPeticionCompraPaginado: ListarEtapaPeticionCompraPaginado
        };

        return service;

        function MostrarInfoDatosGenerales() {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerInformacionDatosGenerales",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoClienteProveedor() {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerInformacionClienteProveedor",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoDetalleCompra() {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerInformacionDetalleCompra", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarEtapaPeticionCompraPaginado(dto) {
            return $http({
                url: $urls.ApiCompra + "Soporte/ListarEtapaPeticionCompraPaginado",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();