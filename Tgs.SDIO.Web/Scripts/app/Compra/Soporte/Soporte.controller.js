(function () {
    'use strict'
    angular
        .module('app.Compra')
        .controller('SoporteController', SoporteController);

        SoporteController.$inject = ['SoporteService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$filter'];

        function SoporteController(SoporteService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $filter) {

        var vm = this;

        CargarDetalle();

        vm.dtInstance = {};
        vm.dtColumns = [
            DTColumnBuilder.newColumn("NUMERO_REGISTRO").withTitle('').notSortable().withOption('width', '5%'),
            DTColumnBuilder.newColumn("Etapa").withTitle('Etapa').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("EstadoEtapa").withTitle('Estado').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("FechaEstado").withTitle('Fecha del estado actual').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("RecursoAtiende").withTitle('Responsable Actual').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn("CorreoRecursoAtiende").withTitle('Correo Responsable Actual').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn("FechaAsignacion2").withTitle('Fecha Asignación 2').notSortable().withOption('width', '5%'),
            DTColumnBuilder.newColumn("ResponsableAnterior").withTitle('Responsable Anterior').notSortable().withOption('width', '5%'),
            DTColumnBuilder.newColumn("FechaAsignacion1").withTitle('Fecha Asignación 1').notSortable().withOption('width', '5%'),
        ];

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('paging', false);
        }

        function ListarEtapaPeticionCompra(sSource, aoData, fnCallback, oSettings) {

            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var request = {
                IdPeticionCompra: $scope.$parent.vm.PeticionCompra.IdPeticionCompra,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = SoporteService.ListarEtapaPeticionCompraPaginado(request);
            promise.then(function (resultado) {

                var listaSoporte = resultado.data.ListaEtapaDtoResponse;
                listaSoporte.forEach(detalle => {
                    var dFechaEstado = new Date(parseInt(detalle.FechaEstado.substr(6)));
                    detalle.FechaEstado = $filter('date')(dFechaEstado, 'dd/MM/yyyy HH:MM:ss');
                });

                var result = {
                    'draw': draw,
                    'recordsTotal': (listaSoporte.length > 0 ? listaSoporte[0].TotalRow : 0),
                    'recordsFiltered': (listaSoporte.length > 0 ? listaSoporte[0].TotalRow : 0),
                    'data': listaSoporte
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function CargarDetalle() {
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(ListarEtapaPeticionCompra)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withLanguage({ "sEmptyTable": "No hay datos disponibles en la tabla" })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }



    }
})();