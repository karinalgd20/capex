(function () {
    'use strict'
    angular
        .module('app.Compra')
        .controller('BandejaPrePeticionCompraController', BandejaPrePeticionCompraController);

    BandejaPrePeticionCompraController.$inject = ['BandejaPrePeticionCompraService', 'MaestraService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$filter'];

    function BandejaPrePeticionCompraController(BandejaPrePeticionCompraService, MaestraService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $filter)
    {
        var vm = this;
       
        vm.BuscarPrePeticionCabecera = BuscarPrePeticionCabecera;

        vm.EnlaceRegistrar = $urls.ApiCompra + "PrePeticionCompra/Index";

        vm.FiltrosPreCompra = {
            Tipo: -1
        }

        vm.Filtros = {};
       
        vm.dtInstancePrePeticionCabecera = {};
        vm.dtOptionsPrePeticionCabecera = [];
        vm.dtColumnsPrePeticionCabecera = [
            DTColumnBuilder.newColumn('Id').notVisible(),
            DTColumnBuilder.newColumn('IdProyecto').notVisible(),
            DTColumnBuilder.newColumn('IdLineaCMI').notVisible(),
            DTColumnBuilder.newColumn('DescripcionCliente').withTitle('Cliente').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('DescripcionProyecto').withTitle('Proyecto').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn('Servicio').withTitle('CMI').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('TipoCompra').withTitle('Tipo Compra').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];

        BuscarPrePeticionCabecera();
        ListarTipoCompra();

        function ListarTipoCompra() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 481
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                Respuesta = Respuesta.filter(tc => tc.Codigo > 1);

                vm.ListarTipoCompra = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function AccionesBusqueda(data, type, full, meta) {
            var lnk = $urls.ApiCompra + "PrePeticionCompra/RegistrarPrePeticionCompra/?IdProyecto=" + data.IdProyecto + "&IdDetalleFinanciero=" + data.IdLineaCMI + "&Id=" + data.Id
            return "<div class='col-xs-6 col-sm-6'><a title='Editar' class='btn btn-primary'  id='tab-button' href='" + lnk +  "' >" + "<span class='fa fa-edit fa-lg'></span>" + "</a></div>  "
        }

        function LimpiarGrillaPrePeticionCompraCabecera() {

            vm.dtOptionsPrePeticionCabecera = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarPrePeticionCabecera() {
            blockUI.start();

            LimpiarGrillaPrePeticionCompraCabecera();

            $timeout(function () {
                vm.dtOptionsPrePeticionCabecera = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withOption('lengthChange', false)
                    .withFnServerData(ListarPrePeticionCabecera)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(5);
            }, 500);

        }

        function ListarPrePeticionCabecera(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var request = {
                IdEstado: 1,
                Tipo: vm.FiltrosPreCompra.Tipo,
                DescripcionCliente: vm.FiltrosPreCompra.DescripcionCliente,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = BandejaPrePeticionCompraService.ListarPrePeticionCabecera(request);
            promise.then(function (resultado) {
                let iTotalRegistros = 0;

                if (resultado.data.length > 0) {
                    iTotalRegistros = resultado.data[0].TotalRegistros;
                }

                var result = {
                    'draw': draw,
                    'recordsTotal': iTotalRegistros,
                    'recordsFiltered': iTotalRegistros,
                    'data': resultado.data
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
            });
        }
    }
})();