(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('BandejaPrePeticionCompraService', BandejaPrePeticionCompraService);

    BandejaPrePeticionCompraService.$inject = ['$http', 'URLS'];

    function BandejaPrePeticionCompraService($http, $urls) {

        var service = {
            ListarPrePeticionCabecera: ListarPrePeticionCabecera
        };

        return service;

        function ListarPrePeticionCabecera(request) {
            return $http({
                url: $urls.ApiCompra + "BandejaPrePeticionCompra/ListaPrePeticionCabecera",
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();