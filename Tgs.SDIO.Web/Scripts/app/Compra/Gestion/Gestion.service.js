(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('GestionService', GestionService);

    GestionService.$inject = ['$http', 'URLS'];

    function GestionService($http, $urls) {

        var service = {
            ObtenerGestion: ObtenerGestion,
            GuardarGestion: GuardarGestion
        };

        return service;

        function ObtenerGestion(request) {
            return $http({
                url: $urls.ApiCompra + "Gestion/ObtenerGestion", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function GuardarGestion(request) {
            return $http({
                url: $urls.ApiCompra + "Gestion/GuardarGestion", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();