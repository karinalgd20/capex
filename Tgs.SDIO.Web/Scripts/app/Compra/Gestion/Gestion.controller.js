(function () {
    'use strict'
    angular
        .module('app.Compra')
        .controller('GestionController', GestionController);

    GestionController.$inject = ['GestionService', 'AnexoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function GestionController(GestionService, AnexoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector)
    {
        var vm = this;
        vm.GuardarGestion = GuardarGestion;
        vm.GuardarActa = GuardarActa;
        vm.DescargarArchivo = DescargarArchivo;
        vm.CestaLiberada = -1;
        vm.PeticionDCM = $scope.$parent.vm.PeticionCompraDCM;
        vm.GestionAsociadaPro = $scope.$parent.vm.AsociadoProyecto;
        vm.FlagAPOrdinariaInv = $scope.$parent.vm.FlagAPOrdinariaInv;
        vm.PeticionOrdinaria = $scope.$parent.vm.PeticionCompraOrdinaria;
        vm.PeticionCompraAPOrdinariaOpex = $scope.$parent.vm.PeticionCompraAPOrdinariaOpex;
        vm.PeticionCompraGeneral = $scope.$parent.vm.PeticionCompraGeneral;
        vm.FlagEsUsuarioGeneraPeticion = $scope.$parent.vm.flagUsuarioAtiende;

        vm.Ejecutado = 0;
        vm.ListaDocumentos = [];
        vm.Ver = $scope.$parent.vm.Ver;
        vm.ArchivoPesoLimite = 5242880; // 5MB

        vm.Gestion = {
            FechaCesta: null,
            FechaPedido: null,
            EnvioPedido: null,
            FechaActaRecibida: null,
            FechaConfirmacion: null,
            FechaAtencionGestor: null,
            ContratoMarco: "",
            IdTipoMoneda: 1
        };

        angular.element(document).ready(function () {
            vm.Accion = Accion;
            vm.Orden = IdOrdenEtapa;
            vm.FlagEtapaCertificada = (Etapa == "CERTIFICADA" ? true : false);
            if (IdPeticionCompra !== "0") {
                blockUI.start();
                ObtenerGestion();
                ListarAnexos();
                ObtenerActa();
            }

            $timeout(function () {
                blockUI.stop();
            }, 500);

        });

        function ObtenerGestion() {
            blockUI.start();
            vm.Gestion.IdPeticionCompra = $scope.$parent.vm.PeticionCompra.IdPeticionCompra;
            
            var promise = GestionService.ObtenerGestion(vm.Gestion);
            promise.then(function (resultado) {
                let oRespuesta = resultado.data;
                vm.Gestion = oRespuesta; 
                vm.Gestion.FechaCesta = vm.Gestion.FechaCesta == null ? null : new Date(parseInt(vm.Gestion.FechaCesta.substr(6)));
                vm.Gestion.FechaPedido = vm.Gestion.FechaPedido == null ? null : new Date(parseInt(vm.Gestion.FechaPedido.substr(6)));
                vm.Gestion.EnvioPedido = vm.Gestion.EnvioPedido == null ? null : new Date(parseInt(vm.Gestion.EnvioPedido.substr(6)));
                vm.Gestion.FechaActaRecibida = vm.Gestion.FechaActaRecibida == null ? null : new Date(parseInt(vm.Gestion.FechaActaRecibida.substr(6)));
                vm.Gestion.FechaConfirmacion = vm.Gestion.FechaConfirmacion == null ? null : new Date(parseInt(vm.Gestion.FechaConfirmacion.substr(6)));
                vm.Gestion.FechaAtencionGestor = vm.Gestion.FechaAtencionGestor == null ? null : new Date(parseInt(vm.Gestion.FechaAtencionGestor.substr(6)));
                vm.CestaLiberada = vm.Gestion.CestaLiberada == null ? "-1": (vm.Gestion.CestaLiberada == true ? "0" : "1");

                if (vm.Gestion.ContratoMarco == null) {
                    vm.Gestion.ContratoMarco = "";
                }

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });

            $timeout(function () {
                blockUI.stop();
            }, 500);
        }

        function ValidarDatos() {
            
            if (!vm.PeticionDCM && !vm.PeticionCompraAPOrdinariaOpex && !vm.FlagAPOrdinariaInv) {
                if (vm.Gestion.ContratoMarco == "") {
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Debe ingresar un Contrato Marco.', 5);
                    $scope.$parent.vm.blockUI.stop();
                    $scope.$parent.vm.blockUI.stop();
                    return false;
                }
            }

            if (vm.GestionAsociadaPro) {
                if (Number(vm.Orden) == 1) {

                    if (!$scope.$parent.vm.AdjuntoCotizacion) {
                        UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Debe de adjuntar cotizacion en la pestana de Comentarios-Anexos.', 5);
                        $scope.$parent.vm.blockUI.stop();
                        $scope.$parent.vm.blockUI.stop();
                        return false;
                    }
                }
            }


            if (vm.Ejecutado != 0) {
                return false;
            }
            return true;
        }

        function GuardarGestion(){
            $scope.$parent.vm.blockUI.start();
            $scope.$parent.vm.blockUI.start();

            if (!ValidarDatos()){
                return;
            }

            vm.Gestion.IdPeticionCompra = $scope.$parent.vm.PeticionCompra.IdPeticionCompra;
            vm.Gestion.IdEtapaPeticionCompra = $scope.$parent.vm.IdEtapaPeticionCompra;
            vm.Ejecutado = 1;
            var promise = GestionService.GuardarGestion(vm.Gestion);
            promise.then(function (resultado) {
                let oRespuesta = resultado.data;

                if (oRespuesta.TipoRespuesta == 0){
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'success', oRespuesta.Mensaje, 5);
                    ObtenerGestion();
                }

                blockUI.stop();

                if (vm.Orden == "1") {
                    IrBandeja();
                }
                vm.Ejecutado = 0;
            }, function (response) {
                blockUI.stop();
            });

            $timeout(function () {
                $scope.$parent.vm.blockUI.stop();
                $scope.$parent.vm.blockUI.stop();
            }, 500);

        }

        function ListarAnexos() {
            var promise = AnexoService.ListarAnexos($scope.$parent.vm.PeticionCompra.IdPeticionCompra);
            promise.then(function (response) {
                var Respuesta = response.data;
                vm.ListaAnexo = Respuesta;
                let AnexosTipoCotizacion = vm.ListaAnexo.filter(d => d.IdTipoDocumento == 3);
                if (AnexosTipoCotizacion.length > 0) {
                    $scope.$parent.vm.AdjuntoCotizacion = true;
                } else {
                    $scope.$parent.vm.AdjuntoCotizacion = false;
                }
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        $scope.UploadFiles = function (files) {
            if (files.length == 0) return;

            let archivo = files[0];

            vm.Documento.Archivo = archivo
            vm.Documento.NombreArchivo2 = archivo.name;
            vm.Documento.PesoArchivo = archivo.size;
        }

        function GuardarActa(){
            $scope.$parent.vm.blockUI.start();
            
            if (!ValidarDocumentoActa()) {
                $timeout(function () {
                    $scope.$parent.vm.blockUI.stop();
                }, 500);
                return;
            }

            vm.Documento.IdTipoDocumento = 6; //Acta de aceptacion
            vm.Documento.EsNuevo = true;
            vm.Documento.NombreArchivo = vm.Documento.NombreArchivo2;
            vm.ListaDocumentos.push(vm.Documento);

            var promise = AnexoService.GuardarAnexos(vm.ListaDocumentos, $scope.$parent.vm.PeticionCompra.IdPeticionCompra).then(function (response) {
                var Respuesta = response;
                if (Respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'success', Respuesta.Mensaje, 5);

                    $scope.$parent.vm.flagActaSubidaUsuario = ($scope.$parent.vm.flagActaAcept == null ? false : (Etapa == "CERTIFICADA" ? true : false));

                    ObtenerActa();
                } else {
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'danger', Respuesta.Mensaje, 20);
                }

                $scope.$parent.vm.blockUI.stop();
            }, function (response) {
                $scope.$parent.vm.blockUI.stop();
            });

            $scope.$parent.vm.blockUI.stop();
        }

        function ValidarDocumentoActa(){
            if (vm.Documento.Archivo == null) {
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe adjuntar un archivo.", 5);
                $scope.$parent.vm.blockUI.stop();
                return false;
            } else if (vm.Documento.NombreArchivo2 == "") {
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe de seleccionar el archivo a adjuntar.", 5);
                $scope.$parent.vm.blockUI.stop();
                return false;
            } else if (vm.Documento.PesoArchivo > vm.ArchivoPesoLimite) {
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "El tamaño del archivo es demasiado grande. El tamaño máximo permitido es de 5 MB.", 5);
                $scope.$parent.vm.blockUI.stop();
                return false;
            }

            return true;
        }

        function ObtenerActa() {
            blockUI.start();
            blockUI.start();

            var promise = AnexoService.ListarAnexos($scope.$parent.vm.PeticionCompra.IdPeticionCompra);
            promise.then(function (response) {
                var Respuesta = response.data;
                let documento = Respuesta.filter(d => d.IdTipoDocumento == 6);
                vm.Documento = documento.length > 0 ? documento[0] : null;

                if (vm.Documento == null) {
                    NuevoDocumento()
                }

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function DescargarArchivo(documento) {
            AnexoService.DescargarAnexoArchivo(documento.IdAnexoPeticionCompra);
        }

        function NuevoDocumento() {
            vm.Documento = {
                IdTipoDocumento: -1,
                DesTipoDocumento: "",
                Archivo: "",
                NombreArchivo: "",
                NombreArchivo2: "",
                EsNuevo: true
            }
            vm.ListaDocumentos = [];
        }

        function IrBandeja(){
            window.location = "/Compra/bandejapeticioncompra";
        }
    }
})();