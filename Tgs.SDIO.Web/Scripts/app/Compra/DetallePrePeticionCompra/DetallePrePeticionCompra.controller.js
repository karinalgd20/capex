(function () {
    'use strict'
    angular
        .module('app.Compra')
        .controller('DetallePrePeticionCompraController', DetallePrePeticionCompraController);

        DetallePrePeticionCompraController.$inject = ['DetallePrePeticionCompraService',
        'MaestraService', 'BuscadorPeticionCompraService', 'PrePeticionCompraService', 'ProveedorService', 'RecursoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$filter'];

    function DetallePrePeticionCompraController(DetallePrePeticionCompraService,
        MaestraService, BuscadorPeticionCompraService, PrePeticionCompraService, ProveedorService, RecursoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $filter)
    {
        var vm = this;
        vm.GuardarPrePeticionDCMDetalle = GuardarPrePeticionDCMDetalle;

        vm.ModalBuscarContratoMarco = ModalBuscarContratoMarco;
        vm.CerrarPopupContratoMarco = CerrarPopupContratoMarco;

        vm.MontoLimiteValido = 1;

        vm.Filtros = {};
        vm.Proyecto = {};

        LimpiarFormulario();

        vm.ClienteSeleccionado = {};
        vm.ContratoMarco = {};

        vm.ObtenerSaldo = ObtenerSaldo;
        
        //ObtenerProyecto();
        //ListarTipoCompra();
        
        ListarMoneda();
        ListarCatalogado();
        ListarTipoCosto();
        ListarLineaProducto();
        ListarTipoEntrega();

        ObtenerPrePeticionDCMDetalle();

        $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });

        function LimpiarFormulario(){
            vm.DetallePrePeticionCompra = {
                IdCabecera: $scope.$parent.vm.PrePeticionCompra.Id,
                TipoCosto: "-1",
                IdLineaProducto: "-1",
                AutorizadoPor: "-1",
                TipoEntrega: "-1",
                MonedaCompra: null,
                PosicionOC: "-1",
                PQAdjudicado: "-1",
                CotizacionAdjunta: "-1",
                IdContratoMarco: 0,
                PlazoEntrega: "",
                FechaEntregaOC: "",
                Observaciones: "",
                Descripcion: "",
                Direccion: "",
                ProductManager: "",
                Monto: ""
            };
        }

        function ObtenerPrePeticionDCMDetalle(){
            var request = {
                Id: $scope.$parent.vm.IdDetalle
            };

            var promise = DetallePrePeticionCompraService.ObtenerPrePeticionDCMDetalle(request);
                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                    
                    if (Respuesta.Id > 0){
                        vm.DetallePrePeticionCompra = Respuesta;

                        if (Respuesta.FechaEntregaOC != null){
                            vm.DetallePrePeticionCompra.FechaEntregaOC = new Date(parseInt(Respuesta.FechaEntregaOC.substr(6)));
                        }
                        

                        var contratoMarco = { IdContratoMarco: Respuesta.IdContratoMarco };
                        var promise = BuscadorPeticionCompraService.ObtenerContratoMarcoPorId(contratoMarco);
                        promise.then(function (resultado) {
                            var RespuestaC = resultado.data;

                            if (RespuestaC.Catalogado != null) {
                                RespuestaC.Catalogado = (RespuestaC.Catalogado == "SI" ? 1 : 2);
                            } else {
                                RespuestaC.Catalogado = "-1";
                            }

                            vm.ContratoMarco.IdContratoMarco = RespuestaC.IdContratoMarco;
                            vm.ContratoMarco.IdProveedor = RespuestaC.IdProveedor;
                            vm.ContratoMarco.NumContrato = RespuestaC.NumContrato;
                            vm.ContratoMarco.FechaFin = new Date(parseInt(RespuestaC.FechaFin.substr(6))).format("dd/mm/yyyy");
                            vm.ContratoMarco.ContratoCatolagado = RespuestaC.Catalogado;
                            vm.ContratoMarco.Moneda = RespuestaC.Moneda;
                            vm.ContratoMarco.Importe = RespuestaC.ImporteContrato;

                            ObtenerProveedor(RespuestaC.IdProveedor);

                        }, function (response) {
                            blockUI.stop();
                        });

                    }else{
                        LimpiarFormulario();
                    }

                    ObtenerSaldo();
                    
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Proyecto', 'danger', MensajesUI.DatosError, 5);
                });
        }

        function ObtenerProveedor(iIdProveedor) {
            let request = {
                IdEstado: 1,
                IdProveedor: iIdProveedor
            };
            var promise = ProveedorService.ObtenerProveedor(request);
            promise.then(function (resultado) {
                let oRespuesta = resultado.data;

                vm.ContratoMarco.Proveedor = oRespuesta.Descripcion;

                vm.ContratoMarco.IdProveedor = oRespuesta.IdProveedor;
                vm.ContratoMarco.RazonSocialProveedor = oRespuesta.Descripcion;
                vm.ContratoMarco.RUC = oRespuesta.RUC;
                vm.ContratoMarco.NombreContacto = oRespuesta.NombrePersonaContacto;
                vm.ContratoMarco.TelefonoContacto = oRespuesta.TelefonoContactoPrincipal;
                vm.ContratoMarco.EmailContacto = oRespuesta.CorreoContactoPrincipal;
                vm.ContratoMarco.CodigoSrm = oRespuesta.CodigoProveedor;
                vm.ContratoMarco.CodigoSap = oRespuesta.CodigoSap;
                vm.ContratoMarco.CodigoProveedor = oRespuesta.CodigoProveedor;

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ObtenerSaldo() {
            let request = {
                Id: vm.DetallePrePeticionCompra.IdCabecera,
                TipoCosto: vm.DetallePrePeticionCompra.TipoCosto
            };
            var promise = PrePeticionCompraService.ObtenerSaldo(request);
            promise.then(function (resultado) {
                let oRespuesta = resultado.data;
                vm.MensajeSaldo = oRespuesta;
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarMoneda() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 98
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion2(maestra);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;

                let oListaMoneda = [];
                Respuesta.forEach(element => {
                    oListaMoneda.push({Codigo: element.Valor, Valor2: element.Valor2, Descripcion: element.Descripcion});
                });
                
                vm.ListaMonedas = UtilsFactory.AgregarItemSelect(oListaMoneda);
                
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarCatalogado() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 537
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListaCatalogado = UtilsFactory.AgregarItemSelect(Respuesta);
                
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarTipoCosto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 485
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;

                Respuesta = Respuesta.filter(tc => tc.Codigo > 1);

                vm.ListaTipoCosto = UtilsFactory.AgregarItemSelect(Respuesta);
                
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarLineaProducto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 489
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaLineaProducto = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarTipoEntrega() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 540
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaTipoEntrega = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ModalBuscarContratoMarco() {
            var RegPeticionCompra = {
                IdPeticionCompra: 0
            };

            var promise = BuscadorPeticionCompraService.ModalPeticionCompraBuscarContratoMarco(RegPeticionCompra);

            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCompraContratoMarco").html(content);
                });

                $('#ModalCompraContratoMarco').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CerrarPopupContratoMarco() {
            $('#ModalCompraContratoMarco').modal('hide');
        };

        function ValidarMontoLimite(){
            var request = {
                Id: vm.DetallePrePeticionCompra.IdCabecera,
                IdDetalle: vm.DetallePrePeticionCompra.Id,
                TipoCosto: vm.DetallePrePeticionCompra.TipoCosto,
                Tipo: $scope.$parent.vm.PrePeticionCompra.Tipo,
                Monto: vm.DetallePrePeticionCompra.Monto
            };

            var promise = PrePeticionCompraService.ValidarMontoLimite(request);
                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                    if (Respuesta == 1){
                        let DesTipoCosto = ""
                        if (vm.DetallePrePeticionCompra.TipoCosto == "2"){
                            DesTipoCosto = "Gasto (Opex)";
                        }else if (vm.DetallePrePeticionCompra.TipoCosto == "3"){
                            DesTipoCosto = "Inversión (Capex)";
                        }
                        
                        UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'danger', "Ha excedido del Monto Limite para el Tipo de Costo: " + DesTipoCosto, 5);
                        blockUI.stop();
                        vm.MontoLimiteValido = 0;
                        return;
                    }else{
                        var promise = DetallePrePeticionCompraService.RegistrarPrePeticionDCMDetalle(vm.DetallePrePeticionCompra);
                        promise.then(function (resultado) {
                            blockUI.stop();
                            
                            var Respuesta = resultado.data;
                            if (Respuesta.TipoRespuesta == 0){
                                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'success', Respuesta.Mensaje, 5);
                                vm.DetallePrePeticionCompra.Id = Respuesta.Id;
                                $scope.$parent.vm.CerrarPrePeticionCompra();
                                $scope.$parent.vm.ListarPrePeticionDetalle();
                            }
                        }, function (response) {
                            return 0;
                            blockUI.stop();
                        });
                    }
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'danger', MensajesUI.DatosError, 5);
                });
        }

        function ValidarDatos(){
            
            if (vm.ContratoMarco.IdContratoMarco == null){
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar un Contrato Marco.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.TipoCosto == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar un Tipo de Costo.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.Direccion == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar una Dirección.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.IdLineaProducto == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar una Línea Negocio.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.Descripcion == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar una Descripción de Compra.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.ProductManager == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar una Product Manager.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.AutorizadoPor == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar un Autorizado por Productos.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.TipoEntrega == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar un Tipo de Entrega del Proveedor.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.MonedaCompra == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar una moneda.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.Monto == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar un monto.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.PlazoEntrega == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar un Plazo de Entrega Orden Compra.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.PQAdjudicado == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar un P X Q Adjunto de acuerdo a preciario.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.CotizacionAdjunta == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar una Cotización Adjunta de Proveedor.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.FechaEntregaOC == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar un Fecha de necesidad entrega de Orden de Compra Proveedor.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.PosicionOC == "-1") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe seleccionar un Posiciones en OC.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.DetallePrePeticionCompra.Observaciones == "") {
                UtilsFactory.Alerta('#lblAlerta_PrePeticionDetalle', 'warning', 'Debe ingresar Observaciones.', 5);
                blockUI.stop();
                return false;
            }
            

            return true;
        }

        function GuardarPrePeticionDCMDetalle() {
            if (!ValidarDatos()){
                return;
            }

            vm.DetallePrePeticionCompra.IdCabecera = $scope.$parent.vm.PrePeticionCompra.Id;
            vm.DetallePrePeticionCompra.IdContratoMarco = vm.ContratoMarco.IdContratoMarco;
            vm.DetallePrePeticionCompra.IdProveedor = vm.ContratoMarco.IdProveedor;
            
            ValidarMontoLimite();
        }
    }
})();