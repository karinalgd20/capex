(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('DetallePrePeticionCompraService', DetallePrePeticionCompraService);

    DetallePrePeticionCompraService.$inject = ['$http', 'URLS'];

    function DetallePrePeticionCompraService($http, $urls) {

        var service = {
            RegistrarPrePeticionDCMDetalle: RegistrarPrePeticionDCMDetalle,
            MostrarPrePeticionCompra: MostrarPrePeticionCompra,
            MostrarDetallePrePeticionCompra: MostrarDetallePrePeticionCompra,
            ObtenerPrePeticionDCMDetalle: ObtenerPrePeticionDCMDetalle
        };

        return service;

        function RegistrarPrePeticionDCMDetalle(request) {
            return $http({
                url: $urls.ApiCompra + "DetallePrePeticionCompra/RegistrarPrePeticionDCMDetalle", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerPrePeticionDCMDetalle(request) {
            return $http({
                url: $urls.ApiCompra + "DetallePrePeticionCompra/ObtenerPrePeticionDCMDetalle", 
                method: "POST",
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarPrePeticionCompra() {
            return $http({
                url: $urls.ApiCompra + "LineasCMI/MostrarPrePeticionCompra",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarDetallePrePeticionCompra() {
            return $http({
                url: $urls.ApiCompra + "PrePeticionCompra/MostrarDetallePrePeticionCompra",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarAnexos(iIdPeticionCompra) {
            return $http({
                url: $urls.ApiCompra + "ComentarioAnexo/ListarAnexos", 
                method: "POST",
                data: {IdPeticionCompra: iIdPeticionCompra}
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarAnexo(iIdPeticionCompra) {
            return $http({
                url: $urls.ApiCompra + "ComentarioAnexo/EliminarAnexo", 
                method: "POST",
                data: {IdPeticionCompra: iIdPeticionCompra}
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function DescargarAnexoArchivo(iIdAnexoArchivo) {
            window.location.href = $urls.ApiCompra + "ComentarioAnexo/DescargarAnexoArchivo?iIdAnexoArchivo=" + iIdAnexoArchivo;
           
        };

        function GuardarAnexos(Anexos, iIdPeticionCompra) {
            var datax = new FormData();
            var sTipoDocumento = [];

            Anexos.forEach(anexo => {
                sTipoDocumento.push(anexo.IdTipoDocumento);
                datax.append("file", anexo.Archivo);
            });
            datax.append("IdTipoDocumento", sTipoDocumento.join(","));
            datax.append("IdPeticionCompra", iIdPeticionCompra);

            var resultado =$.ajax({
                type: "POST",
                url: $urls.ApiCompra + "ComentarioAnexo/GuardarAnexo",  
                dataType: 'json',
                cache: false,
                processData: false,
                contentType: false, 
                data: datax
            }); 

            return resultado;

        };
    }
})();