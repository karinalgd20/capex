(function () {
    'use strict'
    angular
        .module('app.Compra')
        .controller('RegistroPeticionCompraController', RegistroPeticionCompraController);
    
    RegistroPeticionCompraController.$inject = ['RegistroPeticionCompraService', 'BuscadorPeticionCompraService',
        'MaestraService', 'PrePeticionCompraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$filter'];

    function RegistroPeticionCompraController(RegistroPeticionCompraService, BuscadorPeticionCompraService,
        MaestraService, PrePeticionCompraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $filter)
    {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiProyecto + "RegistrarProyecto/Index/";

        vm.MostrarInfoDatosGenerales = MostrarInfoDatosGenerales;
        vm.MostrarInfoClienteProveedor = MostrarInfoClienteProveedor;
        vm.MostrarInfoDetalleCompra = MostrarInfoDetalleCompra;
        vm.MostrarInfoAprobaciones = MostrarInfoAprobaciones;
        vm.MostrarInfoComentarioAnexo = MostrarInfoComentarioAnexo;
        vm.MostrarInfoSoporte = MostrarInfoSoporte;
        vm.MostrarInfoGestion = MostrarInfoGestion;
        vm.MostrarInfoDespacho = MostrarInfoDespacho;
        vm.ModalBuscarComprador = ModalBuscarComprador;
        vm.CerrarPopupComprador = CerrarPopupComprador;
        vm.ModalBuscarCentroCosto = ModalBuscarCentroCosto;
        vm.CerrarPopupCeCo = CerrarPopupCeCo;
        vm.ModalBuscarResponsable = ModalBuscarResponsable;
        vm.CerrarPopupResponsable = CerrarPopupResponsable;
        vm.CargarTipoCosto = CargarTipoCosto;
        vm.GrabarPeticionCompra = GrabarPeticionCompra;
        vm.AprobarPeticionCompra = AprobarPeticionCompra;
        vm.ModalObservacion = ModalObservacion;
        vm.CerrarPopupObservacion = CerrarPopupObservacion;
        vm.fAtenderPeticionCompra = fAtenderPeticionCompra;
        vm.ObservarPeticionCompra = ObservarPeticionCompra;
        vm.AnularPeticionCompra = AnularPeticionCompra;
        vm.SubsanarPeticionCompra = SubsanarPeticionCompra;
        vm.SeleccionarTipoPeticion = SeleccionarTipoPeticion;
        vm.ModalBuscarContratoMarco = ModalBuscarContratoMarco;
        vm.CerrarPopupContratoMarco = CerrarPopupContratoMarco;
        vm.ModalBuscarPrePeticion = ModalBuscarPrePeticion;
        vm.CerrarPopupPrePeticion = CerrarPopupPrePeticion;
        vm.BuscarPrePeticion = 0;

        vm.blockUI = blockUI;
        
        vm.PrePeticionProyecto = {};
        vm.SeleccionarPrePeticion = {
            IdProyecto: 0,
            IdLineaCMI: 0,
            DescripcionCompra: "",
            AsociadoProyecto: ""
        };

        vm.PeticionCompra = {
            IdPeticionCompra: 0,
            IdLineaProducto: -1,
            AdjuntaCotizacion: -1,
            PeticionPenalidad: -1,
            ArrendamientoRenting: -1,
            CodigoPeticionCompraExtendido: "",
            IdComprador: 0,
            MontoCotizacion: 0,
            SubGrupoCompras: "",
            Descripcion: "",
            ElementoPEP: "",
            Grafo: "",
            Cuenta: "",
            DescripcionCuenta: "",
            AreaFuncional: "",
            IdResponsablePostventa: 0,
            IdEtapa: 0,
            Etapa: "",
            TipoCompra: "",
            NumContrato: "",
            Cesta: "",
            FechaModifica: new Date().toLocaleDateString()
        }

        vm.IdAsociadoProyecto = -1;
        vm.IdTipoCompra = -1;
        vm.IdTipoCosto = -1;
        vm.IdAreaSolicitante = -1;
        vm.FechaSol = new Date();
        vm.IdTipoMoneda = 0;

        vm.PeticionCompraDCM = false;
        vm.PeticionCompraOrdinaria = false;
        vm.AsociadoProyecto = false;
        vm.AdjuntoCotizacion = false;
        vm.FlagAPOrdinariaInv = false;

        vm.AtenderPeticionCompra = false;
        vm.ObservarIdOrden = -1;
        vm.ObservarMotivo = -1;
        vm.ObservarComentario = "";
        vm.AsociadoAProyectosDisabled = false;
        vm.TiposDisabled = false;
        vm.IdClienteAP = 0;
        vm.IdProveedorAP = 0;
        vm.flagActaSubidaUsuario = false;

        vm.ContratoMarco = {
            IdContratoMarco: 0,
            NumContrato: "",
            Descripcion: ""
        }

        angular.element(document).ready(function () {
            blockUI.start();

            vm.Accion = Accion;
            vm.IdOrdenEtapa = IdOrdenEtapa;
            vm.EstadoEtapa = IdEstadoEtapa;
            vm.IdEtapaPeticionCompra = IdEtapaPeticionCompra;
            vm.AtenderPeticionCompra = (Accion == 4 || IdEstadoEtapa == 6 ? true : false);
            vm.Observado = Observado;
            vm.ObservacionComentario = ObservacionComentario;
            vm.flagActaAcept = flagActaAcept;
            vm.FlagEtapaCertificadaUsuario = (Etapa == "CERTIFICADA" && (CargoUsuario == "SOLICITANT" || CargoUsuario == "SDIOPY")) ? true : false;
            vm.flagActaSubidaUsuario = (flagActaAcept == null ? false : (Etapa == "CERTIFICADA" ? flagActaAcept : false));
            vm.flagUsuarioAtiende = (flagUsuarioAtiende == null ? false : (flagUsuarioAtiende == "False" ? false : true));
            vm.Etapa = Etapa;
            vm.IdUsuario = IdUsuario;
            vm.Ver = Ver;

            MostrarInfoDatosGenerales();
            CargarDatosUsuarioRegistra(vm.PeticionCompra.IdPeticionCompra);
            if (IdPeticionCompra !== "0") {
                CargarDatosPeticionCompra(IdPeticionCompra);
            }

            $timeout(function () {
                blockUI.stop();
            }, 500);

            CargarTipoCosto();
        });
        
        function blockStart(){
            blockUI.start();
        }

        function blockStop(){
            blockUI.stop();
        }

        function MostrarInfoDatosGenerales() {
            blockUI.start();

            var promise = RegistroPeticionCompraService.MostrarInfoDatosGenerales();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoDatosGenerales").html(content);
                });

                ListarAsociadoProyecto();
                ListarTipoCompra();
                ListarLineaProducto();
                ListarAreaSolicitante();
            }, function (response) {
                blockUI.stop();
            });
        };
        
        function MostrarInfoClienteProveedor() {
            blockUI.start();

            var promise = RegistroPeticionCompraService.MostrarInfoClienteProveedor();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoClienteProveedor").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        };

        function MostrarInfoDetalleCompra() {
            blockUI.start();

            var promise = RegistroPeticionCompraService.MostrarInfoDetalleCompra();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoDetalleCompra").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        };

        function MostrarInfoAprobaciones() {
            blockUI.start();

            var promise = RegistroPeticionCompraService.MostrarInfoAprobaciones();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoDetalleAprobaciones").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        };

        function MostrarInfoComentarioAnexo() {
            blockUI.start();

            var promise = RegistroPeticionCompraService.MostrarInfoComentarioAnexo();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoCometarioAnexo").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        };

        function MostrarInfoSoporte() {
            blockUI.start();

            var promise = RegistroPeticionCompraService.MostrarInfoSoporte();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoSoporte").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        };

        function MostrarInfoGestion() {
            blockUI.start();

            var promise = RegistroPeticionCompraService.MostrarInfoGestion();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoGestion").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        };

        function MostrarInfoDespacho() {
            blockUI.start();

            var promise = RegistroPeticionCompraService.MostrarInfoDespacho();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoInfoDespacho").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        };

        function ListarAsociadoProyecto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 518
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaAsociadoProyecto = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarTipoCompra() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 481
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaTipoCompra = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarTipoCosto(asociadoProyecto) {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 485
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                if (asociadoProyecto == "1") {
                    var lista = [];
                    var oCeCo = Respuesta.find(r => r.Codigo == 1);
                    lista.push(oCeCo);
                    vm.ListaTipoCosto = UtilsFactory.AgregarItemSelect(lista);
                } else {
                    vm.ListaTipoCosto = UtilsFactory.AgregarItemSelect(Respuesta);
                }

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarLineaProducto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 489
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaLineaProducto = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarAreaSolicitante() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 496
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaAreaSolicitante = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function CargarTipoCosto() {
            var idAsociadoProyecto = vm.IdAsociadoProyecto;
            SeleccionarTipoPeticion();
            ListarTipoCosto(idAsociadoProyecto);
            if (idAsociadoProyecto == "2") {
                vm.TiposDisabled = false;
                vm.AsociadoProyecto = true;
            } else {
                vm.AsociadoProyecto = false;
            }
        }

        function CargarDatosUsuarioRegistra(id) {

            var ReqPeticionCompra = {
                IdPeticionCompra: id
            };

            var promise = RegistroPeticionCompraService.CargarDatosUsuarioRegistra(ReqPeticionCompra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.Solicitante = Respuesta.NombreUsuarioActual;
                vm.CorreoSolicitante = Respuesta.EmailUsuarioActual;

            }, function (response) {
                blockUI.stop();
            });

        }

        function SeleccionarTipoPeticion(){
            vm.PeticionCompraDCM = false;
            vm.PeticionCompraOrdinaria = false;
            vm.PeticionCompraAP = false;
            vm.PeticionCompraAPOrdinariaOpex = false;
            vm.FlagAPOrdinariaInv = false;
            vm.PeticionCompraGeneral = false;

            if (vm.IdAsociadoProyecto == "1")
            {
                if (vm.IdTipoCompra == "2") {
                    vm.PeticionCompraDCM = true;
                }else if (vm.IdTipoCompra == "3") {
                    vm.PeticionCompraOrdinaria = true;
                } else {
                    vm.PeticionCompraGeneral = true;
                }
            }else if (vm.IdAsociadoProyecto == "2"){
                vm.PeticionCompraAP = true;

                if (vm.IdTipoCompra == "3"){
                    if (vm.IdTipoCosto == "2"){
                        vm.PeticionCompraAPOrdinariaOpex = true;
                    } else if (vm.IdTipoCosto == "3") {
                        vm.FlagAPOrdinariaInv = true;
                    }
                } else if (vm.IdTipoCompra == "2") {
                    if (vm.IdTipoCosto == "2" || vm.IdTipoCosto == "3") {
                        vm.PeticionCompraDCM = true;
                    }
                }
            }

        }

        function CargarDatosPeticionCompra(iIdPeticionCompra) {
            blockUI.start();
            var peticionCompra = {
                IdPeticionCompra: iIdPeticionCompra,
            };

            var promise = RegistroPeticionCompraService.ObtenerPeticionCompraPorId(peticionCompra);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                if (Respuesta != ""){
                    vm.PeticionCompra = Respuesta;
                    var fechaActual = new Date(parseInt(vm.PeticionCompra.FechaCreacion.substr(6)));

                    vm.PeticionCompra.FechaCreacion = $filter('date')(fechaActual, 'dd/MM/yyyy');
                    vm.PeticionCompra.FechaModifica = (vm.PeticionCompra.FechaModifica == null ? vm.PeticionCompra.FechaCreacion : $filter('date')(new Date(parseInt(vm.PeticionCompra.FechaModifica.substr(6))), 'dd/MM/yyyy'));
                    vm.FechaSol = fechaActual;
                    vm.PeticionCompra.ArrendamientoRenting = (vm.PeticionCompra.ArrendamientoRenting == true ? "0" : "1");
                    vm.PeticionCompra.PeticionPenalidad = (vm.PeticionCompra.PeticionPenalidad == true ? "0" : "1");
                    vm.PeticionCompra.AdjuntaCotizacion = (vm.PeticionCompra.AdjuntaCotizacion == true ? "0" : "1");
                    vm.IdAsociadoProyecto = (Respuesta.AsociadoAProyecto == true ? "2" : "1");
                    vm.PeticionCompra.IdLineaProducto = Respuesta.IdLineaNegocio;
                    vm.IdTipoCompra = Respuesta.IdTipoCompra;
                    CargarTipoCosto();
                    vm.IdTipoCosto = Respuesta.IdTipoCosto;
                    vm.BComprador = Respuesta.NombreComprador;
                    vm.BCorreoComprador = Respuesta.CorreoComprador;
                    vm.BUnidadSol = Respuesta.Unidad;
                    vm.BGerenciaSol = Respuesta.Gerencia;
                    vm.BDireccionSol = Respuesta.Direccion;
                    vm.IdAreaSolicitante = Respuesta.IdAreaSolicitante;
                    vm.Solicitante = Respuesta.Solicitante;
                    vm.CorreoSolicitante = Respuesta.CorreoSolicitante;
                    vm.BCeCo = Respuesta.CodigoCeCo;
                    vm.bResponsablePostventa = Respuesta.NombreResponsablePostventa;
                    vm.bCorreoResponsable = Respuesta.CorreoResponsablePostventa;
                    vm.PeticionCompra.Cesta = Respuesta.Cesta;
                    vm.BIdCentroCosto = Respuesta.IdCentroCosto;

                    vm.ContratoMarco.NumContrato = vm.PeticionCompra.NumContrato;
                    vm.ContratoMarco.IdContratoMarco = vm.PeticionCompra.IdContratoMarco;

                    vm.PrePeticionProyecto.IdProyectoLKM = vm.PeticionCompra.IdProyectoLKM;
                    vm.SeleccionarPrePeticion.Linea = vm.PeticionCompra.Linea;
                    vm.SeleccionarPrePeticion.SubLinea = vm.PeticionCompra.SubLinea;
                    vm.PrePeticionProyecto.Descripcion = vm.PeticionCompra.DescripcionProyecto; 
                    vm.SeleccionarPrePeticion.Servicio = vm.PeticionCompra.Servicio;
                    vm.PrePeticionProyecto.NombreJefeProyecto = vm.PeticionCompra.NombreJefeProyecto;

                    if (vm.IdAsociadoProyecto == 2) {
                        vm.AsociadoProyecto = true;
                    }

                    if (vm.IdOrdenEtapa > 1) {
                        vm.AsociadoAProyectosDisabled = true;
                        vm.TiposDisabled = true;
                    }

                    SeleccionarTipoPeticion();

                }else{
                    vm.FechaSol = new Date();
                }
               
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_Compras', 'danger', MensajesUI.DatosError, 5);
            });

            $timeout(function () {
                blockUI.stop();
            }, 500);
        }

        function ValidarDatos(){
            if (vm.PeticionCompra.Descripcion == 0){
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Debe ingresar una Descripcion de compra.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.IdAsociadoProyecto == -1) {
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Debe seleccionar si esta asociado a proyecto o no.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.IdTipoCompra == -1){
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Debe seleccionar un Tipo de compra.', 5);
                blockUI.stop();
                return false;
            }

            if (vm.IdTipoCosto == -1){
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Debe seleccionar un Tipo de costo.', 5);
                blockUI.stop();
                return false;
            }

            if (!vm.PeticionCompraDCM && vm.IdAsociadoProyecto != '2'){
                if ($.trim(vm.PeticionCompra.SubGrupoCompras) == "") {
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Ingrese un sub-grupo de compras.', 5);
                    blockUI.stop();
                    return;
                } else if (vm.PeticionCompra.IdComprador == 0) {
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Seleccione un comprador.', 5);
                    blockUI.stop();
                    return;
                }
            } else if (vm.FlagAPOrdinariaInv || vm.PeticionCompraAPOrdinariaOpex){
                if ($.trim(vm.PeticionCompra.SubGrupoCompras) == "") {
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Ingrese un sub-grupo de compras.', 5);
                    blockUI.stop();
                    return;
                } else if (vm.PeticionCompra.IdComprador == 0) {
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Seleccione un comprador.', 5);
                    blockUI.stop();
                    return;
                }
            } else {
                if (vm.ContratoMarco.IdContratoMarco == 0){
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Debe seleccionar un Contrato Marco.', 5);
                    blockUI.stop();
                    return;
                }
            }
            return true;
        }

        function GrabarPeticionCompra() {
            vm.blockUI.start();
            vm.blockUI.start();
            vm.blockUI.start();

            var Id = vm.PeticionCompra.IdPeticionCompra;

            if (!ValidarDatos()) {
                $timeout(function () {
                    vm.blockUI.stop();
                    vm.blockUI.stop();
                }, 500);
                return;
            }

            var peticionClienteDGDto = {
                IdPeticionCompra: Id,
                IdPrePeticionDetalle: vm.PeticionCompra.IdPrePeticionDetalle,
                Descripcion: vm.PeticionCompra.Descripcion,
                AsociadoAProyecto: (vm.IdAsociadoProyecto == "-1" ? null : (vm.IdAsociadoProyecto == "2" ? true : false)),
                IdTipoCompra: vm.IdTipoCompra,
                IdTipoCosto: vm.IdTipoCosto,
                IdLineaNegocio: vm.PeticionCompra.IdLineaProducto,
                IdComprador: vm.PeticionCompra.IdComprador,
                AdjuntaCotizacion: (vm.PeticionCompra.AdjuntaCotizacion == "-1" ? null : (vm.PeticionCompra.AdjuntaCotizacion == "0" ? true : false)),
                MontoCotizacion: vm.PeticionCompra.MontoCotizacion,
                SubGrupoCompras: vm.PeticionCompra.SubGrupoCompras,
                IdAreaSolicitante: vm.IdAreaSolicitante,
                IdCentroCosto: vm.BIdCentroCosto,
                IdResponsablePostventa: vm.PeticionCompra.IdResponsablePostventa,
                PeticionPenalidad: (vm.PeticionCompra.PeticionPenalidad == "-1" ? null : (vm.PeticionCompra.PeticionPenalidad == "0" ? true : false)),
                ElementoPEP: vm.PeticionCompra.ElementoPEP,
                Grafo: vm.PeticionCompra.Grafo,
                Cuenta: vm.PeticionCompra.Cuenta,
                DescripcionCuenta: vm.PeticionCompra.DescripcionCuenta,
                AreaFuncional: vm.PeticionCompra.AreaFuncional,
                ArrendamientoRenting: (vm.PeticionCompra.ArrendamientoRenting == "-1" ? null : (vm.PeticionCompra.ArrendamientoRenting == "0" ? true : false)),
                IdContratoMarco: (vm.ContratoMarco.IdContratoMarco != 0 ? vm.ContratoMarco.IdContratoMarco : null)
            };


            var promise = (Id > 0) ? RegistroPeticionCompraService.ActualizarPeticionCompraDatosGenerales(peticionClienteDGDto) : RegistroPeticionCompraService.RegistrarPeticionCompraDatosGenerales(peticionClienteDGDto);
            promise.then(function (response) {
                var Respuesta = response.data;
                if (Respuesta.TipoRespuesta != 0) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'danger', MensajesUI.DatosError, 20);
                } else {
                    //Resfrescar Datos en Pantalla.
                    var peticionCompra = {
                        IdPeticionCompra: Respuesta.Id,
                    };

                    CargarDatosPeticionCompra(peticionCompra.IdPeticionCompra);

                    UtilsFactory.Alerta('#lblAlerta_Compras', 'success', Respuesta.Mensaje, 10);

                }
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_Compras', 'danger', MensajesUI.DatosError, 5);
            });

            $timeout(function () {
                vm.blockUI.stop();
            }, 500);
        }

        $scope.CheckTipoMoneda = function (i) {
            vm.IdTipoMoneda = i;
        };

        /*********************** INICIO :: Modal de Buscar Comprador*************************/
        function ModalBuscarComprador() {

            var RegPeticionCompra = {
                IdPeticionCompra: 0
            };

            var promise = BuscadorPeticionCompraService.ModalPeticionCompraBuscarComprador(RegPeticionCompra);

            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCompraComprador").html(content);
                });

                $('#ModalCompraComprador').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CerrarPopupComprador() {
            $('#ModalCompraComprador').modal('hide');
        };

        /***********************FIN :: Modal de Buscar Comprador*************************/

        /***********************INICIO :: Modal de Buscar Centro Costo*************************/

        function ModalBuscarCentroCosto() {

            var RegPeticionCompra = {
                IdAreaSolicitante: vm.IdAreaSolicitante
            };

            if (vm.IdAreaSolicitante == -1) {
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe seleccionar el area solicitante.", 5);
                blockUI.stop();
                return;
            }

            var promise = BuscadorPeticionCompraService.ModalPeticionCompraBuscarCentroCosto(RegPeticionCompra);

            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCompraCentroCosto").html(content);
                });

                $('#ModalCompraCentroCosto').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CerrarPopupCeCo() {
            $('#ModalCompraCentroCosto').modal('hide');
        };

        /***********************FIN :: Modal de Buscar Centro Costo*************************/

        /***********************INICIO :: Modal de Buscar Responsable Post*************************/

        function ModalBuscarResponsable() {

            var RegPeticionCompra = {
                IdPeticionCompra: 0
            };

            var promise = BuscadorPeticionCompraService.ModalPeticionCompraBuscarResponsable(RegPeticionCompra);

            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCompraResponsablePost").html(content);
                });

                $('#ModalCompraResponsablePostVenta').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CerrarPopupResponsable() {
            $('#ModalCompraResponsablePostVenta').modal('hide');
        };

        /***********************FIN :: Modal de Buscar Responsable Post*************************/
        function ObtenerEtapaActual(EtapaPeticionCompra){
            var promise = RegistroPeticionCompraService.ObtenerEtapaActual(EtapaPeticionCompra.IdPeticionCompra);
            promise.then(function (response) {
                var respuesta = response.data;
                vm.Accion = respuesta.Accion;
                vm.IdOrdenEtapa = respuesta.Orden;
                vm.EstadoEtapa = respuesta.IdEstadoEtapa;
                vm.IdEtapaPeticionCompra = respuesta.IdEtapaPeticionCompra;
                vm.AtenderPeticionCompra = (respuesta.Accion == 4 ? true : false);
                vm.Observado = respuesta.Observado;

            }, function (response) {
                blockUI.stop();
            });
        }
        
        function RegistrarEtapaPeticionCompra(EtapaPeticionCompra, IrSiguienteEtapa, iIrBandeja){
            var promise = RegistroPeticionCompraService.RegistrarEtapaPeticionCompra(EtapaPeticionCompra);
            promise.then(function (response) {
                var respuesta = response.data;

                let sTipoMensaje = "";
                if (respuesta.TipoRespuesta == 0){
                    sTipoMensaje = 'success'
                }else{
                    sTipoMensaje = 'danger'
                }

                UtilsFactory.Alerta('#lblAlerta_Compras', sTipoMensaje, respuesta.Mensaje, 10);
                blockUI.stop();
                
                if (IrSiguienteEtapa == 1) {
                    SiguienteEtapaPeticionCompra();
                } else if (IrSiguienteEtapa == -1) {
                    AnteriorEtapaPeticionCompra();
                } else {
                    if (iIrBandeja == 1) {
                        IrBandeja();
                    }

                    if (iIrBandeja == 2) {
                        ObtenerEtapaActual(EtapaPeticionCompra);
                    }
                }
            }, function (response) {
                blockUI.stop();
            });
        }

        function AprobarPeticionCompra() {
            blockUI.start();

            if (vm.flagActaSubidaUsuario == 'False' && Etapa == "CERTIFICADA") {
                blockUI.stop();
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_Compras', 'danger', "No es posible la aprobar, debido a que no se ha adjuntado el acta de aceptacion.", 20);
                return;
            }

            let EtapaPeticionCompra = {
                IdEstadoEtapa: 4,
                IdPeticionCompra: vm.PeticionCompra.IdPeticionCompra,
                IdOrden: IdOrdenEtapa
            };
            
            if (vm.PeticionCompraDCM && Number(IdOrdenEtapa) == 6) {
                RegistrarEtapaPeticionCompra(EtapaPeticionCompra, 0, 1);
            } else if (vm.PeticionCompraDCM && Number(IdOrdenEtapa) == 5 && vm.IdTipoCosto != 1) {
                RegistrarEtapaPeticionCompra(EtapaPeticionCompra, 0, 1);
            } else if (vm.PeticionCompraOrdinaria && Number(IdOrdenEtapa) == 8) {
                RegistrarEtapaPeticionCompra(EtapaPeticionCompra, 0, 1);
            } else if (vm.AsociadoProyecto && vm.PeticionCompraAPOrdinariaOpex && Number(IdOrdenEtapa) == 7) {
                RegistrarEtapaPeticionCompra(EtapaPeticionCompra, 0, 1);
            } else if (vm.AsociadoProyecto && vm.FlagAPOrdinariaInv && Number(IdOrdenEtapa) == 7) {
                RegistrarEtapaPeticionCompra(EtapaPeticionCompra, 0, 1);
            } else {
                RegistrarEtapaPeticionCompra(EtapaPeticionCompra, 1, 1);
            }

        }

        function AnularPeticionCompra() {
            blockUI.start();

            let EtapaPeticionCompra = {
                IdEstadoEtapa: 5,
                IdPeticionCompra: vm.PeticionCompra.IdPeticionCompra,
                IdOrden: IdOrdenEtapa
            }
            
            RegistrarEtapaPeticionCompra(EtapaPeticionCompra, 0, 1);
        }

        function SubsanarPeticionCompra() {
            blockUI.start();

            let EtapaPeticionCompra = {
                IdEstadoEtapa: 3,
                IdPeticionCompra: vm.PeticionCompra.IdPeticionCompra,
                IdOrden: IdOrdenEtapa
            }
            if (vm.AsociadoProyecto && Number(IdOrdenEtapa) == 3) {
                //Volver a una Etapa Anterior
                RegistrarEtapaPeticionCompra(EtapaPeticionCompra, -1, 1);
            } else {
                RegistrarEtapaPeticionCompra(EtapaPeticionCompra, 0, 2);
            }

        }

        function SiguienteEtapaPeticionCompra() {
            var iOrden = 0;
            if (vm.AsociadoProyecto && Number(IdOrdenEtapa) == 2 && vm.IdTipoCosto == 2 && vm.IdTipoCompra == 2) {
                iOrden = Number(IdOrdenEtapa) + 2;
            } else {
                iOrden = Number(IdOrdenEtapa) + 1;
            }

            let EtapaPeticionCompra = {
                IdEstadoEtapa: 1,
                IdPeticionCompra: vm.PeticionCompra.IdPeticionCompra,
                IdOrden: iOrden,
                IrSiguienteEtapa: 1
            }
            
            RegistrarEtapaPeticionCompra(EtapaPeticionCompra, 2, 1);
        }

        function AnteriorEtapaPeticionCompra() {
            let EtapaPeticionCompra = {
                IdEstadoEtapa: 1,
                IdPeticionCompra: vm.PeticionCompra.IdPeticionCompra,
                IdOrden: Number(IdOrdenEtapa) - 1,
                IrSiguienteEtapa: 0
            }

            RegistrarEtapaPeticionCompra(EtapaPeticionCompra, 2, 1);
        }


        function ObservarPeticionCompra() {
            if (vm.ObservarMotivo == -1){
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_Observacion', 'danger', "Debe seleccionar un motivo", 20);
                return;
            }

            let EtapaPeticionCompra = {
                IdEstadoEtapa: 2,
                IdPeticionCompra: vm.PeticionCompra.IdPeticionCompra,
                IdOrden: vm.ObservarIdOrden,
                IdOrdenObservar: vm.ObservarIdOrden,
                Comentario: vm.ObservarComentario
            }
            blockUI.start();
            var promise = RegistroPeticionCompraService.RegistrarEtapaPeticionCompra(EtapaPeticionCompra);
            promise.then(function (response) {
                var respuesta = response.data;

                let sTipoMensaje = "";
                if (respuesta.TipoRespuesta == 0){
                    sTipoMensaje = 'success'
                }else{
                    sTipoMensaje = 'danger'
                }

                UtilsFactory.Alerta('#lblAlerta_Compras', sTipoMensaje, respuesta.Mensaje, 10);

                IrBandeja();

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        function fAtenderPeticionCompra(){
            vm.AtenderPeticionCompra = !vm.AtenderPeticionCompra;

            blockUI.start();

            let EtapaPeticionCompra = {
                IdEstadoEtapa: 6,
                IdPeticionCompra: vm.PeticionCompra.IdPeticionCompra,
                IdOrden: IdOrdenEtapa
            }
            
            RegistrarEtapaPeticionCompra(EtapaPeticionCompra, 0, 0);
        }
        
        function ListarMotivos() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 547
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaMotivos = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarEtapas() {
            let ConfiguracionEtapa = {
                IdTipoCompra: vm.PeticionCompra.IdTipoCompra,
                IdTipoCosto: vm.PeticionCompra.IdTipoCosto,
                Orden: Number(IdOrdenEtapa)
            }
            var promise = RegistroPeticionCompraService.ListarEtapas(ConfiguracionEtapa);
            promise.then(function (response) {
                var Respuesta = response.data;
                vm.ListaEtapas = [];
                vm.ListaEtapas.push({Descripcion: "--Seleccione--", Orden: -1});

                Respuesta.forEach(item => {
                    vm.ListaEtapas.push(item);
                });

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        }

        /*********************** INICIO :: Modal de Observacion*************************/
        function ModalObservacion() {
            var RegPeticionCompra = {
                IdPeticionCompra: 0
            };

            ListarEtapas();
            ListarMotivos();

            $('#ModalCompraObservacion').modal({
                keyboard: false,
                backdrop: 'static'
            });
        };

        function CerrarPopupObservacion() {
            $('#ModalCompraObservacion').modal('hide');
        };

        /***********************FIN :: Modal de Buscar Observacion*************************/

        /***********************INICIO :: Modal de Buscar Contrato Marco*************************/

        function ModalBuscarContratoMarco() {

            var RegPeticionCompra = {
                IdPeticionCompra: 0
            };

            var promise = BuscadorPeticionCompraService.ModalPeticionCompraBuscarContratoMarco(RegPeticionCompra);

            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCompraContratoMarco").html(content);
                });

                $('#ModalCompraContratoMarco').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CerrarPopupContratoMarco() {
            $('#ModalCompraContratoMarco').modal('hide');
        };
        /***********************FIN :: Modal de Buscar Contrato Marco*************************/

        function ModalBuscarPrePeticion() {
            var RegPeticionCompra = {
                IdPeticionCompra: 0
            };

            var promise = PrePeticionCompraService.ModalBuscarPrePeticion();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoPrePeticion").html(content);
                });

                $('#ModalPrePeticion').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CerrarPopupPrePeticion() {
            $('#ModalPrePeticion').modal('hide');
        };

        function IrBandeja(){
            window.location = $urls.ApiCompra +"/bandejapeticioncompra";
        }
    }
})();