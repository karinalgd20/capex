(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('RegistroPeticionCompraService', RegistroPeticionCompraService);

    RegistroPeticionCompraService.$inject = ['$http', 'URLS'];

    function RegistroPeticionCompraService($http, $urls) {

        var service = {
            MostrarInfoDatosGenerales: MostrarInfoDatosGenerales,
            MostrarInfoClienteProveedor: MostrarInfoClienteProveedor,
            MostrarInfoDetalleCompra: MostrarInfoDetalleCompra,
            MostrarInfoAprobaciones: MostrarInfoAprobaciones,
            MostrarInfoComentarioAnexo: MostrarInfoComentarioAnexo,
            MostrarInfoSoporte: MostrarInfoSoporte,
            MostrarInfoGestion: MostrarInfoGestion,
            MostrarInfoDespacho: MostrarInfoDespacho,
            CargarDatosUsuarioRegistra: CargarDatosUsuarioRegistra,
            RegistrarPeticionCompraDatosGenerales: RegistrarPeticionCompraDatosGenerales,
            ActualizarPeticionCompraDatosGenerales: ActualizarPeticionCompraDatosGenerales,
            ObtenerPeticionCompraPorId: ObtenerPeticionCompraPorId,
            RegistrarEtapaPeticionCompra: RegistrarEtapaPeticionCompra,
            ListarEtapas: ListarEtapas,
            ObtenerEtapaActual: ObtenerEtapaActual
        };

        return service;

        function MostrarInfoDatosGenerales() {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerInformacionDatosGenerales", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoClienteProveedor() {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerInformacionClienteProveedor",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoDetalleCompra() {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerInformacionDetalleCompra", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoAprobaciones() {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerInformacionAprobaciones",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoComentarioAnexo() {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerInformacionComentarioAnexo", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoSoporte() {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerInformacionSoporte",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoGestion() {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerInformacionGestion",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoDespacho() {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerInformacionDespacho", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function CargarDatosUsuarioRegistra(dto) {
            return $http({
                url: $urls.ApiCompra + "DatosGenerales/CargarDatosUsuarioRegistra",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarPeticionCompraDatosGenerales(dto) {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/RegistrarPeticionCompra",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarPeticionCompraDatosGenerales(dto) {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ActualizarPeticionCompra",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerPeticionCompraPorId(dto) {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerPeticionCompraPorId",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarEtapaPeticionCompra(dto) {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/RegistrarEtapaPeticionCompra",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarEtapas(dto) {
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ListarEtapas",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerEtapaActual(IdPeticionCompra) {
            let data = {iIdPeticionCompra: IdPeticionCompra}
            return $http({
                url: $urls.ApiCompra + "PeticionCompra/ObtenerEtapaActual",
                method: "POST",
                data: JSON.stringify(data)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();