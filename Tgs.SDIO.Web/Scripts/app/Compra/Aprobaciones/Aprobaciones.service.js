﻿(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('AprobacionesService', AprobacionesService);

    AprobacionesService.$inject = ['$http', 'URLS'];

    function AprobacionesService($http, $urls) {

        var service = {
            ListarAprobaciones: ListarAprobaciones
        };

        return service;

        function ListarAprobaciones(dto) {
            return $http({
                url: $urls.ApiCompra + "Aprobaciones/ListarAprobaciones",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();