﻿(function () {
    'use strict'
    angular
        .module('app.Compra')
        .controller('AprobacionesController', AprobacionesController);

    AprobacionesController.$inject = ['AprobacionesService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$filter'];

    function AprobacionesController(AprobacionesService,
    blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $filter) {

        var vm = this;

        CargarDetalle();

        vm.dtAprobacionesInstance = {};
        vm.dtAprobacionesColumns = [
            DTColumnBuilder.newColumn("Area").withTitle('Cargo').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn("Responsable").withTitle('Responsable').notSortable().withOption('width', '40%'),
            DTColumnBuilder.newColumn("FechaAprobacion").withTitle('Fecha Aprobacion').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn("Estado").withTitle('Estado').notSortable().withOption('width', '20%'),
         ];

        function LimpiarGrilla() {
            vm.dtAprobacionesOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('paging', false);
        }

        function ListarAprobaciones(sSource, aoData, fnCallback, oSettings) {

            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var request = {
                IdPeticionCompra: $scope.$parent.vm.PeticionCompra.IdPeticionCompra
            };

            var promise = AprobacionesService.ListarAprobaciones(request);
            promise.then(function (resultado) {

                var lista = resultado.data;
                lista.forEach(detalle => {
                    detalle.FechaAprobacion = (detalle.FechaAprobacion == null ? null: $filter('date')(new Date(parseInt(detalle.FechaAprobacion.substr(6))),'dd/MM/yyyy'));
                });

                var result = {
                    'draw': draw,
                    'recordsTotal': (lista.length > 0 ? lista.length : 0),
                    'recordsFiltered': (lista.length > 0 ? lista.length : 0),
                    'data': lista
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function CargarDetalle() {
            LimpiarGrilla();

            $timeout(function () {
                vm.dtAprobacionesOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(ListarAprobaciones)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withLanguage({ "sEmptyTable": "No hay datos disponibles en la tabla" })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }

    }
})();