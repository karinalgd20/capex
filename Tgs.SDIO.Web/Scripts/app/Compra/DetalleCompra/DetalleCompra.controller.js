(function () {
    'use strict'
    angular
        .module('app.Compra')
        .controller('DetalleCompraController', DetalleCompraController);

    DetalleCompraController.$inject = ['DetalleCompraService', 'BuscadorPeticionCompraService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function DetalleCompraController(DetalleCompraService, BuscadorPeticionCompraService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector)
    {
        var vm = this;

        vm.ModalBuscarCuenta = ModalBuscarCuenta;
        vm.AgregarDetalle = AgregarDetalle;
        vm.CerrarPopupCuenta = CerrarPopupCuenta;
        vm.GrabarDetalleCompra = GrabarDetalleCompra;
        vm.CalcularTotal = CalcularTotal;
        vm.EliminarDetalleCompra = EliminarDetalleCompra;
        vm.DatoModificado = DatoModificado;
        vm.GestionAsociadaPro = $scope.$parent.vm.AsociadoProyecto;
        vm.IdTipoCosto = $scope.$parent.vm.IdTipoCosto;
        vm.IdTipoCompra = $scope.$parent.vm.IdTipoCompra;
        vm.Ver = $scope.$parent.vm.Ver;

        vm.listaDetalleCompra = [];
        vm.IdDetalleCompra = 0;
        vm.IdDetalleCompraActual = 0;
        vm.AccionEstrategica = {};
        vm.PEP = {};
        vm.Actividad = {};
        vm.Cantidad = {};
        vm.Descripcion = {};
        vm.CostoUnitario = {};
        vm.IdTipoMoneda = 0;
        vm.CargaDetalleBase = 1;
        vm.tituloCuenta = (vm.GestionAsociadaPro && vm.IdTipoCosto == 3 && (vm.IdTipoCompra == 2 || vm.IdTipoCompra == 3)) ? 'Concepto Capex' : 'Cuenta';

        vm.dtInstanceDetCompra = {};
        vm.dtColumnsDetCompra = [
            DTColumnBuilder.newColumn('IdPeticionCompra').notVisible(),
            DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(DrawBuscador).withOption('width', '5%'),
            DTColumnBuilder.newColumn('Cuenta').withTitle(vm.tituloCuenta).notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Actividad').notSortable().renderWith(DrawActividad).withOption('width', '20%'),
            DTColumnBuilder.newColumn('CuentaAnterior').withTitle('Cuenta Anterior').notSortable().withOption('width', '5%'),
            DTColumnBuilder.newColumn(null).withTitle('Acción Estratégica').notSortable().renderWith(DrawAccionEstrategica).withOption('width', '20%'),
            DTColumnBuilder.newColumn(null).withTitle('PEP 2').notSortable().renderWith(DrawPep).withOption('width', '20%'),

            DTColumnBuilder.newColumn(null).withTitle('Cantidad').notSortable().renderWith(DrawCantidad).withOption('width', '5%'),
            DTColumnBuilder.newColumn(null).withTitle('Descripción').notSortable().renderWith(DrawDescripcion).withOption('width', '45%'),
            DTColumnBuilder.newColumn(null).withTitle('Costo x Unidad').notSortable().renderWith(DrawCostoPorUnidad).withOption('width', '5%'),
            DTColumnBuilder.newColumn(null).withTitle('Total (Sin IGV)').notSortable().renderWith(DrawTotal).withOption('width', '5%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesDetalle).withOption('width', '5%')
        ];

        CargarDetalle();

        function AccionesDetalle(data, type, full, meta) {
            var respuesta = "<div class='col-xs-12 col-sm-12' ><a title='Eliminar' class='btn btn-sm' id='tab-button' ng-click='vm.EliminarDetalleCompra(" + data.IdDetalleCompra + ")' >" + "<span class='fa fa-trash-o'></span></a></div>";
            
            if (vm.Ver == 1){
                respuesta = "<div class='col-xs-12 col-sm-12' ></div>";
            }

            return respuesta;
        };

        function DrawBuscador(data, type, full, meta) {
            return "<div class='col-xs-1 col-sm-1'><a title='Buscar cuenta' class='btn btn-primary' ng-hide='vm.Ver == 1' ng-click='vm.ModalBuscarCuenta(" + data.IdDetalleCompra + ")' ><span class='fa fa-search'></span></a></div>";
        };

        function DrawActividad(data, type, full, meta) {
            return "<div class='col-xs-12 col-sm-12'> <input type='text' class='form-control' ng-disabled='vm.Ver == 1' style='width:100px' id='txtActividad' ng-model='vm.Actividad[" + data.IdDetalleCompra + "]' ng-change='vm.DatoModificado(" + data.IdDetalleCompra + ")' /> </div> ";
        };

        function DrawCantidad(data, type, full, meta) {
            return "<div class='col-xs-12 col-sm-12'> <input type='number' min='0' ng-disabled='vm.Ver == 1' class='form-control' id='txtCantidad' ng-model='vm.Cantidad[" + data.IdDetalleCompra + "]' ng-change='vm.CalcularTotal(" + data.IdDetalleCompra + ")' /> </div> ";
        };

        function DrawDescripcion(data, type, full, meta) {
            return "<div class='col-xs-12 col-sm-12'> <input type='text' class='form-control' ng-disabled='vm.Ver == 1' style='width:255px' id='txtDesc' ng-model='vm.Descripcion[" + data.IdDetalleCompra + "]' ng-change='vm.DatoModificado(" + data.IdDetalleCompra + ")' /> </div> ";
        };

        function DrawCostoPorUnidad(data, type, full, meta) {
            return "<div class='col-xs-12 col-sm-12'> <input type='number' min='0' class='form-control' ng-disabled='vm.Ver == 1' id='txtCostoPorUnidad_" + data.IdDetalleCompra + "' ng-model='vm.CostoUnitario[" + data.IdDetalleCompra + "]' ng-change='vm.CalcularTotal(" + data.IdDetalleCompra + ")' /> <label for='txtCostoPorUnidad_" + data.IdDetalleCompra + "'>{{vm.CostoUnitario[" + data.IdDetalleCompra + "] | number}}</label></div> ";
        };

        function DrawTotal(data, type, full, meta) {
            return "<div class='col-xs-12 col-sm-12'> <label id='lblTotal_" + data.IdDetalleCompra + "' class='col-sm-3 col-form-label' style='width:50%'/>" + data.TotalFormated + "</div> ";
        };

        function DrawAccionEstrategica(data, type, full, meta) {
            return "<div class='col-xs-12 col-sm-12'> <input type='text' class='form-control' ng-disabled='vm.Ver == 1' style='width:100px' id='txtAccionEstrategica' ng-model='vm.AccionEstrategica[" + data.IdDetalleCompra + "]' ng-change='vm.DatoModificado(" + data.IdDetalleCompra + ")' /> </div> ";
        };

        function DrawPep(data, type, full, meta) {
            return "<div class='col-xs-12 col-sm-12'> <input type='text' class='form-control' ng-disabled='vm.Ver == 1' style='width:100px' id='txtPep' ng-model='vm.PEP[" + data.IdDetalleCompra + "]' ng-change='vm.DatoModificado(" + data.IdDetalleCompra + ")' /> </div> ";
        };

        function CargarDetalle() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsDetCompra = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('lengthChange', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarDetalleCompra)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('scrollCollapse', true)
                    .withOption('paging', false)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
            }, 500);

        };

        function MostrarColumns() {

            if (vm.GestionAsociadaPro && vm.IdTipoCosto == 3 && (vm.IdTipoCompra == 2 || vm.IdTipoCompra == 3)) {
                vm.dtDetCompraColumnDefs = [
                    DTColumnDefBuilder.newColumnDef(0),
                    DTColumnDefBuilder.newColumnDef(1),
                    DTColumnDefBuilder.newColumnDef(2),
                    DTColumnDefBuilder.newColumnDef(3).notVisible(),
                    DTColumnDefBuilder.newColumnDef(4).notVisible(),
                    DTColumnDefBuilder.newColumnDef(5),
                    DTColumnDefBuilder.newColumnDef(6),
                    DTColumnDefBuilder.newColumnDef(7),
                    DTColumnDefBuilder.newColumnDef(8),
                    DTColumnDefBuilder.newColumnDef(9),
                    DTColumnDefBuilder.newColumnDef(10),
                    DTColumnDefBuilder.newColumnDef(11)
                ];
            } else {
                vm.dtDetCompraColumnDefs = [
                    DTColumnDefBuilder.newColumnDef(0),
                    DTColumnDefBuilder.newColumnDef(1),
                    DTColumnDefBuilder.newColumnDef(2),
                    DTColumnDefBuilder.newColumnDef(3),
                    DTColumnDefBuilder.newColumnDef(4),
                    DTColumnDefBuilder.newColumnDef(5).notVisible(),
                    DTColumnDefBuilder.newColumnDef(6).notVisible(),
                    DTColumnDefBuilder.newColumnDef(7),
                    DTColumnDefBuilder.newColumnDef(8),
                    DTColumnDefBuilder.newColumnDef(9),
                    DTColumnDefBuilder.newColumnDef(10),
                    DTColumnDefBuilder.newColumnDef(11)
                ];
            }
        };

        function BuscarDetalleCompra(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var dto = {
                IdPeticionCompra: $scope.$parent.vm.PeticionCompra.IdPeticionCompra,
            };

            if (vm.CargaDetalleBase == 1) { //Carga desde BD
                var promise = DetalleCompraService.ListarDetalleCompra(dto);
                promise.then(function (response) {
                    vm.listaDetalleCompra = response.data;

                    var records = {
                        'draw': draw,
                        'recordsTotal': (vm.listaDetalleCompra.length > 0 ? vm.listaDetalleCompra.length : 0),
                        'recordsFiltered': (vm.listaDetalleCompra.length > 0 ? vm.listaDetalleCompra.length : 0),
                        'data': vm.listaDetalleCompra
                    };

                    if (dto.IdPeticionCompra > 0 && vm.listaDetalleCompra.length > 0) {
                        if (response.data.length > 0) {
                            vm.IdTipoMoneda = vm.listaDetalleCompra[0].IdTipoMoneda;
                            vm.Sustento = vm.listaDetalleCompra[0].SustentoCualitativo;
                            vm.Solpe = vm.listaDetalleCompra[0].DetalleSolpe;
                        }

                        vm.listaDetalleCompra.forEach(detalle => {
                            vm.Actividad[detalle.IdDetalleCompra] = detalle.Actividad,
                            vm.Descripcion[detalle.IdDetalleCompra] = detalle.Descripcion,
                            vm.Cantidad[detalle.IdDetalleCompra] = detalle.Cantidad,
                            vm.CostoUnitario[detalle.IdDetalleCompra] = detalle.CostoPorUnidad,
                            vm.AccionEstrategica[detalle.IdDetalleCompra] = detalle.AccionEstrategica,
                            vm.PEP[detalle.IdDetalleCompra] = detalle.Pep2,
                            detalle.TotalFormated = detalle.Total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                        });
                    }
                    var sumaTotal = CalcularTotalCosto();
                    vm.TotalCosto = sumaTotal;
                    var sumaTotalConverted = sumaTotal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    vm.TotalCosto2 = sumaTotalConverted;
                    MostrarColumns();
                    blockUI.stop();
                    fnCallback(records);
                }, function (response) {
                    blockUI.stop();
                    LimpiarGrilla();
                });
            } else { //Carga en Memoria
                var records = {
                    'draw': draw,
                    'recordsTotal': (vm.listaDetalleCompra.length > 0 ? vm.listaDetalleCompra.length : 0),
                    'recordsFiltered': (vm.listaDetalleCompra.length > 0 ? vm.listaDetalleCompra.length : 0),
                    'data': vm.listaDetalleCompra
                };

                vm.listaDetalleCompra.forEach(detalle => {
                    vm.Actividad[detalle.IdDetalleCompra] = detalle.Actividad,
                    vm.Descripcion[detalle.IdDetalleCompra] = detalle.Descripcion,
                    vm.Cantidad[detalle.IdDetalleCompra] = detalle.Cantidad,
                    vm.CostoUnitario[detalle.IdDetalleCompra] = detalle.CostoPorUnidad,
                    vm.AccionEstrategica[detalle.IdDetalleCompra] = detalle.AccionEstrategica,
                    vm.PEP[detalle.IdDetalleCompra] = detalle.Pep2,
                    detalle.TotalFormated = detalle.Total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                });

                var sumaTotal = CalcularTotalCosto();
                vm.TotalCosto = sumaTotal;
                var sumaTotalConverted = sumaTotal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                vm.TotalCosto2 = sumaTotalConverted;
                blockUI.stop();
                fnCallback(records);
            }
        };

        function LimpiarGrilla() {
            vm.dtOptionsDetCompra = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', true)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };
    
        function AgregarDetalle() {
            blockUI.start();
            if (vm.IdTipoMoneda == 0) {
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', 'Seleccione tipo de moneda.', 5);
                return;
            }

            if (vm.listaDetalleCompra.length > 0) {
                vm.listaDetalleCompra.forEach(detalle => {
                    detalle.Actividad = vm.Actividad[detalle.IdDetalleCompra],
                    detalle.Descripcion = vm.Descripcion[detalle.IdDetalleCompra],
                    detalle.Cantidad = vm.Cantidad[detalle.IdDetalleCompra],
                    detalle.CostoPorUnidad = vm.CostoUnitario[detalle.IdDetalleCompra] ,
                    detalle.AccionEstrategica = vm.AccionEstrategica[detalle.IdDetalleCompra],
                    detalle.Pep2 = vm.PEP[detalle.IdDetalleCompra]
                });
            }

            var IdDet = (vm.listaDetalleCompra.length > 0 ? Math.max.apply(Math, vm.listaDetalleCompra.map(function (item) { return item.IdDetalleCompra; })) : vm.IdDetalleCompra);

            vm.IdDetalleCompra = (IdDet + 1);
            vm.DetalleCompra = {
                IdDetalleCompra: vm.IdDetalleCompra,
                IdPeticionCompra: 0,
                IdCuenta: 0,
                Cuenta: "",
                Actividad: "",
                IdCuentaAnterior: 0,
                CuentaAnterior: "",
                Descripcion: "",
                Cantidad: 0,
                CostoPorUnidad: 0,
                Total: 0,
                IdTipoMoneda: 0,
                SustentoCualitativo: "",
                DetalleSolpe: "",
                TotalCosto: 0,
                EsNuevo: true
            };
            vm.CostoUnitario[vm.IdDetalleCompra] = 0;
            vm.listaDetalleCompra.push(vm.DetalleCompra);
            vm.CargaDetalleBase = 0;
            CargarDetalle();
        };

        function CalcularTotal(id) {
            let oDetalleCompra = vm.listaDetalleCompra.find(dp => dp.IdDetalleCompra == id);
            var total = 0, cant = 0, costoUnitario = 0;
            cant = vm.Cantidad[id] == undefined ? 0 : vm.Cantidad[id];
            costoUnitario = vm.CostoUnitario[id] == undefined ? 0 : vm.CostoUnitario[id];
            total = cant * costoUnitario;
            oDetalleCompra.Total = total;

            var converted = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $("#lblTotal_" + id).text(converted);
            oDetalleCompra.TotalFormated = converted;

            var sumaTotal = CalcularTotalCosto();
            vm.TotalCosto = sumaTotal;

            var sumaTotalConverted = sumaTotal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

            vm.TotalCosto2 = sumaTotalConverted;
            oDetalleCompra.EsModificado = true;
        };

        function CalcularTotalCosto() {
            var total = 0;
            for (var i = 0; i < vm.listaDetalleCompra.length; i++) {
                var detalle = vm.listaDetalleCompra[i];
                total += detalle.Total;
            }
            return total;
        }

        function EliminarDetalleCompra(id) {
            blockUI.start();
            let oDetalleCompra = vm.listaDetalleCompra.find(d => d.IdDetalleCompra === id);
            if (oDetalleCompra.EsNuevo) {
                vm.CargaDetalleBase = 0;
                let idx = vm.listaDetalleCompra.findIndex(d => d.IdDetalleCompra === id);
                vm.listaDetalleCompra.splice(idx, 1);
            } else {
                var promise = DetalleCompraService.EliminarDetalleCompra(oDetalleCompra).then(function (response) {
                    var Respuesta = response.data;
                    if (Respuesta) {
                        let idx = vm.listaDetalleCompra.findIndex(d => d.IdDetalleCompra == id);
                        vm.listaDetalleCompra.splice(idx, 1);
                        vm.CargaDetalleBase = 1;
                        UtilsFactory.Alerta('#lblAlerta_Compras', 'success', "Se elimino el detalle seleccionado satisfactoriamente.", 5);
                    }
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                });
            }
            CargarDetalle();
        };

        function GrabarDetalleCompra() {

            $scope.$parent.vm.blockUI.start();
            $scope.$parent.vm.blockUI.start();
            if (vm.listaDetalleCompra.length == 0) {
                UtilsFactory.Alerta('#lblAlerta_Compras', 'warning', "Debe ingresar al menos un detalle.", 5);
                $timeout(function () {
                    vm.blockUI.stop();
                    vm.blockUI.stop();
                }, 500);
                return;
            }

            vm.listaDetalleCompra.forEach(detalle => {
                detalle.IdTipoMoneda = vm.IdTipoMoneda,
                detalle.TotalCosto = vm.TotalCosto,
                detalle.SustentoCualitativo = vm.Sustento,
                detalle.DetalleSolpe = vm.Solpe,
                detalle.IdPeticionCompra = $scope.$parent.vm.PeticionCompra.IdPeticionCompra,
                detalle.Actividad = vm.Actividad[detalle.IdDetalleCompra],
                detalle.Descripcion = vm.Descripcion[detalle.IdDetalleCompra],
                detalle.Cantidad = vm.Cantidad[detalle.IdDetalleCompra],
                detalle.CostoPorUnidad = vm.CostoUnitario[detalle.IdDetalleCompra],
                detalle.AccionEstrategica = vm.AccionEstrategica[detalle.IdDetalleCompra],
                detalle.Pep2 = vm.PEP[detalle.IdDetalleCompra]
            });

            var promise = DetalleCompraService.RegistrarDetalleCompra(vm.listaDetalleCompra).then(function (response) {
                var Respuesta = response.data;
                if (Respuesta.TipoRespuesta == 0) {
                    vm.CargaDetalleBase = 1;
                    CargarDetalle();
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'success', Respuesta.Mensaje, 5);
                } else {
                    UtilsFactory.Alerta('#lblAlerta_Compras', 'danger', Respuesta.Mensaje, 20);
                }

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });

            $timeout(function () {
                blockUI.stop();
            }, 500);
        };

        function DatoModificado(id) {
            let oDetalleCompra = vm.listaDetalleCompra.find(d => d.IdDetalleCompra === id);
            oDetalleCompra.EsModificado = true;
        };

        /***********************INICIO :: Modal de Buscar Cuenta*************************/

        function ModalBuscarCuenta(id) {

            vm.IdDetalleCompraActual = id;
            var cuentaDto = {
                IdCuenta: 0
            };

            var promise = BuscadorPeticionCompraService.ModalDetalleCompraBuscarCuenta(cuentaDto);

            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCompraCuenta").html(content);
                });

                $('#ModalCompraCuenta').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CerrarPopupCuenta() {
            $('#ModalCompraCuenta').modal('hide');
            if (vm.listaDetalleCompra.length > 0) {
                vm.listaDetalleCompra.forEach(detalle => {
                    detalle.Actividad = vm.Actividad[detalle.IdDetalleCompra],
                    detalle.Descripcion = vm.Descripcion[detalle.IdDetalleCompra],
                    detalle.Cantidad = vm.Cantidad[detalle.IdDetalleCompra],
                    detalle.CostoPorUnidad = vm.CostoUnitario[detalle.IdDetalleCompra],
                    detalle.AccionEstrategica = vm.AccionEstrategica[detalle.IdDetalleCompra],
                    detalle.Pep2 = vm.PEP[detalle.IdDetalleCompra]
                });
            }
            vm.CargaDetalleBase = 0;
            CargarDetalle();
        };

        /***********************FIN :: Modal de Buscar Cuenta*************************/
    }
})();