(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('DetalleCompraService', DetalleCompraService);

    DetalleCompraService.$inject = ['$http', 'URLS'];

    function DetalleCompraService($http, $urls) {

        var service = {
            MostrarInfoDatosGenerales: MostrarInfoDatosGenerales,
            MostrarInfoClienteProveedor: MostrarInfoClienteProveedor,
            MostrarInfoDetalleCompra: MostrarInfoDetalleCompra,
            ListarDetalleCompra: ListarDetalleCompra,
            EliminarDetalleCompra: EliminarDetalleCompra,
            RegistrarDetalleCompra: RegistrarDetalleCompra,
        };

        return service;

        function MostrarInfoDatosGenerales() {
            return $http({
                url: $urls.ApiCompra + "RegistrarPeticionCompra/ObtenerInformacionDatosGenerales", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoClienteProveedor() {
            return $http({
                url: $urls.ApiCompra + "RegistrarPeticionCompra/ObtenerInformacionClienteProveedor",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function MostrarInfoDetalleCompra() {
            return $http({
                url: $urls.ApiCompra + "RegistrarPeticionCompra/ObtenerInformacionDetalleCompra",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarDetalleCompra(dto) {
            return $http({
                url: $urls.ApiCompra + "DetalleCompra/ListarDetalleCompra",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
        function RegistrarDetalleCompra(dto) {
            return $http({
                url: $urls.ApiCompra + "DetalleCompra/RegistrarDetalleCompra",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarDetalleCompra(dto) {
            return $http({
                url: $urls.ApiCompra + "DetalleCompra/EliminarDetalleCompra",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();