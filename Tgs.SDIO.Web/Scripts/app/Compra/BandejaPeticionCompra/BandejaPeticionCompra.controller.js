(function () {
    'use strict'
    angular
        .module('app.Compra')
        .controller('BandejaPeticionCompraController', BandejaPeticionCompraController);

        BandejaPeticionCompraController.$inject = ['BandejaPeticionCompraService',
        'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$filter'];

    function BandejaPeticionCompraController(BandejaPeticionCompraService,
        MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $filter)
    {
        var vm = this;

        vm.EnlaceRegistrar = $urls.ApiCompra + "PeticionCompra/Index/";
        vm.EnlaceVer = $urls.ApiCompra + "PeticionCompra/Ver";
        
        vm.FiltrosPreCompra = {
            Tipo: -1
        }

        vm.BuscarPeticionCompra = BuscarPeticionCompra;
        vm.CargarTipoCosto = CargarTipoCosto;

        vm.CodigoCompra = "";
        vm.IdAsociadoProyecto = "0";
        vm.IdTipoCompra = "0";
        vm.IdTipoCosto = "0";
        vm.FechaInicio = "";
        vm.FechaFin = "";


        ListarAsociadoProyecto();
        ListarTipoCompra();
        ListarTipoCosto("0");
        BuscarPeticionCompra();

        vm.dtInstance = {};
        vm.dtColumns = [
            DTColumnBuilder.newColumn("IdPeticionCompra").withTitle('Código').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Asociado a Proyecto').notSortable().renderWith(AsociadoAProyecto).withOption('width', '10%'),
            DTColumnBuilder.newColumn("TipoCompra").withTitle('Tipo de Compra').notSortable().withOption('width', '20%'),
            DTColumnBuilder.newColumn("TipoCosto").withTitle('Tipo de Costo').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn("ElementoPEP").withTitle('Codigo PEP').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("Etapa").withTitle('Etapa').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("Fecha").withTitle('Fecha').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("EstadoEtapa").withTitle('Estado Etapa').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("LineaNegocio").withTitle('Linea de Negocio').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("Cliente").withTitle('Cliente').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("Solicitante").withTitle('Solicitante').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("Proveedor").withTitle('Proveedor').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("DetalleSolpe").withTitle('Solpe').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn("NumeroPedido").withTitle('#OC').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ]; 

        function AsociadoAProyecto(data, type, full, meta) {
            var bAsociadoAProyecto = data.AsociadoAProyecto;
            
            return bAsociadoAProyecto ? "Si" : "No";
        }

        function AccionesBusqueda(data, type, full, meta) {
            var lnk = vm.EnlaceRegistrar + data.IdPeticionCompra;
            var icono = "fa fa-edit fa-lg";
            var title  = "Editar";

            if (Seguimiento == 1){
                lnk = vm.EnlaceRegistrar + "?id=" + data.IdPeticionCompra + "&ver=" + 1;
                icono = "fa fa-eye";
                title  = "Ver";
            }

            return "<div class='col-xs-6 col-sm-6'><a title='" + title + "' class='btn btn-primary'  id='tab-button' href='" + lnk + "' >" + "<span class='" + icono + "'></span>" + "</a></div>  "
        }

        function ListarPeticionCompra(sSource, aoData, fnCallback, oSettings) {
            
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            
            var request = {
                CodigoPeticionCompra: vm.CodigoCompra,
                AsociadoAProyecto: (vm.IdAsociadoProyecto == "0" ? null : (vm.IdAsociadoProyecto == "2" ? true : false)),
                IdTipoCompra: vm.IdTipoCompra,
                IdTipoCosto: vm.IdTipoCosto,
                FechaInicio: vm.FechaInicio,
                FechaFin: vm.FechaFin,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = BandejaPeticionCompraService.ListarPeticionCompraPaginado(request);
            promise.then(function (resultado) {

                var listaBandeja = resultado.data.ListaPeticionDtoResponse;
                
                listaBandeja.forEach(detalle => {
                    var fecha = new Date(parseInt(detalle.Fecha.substr(6)));
                    detalle.Fecha =  $filter('date')(fecha, 'dd/MM/yyyy');
                });

                var result = {
                    'draw': draw,
                    'recordsTotal': (listaBandeja.length > 0 ? listaBandeja[0].TotalRegistros : 0),
                    'recordsFiltered': (listaBandeja.length > 0 ? listaBandeja[0].TotalRegistros : 0),
                    'data': listaBandeja
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function BuscarPeticionCompra() {
            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(ListarPeticionCompra)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withLanguage({"sEmptyTable" : "No hay datos disponibles en la tabla"})
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('paging', false);
        }

        function ListarAsociadoProyecto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 518
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaAsociadoProyecto = UtilsFactory.AgregarItemSelectTodos(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarTipoCompra() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 481
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListaTipoCompra = UtilsFactory.AgregarItemSelectTodos(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarTipoCosto(asociadoProyecto) {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 485
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                
                if (asociadoProyecto == "0") {
                    var lista = [];
                    vm.ListaTipoCosto = UtilsFactory.AgregarItemSelectTodos(lista);
                } else if (asociadoProyecto == "1") {
                    var lista = [];
                    var oCeCo = Respuesta.find(r => r.Codigo == 1);
                    lista.push(oCeCo);
                    vm.ListaTipoCosto = UtilsFactory.AgregarItemSelectTodos(lista);
                } else if (asociadoProyecto == "2") {
                    vm.ListaTipoCosto = UtilsFactory.AgregarItemSelectTodos(Respuesta);
                }
            }, function (response) {
                blockUI.stop();
            });
        }

        function CargarTipoCosto() {
            var idAsociadoProyecto = vm.IdAsociadoProyecto;
            ListarTipoCosto(idAsociadoProyecto);
        }

    }
})();