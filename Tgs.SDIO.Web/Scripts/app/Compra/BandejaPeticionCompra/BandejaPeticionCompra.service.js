(function () {
    'use strict',
     angular
    .module('app.Compra')
    .service('BandejaPeticionCompraService', BandejaPeticionCompraService);

    BandejaPeticionCompraService.$inject = ['$http', 'URLS'];

    function BandejaPeticionCompraService($http, $urls) {

        var service = {
            ListarPeticionCompraPaginado: ListarPeticionCompraPaginado,
        };

        return service;

        function ListarPeticionCompraPaginado(dto) {
            return $http({
                url: $urls.ApiCompra + "BandejaPeticionCompra/ListarPeticionCompraPaginado",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();