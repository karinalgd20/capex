﻿var vm;

(function () {
    'use strict',

    angular
    .module('app.Negocios', [])
    .controller('ResumenServicioOfertaController', ResumenServicioOfertaController)

    ResumenServicioOfertaController.$inject =
        [
            'ResumenServicioOfertaService', 'blockUI', 'UtilsFactory',
            'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI',
            'URLS', '$scope', '$compile', '$modal', '$injector'
        ]
    
    function ResumenServicioOfertaController
        (
            ResumenServicioOfertaService, blockUI, UtilsFactory,
            DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI,
            URLS, $scope, $compile, $modal, $injector
        )

    {
        vm = this;

        /*--------------- Tabla Contactos ----------------*/

        vm.dtInstanciaDetalleContacto = {};

        vm.dtColumnsDetalleContacto =
           [
               DTColumnBuilder.newColumn('Id').notVisible(),
               DTColumnBuilder.newColumn(null).withTitle(titleHtmlServicio).notSortable().withOption('width', '3%').renderWith(function (data, type, full, meta) {
                   vm.selectedServicio[full.Id] = false;
                   return '<input type="checkbox" class="chkServicio" name="checkboxServicio" ng-model="vm.selectedServicio[\'' + data.Id + '\']" ng-click="vm.ObtenerServicioById()">';
               }),
               DTColumnBuilder.newColumn('NombreServicio').withTitle('Descripcion').notSortable(),
               DTColumnBuilder.newColumn(null).withTitle('Completa').notSortable(),
               DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesServiciosOfertas)
           ];

        function AccionesDetalleContacto(data, type, full, meta) {
            var respuesta = "";
            respuesta = "<center><a title='Editar Contacto' ng-click='vm.EditarContacto(" + data.Id + ");'>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6;'></span></a></center>";
            //respuesta = "<center><a title='Eliminar Servicio'  ng-click='vm.EliminarServicio(" + data.Id + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6;'></span></a></center>";
            return respuesta;
        }

        function LimpiarGrillaDetalleContacto() {
            vm.dtOptionsDetalleContacto = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('scrollY', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarDetalleContacto() {

            blockUI.start();
            LimpiarGrillaDetalleContacto()
            $timeout(function () {
                vm.dtOptionsDetalleContacto = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withOption('scrollY', 200)
                .withFnServerData(BuscarDetalleContactoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', false)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        };

        function BuscarDetalleContactoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var DetalleContacto = {
                Id: vm.IdContacto,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ResumenServicioOfertaService.ListarDetalleContactoPaginado(DetalleContacto);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaServiciosxNroOfertaDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaDetalleContacto()
            });
        };

    }

})();