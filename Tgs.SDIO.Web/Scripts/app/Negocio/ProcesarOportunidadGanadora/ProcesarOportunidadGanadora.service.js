﻿(function () {
    'use strict',

    angular
    .module('app.Negocio')
    .service('ProcesarOportunidadGanadoraService', ProcesarOportunidadGanadoraService);

    ProcesarOportunidadGanadoraService.$inject = ['$http', 'URLS']

    function ProcesarOportunidadGanadoraService($http, $urls)
    {
        var service = {

            ListarIsisNroOferta: ListarIsisNroOferta,
            AgregarServicioRPA: AgregarServicioRPA,
            ListarServiciosNroOfertaPaginado: ListarServiciosNroOfertaPaginado,
            AgregarSisegoDetalles: AgregarSisegoDetalles,
            ListarSisegoDetallesPaginado: ListarSisegoDetallesPaginado,
            AsociarServicioSisego: AsociarServicioSisego,
            ListarServicioSisegoPaginado: ListarServicioSisegoPaginado,
            ObtenerOportunidadById: ObtenerOportunidadById,
            ObtenerIdOportunidad: ObtenerIdOportunidad,
            ObtenerRucClientePorIdOportunidad: ObtenerRucClientePorIdOportunidad,
            ActualizarEtapaCierreOportunidad: ActualizarEtapaCierreOportunidad
        };

        return service;

        /*------------- Implementacion de Servicios -------------*/

        function ListarIsisNroOferta(request) {
            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/ListarIsisNroOferta",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function AgregarServicioRPA(request)
        {
            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/AgregarServicioRPA",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ListarServiciosNroOfertaPaginado(request) {
            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/ListarServiciosNroOfertaPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function AgregarSisegoDetalles(request) {
            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/AgregarSisegoDetalles",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ListarSisegoDetallesPaginado(request) {
            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/ListarSisegoDetallesPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function AsociarServicioSisego(request) {
            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/AsociarServicioSisego",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ListarServicioSisegoPaginado(request) {
            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/ListarServicioSisegoPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ObtenerOportunidadById(request) {
            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/ObtenerOportunidadById",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ObtenerRucClientePorIdOportunidad(request) {

            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/ObtenerRucClientePorIdOportunidad",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ObtenerIdOportunidad(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/ObtenerOportunidadGanadora",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        }

        function ActualizarEtapaCierreOportunidad(request) {
            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/ActualizarEtapaCierreOportunidad",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        }
            //function ObtenerOportunidadGanadora(request) {
            //    return $http({
            //        url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/ObtenerOportunidadGanadora",
            //        method: "POST",
            //        data: JSON.stringify(request)
            //    }).then(DatosCompletados);

            //    function DatosCompletados(response) {
            //        return response;
            //    }
            //}

            //function RegistrarOportunidad(request) {
            //    return $http({
            //        url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/RegistarOportunidadGanadora",
            //        method: "POST",
            //        data: JSON.stringify(request)
            //    }).then(DatosCompletados);

            //    function DatosCompletados(resultado) {
            //        return resultado;
            //    }
            //}
    }

})();