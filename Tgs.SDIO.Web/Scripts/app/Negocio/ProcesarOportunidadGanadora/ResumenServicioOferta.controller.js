﻿var vm;

(function () {
    'use strict',

    angular
    .module('app.Negocio')
    .controller('ResumenServicioOfertaController', ResumenServicioOfertaController)

    ResumenServicioOfertaController.$inject =
        [
            'ResumenServicioOfertaService','MaestraService', 'blockUI', 'UtilsFactory',
            'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI',
            'URLS', '$scope', '$compile', '$modal', '$injector'
        ]

    function ResumenServicioOfertaController
        (
            ResumenServicioOfertaService, MaestraService, blockUI, UtilsFactory,
            DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI,
            URLS, $scope, $compile, $modal, $injector
        ) {
        vm = this;
        vm.IdServicio = "";
        vm.IdServicio = parseInt($('#IdServicio').val());
        CargarTipoVia();
        vm.ActualizarContacto = ModalActualizarContacto;
        vm.GuardarContacto = GuardarContacto;
        vm.LimpiarFormulario = LimpiarFormulario;
        vm.CerrarPopup = CerrarPopup;
        vm.BuscarDetalleContacto = BuscarDetalleContacto;
        vm.GrabarDetalles = GrabarDetalles;

        /****** Servicio Detalles ******/
        vm.Cantidad = "";
        vm.Servicio = "";
        vm.Velocidad = "";
        vm.Accion = "";
        vm.MedioTx = "";
        vm.TipoCircuito = -1;
        vm.NumeroCDK = 0;
        vm.NumeroCD = 0;
        vm.ModalidadConexion = "";
        vm.DatosPlatinium = "";
        vm.BW = "";
        vm.CaudalVoz5 = "";
        vm.CaudalVoz2 = "";
        vm.TiempoRealVOZ = "";
        vm.DatosORO = "";
        vm.CaudalORO = "";
        vm.CaudalVideo4 = "";
        vm.VRF = "";
        vm.TiempoRealVideo = "";
        vm.DatosPLATA = "";
        vm.CaudalPLATA = "";
        vm.CaudalPlatinium = "";
        vm.DatosBRONCE = "";
        vm.CaudalLDN = "";
        vm.CaudalOroTos2 = "";
        CargarDetallesServicio();

        function CargarDetallesServicio() {
            if (vm.IdServicio > 0) {
                var Servicio =
                    {
                        Id: vm.IdServicio
                    }
                var promise = ResumenServicioOfertaService.ListarServicioDetalles(Servicio);
                promise.then(function (response) {
                    if (response.data != null && response.data != '') {
                        vm.Cantidad = response.data[0].Cantidad;
                        vm.Servicio = response.data[0].NombreServicio;
                        vm.Velocidad = response.data[0].Velocidad;
                        vm.Accion =  response.data[0].Accion;
                        vm.MedioTx = response.data[0].MedioTx;
                        vm.TipoCircuito = response.data[0].TipoCircuito;
                        vm.NumeroCDK = response.data[0].NumeroCDK;
                        vm.NumeroCD = response.data[0].NumeroCD;

                        /**** Condicional IVPN *****/
                        if (vm.Servicio.toUpperCase() == "IVPN") {
                            HabilitaCampoIVPN();
                        }
                        vm.ModalidadConexion = response.data[0].ModalidadConexion;
                        vm.DatosPlatinium = response.data[0].DatosPlatinium;
                        vm.BW = response.data[0].BW;;
                        vm.CaudalVoz5 = response.data[0].CaudalVoz5;
                        vm.CaudalVoz2 = response.data[0].CaudalVoz2;
                        vm.TiempoRealVOZ = response.data[0].Realtime;
                        vm.DatosORO = response.data[0].DatosORO;
                        vm.CaudalORO = response.data[0].Caudal_oro;
                        vm.CaudalVideo4 = response.data[0].Video;
                        vm.VRF = response.data[0].Caudal_VRF;
                        vm.TiempoRealVideo = response.data[0].TiempoRealVideo;
                        vm.DatosPLATA = response.data[0].DatosPLATA;
                        vm.CaudalPLATA = response.data[0].Caudal_plata;
                        vm.CaudalPlatinium = response.data[0].Caudal_platino;
                        vm.DatosBRONCE = response.data[0].Caudal_Bronce;
                        vm.CaudalLDN = response.data[0].LDN;
                        vm.CaudalOroTos2 = response.data[0].CaudalOroTos2;
                    }
                }, function (response) {
                    blockui.stop();
                });
            }
        }
        
        /****** Sisego Detalles ******/
        vm.TipoVia = -1;
        vm.Direccion = "";
        vm.Piso = "";
        vm.Numero = "";
        vm.Interior = "";
        vm.Departamento = "";
        vm.Provincia = "";
        vm.Ciudad = "";
        vm.Distrito = "";
        CargarSisegoDetalles();
        vm.selectedvalorAgregado = {};

        function CargarSisegoDetalles() {
            if (vm.IdServicio > 0) {

                var Servicio =
                    {
                        Id: vm.IdServicio
                    }
                var promise = ResumenServicioOfertaService.ListarSisegoDetalles(Servicio);
                promise.then(function (response) {
                    if (response.data != null && response.data != '') {
                        vm.Direccion = response.data[0].Direccion;
                        vm.Piso = response.data[0].Piso;
                        vm.Numero = response.data[0].Numero;
                        vm.Interior = response.data[0].Interior;
                        vm.Departamento = response.data[0].Departamento;
                        vm.Provincia = response.data[0].Provincia;
                        vm.Ciudad = response.data[0].Ciudad;
                        vm.Distrito = response.data[0].Distrito;
                    }
                }, function (response) {
                    blockui.stop();
                });
            }
        }

        /*--------------- Modal SisegoDetalles ----------------*/
        function ModalSisegoDetalles(Id) {

            vm.LimpiarFormulario();
            $("#titleModal").val("Detalles SISEGO");
            
            $('#ModalSisegoDetalles').modal({
                keyboard: false
            });

        }

        /*--------------- Tabla Detalles Equipo----------------*/
        
        vm.dtInstanceDatosEquipos = {};

        vm.dtColumnsDatosEquipos =
           [
               DTColumnBuilder.newColumn('Id').notVisible(),
               DTColumnBuilder.newColumn('Cod_Equipo').withTitle('Codigo Equipo').notSortable(),
               DTColumnBuilder.newColumn('Tipo').withTitle('Tipo').notSortable(),
               DTColumnBuilder.newColumn('Marca').withTitle('Marca').notSortable(),
               DTColumnBuilder.newColumn('Modelo').withTitle('Modelo').notSortable(),
               DTColumnBuilder.newColumn('Componente_1').withTitle('Componente').notSortable(),
               DTColumnBuilder.newColumn('Tipo_de_acceso').withTitle('Tipo de Acceso').notSortable(),
               DTColumnBuilder.newColumn('').withTitle('Valor Agregado').renderWith((data, type, full, meta) =>
               {
                   let NumeroFila = meta.row;
                   let respuesta = "";
                   respuesta = '<center><input type="checkbox" ng-model="vm.selectedvalorAgregado[\'' + NumeroFila + '\']"></center>';
                   return respuesta;
               }).notSortable()
           ];

        LimpiarGrillaDetalleEquipo();

        BuscarDetalleEquipo();

        function LimpiarGrillaDetalleEquipo() {
            vm.dtOptionsDatosEquipos = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('scrollY', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarDetalleEquipo() {

            blockUI.start();
            LimpiarGrillaDetalleEquipo()
            $timeout(function () {
                vm.dtOptionsDatosEquipos = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withOption('scrollY', 200)
                .withFnServerData(BuscarDetalleEquipoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', false)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        };

        function BuscarDetalleEquipoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var DetalleContacto = {
                Id: vm.IdContacto,
                IdServicioSisego: vm.IdServicio,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ResumenServicioOfertaService.ListarDetalleEquipos(DetalleContacto);
            promise.then((resultado) => {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaEquiposServicioDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, (response) => {
                blockUI.stop();
                LimpiarGrillaDetalleEquipo()
            });
        };
        /*--------------- Tabla Contactos ----------------*/

        vm.dtInstanciaDetalleContacto = {};

        vm.dtColumnsDetalleContacto =
           [
               DTColumnBuilder.newColumn('Id').notVisible(),
               DTColumnBuilder.newColumn('Contacto').withTitle('Contacto 1').notSortable(),
               DTColumnBuilder.newColumn('Telefono_1').withTitle('Telefono 1').notSortable(),
               DTColumnBuilder.newColumn('Telefono_2').withTitle('Telefono 2').notSortable(),
               DTColumnBuilder.newColumn('Telefono_3').withTitle('Telefono 3').notSortable(),
               DTColumnBuilder.newColumn('Contacto_2').withTitle('Contacto 2').notSortable(),
               DTColumnBuilder.newColumn('Telefono1_Contacto2').withTitle('Telefono 1').notSortable(),
               DTColumnBuilder.newColumn('Telefono2_Contacto2').withTitle('Telefono 2').notSortable(),
               DTColumnBuilder.newColumn('Telefono3_Contacto2').withTitle('Telefono 3').notSortable(),
               DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesDetalleContacto)
           ];
        
        function AccionesDetalleContacto(data, type, full, meta) {
            let NumeroFila = meta.row;
            respuesta = "<center><a title='Actualizar Contacto' ng-click='vm.ActualizarContacto(\"" + NumeroFila + "\");'>" + "<span class='glyphicon glyphicon-save' style='color: #00A4B6; margin-right:.5em'></span></a>"
            + "<a title='Eliminar Contacto'  ng-click='vm.EliminarContacto(\"" + NumeroFila + "\");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6;'></span></a></center>";
            return respuesta;
        }

        LimpiarGrillaDetalleContacto();

        BuscarDetalleContacto();

        function LimpiarGrillaDetalleContacto() {
            vm.dtOptionsDetalleContacto = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('scrollY', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarDetalleContacto() {

            blockUI.start();
            LimpiarGrillaDetalleContacto()
            $timeout(function () {
                vm.dtOptionsDetalleContacto = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withOption('scrollY', 200)
                .withFnServerData(BuscarDetalleContactoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', false)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        };

        function BuscarDetalleContactoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var DetalleContacto = {
                Id: vm.IdContacto,
                IdServicio: vm.IdServicio,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ResumenServicioOfertaService.ListarDetalleContacto(DetalleContacto);
            promise.then((resultado) => {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaDetallesContactoRPAPaginadoDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, (response) => {
                blockUI.stop();
                LimpiarGrillaDetalleContacto()
            });
        };

        function CargarTipoVia() {
            var filtro = {
                IdRelacion: 445
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then((resultado) => {
                var Respuesta = resultado.data;
                vm.ListaTipoVia = UtilsFactory.AgregarItemSelect(Respuesta);
            },(response) => {
                blockUI.stop();
            });
        };
       
        function HabilitaCampoIVPN() {
            $('#ModalidadConexion').removeAttr('disabled', 'disabled')
            $('#DatosPlatinium').removeAttr('disabled', 'disabled')
            $('#BW').removeAttr('disabled', 'disabled')
            $('#CaudalVoz').removeAttr('disabled', 'disabled')
            $('#CaudalVoz5').removeAttr('disabled', 'disabled')
            $('#TiempoRealVOZ').removeAttr('disabled', 'disabled')
            $('#DatosORO').removeAttr('disabled', 'disabled')
            $('#CaudalORO').removeAttr('disabled', 'disabled')
            $('#CaudalVideo4').removeAttr('disabled', 'disabled')
            $('#VRF').removeAttr('disabled', 'disabled')
            $('#TiempoRealVideo').removeAttr('disabled', 'disabled')
            $('#DatosPLATA').removeAttr('disabled', 'disabled')
            $('#CaudalPLATA').removeAttr('disabled', 'disabled')
            $('#CaudalPlatinum3').removeAttr('disabled', 'disabled')
            $('#CaudalVideo4').removeAttr('disabled', 'disabled')
            $('#DatosBRONCE').removeAttr('disabled', 'disabled')
            $('#CaudalLDN').removeAttr('disabled', 'disabled')
            $('#CaudalOroTos2').removeAttr('disabled', 'disabled')
         
        }

        /*--------------- Modal Contactos ----------------*/
        function ModalActualizarContacto(NumeroFila) {
            let that = this;
            let contacto = that.dtInstanciaDetalleContacto.dataTable.fnGetData(NumeroFila);

            vm.LimpiarFormulario();
            $("#titleModal").val("Actualizar Contacto");
            contacto.Id = parseInt(contacto.Id);
            contacto.Contacto = contacto.Contacto;
            contacto.Telefono1 = contacto.Telefono_1
            contacto.Telefono2 = contacto.Telefono_2
            contacto.Telefono3 = contacto.Telefono_3
            contacto.Contacto2 = contacto.Contacto_2
            vm.contacto = contacto;
            vm.contacto.IdEstado = 1;

            $('#ModalActualizarContactoSisego').modal({
                keyboard: false
            });

        }

        function LimpiarFormulario() {
            vm.contacto = {};
            vm.contacto.Id = 0;
            vm.contacto.Contacto = "";
            vm.contacto.Telefono1 = "";
            vm.contacto.Telefono2 = "";
            vm.contacto.Telefono3 = "";
            vm.contacto.Contacto_2 = "";
            vm.contacto.Telefono1_Contacto2 = "";
            vm.contacto.Telefono2_Contacto2 = "";
            vm.contacto.Telefono3_Contacto2 = "";
        }

        /*-------- Accion Modal Guardar Contactos --------*/
        function GuardarContacto(contacto, divAlerta) {
            if ($scope.formularioActualizarContacto.$valid) {
                blockUI.start();
                var promise = null;

                if (contacto.Id > 0) {
                    promise = ResumenServicioOfertaService.ActualizarContactoServicio(contacto);
                } else {
                    promise = ResumenServicioOfertaService.RegistrarContactoServicio(contacto);
                }

                promise.then(
                    //Success:
                     (response) => {
                        blockUI.stop();
                        var Respuesta = response.data;

                        if (Respuesta.TipoRespuesta != 0) {
                            UtilsFactory.Alerta('#' + divAlerta, 'danger', Respuesta.Mensaje, 5);
                        } else {
                            UtilsFactory.Alerta('#' + divAlerta, 'success', Respuesta.Mensaje, 3);
                            vm.BuscarDetalleContacto();
                            setTimeout(() => { vm.CerrarPopup(); }, 3000);

                        }
                    },
                    //Error:
                    function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#' + divAlerta, 'danger', MensajesUI.DatosError, 5);
                    });
            }
        }

        function CerrarPopup() {
            $('#ModalActualizarContactoSisego').modal('hide');
            $('#ModalEliminarContactoSisego').modal('hide');
        };

        /*--------Grabar Detalles  --------*/
        function GrabarDetalles() {
            var dto = {}

            var promise = null;

            promise = ResumenServicioOfertaService.GrabarDetalles(dto);
            promise.then(
                (response) => {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#' + divAlerta, 'danger', Respuesta.Mensaje, 5);
                    }
                    else {
                        UtilsFactory.Alerta('#' + divAlerta, 'success', Respuesta.Mensaje, 3);
                    }
                },
                (response) => {
                    blockUI.stop();
                    UtilsFactory.Alerta('#' + divAlerta, 'danger', MensajesUI.DatosError, 5);
                });
        }
    }

})();