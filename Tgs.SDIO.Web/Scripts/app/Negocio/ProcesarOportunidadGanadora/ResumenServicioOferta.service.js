﻿(function () {
    'use strict',

    angular
    .module('app.Negocio')
    .service('ResumenServicioOfertaService', ResumenServicioOfertaService);

    ResumenServicioOfertaService.$inject = ['$http', 'URLS']

    function ResumenServicioOfertaService($http, $urls) {
        var service = {
            ListarServicioDetalles: ListarServicioDetalles,
            ListarSisegoDetalles: ListarSisegoDetalles,
            ListarDetalleEquipos: ListarDetalleEquipos,
            ListarDetalleContacto: ListarDetalleContacto,
            ActualizarContactoServicio: ActualizarContactoServicio,
            RegistrarContactoServicio: RegistrarContactoServicio
        };

        return service;

        function ListarServicioDetalles(request) {

            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/ListarServicioDetalles",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ListarSisegoDetalles(request) {

            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/ListarSisegoDetalles",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ListarDetalleEquipos(request) {

            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/ListarDetalleEquipos",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ListarDetalleContacto(request) {

            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/ListarDetalleContacto",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ActualizarContactoServicio(request) {

            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/ActualizarContactoServicio",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function RegistrarContactoServicio(request) {

            return $http({
                url: $urls.ApiNegocio + "ProcesarOportunidadGanadora/RegistrarContactoServicio",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
        
        
    }

})();