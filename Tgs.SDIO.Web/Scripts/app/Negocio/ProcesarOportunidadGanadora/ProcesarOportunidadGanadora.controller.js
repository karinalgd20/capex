﻿var vm;
(function () {
    'use strict',

    angular
    .module('app.Negocio')
    .controller('ProcesarOportunidadGanadoraController', ProcesarOportunidadGanadoraController)

    ProcesarOportunidadGanadoraController.$inject =
        [
            'ProcesarOportunidadGanadoraService', 'blockUI', 'UtilsFactory',
            'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI',
            'URLS', '$scope', '$compile', '$modal', '$injector'
        ]

    function ProcesarOportunidadGanadoraController
        (
            ProcesarOportunidadGanadoraService, blockUI, UtilsFactory,
            DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI,
            $urls, $scope, $compile, $modal, $injector
        )
    {
        vm = this;
        vm.optionGics = "";
        vm.optionGics = $('#optionsGics').val();
        vm.CargarDatos = CargarDatos;
        vm.CodigoCD = "";
        vm.IdOferta = "-1";
        vm.AsociarServiciosSisegos = AsociarServiciosSisegos;
        vm.btnProcesarCierre = ActualizarEtapaCierreOportunidad;
        vm.btnModalProcesarCierre = ModalCierreOportunidad;
        //vm.ValidarCampoCD = ValidarCampoCD;
        vm.ObtenerServicioById = ObtenerServicioById;
        vm.ObtenerSisegoById = ObtenerSisegoById;
        vm.Oportunidad = "";
        ObtenerOportunidadById();
        ListarIsisNroOferta();
       
        /*--------- Tabla Servicios x Nro Ofertas ---------*/
        vm.IdServicio = "";
        vm.selectedServicio = {};
        vm.toggleOneServicio = toggleOneServicio;
        vm.toggleAllServicio = toggleAllServicio;
        vm.ModalDetalleServicio = ModalDetalleServicio;

        var titleHtmlServicio = '<input type="checkbox"  ng-model="vm.selectAllServicio" ng-click="vm.toggleAllServicio(vm.selectAllServicio, vm.selectedServicio)">';

        function ObtenerOportunidadById()
        {
            var IdOportunidad = parseInt($('#PER').val());

            if (IdOportunidad > 0) {
                var dto =
                    {
                        Id: IdOportunidad
                    }
                var promise = ProcesarOportunidadGanadoraService.ObtenerOportunidadById(dto);
                promise.then((response) => {
                    if (response.data != null && response.data != '') {
                        vm.Oportunidad = response.data.PER;
                        var NumeroPer = response.data.PER;
                        var PER =
                            {
                                PER: vm.Oportunidad
                            }
                        var promise = ProcesarOportunidadGanadoraService.ObtenerRucClientePorIdOportunidad(PER);
                        promise.then((response) => {
                            if (response.data != null && response.data != '') {
                                vm.Cliente = response.data.Descripcion;
                                vm.RUC = response.data.NumeroIdentificadorFiscal;
                                vm.CodCliente = response.data.CodigoCliente;
                            }
                        }, (response) => {
                            blockUI.stop();
                        });
                    }
                }, (response) => {
                    blockUI.stop();
                });
            }
        }

        vm.dtInstanceServiciosOfertas = {};

        vm.dtColumnsServiciosOfertas =
           [
               DTColumnBuilder.newColumn('Id').notVisible(),
               DTColumnBuilder.newColumn('Cantidad').notVisible(),
               DTColumnBuilder.newColumn('Velocidad').notVisible(),
               DTColumnBuilder.newColumn('Accion').notVisible(),
               DTColumnBuilder.newColumn('ModeloTx').notVisible(),
               DTColumnBuilder.newColumn('TipoCircuito').notVisible(),
               DTColumnBuilder.newColumn('LDN').notVisible(),
               DTColumnBuilder.newColumn('Video').notVisible(),
               DTColumnBuilder.newColumn('Realtime').notVisible(),
               DTColumnBuilder.newColumn('Caudal_Bronce').notVisible(),
               DTColumnBuilder.newColumn('Caudal_plata').notVisible(),
               DTColumnBuilder.newColumn('Caudal_platino').notVisible(),
               DTColumnBuilder.newColumn('Caudal_VRF').notVisible(),
               DTColumnBuilder.newColumn(null).withTitle(titleHtmlServicio).notSortable().renderWith(function (data, type, full, meta) {
                   vm.selectedServicio[full.Id] = false;
                   return '<input type="checkbox" class="chkServicio" id="chkServicio' + data.Id + '" name="checkboxServicio" ng-model="vm.selectedServicio[\'' + data.Id + '\']" ng-click="vm.ObtenerServicioById(\'' + data.Id + '\')">';
               }),
               DTColumnBuilder.newColumn('NombreServicio').withTitle('Descripcion').notSortable(),
               DTColumnBuilder.newColumn('Completa').withTitle('Completa').notSortable(),
               DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesServiciosOfertas)
           ];

        function toggleAllServicio(selectAllServicio, selectedServicioItems) {
            for (var Id in selectedServicioItems) {
                if (selectedServicioItems.hasOwnProperty(Id)) {
                    selectedServicioItems[Id] = selectAllServicio;
                }
            }
        }

        function toggleOneServicio(selectedServicioItems) {
            for (var Id in selectedServicioItems) {
                if (selectedServicioItems.hasOwnProperty(Id)) {
                    if (!selectedServicioItems[Id]) {
                        vm.selectAllServicio = false;
                        return;
                    }
                }
            }
            vm.selectedServicioItems = false;
        }

        function AccionesServiciosOfertas(data, type, full, meta) {
            let NumeroFila = meta.row;
            respuesta = "<center><a title='Ver Detalle Servicio' ng-click='vm.ModalDetalleServicio(\"" + NumeroFila + "\");'>" + "<span class='glyphicon glyphicon-search' style='color: #00A4B6; margin-right:.5em'></span></a>"
            + "<a title='Eliminar Servicio'  ng-click='vm.EliminarServicio(" + data.Id + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6;'></span></a></center>";
            return respuesta;
        }

        function LimpiarGrillaServiciosOfertas() {
            vm.dtOptionsServiciosOfertas = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('scrollY', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarServiciosOfertas() {

            blockUI.start();
            LimpiarGrillaServiciosOfertas()
            $timeout(() => {
                vm.dtOptionsServiciosOfertas = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withOption('scrollY', 200)
                .withFnServerData(BuscarServiciosOfertasPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', false)
                    .withOption('createdRow', (row, data, dataIndex) => {
                        $compile(angular.element(row).contents())($scope);
                    })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        };

        function BuscarServiciosOfertasPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var ServiciosOfertas = {
                Id: vm.IdServicio,
                Id_Oferta: vm.IdOferta,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ProcesarOportunidadGanadoraService.ListarServiciosNroOfertaPaginado(ServiciosOfertas);
            promise.then((resultado) => {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaServiciosxNroOfertaDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, (response) => {
                blockUI.stop();
                LimpiarGrillaServiciosOfertas()
            });
        };

        function ModalDetalleServicio(NumeroFila) {
            let that = this;
            let servicio = that.dtInstanceServiciosOfertas.dataTable.fnGetData(NumeroFila);
            $("#titleModal").val("Actualizar Contacto");
            servicio.Id = parseInt(servicio.Id);
            vm.servicio = servicio;
            $('#ModalServicioDetalles').modal({
                keyboard: false
            });
        }

        /*----------- Tabla SISEGOS RPA -----------*/
        vm.IdSisego = "";
        vm.selectedSisego = {};
        vm.toggleOneSisego = toggleOneSisego;
        vm.toggleAllSisego = toggleAllSisego;
        vm.ModalDetalleSisego = ModalDetalleSisego;
        var titleHtmlSisego = '<input type="checkbox"  ng-model="vm.selectAllSisego" ng-click="vm.toggleAllSisego(vm.selectAllSisego, vm.selectedSisego)">';

        vm.dtInstanceSisegoDetalles = {};

        vm.dtColumnsSisegoDetalles =
          [
              DTColumnBuilder.newColumn('Id').notVisible(),
              DTColumnBuilder.newColumn('Ciudad').notVisible(),
              DTColumnBuilder.newColumn('Nro_Piso').notVisible(),
              DTColumnBuilder.newColumn('Numero').notVisible(),
               DTColumnBuilder.newColumn('Interior').notVisible(),
              DTColumnBuilder.newColumn(null).withTitle(titleHtmlSisego).notSortable().withOption('width', '3%').renderWith(function (data, type, full, meta) {
                  vm.selectedSisego[full.Id] = false;
                  return '<input type="checkbox" class="chkSisego" name="checkboxSisego"  id="chkSisego' + data.Id + '" ng-model="vm.selectedSisego[\'' + data.Id + '\']" ng-click="vm.ObtenerSisegoById(\'' + data.Id + '\')">';
              }),
              DTColumnBuilder.newColumn('CodSisego').withTitle('SISEGO').notSortable().withOption('width', '3%'),

              DTColumnBuilder.newColumn('Departamento').withTitle('DPTO').notSortable().withOption('width', '3%'),
              DTColumnBuilder.newColumn('Provincia').withTitle('PROV').notSortable().withOption('width', '3%'),
              DTColumnBuilder.newColumn('Distrito').withTitle('DIST').notSortable().withOption('width', '3%'),
              DTColumnBuilder.newColumn('Direccion').withTitle('Direccion').notSortable().withOption('width', '3%'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesSisegoDetalles).withOption('width', '3%')
          ];

        function toggleAllSisego(selectAllSisego, selectedSisegoItems) {
            for (var Id in selectedSisegoItems) {
                if (selectedSisegoItems.hasOwnProperty(Id)) {
                    selectedSisegoItems[Id] = selectAllSisego;
                }
            }
        }

        function toggleOneSisego(selectedSisegoItems) {
            for (var Id in selectedSisegoItems) {
                if (selectedSisegoItems.hasOwnProperty(Id)) {
                    if (!selectedSisegoItems[Id]) {
                        vm.selectAllSisego = false;
                        return;
                    }
                }
            }
            vm.selectedSisegoItems = false;
        }

        function AccionesSisegoDetalles(data, type, full, meta) {
            let NumeroFila = meta.row;
            respuesta = "<center><a title='Ver Detalle Sisego' ng-click='vm.ModalDetalleSisego(\"" + NumeroFila + "\");'>" + "<span class='glyphicon glyphicon-search' style='color: #00A4B6; margin-right:.5em'></span></a>"
            +"<a title='Eliminar Sisego'  ng-click='vm.EliminarSisego(" + data.Id + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6;'></span></a></center>";
            return respuesta;
        }

        function LimpiarGrillaSisegoDetalles() {
            vm.dtOptionsSisegoDetalles = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('scrollY', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarSisegoDetalles() {

            blockUI.start();
            LimpiarGrillaSisegoDetalles()
            $timeout(() => {
                vm.dtOptionsSisegoDetalles = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withOption('scrollY', 200)
                .withFnServerData(BuscarSisegoDetallesPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', false)
                    .withOption('createdRow', (row, data, dataIndex) => {
                        $compile(angular.element(row).contents())($scope);
                    })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        };

        function BuscarSisegoDetallesPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var SisegoDetalles = {
                Id: vm.IdSisego,
                Id_Oferta: vm.IdOferta,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ProcesarOportunidadGanadoraService.ListarSisegoDetallesPaginado(SisegoDetalles);
            promise.then((resultado) => {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaSisegoDetalleDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, (response) => {
                blockUI.stop();
                LimpiarGrillaSisegoDetalles()
            });
        };

        function ModalDetalleSisego(NumeroFila) {
            let that = this;
            let sisego = that.dtInstanceSisegoDetalles.dataTable.fnGetData(NumeroFila);
            //vm.LimpiarFormulario();
            $("#titleModal").val("Actualizar Contacto");
            sisego.Id = parseInt(sisego.Id);
            sisego.CodSisego = sisego.CodSisego;
            sisego.Ciudad = sisego.Ciudad;
            sisego.Departamento = sisego.Departamento
            sisego.Distrito = sisego.Distrito
            sisego.Direccion = sisego.Direccion

            vm.sisego = sisego;

            $('#ModalSisegoDetalles').modal({
                keyboard: false
            });

        }

        /*----------- Tabla Servicio SISEGOS RPA -----------*/

        function AsociarServiciosSisegos() {
            var ServicioId = parseInt(ObtenerIdServicio(vm.selectedServicio));
            var SisegoId = parseInt(ObtenerIdSisego(vm.selectedSisego));
            var ServicioSisegos = {
                IdServicio: ServicioId,
                IdSisego: SisegoId,
            }

            debugger;
            var promise = ProcesarOportunidadGanadoraService.AsociarServicioSisego(ServicioSisegos);
            blockUI.start();
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                    vm.selectedServicio = false;
                    vm.selectedSisego = false;
                    BuscarServiciosSisegos();
                } else {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'danger', Respuesta.Mensaje, 5);
                }
            })
        } 

        vm.dtInstanceServicioSisegos = {};

        vm.dtColumnsServicioSisegos =
          [
              DTColumnBuilder.newColumn('Id').notVisible(),
              DTColumnBuilder.newColumn('Servicio').withTitle('Servicio').notSortable(),
              DTColumnBuilder.newColumn('Equipo').withTitle('Equipo').notSortable(),
              DTColumnBuilder.newColumn('CodSisego').withTitle('SISEGO').notSortable(),
              DTColumnBuilder.newColumn('Departamento').withTitle('DPTO').notSortable(),
              DTColumnBuilder.newColumn('Provincia').withTitle('PROV').notSortable(),
              DTColumnBuilder.newColumn('Distrito').withTitle('DIST').notSortable(),
              DTColumnBuilder.newColumn('Direccion').withTitle('Direccion').notSortable(),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesServiciosSisegos)
          ];

        function AccionesServiciosSisegos(data, type, full, meta) {
            respuesta = "<center><a title='Ver Detalles' href='../Detalle/" + data.Id + "')'>" + "<span class='glyphicon glyphicon-search' style='color: #00A4B6; margin-right:.5em'></span></a>" +
                        "<a title='Eliminar Servicio'  ng-click='vm.EliminarServicioSisego(" + data.Id + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6;'></span></a></center>";
            return respuesta;
        }

        function LimpiarGrillaServiciosSisegos() {
            vm.dtOptionsServicioSisegos = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('scrollY', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarServiciosSisegos() {

            blockUI.start();
            LimpiarGrillaServiciosSisegos()
            $timeout(() => {
                vm.dtOptionsServicioSisegos = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withOption('scrollY', 200)
                .withFnServerData(BuscarServiciosSisegosPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                    .withOption('createdRow', (row, data, dataIndex) => {
                        $compile(angular.element(row).contents())($scope);
                    })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        };

        function BuscarServiciosSisegosPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var ServicioSisego = {
                Id: vm.IdServicioSisego,
                IdOferta: parseInt(vm.IdOferta),
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ProcesarOportunidadGanadoraService.ListarServicioSisegoPaginado(ServicioSisego);
            promise.then((resultado) => {
                var result = { 
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaAsociarServicioSisegosDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, (response) => {
                    blockUI.stop();
                    LimpiarGrillaServiciosSisegos()
            });
        };

        /*---------  Funciones ---------*/

        function ModalCierreOportunidad() {
            if (vm.optionGics == "False") {
                $('#grupoCD').css('display', 'none')
            }
            let caja = $('#CajaCierre');
            let Ficha = parseInt($('#PER').val());
            caja.html('<div style="margin-left:2em"><h4>Deseas Cerrar La Ficha: <strong> ' + Ficha + '</strong> Oportunidad: <strong> '+ vm.Oportunidad +' </strong>?</h4></div><hr/>');
            $('#ModalCierreOferta').modal({
                keyboard: false
            });
        }

        function ActualizarEtapaCierreOportunidad() {
            let Id = parseInt($('#PER').val());
            if (vm.optionGics == "True") {
                let alerta = $('#CajaCierre');
                var CodigoCd = vm.CodigoCD
                if (CodigoCd == "" || CodigoCd == null) {
                    UtilsFactory.InputBorderColor('#CodigoCD', 'Rojo');
                    alerta.append('<p style="margin-left:2em; color:red">Debes Introducir Codigo de CD/CDK generado manualmente</p><br/>')
                    $timeout(() => {
                        UtilsFactory.InputBorderColor('#CodigoCD', 'Ninguno');
                        let alerta = $('#CajaCierre p');
                        let espacio = $('#CajaCierre br');
                        alerta.remove();
                        espacio.remove();
                    }, 3000);
                }
                else {
                    var oportunidad =
                        {
                            Id: Id
                        }
                    var promise = ProcesarOportunidadGanadoraService.ActualizarEtapaCierreOportunidad(oportunidad);
                    promise.then((response) => {
                        if (response.data != null && response.data != '') {
                            blockUI.stop();
                            UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                        }
                        else {
                            blockUI.stop();
                            UtilsFactory.Alerta('#divAlert', 'danger', Respuesta.Mensaje, 5);
                        }
                    }, (response) => {
                        blockUI.stop();
                    });
                }
            }
            else if (vm.optionGics == "False") {
                var oportunidad =
                        {
                            Id: Id
                        }
                var promise = ProcesarOportunidadGanadoraService.ActualizarEtapaCierreOportunidad(oportunidad);
                promise.then((response) => {
                    if (response.data != null && response.data != '') {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                    }
                    else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', Respuesta.Mensaje, 5);
                    }
                }, (response) => {
                    blockUI.stop();
                });
            }
        }

        function ObtenerIdServicio(dato) {
            var array = [];
            for (var i in dato) {
                if (dato[i] == true) {
                    array.push(i);
                }
            }
            return array;
        }

        function ObtenerServicioById(Id) {
            $('.chkServicio').prop('checked', false);
            $('#chkServicio' + Id).prop('checked', true);
        }

        function ObtenerIdSisego(dato) {
            var array = [];
            for (var i in dato) {
                if (dato[i] == true) {
                    array.push(i);
                }
            }
            return array;
        }

        function ObtenerSisegoById(Id) {
            $('.chkSisego').prop('checked', false);
            $('#chkSisego' + Id).prop('checked', true);
        }

        function ListarIsisNroOferta() {
            var PER = parseInt($('#PER').val());
            var ofertas = {
                IdEstado: 1,
                Id_OportunidadGanadora: PER
            };
            var promise = ProcesarOportunidadGanadoraService.ListarIsisNroOferta(ofertas);

            promise.then((resultado) => {
                blockUI.stop();

                var Respuesta = resultado.data;

                vm.ListaNroOfertas = UtilsFactory.AgregarItemSelect(Respuesta);

            }, (response) => {
                blockUI.stop();
            });
        }

        function CargarDatos() {
            if (vm.IdOferta == "-1") {
                BuscarServiciosOfertas();
                BuscarSisegoDetalles();
                BuscarServiciosSisegos();
                return;
            }
            BuscarServiciosOfertas();
            BuscarSisegoDetalles();
            BuscarServiciosSisegos();
        }   
        
        /*--------- Inicializar Funciones ---------*/
        
        toggleOneServicio();
        toggleAllServicio();
        LimpiarGrillaServiciosOfertas();
        //BuscarServiciosOfertas();
        LimpiarGrillaSisegoDetalles();
        //BuscarSisegoDetalles();
        LimpiarGrillaServiciosSisegos();
        BuscarServiciosSisegos();
        vm.IdOferta = -1;
    }

})();