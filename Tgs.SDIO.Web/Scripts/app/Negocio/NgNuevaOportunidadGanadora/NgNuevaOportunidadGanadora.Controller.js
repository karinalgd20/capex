﻿var vm;
(function () {
    'use strict',

    angular
    .module('app.Negocio')
    .controller('NgNuevaOportunidad', NgNuevaOportunidad)
    NgNuevaOportunidad.$inject = ['NgNuevaOportunidadGanadoraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector']

    function NgNuevaOportunidad(NgNuevaOportunidadGanadoraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector)
    {
        vm = this;

        /* --- Se Definen e Inicializan Variables, Funciones y Json --- */

        vm.IdOportunidad = "";
        vm.PER = "";
        vm.CasoDerivado = "";
        vm.CodigoSisego = "";
        vm.NroOferta = "";
        vm.IdOferta = "";
        vm.Oferta = "";
        vm.Idsisego = "";
        vm.sisego = "";
        vm.NroOfertaVersion = "";
        vm.IdIsisNroOferta = "";
        vm.selectedOferta = {};
        vm.selectedSisego = {};
        vm.toggleOneOfertas = toggleOneOfertas;
        vm.toggleAllOfertas = toggleAllOfertas;
        vm.toggleAllSisego = toggleAllSisego;
        vm.toggleOneSisego = toggleOneSisego;
        vm.ObtenerOportunidadGanadora = ObtenerOportunidadGanadora;
        vm.btnAgregarOferta = AgregarOferta;
        vm.btnAgregarSisego = AgregarSisego;
        vm.btnAgregarOportunidad = RegistrarOportunidad;
        vm.btnEnviarValidacion = ValidarOportunidadGanadora;
        vm.btnAsociarOfertaSisego = AsociarOfertasSisego;
        vm.BuscarCaso = VerificaCampoOportunidadGanadora;
        vm.ObtenerNroOfertaById = ObtenerNroOfertaById;
        vm.tablaOfertas = null;
        vm.tablaSisegos = null;
        vm.EliminarIsisNroOferta = EliminarIsisNroOferta;
        vm.EliminarSisegoCotizado = EliminarSisegoCotizado;
        vm.EliminarOfertaSisego = EliminarOfertaSisego;
        vm.dtInstanceIsisNroOferta = {};
        vm.dtInstanceSisegoCotizado = {};
        vm.dtInstanceAsociarOfertaSisego = {};
        ListSisego = {};
        vm.PER = $("#per").val();

        /* ------------ Registrar Oportunidad Ganadora ------------ */

        function RegistrarOportunidad() {
           
             var OportunidadGanadora = {
                        PER: vm.PER,
                    }
             debugger;
             var mensaje = ValidarCamposOportunidad();
             if (mensaje == "") {
                 var promise = NgNuevaOportunidadGanadoraService.RegistrarOportunidad(OportunidadGanadora);
                        blockUI.start();
                        promise.then((resultado) => {
                            var Respuesta = resultado.data;
                            if (Respuesta.TipoRespuesta == 0) {
                                blockUI.stop();
                                UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                                $('#btnAgregarOportunidad').css("display", "none")
                                $('#Oportunidad').attr('disabled', 'disabled')
                                $('#CasoDerivado').attr('disabled', 'disabled')
                                BuscarIdOportunidad();
                            } else {
                                blockUI.stop();
                                UtilsFactory.Alerta('#divAlert', 'danger', Respuesta.Mensaje, 5);
                            }
                        })
                    }
             else {
                        UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                        $timeout(() => {
                            DesaparecerEfectosOportunidad();
                        }, 3000);
                    }
                
            
        }

        /* ------------ Validar Oportunidad Ganadora ------------ */

        function ValidarOportunidadGanadora() {
            if (vm.PER == null || vm.PER =="") {
                UtilsFactory.Alerta('#divAlert', 'danger', 'Debe Completar Los pasos para enviar validacion', 5);
                UtilsFactory.InputBorderColor('#Oportunidad', 'Rojo');
                $timeout(() => {
                    UtilsFactory.InputBorderColor('#Oportunidad', 'Ninguno');
                }, 2000);
                return;
            }
            if (vm.PER != null || vm.PER != "") {
                var dto = {
                    PER: vm.PER
                };
                var promise = NgNuevaOportunidadGanadoraService.ObtenerIdOportunidad(dto);
                promise.then((response) => {
                    if (response.data == null || response.data == '') {
                        UtilsFactory.Alerta('#divAlert', 'danger', 'No se Encuentra Numero de Oportunidad Verifique', 5);
                        return;
                    }
                    if (response.data != null && response.data != '') {
                        vm.Id = response.data.Id;
                        vm.IdOportunidad = response.data.Id;
                        var OportunidadGanadora = {
                            Id: vm.IdOportunidad,
                        }
                        debugger;
                        var mensaje = ValidarCamposOportunidad();
                        if (mensaje == "") {
                            var promise = NgNuevaOportunidadGanadoraService.ValidarOportunidadGanadora(OportunidadGanadora);
                            blockUI.start();
                            promise.then((resultado) => {
                                var Respuesta = resultado.data;
                                if (Respuesta.TipoRespuesta == 0) {
                                    blockUI.stop();
                                    UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                                   
                                } else {
                                    blockUI.stop();
                                    UtilsFactory.Alerta('#divAlert', 'danger', Respuesta.Mensaje, 5);
                                }
                            })
                        }
                        else {
                            UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                            $timeout(() => {
                                DesaparecerEfectosOportunidad();
                            }, 3000);
                        }
                    }
                }, (response) => {
                    blockUI.stop();
                });
            }
        }

        /* ------------ Agregar IsisNroOferta ------------ */
        
        function AgregarOferta() {
            var IdPER = vm.IdOportunidad;
            if (vm.NroOferta == null || vm.NroOferta == 'undefined' ) {
                UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese un Nro de Oferta", 5);
                return;
            }
            else
            {
                if (vm.NroOfertaVersion == null || vm.NroOfertaVersion == 'undefined'  ) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese un Nro de Version", 5);
                    return;
                }
                else
                {
                    
                    var IsisNroOferta = {
                        Id_OportunidadGanadora: IdPER,
                        Nro_oferta: vm.NroOferta,
                        Nro_version_Oferta: vm.NroOfertaVersion
                    }
                    debugger;
                    var mensaje = ValidarCamposOferta();
                    if (mensaje == "") {
                        var promise = NgNuevaOportunidadGanadoraService.AgregarIsisNroOferta(IsisNroOferta);
                        blockUI.start();
                        promise.then(function (resultado) {
                            var Respuesta = resultado.data;
                            if (Respuesta.TipoRespuesta == 0) {
                                blockUI.stop();
                                BuscarIsisNroOferta();
                                UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                                vm.NroOferta = "";
                                vm.NroOfertaVersion = "";
                            } else {
                                blockUI.stop();
                                UtilsFactory.Alerta('#divAlert', 'danger', Respuesta.Mensaje, 5);
                            }
                        })
                    }
                    else {
                        UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                        $timeout(function () {
                            DesaparecerEfectosOferta();
                        }, 3000);
                    }
                }
            }       
        }

        /* ------------ Tabla NroOfertas ------------ */
        var titleHtmlOfertas = '<input type="checkbox"  ng-model="vm.selectAllOfertas" ng-click="vm.toggleAllOfertas(vm.selectAllOfertas, vm.selectedOfertas)">';

        vm.dtColumnsIsisNroOferta = 
        [
              DTColumnBuilder.newColumn('Id').notVisible(),
              DTColumnBuilder.newColumn(null).withTitle(titleHtmlOfertas).renderWith(function (data, type, full, meta) {
                  vm.selectedOferta[full.Id] = false;
                  return '<input type="checkbox" class="chkOferta" id="chkOferta' + data.Id + '" name="checkboxOferta" ng-model="vm.selectedOferta[\'' + data.Id + '\']" ng-click="vm.ObtenerNroOfertaById(\'' + data.Id + '\')">';
              }),
              DTColumnBuilder.newColumn('Nro_oferta').withTitle('NroOferta'),
              DTColumnBuilder.newColumn('Nro_version_Oferta').withTitle('Version'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesIsisNroOferta)
        ];

        function toggleAllOfertas(selectAllOfertas, selectedOfertasItems) {
            for (var Id in selectedOfertasItems) {
                if (selectedOfertasItems.hasOwnProperty(Id)) {
                    selectedOfertasItems[Id] = selectAllOfertas;
                }
            }
        }

        function toggleOneOfertas(selectedOfertasItems) {
            for (var Id in selectedOfertasItems) {
                if (selectedOfertasItems.hasOwnProperty(Id)) {
                    if (!selectedOfertasItems[Id]) {
                        vm.selectAllOfertas = false;
                        return;
                    }
                }
            }
            vm.selectedOfertasItems = false;
        }
        
        function AccionesIsisNroOferta(data, type, full, meta) {
            respuesta = "<center><a title='Eliminar Oferta'  ng-click='vm.EliminarIsisNroOferta(" + data.Id + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6;'></span></a></center>";
            return respuesta;
        }

        function BuscarIsisNroOferta() {

            blockUI.start();
            LimpiarGrillaIsisNroOferta()
            $timeout(() => {
                vm.dtOptionsIsisNroOferta = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withOption('scrollY', 200)
                .withFnServerData(BuscarIsisNroOfertaPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                    .withOption('createdRow', (row, data, dataIndex) => {
                        $compile(angular.element(row).contents())($scope);
                    })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        };

        function BuscarIsisNroOfertaPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var IsisNroOferta = {
                Id_OportunidadGanadora: vm.IdOportunidad,
                Id: vm.IdIsisNroOferta,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = NgNuevaOportunidadGanadoraService.ListarIsisNroOferta(IsisNroOferta);
            promise.then((resultado) => {
                vm.tablaOfertas = resultado.data.ListaOfertasPaginadoDtoResponse;
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaOfertasPaginadoDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, (response) => {
                blockUI.stop();
                LimpiarGrillaIsisNroOferta()
            });
        };

        function LimpiarGrillaIsisNroOferta() {
            vm.dtOptionsIsisNroOferta = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('scrollY', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        /* ------------ Agregar SisegoCotizado ------------ */
        
        function AgregarSisego() {
            var IdPER = vm.IdOportunidad;
            if (vm.CodigoSisego == null || vm.CodigoSisego == "") {
                UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese Codigo de SISEGO", 5);
                return;
            }
            else
            {
                var SisegoCotizado = {
                    Id_OportunidadGanadora: IdPER,
                    codigo_sisego: vm.CodigoSisego
                }
                debugger;
                var mensaje = ValidarCamposSisego();
                if (mensaje == "") {
                    var promise = NgNuevaOportunidadGanadoraService.AgregarSisegoCotizado(SisegoCotizado);
                    blockUI.start();
                    promise.then((resultado) => {
                        var Respuesta = resultado.data;
                        if (Respuesta.TipoRespuesta == 0) {
                            blockUI.stop();
                            BuscarSisegoCotizado();
                            UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                            vm.CodigoSisego = "";
                        } else {
                            blockUI.stop();
                            UtilsFactory.Alerta('#divAlert', 'danger', Respuesta.Mensaje, 5);

                        }
                    })
                }
            }
        }

        /* ------------ Tabla SisegoCotizado ------------ */

        var titleHtmlSisego = '<input type="checkbox" ng-model="vm.selectAllSisego" ng-click="vm.toggleAllSisego(vm.selectAllSisego, vm.selectedSisego)">';

        vm.dtColumnsSisegoCotizado =
            [
                DTColumnBuilder.newColumn('Id').notVisible(),
                DTColumnBuilder.newColumn(null).withTitle(titleHtmlSisego). 
                    renderWith(function (data, type, full, meta) {
                    vm.selectedSisego[full.Id] = false;
                    return '<input type="checkbox" name="checkbox" ng-model="vm.selectedSisego[\'' + data.Id + '\']">';
                    }),
                DTColumnBuilder.newColumn('codigo_sisego').withTitle('Codigo Sisego'),
                DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesSisegoCotizado)
            ];

        function toggleAllSisego(selectAllSisego, selectedSisegoItems) {
            for (var Id in selectedSisegoItems) {
                if (selectedSisegoItems.hasOwnProperty(Id)) {
                    selectedSisegoItems[Id] = selectAllSisego;
                }
            }
        }

        function toggleOneSisego(selectedSisegoItems) {
            for (var Id in selectedSisegoItems) {
                if (selectedSisegoItems.hasOwnProperty(Id)) {
                    if (!selectedSisegoItems[Id]) {
                        vm.selectAllSisego = false;
                        return;
                    }
                }
            }
            vm.selectAllSisego = true;
        }

        function AccionesSisegoCotizado(data, type, full, meta) {
            respuesta = "<center><a title='Eliminar Sisego'  ng-click='vm.EliminarSisegoCotizado(" + data.Id + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6;'></span></a></center>";
            return respuesta;
        }

        function LimpiarGrillaSisegoCotizado() {
            vm.dtOptionsSisegoCotizado = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('scrollY', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarSisegoCotizado() {

            blockUI.start();
            LimpiarGrillaSisegoCotizado()
            $timeout(function () {
                vm.dtOptionsSisegoCotizado = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withOption('scrollY', 200)
                .withFnServerData(BuscarSisegoCotizadoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 100);
        };

        function BuscarSisegoCotizadoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var IdOportunidadSisego = vm.IdOportunidad;
            var SisegoCotizado = {
                Id_OportunidadGanadora: vm.IdOportunidad,
                Id: vm.IdSisegoCotizado,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = NgNuevaOportunidadGanadoraService.ListarSisegoCotizado(SisegoCotizado);
            promise.then(function (resultado) {
                vm.tablaSisegos = resultado.data.ListaSisegoCotizadoDtoResponse;
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaSisegoCotizadoDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaSisegoCotizado()
            });
        };

        /* ----- Tabla Asociacion NroOfertas y Sisegos ----- */
        
        function AsociarOfertasSisego()
        {
            var OfertaId = parseInt(ObtenerIdOferta(vm.selectedOferta).toString());
            var ArraySisegos = ObtenerIdSisego(vm.selectedSisego).toString();
            var OfertaSisegos = {
                IdOferta: OfertaId, 
                NroOferta: vm.Oferta,
                ListSisego: ArraySisegos
                        
            }
            debugger;
            var promise = NgNuevaOportunidadGanadoraService.AsociarOfertaSisego(OfertaSisegos);
            blockUI.start();
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    vm.Oferta = "";
                    vm.Sisego = "";
                    vm.selectedOferta = false;
                    vm.selectedSisego = false;
                    BuscarAsociarOfertasSisego();
                } else {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar La Asociacion.", 5);
                }
            })
        }

        function ObtenerIdSisego(dato) {
            var ListSisego = [];
            for (var i in dato) {
                if (dato[i] == true) {
                    ListSisego.push(i);
                }
            }
            return ListSisego;
        }

        function ObtenerIdOferta(dato) {
            var array = [];
            for (var i in dato) {
                if (dato[i] == true) {
                    array.push(i);
                }
            }
            return array;
        }

        vm.dtColumnsAsociarOfertaSisego =
           [
               DTColumnBuilder.newColumn('Id').notVisible(),
               DTColumnBuilder.newColumn('IdOferta').withTitle('Id Oferta'),
               DTColumnBuilder.newColumn('NroOferta').withTitle('Nro Oferta'),
                DTColumnBuilder.newColumn('IdSisego').withTitle('Id Sisego'),
               DTColumnBuilder.newColumn('CodSisego').withTitle('Codigo Sisego'),
               DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesAsociarOfertasSisego)
           ];

        function AccionesAsociarOfertasSisego(data, type, full, meta) {
            respuesta = "<center><a title='Eliminar Sisego a Oferta'  ng-click='vm.EliminarOfertaSisego(" + data.Id + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6;'></span></a></center>";
            return respuesta;
        }

        function LimpiarGrillaAsociarOfertasSisego() {
            vm.dtOptionsAsociarOfertaSisego = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('scrollY', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarAsociarOfertasSisego() {

            blockUI.start();
            LimpiarGrillaAsociarOfertasSisego()
            $timeout(() => {
                vm.dtOptionsAsociarOfertaSisego = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withOption('scrollY', 200)
                .withFnServerData(BuscarAsociarOfertasSisegoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                    .withOption('createdRow', (row, data, dataIndex) => {
                        $compile(angular.element(row).contents())($scope);
                    })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 100);
        };

        function BuscarAsociarOfertasSisegoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var IdOportunidad = vm.IdOportunidad;
            var AsociarOfertasSisego = {
                IdOportunidad: IdOportunidad,
                Id: vm.Id,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = NgNuevaOportunidadGanadoraService.ListarOfertaSisego(AsociarOfertasSisego);
            promise.then((resultado) => {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaAsociarOfertaSisegosDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, (response) => {
                blockUI.stop();
                LimpiarGrillaAsociarOfertasSisego()
            });
        };

        /*--Valida los campos ingresados antes del registro--*/
        
        function ValidarCamposOportunidad() {
            var mensaje = "";
            if ($.trim(vm.PER) == "") {
                mensaje = mensaje + "Ingrese Oportunidad Ganadora" + '<br/>';
                UtilsFactory.InputBorderColor('#Oportunidad', 'Rojo');
            }
            return mensaje;
        }

        function ValidarCamposOferta() {
            var mensaje = "";
            if ($.trim(vm.NroOferta) == "") {
                mensaje = mensaje + "Ingrese Nro Oferta" + '<br/>';
                UtilsFactory.InputBorderColor('#NroOferta', 'Rojo');
                return mensaje;
            }
            if ($.trim(vm.NroOfertaVersion) == "") {
                mensaje = mensaje + "Ingrese Numero de Version" + '<br/>';
                UtilsFactory.InputBorderColor('#NroVersion', 'Rojo');
                return mensaje;
            }

            return mensaje;
        }

        function DesaparecerEfectosOferta() {
            UtilsFactory.InputBorderColor('#NroOferta', 'Ninguno');
            UtilsFactory.InputBorderColor('#NroVersion', 'Ninguno');
        }

        function ValidarCamposSisego() {
            var mensaje = "";
            if ($.trim(vm.CodigoSisego) == "") {
                mensaje = mensaje + "Ingrese Codigo Sisego" + '<br/>';
                UtilsFactory.InputBorderColor('#CodigoSisego', 'Rojo');
                return mensaje;
            }
            return mensaje;
        }

        function DesaparecerEfectosSisegos() {
            UtilsFactory.InputBorderColor('#CodigoSisego', 'Ninguno');
        }

        /* ----------------------------- Funciones ---------------------------------- */
        function HabiltaCampos()
        {
            $('#btnAgregarOportunidad').css("display", "inline")
            $('#CasoDerivado').removeAttr("disabled", "disabled")
            $('#NroOferta').removeAttr("disabled", "disabled")
            $('#NroVersion').removeAttr("disabled", "disabled")
            $('#btnAgregarOferta').removeAttr("disabled", "disabled")
            $('#CodigoSisego').removeAttr("disabled", "disabled")
            $('#btnAgregarSisego').removeAttr("disabled", "disabled")
        }

        function DesHabilitaCampos()
        {
            $('#btnAgregarOportunidad').css("display", "none")
            $('#CasoDerivado').attr("disabled", "disabled")
            $('#NroOferta').attr("disabled", "disabled")
            $('#NroVersion').attr("disabled", "disabled")
            $('#btnAgregarOferta').attr("disabled", "disabled")
            $('#CodigoSisego').attr("disabled", "disabled")
            $('#btnAgregarSisego').attr("disabled", "disabled")
        }

        function EliminarIsisNroOferta(Id)
        {
            if (confirm('¿Estas seguro que desea eliminar la oferta?')) {

                var oferta =
                 {
                     Id: Id
                 }
                var promise = NgNuevaOportunidadGanadoraService.EliminarIsisNroOferta(oferta);
                promise.then((resultado) => {
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                        BuscarIsisNroOferta();
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);

                    }
                })
            }
        }

        function EliminarSisegoCotizado(Id)
        {
            if (confirm('¿Estas seguro que desea eliminar la oferta?')) {
                var sisego =
                {
                    Id: Id
                }
                var promise = NgNuevaOportunidadGanadoraService.EliminarSisegoCotizado(sisego);
                promise.then((resultado) => {
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                        BuscarSisegoCotizado();
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    }
                })
            }
        }

        function EliminarOfertaSisego(Id)
        {
            if (confirm('¿Estas seguro que desea eliminar la Asociacion?'))
            {
                var ofertaSisego =
                {
                    Id: Id
                }
                var promise = NgNuevaOportunidadGanadoraService.EliminarOfertaSisego(ofertaSisego);
                promise.then((resultado) => {
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                        BuscarSisegoCotizado();
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    }
                })
            
            }
        }

        /* --------------------- Buscar IdOportunidad -------------------- */
        function BuscarIdOportunidad()
        {
            if (vm.PER != null || vm.PER != "") {
                var dto = {
                    PER: vm.PER
                };
                var promise = NgNuevaOportunidadGanadoraService.ObtenerIdOportunidad(dto);
                promise.then((response) => {
                    if (response.data != null && response.data != '') {
                        vm.Id = response.data.Id;
                        vm.IdOportunidad = response.data.Id;
                    }
                }, (response) => {
                    blockUI.stop();
                });
            }
        }

        /* ------------ Obtener Ruc Cliente Por IdOportunidad ------------ */
        function ObtenerRucClientePorIdOportunidad() {
            if (vm.PER != null && vm.PER != 0) {
                var dto = {
                    PER: vm.PER
                };
                var promise = NgNuevaOportunidadGanadoraService.ObtenerRucClientePorIdOportunidad(dto);
                promise.then((response) => {
                    if (response.data != null && response.data != '') {
                        vm.Id = response.data.Id;
                        vm.RUC = response.data.NumeroIdentificadorFiscal;
                    }
                }, (response) => {
                    blockUI.stop();
                });
            }
        };

        /* ------------- Verifica Campos Oportunidad Ganadora ------------ */
        function VerificaCampoOportunidadGanadora() {
            if (vm.PER == "" || vm.PER == null) {
                DesHabilitaCampos();
            }
            if (vm.PER != null && vm.PER != 0) {
                HabiltaCampos();
                UtilsFactory.InputBorderColor('#Oportunidad', 'Ninguno');
            }
        };

        /* --------------------- Buscar Nro Oferta por Id -------------------- */
        function ObtenerNroOfertaById(Id) {
             var dtoIdOferta = {
                 IdOferta: Id
             };
             $('.chkOferta').prop('checked', false);
             $('#chkOferta' + Id).prop('checked', true);
                var promise = NgNuevaOportunidadGanadoraService.ObtenerNroOfertaById(dtoIdOferta);
                promise.then((response) => {
                    if (response.data != null && response.data != '') {
                        vm.Id = response.data.Id;
                        vm.Oferta = response.data.Nro_oferta;
                    }
                }, (response) => {
                    blockUI.stop();
                });
        }

        /* --------------------- Buscar Oportunidad Ganadora -------------------- */
        function ObtenerOportunidadGanadora() {
            let per = $("#per").val().length;
            if (per > 0) {

                if (vm.PER != null || vm.PER != "") {
                    var dto = {
                        PER: vm.PER
                    };
                    var promise = NgNuevaOportunidadGanadoraService.ObtenerIdOportunidad(dto);
                    promise.then((response) => {
                        if (response.data != null && response.data != '') {
                            vm.Id = response.data.Id;
                            vm.IdOportunidad = response.data.Id;
                            /******************************************************************/
                            $('#NroOferta').removeAttr("disabled", "disabled")
                            $('#NroVersion').removeAttr("disabled", "disabled")
                            $('#btnAgregarOferta').removeAttr("disabled", "disabled")
                            $('#CodigoSisego').removeAttr("disabled", "disabled")
                            $('#btnAgregarSisego').removeAttr("disabled", "disabled")
                            BuscarIsisNroOferta();
                            BuscarSisegoCotizado();
                            BuscarAsociarOfertasSisego();
                        }
                    }, (response) => {
                        blockUI.stop();
                    });
                }
            }
        }

        //Inicializar 

        LimpiarGrillaIsisNroOferta();
        LimpiarGrillaSisegoCotizado();
        LimpiarGrillaAsociarOfertasSisego();
        ObtenerOportunidadGanadora();
    }
})();