﻿(function () {
    'use strict',
         angular
        .module('app.Negocio')
        .service('NgNuevaOportunidadGanadoraService', NgNuevaOportunidadGanadoraService);

    NgNuevaOportunidadGanadoraService.$inject = ['$http', 'URLS'];

    function NgNuevaOportunidadGanadoraService($http, $urls) {

        var service = {

            AgregarIsisNroOferta: AgregarIsisNroOferta,
            ListarIsisNroOferta: ListarIsisNroOferta,
            EliminarIsisNroOferta: EliminarIsisNroOferta,
            AgregarSisegoCotizado: AgregarSisegoCotizado,
            ListarSisegoCotizado: ListarSisegoCotizado,
            EliminarSisegoCotizado: EliminarSisegoCotizado,
            AsociarOfertaSisego: AsociarOfertaSisego,
            ListarOfertaSisego: ListarOfertaSisego,
            EliminarOfertaSisego: EliminarOfertaSisego,
            RegistrarOportunidad: RegistrarOportunidad,
            ValidarOportunidadGanadora: ValidarOportunidadGanadora,
            ObtenerIdOportunidad: ObtenerIdOportunidad,
            ObtenerRucClientePorIdOportunidad: ObtenerRucClientePorIdOportunidad,
            ObtenerCasoOportunidadGanadora: ObtenerCasoOportunidadGanadora,
            ObtenerNroOfertaById: ObtenerNroOfertaById,
            ObtenerOportunidadGanadora: ObtenerOportunidadGanadora
        };

        return service;

        function ObtenerOportunidadGanadora(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/ObtenerOportunidadGanadora",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        }

        function RegistrarOportunidad(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/RegistarOportunidadGanadora",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ValidarOportunidadGanadora(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/ValidarOportunidadGanadora",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function AgregarIsisNroOferta(request)
        {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/AgregarIsisNroOferta",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ListarIsisNroOferta(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/ListarIsisNroOfertaPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function EliminarIsisNroOferta(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/EliminarIsisNroOferta",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function AgregarSisegoCotizado(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/AgregarSisegoCotizado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ListarSisegoCotizado(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/ListarSisegoCotizadoPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function EliminarSisegoCotizado(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/EliminarSisegoCotizado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function AsociarOfertaSisego(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/AsociarOfertaSisego",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ListarOfertaSisego(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/ListarOfertaSisegoPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function EliminarOfertaSisego(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/EliminarOfertaSisego",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }
        
        function ObtenerRucClientePorIdOportunidad(request) {

            return $http({
                url: $urls.ApiNegocio + "NgBandejaOportunidades/ObtenerRucClientePorIdOportunidad",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ObtenerCasoOportunidadGanadora(request)
        {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/ObtenerCasoOportunidadGanadora",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        }

        function ObtenerIdOportunidad(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/ObtenerIdOportunidadGanadora",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        }

        function ObtenerNroOfertaById(request) {
            return $http({
                url: $urls.ApiNegocio + "NgNuevaOportunidadGanadora/ObtenerNroOfertaById",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        }

    }
})();