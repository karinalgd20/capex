﻿var vm;

(function () {
    'use strict',

    angular
    .module('app.Negocio')
    .controller('NgBandejaOportunidadesController', NgBandejaOportunidadesController)

    NgBandejaOportunidadesController.$inject = ['NgBandejaOportunidadesService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector']

    function NgBandejaOportunidadesController(NgBandejaOportunidadesService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        vm = this;

        vm.PER = "";
        vm.Cliente = "";
        vm.RUC = "";
        vm.CodCliente = "";
        vm.Sisego = "";
        vm.Etapa = "";
        vm.EstadoRPA = "";
        vm.FechaInicio = "";
        vm.FechaFin = "";
        vm.optionGics = "";
        vm.optionGics = $('#gics').val();
        vm.Tamanio = 10;
        vm.Indice = 1;
        var total = 0;
        var totalPage = 0;

        vm.CambioPER = CambioPER;
        vm.BuscarOportunidad = BuscarNgOportunidades;
        vm.Manipulado = "";
        vm.Nuevo = $urls.ApiNegocio + "NgNuevaOportunidadGanadora/Index/";
        vm.EliminarOportunidadGanadora = EliminarOportunidadGanadora;
        vm.tablaOportunidades = {};

        CargarEstadoRPA();
        vm.EstadoRPA = -1;
        CargarEtapasRPA();
        vm.EtapaRPA = -1;
        OptionGics();
        /* ------------ Option GICS ------------ */
        function OptionGics() {
            if (vm.optionGics == "True")
            {
                $('#btnNuevo').css('display', 'none');
            }
            else if (vm.optionGics == "False")
            {
                $('#btnNuevo').css('display', 'inline');
            }
        }

        /* ------------ Tabla de BandejaOportunidades ------------ */

        vm.dtInstanceNgOportunidades = {};

        vm.dtColumnsNgOportunidades = [
             DTColumnBuilder.newColumn('Id').withTitle('ID'),
             DTColumnBuilder.newColumn('PER').withTitle('PER'),
             DTColumnBuilder.newColumn('IdEtapa').withTitle('Etapa RPA'),
             DTColumnBuilder.newColumn('strFechaCreacion').withTitle('Fecha Ingreso'),
             DTColumnBuilder.newColumn('Cliente').withTitle('Cliente'),
             DTColumnBuilder.newColumn('Ruc').withTitle('RUC'),
             DTColumnBuilder.newColumn('IdEstado').withTitle('Estado RPA'),
             DTColumnBuilder.newColumn('Progreso').withTitle('Progreso'),
             DTColumnBuilder.newColumn('Manipulado').withTitle('Manipulado'),
             DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesNgOportunidades)
        ];

        function AccionesNgOportunidades(data, type, full, meta) {
            var respuesta = "";
            respuesta = "<center><a title='Editar Oportunidad' href='../NgNuevaOportunidadGanadora/Index?PER=" + data.PER + "')'>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6;'></span></a>"
            + "<a title='Eliminar Oportunidad' ng-click='vm.EliminarOportunidadGanadora(" + data.Id + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6; margin-left:.5em;'></span></a>"
            + "<a title='Procesar Oportunidad' href='../ProcesarOportunidadGanadora/Index/" + data.Id + "')  >" + "<span class='glyphicon glyphicon-ok' style='color: #00A4B6; margin-left:.5em;'></span></a></center>";
            return respuesta;
        };

        function BuscarNgOportunidades() {

            blockUI.start();
            LimpiarGrillaNgOportunidades()
            $timeout(function () {
                vm.dtOptionsNgOportunidades = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withOption('scrollY', 200)
                    .withOption('autoWidth', true)
                    .withOption('sortable ', false)
                .withFnServerData(BuscarNgOportunidadesPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 100);
        };

        function BuscarNgOportunidadesPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var NgBandejaOportunidades = {
                Id: vm.Id,
                PER: vm.PER,
                Ruc: vm.RUC,
                Cliente: vm.Cliente,
                CodigoCliente: vm.CodCliente,
                FechaCreacion: vm.FechaInicio,
                Indice: vm.Indice,
                Tamanio: vm.Tamanio
            };

            var promise = NgBandejaOportunidadesService.ListaBandejaOportunidad(NgBandejaOportunidades);
            
            promise.then((resultado) => {
                vm.tablaOportunidades = resultado.data.ListaBandejaOportunidad;
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data
                };
                blockUI.stop();
                fnCallback(result)

            },(response) => {
                blockUI.stop();
                LimpiarGrillaNgOportunidades()
            });
        };

        function LimpiarGrillaNgOportunidades() {
            vm.dtOptionsNgOportunidades = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('scrollY', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function BuscarOportunidadGanadora() {
            var Oportunidad = vm.PER;
            var Cliente = vm.Cliente;
            var RUC = vm.RUC;
            var CodCliente = vm.CodCliente;

            if (vm.PER != null && vm.PER != 0) {
                var dto = {
                    PER: vm.PER
                };
                var promise = NgBandejaOportunidadesService.BuscarOportunidadGanadora(dto);
                promise.then((response) => {
                    if (response.data != null && response.data != '') {
                        
                      
                    }
                }, (response) => {
                    blockUI.stop();
                });
            }
            else
            {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', "Oportunidad Ganadora no Encontrada Verifique.", 5);
            }
        };

        /* ---------------------------- Funciones Utilizadas -------------------------- */

        function EliminarOportunidadGanadora(Id) {
            if (confirm('¿Estas seguro que desea eliminar la Oportunidad?')) {
                var oportunidad =
                {
                    Id: Id
                }
                var promise = NgBandejaOportunidadesService.EliminarOportunidadGanadora(oportunidad);
                promise.then((resultado) => {
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                        BuscarNgOportunidades();
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    }
                })

            }
        }

        function CambioPER() {
            if (vm.PER == "") {
                HabilitaCampos();
                return;
            }
            DeshabilitaCampos();
        }

        function DeshabilitaCampos() {
            $('#Cliente').attr('disabled', 'disabled')
            $('#RUC').attr('disabled', 'disabled')
            $('#CodCliente').attr('disabled', 'disabled')
            $('#Sisego').attr('disabled', 'disabled')
            $('#Etapa').attr('disabled', 'disabled')
            $('#EstadoRPA').attr('disabled', 'disabled')
            $('#CodCliente').attr('disabled', 'disabled')
            $('#FInicio').attr('disabled', 'disabled')
            $('#FFin').attr('disabled', 'disabled')
            $('#Manipulado').attr('disabled', 'disabled')
        }

        function HabilitaCampos() {
            $('#Cliente').removeAttr('disabled', 'disabled')
            $('#RUC').removeAttr('disabled', 'disabled')
            $('#CodCliente').removeAttr('disabled', 'disabled')
            $('#Sisego').removeAttr('disabled', 'disabled')
            $('#Etapa').removeAttr('disabled', 'disabled')
            $('#EstadoRPA').removeAttr('disabled', 'disabled')
            $('#CodCliente').removeAttr('disabled', 'disabled')
            $('#FInicio').removeAttr('disabled', 'disabled')
            $('#FFin').removeAttr('disabled', 'disabled')
            $('#Manipulado').removeAttr('disabled', 'disabled')
        }

        /* ---------------------------- Cargar ComboBox ------------------------- */
        function CargarEstadoRPA() {
            var filtro = {
                IdRelacion: 470
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then((resultado) => {
                var Respuesta = resultado.data;
                vm.ListaEstadosRPA = UtilsFactory.AgregarItemSelect(Respuesta);
            }, (response) => {
                blockUI.stop();
            });
        };

        function CargarEtapasRPA() {
            var filtro = {
                IdRelacion: 465
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then((resultado) => {
                var Respuesta = resultado.data;
                vm.ListaEtapasRPA = UtilsFactory.AgregarItemSelect(Respuesta);
            }, (response) => {
                blockUI.stop();
            });
        };

        /* ---------------------------- Inicializacion -------------------------- */

        BuscarNgOportunidades();
        DeshabilitaCampos();
        HabilitaCampos();
        
    }

})();