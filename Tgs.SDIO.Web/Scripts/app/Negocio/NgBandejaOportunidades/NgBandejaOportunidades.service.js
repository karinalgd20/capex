﻿(function () {
    'use strict',
     angular
    .module('app.Negocio')
    .service('NgBandejaOportunidadesService', NgBandejaOportunidadesService);

    NgBandejaOportunidadesService.$inject = ['$http', 'URLS'];

    function NgBandejaOportunidadesService($http, $urls) {
        var service = {
            ListaBandejaOportunidad: ListaBandejaOportunidad,
            EliminarOportunidadGanadora: EliminarOportunidadGanadora,
            BuscarOportunidadGanadora: BuscarOportunidadGanadora,
        };

        return service;

        function EliminarOportunidadGanadora(request) {
            return $http({
                url: $urls.ApiNegocio + "NgBandejaOportunidades/EliminarOportunidadGanadora",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ListaBandejaOportunidad(request) {

            return $http({
                url: $urls.ApiNegocio + "NgBandejaOportunidades/ListaBandejaOportunidad",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function BuscarOportunidadGanadora(request) {

            return $http({
                url: $urls.ApiNegocio + "NgBandejaOportunidades/BuscarOportunidadGanadora",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
    }

})();