﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('DatosPreventaMovilService', DatosPreventaMovilService);

    DatosPreventaMovilService.$inject = ['$http', 'URLS'];

    function DatosPreventaMovilService($http, $urls) {

        var service = {
            ObtenerDatosPreventaMovilPorId: ObtenerDatosPreventaMovilPorId,
            RegistrarArchivo: RegistrarArchivo
        };

        return service;

        function ObtenerDatosPreventaMovilPorId(datosPreventa) {
            return $http({
                url: $urls.ApiTrazabilidad + "DatosPreventaMovil/ObtenerDatosPreventaMovilPorId",
                method: "POST",
                data: JSON.stringify(datosPreventa)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


        function RegistrarArchivo(request) {
            debugger;
            return $http({
                url: $urls.ApiTrazabilidad + "DatosPreventaMovil/RegistrarArchivo",
                method: "POST",
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined },
                data: request
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();