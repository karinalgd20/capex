﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('BandejaDatosPostventaMovil', BandejaDatosPostventaMovil);
    BandejaDatosPostventaMovil.$inject = ['BandejaDatosPostventaMovilService', 'MaestraService', 'EstadoCasoService', 'TipoOportunidadService', 'EtapaService', 'SegmentosNegocioService', 'OportunidadSTService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function BandejaDatosPostventaMovil(BandejaDatosPostventaMovilService, MaestraService, EstadoCasoService, TipoOportunidadService, EtapaService, SegmentosNegocioService, OportunidadSTService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.BuscarDatosPostventaMovil = BuscarDatosPostventaMovil;
        vm.EnlaceRegistrar = $urls.ApiTrazabilidad + "BandejaTiempoAtencionEquipo/Index/";
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.IdOportunidadSF = "";
        vm.DescripcionCliente = "";
        vm.IdEtapa = "-1";
        vm.IdEstadoCaso = "-1";
        vm.IdSegmentoNegocio = "-1";
        vm.IdTipoOportunidad = "-1";
        vm.IdComplejidad = "-1";
        vm.CodSisego = "";
        LimpiarGrilla();
        ListarTipoComplejidad();
        vm.listComplejidad = [];
        ObtenerEtapa();
        vm.listEtapas = [];
        ListarComboSegmentos();
        vm.listSegmentosNegocios = [];
        ListarComboEstadosCaso();
        vm.listEstadosCaso = [];
        ListarComboTipoOportunidad();
        vm.listTiposOportunidad = [];

        vm.dtColumns = [
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '1%'),
          DTColumnBuilder.newColumn('IdCaso').notVisible(),
          DTColumnBuilder.newColumn('NombreRecurso').withTitle('PREVENTA').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('IdOportunidadSF').withTitle('OPORTUNIDAD SF').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('IdCasoSF').withTitle('CASO SF').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('CodSisego').withTitle('SISEGO').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('EstadoIngresoSisego').withTitle('ESTADO SISEGO').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DiasSisego').withTitle('DIAS SISEGO').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('strFechaIngreso').withTitle('FECHA DE INGRESO DE SISEGO').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('strFechaInicioEje').withTitle('FECHA INICIO EJE').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DescripcionCliente').withTitle('CLIENTE').notSortable().withOption('width', '44%'),
          DTColumnBuilder.newColumn('Sede').withTitle('SEDE').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('EstadoDeCaso').withTitle('ESTADO CASO').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DescripcionEquipoTrabajo').withTitle('EQUIPO RESPONSABLE ACTUAL').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('strTiempoatencionFechaEdicion').withTitle('FECHA TIEMPO ATENCION').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('strFechaCompromisoAtencion').withTitle('FECHA COMPROMISO ATENCION').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DescripcionTipoEntidad').withTitle('TIPO CLIENTE').notSortable().withOption('width', '1%'),

          DTColumnBuilder.newColumn('strFechaInicioSisego').withTitle('Fecha Inicio Sisego').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('strFechaFinSisego').withTitle('Fecha Fin Sisego').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('DiasGestionSisego').withTitle('Dias Gestion Sisego').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('ObservacionesSisego').withTitle('Observaciones Sisego').notSortable().withOption('width', '1%').withClass('General-heading'),

          DTColumnBuilder.newColumn('strFechaInicioPreventa').withTitle('Fecha Inicio Preventa').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('strFechaFinPreventa').withTitle('Fecha Fin Preventa').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DiasGestionPreventa').withTitle('Dias Gestion Preventa').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ObservacionesPreventa').withTitle('Observaciones Preventa').notSortable().withOption('width', '1%'),

          DTColumnBuilder.newColumn('strFechaInicioGics').withTitle('Fecha Inicio Gics').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('strFechaFinGics').withTitle('Fecha Fin Gics').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('DiasGestionGics').withTitle('Dias Gestion Gics').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('ObservacionesGics').withTitle('Observaciones Gics').notSortable().withOption('width', '1%').withClass('General-heading'),

          DTColumnBuilder.newColumn('strFechaInicioCalidad').withTitle('Fecha Inicio Calidad').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('strFechaFinCalidad').withTitle('Fecha Fin Calidad').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DiasGestionCalidad').withTitle('Dias Gestion Calidad').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ObservacionesCalidad').withTitle('Observaciones Calidad').notSortable().withOption('width', '1%'),

          DTColumnBuilder.newColumn('strFechaInicioFinanzas').withTitle('Fecha Inicio Finanzas').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('strFechaFinFinanzas').withTitle('Fecha Fin Finanzas').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('DiasGestionFinanzas').withTitle('Dias Gestion Finanzas').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('ObservacionesFinanzas').withTitle('Observaciones Finanzas').notSortable().withOption('width', '1%').withClass('General-heading'),

          DTColumnBuilder.newColumn('strFechaInicioPlaneamiento').withTitle('Fecha Inicio Planeamiento').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('strFechaFinPlaneamiento').withTitle('Fecha Fin Planeamiento').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DiasGestionPlaneamiento').withTitle('Dias Gestion Planeamiento').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ObservacionesPlaneamiento').withTitle('Observaciones Planeamiento').notSortable().withOption('width', '1%'),

          DTColumnBuilder.newColumn('strFechaInicioComercial').withTitle('Fecha Inicio Comercial').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('strFechaFinComercial').withTitle('Fecha Fin Comercial').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('DiasGestionComercial').withTitle('Dias Gestion Comercial').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('ObservacionesComercial').withTitle('Observaciones Comercial').notSortable().withOption('width', '1%').withClass('General-heading'),

          DTColumnBuilder.newColumn('strFechaInicioProducto').withTitle('Fecha Inicio Producto').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('strFechaFinProducto').withTitle('Fecha Fin Producto').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DiasGestionProducto').withTitle('Dias Gestion Producto').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ObservacionesProducto').withTitle('Observaciones Producto').notSortable().withOption('width', '1%'),

          DTColumnBuilder.newColumn('strFechaInicioJProyecto').withTitle('Fecha Inicio JProyecto').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('strFechaFinJProyecto').withTitle('Fecha Fin JProyecto').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('DiasGestionJProyecto').withTitle('Dias Gestion JProyecto').notSortable().withOption('width', '1%').withClass('General-heading'),
          DTColumnBuilder.newColumn('ObservacionesJProyecto').withTitle('Observaciones JProyecto').notSortable().withOption('width', '1%').withClass('General-heading'),

          DTColumnBuilder.newColumn('strFechaInicioProgramacionKO').withTitle('Fecha Inicio ProgramacionKO').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('strFechaFinProgramacionKO').withTitle('Fecha Fin ProgramacionKO').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DiasGestionProgramacionKO').withTitle('Dias Gestion ProgramacionKO').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ObservacionesProgramacionKO').withTitle('Observaciones ProgramacionKO').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DiasDemoraOtrasArea').withTitle('Dias de demora total de la propuesta de otras areas').notSortable().withOption('width', '1%').withClass('General-heading')
          
        ];

        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarTipoComplejidad() {
            blockUI.start();
            var maestra = {
                IdEstado: 1,
                IdRelacion: 160
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listComplejidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[EtapaOportunidad] que esten Activos.
        function ObtenerEtapa() {
            blockUI.start();
            var promise = EtapaService.ListarComboEtapas();
            promise.then(function (response) {
                blockUI.stop();
                    if (response == "") {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Etapas", 5);
                    } else {
                        vm.listEtapas = UtilsFactory.AgregarItemSelect(response.data);
                    }
            }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[SegmentoNegocio] que esten Activos.
        function ListarComboSegmentos() {
            blockUI.start();
            var promise = SegmentosNegocioService.ListarComboSegmentoNegocioSimple();
            promise.then(function (response) {
                blockUI.stop();
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Segmentos de Negocio", 5);
                } else {
                    vm.listSegmentosNegocios = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[EstadoCaso] que esten Activos.
        function ListarComboEstadosCaso() {
            blockUI.start();
            var promise = EstadoCasoService.ListarComboEstadoCaso();
            promise.then(function (response) {
                blockUI.stop();
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Estados de Caso", 5);
                } else {
                    vm.listEstadosCaso = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[TipoOportunidad] que esten Activos.
        function ListarComboTipoOportunidad() {
            blockUI.start();
            var promise = TipoOportunidadService.ListarComboTipoOportunidad();
            promise.then(function (response) {
                blockUI.stop();
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Tipos de Oportunidad", 5);
                } else {
                    vm.listTiposOportunidad = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }   
        //Filtro de la  tabla [TRAZABILIDAD].[Recurso] por Descripcion.
        function BuscarDatosPostventaMovil() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withFnServerData(BuscarDatosPostventaMovilPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);

        }
        function BuscarDatosPostventaMovilPaginado(sSource, aoData, fnCallback, oSettings) {
            debugger;
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var request = {
                IdOportunidadSF: vm.IdOportunidadSF,
                DescripcionCliente: vm.DescripcionCliente,
                IdEtapa: vm.IdEtapa,
                IdEstadoCaso: vm.IdEstadoCaso,
                IdSegmentoNegocio: vm.IdSegmentoNegocio,
                IdTipoOportunidad: vm.IdTipoOportunidad,
                IdComplejidad: vm.IdComplejidad,
                CodSisego: vm.CodSisego,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = OportunidadSTService.ListaOportunidadSTSeguimientoPostventaPaginado(request);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListOportunidadSTSeguimientoPostventaDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('scrollCollapse', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('scrollX', true)
                .withOption('paging', false);
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusqueda(data, type, full, meta) {
            var respuesta = "";
            respuesta = respuesta + "<a title='Tiempo de Atencion'  href='" + vm.EnlaceRegistrar + data.IdOportunidadSF + "');'>" + "<span class='glyphicon glyphicon-file' style='color: #00A4B6; margin-left: 1px;'></span></a>";
            return respuesta;
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.IdOportunidadSF = "";
            vm.DescripcionCliente = "";
            vm.IdEtapa = "-1";
            vm.IdEstadoCaso = "-1";
            vm.IdSegmentoNegocio = "-1";
            vm.IdTipoOportunidad = "-1";
            vm.IdComplejidad = "-1";
            vm.CodSisego = "";
            LimpiarGrilla();
        }
    }
})();