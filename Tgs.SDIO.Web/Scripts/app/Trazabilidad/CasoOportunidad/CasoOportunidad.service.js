﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('CasoOportunidadService', CasoOportunidadService);

    CasoOportunidadService.$inject = ['$http', 'URLS'];

    function CasoOportunidadService($http, $urls) {

        var service = {
            ObtenerCasoOportunidadPorId: ObtenerCasoOportunidadPorId,
            ActualizarCasoOportunidadModalPreventa: ActualizarCasoOportunidadModalPreventa
            
        };

        return service;


        function ObtenerCasoOportunidadPorId(caso) {
            return $http({
                url: $urls.ApiTrazabilidad + "CasoOportunidad/ObtenerCasoOportunidadPorId",
                method: "POST",
                data: JSON.stringify(caso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarCasoOportunidadModalPreventa(caso) {
            return $http({
                url: $urls.ApiTrazabilidad + "CasoOportunidad/ActualizarCasoOportunidadModalPreventa",
                method: "POST",
                data: JSON.stringify(caso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();