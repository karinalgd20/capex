﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RptOportunidadesAtendidasAcumuladasBusquedaRecursoService', RptOportunidadesAtendidasAcumuladasBusquedaRecursoService);

    RptOportunidadesAtendidasAcumuladasBusquedaRecursoService.$inject = ['$http', 'URLS'];

    function RptOportunidadesAtendidasAcumuladasBusquedaRecursoService($http, $urls) {

        var service = {
            ModalRptOportunidadesAtendidasAcumuladasBusquedaRecurso: ModalRptOportunidadesAtendidasAcumuladasBusquedaRecurso
            
        };

        return service;
   


        function ModalRptOportunidadesAtendidasAcumuladasBusquedaRecurso() {

            return $http({
                url: $urls.ApiTrazabilidad + "RptOportunidadesAtendidasAcumuladasBusquedaRecurso/Index",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();