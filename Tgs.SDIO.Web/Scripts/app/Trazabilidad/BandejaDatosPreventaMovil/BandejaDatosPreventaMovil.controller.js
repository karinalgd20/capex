﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('BandejaDatosPreventaMovil', BandejaDatosPreventaMovil);
    BandejaDatosPreventaMovil.$inject = ['BandejaDatosPreventaMovilService', 'RegistrarDatosPreventaMovilService', 'CargaExcelDatosPreventaMovilService', 'MaestraService', 'EstadoCasoService', 'TipoOportunidadService', 'EtapaService', 'SegmentosNegocioService', 'OportunidadSTService', 'LineaNegocioService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function BandejaDatosPreventaMovil(BandejaDatosPreventaMovilService, RegistrarDatosPreventaMovilService, CargaExcelDatosPreventaMovilService, MaestraService, EstadoCasoService, TipoOportunidadService, EtapaService, SegmentosNegocioService, OportunidadSTService, LineaNegocioService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.BuscarDatosPreventaMovil = BuscarDatosPreventaMovil;
        vm.EliminarDatosPreventaMovil = EliminarDatosPreventaMovil;
        vm.EnlaceRegistrar = $urls.ApiTrazabilidad + "BandejaObservacionOportunidad/Index/";
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.IdOportunidadSF = "";
        vm.IdCasoSF = "";
        vm.DescripcionCliente = "";
        vm.IdEtapa = "-1";
        vm.IdEstadoCaso = "-1";
        vm.IdSegmentoNegocio = "-1";
        vm.IdTipoOportunidad = "-1";
        vm.Codigo = "-1";
        vm.IdLineaNegocio = "-1";
        LimpiarGrilla();
        ListarTipoComplejidad();
        vm.listComplejidad = [];
        ObtenerEtapa();
        vm.listEtapas = [];
        ListarComboSegmentos();
        vm.listSegmentosNegocios = [];
        ListarComboEstadosCaso();
        vm.listEstadosCaso = [];
        ListarComboTipoOportunidad();
        vm.listTiposOportunidad = [];
        ListarComboLineaNegocio();
        vm.listLineaNegocio = [];
        vm.dtColumns = [
         //38 1%
         //4 otros
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '1%'),
          DTColumnBuilder.newColumn('IdDatosPreventaMovil').withTitle('ID Preventa').notSortable().notVisible(),
          DTColumnBuilder.newColumn('IdOportunidad2').withTitle('ID Oportunidad').notSortable().notVisible(),
          DTColumnBuilder.newColumn('IdCaso2').withTitle('ID Caso').notSortable().notVisible().notVisible(),
          DTColumnBuilder.newColumn('IdOportunidadSF').withTitle('Id de la Oportunidad').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('IdCasoSF').withTitle('Número del Caso').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('strFechaApertura').withTitle('Fecha de Apertura').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DescripcionCliente').withTitle('Nombre Cliente').notSortable().withOption('width', '18%'),         
          DTColumnBuilder.newColumn('strFechaCierre').withTitle('Fecha/Hora Cierre').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DescripcionTipoEntidadCliente').withTitle('Tipo Cliente').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DescripcionComplejidad').withTitle('Complejidad').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DescripcionEtapa').withTitle('Etapa Oportunidad').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DescripcionMotivoOportunidad').withTitle('Motivo Oportunidad').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DescripcionSegmento').withTitle('Segmento Negocio').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DescripcionComercial').withTitle('Comercial').notSortable().withOption('width', '15%'),
          DTColumnBuilder.newColumn('DescripcionPreventa').withTitle('Preventa').notSortable().withOption('width', '15%'),
          DTColumnBuilder.newColumn('DescripcionTipoOportunidad').withTitle('Tipo Oportunidad').notSortable().withOption('width', '15%'),        
          DTColumnBuilder.newColumn('DescripcionOperadorActual').withTitle('OperadorActual').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('LineasActuales').withTitle('LineasActuales').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('MesesContratoActual').withTitle('MesesContratoActual').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('RecurrenteMensualActual').withTitle('RecurrenteMensualActual').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('FCVActual').withTitle('FCVActual').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ArpuServicioActual').withTitle('ArpuServicioActual').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('Lineas').withTitle('Lineas').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('MesesContrato').withTitle('MesesContrato').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('RecurrenteMensualVR').withTitle('RecurrenteMensualVR').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('FCVVR').withTitle('FCVVR').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ArpuServicioVR').withTitle('ArpuServicioVR').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('strFechaPresentacion').withTitle('FechaPresentacion').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('strFechaBuenaPro').withTitle('FechaBuenaPro').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('Observaciones').withTitle('Observaciones').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('MontoCapex').withTitle('MontoCapex').notSortable().withOption('width', '1%'),
          //DTColumnBuilder.newColumn('DescripcionEstadoOportunidad').withTitle('EstadoOportunidad').notSortable().withOption('width', '3%'),
          DTColumnBuilder.newColumn('FCVMovistar').withTitle('Tipo').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ArpuTotalMovistar').withTitle('ArpuTotalMovistar').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('VanVai').withTitle('VanVai').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('MesesRecupero').withTitle('MesesRecupero').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('FCVClaro').withTitle('FCVClaro').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ArpuTotalClaro').withTitle('ArpuTotalClaro').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('FCVEntel').withTitle('FCVEntel').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ArpuTotalEntel').withTitle('ArpuTotalEntel').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('FCVViettel').withTitle('FCVViettel').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ArpuTotalViettel').withTitle('ArpuTotalViettel').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ArpuServicio').withTitle('ArpuServicio').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('ArpuEquipos').withTitle('ArpuEquipos').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('DescripcionModalidadEquipo').withTitle('ModalidadEquipo').notSortable().withOption('width', '3%'),
          DTColumnBuilder.newColumn('CostoPromedioEquipo').withTitle('CostoPromedioEquipo').notSortable().withOption('width', '1%'),
          DTColumnBuilder.newColumn('PorcentajeSubsidio').withTitle('%Subsidio').notSortable().withOption('width', '1%')
        ];

        //****************************************************** Modal Registrar *********************************************************************************

        vm.ModalDatosPreventaMovil = ModalDatosPreventaMovil;

        function ModalDatosPreventaMovil(IdDatosPreventaMovil, IdOportunidad2, IdCaso2) {
            vm.IdDatosPreventaMovil = IdDatosPreventaMovil;
            vm.IdOportunidad = IdOportunidad2;
            vm.IdCaso = IdCaso2;

            var datosPreventa = {
                IdDatosPreventaMovil: IdDatosPreventaMovil,
                IdOportunidad: IdOportunidad2,
                IdCaso: IdCaso2
            };

            var promise = RegistrarDatosPreventaMovilService.ModalDatosPreventaMovil(datosPreventa);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoDatosPreventaMovil").html(content);
                });

                $('#ModalRegistrarDatosPreventaMovil').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        vm.MuestraMensaje = MuestraMensaje;
        function MuestraMensaje(TipoMensaje, Mensaje) { 
            //BuscarDatosPreventaMovil();
            UtilsFactory.Alerta('#divAlertBandeja', TipoMensaje, Mensaje, 5);
        }

        //****************************************************** Modal Cargar Excel *********************************************************************************

        vm.CargarExcelOportunidadesVenta = CargarExcelOportunidadesVenta;

        function CargarExcelOportunidadesVenta() {

            var promise = CargaExcelDatosPreventaMovilService.ModalCargaExcelDatosPreventaMovil();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCargaExcelDatosPreventaMovil").html(content);
                });

                $('#ModalCargaExcelDatosPreventaMovil').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };
        //Lista los IdLineaNegocio y Descripciones de la tabla [Comun].[LineaNegocio] que esten Activos.
        function ListarComboLineaNegocio() {
            blockUI.start();
            var promise = LineaNegocioService.ListarComboLineaNegocioSaleforce();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Lineas de Negocio", 5);
                } else {
                    vm.listLineaNegocio = UtilsFactory.AgregarItemSelect(respuesta);
                    vm.IdLineaNegocio = "-1";
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarTipoComplejidad() {
            blockUI.start();
            var maestra = {
                IdEstado: 1,
                IdRelacion: 160
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listComplejidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[EtapaOportunidad] que esten Activos.
        function ObtenerEtapa() {
            blockUI.start();
            var promise = EtapaService.ListarComboEtapas();
            promise.then(function (response) {
                blockUI.stop();
                    if (response == "") {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Etapas", 5);
                    } else {
                        vm.listEtapas = UtilsFactory.AgregarItemSelect(response.data);
                    }
            }, function (response) {
                blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[SegmentoNegocio] que esten Activos.
        function ListarComboSegmentos() {
            blockUI.start();
            var promise = SegmentosNegocioService.ListarComboSegmentoNegocioSimple();
            promise.then(function (response) {
                blockUI.stop();
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Segmentos de Negocio", 5);
                } else {
                    vm.listSegmentosNegocios = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[EstadoCaso] que esten Activos.
        function ListarComboEstadosCaso() {
            blockUI.start();
            var promise = EstadoCasoService.ListarComboEstadoCaso();
            promise.then(function (response) {
                blockUI.stop();
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Estados de Caso", 5);
                } else {
                    vm.listEstadosCaso = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[TipoOportunidad] que esten Activos.
        function ListarComboTipoOportunidad() {
            blockUI.start();
            var promise = TipoOportunidadService.ListarComboTipoOportunidad();
            promise.then(function (response) {
                blockUI.stop();
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Tipos de Oportunidad", 5);
                } else {
                    vm.listTiposOportunidad = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Eliminacion logica de la tabla [TRAZABILIDAD].[Recurso]
        function EliminarDatosPreventaMovil(IdDatosPreventaMovil) {
            blockUI.start();
            if (confirm('¿Estas seguro que desea eliminar?')) {

                var datosPreventa = {
                    IdDatosPreventaMovil: IdDatosPreventaMovil
                }

                var promise = BandejaDatosPreventaMovilService.EliminarDatosPreventaMovil(datosPreventa);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                        BuscarDatosPreventaMovil()
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                    }
                    modalpopupConfirm.modal('hide');
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            }
            else {
                blockUI.stop();
            }
        }     
        //Filtro de la  tabla [TRAZABILIDAD].[Recurso] por Descripcion.
        function BuscarDatosPreventaMovil() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarDatosPreventaMovilPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);

        }
        function BuscarDatosPreventaMovilPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var recurso = {
                IdOportunidadSF: vm.IdOportunidadSF,
                IdCasoSF: vm.IdCasoSF,
                DescripcionCliente: vm.DescripcionCliente,
                IdLineaNegocio: vm.IdLineaNegocio,
                IdEtapa: vm.IdEtapa,
                IdEstadoCaso: vm.IdEstadoCaso,
                IdSegmentoNegocio: vm.IdSegmentoNegocio,
                IdTipoOportunidad: vm.IdTipoOportunidad,
                Codigo: vm.Codigo,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = OportunidadSTService.ListaOportunidadSTSeguimientoPreventaPaginado(recurso);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListOportunidadSTSeguimientoPreventaDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('scrollCollapse', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('scrollX', true)
                .withOption('paging', false);
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusqueda(data, type, full, meta) {
            var respuesta = "";
            respuesta = respuesta + " <a title='Editar'   ng-click='vm.ModalDatosPreventaMovil(" + data.IdDatosPreventaMovil + ","+data.IdOportunidad2+","+data.IdCaso2+");'>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + " <a title='Eliminar'  onclick='EliminarDatosPreventaMovil(" + data.IdDatosPreventaMovil + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + "<a title='Quiebres'  href='" + vm.EnlaceRegistrar + data.IdOportunidadSF + "');'>" + "<span class='glyphicon glyphicon-file' style='color: #00A4B6; margin-left: 1px;'></span></a>";
            return respuesta;
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.IdOportunidadSF = "";
            vm.IdCasoSF = "";
            vm.DescripcionCliente = "";
            vm.IdEtapa = "-1";
            vm.IdEstadoCaso = "-1";
            vm.IdSegmentoNegocio = "-1";
            vm.IdTipoOportunidad = "-1";
            vm.Codigo = "-1";
            vm.IdLineaNegocio = "-1";
            LimpiarGrilla();
        }
    }
})();