﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('BandejaDatosPreventaMovilService', BandejaDatosPreventaMovilService);

    BandejaDatosPreventaMovilService.$inject = ['$http', 'URLS'];

    function BandejaDatosPreventaMovilService($http, $urls) {

        var service = {
            EliminarDatosPreventaMovil: EliminarDatosPreventaMovil
        };

        return service;

     

        function EliminarDatosPreventaMovil(datosPreventa) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaDatosPreventaMovil/EliminarDatosPreventaMovil",
                method: "POST",
                data: JSON.stringify(datosPreventa)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();