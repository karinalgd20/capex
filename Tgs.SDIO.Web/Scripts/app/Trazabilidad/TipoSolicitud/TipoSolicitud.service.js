﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('TipoSolicitudService', TipoSolicitudService);

    TipoSolicitudService.$inject = ['$http', 'URLS'];

    function TipoSolicitudService($http, $urls) {

        var service = {
            ListarComboTipoSolicitud: ListarComboTipoSolicitud
        };

        return service;

        function ListarComboTipoSolicitud() {
            return $http({
                url: $urls.ApiTrazabilidad + "TipoSolicitud/ListarComboTipoSolicitud",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();