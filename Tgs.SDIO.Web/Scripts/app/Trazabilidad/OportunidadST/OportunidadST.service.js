﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('OportunidadSTService', OportunidadSTService);

    OportunidadSTService.$inject = ['$http', 'URLS'];

    function OportunidadSTService($http, $urls) {

        var service = {
            ListaOportunidadSTModalObservacionPaginado: ListaOportunidadSTModalObservacionPaginado,
            ObtenerOportunidadSTPorId: ObtenerOportunidadSTPorId,
            ListaOportunidadSTModalRecursoPaginado: ListaOportunidadSTModalRecursoPaginado,
            ListaOportunidadSTMasivoPaginado: ListaOportunidadSTMasivoPaginado,
            ListaOportunidadSTSeguimientoPreventaPaginado: ListaOportunidadSTSeguimientoPreventaPaginado,
            ListaOportunidadSTSeguimientoPostventaPaginado: ListaOportunidadSTSeguimientoPostventaPaginado,
            ActualizarOportunidadSTModalPreventa: ActualizarOportunidadSTModalPreventa
            
        };

        return service;

        function ListaOportunidadSTModalObservacionPaginado(oportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/ListaOportunidadSTModalObservacionPaginado",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerOportunidadSTPorId(oportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/ObtenerOportunidadSTPorId",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaOportunidadSTModalRecursoPaginado(oportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/ListaOportunidadSTModalRecursoPaginado",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaOportunidadSTMasivoPaginado(oportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/ListaOportunidadSTMasivoPaginado",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaOportunidadSTSeguimientoPreventaPaginado(oportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/ListaOportunidadSTSeguimientoPreventaPaginado",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaOportunidadSTSeguimientoPostventaPaginado(request) {
            debugger;
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/ListaOportunidadSTSeguimientoPostventaPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarOportunidadSTModalPreventa(oportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/ActualizarOportunidadSTModalPreventa",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();