﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RegistrarDatosPreventaMovilService', RegistrarDatosPreventaMovilService);

    RegistrarDatosPreventaMovilService.$inject = ['$http', 'URLS'];

    function RegistrarDatosPreventaMovilService($http, $urls) {

        var service = {
            ActualizarDatosPreventaMovilModalPreventa: ActualizarDatosPreventaMovilModalPreventa,
            RegistrarDatosPreventaMovil: RegistrarDatosPreventaMovil,
            ModalDatosPreventaMovil: ModalDatosPreventaMovil

        };

        return service;


        function ActualizarDatosPreventaMovilModalPreventa(datosPreventa) {
            return $http({
                url: $urls.ApiTrazabilidad + "RegistrarDatosPreventaMovil/ActualizarDatosPreventaMovilModalPreventa",
                method: "POST",
                data: JSON.stringify(datosPreventa)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


        function RegistrarDatosPreventaMovil(datosPreventa) {
            return $http({
                url: $urls.ApiTrazabilidad + "RegistrarDatosPreventaMovil/RegistrarDatosPreventaMovil",
                method: "POST",
                data: JSON.stringify(datosPreventa)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        

        function ModalDatosPreventaMovil(datosPreventa) {
            return $http({
                url: $urls.ApiTrazabilidad + "RegistrarDatosPreventaMovil/Index",
                method: "POST",
                data: JSON.stringify(datosPreventa)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();