﻿(function () {
    'use strict'

    angular
    .module('app.Trazabilidad')
    .controller('RegistrarDatosPreventaMovil', RegistrarDatosPreventaMovil);

    RegistrarDatosPreventaMovil.$inject = ['RegistrarDatosPreventaMovilService', 'MaestraService', 'EstadoCasoService', 'SegmentosNegocioService', 'TipoOportunidadService', 'EtapaService', 'FuncionPropietarioService', 'SectorService', 'MotivoOportunidadService', 'DatosPreventaMovilService', 'OportunidadSTService', 'CasoOportunidadService', 'ClienteService', 'TipoSolicitudService', 'LineaNegocioService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarDatosPreventaMovil(RegistrarDatosPreventaMovilService, MaestraService, EstadoCasoService, SegmentosNegocioService, TipoOportunidadService, EtapaService, FuncionPropietarioService, SectorService, MotivoOportunidadService, DatosPreventaMovilService, OportunidadSTService, CasoOportunidadService, ClienteService, TipoSolicitudService, LineaNegocioService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;


        vm.CargaModalDatosPreventaMovil = CargaModalDatosPreventaMovil;
        vm.ActualizarDatosPreventaMovil = ActualizarDatosPreventaMovil;
        vm.ObtenerDatosPreventaMovil = ObtenerDatosPreventaMovil;

        vm.Calculo = Calculo;

        vm.listTiposOportunidad = [];
        vm.listFuncionesProp = [];
        vm.listEtapas = [];
        vm.listMotivos = [];
        vm.listSectores = [];
        vm.listSegmentosNegocios = [];
        vm.listEstadosCaso = [];
        vm.listOperadores = [];
        vm.listEstadoOportunidad = [];

        vm.CostoPromedioEquipo = 0;
        vm.ArpuEquipos = 0;
        vm.FCVViettel = 0;
        vm.MesesContrato = 0;
        vm.Lineas = 0;
        vm.FCVEntel = 0;
        vm.FCVClaro = 0;
        vm.FCVMovistar = 0;
        vm.RecurrenteMensualVR = 0;
        vm.RecurrenteMensualActual = 0;
        vm.LineasActuales = 0;
        vm.MesesContratoActual = 0;

        vm.FCVActual = 0;
        vm.PorcentajeSubsidio = 0;
        vm.ArpuTotalViettel = 0;
        vm.ArpuTotalEntel = 0;
        vm.ArpuTotalClaro = 0;
        vm.ArpuTotalMovistar = 0;;
        vm.ArpuServicioVR = 0;
        vm.ArpuServicioActual = 0;
        vm.FCVVR = 0;
        vm.MontoCapex = 0;
        vm.VanVai = 0;
        vm.MesesRecupero = 0;
        vm.ArpuServicio = 0;

        function Calculo() {
            vm.FCVActual = vm.RecurrenteMensualActual * vm.MesesContratoActual;
            vm.PorcentajeSubsidio = 1 - (vm.CostoPromedioEquipo / vm.ArpuEquipos);
            vm.ArpuTotalViettel = (vm.FCVViettel / vm.MesesContrato) / vm.Lineas;
            vm.ArpuTotalEntel = (vm.FCVEntel / vm.MesesContrato) / vm.Lineas;
            vm.ArpuTotalClaro = (vm.FCVClaro / vm.MesesContrato) / vm.Lineas;
            vm.ArpuTotalMovistar = (vm.FCVMovistar / vm.MesesContrato) / vm.Lineas;
            vm.ArpuServicioVR = vm.RecurrenteMensualVR / vm.Lineas;
            vm.ArpuServicioActual = vm.RecurrenteMensualActual / vm.LineasActuales;
        }

        vm.DescripcionOportunidad = "";
        vm.DescripcionCliente = "";
        vm.NumeroIdentificadorFiscal = "";
        vm.IdCasoSF = "";
        vm.IdCaso = "";
        vm.IdOportunidad = "";
        vm.IdCliente = "";
        vm.Asunto = "";
        vm.FechaApertura = "";
        vm.FechaCierre = "";
        vm.DescripcionRecurso = "";  

        vm.FechaPresentacion = "";
        vm.FechaBuenaPro = "";
        vm.IdTipoOportunidad = "-1";
        vm.IdEtapa = "-1";
        vm.IdMotivoOportunidad = "-1";
        vm.IdSector = "-1";
        vm.IdSegmentoNegocio = "-1";
        vm.IdEstadoCaso = "-1";
        vm.IdTipoSolicitud = "-1"
        vm.IdOperadorActual = "-1";
        vm.IdEstadoOportunidad = "-1";
        vm.IdModalidadEquipo = "-1";
        vm.IdLineaNegocio = "-1";


        ListarComboEstadosCaso();
        ListarComboSegmentos();
        ListarComboTipoOportunidad();
        ObtenerFuncionesProp();
        ListarOperadores();
        ObtenerEtapa();
        ListarEstadoOportunidad();
        ObtenerSectores();
        ObtenerMotivoOportunidad();
        CargaModalDatosPreventaMovil();
        ObtenerTipoSolicitud();
        ListarModalidadEquipo();
        ListarComboLineaNegocio();

        function CargaModalDatosPreventaMovil() {
            vm.IdDatosPreventaMovil = $scope.$parent.vm.IdDatosPreventaMovil;
            vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;
            vm.IdCaso = $scope.$parent.vm.IdCaso;
            ObtenerDatosPreventaMovil();
        }
        //Lista los IdLineaNegocio y Descripciones de la tabla [Comun].[LineaNegocio] que esten Activos.
        function ListarComboLineaNegocio() {
            blockUI.start();
            var promise = LineaNegocioService.ListarComboLineaNegocioSaleforce();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Lineas de Negocio", 5);
                } else {
                    vm.listLineaNegocio = UtilsFactory.AgregarItemSelect(respuesta);
                    vm.IdLineaNegocio = "-1";
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[MotivoOportunidad] que esten Activos.
        function ObtenerTipoSolicitud() {
            var promise = TipoSolicitudService.ListarComboTipoSolicitud();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlertRegistro', 'danger', "No Existen Tipo Solicitud", 5);
                } else {
                    vm.listTipoSolicitud = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlertRegistro', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarEstadoOportunidad() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 11
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listEstadoOportunidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarOperadores() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 195
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listOperadores = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[MotivoOportunidad] que esten Activos.
        function ObtenerMotivoOportunidad() {
            var promise = MotivoOportunidadService.ListarComboMotivoOportunidad();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlertRegistro', 'danger', "No Existen Motivos de Oportunidad", 5);
                } else {
                    vm.listMotivos = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlertRegistro', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[FuncionPropietario] que esten Activos.
        function ObtenerSectores() {
            var promise = SectorService.ListarComboSector();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlertRegistro', 'danger', "No Existen Sectores", 5);
                } else {
                    vm.listSectores = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlertRegistro', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[FuncionPropietario] que esten Activos.
        function ObtenerFuncionesProp() {
            var promise = FuncionPropietarioService.ListarComboFuncionPropietario();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlertRegistro', 'danger', "No Existen Funciones de Propietarios", 5);
                } else {
                    vm.listFuncionesProp = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlertRegistro', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[EtapaOportunidad] que esten Activos.
        function ObtenerEtapa() {
            var promise = EtapaService.ListarComboEtapas();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlertRegistro', 'danger', "No Existen Etapas", 5);
                } else {
                    vm.listEtapas = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlertRegistro', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[TipoOportunidad] que esten Activos.
        function ListarComboTipoOportunidad() {
            var promise = TipoOportunidadService.ListarComboTipoOportunidad();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlertRegistro', 'danger', "No Existen Tipos de Oportunidad", 5);
                } else {
                    vm.listTiposOportunidad = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlertRegistro', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[SegmentoNegocio] que esten Activos.
        function ListarComboSegmentos() {
            var promise = SegmentosNegocioService.ListarComboSegmentoNegocioSimple();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlertRegistro', 'danger', "No Existen Segmentos de Negocio", 5);
                } else {
                    vm.listSegmentosNegocios = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlertRegistro', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[EstadoCaso] que esten Activos.
        function ListarComboEstadosCaso() {
            var promise = EstadoCasoService.ListarComboEstadoCaso();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlertRegistro', 'danger', "No Existen Estados de Caso", 5);
                } else {
                    vm.listEstadosCaso = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlertRegistro', 'danger', MensajesUI.DatosError, 5);
            });
        }

        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarModalidadEquipo() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 201
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listModalidadEquipo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function ObtenerDatosPreventaMovil() {
            blockUI.start();

            var datosPreventa = {
                IdDatosPreventaMovil: vm.IdDatosPreventaMovil
            }
            var promise = DatosPreventaMovilService.ObtenerDatosPreventaMovilPorId(datosPreventa);

            promise.then(function (resultado) {
                debugger;
                blockUI.stop();
                var Respuesta = resultado.data;
                var dateParts1 = Respuesta.strFechaPresentacion.split("/");
                var FechaPresentacion = new Date(dateParts1[2], dateParts1[1] - 1, dateParts1[0]);
                var dateParts2 = Respuesta.strFechaBuenaPro.split("/");
                var FechaBuenaPro = new Date(dateParts2[2], dateParts2[1] - 1, dateParts2[0]);
                if (Respuesta.IdOperadorActual == null) {
                    vm.IdOperadorActual = -1;
                } else {
                    vm.IdOperadorActual = Respuesta.IdOperadorActual;
                }
                vm.FCVActual = Respuesta.FCVActual;
                vm.PorcentajeSubsidio = Respuesta.PorcentajeSubsidio;
                vm.ArpuTotalViettel = Respuesta.ArpuTotalViettel;
                vm.ArpuTotalEntel = Respuesta.ArpuTotalEntel;
                vm.ArpuTotalClaro = Respuesta.ArpuTotalClaro;
                vm.ArpuTotalMovistar = Respuesta.ArpuTotalMovistar;
                vm.ArpuServicioVR = Respuesta.ArpuServicioVR;
                vm.ArpuServicioActual = Respuesta.ArpuServicioActual;
                vm.IdOportunidad = Respuesta.IdOportunidad;
                vm.LineasActuales = Respuesta.LineasActuales;
                vm.MesesContratoActual = Respuesta.MesesContratoActual;
                vm.Lineas = Respuesta.Lineas;
                vm.RecurrenteMensualActual = Respuesta.RecurrenteMensualActual;
                vm.MesesContrato = Respuesta.MesesContrato;
                vm.RecurrenteMensualVR = Respuesta.RecurrenteMensualVR;
                vm.FCVVR = Respuesta.FCVVR;
                vm.FechaPresentacion = FechaPresentacion;
                vm.FechaBuenaPro = FechaBuenaPro;
                vm.MontoCapex = Respuesta.MontoCapex;
                if (Respuesta.IdEstadoOportunidad == null) {
                    vm.IdEstadoOportunidad = -1;
                } else {
                    vm.IdEstadoOportunidad = Respuesta.IdEstadoOportunidad;
                }
                vm.Observaciones = Respuesta.Observaciones;
                vm.FCVMovistar = Respuesta.FCVMovistar;
                vm.VanVai = Respuesta.VanVai;
                vm.MesesRecupero = Respuesta.MesesRecupero;
                vm.FCVClaro = Respuesta.FCVClaro;
                vm.FCVEntel = Respuesta.FCVEntel;
                vm.FCVViettel = Respuesta.FCVViettel;
                vm.ArpuServicio = Respuesta.ArpuServicio;
                vm.ArpuEquipos = Respuesta.ArpuEquipos;
                if (Respuesta.ModalidadEquipo == null) {
                    vm.IdModalidadEquipo = -1;
                } else {
                    vm.IdModalidadEquipo = Respuesta.ModalidadEquipo;
                }
                vm.CostoPromedioEquipo = Respuesta.CostoPromedioEquipo;
            }, function (response) {
                blockUI.stop();

            });
        }
        ObtenerDatosOportunidad(vm.IdOportunidad);
        function ObtenerDatosOportunidad(IdOportunidad) {

            blockUI.start();

            var oportunidad = {
                IdOportunidad: IdOportunidad
            }

            var promise = OportunidadSTService.ObtenerOportunidadSTPorId(oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();
                debugger;
                var Respuesta = resultado.data;
                vm.IdOportunidadSF = Respuesta.IdOportunidadSF;
                vm.IdCliente = Respuesta.IdCliente;
                vm.DescripcionOportunidad = Respuesta.Descripcion;
                if (Respuesta.IdTipoOportunidad == null) {
                    vm.IdTipoOportunidad = -1;
                } else {
                    vm.IdTipoOportunidad = Respuesta.IdTipoOportunidad;
                }
                vm.IdEtapa = Respuesta.IdEtapa;
                if (Respuesta.IdMotivoOportunidad == null) {
                    vm.IdMotivoOportunidad = -1;
                } else {
                    vm.IdMotivoOportunidad = Respuesta.IdMotivoOportunidad;
                }
                if (Respuesta.IdSector == null) {
                    vm.IdSector = -1;
                } else {
                    vm.IdSector = Respuesta.IdSector;
                }
                if (Respuesta.IdSegmentoNegocio == null) {
                    vm.IdSegmentoNegocio = -1;
                } else {
                    vm.IdSegmentoNegocio = Respuesta.IdSegmentoNegocio;
                }

                if (Respuesta.IdLineaNegocio == null) {
                    vm.IdLineaNegocio = -1;
                } else {
                    vm.IdLineaNegocio = Respuesta.IdLineaNegocio;
                }

                if (Respuesta.IdCliente != null && Respuesta.IdCliente != 0) {
                    ObtenerDatosCliente(Respuesta.IdCliente);
                }
            }, function (response) {
                blockUI.stop();

            });
        }


        ObtenerDatosCaso(vm.IdCaso);
        function ObtenerDatosCaso(IdCaso) {
            blockUI.start();

            var caso = {
                IdCaso: IdCaso
            }

            var promise = CasoOportunidadService.ObtenerCasoOportunidadPorId(caso);

            promise.then(function (resultado) {
                debugger;
                blockUI.stop();
                var Respuesta = resultado.data;
                var dateParts1 = Respuesta.strFechaApertura.split("/");
                var FechaApertura = new Date(dateParts1[2], dateParts1[1] - 1, dateParts1[0]);
                var dateParts2 = Respuesta.strFechaCierre.split("/");
                var FechaCierre = new Date(dateParts2[2], dateParts2[1] - 1, dateParts2[0]);
                vm.IdCasoSF = Respuesta.IdCasoSF;
                if (Respuesta.IdEstadoCaso == null) {
                    vm.IdEstadoCaso = -1;
                } else {
                    vm.IdEstadoCaso = Respuesta.IdEstadoCaso;
                }
                vm.IdCaso = Respuesta.IdCaso;
                vm.Asunto = Respuesta.Asunto;
                if (Respuesta.IdTipoSolicitud == null) {
                    vm.IdTipoSolicitud = -1;
                } else {
                    vm.IdTipoSolicitud = Respuesta.IdTipoSolicitud;
                }
                vm.FechaApertura = FechaApertura;
                vm.FechaCierre = FechaCierre;
                vm.DescripcionRecurso = Respuesta.Descripcion;
               

            }, function (response) {
                blockUI.stop();

            });
        }

        function ObtenerDatosCliente(IdCliente) {
            blockUI.start();
            var cliente = {
                IdCliente: IdCliente,
                IdEstado: 1
            }
            var promise = ClienteService.ObtenerCliente(cliente);
            promise.then(function (resultado) {
                debugger;
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.DescripcionCliente = Respuesta.Descripcion;
                vm.NumeroIdentificadorFiscal = Respuesta.NumeroIdentificadorFiscal;

            }, function (response) {
                blockUI.stop();

            });
        }

        function ActualizarDatosPreventaMovil() {
            var IdEstadoOportunidad = null;
            var IdOperadorActual = null;
            var IdEstadoCaso = null;
            var IdTipoSolicitud = null;
            var IdTipoOportunidad = null;
            var IdMotivoOportunidad = null;
            var IdSector = null;
            var IdSegmentoNegocio = null;
            var ModalidadEquipo = null;
            var IdLineaNegocio = null;

            if (vm.IdLineaNegocio != "-1") {
                IdLineaNegocio = vm.IdLineaNegocio;
            }
            if (vm.IdEstadoCaso != "-1") {
                IdEstadoCaso = vm.IdEstadoCaso;
            }
            if (vm.IdTipoSolicitud != "-1") {
                IdTipoSolicitud = vm.IdTipoSolicitud;
            }
            if (vm.IdTipoOportunidad != "-1") {
                IdTipoOportunidad = vm.IdTipoOportunidad;
            }
            if (vm.IdMotivoOportunidad != "-1") {
                IdMotivoOportunidad = vm.IdMotivoOportunidad;
            }
            if (vm.IdSector != "-1") {
                IdSector = vm.IdSector;
            }
            if (vm.IdSegmentoNegocio != "-1") {
                IdSegmentoNegocio = vm.IdSegmentoNegocio;
            }
            if (vm.IdEstadoOportunidad != "-1") {
                IdEstadoOportunidad = vm.IdEstadoOportunidad;
            }
            if (vm.IdOperadorActual != "-1") {
                IdOperadorActual = vm.IdOperadorActual;
            }
            if (vm.IdModalidadEquipo != "-1") {
                ModalidadEquipo = vm.IdModalidadEquipo;
            }
            var datosPreventa = {
                IdDatosPreventaMovil: vm.IdDatosPreventaMovil,
                IdOportunidad: vm.IdOportunidad,
                IdCaso: vm.IdCaso,
                IdOperadorActual: IdOperadorActual,
                LineasActuales: vm.LineasActuales,
                MesesContratoActual: vm.MesesContratoActual,
                RecurrenteMensualActual: vm.RecurrenteMensualActual,

                FCVActual: vm.RecurrenteMensualActual * vm.MesesContratoActual,
                ArpuServicioActual: vm.RecurrenteMensualActual / vm.LineasActuales,

                Lineas: vm.Lineas,
                MesesContrato: vm.MesesContrato,
                RecurrenteMensualVR: vm.RecurrenteMensualVR,
                FCVVR: vm.FCVVR,
                ArpuServicioVR: vm.RecurrenteMensualVR / vm.Lineas,

                FechaPresentacion: vm.FechaPresentacion,
                FechaBuenaPro: vm.FechaBuenaPro,
                Observaciones: vm.Observaciones,
                MontoCapex: vm.MontoCapex,
                IdEstadoOportunidad: IdEstadoOportunidad,
                FCVMovistar: vm.FCVMovistar,
                ArpuTotalMovistar: (vm.FCVMovistar / vm.MesesContrato) / vm.Lineas,

                VanVai: vm.VanVai,
                MesesRecupero: vm.MesesRecupero,
                FCVClaro: vm.FCVClaro,
                ArpuTotalClaro: (vm.FCVClaro / vm.MesesContrato) / vm.Lineas,

                FCVEntel: vm.FCVEntel,
                ArpuTotalEntel: (vm.FCVEntel / vm.MesesContrato) / vm.Lineas,

                FCVViettel: vm.FCVViettel,
                ArpuTotalViettel: (vm.FCVViettel / vm.MesesContrato) / vm.Lineas,

                ArpuServicio: vm.ArpuServicio,
                ArpuEquipos: vm.ArpuEquipos,
                ModalidadEquipo: ModalidadEquipo,
                CostoPromedioEquipo: vm.CostoPromedioEquipo,
                PorcentajeSubsidio: 1 - (vm.CostoPromedioEquipo / vm.ArpuEquipos)
            }

            var oportunidad = {
                IdOportunidad: vm.IdOportunidad,
                IdOportunidadSF: vm.IdOportunidadSF,
                DescripcionOportunidad: vm.DescripcionOportunidad,
                IdTipoOportunidad: IdTipoOportunidad,
                IdMotivoOportunidad: IdMotivoOportunidad,
                IdSector: IdSector,
                IdSegmentoNegocio: IdSegmentoNegocio,
                IdEtapa: vm.IdEtapa,
                IdLineaNegocio: IdLineaNegocio
            }

            var caso = {
                IdCaso: vm.IdCaso,
                IdCasoSF: vm.IdCasoSF,
                Asunto: vm.Asunto,
                IdTipoSolicitud: IdTipoSolicitud,
                IdEstadoCaso: IdEstadoCaso,
                FechaApertura: vm.FechaApertura,
                FechaCierre: vm.FechaCierre               
            }

            var cliente = {
                IdCliente: vm.IdCliente,
                Descripcion: vm.DescripcionCliente,
                NumeroIdentificadorFiscal: vm.NumeroIdentificadorFiscal,
                IdEstado: 1
            }

            var mensaje = ValidarCampos();
            debugger;
            if (mensaje == "") {
                blockUI.start();
                var promisePreventa = (vm.IdDatosPreventaMovil > 0) ? RegistrarDatosPreventaMovilService.ActualizarDatosPreventaMovilModalPreventa(datosPreventa) : RegistrarDatosPreventaMovilService.RegistrarDatosPreventaMovil(datosPreventa);
       
                    promisePreventa.then(function (resultadoPreventa) {
                    var RespuestaPreventa = resultadoPreventa.data;
                    if (RespuestaPreventa.TipoRespuesta == 0) {
                       
                                        $('#ModalRegistrarDatosPreventaMovil').modal('hide');
                                        blockUI.stop();
                                        $scope.$parent.vm.MuestraMensaje("success", "Datos Registrados Correctamente");
                                        $scope.$parent.vm.BuscarDatosPreventaMovil();
                    } else {
                        blockUI.stop();
                        $scope.$parent.vm.MuestraMensaje("danger", "No se pudo registrar la Preventa");
                    }

                }, function (response) {
                    UtilsFactory.Alerta('#divAlertRegistro', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });

                        var promiseOportunidad = OportunidadSTService.ActualizarOportunidadSTModalPreventa(oportunidad);
                        promiseOportunidad.then(function (resultadoOportunidad) {
                            var RespuestaOportunidad = resultadoOportunidad.data;
                            if (RespuestaOportunidad.TipoRespuesta != 0) {
                                $scope.$parent.vm.MuestraMensaje("danger", "No se pudo Actualizar la Oportunidad.");
                            } 
                        });

                        var promiseCaso = CasoOportunidadService.ActualizarCasoOportunidadModalPreventa(caso);
                        promiseCaso.then(function (resultadoCaso) {
                            var RespuestaCaso = resultadoCaso.data;
                            if (RespuestaCaso.TipoRespuesta != 0) {
                                $scope.$parent.vm.MuestraMensaje("danger", "No se pudo Actualizar la Caso.");
                            } 
                        });

            } else {
                UtilsFactory.Alerta('#divAlertRegistro', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }


        function ValidarCampos() {

            var mensaje = "";

            return mensaje;
        }

        function DesaparecerEfectosDeValidacion() {

        }
    }
})();








