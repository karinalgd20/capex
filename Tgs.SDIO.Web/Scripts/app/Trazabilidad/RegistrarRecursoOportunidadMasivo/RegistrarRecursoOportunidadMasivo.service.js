﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RegistrarRecursoOportunidadMasivoService', RegistrarRecursoOportunidadMasivoService);

    RegistrarRecursoOportunidadMasivoService.$inject = ['$http', 'URLS'];

    function RegistrarRecursoOportunidadMasivoService($http, $urls) {

        var service = {
            RegistrarRecursoOportunidadMasivo: RegistrarRecursoOportunidadMasivo
        };

        return service;

        function RegistrarRecursoOportunidadMasivo(recursoOportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "RegistrarRecursoOportunidadMasivo/RegistrarRecursoOportunidadMasivo",
                method: "POST",
                data: JSON.stringify(recursoOportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();