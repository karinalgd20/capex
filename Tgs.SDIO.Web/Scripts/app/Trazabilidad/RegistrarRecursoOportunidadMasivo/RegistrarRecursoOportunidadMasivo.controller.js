﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RegistrarRecursoOportunidadMasivo', RegistrarRecursoOportunidadMasivo);
    RegistrarRecursoOportunidadMasivo.$inject = ['RegistrarRecursoOportunidadMasivoService', 'OportunidadSTService', 'BandejaRecursoService', 'RolRecursoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$templateCache'];
    function RegistrarRecursoOportunidadMasivo(RegistrarRecursoOportunidadMasivoService, OportunidadSTService, BandejaRecursoService, RolRecursoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $templateCache) {
        var vm = this;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.RegistraRecursoOportunidadMasivo = RegistraRecursoOportunidadMasivo;
        vm.EnlaceBandeja = $urls.ApiTrazabilidad + "BandejaRecursoOportunidad/Index/";
        debugger;
        vm.IdRol = "-1";
        vm.FInicioAsignacion = "";
        vm.FFinAsignacion = "";
        ListarComboRolRecurso();
        //Lista los IdFase y Descripciones de la tabla [TRAZABILIDAD].[RolRecurso] que esten Activos.
        function ListarComboRolRecurso() {
            blockUI.start();
            var promise = RolRecursoService.ListarComboRolRecurso();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Roles", 5);
                } else {
                    vm.listRolRecurso = UtilsFactory.AgregarItemSelect(respuesta);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
          //Limpia los controles del formulacion a sus valores por defecto.
         function LimpiarCampos() {
             vm.IdRol = "-1";
             vm.FInicioAsignacion = "";
             vm.FFinAsignacion = "";         
         }

         function ObtenerIdSeleccionado(dato) {
             var array = [];
             for (var i in dato) {
                 if(dato[i]==true){
                     array.push({ "id": i});
                 }
             }
             return array;
        }

        //Registra en  la tabla [TRAZABILIDAD].[RecursoOportunidad]
         function RegistraRecursoOportunidadMasivo() {            
             var recursoOportunidad = {                
                 ListOportunidadSelect: ObtenerIdSeleccionado(vm.selectedOportunidad),
                 ListRecursoSelect: ObtenerIdSeleccionado(vm.selectedRecurso),
                 IdRol: vm.IdRol,
                 FInicioAsignacion: vm.FInicioAsignacion,
                 FFinAsignacion: vm.FFinAsignacion
            };
             debugger;
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                debugger;
                var promise = RegistrarRecursoOportunidadMasivoService.RegistrarRecursoOportunidadMasivo(recursoOportunidad);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        LimpiarCampos();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar.", 5);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            }
            else {
                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";

            if ($.trim(vm.FInicioAsignacion) == "") {
                mensaje = mensaje + "Ingrese Fecha de Inicio de Asignación" + '<br/>';
                UtilsFactory.InputBorderColor('#FInicioAsignacion', 'Rojo');
            }
            if ($.trim(vm.FFinAsignacion) == "") {
                mensaje = mensaje + "Ingrese Fecha de Fin de Asignación" + '<br/>';
                UtilsFactory.InputBorderColor('#FFinAsignacion', 'Rojo');
            }
            if ($.trim(vm.IdRol) == "-1") {
                mensaje = mensaje + "Seleccione Rol de Recurso" + '<br/>';
                UtilsFactory.InputBorderColor('#IdRol', 'Rojo');
            }

            if (JSON.stringify(vm.selectedOportunidad).indexOf("true") == -1) {
                mensaje = mensaje + "Seleccione Oportunidad" + '<br/>';
                UtilsFactory.InputBorderColor('#gridOportunidad', 'Rojo');
            }
            if (JSON.stringify(vm.selectedRecurso).indexOf("true") == -1) {
                mensaje = mensaje + "Seleccione Recurso" + '<br/>';
                UtilsFactory.InputBorderColor('#gridRecurso', 'Rojo');
            }
            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#FInicioAsignacion', 'Ninguno');
            UtilsFactory.InputBorderColor('#FFinAsignacion', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdRol', 'Ninguno');
            UtilsFactory.InputBorderColor('#gridOportunidad', 'Ninguno');
            UtilsFactory.InputBorderColor('#gridRecurso', 'Ninguno');
        }

        //******************************************************** Grilla Oportunidad *********************************************************************
        vm.BuscarOportunidad = BuscarOportunidad;
        vm.LimpiarFiltrosOportunidad = LimpiarFiltrosOportunidad;
        vm.Nombre = "";
        LimpiarGrillaOportunidad();
        vm.IdOportunidad = "";
        vm.selectedOportunidad = {};
        vm.selectAllOportunidad = false;
        vm.toggleAllOportunidad = toggleAllOportunidad;
        vm.toggleOneOportunidad = toggleOneOportunidad;
        var titleHtmlOportunidad = '<input type="checkbox" ng-model="vm.selectAllOportunidad" ng-click="vm.toggleAllOportunidad(vm.selectAllOportunidad, vm.selectedOportunidad)">';

        vm.dtColumnsOportunidad = [
          DTColumnBuilder.newColumn(null).withTitle(titleHtmlOportunidad)
            .notSortable().withOption('width', '1%')
            .renderWith(function (data, type, full, meta) {
                vm.selectedOportunidad[full.IdOportunidad] = false;

                return '<input type="checkbox" ng-model="vm.selectedOportunidad[\'' + data.IdOportunidad + '\']" ng-click="vm.toggleOneOportunidad(vm.selectedOportunidad)">';
            }),
          DTColumnBuilder.newColumn('IdOportunidad').withTitle('ID').notSortable().withOption('width', '4%'),
          DTColumnBuilder.newColumn('IdOportunidadSF').withTitle('ID  Oportunidad').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('DescripcionOportunidad').withTitle('Nombre Oportunidad').notSortable().withOption('width', '19%'),
          DTColumnBuilder.newColumn('IdCasoSF').withTitle('N° Caso').notSortable().withOption('width', '6%'),
          DTColumnBuilder.newColumn('DescripcionCliente').withTitle('Nombre Cliente').notSortable().withOption('width', '32%'),
          DTColumnBuilder.newColumn('PorcentajeCierre').withTitle('% Cierre').notSortable().withOption('width', '6%'),
          DTColumnBuilder.newColumn('PorcentajeCheckList').withTitle('% Check List').notSortable().withOption('width', '7%'),
          DTColumnBuilder.newColumn('DescripcionEstadoCaso').withTitle('Estado Caso').notSortable().withOption('width', '7%'),
          DTColumnBuilder.newColumn('DescripcionEtapa').withTitle('Etapa').notSortable().withOption('width', '8%')
        ];

        function toggleAllOportunidad(selectAllOportunidad, selectedOportunidadItems) {
            for (var IdOportunidad in selectedOportunidadItems) {
                if (selectedOportunidadItems.hasOwnProperty(IdOportunidad)) {
                    selectedOportunidadItems[IdOportunidad] = selectAllOportunidad;
                }
            }
        }

        function toggleOneOportunidad(selectedOportunidadItems) {
            for (var IdOportunidad in selectedOportunidadItems) {
                if (selectedOportunidadItems.hasOwnProperty(IdOportunidad)) {
                    if (!selectedOportunidadItems[IdOportunidad]) {
                        vm.selectAllOportunidad = false;
                        return;
                    }
                }
            }
            vm.selectAllOportunidad = true;
        }


        //Filtro de la  tabla [TRAZABILIDAD].[Recurso] por Descripcion.
        function BuscarOportunidad() {
            blockUI.start();
            LimpiarGrillaOportunidad();
            $timeout(function () {
                vm.dtOptionsOportunidad = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarOportunidadSTPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('headerCallback', function (header) {

                    $compile.headerCompiled = true;
                    $compile(angular.element(header).contents())($scope);
                });

            }, 500);
        }
        function BuscarOportunidadSTPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var oportunidad = {
                DescripcionOportunidad: vm.DescripcionOportunidad,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = OportunidadSTService.ListaOportunidadSTMasivoPaginado(oportunidad);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListOportunidadSTDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrillaOportunidad();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrillaOportunidad() {
            vm.dtOptionsOportunidad = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltrosOportunidad() {
            vm.DescripcionOportunidad = "";
            LimpiarGrillaOportunidad();
        }
        //******************************************************** Grilla Oportunidad *********************************************************************
        //******************************************************** Grilla Recurso *********************************************************************
        vm.BuscarRecurso = BuscarRecurso;
        vm.LimpiarFiltrosRecurso = LimpiarFiltrosRecurso;
        vm.Nombre = "";
        LimpiarGrillaRecurso();
        vm.IdRecurso = "";
        vm.selectedRecurso = {};
        vm.selectAllRecurso = false;
        vm.toggleAllRecurso = toggleAllRecurso;
        vm.toggleOneRecurso = toggleOneRecurso;
        var titleHtmlRecurso = '<input type="checkbox" ng-model="vm.selectAllRecurso" ng-click="vm.toggleAllRecurso(vm.selectAllRecurso, vm.selectedRecurso)">';

        vm.dtColumnsRecurso = [
                    DTColumnBuilder.newColumn(null).withTitle(titleHtmlRecurso)
            .notSortable().withOption('width', '3%')
            .renderWith(function (data, type, full, meta) {
                vm.selectedRecurso[full.IdRecurso] = false;
                return '<input type="checkbox" ng-model="vm.selectedRecurso[\'' + data.IdRecurso + '\']" ng-click="vm.toggleOneRecurso(vm.selectedRecurso)">';
            }),
          DTColumnBuilder.newColumn('IdRecurso').withTitle('Código').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Nombre').withTitle('Nombre Recurso').notSortable().withOption('width', '50%'),
          DTColumnBuilder.newColumn('UserNameSF').withTitle('User Name SF').notSortable().withOption('width', '35%')
        ];

        function toggleAllRecurso(selectAllRecurso, selectedRecursoItems) {
            for (var IdOportunidad in selectedRecursoItems) {
                if (selectedRecursoItems.hasOwnProperty(IdOportunidad)) {
                    selectedRecursoItems[IdOportunidad] = selectAllRecurso;
                }
            }
        }

        function toggleOneRecurso(selectedRecursoItems) {
            for (var IdOportunidad in selectedRecursoItems) {
                if (selectedRecursoItems.hasOwnProperty(IdOportunidad)) {
                    if (!selectedRecursoItems[IdOportunidad]) {
                        vm.selectAllRecurso = false;
                        return;
                    }
                }
            }
            vm.selectAllRecurso = true;
        }
        //Filtro de la  tabla [TRAZABILIDAD].[Recurso] por Descripcion.
        function BuscarRecurso() {
            blockUI.start();
            LimpiarGrillaRecurso();
            $timeout(function () {
                vm.dtOptionsRecurso = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarRecursoSTPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
               .withOption('headerCallback', function (header) {
                   $compile.headerCompiled = true;
                   $compile(angular.element(header).contents())($scope);
               });
            }, 500);
        }
        function BuscarRecursoSTPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var recurso = {
                Nombre: vm.DescripcionRecurso,
                TipoRecurso: -1,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = BandejaRecursoService.ListarRecursoPaginado(recurso);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListRecursoDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrillaRecurso();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrillaRecurso() {
            vm.dtOptionsRecurso = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltrosRecurso() {
            vm.DescripcionRecurso = "";
            LimpiarGrillaRecurso();
        }
        //******************************************************** Grilla Recurso *********************************************************************
    }
})();