﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RptFCVGrillaBusquedaRecursoService', RptFCVGrillaBusquedaRecursoService);

    RptFCVGrillaBusquedaRecursoService.$inject = ['$http', 'URLS'];

    function RptFCVGrillaBusquedaRecursoService($http, $urls) {

        var service = {
            ModalRptFCVGrillaBusquedaRecurso: ModalRptFCVGrillaBusquedaRecurso
            
        };

        return service;
   


        function ModalRptFCVGrillaBusquedaRecurso() {

            return $http({
                url: $urls.ApiTrazabilidad + "RptFCVGrillaBusquedaRecurso/Index",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();