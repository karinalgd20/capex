﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RptFCVGrillaBusquedaRecurso', RptFCVGrillaBusquedaRecurso);
    RptFCVGrillaBusquedaRecurso.$inject = ['RecursoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RptFCVGrillaBusquedaRecurso(RecursoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.BuscarRecurso = BuscarRecurso;
        vm.LimpiarFiltrosR = LimpiarFiltrosR;
        vm.DescripcionRecurso = "";
        vm.HDescripcionRecurso = "";
        vm.SeleccionarRecurso = SeleccionarRecurso;
        LimpiarGrillaR();
        vm.dtColumnsR = [
          DTColumnBuilder.newColumn('IdRecurso').withTitle('ID').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Nombre').withTitle('Nombre').notSortable().withOption('width', '75%'),
            DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn(null).withTitle('Acción').notSortable().renderWith(AccionesBusquedaRecurso).withOption('width', '5%')
        ];
        //variables Globales
        var modalpopupBuscarR = angular.element(document.getElementById("dlgBuscarR"));
        //Funcion carga input de Recurso por su Id
        function CargaRecurso(IdRecurso) {
            var recurso = {
                IdRecurso: IdRecurso
            };
            var promise = RecursoService.ObtenerRecursoPorId(recurso);
            promise.then(function (resultado) {
                vm.HDescripcionRecurso = resultado.data.Nombre;
            }, function (response) {
                LimpiarGrillaR();
            });
        }

        //Evento de mostrar PopUp
        $("#btnBuscarR").click(function () {
            modalpopupBuscarR.modal('show');
            LimpiarFiltrosR();
        });

        //Filtro de la  tabla [TRAZABILIDAD].[OportunidadST] por Descripcion.
        function BuscarRecurso() {
            blockUI.start();
            LimpiarGrillaR();

            $timeout(function () {
                vm.dtOptionsR = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarRecursoPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }
        function BuscarRecursoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var recurso = {
                Nombre: vm.DescripcionRecurso,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = RecursoService.ListaRecursoModalRecursoPaginado(recurso);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListRecursoDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrillaR();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrillaR() {
            vm.dtOptionsR = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltrosR() {
            vm.DescripcionRecurso = "";
            LimpiarGrillaR();
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusquedaRecurso(data, type, full, meta) {
            var respuesta = "";
            respuesta = respuesta + " <a title='Editar'   ng-click='vm.SeleccionarRecurso(" + data.IdRecurso + ");'>" + "<span class='glyphicon glyphicon-hand-right' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            return respuesta;
        }

        //Selecciona registro
        function SeleccionarRecurso(IdRecurso) {
            $scope.$parent.vm.IdRecurso = IdRecurso;
            $scope.$parent.vm.BuscarRecursoIdRpt();
            $("#ModalRptFCVGrillaBusquedaRecurso").modal('hide');
        }

    }
})();