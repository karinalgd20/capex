﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('EtapaOportunidadService', EtapaOportunidadService);

    EtapaOportunidadService.$inject = ['$http', 'URLS'];

    function EtapaOportunidadService($http, $urls) {

        var service = {
            ListarComboEtapaOportunidadPorIdFase: ListarComboEtapaOportunidadPorIdFase,
            ListarComboEtapas: ListarComboEtapas,
        };

        return service;

        function ListarComboEtapaOportunidadPorIdFase(fase) {
            return $http({
                url: $urls.ApiTrazabilidad + "EtapaOportunidad/ListarComboEtapaOportunidadPorIdFase",
                method: "POST",
                data: JSON.stringify(fase)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

         function ListarComboEtapas() {
            return $http({
                url: $urls.ApiTrazabilidad + "EtapaOportunidad/ListarComboEtapas",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();