﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RptOportunidadesAtendidasBusquedaRecursoService', RptOportunidadesAtendidasBusquedaRecursoService);

    RptOportunidadesAtendidasBusquedaRecursoService.$inject = ['$http', 'URLS'];

    function RptOportunidadesAtendidasBusquedaRecursoService($http, $urls) {

        var service = {
            ModalRptOportunidadesAtendidasBusquedaRecurso: ModalRptOportunidadesAtendidasBusquedaRecurso
            
        };

        return service;
   


        function ModalRptOportunidadesAtendidasBusquedaRecurso() {

            return $http({
                url: $urls.ApiTrazabilidad + "RptOportunidadesAtendidasBusquedaRecurso/Index",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();