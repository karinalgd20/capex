﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('TipoOportunidadService', TipoOportunidadService);

    TipoOportunidadService.$inject = ['$http', 'URLS'];

    function TipoOportunidadService($http, $urls) {

        var service = {
            ListarComboTipoOportunidad: ListarComboTipoOportunidad
        };

        return service;

      function ListarComboTipoOportunidad() {
            return $http({
                url: $urls.ApiTrazabilidad + "TipoOportunidad/ListarComboTipoOportunidad",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();