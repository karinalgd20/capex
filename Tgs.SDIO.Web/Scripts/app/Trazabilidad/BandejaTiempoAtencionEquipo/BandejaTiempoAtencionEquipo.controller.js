﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('BandejaTiempoAtencionEquipo', BandejaTiempoAtencionEquipo);
    BandejaTiempoAtencionEquipo.$inject = ['BandejaTiempoAtencionEquipoService', 'EquipoTrabajoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function BandejaTiempoAtencionEquipo(BandejaTiempoAtencionEquipoService, EquipoTrabajoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiTrazabilidad + "RegistrarTiempoAtencionEquipo/Index/";
        vm.BuscarTiempoAtencionEquipo = BuscarTiempoAtencionEquipo;
        vm.EliminarTiempoAtencionEquipo = EliminarTiempoAtencionEquipo;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.IdOportunidadSF = "";
        vm.IdCasoSF = "";
        vm.IdSisegoSW = "";
        vm.IdEquipoTrabajo = "-1";
        LimpiarGrilla();
        ListarTipoEquipoTrabajo();
        vm.listEquipoTrabajo = [];
        vm.dtColumns = [
          DTColumnBuilder.newColumn('IdTiempoAtencion').withTitle('Código').notSortable().withOption('width', '6%'),
          DTColumnBuilder.newColumn('IdOportunidadSF').withTitle('Oportunidad').notSortable().withOption('width', '15%'),
          DTColumnBuilder.newColumn('IdCasoSF').withTitle('Caso').notSortable().withOption('width', '8%'),
          DTColumnBuilder.newColumn('IdSisegoSW').withTitle('Sisego').notSortable().withOption('width', '8%'),
          DTColumnBuilder.newColumn('DescripcionEquipoTrabajo').withTitle('Equipo de Trabajo').notSortable().withOption('width', '8%'),
          DTColumnBuilder.newColumn('strFechaInicio').withTitle('Fecha Inicio').notSortable().withOption('width', '14%'),
          DTColumnBuilder.newColumn('strFechaFin').withTitle('Fecha Fin').notSortable().withOption('width', '14%'),
          DTColumnBuilder.newColumn('DiasGestion').withTitle('Dias Gestion').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Observaciones').withTitle('Observaciones').notSortable().withOption('width', '19%'),
          DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '6%'),
          DTColumnBuilder.newColumn('UsuarioEdicion').withTitle('Usuario Edicion').notSortable().withOption('width', '6%'),
          DTColumnBuilder.newColumn('strFechaEdicion').withTitle('Fecha Edicion').notSortable().withOption('width', '6%'),
          DTColumnBuilder.newColumn('UsuarioCreacion').withTitle('Usuario Creacion').notSortable().withOption('width', '6%'),
          DTColumnBuilder.newColumn('strFechaCreacion').withTitle('Fecha Creacion').notSortable().withOption('width', '6%'),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];
        //Lista los IdEquipoTrabajo y Descripciones de la tabla [COMUN].[EquipoTrabajo] que esten Activos.
        function ListarTipoEquipoTrabajo() {
            var promise = EquipoTrabajoService.ListarComboEquipoTrabajoAreas();
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listEquipoTrabajo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        vm.CargarDatoPostventaSeleccionado = CargarDatoPostventaSeleccionado;
        CargarDatoPostventaSeleccionado();
        function CargarDatoPostventaSeleccionado() {
            if (jsonTiempoAtencionEquipo != '0') {
                debugger;
                vm.IdOportunidadSF = jsonTiempoAtencionEquipo;
                BuscarTiempoAtencionEquipo()
            }
        }
        //Eliminacion logica de la tabla [TRAZABILIDAD].[ObservacionOportunidad]
        function EliminarTiempoAtencionEquipo(IdTiempoAtencion) {
            if (confirm('¿Estas seguro que desea eliminar?')) {

                var objObservacion = {
                    IdTiempoAtencion: IdTiempoAtencion
                }
                blockUI.start();
                var promise = BandejaTiempoAtencionEquipoService.EliminarTiempoAtencionEquipo(objObservacion);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                        BuscarTiempoAtencionEquipo()
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                    }
                    modalpopupConfirm.modal('hide');
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
             }
            else {
                blockUI.stop();
            }
        }
        vm.CargarDatoPreventaSeleccionado = CargarDatoPreventaSeleccionado;
        CargarDatoPreventaSeleccionado();
        function CargarDatoPreventaSeleccionado() {
            if (jsonTiempoAtencionEquipo != '0') {
                vm.IdOportunidadSF = jsonTiempoAtencionEquipo;
                BuscarTiempoAtencionEquipo()
            }
        }
        //Filtro de la  tabla [TRAZABILIDAD].[TiempoAtencionEquipo] por IdOportunidadSF y Concepto.
        function BuscarTiempoAtencionEquipo() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarTiempoAtencionEquipoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }
        function BuscarTiempoAtencionEquipoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var tiempoAtencion = {
                IdOportunidadSF: vm.IdOportunidadSF,
                IdCasoSF: vm.IdCasoSF,
                IdSisegoSW: vm.IdSisegoSW,
                IdEquipoTrabajo: vm.IdEquipoTrabajo,
                Indice: pageNumber,
                Tamanio: length
            };
            debugger;
            var promise = BandejaTiempoAtencionEquipoService.ListarTiempoAtencionEquipoPaginado(tiempoAtencion);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListTiempoAtencionEquipoDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusqueda(data, type, full, meta) {
            var respuesta = "";
            respuesta = respuesta + " <a title='Editar'   href='" + vm.EnlaceRegistrar + data.IdTiempoAtencion + "'>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + "<a title='Eliminar'  onclick='EliminarTiempoAtencionEquipo(" + data.IdTiempoAtencion + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6; margin-left: 1px;'></span></a>";
            return respuesta;
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.IdOportunidadSF = "";
            vm.IdCasoSF = "";
            vm.CodSisego = "";
            vm.IdEquipoTrabajo = "-1";
            LimpiarGrilla();
        }
    }
})();