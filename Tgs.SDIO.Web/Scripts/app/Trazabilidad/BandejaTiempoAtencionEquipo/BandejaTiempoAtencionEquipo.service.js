﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('BandejaTiempoAtencionEquipoService', BandejaTiempoAtencionEquipoService);

    BandejaTiempoAtencionEquipoService.$inject = ['$http', 'URLS'];

    function BandejaTiempoAtencionEquipoService($http, $urls) {

        var service = {
            ListarTiempoAtencionEquipoPaginado: ListarTiempoAtencionEquipoPaginado,
            EliminarTiempoAtencionEquipo: EliminarTiempoAtencionEquipo
        };

        return service;

      
        function ListarTiempoAtencionEquipoPaginado(tiempoAtencion) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaTiempoAtencionEquipo/ListadoTiempoAtencionEquipoPaginado",
                method: "POST",
                data: JSON.stringify(tiempoAtencion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarTiempoAtencionEquipo(tiempoAtencion) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaTiempoAtencionEquipo/EliminarTiempoAtencionEquipo",
                method: "POST",
                data: JSON.stringify(tiempoAtencion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();