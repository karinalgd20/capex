﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RecursoOportunidadBusquedaOportunidad', RecursoOportunidadBusquedaOportunidad);
    RecursoOportunidadBusquedaOportunidad.$inject = ['RecursoOportunidadBusquedaOportunidadService', 'OportunidadSTService', 'RecursoService', 'RolRecursoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RecursoOportunidadBusquedaOportunidad(RecursoOportunidadBusquedaOportunidadService, OportunidadSTService, RecursoService, RolRecursoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.BuscarOportunidadST = BuscarOportunidadST;
        vm.LimpiarFiltrosO = LimpiarFiltrosO;
        vm.HDescripcionOportunidad = "";
        vm.DescripcionOportunidad = "";
        vm.IdOportunidadSF = "";
        vm.SeleccionarOportunidadSF = SeleccionarOportunidadSF;
        LimpiarGrillaO();
        vm.dtColumnsO = [
          DTColumnBuilder.newColumn('IdOportunidad').withTitle('ID').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('IdOportunidadSF').withTitle('ID Oportunidad SF').notSortable().withOption('width', '20%'),
          DTColumnBuilder.newColumn('DescripcionOportunidad').withTitle('Nombre Oportunidad').notSortable().withOption('width', '65%'),
          DTColumnBuilder.newColumn(null).withTitle('Acción').notSortable().renderWith(AccionesBusquedaOportunidadSF).withOption('width', '5%')
        ];
        //Funcion carga input de oportunidad por su Id
        function CargarOportunidad(IdOportunidad) {
            var oportunidad = {
                IdOportunidad: IdOportunidad
            };
            var promise = OportunidadSTService.ObtenerOportunidadSTPorId(oportunidad);
            promise.then(function (resultado) {
                vm.HDescripcionOportunidad = resultado.data.Descripcion;
            }, function (response) {
                LimpiarGrilla();
            });
        }
        //Evento de mostrar PopUp
        $("#btnBuscarO").click(function () {
            modalpopupBuscarO.modal('show');
            LimpiarFiltrosO();
        });

        //Filtro de la  tabla [TRAZABILIDAD].[OportunidadST] por Descripcion.
        function BuscarOportunidadST() {
            blockUI.start();
            LimpiarGrillaO();
            $timeout(function () {
                vm.dtOptionsO = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarOportunidadSTPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }
        function BuscarOportunidadSTPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var oportunidad = {
                DescripcionOportunidad: vm.DescripcionOportunidad,
                IdOportunidadSF: vm.IdOportunidadSF,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = OportunidadSTService.ListaOportunidadSTModalRecursoPaginado(oportunidad);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListOportunidadSTDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrillaO();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrillaO() {
            vm.dtOptionsO = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltrosO() {
            vm.DescripcionOportunidad = "";
            vm.IdOportunidadSF = "";
            LimpiarGrillaO();
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusquedaOportunidadSF(data, type, full, meta) {
            var respuesta = "";
            respuesta = respuesta + " <a title='Editar'   ng-click='vm.SeleccionarOportunidadSF(" + data.IdOportunidad + ");'>" + "<span class='glyphicon glyphicon-hand-right' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            return respuesta;
        }

        //Selecciona registro
        function SeleccionarOportunidadSF(IdOportunidad) {
            $scope.$parent.vm.IdOportunidad = IdOportunidad;            
            $scope.$parent.vm.BuscarOportunidadId();
            $("#ModalBusquedaOportunidad").modal('hide');
        }


    }
})();