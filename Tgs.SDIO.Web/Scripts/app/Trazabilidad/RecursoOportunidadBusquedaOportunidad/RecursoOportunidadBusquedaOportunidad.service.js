﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RecursoOportunidadBusquedaOportunidadService', RecursoOportunidadBusquedaOportunidadService);

    RecursoOportunidadBusquedaOportunidadService.$inject = ['$http', 'URLS'];

    function RecursoOportunidadBusquedaOportunidadService($http, $urls) {

        var service = {
            ModalRecursoOportunidadBusquedaOportunidadService: ModalRecursoOportunidadBusquedaOportunidadService,
            SeleccionarOportunidadSF: SeleccionarOportunidadSF
            
            
        };

        return service;



        function ModalRecursoOportunidadBusquedaOportunidadService() {
            return $http({
                url: $urls.ApiTrazabilidad + "RecursoOportunidadBusquedaOportunidad/Index",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function SeleccionarOportunidadSF(datosOportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "RegistrarRecursoOportunidad/Index",
                method: "POST",
                data: JSON.stringify(datosOportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
    }
})();