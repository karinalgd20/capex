﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('BandejaRecursoOportunidad', BandejaRecursoOportunidad);
    BandejaRecursoOportunidad.$inject = ['BandejaRecursoOportunidadService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function BandejaRecursoOportunidad(BandejaRecursoOportunidadService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiTrazabilidad + "RegistrarRecursoOportunidad/Index/";
        vm.EnlaceRegistrarMasivo = $urls.ApiTrazabilidad + "RegistrarRecursoOportunidadMasivo/Index";
        vm.BuscarRecursoOportunidad = BuscarRecursoOportunidad;
        vm.EliminarRecursoOportunidad = EliminarRecursoOportunidad;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.DescripcionRecurso = "";
        vm.IdOportunidadSF = "";
        vm.IdAsignacion = "";
        LimpiarGrilla();
        vm.dtColumns = [
          DTColumnBuilder.newColumn('IdAsignacion').withTitle('Código').notSortable().withOption('width', '8%'),
          DTColumnBuilder.newColumn('DescripcionRecurso').withTitle('Recurso').notSortable().withOption('width', '28%'),
          DTColumnBuilder.newColumn('DescripcionRolRecurso').withTitle('Rol de Recurso').notSortable().withOption('width', '20%'),
          DTColumnBuilder.newColumn('strFInicioAsignacion').withTitle('Fecha Inicio').notSortable().withOption('width', '9%'),
          DTColumnBuilder.newColumn('strFFinAsignacion').withTitle('Fecha Fin').notSortable().withOption('width', '8%'),
          DTColumnBuilder.newColumn('PorcentajeDedicacion').withTitle('% Dedicación').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '7%'),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];   
         //Eliminacion logica de la tabla [TRAZABILIDAD].[RolRecurso] 
        function EliminarRecursoOportunidad(IdAsignacion) {
            if (confirm('¿Estas seguro que desea eliminar?')) {
                var request = {
                    IdAsignacion: IdAsignacion
                }
                blockUI.start();
                var promise = BandejaRecursoOportunidadService.EliminarRecursoOportunidad(request);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                        BuscarRecursoOportunidad()
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                    }
                    modalpopupConfirm.modal('hide');
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
             }
            else {
                blockUI.stop();
            }
        }       
        //Filtro de la  tabla [TRAZABILIDAD].[RecursoOportunidad] por DescripcionRecurso y Oportunidad.
        function BuscarRecursoOportunidad() {
            debugger;
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(BuscarRecursoOportunidadPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }
        function BuscarRecursoOportunidadPaginado(sSource, aoData, fnCallback, oSettings) { 
        debugger;
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start+length)/length;
            var recursoOportunidad = {
                DescripcionRecurso: vm.DescripcionRecurso,
                IdOportunidadSF: vm.IdOportunidadSF,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = BandejaRecursoOportunidadService.ListarRecursoOportunidadPaginado(recursoOportunidad);
            promise.then(function (resultado) {
            debugger;
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListRecursoOportunidadDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
         //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
         //Inicializa  html de la columna Accion.
        function AccionesBusqueda(data, type, full, meta) {
            var respuesta = "";
            respuesta = respuesta + " <a title='Editar'   href='" + vm.EnlaceRegistrar + data.IdAsignacion + "'>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + "<a title='Eliminar'  onclick='EliminarRecursoOportunidad(" + data.IdAsignacion + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6; margin-left: 1px;'></span></a>";
            return respuesta;
        }
         //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.DescripcionRecurso = "";
            vm.IdOportunidadSF = "";
            LimpiarGrilla();
        }
    }
})();