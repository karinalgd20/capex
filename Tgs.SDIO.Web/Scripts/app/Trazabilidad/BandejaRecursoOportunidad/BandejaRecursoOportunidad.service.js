﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('BandejaRecursoOportunidadService', BandejaRecursoOportunidadService);

    BandejaRecursoOportunidadService.$inject = ['$http', 'URLS'];

    function BandejaRecursoOportunidadService($http, $urls) {

        var service = {
            ListarRecursoOportunidadPaginado: ListarRecursoOportunidadPaginado,
            EliminarRecursoOportunidad: EliminarRecursoOportunidad
        };

        return service;


        function ListarRecursoOportunidadPaginado(recursoOportunidad) {
        debugger;
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaRecursoOportunidad/ListarRecursoOportunidad",
                method: "POST",
                data: JSON.stringify(recursoOportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarRecursoOportunidad(recursoOportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaRecursoOportunidad/EliminarRecursoOportunidad",
                method: "POST",
                data: JSON.stringify(recursoOportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();