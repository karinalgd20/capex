﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RegistrarTiempoAtencionEquipo', RegistrarTiempoAtencionEquipo);
    RegistrarTiempoAtencionEquipo.$inject = ['RegistrarTiempoAtencionEquipoService', 'EquipoTrabajoService', 'OportunidadSTService', 'CasoOportunidadService', 'ObservacionOportunidadBusquedaOportunidadCasoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RegistrarTiempoAtencionEquipo(RegistrarTiempoAtencionEquipoService, EquipoTrabajoService, OportunidadSTService, CasoOportunidadService, ObservacionOportunidadBusquedaOportunidadCasoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.CargarDatos = CargarDatos;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.RegistraTiempoAtencionEquipo = RegistraTiempoAtencionEquipo;
        vm.EnlaceBandeja = $urls.ApiTrazabilidad + "BandejaTiempoAtencionEquipo/Index/";
        vm.IdTiempoAtencion = "";
        vm.IdOportunidad = "";
        vm.IdCaso = "";
        vm.IdSisegoSW = "";
        vm.FechaInicio = "";
        vm.FechaFin = "";
        vm.Observaciones = "";
        vm.IdEquipoTrabajo = "-1";
        vm.IdEstado = "1";
        CargarDatos();
        ListarTipoEquipoTrabajo();
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        //Lista los IdEquipoTrabajo y Descripciones de la tabla [COMUN].[ConceptoSeguimiento] que esten Activos.
        function ListarTipoEquipoTrabajo() {
            var promise = EquipoTrabajoService.ListarComboEquipoTrabajoAreas();
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listEquipoTrabajo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si existente.
        function CargarDatos() {
            debugger;
            if (jsonTiempoAtencionEquipo == null || jsonTiempoAtencionEquipo == "") {
                vm.IdTiempoAtencion = "";
                vm.IdOportunidad = "";
                vm.IdCaso = "";
                vm.IdSisegoSW = "";
                vm.FechaInicio = "";
                vm.FechaFin = "";
                vm.Observaciones = "";
                vm.IdEquipoTrabajo = "-1";
                vm.IdEstado = "1";
            } else {
                if (jsonTiempoAtencionEquipo.strFechaInicio == "") {
                    var FechaInicio = null;
                } else { 
                    var dateParts1 = jsonTiempoAtencionEquipo.strFechaInicio.split("/");
                    var FechaInicio = new Date(dateParts1[2], dateParts1[1] - 1, dateParts1[0]);
                }
                if (jsonTiempoAtencionEquipo.strFechaFin == "") {
                    var FechaFin = null;
                } else {
                    var dateParts2 = jsonTiempoAtencionEquipo.strFechaFin.split("/");
                    var FechaFin = new Date(dateParts2[2], dateParts2[1] - 1, dateParts2[0]);
                }

                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdTiempoAtencion = jsonTiempoAtencionEquipo.IdTiempoAtencion;
                vm.Observaciones = jsonTiempoAtencionEquipo.Observaciones;
                vm.IdOportunidad = jsonTiempoAtencionEquipo.IdOportunidad;
                vm.IdCaso = jsonTiempoAtencionEquipo.IdCaso;
                CargarOportunidad(jsonTiempoAtencionEquipo.IdOportunidad);
                CargaCaso(jsonTiempoAtencionEquipo.IdCaso);
                vm.FechaInicio = jsonTiempoAtencionEquipo.strFechaInicio;
                vm.FechaFin = jsonTiempoAtencionEquipo.strFechaFin;
                vm.IdSisegoSW = jsonTiempoAtencionEquipo.IdSisegoSW;
                vm.IdEquipoTrabajo = jsonTiempoAtencionEquipo.IdEquipoTrabajo;
                vm.IdEstado = jsonTiempoAtencionEquipo.IdEstado;
            }
        }
        //Limpia los controles del formulacion a sus valores por defecto.
        function LimpiarCampos() {
            vm.IdTiempoAtencion = "";
            vm.IdOportunidad = "";
            vm.IdCaso = "";
            vm.IdSisegoSW = "";
            vm.FechaInicio = "";
            vm.FechaFin = "";
            vm.Observaciones = "";
            vm.IdEquipoTrabajo = "-1";
            vm.HIdOportunidadSF = "";
            vm.HIdCasoSF = "";
            vm.IdEstado = "1";
        }
        //Registra en  la tabla [TRAZABILIDAD].[Actividad] 
        function RegistraTiempoAtencionEquipo() {
            var tiempoAtencion = {
                IdTiempoAtencion: vm.IdTiempoAtencion,
                Observaciones: vm.Observaciones,
                IdOportunidad: vm.IdOportunidad,
                IdCaso: vm.IdCaso,
                IdSisegoSW: vm.IdSisegoSW,
                FechaInicio: vm.FechaInicio,
                FechaFin: vm.FechaFin,
                IdEquipoTrabajo: vm.IdEquipoTrabajo,
                IdEstado: vm.IdEstado
            };
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promise = RegistrarTiempoAtencionEquipoService.RegistrarTiempoAtencionEquipo(tiempoAtencion);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        LimpiarCampos();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar.", 5);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            }
            else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }
            if ($.trim(vm.IdEquipoTrabajo) == "-1") {
                mensaje = mensaje + "Elija Tipo de Equipo de Trabajo" + '<br/>';
                UtilsFactory.InputBorderColor('#IdEquipoTrabajo', 'Rojo');
            }

            if ($.trim(vm.HIdOportunidadSF) == "") {
                mensaje = mensaje + "Seleccione una Oportunidad" + '<br/>';
                UtilsFactory.InputBorderColor('#HIdOportunidadSF', 'Rojo');
            }

            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            //UtilsFactory.InputBorderColor('#Observaciones', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdEquipoTrabajo', 'Ninguno');
            UtilsFactory.InputBorderColor('#HIdOportunidadSF', 'Ninguno');
        }

        //Busca Id Seleccionado de Oportunidad 
        vm.BuscarOportunidadId = BuscarOportunidadId;
        function BuscarOportunidadId() {
            CargarOportunidad(vm.IdOportunidad);
            CargaCaso(vm.IdCaso)
        }

        function CargarOportunidad(IdOportunidad) {
            var oportunidad = {
                IdOportunidad: IdOportunidad
            };
            var promise = OportunidadSTService.ObtenerOportunidadSTPorId(oportunidad);
            promise.then(function (resultado) {
                vm.HIdOportunidadSF = resultado.data.IdOportunidadSF;
            }, function (response) {
                LimpiarGrilla();
            });
        }

        //Modal Buscar Recurso
        vm.ModalBusquedaOportunidadCaso = ModalBusquedaOportunidadCaso;

        function ModalBusquedaOportunidadCaso() {

            var promise = ObservacionOportunidadBusquedaOportunidadCasoService.SeleccionarOportunidadCaso();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoBusquedaOportunidadCaso").html(content);
                });

                $('#ModalBusquedaOportunidadCaso').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaCaso(IdCaso) {
            var caso = {
                IdCaso: IdCaso
            };
            var promise = CasoOportunidadService.ObtenerCasoOportunidadPorId(caso);
            promise.then(function (resultado) {
                debugger;
                vm.HIdCasoSF = resultado.data.IdCasoSF;
            }, function (response) {
                LimpiarGrilla();
            });
        }

    }
})();