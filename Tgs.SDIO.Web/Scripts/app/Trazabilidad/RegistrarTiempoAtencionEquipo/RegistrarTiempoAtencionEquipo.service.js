﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RegistrarTiempoAtencionEquipoService', RegistrarTiempoAtencionEquipoService);

    RegistrarTiempoAtencionEquipoService.$inject = ['$http', 'URLS'];

    function RegistrarTiempoAtencionEquipoService($http, $urls) {

        var service = {
            RegistrarTiempoAtencionEquipo: RegistrarTiempoAtencionEquipo
        };

        return service;


        function RegistrarTiempoAtencionEquipo(tiempoAtencion) {
            return $http({
                url: $urls.ApiTrazabilidad + "RegistrarTiempoAtencionEquipo/RegistrarTiempoAtencionEquipo",
                method: "POST",
                data: JSON.stringify(tiempoAtencion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };    

    }
})();