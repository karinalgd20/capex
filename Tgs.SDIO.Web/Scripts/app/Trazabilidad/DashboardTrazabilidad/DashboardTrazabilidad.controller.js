﻿(function () {
    'use strict',

    angular
    .module('app.Trazabilidad')
    .controller('DashboardTrazabilidadController', DashboardTrazabilidadController);

    DashboardTrazabilidadController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS'];

    function DashboardTrazabilidadController(blockUI, $timeout, UtilsFactory, $urls) {
        var vm = this;
        var idJefe = 0;
        vm.MostrarDashboard = MostrarDashboard;


        function MostrarDashboard() {
            var fechaActual = new Date();
            var anioActual = fechaActual.getFullYear();
            var tipoReporte = 1;
            var idPreventa = 0;
            var perfiles = JSON.parse(codigosPerfiles);
            debugger;
            for (var i = 0; i <= perfiles.length - 1 ; i++) {
                if (perfiles[i] == coordinadorServicioF4) {
                    tipoReporte = 1;
                    idJefe = idUsuario;            
                    break;
                }
                else {
                    tipoReporte = 2;
                    idPreventa = idUsuario;
                    break;
                }
            }

            var parametros =
                    "reporte=DashboardTrazabilidad&Anio=" + anioActual +
                    "&IdPreventa=" + idPreventa +
                    "&IdJefe=" + idJefe +
                    "&TipoReporte=" + tipoReporte;
            blockUI.start();

            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);

            $timeout(function () {
                blockUI.stop();
            }, 5000);
        }
        
    }
})();

