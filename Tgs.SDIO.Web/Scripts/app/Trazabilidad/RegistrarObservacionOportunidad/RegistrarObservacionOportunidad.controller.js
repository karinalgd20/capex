﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RegistrarObservacionOportunidad', RegistrarObservacionOportunidad);
    RegistrarObservacionOportunidad.$inject = ['RegistrarObservacionOportunidadService', 'ConceptoSeguimientoService', 'OportunidadSTService', 'CasoOportunidadService', 'ObservacionOportunidadBusquedaOportunidadCasoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RegistrarObservacionOportunidad(RegistrarObservacionOportunidadService, ConceptoSeguimientoService, OportunidadSTService, CasoOportunidadService, ObservacionOportunidadBusquedaOportunidadCasoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.CargarDatos = CargarDatos;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.RegistraObservacionOportunidad = RegistraObservacionOportunidad;
        vm.EnlaceBandeja = $urls.ApiTrazabilidad + "BandejaObservacionOportunidad/Index/";
        vm.IdObservacion = "";
        vm.IdOportunidad = "";
        vm.IdCaso = "";
        vm.FechaSeguimiento = UtilsFactory.ObtenerFechaActual();
        vm.Observaciones = "";
        vm.IdConcepto = "-1";
        vm.IdEstado = "1";
        CargarDatos();
        ListarTipoConcepto();
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        //Lista los IdConcepto y Descripciones de la tabla [COMUN].[ConceptoSeguimiento] que esten Activos.
        function ListarTipoConcepto() {
            var promise = ConceptoSeguimientoService.ListarComboConceptoSeguimientoAgrupador();
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listConcepto = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si existente.
        function CargarDatos() {
            if (jsonDatoObservacionOportunidad == null || jsonDatoObservacionOportunidad == "") {
                vm.IdObservacion = "";
                vm.IdOportunidad = "";
                vm.IdCaso = "";
                vm.FechaSeguimiento = UtilsFactory.ObtenerFechaActual();
                vm.Observaciones = "";
                vm.IdConcepto = "-1";
                vm.IdEstado = "1";
            } else {
                var dateParts2 = jsonDatoObservacionOportunidad.strFechaSeguimiento.split("/");
                var FechaSeguimiento = new Date(dateParts2[2], dateParts2[1] - 1, dateParts2[0]);
                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdObservacion = jsonDatoObservacionOportunidad.IdObservacion;
                vm.Observaciones = jsonDatoObservacionOportunidad.Observaciones;
                vm.IdOportunidad = jsonDatoObservacionOportunidad.IdOportunidad;
                vm.IdCaso = jsonDatoObservacionOportunidad.IdCaso;
                CargarOportunidad(jsonDatoObservacionOportunidad.IdOportunidad);
                CargaCaso(jsonDatoObservacionOportunidad.IdCaso);
                vm.FechaSeguimiento = FechaSeguimiento;
                vm.IdConcepto = jsonDatoObservacionOportunidad.IdConcepto;
                vm.IdEstado = jsonDatoObservacionOportunidad.IdEstado;
            }
        }
        //Limpia los controles del formulacion a sus valores por defecto.
        function LimpiarCampos() {
            vm.IdObservacion = "";
            vm.IdOportunidad = "";
            vm.IdCaso = "";
            vm.HIdOportunidadSF = "";
            vm.HIdCasoSF = "";
            vm.FechaSeguimiento = "";
            vm.Observaciones = "";
            vm.IdConcepto = "-1";
            vm.IdEstado = "1";
        }
        //Registra en  la tabla [TRAZABILIDAD].[Actividad] 
        function RegistraObservacionOportunidad() {
            var observacion = {
                IdObservacion: vm.IdObservacion,
                Observaciones: vm.Observaciones,
                IdOportunidad: vm.IdOportunidad,
                IdCaso: vm.IdCaso,
                FechaSeguimiento: vm.FechaSeguimiento,
                IdConcepto: vm.IdConcepto,
                IdEstado: vm.IdEstado
            };
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promise = RegistrarObservacionOportunidadService.RegistrarObservacionOportunidad(observacion);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    debugger;
                    if (Respuesta.TipoRespuesta == 0) {
                        LimpiarCampos();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar.", 5);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            }
            else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }
            if ($.trim(vm.IdConcepto) == "-1") {
                mensaje = mensaje + "Elija Tipo Concepto" + '<br/>';
                UtilsFactory.InputBorderColor('#IdConcepto', 'Rojo');
            }

            if ($.trim(vm.FechaSeguimiento) == "") {
                mensaje = mensaje + "Ingrese Fecha" + '<br/>';
                UtilsFactory.InputBorderColor('#FechaSeguimiento', 'Rojo');
            }

            if ($.trim(vm.HIdOportunidadSF) == "") {
                mensaje = mensaje + "Seleccione una Oportunidad" + '<br/>';
                UtilsFactory.InputBorderColor('#HDescripcionOportunidad', 'Rojo');
            }

            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            //UtilsFactory.InputBorderColor('#Observaciones', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdConcepto', 'Ninguno');
            UtilsFactory.InputBorderColor('#FechaSeguimiento', 'Ninguno');
            UtilsFactory.InputBorderColor('#HIdOportunidadSF', 'Ninguno');
        }

        //Busca Id Seleccionado de Recurso 
        vm.BuscarOportunidadId = BuscarOportunidadId;
        function BuscarOportunidadId() {
            CargarOportunidad(vm.IdOportunidad);
            CargaCaso(vm.IdCaso)
        }

        function CargarOportunidad(IdOportunidad) {
            var oportunidad = {
                IdOportunidad: IdOportunidad
            };
            var promise = OportunidadSTService.ObtenerOportunidadSTPorId(oportunidad);
            promise.then(function (resultado) {
                vm.HIdOportunidadSF = resultado.data.IdOportunidadSF;
            }, function (response) {
                LimpiarGrilla();
            });
        }

        //Modal Buscar Recurso
        vm.ModalBusquedaOportunidadCaso = ModalBusquedaOportunidadCaso;

        function ModalBusquedaOportunidadCaso() {

            var promise = ObservacionOportunidadBusquedaOportunidadCasoService.SeleccionarOportunidadCaso();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoBusquedaOportunidadCaso").html(content);
                });

                $('#ModalBusquedaOportunidadCaso').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaCaso(IdCaso) {
            debugger;
            var caso = {
                IdCaso: IdCaso
            };
            var promise = CasoOportunidadService.ObtenerCasoOportunidadPorId(caso);
            promise.then(function (resultado) {
                debugger;
                vm.HIdCasoSF = resultado.data.IdCasoSF;
            }, function (response) {
                LimpiarGrilla();
            });
        }

    }
})();