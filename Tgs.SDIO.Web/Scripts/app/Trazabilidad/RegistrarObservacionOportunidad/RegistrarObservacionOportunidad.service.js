﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RegistrarObservacionOportunidadService', RegistrarObservacionOportunidadService);

    RegistrarObservacionOportunidadService.$inject = ['$http', 'URLS'];

    function RegistrarObservacionOportunidadService($http, $urls) {

        var service = {
            RegistrarObservacionOportunidad: RegistrarObservacionOportunidad
        };

        return service;


        function RegistrarObservacionOportunidad(observacion) {
            return $http({
                url: $urls.ApiTrazabilidad + "RegistrarObservacionOportunidad/RegistrarObservacionOportunidad",
                method: "POST",
                data: JSON.stringify(observacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };    

    }
})();