﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RecursoOportunidadBusquedaRecursoService', RecursoOportunidadBusquedaRecursoService);

    RecursoOportunidadBusquedaRecursoService.$inject = ['$http', 'URLS'];

    function RecursoOportunidadBusquedaRecursoService($http, $urls) {

        var service = {
            ModalRecursoOportunidadBusquedaRecurso: ModalRecursoOportunidadBusquedaRecurso
            
        };

        return service;
   


        function ModalRecursoOportunidadBusquedaRecurso() {

            return $http({
                url: $urls.ApiTrazabilidad + "RecursoOportunidadBusquedaRecurso/Index",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();