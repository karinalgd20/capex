﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RptOportunidadesService', RptOportunidadesService);

    RptOportunidadesService.$inject = ['$http', 'URLS'];

    function RptOportunidadesService($http, $urls) {

        var service = {
            GenerarRptOportunidad: GenerarRptOportunidad
        };

        return service;

     

        function GenerarRptOportunidad(request) {
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/GenerarRptOportunidad",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();