﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RptOportunidades', RptOportunidades);
    RptOportunidades.$inject = [ 'MaestraService', 'EstadoCasoService', 'TipoOportunidadService', 'EtapaService', 'SegmentosNegocioService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RptOportunidades( MaestraService, EstadoCasoService, TipoOportunidadService, EtapaService, SegmentosNegocioService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.DescripcionOportunidad = "";
        vm.DescripcionCliente = "";
        vm.IdEtapa = "-1";
        vm.IdEstadoCaso = "-1";
        vm.IdSegmentoNegocio = "-1";
        vm.IdTipoOportunidad = "-1";
        vm.Codigo = "-1";
        vm.PorcentajeCierre = "";
        ListarTipoComplejidad();
        vm.listComplejidad = [];
        ObtenerEtapa();
        vm.listEtapas = [];
        ListarComboSegmentos();
        vm.listSegmentosNegocios = [];
        ListarComboEstadosCaso();
        vm.listEstadosCaso = [];
        ListarComboTipoOportunidad();
        vm.listTiposOportunidad = [];

        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarTipoComplejidad() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 160
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listComplejidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[EtapaOportunidad] que esten Activos.
        function ObtenerEtapa() {
                var promise = EtapaService.ListarComboEtapas();
                promise.then(function (response) {
                    if (response == "") {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Etapas", 5);
                    } else {
                        vm.listEtapas = UtilsFactory.AgregarItemSelect(response.data);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[SegmentoNegocio] que esten Activos.
        function ListarComboSegmentos() {
            var promise = SegmentosNegocioService.ListarComboSegmentoNegocioSimple();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Segmentos de Negocio", 5);
                } else {
                    vm.listSegmentosNegocios = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[EstadoCaso] que esten Activos.
        function ListarComboEstadosCaso() {
            var promise = EstadoCasoService.ListarComboEstadoCaso();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Estados de Caso", 5);
                } else {
                    vm.listEstadosCaso = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[TipoOportunidad] que esten Activos.
        function ListarComboTipoOportunidad() {

            var promise = TipoOportunidadService.ListarComboTipoOportunidad();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Tipos de Oportunidad", 5);
                } else {
                    vm.listTiposOportunidad = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }

        vm.GenerarReporte = GenerarReporte;

        function GenerarReporte() {
            blockUI.start();
                    var parametros = "reporte=TrazabilidadOportunidad&DescripcionOportunidad=" + vm.DescripcionOportunidad +
                        "&DescripcionCliente=" + vm.DescripcionCliente +
                        "&PorcentajeCierre=" + vm.PorcentajeCierre +
                        "&IdEtapa=" + vm.IdEtapa +
                        "&IdEstadoCaso=" + vm.IdEstadoCaso +
                        "&IdSegmentoNegocio=" + vm.IdSegmentoNegocio +
                        "&IdTipoOportunidad=" + vm.IdTipoOportunidad +
                        "&Codigo=" + vm.Codigo;
                    debugger;
                    $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
                    vm.FlagMostrarReporte = true;

                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DetalleGeneradoOk, 5);
        }

        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.DescripcionOportunidad = "";
            vm.DescripcionCliente = "";
            vm.IdEtapa = "-1";
            vm.IdEstadoCaso = "-1";
            vm.IdSegmentoNegocio = "-1";
            vm.IdTipoOportunidad = "-1";
            vm.Codigo = "-1";
            vm.PorcentajeCierre = "";
        }
        
    }
})();