﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RptFCV', RptFCV);
    RptFCV.$inject = ['RptFCVService', 'MaestraService', 'EtapaService', 'RptFCVGrillaBusquedaRecursoService', 'RecursoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RptFCV(RptFCVService, MaestraService, EtapaService, RptFCVGrillaBusquedaRecursoService, RecursoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.ListaAnios = "";
        vm.IdMesesInicio = "-1";
        vm.IdMesesFin = "-1";
        vm.IdEtapa = "-1";
        vm.IdRecurso = "";
        vm.IdReporte = "-1";
        vm.IdEstadoOportunidad = "-1";
        ListarEstadoOportunidad();
        vm.listEstadoOportunidad = [];
        ObtenerEtapa();
        vm.listEtapas = [];
        vm.listMeses = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" },
            { Codigo: '01', Descripcion: 'Enero' },
            { Codigo: '02', Descripcion: 'Febrero' },
            { Codigo: '03', Descripcion: 'Marzo' },
            { Codigo: '04', Descripcion: 'Abril' },
            { Codigo: '05', Descripcion: 'Mayo' },
            { Codigo: '06', Descripcion: 'Junio' },
            { Codigo: '07', Descripcion: 'Julio' },
            { Codigo: '08', Descripcion: 'Agosto' },
            { Codigo: '09', Descripcion: 'Septiembre' },
            { Codigo: '10', Descripcion: 'Obtubre' },
            { Codigo: '11', Descripcion: 'Noviembre' },
            { Codigo: '12', Descripcion: 'Diciembre' }];
        vm.listReportes = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "FCV Grilla" }, { "Codigo": "2", "Descripcion": "FCV 2 Grafico de Barras" }];
        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarEstadoOportunidad() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 11
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listEstadoOportunidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[EtapaOportunidad] que esten Activos.
        function ObtenerEtapa() {
                var promise = EtapaService.ListarComboEtapas();
                promise.then(function (response) {
                    if (response == "") {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Etapas", 5);
                    } else {
                        vm.listEtapas = UtilsFactory.AgregarItemSelect(response.data);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
        }

        vm.GenerarReporte = GenerarReporte;

        function GenerarReporte() {
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();

                if ($.trim(vm.IdReporte) == "1") {

                    var parametros = "reporte=TrazabilidadFCVGrilla&Anio=" + vm.ListaAnios +
                            "&MesInicio=" + vm.IdMesesInicio +
                            "&MesFin=" + vm.IdMesesFin +
                            "&IdEtapa=" + vm.IdEtapa +
                            "&IdRecurso=" + vm.IdRecurso +
                            "&IdEstadoOportunidad=" + vm.IdEstadoOportunidad;

                    $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
                    vm.FlagMostrarReporte = true;

                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'success', 'Reporte se genero correctamente', 5);

                } else {
                    var parametros = "reporte=TrazabilidadFcv2GraficoBarras&Anio=" + vm.ListaAnios +
                        "&MesInicio=" + vm.IdMesesInicio +
                        "&MesFin=" + vm.IdMesesFin +
                        "&IdEtapa=" + vm.IdEtapa;

                    $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
                    vm.FlagMostrarReporte = true;

                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'success', 'Reporte se genero correctamente', 5);
                }


            }
            else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.ListaAnios) == "") {
                mensaje = mensaje + "Seleccione Año" + '<br/>';
                UtilsFactory.InputBorderColor('#CmbAnios', 'Rojo');
            }
            if ($.trim(vm.IdMesesInicio) == "-1") {
                mensaje = mensaje + "Seleccione Mes Inicio" + '<br/>';
                UtilsFactory.InputBorderColor('#IdMesesInicio', 'Rojo');
            }
            if ($.trim(vm.IdMesesFin) == "-1") {
                mensaje = mensaje + "Seleccione Mes Fin" + '<br/>';
                UtilsFactory.InputBorderColor('#IdMesesFin', 'Rojo');
            }
            if ($.trim(vm.IdMesesFin) < $.trim(vm.IdMesesInicio)) {
                mensaje = mensaje + "El Mes Fin tiene que ser mayor que el Mes Inicio" + '<br/>';
                UtilsFactory.InputBorderColor('#IdMesesFin', 'Rojo');
                UtilsFactory.InputBorderColor('#IdMesesInicio', 'Rojo');
            }
            if ($.trim(vm.IdReporte) == "-1") {
                mensaje = mensaje + "Seleccione Tipo de Reporte" + '<br/>';
                UtilsFactory.InputBorderColor('#IdReporte', 'Rojo');
            }

            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#CmbAnios', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdMesesInicio', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdMesesFin', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdReporte', 'Ninguno');
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.ListaAnios = "";
            vm.IdMesesInicio = "-1";
            vm.IdMesesFin = "-1";
            vm.IdRecurso = "";
            vm.HDescripcionRecurso = "";
            vm.IdEtapa = "-1";
            vm.IdReporte = "-1";
            vm.IdEstadoOportunidad = "-1";
        }


        //Modal Buscar Recurso
        vm.ModalBusquedaRecurso = ModalBusquedaRecurso;

        function ModalBusquedaRecurso() {

            var promise = RptFCVGrillaBusquedaRecursoService.ModalRptFCVGrillaBusquedaRecurso();
            promise.then(function (response) {
                var respuesta = $(response.data);
                debugger;
                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoRptFCVGrillaBusquedaRecurso").html(content);
                });

                $('#ModalRptFCVGrillaBusquedaRecurso').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };


        //Busca Id Seleccionado de Recurso 
        vm.BuscarRecursoIdRpt = BuscarRecursoIdRpt;
        function BuscarRecursoIdRpt() {
            CargaRecursoRpt(vm.IdRecurso);
        }

        function CargaRecursoRpt(IdRecurso) {
            var recurso = {
                IdRecurso: IdRecurso
            };
            var promise = RecursoService.ObtenerRecursoPorId(recurso);
            promise.then(function (resultado) {
                vm.HDescripcionRecurso = resultado.data.Nombre;
            }, function (response) {
                LimpiarGrillaR();
            });
        }
        
    }
})();