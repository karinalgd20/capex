﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RptQuiebresOportunidades', RptQuiebresOportunidades);
    RptQuiebresOportunidades.$inject = ['RptQuiebresOportunidadesService', 'MaestraService', 'EtapaService', 'SegmentosNegocioService', 'ConceptoSeguimientoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RptQuiebresOportunidades(RptQuiebresOportunidadesService, MaestraService, EtapaService, SegmentosNegocioService, ConceptoSeguimientoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;  
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.FechaInicio = "";
        vm.FechaFin = "";
        vm.IdEtapa = "-1";
        vm.IdSegmentoNegocio = "-1";
        vm.IdEstadoOportunidad = "-1";
        vm.IdNivel = "1";
        ListarEstadoOportunidad();
        vm.listEstadoOportunidad = [];
        ObtenerEtapa();
        vm.listEtapas = [];
        ListarComboSegmentos();
        vm.listSegmentosNegocios = [];
        ListarNiveles();
        vm.listNiveles = [];

        //Lista los IdNiveles  de la tabla [COMUN].[ConceptoSeguimiento] con distinct que esten Activos.
        function ListarNiveles() {
            var promise = ConceptoSeguimientoService.ListarComboConceptoSeguimientoNiveles();
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listNiveles = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarEstadoOportunidad() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 11
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listEstadoOportunidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[EtapaOportunidad] que esten Activos.
        function ObtenerEtapa() {
                var promise = EtapaService.ListarComboEtapas();
                promise.then(function (response) {
                    if (response == "") {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Etapas", 5);
                    } else {
                        vm.listEtapas = UtilsFactory.AgregarItemSelect(response.data);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[SegmentoNegocio] que esten Activos.
        function ListarComboSegmentos() {
            var promise = SegmentosNegocioService.ListarComboSegmentoNegocioSimple();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Segmentos de Negocio", 5);
                } else {
                    vm.listSegmentosNegocios = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }

        vm.GenerarReporte = GenerarReporte;

        function GenerarReporte() {
            debugger;
            var mensaje = ValidarCampos();
            if (mensaje == "") {
            blockUI.start();
            var parametros = "reporte=TrazabilidadQuiebresOportunidades&FechaInicio=" + vm.FechaInicio +
                    "&FechaFin=" + vm.FechaFin +
                    "&IdEtapa=" + vm.IdEtapa +
                    "&IdSegmentoNegocio=" + vm.IdSegmentoNegocio +
                    "&IdEstadoOportunidad=" + vm.IdEstadoOportunidad +
                    "&IdNivel=" + vm.IdNivel;
            debugger;
                    $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
                    vm.FlagMostrarReporte = true;

                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'success', 'Reporte se genero correctamente', 5);

            }
            else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.FechaInicio) == "") {
                mensaje = mensaje + "Ingrese Fecha de Inicio" + '<br/>';
                UtilsFactory.InputBorderColor('#FechaInicio', 'Rojo');
            }
            if ($.trim(vm.FechaFin) == "") {
                mensaje = mensaje + "Ingrese Fecha Fin" + '<br/>';
                UtilsFactory.InputBorderColor('#FechaFin', 'Rojo');
            }
            if ($.trim(vm.IdNivel) == "-1") {
                mensaje = mensaje + "Seleccione Nivel" + '<br/>';
                UtilsFactory.InputBorderColor('#IdNivel', 'Rojo');
            }

            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#FechaInicio', 'Ninguno');
            UtilsFactory.InputBorderColor('#FechaFin', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdNivel', 'Ninguno');
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.FechaInicio = "";
            vm.FechaFin = "";
            vm.IdEtapa = "-1";
            vm.IdSegmentoNegocio = "-1";
            vm.IdEstadoOportunidad = "-1";
            vm.IdNivel = "1";
        }
        
    }
})();