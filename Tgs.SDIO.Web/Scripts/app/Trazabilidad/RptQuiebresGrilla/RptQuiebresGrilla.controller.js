﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RptQuiebresGrilla', RptQuiebresGrilla);
    RptQuiebresGrilla.$inject = ['MaestraService', 'EtapaService', 'SegmentosNegocioService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RptQuiebresGrilla( MaestraService, EtapaService, SegmentosNegocioService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.FechaInicio = "";
        vm.FechaFin = "";
        vm.IdEtapa = "-1";
        vm.IdSegmentoNegocio = "-1";
        vm.IdEstadoOportunidad = "-1";
        ListarEstadoOportunidad();
        vm.listEstadoOportunidad = [];
        ObtenerEtapa();
        vm.listEtapas = [];
        ListarComboSegmentos();
        vm.listSegmentosNegocios = [];

        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarEstadoOportunidad() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 11
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listEstadoOportunidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[EtapaOportunidad] que esten Activos.
        function ObtenerEtapa() {
                var promise = EtapaService.ListarComboEtapas();
                promise.then(function (response) {
                    if (response == "") {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Etapas", 5);
                    } else {
                        vm.listEtapas = UtilsFactory.AgregarItemSelect(response.data);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[SegmentoNegocio] que esten Activos.
        function ListarComboSegmentos() {
            var promise = SegmentosNegocioService.ListarComboSegmentoNegocioSimple();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Segmentos de Negocio", 5);
                } else {
                    vm.listSegmentosNegocios = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }

        vm.GenerarReporte = GenerarReporte;

        function GenerarReporte() {
            var mensaje = ValidarCampos();
            if (mensaje == "") {
            blockUI.start();
            var parametros = "reporte=TrazabilidadQuiebresGrilla&FechaInicio=" + vm.FechaInicio +
                    "&FechaFin=" + vm.FechaFin +
                    "&IdEtapa=" + vm.IdEtapa +
                    "&IdSegmentoNegocio=" + vm.IdSegmentoNegocio +
                    "&IdEstadoOportunidad=" + vm.IdEstadoOportunidad;

                    $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
                    vm.FlagMostrarReporte = true;

                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'success', 'Reporte se genero correctamente', 5);

            }
            else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.FechaInicio) == "") {
                mensaje = mensaje + "Ingrese Fecha de Inicio" + '<br/>';
                UtilsFactory.InputBorderColor('#FechaInicio', 'Rojo');
            }
            if ($.trim(vm.FechaFin) == "") {
                mensaje = mensaje + "Ingrese Fecha Fin" + '<br/>';
                UtilsFactory.InputBorderColor('#FechaFin', 'Rojo');
            }

            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#FechaInicio', 'Ninguno');
            UtilsFactory.InputBorderColor('#FechaFin', 'Ninguno');
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.FechaInicio = "";
            vm.FechaFin = "";
            vm.IdEtapa = "-1";
            vm.IdSegmentoNegocio = "-1";
            vm.IdEstadoOportunidad = "-1";
        }
        
    }
})();