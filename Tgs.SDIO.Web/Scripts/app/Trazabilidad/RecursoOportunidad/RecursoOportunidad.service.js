﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RecursoOportunidadService', RecursoOportunidadService);

    RecursoOportunidadService.$inject = ['$http', 'URLS'];

    function RecursoOportunidadService($http, $urls) {

        var service = {
            ObtenerRecursoOportunidadPorId: ObtenerRecursoOportunidadPorId,
            ActualizarRecursoOportunidad: ActualizarRecursoOportunidad,
            RegistrarRecursoOportunidad: RegistrarRecursoOportunidad
            
        };

        return service;

        function ObtenerRecursoOportunidadPorId(recurso) {
            return $http({
                url: $urls.ApiTrazabilidad + "RecursoOportunidad/ObtenerRecursoOportunidadPorId",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarRecursoOportunidad(recurso) {
            return $http({
                url: $urls.ApiTrazabilidad + "RecursoOportunidad/ActualizarRecursoOportunidad",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarRecursoOportunidad(recurso) {
            return $http({
                url: $urls.ApiTrazabilidad + "RecursoOportunidad/RegistrarRecursoOportunidad",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();