﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('CargaExcelDatosPreventaMovilService', CargaExcelDatosPreventaMovilService);

    CargaExcelDatosPreventaMovilService.$inject = ['$http', 'URLS'];

    function CargaExcelDatosPreventaMovilService($http, $urls) {

        var service = {
            ModalCargaExcelDatosPreventaMovil: ModalCargaExcelDatosPreventaMovil
            
        };

        return service;

          function ModalCargaExcelDatosPreventaMovil() {

            return $http({
                url: $urls.ApiTrazabilidad + "CargaExcelDatosPreventaMovil/Index",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();