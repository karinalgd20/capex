﻿(function () {
    'use strict'

    angular
    .module('app.Trazabilidad')
    .controller('CargaExcelDatosPreventaMovil', CargaExcelDatosPreventaMovil);

    CargaExcelDatosPreventaMovil.$inject = ['RegistrarDatosPreventaMovilService', 'MaestraService', 'EstadoCasoService', 'SegmentosNegocioService', 'TipoOportunidadService', 'FuncionPropietarioService', 'SectorService', 'MotivoOportunidadService', 'DatosPreventaMovilService', 'OportunidadSTService', 'CasoOportunidadService', 'ClienteService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', 'Upload', 'ArchivoService'];

    function CargaExcelDatosPreventaMovil(RegistrarDatosPreventaMovilService, MaestraService, EstadoCasoService, SegmentosNegocioService, TipoOportunidadService, FuncionPropietarioService, SectorService, MotivoOportunidadService, DatosPreventaMovilService, OportunidadSTService, CasoOportunidadService, ClienteService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, Upload, ArchivoService) {

        var vm = this;
        vm.CargarOportunidadesVenta = CargarOportunidadesVenta;
        //vm.CargaFile = CargaFile;
        vm.NombreArchivo = "";

        vm.CargaMensaje = "Ningun archivo seleccionado.";

        function LimpiarCampos() {
            vm.NombreArchivo = "";
            vm.CargaMensaje = "";
        }



        function CargarOportunidadesVenta() {
            blockUI.start();
            if ($.trim(vm.TxtArchivos.length) == 0) {
                UtilsFactory.Alerta('#lblAlertArchivo', 'warning', 'Seleccione un archivo', 5);
                return;
            }

            var archivo = {
                CodigoTabla: 105,
                IdEntidad: 1,
                IdCategoria: 101,//REQUERIDO
                Descripcion: 'Descripcion Ejemplo'
            };

            var formData = new FormData();
            formData.append('cargaArchivo', angular.toJson(archivo));
            angular.forEach(vm.TxtArchivos, function (file) {
                formData.append('files', file);

            });
            var promise = DatosPreventaMovilService.RegistrarArchivo(formData);
            promise.then(function (resultado) {
                debugger;
                blockUI.stop();
                var Respuesta = resultado.data;
                   if (Respuesta.TipoRespuesta == 0) {
                       UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                 } else {
                       UtilsFactory.Alerta('#divAlert', 'danger', Respuesta.Mensaje, 5);
                 }
                vm.DescripcionArchivo = '';

            }, function (response) {
                blockUI.stop();
            });
        }



        function ValidarCampos() {

            var mensaje = "";
            if ($.trim(vm.NombreArchivo) == "") {
                mensaje = mensaje + "No hay Excel Cargado" + '<br/>';
                UtilsFactory.InputBorderColor('#NombreArchivo', 'Rojo');
            }

            if ($.trim(vm.ExtencionArchivo) != "xlsx") {
                mensaje = mensaje + "El tipo de Archivo tiene que ser Excel." + '<br/>';
                UtilsFactory.InputBorderColor('#NombreArchivo', 'Rojo');
            }


            return mensaje;
        }

        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#NombreArchivo', 'Ninguno');

        }
    }
})();








