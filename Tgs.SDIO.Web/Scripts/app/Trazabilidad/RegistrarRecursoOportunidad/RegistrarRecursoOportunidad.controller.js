﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RegistrarRecursoOportunidad', RegistrarRecursoOportunidad);
    RegistrarRecursoOportunidad.$inject = ['RegistrarRecursoOportunidadService', 'OportunidadSTService', 'RecursoService', 'RolRecursoService', 'RecursoOportunidadBusquedaOportunidadService', 'RecursoOportunidadBusquedaRecursoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RegistrarRecursoOportunidad(RegistrarRecursoOportunidadService, OportunidadSTService, RecursoService, RolRecursoService, RecursoOportunidadBusquedaOportunidadService, RecursoOportunidadBusquedaRecursoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.CargarDatos = CargarDatos;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.RegistraRecursoOportunidad = RegistraRecursoOportunidad;
        vm.EnlaceBandeja = $urls.ApiTrazabilidad + "BandejaRecursoOportunidad/Index/";
        vm.IdAsignacion = "";
        vm.IdEstado = "1";
        vm.IdOportunidad = "";
        vm.IdRecurso = "";
        vm.IdRol = "-1";
        vm.PorcentajeDedicacion = "";
        vm.FInicioAsignacion = "";
        vm.FFinAsignacion = "";
        vm.Observaciones = "";
        CargarDatos();
        ListarComboRolRecurso();
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        //Lista los IdFase y Descripciones de la tabla [TRAZABILIDAD].[RolRecurso] que esten Activos.
        function ListarComboRolRecurso() {
            blockUI.start();
            var promise = RolRecursoService.ListarComboRolRecurso();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Roles", 5);
                } else {
                    vm.listRolRecurso = UtilsFactory.AgregarItemSelect(respuesta);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si existente.
        function CargarDatos() {
            if (jsonDatoRecursoOportunidad == null || jsonDatoRecursoOportunidad == "") {
                vm.IdAsignacion = "";
                vm.IdEstado = "1";
                vm.IdOportunidad = "";
                vm.IdRecurso = "";
                vm.IdRol = "-1";
                vm.PorcentajeDedicacion = "";
                vm.FInicioAsignacion = "";
                vm.FFinAsignacion = "";
                vm.Observaciones = "";
            } else {
                var dateParts1 = jsonDatoRecursoOportunidad.strFInicioAsignacion.split("/");
                var FechaInicio = new Date(dateParts1[2], dateParts1[1] - 1, dateParts1[0]);
                var dateParts2 = jsonDatoRecursoOportunidad.strFFinAsignacion.split("/");
                var FechaFin = new Date(dateParts2[2], dateParts2[1] - 1, dateParts2[0]);
                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdAsignacion = jsonDatoRecursoOportunidad.IdAsignacion;
                vm.Observaciones = jsonDatoRecursoOportunidad.Observaciones;
                vm.IdEstado = jsonDatoRecursoOportunidad.IdEstado;
                vm.IdOportunidad = jsonDatoRecursoOportunidad.IdOportunidad;
                vm.IdRecurso = jsonDatoRecursoOportunidad.IdRecurso;
                CargaRecurso(jsonDatoRecursoOportunidad.IdRecurso);
                CargarOportunidad(jsonDatoRecursoOportunidad.IdOportunidad);
                vm.IdRol = jsonDatoRecursoOportunidad.IdRol;
                vm.PorcentajeDedicacion = jsonDatoRecursoOportunidad.PorcentajeDedicacion;
                vm.FInicioAsignacion = FechaInicio;
                vm.FFinAsignacion = FechaFin;
            }
        }
        //Limpia los controles del formulacion a sus valores por defecto.
        function LimpiarCampos() {
            vm.IdAsignacion = "";
            vm.IdEstado = "1";
            vm.IdOportunidad = "";
            vm.IdRecurso = "";
            vm.IdRol = "-1";
            vm.PorcentajeDedicacion = "";
            vm.FInicioAsignacion = "";
            vm.FFinAsignacion = "";
            vm.Observaciones = "";
            vm.HDescripcionOportunidad = "";
            vm.HDescripcionRecurso = "";
        }
        //Registra en  la tabla [TRAZABILIDAD].[RecursoOportunidad]
        function RegistraRecursoOportunidad() {
            var recursoOportunidad = {
                IdAsignacion: vm.IdAsignacion,
                Observaciones: vm.Observaciones,
                IdEstado: vm.IdEstado,
                IdOportunidad: vm.IdOportunidad,
                IdRecurso: vm.IdRecurso,
                IdRol: vm.IdRol,
                PorcentajeDedicacion: vm.PorcentajeDedicacion,
                FInicioAsignacion: vm.FInicioAsignacion,
                FFinAsignacion: vm.FFinAsignacion
            };
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promise = RegistrarRecursoOportunidadService.RegistrarRecursoOportunidad(recursoOportunidad);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        LimpiarCampos();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar.", 5);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            }
            else {
                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";

            if ($.trim(vm.HDescripcionOportunidad) == "") {
                mensaje = mensaje + "Seleccione una Oportunidad" + '<br/>';
                UtilsFactory.InputBorderColor('#HDescripcionOportunidad', 'Rojo');
            }
            if ($.trim(vm.HDescripcionRecurso) == "") {
                mensaje = mensaje + "Seleccione un Recurso" + '<br/>';
                UtilsFactory.InputBorderColor('#HDescripcionRecurso', 'Rojo');
            }
            if ($.trim(vm.IdRol) == "-1") {
                mensaje = mensaje + "Seleccione Rol de Recurso" + '<br/>';
                UtilsFactory.InputBorderColor('#IdRol', 'Rojo');
            }
            if ($.trim(vm.PorcentajeDedicacion) == "-1") {
                mensaje = mensaje + "Ingrese Porcentaje de dedicacion" + '<br/>';
                UtilsFactory.InputBorderColor('#PorcentajeDedicacion', 'Rojo');
            }

            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }
            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#HDescripcionOportunidad', 'Ninguno');
            UtilsFactory.InputBorderColor('#HDescripcionRecurso', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdRol', 'Ninguno');
            UtilsFactory.InputBorderColor('#PorcentajeDedicacion', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
        }

        //Modal Buscar Oportunidad 

        vm.ModalBusquedaOportunidad = ModalBusquedaOportunidad;

        function ModalBusquedaOportunidad() {
            debugger;

            var promise = RecursoOportunidadBusquedaOportunidadService.ModalRecursoOportunidadBusquedaOportunidadService();
            promise.then(function (response) {
                debugger;
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoBusquedaOportunidad").html(content);
                });

                $('#ModalBusquedaOportunidad').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        //Modal Buscar Recurso
        vm.ModalBusquedaRecurso = ModalBusquedaRecurso;

        function ModalBusquedaRecurso() {

            var promise = RecursoOportunidadBusquedaRecursoService.ModalRecursoOportunidadBusquedaRecurso();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoBusquedaRecurso").html(content);
                });

                $('#ModalBusquedaRecurso').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };
        //Busca Id Seleccionado de Oportunidad 
        vm.BuscarOportunidadId = BuscarOportunidadId;
        function BuscarOportunidadId() {
            CargarOportunidad(vm.IdOportunidad);
        }
        function CargarOportunidad(IdOportunidad) {
            var oportunidad = {
                IdOportunidad: IdOportunidad
            };
            var promise = OportunidadSTService.ObtenerOportunidadSTPorId(oportunidad);
            promise.then(function (resultado) {
                vm.HDescripcionOportunidad = resultado.data.Descripcion;
            }, function (response) {
                LimpiarGrilla();
            });
        }
        //Busca Id Seleccionado de Recurso 
        vm.BuscarRecursoId = BuscarRecursoId;
        function BuscarRecursoId() {
            CargaRecurso(vm.IdRecurso);
        }

        function CargaRecurso(IdRecurso) {
            var recurso = {
                IdRecurso: IdRecurso
            };
            var promise = RecursoService.ObtenerRecursoPorId(recurso);
            promise.then(function (resultado) {
                vm.HDescripcionRecurso = resultado.data.Nombre;
            }, function (response) {
                LimpiarGrillaR();
            });
        }

    }
})();