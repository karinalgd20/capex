﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('ObservacionOportunidadBusquedaOportunidadCasoService', ObservacionOportunidadBusquedaOportunidadCasoService);

    ObservacionOportunidadBusquedaOportunidadCasoService.$inject = ['$http', 'URLS'];

    function ObservacionOportunidadBusquedaOportunidadCasoService($http, $urls) {

        var service = {
            SeleccionarOportunidadCaso: SeleccionarOportunidadCaso                       
        };

        return service;

        function SeleccionarOportunidadCaso() {
            return $http({
                url: $urls.ApiTrazabilidad + "ObservacionOportunidadBusquedaOportunidadCaso/Index",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
    }
})();