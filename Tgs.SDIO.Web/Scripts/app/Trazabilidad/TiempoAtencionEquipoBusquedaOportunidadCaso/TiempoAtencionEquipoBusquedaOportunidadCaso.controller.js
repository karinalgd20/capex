﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('ObservacionOportunidadBusquedaOportunidadCaso', ObservacionOportunidadBusquedaOportunidadCaso);
    ObservacionOportunidadBusquedaOportunidadCaso.$inject = ['ObservacionOportunidadBusquedaOportunidadCasoService', 'OportunidadSTService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function ObservacionOportunidadBusquedaOportunidadCaso(ObservacionOportunidadBusquedaOportunidadCasoService, OportunidadSTService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.BuscarOportunidadST = BuscarOportunidadST;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.HDescripcionCaso = "";
        vm.HDescripcionOportunidad = "";
        vm.DescripcionOportunidad = "";
        vm.DescripcionCaso = "";
        vm.IdCasoSF = "";
        vm.IdOportunidadSF = "";
        vm.SeleccionarOportunidadSF = SeleccionarOportunidadSF;
        LimpiarGrilla();
        vm.dtColumns = [
          DTColumnBuilder.newColumn('IdOportunidad').withTitle('ID').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('IdOportunidadSF').withTitle('Oportunidad SF').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('DescripcionOportunidad').withTitle('Nombre Oportunidad').notSortable().withOption('width', '22%'),
          DTColumnBuilder.newColumn('IdCaso').withTitle('ID Caso').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('IdCasoSF').withTitle('Nro del Caso SF').notSortable().withOption('width', '12%'),
          DTColumnBuilder.newColumn('DescripcionCaso').withTitle('Descripción Caso').notSortable().withOption('width', '20%'),
          DTColumnBuilder.newColumn('DescripcionSolicitud').withTitle('Tipo Solicitud').notSortable().withOption('width', '15%'),
          DTColumnBuilder.newColumn(null).withTitle('Acción').notSortable().renderWith(AccionesBusquedaOportunidadSF).withOption('width', '1%')
        ];
        //variables Globales
        var modalpopupBuscar = angular.element(document.getElementById("dlgBuscar"));
        //Funcion carga input de oportunidad por su Id
        function CargarOportunidad(IdOportunidad) {
            var oportunidad = {
                IdOportunidad: IdOportunidad
            };
            var promise = OportunidadSTService.ObtenerOportunidadSTPorId(oportunidad);
            promise.then(function (resultado) {
                vm.HDescripcionOportunidad = resultado.data.Descripcion;
            }, function (response) {
                LimpiarGrilla();
            });
        }
        //Funcion carga input de Caso por su Id
        function CargaCaso(IdCaso) {
            debugger;
            var caso = {
                IdCaso: IdCaso
            };
            var promise = CasoOportunidadService.ObtenerCasoOportunidadPorId(caso);
            promise.then(function (resultado) {
                debugger;
                vm.HDescripcionCaso = resultado.data.Descripcion;
            }, function (response) {
                LimpiarGrilla();
            });
        }
        //Evento de mostrar PopUp
        $("#btnBuscar").click(function () {
            modalpopupBuscar.modal('show');
            LimpiarFiltros();
        });

        //Filtro de la  tabla [TRAZABILIDAD].[OportunidadST] por Descripcion.
        function BuscarOportunidadST() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarOportunidadSTPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }
        function BuscarOportunidadSTPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var oportunidad = {
                DescripcionOportunidad: vm.DescripcionOportunidad,
                DescripcionCaso: vm.DescripcionCaso,
                IdCasoSF: vm.IdCasoSF,
                IdOportunidadSF: vm.IdOportunidadSF,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = OportunidadSTService.ListaOportunidadSTModalObservacionPaginado(oportunidad);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListOportunidadSTDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withOption('scrollX', true)
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.DescripcionOportunidad = "";
            vm.DescripcionCaso = "";
            vm.IdCasoSF = "";
            vm.IdOportunidadSF = "";
            LimpiarGrilla();
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusquedaOportunidadSF(data, type, full, meta) {
            var respuesta = "";
            respuesta = respuesta + " <a title='Editar'   ng-click='vm.SeleccionarOportunidadSF(" + data.IdCaso + ", " + data.IdOportunidad + ");'>" + "<span class='glyphicon glyphicon-hand-right' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            return respuesta;
        }

        //Selecciona registro
        function SeleccionarOportunidadSF(IdCaso, IdOportunidad) {
            debugger;
            $scope.$parent.vm.IdOportunidad = IdOportunidad;
            $scope.$parent.vm.IdCaso = IdCaso;
            $scope.$parent.vm.BuscarOportunidadId();
            $("#ModalBusquedaOportunidadCaso").modal('hide');
        }

    }
})();