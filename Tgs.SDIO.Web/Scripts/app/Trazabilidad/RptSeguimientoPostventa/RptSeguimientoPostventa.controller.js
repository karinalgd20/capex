﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RptSeguimientoPostventa', RptSeguimientoPostventa);
    RptSeguimientoPostventa.$inject = ['RptSeguimientoPostventaService', 
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RptSeguimientoPostventa(RptSeguimientoPostventaService, 
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;  
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.FechaInicio = "";
        vm.FechaFin = "";


        vm.GenerarReporte = GenerarReporte;

        function GenerarReporte() {

            var mensaje = ValidarCampos();
            if (mensaje == "") {
            blockUI.start();
            var parametros = "reporte=TrazabilidadSeguimientoPostventa&FechaInicio=" + vm.FechaInicio +
                    "&FechaFin=" + vm.FechaFin;

                    $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
                    vm.FlagMostrarReporte = true;

                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlert', 'success', 'Reporte se genero correctamente', 5);

            }
            else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.FechaInicio) == "") {
                mensaje = mensaje + "Ingrese Fecha de Inicio" + '<br/>';
                UtilsFactory.InputBorderColor('#FechaInicio', 'Rojo');
            }
            if ($.trim(vm.FechaFin) == "") {
                mensaje = mensaje + "Ingrese Fecha Fin" + '<br/>';
                UtilsFactory.InputBorderColor('#FechaFin', 'Rojo');
            }

            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#FechaInicio', 'Ninguno');
            UtilsFactory.InputBorderColor('#FechaFin', 'Ninguno');

        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.FechaInicio = "";
            vm.FechaFin = "";
        }
        
    }
})();