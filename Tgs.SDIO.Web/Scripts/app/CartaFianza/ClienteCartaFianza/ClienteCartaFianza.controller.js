﻿(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('ClienteCartaFianzaController', ClienteCartaFianzaController);

    ClienteCartaFianzaController.$inject = ['ClienteCartaFianzaService','ClienteService', 'blockUI', 'UtilsFactory', 'DTColumnDefBuilder', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function ClienteCartaFianzaController(ClienteCartaFianzaService,ClienteService, blockUI, UtilsFactory, DTColumnDefBuilder, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.BuscarClienteCartaFianza = BuscarClienteCartaFianza;
        vm.SelecionarClienteId = SelecionarClienteId;
        vm.LimpiarFiltros = LimpiarFiltros;

        vm.NumeroIdentificadorFiscal = '';
        vm.NombreCliente = '';
        vm.IdCliente = 0;
        vm.IdEstado = 1;

        vm.dataMasiva = [];
        vm.dtBuscarClienteInstanceCartaFianza = {};
        vm.dtBuscarClienteColumnsCartaFianza = [
        DTColumnBuilder.newColumn('IdCliente').notVisible(), 
        DTColumnBuilder.newColumn('NumeroIdentificadorFiscal').withTitle('RUC/NIF').notSortable().withOption('width', '20%'),
        DTColumnBuilder.newColumn('Descripcion').withTitle('Cliente').withOption('width', '70%'),

       DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCartaFianza).withOption('width', '6%')
        ];

        function AccionesCartaFianza(data, type, full, meta) {

            var respuesta = "<center><a title='Selecionar'  data-dismiss='modal' ng-click='vm.SelecionarClienteId(\"" + data.IdCliente + "\",\"" + data.NumeroIdentificadorFiscal + "\",\"" + data.Descripcion + "\");'>" + "<span class='glyphicon glyphicon-ok style='background-color: #00A4B6;'></span></a> </center>";
            return respuesta;
        };
        //id='tab-button' class='btn btn-primary' 


        function BuscarClienteCartaFianza() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsCartaFianza = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarCartaFianzaClientePaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarCartaFianzaClientePaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var clienteCartaFianza = {
                NumeroIdentificadorFiscal: vm.bClienteNumeroIdentificador,
                Descripcion: vm.bClienteNombreCliente,
                IdEstado: vm.IdEstado,
                Indice: pageNumber,
                Tamanio: length,
                ColumnName: 'IdCliente',
                OrderType: 'asc'
            };

            var promise = ClienteService.ListarClientePaginado(clienteCartaFianza);
            promise.then(function (response) {
        
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListCliente
                };

                blockUI.stop();
                fnCallback(records);
 
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsCartaFianza = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
    
        };



        BuscarClienteCartaFianza();

        function SelecionarClienteId(idCliente, numeroIdentificadorFiscal, descripcion)
        {
           
            vm.NumeroIdentificadorFiscal = numeroIdentificadorFiscal;
            vm.NombreCliente = descripcion;
            vm.IdCliente = idCliente;
            var IdClienteb = $scope.$parent.vm.IdCliente ;

            if(    IdClienteb >= 0 ){ /*    maestro   */
                $scope.$parent.vm.BNombreCliente = vm.NombreCliente;
            $scope.$parent.vm.BNumeroIdentificador = vm.NumeroIdentificadorFiscal,
            $scope.$parent.vm.IdCliente = vm.IdCliente;
             UtilsFactory.Alerta('#divAlertBuscarCliente', 'success', "Cliente selecionado " + vm.NumeroIdentificadorFiscal + " - " + vm.NombreCliente, 5);
         
            $scope.$parent.vm.CargaModalCartaFianzaRegistro();
            }
            if( IdClienteb == -5){  /*    sol de devolucion   */
                $scope.$parent.vm.NombreCliente = vm.NombreCliente;
                $scope.$parent.vm.NumeroIdentificadorFiscal = vm.NumeroIdentificadorFiscal,
                $scope.$parent.vm.IdCliente = vm.IdCliente;
             UtilsFactory.Alerta('#divAlertBuscarCliente', 'success', "Cliente selecionado " + vm.NumeroIdentificadorFiscal + " - " + vm.NombreCliente, 5);
         
            $scope.$parent.vm.BuscarReporteSolDevolucion();
            }

          if(  IdClienteb == -6){ /*    sol de renovacion   */
                $scope.$parent.vm.NombreCliente = vm.NombreCliente;
                $scope.$parent.vm.NumeroIdentificadorFiscal = vm.NumeroIdentificadorFiscal,
                $scope.$parent.vm.IdCliente = vm.IdCliente;
             UtilsFactory.Alerta('#divAlertBuscarCliente', 'success', "Cliente selecionado " + vm.NumeroIdentificadorFiscal + " - " + vm.NombreCliente, 5);
         
            $scope.$parent.vm.BuscarReporteSolRenovacion();
            }
        


           
           
      
        }

        function LimpiarFiltros()
        {
            //vm.NumeroIdentificadorFiscal = null;
            //vm.NombreCliente = null;
            vm.bClienteNumeroIdentificador = null;
            vm.bClienteNombreCliente = null;
        }


    }

})();