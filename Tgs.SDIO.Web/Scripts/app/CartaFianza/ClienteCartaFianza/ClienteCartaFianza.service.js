﻿(function () {
    'use strict',
     angular
    .module('app.CartaFianza')
    .service('ClienteCartaFianzaService', ClienteCartaFianzaService);

    ClienteCartaFianzaService.$inject = ['$http', 'URLS'];


    function ClienteCartaFianzaService($http, $urls) {


        var service = {

            ModalCartaFianzaBuscarCliente: ModalCartaFianzaBuscarCliente
        };
        return service;



        function ModalCartaFianzaBuscarCliente(dto) {
            //regCartaFianza = dto;
            // console.log(regCartaFianza);
            return $http({
                url: $urls.ApiCartaFianza + "ClienteCartaFianza/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };


     

        
    }



})();