﻿var vm;
(function () {
    'use strict'
    angular
   .module('app.CartaFianza')
   .controller('RenovacionCartaFianzaController', RenovacionCartaFianzaController);

    RenovacionCartaFianzaController.$inject = [
                 'RenovacionCartaFianzaService',
                 'MaestroCartaFianzaService',
                  'RegistroCartaFianzaService',
                 'CorreoService',
                 'MaestraService',
                 'AccionCartaFianzaService',
                 'ReporteCartaFianzaRenovacionGarantiaService',
                 'blockUI',
                 'UtilsFactory',
                 'DTColumnDefBuilder',
                 'DTOptionsBuilder',
                 'DTColumnBuilder',
                 '$timeout',
                 'MensajesUI',
                 'URLS',
                 '$scope',
                 '$compile',
                 '$modal',
                 '$injector'
    ];

    function RenovacionCartaFianzaController(
                 RenovacionCartaFianzaService,
                 MaestroCartaFianzaService,
                 RegistroCartaFianzaService,
                 CorreoService,
                 MaestraService,
                 AccionCartaFianzaService,
                 ReporteCartaFianzaRenovacionGarantiaService,
                 blockUI,
                 UtilsFactory,
                 DTColumnDefBuilder,
                 DTOptionsBuilder,
                 DTColumnBuilder,
                 $timeout,
                 MensajesUI,
                 $urls,
                 $scope,
                 $compile,
                 $modal,
                 $injector
             ) {

        vm = this;

        vm.BuscarCartaFianzaRenovacion = BuscarCartaFianzaRenovacion;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.CargarModalCartaFianzaRenovacion = CargarModalCartaFianzaRenovacion;
        vm.GuardarCartaFianzaRenovacion = GuardarCartaFianzaRenovacion;
        vm.CerrarPopup = CerrarPopup;
        vm.ValidarCartaFianzaRenovacion = ValidarCartaFianzaRenovacion;

        vm.CargaModalCartaFianzaId = CargaModalCartaFianzaId;

        vm.CambiaIconoEmailGerente = CambiaIconoEmailGerente;
        vm.CambiaIconoIndicador = CambiaIconoIndicador;
        vm.CambiaIconoEmailTesoreria = CambiaIconoEmailTesoreria;
        vm.CambiaIconoIndicadorTsr = CambiaIconoIndicadorTsr;

        vm.CargarTipoRequerida = CargarTipoRequerida;
        vm.CargarEntidadBancaria = CargarEntidadBancaria;
        vm.CargarTipoRenovacion = CargarTipoRenovacion;
        vm.CargarTipoValidacionRenovacion = CargarTipoValidacionRenovacion;

        vm.DetalleCartaFianzaAccion = DetalleCartaFianzaAccion;
        vm.VerificarItemSelecionadosCartaFianzaRevertirRenovacion = VerificarItemSelecionadosCartaFianzaRevertirRenovacion;
        vm.VerificarItemSelecionadosCartaFianzaRenovacion = VerificarItemSelecionadosCartaFianzaRenovacion;
        vm.RenovarCartaFianzaReversion = RenovarCartaFianzaReversion;
        vm.RenovarCartaFianza = RenovarCartaFianza;

        vm.ObtenerDatosCorreoTesoreria = ObtenerDatosCorreoTesoreria;
        vm.ObtenerDatosCorreoGC = ObtenerDatosCorreoGC;
        vm.SetearDatosCorreo = SetearDatosCorreo;

        vm.existeFecha = existeFecha;
        vm.monthDiff = monthDiff;
        vm.CalculaFechaVencimiento = CalculaFechaVencimiento;

        vm.DetalleCartaFianzaSolRenovacion = DetalleCartaFianzaSolRenovacion;
        vm.Renovar = Renovar;

        vm.DetalleRequerida = DetalleRequerida;
        vm.RequeridaCartaFianza = RequeridaCartaFianza;
        
        vm.SelecionarFechaRequerida = SelecionarFechaRequerida;
        vm.SelecionarFechaRequeridaBloqueado = SelecionarFechaRequeridaBloqueado;


        vm.CargarGerenteCuenta = CargarGerenteCuenta;
        vm.EnviarCorreoGC = EnviarCorreoGC;

        vm.renovacion = {};
        vm.search = {};
        vm.correo = {};

        vm.selected = {};
        vm.selectAll = false;
        vm.toggleAll = toggleAll;
        vm.toggleOne = toggleOne;
        vm.color;

       vm.AlertaError = AlertaError();

        vm.ListEntidadBancaria = [];
        vm.CmbIdTipoBancoReemplazo = '-1';

        vm.ListTipoRenovacion = [];
        vm.CmbTipoRenovacionTm = '-1';

            
	    vm.ListTipoValidacionRenovacion = [];
	   vm.CmbTipoValidacionRenovacionTm = '-1';

        /******************************************* Tabla - Registro de Carta Fianza *******************************************/
        vm.dtInstanceCartaFianzaRenovacion = {};

        var titleHtml = '<input type="checkbox" ng-model="vm.selectAll" ng-click="vm.toggleAll(vm.selected)">';

        vm.dtColumnsCartaFianza = [
            DTColumnBuilder.newColumn(null).withTitle(titleHtml)
                    .withOption('width', '4%')
                    .renderWith(function (data, type, full, meta) {
                        vm.selected[full.IdCartaFianzaDetalleRenovacion] = false;
                        if (!data.IdCartaFianzaInicial || data.IdCartaFianzaInicial == null || data.IdCartaFianzaInicial == '') return '<input type="checkbox"  ng-model="vm.selected[\'' + data.IdCartaFianzaDetalleRenovacion + '\']" ng-click="vm.toggleOne(vm.selected)">';
                        else return null;
                    }).notSortable(),

             DTColumnBuilder.newColumn(null).withTitle('Vcto.Ban').notSortable().renderWith(AccionesCartaFianzaIndicardor).withOption('width', '4%'),
                          
             DTColumnBuilder.newColumn(null).withTitle('Rtp_Tso').notSortable().renderWith(AccionesCartaFianzaIndicardorRtpTesoreria).withOption('width', '4%'),
            // DTColumnBuilder.newColumn('FechaVencimientoRenovacionStr').withTitle('Vcto.Renovación').notSortable().withOption('width', '3%'),
             DTColumnBuilder.newColumn('FechaVencimientoBancoRnvStr').withTitle('Vcto.RnvBanco').notSortable().withOption('width', '6%'),

             DTColumnBuilder.newColumn('IdCartaFianza').notVisible(),
             DTColumnBuilder.newColumn('IdCliente').notVisible(),
             DTColumnBuilder.newColumn('IdCartaFianzaDetalleRenovacion').notVisible(),

             DTColumnBuilder.newColumn('IdRenovarTm').withTitle('IdRenovarTm?').notVisible(),
             DTColumnBuilder.newColumn('EstadoRenovacion').withTitle('Renovar?').withOption('width', '5%'),

             DTColumnBuilder.newColumn('IdEstadoCartaFianzaTm').withTitle('IdEstadoCartaFianzaTm').notVisible(),
             DTColumnBuilder.newColumn('DesEstadoCartaFianzaTm').withTitle('Estado.C.F').notSortable().withOption('width', '5%'),
             DTColumnBuilder.newColumn('DesTipoAccionTm').withTitle('TipoAcción').withOption('width', '5%'),

             DTColumnBuilder.newColumn('NumeroGarantia').withTitle('N°.Garantia').notSortable().withOption('width', '5%'),
             DTColumnBuilder.newColumn('NumeroGarantiaReemplazo').withTitle('N°.Garantía.Re.').notSortable().withOption('width', '5%'),
             DTColumnBuilder.newColumn('Cliente').withTitle('Cliente').withOption('width', '15%'),
             DTColumnBuilder.newColumn('ImporteStr').withTitle('Importe').notSortable().withOption('width', '8%'),

             DTColumnBuilder.newColumn('FechaSolitudTesoriaStr').withTitle('Envio.Tesoreria').notSortable().withOption('width', '6%'),

             DTColumnBuilder.newColumn('NombreCompletoColaborador').withTitle('GerenteCuenta/SM').notSortable().withOption('width', '15%'),
             DTColumnBuilder.newColumn('EntidadBancaria').withTitle('Entidad.Bancaria').notSortable().withOption('width', '10%'),
             DTColumnBuilder.newColumn('desTipoRequeridaTm').withTitle('Requerida').notSortable().withOption('width', '4%'),

             DTColumnBuilder.newColumn('ValidacionContratoGcSm').notVisible(),
             DTColumnBuilder.newColumn('FechaRespuestaFinalGcSm').notVisible(),
             DTColumnBuilder.newColumn('FechaRecepcionCartaFianzaRenovacion').notVisible(),
             DTColumnBuilder.newColumn('FechaEntregaRenovacion').notVisible(),

             DTColumnBuilder.newColumn('FechaRenovacionEjecucionStr').notVisible(),
             DTColumnBuilder.newColumn('FechaDesestimarRequerimientoStr').notVisible(),
             DTColumnBuilder.newColumn('FechaEjecucionStr').notVisible(),

             DTColumnBuilder.newColumn('IdTipoRequeridaTm').notVisible(),

             DTColumnBuilder.newColumn('IdTipoAccionTm').notVisible(),
             DTColumnBuilder.newColumn('MotivoCartaFianzaRequerida').notVisible(),

             DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCartaFianza).withOption('width', '4%')
        ];

        function toggleAll(selectedItems) {
            for (var CodigoSolicitud in selectedItems) {
                if (selectedItems.hasOwnProperty(CodigoSolicitud)) {
                    selectedItems[CodigoSolicitud] = vm.selectAll;
                }
            }
        }

        function toggleOne(selectedItems) {
            for (var CodigoSolicitud in selectedItems) {

                if (selectedItems.hasOwnProperty(CodigoSolicitud)) {
                    if (!selectedItems[CodigoSolicitud]) {
                        vm.selectAll = false;
                        return;
                    }
                }
            }
            vm.selectAll = true;
        }

        function AccionesCartaFianzaIndicardor(data, type, full, meta) {
            let NumeroFila = meta.row;
            var respuesta;

            if ((data.FechaVencimientoBancoStr || data.FechaVencimientoBancoStr != '') && (!data.IdCartaFianzaInicial || data.IdCartaFianzaInicial == null || data.IdCartaFianzaInicial == '')) {
                        respuesta = "<center>";
                        respuesta = respuesta + "<div class='col-sm-2'><span id='indicardor" + data.IdCartaFianza + "' ng-click='vm.ObtenerDatosCorreoGC(" + NumeroFila + ")'  title='Enviar Correo G.C' ng-mouseover='vm.CambiaIconoEmailGerente(" + data.IdCartaFianza + ")'    ng-mouseleave='vm.CambiaIconoIndicador(" + data.IdCartaFianza + "," + data.IdIndicadorSemaforoColor + ")'  style='color:" + (data.FechaVencimientoBancostr == '' ? '' : data.IdIndicadorSemaforoColor) + ";'    class='" + (data.FechaVencimientoBancostr == '' ? '' : 'glyphicon glyphicon-circle-arrow-up') + "' ></span></a></div>";
                        respuesta = respuesta + "<div class='col-sm-2'> " + (data.FechaVencimientoBancostr == '' ? '' : data.IdIndicadorSemaforo) + " </div> </center>";


                        return respuesta;

            } else { return ''; }
        };

        function AccionesCartaFianzaIndicardorRtpTesoreria(data, type, full, meta) {
            let NumeroFila = meta.row;
            var respuesta;

            if ((data.FechaSolitudTesoriaStr || data.FechaSolitudTesoriaStr != "" || data.FechaSolitudTesoriaStr.length != 0) && (!data.IdCartaFianzaInicial || data.IdCartaFianzaInicial == null || data.IdCartaFianzaInicial == '')) {

                respuesta = "<center>  ";/// + "<span class='glyphicon glyphicon-circle-arrow-up' style = 'color:" + data.IdIndicadorSemaforoColorTpsTesoreria + ";'></span></a></div>";
                respuesta = respuesta + "<a><span id='indicardorTsr" + data.IdCartaFianza + "' ng-click='vm.ObtenerDatosCorreoTesoreria(" + NumeroFila + ")' title='Reenviar Sol. tso.' ng-mouseover='vm.CambiaIconoEmailTesoreria(" + data.IdCartaFianza + ")'    ng-mouseleave='vm.CambiaIconoIndicadorTsr(" + data.IdCartaFianza + "," + data.IdIndicadorSemaforoColorTpsTesoreria + ")'  style='color:" + (data.FechaSolitudTesoriaStr == '' ? '' : data.IdIndicadorSemaforoColorTpsTesoreria) + ";'    class='" + (data.FechaSolitudTesoriaStr == '' ? '' : 'glyphicon glyphicon-circle-arrow-up') + "' ></span></a>";
                respuesta = respuesta + "   <div class='col-sm-2'> " + data.IdIndicadorSemaforoTpsTesoreria + " </div>  </center>";

                return respuesta;
            }

            else if (!data.IdCartaFianzaInicial || data.IdCartaFianzaInicial == null || data.IdCartaFianzaInicial == '') { return "<center> <a title='Enviar Sol. tso.' ng-click='vm.ObtenerDatosCorreoTesoreria(" + NumeroFila + ")'><span class='glyphicon fa fa-envelope' style='color: #3949ab; margin-left: 1px;'></span></a></center>"; }
            else { return ""; };

        };

        function AccionesCartaFianza(data, type, full, meta) {
            let NumeroFila = meta.row;
            var respuesta = "";
            respuesta = respuesta + "<center>";

            respuesta = respuesta + "<a title='Editar C.F'   ng-click='vm.CargaModalCartaFianzaId(\"" + NumeroFila + "\");'>" + "<span class='glyphicon fa fa-edit' style='color: #3949ab; margin-left: 1px;'></span></a> ";
         
            respuesta = respuesta + " <a title='Editar Renovación' ng-click='vm.CargarModalCartaFianzaRenovacion(\"" + NumeroFila + "\");'>" + "<span class='glyphicon fa fa-pencil' style='color: #3949ab; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + " <a title='Acciones' ng-click='vm.DetalleCartaFianzaAccion(\"" + data.IdCliente + "\",\"" + data.IdCartaFianza + "\");'>" + "<span class='glyphicon fa fa-tasks' style='color:#3949ab; margin-left: 1px;'></span></a>";
            respuesta = respuesta + " <a title='Sol.Renovación' ng-click='vm.DetalleCartaFianzaSolRenovacion(\"" + NumeroFila + "\");'>" + "<span class='glyphicon fa fa-archive' style='color:#3949ab; margin-left: 1px;'></span></a>";
            respuesta = respuesta + "<a title='Requerida'   ng-click='vm.DetalleRequerida(" + NumeroFila + ");'>" + "<span class='glyphicon fa fa-retweet' style='color:#3949ab; margin-left: 5px;'></span></a>";

            respuesta = respuesta + "</div></center>";
            return respuesta;
        };//

        /***********************Carga Incial de Carta Fianza*************************/
        function BuscarCartaFianzaRenovacion() {
            blockUI.start();
            vm.selected = {};
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptionsCartaFianza = DTOptionsBuilder
                     .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                       .withFnServerData(BuscarCartaFianzaRenovacionPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)

                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })

                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarCartaFianzaRenovacionPaginado(sSource, aoData, fnCallback, oSettings) {

            var draw = aoData[0].value;
            var start = aoData[3].value;
            var size = aoData[4].value;
            var pageNumber = (start + size) / size;

            vm.search.Indice = pageNumber;
            vm.search.Tamanio = size;
            vm.search.SortName = aoData[1].value[oSettings.aLastSort[0].col].data;
            vm.search.sort = oSettings.aaSorting[0]._idx;

            var cartaFianza = vm.search;


            vm.selected = {};

            var promise = RenovacionCartaFianzaService.ListarCartaFianzaRenovacion(cartaFianza);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListCartaFianzaRenovacion
                };
                blockUI.stop();
                fnCallback(records);
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });

            $("#tbCartaFianzaRenovacion").removeClass("dtr-inline")
        };

        function LimpiarGrilla() {
            vm.dtOptionsCartaFianza = DTOptionsBuilder
            .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('destroy', true)
                .withOption('scrollCollapse', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('scrollX', true)
                .withOption('paging', false);

        };

        function LimpiarFiltros() {
            vm.search = {};
            vm.search.IdRenovarTm = '-1';
        }

        /***********************Modal para Modificar Carta Fianza*************************/
        function CargarModalCartaFianzaRenovacion(NumeroFila) {
            let that = this;
            let cartaFianzaDetalleRenovacion = that.dtInstanceCartaFianzaRenovacion.dataTable.fnGetData(NumeroFila);

            vm.IdentidificadoFiscal = cartaFianzaDetalleRenovacion.NumeroIdentificadorFiscal;
            vm.NombreCliente = cartaFianzaDetalleRenovacion.Cliente;
            vm.DescripcionCartaFianza = cartaFianzaDetalleRenovacion.DescripcionCartaFianza;
            vm.renovacion.IdCartaFianzaDetalleRenovacion = cartaFianzaDetalleRenovacion.IdCartaFianzaDetalleRenovacion;
            vm.renovacion.IdRenovarTm = cartaFianzaDetalleRenovacion.IdRenovarTm;


            var vFechaVencimientoRenovacionStr = cartaFianzaDetalleRenovacion.FechaVencimientoRenovacionStr;
            var vFechaVencimientoRenovacionStr = vFechaVencimientoRenovacionStr.substring(3, 5) + '-' + vFechaVencimientoRenovacionStr.substring(0, 2) + '-' + vFechaVencimientoRenovacionStr.substring(6, 10);
            vm.renovacion.FechaVencimientoRenovacion = new Date(vFechaVencimientoRenovacionStr);


            vm.renovacion.PeriodoMesRenovacion = cartaFianzaDetalleRenovacion.PeriodoMesRenovacion;
            vm.renovacion.SustentoRenovacion = cartaFianzaDetalleRenovacion.SustentoRenovacion;
            vm.renovacion.Observacion = cartaFianzaDetalleRenovacion.Observacion;

            vm.ImporteStr = cartaFianzaDetalleRenovacion.ImporteStr;

            vm.renovacion.CartaFianzaReemplazo = cartaFianzaDetalleRenovacion.CartaFianzaReemplazo;
            vm.renovacion.FechaSolitudTesoria = cartaFianzaDetalleRenovacion.FechaSolitudTesoria;
            vm.renovacion.NumeroGarantia = cartaFianzaDetalleRenovacion.NumeroGarantia;
            vm.renovacion.NumeroGarantiaReemplazo = cartaFianzaDetalleRenovacion.NumeroGarantiaReemplazo;
            vm.renovacion.IdTipoBancoReemplazo = cartaFianzaDetalleRenovacion.IdTipoBancoReemplazo;

            vm.renovacion.ValidacionContratoGcSm = cartaFianzaDetalleRenovacion.ValidacionContratoGcSm;


            var vFechaEmisionBancoRnvStr = cartaFianzaDetalleRenovacion.FechaEmisionBancoRnvStr;
            // if(vFechaEmisionBancoRnvStr !=''){
            var vFechaEmisionBancoRnvStr = vFechaEmisionBancoRnvStr.substring(3, 5) + '-' + vFechaEmisionBancoRnvStr.substring(0, 2) + '-' + vFechaEmisionBancoRnvStr.substring(6, 10);
            vm.renovacion.FechaEmisionBancoRnv = new Date(vFechaEmisionBancoRnvStr);
            //$('#FechaEmisionBancoRnv').attr('disabled', 'disabled');
            //} else { $('FechaEmisionBancoRnv').prop("disabled", true); }



            var vFechaVigenciaBancoRnvStr = cartaFianzaDetalleRenovacion.FechaVigenciaBancoRnvStr;
            //if (vFechaVigenciaBancoRnvStr != '') {
            var vFechaVigenciaBancoRnvStr = vFechaVigenciaBancoRnvStr.substring(3, 5) + '-' + vFechaVigenciaBancoRnvStr.substring(0, 2) + '-' + vFechaVigenciaBancoRnvStr.substring(6, 10);
            vm.renovacion.FechaVigenciaBancoRnv = new Date(vFechaVigenciaBancoRnvStr);
            // $('#FechaVigenciaBancoRnv').attr('disabled', 'disabled');
            //} else { $('FechaVigenciaBancoRnv').prop("disabled", true); }


            var vFechaVencimientoBancoRnvStr = cartaFianzaDetalleRenovacion.FechaVencimientoBancoRnvStr;
            //if (vFechaVencimientoBancoRnvStr != '') {
            var vFechaVencimientoBancoRnvStr = vFechaVencimientoBancoRnvStr.substring(3, 5) + '-' + vFechaVencimientoBancoRnvStr.substring(0, 2) + '-' + vFechaVencimientoBancoRnvStr.substring(6, 10);
            vm.renovacion.FechaVencimientoBancoRnv = new Date(vFechaVencimientoBancoRnvStr);
            //$('#FechaVencimientoBancoRnv').attr('disabled', 'disabled');
            //  } else { $('FechaVencimientoBancoRnv').prop("disabled", true); }


            var vFechaRespuestaFinalGcSmStr = cartaFianzaDetalleRenovacion.FechaRespuestaFinalGcSmStr;
            if (vFechaRespuestaFinalGcSmStr != '') {
                var vFechaRespuestaFinalGcSmStr = vFechaRespuestaFinalGcSmStr.substring(3, 5) + '-' + vFechaRespuestaFinalGcSmStr.substring(0, 2) + '-' + vFechaRespuestaFinalGcSmStr.substring(6, 10);
                vm.renovacion.FechaRespuestaFinalGcSm = new Date(vFechaRespuestaFinalGcSmStr);
            }

            var vFechaRecepcionCartaFianzaRenovacionStr = cartaFianzaDetalleRenovacion.FechaRecepcionCartaFianzaRenovacionStr;
            if (vFechaRecepcionCartaFianzaRenovacionStr != '') {
                var vFechaRecepcionCartaFianzaRenovacionStr = vFechaRecepcionCartaFianzaRenovacionStr.substring(3, 5) + '-' + vFechaRecepcionCartaFianzaRenovacionStr.substring(0, 2) + '-' + vFechaRecepcionCartaFianzaRenovacionStr.substring(6, 10);
                vm.renovacion.FechaRecepcionCartaFianzaRenovacion = new Date(vFechaRecepcionCartaFianzaRenovacionStr);
            }
            var vFechaEntregaRenovacionStr = cartaFianzaDetalleRenovacion.FechaEntregaRenovacionStr;
            if (vFechaEntregaRenovacionStr != '') {
                var vFechaEntregaRenovacionStr = vFechaEntregaRenovacionStr.substring(3, 5) + '-' + vFechaEntregaRenovacionStr.substring(0, 2) + '-' + vFechaEntregaRenovacionStr.substring(6, 10);
                vm.renovacion.FechaEntregaRenovacion = new Date(vFechaEntregaRenovacionStr);
            }

            vm.renovacion.SustentoRenovacion = cartaFianzaDetalleRenovacion.SustentoRenovacion;


            $('#ModalCartaFianzaRenovacion').modal({
                keyboard: false
            });

        };

        function CerrarPopup() {
            $('#ModalCartaFianza').modal('hide');
            $('#ModalCartaFianzaRenovacion').modal('hide');
            
        };

        /**********************carga detalle de acciones****************************/
        function DetalleCartaFianzaAccion(IdCliente, IdCartaFianza) {
            //let that = this;
            //let cartaFianzaDetalleRenovacion = that.dtInstanceCartaFianzaRenovacion.dataTable.fnGetData(NumeroFila);
            vm.IdCartaFianza = IdCartaFianza;
            vm.IdCliente = IdCliente;
            var RegCartaFianzaAccion = {
                IdCartaFianza: vm.IdCartaFianza,
                IdCliente: vm.IdCliente
            };

            var promise = AccionCartaFianzaService.ModalCartaFianzaAccion(RegCartaFianzaAccion);

            promise.then(function (response) {

                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCartaFianzaAcciones").html(content);
                });

                $('#ModalCartaFianzaAccion').modal({
                    keyboard: false,
                    backdrop: 'static'

                });
            }, function (response) {
                blockUI.stop();
            });

        }

        /********************** Tesoreria cambio de boton de imagen indicado a mail***************************/

        function CambiaIconoEmailGerente(object) {

            if ($('#indicardor' + object + '').hasClass('glyphicon glyphicon-circle-arrow-up')) {
                $('#indicardor' + object + '').removeClass('glyphicon glyphicon-circle-arrow-up');
                $('#indicardor' + object + '').addClass('glyphicon fa fa-envelope');
                vm.color = $('#indicardor' + object + '').css('color');
                $('#indicardor' + object + '').css('color', '#3949ab');
            }
            //CambiaIconoIndicador(object, color);
        }

        function CambiaIconoIndicador(object, color) {

            if ($('#indicardor' + object + '').hasClass('glyphicon fa fa-envelope')) {

                $('#indicardor' + object + '').removeClass('glyphicon fa fa-envelope');
                $('#indicardor' + object + '').addClass('glyphicon glyphicon-circle-arrow-up');
                $('#indicardor' + object + '').css('color', vm.color);
            }

        }

        function CambiaIconoEmailTesoreria(object) {

            if ($('#indicardorTsr' + object + '').hasClass('glyphicon glyphicon-circle-arrow-up')) {

                $('#indicardorTsr' + object + '').removeClass('glyphicon glyphicon-circle-arrow-up');
                $('#indicardorTsr' + object + '').addClass('glyphicon fa fa-envelope');
                vm.color = $('#indicardorTsr' + object + '').css('color');
                $('#indicardorTsr' + object + '').css('color', '#3949ab');
            }
            //CambiaIconoIndicador(object, color);
        }

        function CambiaIconoIndicadorTsr(object, color) {

            if ($('#indicardorTsr' + object + '').hasClass('glyphicon fa fa-envelope')) {

                $('#indicardorTsr' + object + '').removeClass('glyphicon fa fa-envelope');
                $('#indicardorTsr' + object + '').addClass('glyphicon glyphicon-circle-arrow-up');
                $('#indicardorTsr' + object + '').css('color', vm.color);
            }

        }

        /****************************** Combos Box *********************************/
        function CargarTipoRenovacion() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 218
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaTipoRenovacion = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarEntidadBancaria() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 239

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListEntidadBancaria = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarTipoRenovacion() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 218
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoRenovacion = [
                    { Codigo: "-1", Descripcion: "Selecione" },
                    { Codigo: "1", Descripcion: "No" },
                    { Codigo: "5", Descripcion: "Si" }
                ]; //UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarTipoRequerida() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 390
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoRequerida = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarTipoValidacionRenovacion() {

            vm.ListTipoValidacionRenovacion = [
                { Codigo: "-1", Descripcion: "Selecione" },
                { Codigo: "1", Descripcion: "Validación C.G y S.M" },
                { Codigo: "2", Descripcion: "Validación de Contrato" },
                { Codigo: "3", Descripcion: "A sol entidad" }
            ]; 

        };

        function CargarGerenteCuenta() {
             blockUI.start();

             var maestra = {
                 //IdCargo -> Recurso
                 IdTipoColaboradorFianzaTm: 3,
                 IdSupervisorColaborador: -1
             };

             var promise = MaestroCartaFianzaService.ListarColaborador(maestra);

             promise.then(
                 function (response) {
                     blockUI.stop();

                     var Respuesta = response.data;

                     $("#IdColaborador").select2({
                         data: Respuesta,
                         placeholder: '--Seleccione--',
                         allowClear: true
                     });
                     $("#IdColaborador").val(-1).change();

                 },
                 function (response) {
                     blockUI.stop();
                 });
         };
        /****************************** Mantenimiento *********************************/
        function GuardarCartaFianzaRenovacion(cartaFianzaRenovacion) {
            if (vm.ValidarCartaFianzaRenovacion(cartaFianzaRenovacion)) {
                blockUI.start();

                var promise = RenovacionCartaFianzaService.ActualizarCartaFianzaRenovacion(cartaFianzaRenovacion);

                promise.then(
                    //Success:
                    function (response) {
                        blockUI.stop();
                        var Respuesta = response.data;

                        if (Respuesta.TipoRespuesta != 0) {
                            UtilsFactory.Alerta('#divAlertRegistrarRenovacion', 'danger', Respuesta.Mensaje, 20);
                        } else {
                            UtilsFactory.Alerta('#divAlertRegistrarRenovacion', 'success', 'Datos Actualizados Correctamente', 10);

                            var time = 1500;
                            if ((vm.renovacion.NumeroGarantiaReemplazo != '' || vm.renovacion.NumeroGarantiaReemplazo != null) && (vm.CmbIdTipoBancoReemplazo != -1) && vm.existeFecha(vm.FechaVencimientoBancoRnv) != false) {
                                time = 4500;
                                UtilsFactory.Alerta('#divAlertRegistrarRenovacion', 'warning', 'Datos Actualizados Correctamente' + ';<br/> Ingrese Garantia Reemplazo,Banco Reemplazo,Nuevo Vencimiento para Finalizar la Nueva Renovación', 20);
                            }

                            setTimeout(() => {
                                vm.BuscarCartaFianzaRenovacion();
                                vm.CerrarPopup();
                            }, time);
                        }
                    },
                    //Error:
                    function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlertRegistrarRenovacion', 'danger', MensajesUI.DatosError, 5);
                    });
            }
        }

        function ValidarCartaFianzaRenovacion(cf) {
            var response = false;
            /*
             
            */
            var v_renovacion = 0;


            if (vm.existeFecha(vm.renovacion.FechaVencimientoBancoRnv) != false) { v_renovacion = v_renovacion + 1; }
            if (vm.renovacion.NumeroGarantiaReemplazo.length > 2 ) { v_renovacion = v_renovacion + 1; }
           // if (vm.renovacion.IdTipoBancoReemplazo != -1) { v_renovacion = v_renovacion + 1; }


            if (cf.IdRenovarTm == 0 || cf.IdRenovarTm == null || cf.IdRenovarTm == -1) {
                UtilsFactory.Alerta('#divAlertRegistrarRenovacion', 'danger', 'Selecione un tipo de Renovación', 20);
                AlertaError();
            }
            if (v_renovacion >= 1 && v_renovacion < 2) {
                UtilsFactory.Alerta('#divAlertRegistrarRenovacion', 'danger', 'Selecione Fecha de Renovación e Ingrese Número de Garantía de Reemplazo', 20);
                 AlertaError();
            }           
            else {
                response = true;
            }

            return response;
        }

        function monthDiff(d2) {
              var d1 = new Date();
              var months;
              if (d2 != null) {
                    months = (d2.getFullYear() - d1.getFullYear()) * 12; months -= d1.getMonth() + 1; months += d2.getMonth();
                            if (d2.getDate() >= d1.getDate())
                                months++;
                            vm.renovacion.PeriodoMesRenovacion = (months <= 0 ? 0 : months);
              }         
           
        }

        function VerificarItemSelecionadosCartaFianzaRevertirRenovacion() {
            vm.vSIdCartaFianza = '';
            vm.Id = -1;
            for (var CodigoSolicitud in vm.selected) {
                if (vm.selected.hasOwnProperty(CodigoSolicitud)) {
                    if (vm.selected[CodigoSolicitud]) {
                        vm.vSIdCartaFianza = vm.vSIdCartaFianza + "" + CodigoSolicitud + ",";
                    }
                }
            }
            if (vm.vSIdCartaFianza != "") {
                console.log(vm.vSIdCartaFianza);
                $('#ModalRenovacionRevertir').modal({
                    keyboard: false
                });
                if (vm.Id == 0) {
                    BuscarCartaFianzaRenovacion();
                }
            } else {
                UtilsFactory.Alerta('#divAlertMaestro', 'danger', 'Selecione item de la Lista de Fianzas', 5);
            }

        }

        function VerificarItemSelecionadosCartaFianzaRenovacion() {
            vm.vSIdCartaFianza = '';
            vm.Id = -1;
            vm.CmbTipoRenovacionTm = -1;
            for (var CodigoSolicitud in vm.selected) {
                if (vm.selected.hasOwnProperty(CodigoSolicitud)) {
                    if (vm.selected[CodigoSolicitud]) {
                        vm.vSIdCartaFianza = vm.vSIdCartaFianza + "" + CodigoSolicitud + ",";
                    }
                }
            }
            if (vm.vSIdCartaFianza != "") {
                console.log(vm.vSIdCartaFianza);
                $('#ModalRenovacion').modal({
                    keyboard: false
                });
                if (vm.Id == 0) {
                    BuscarCartaFianzaRenovacion();
                }
            } else {
                UtilsFactory.Alerta('#divAlertMaestro', 'danger', 'Selecione item de la Lista de Fianzas', 5);
            }

        }

        /*******************************Renovacion****************************************/
        function RenovarCartaFianzaReversion() {
            blockUI.start();
            var vIdTipoAccionTm = 3;
            var vIdRenovacionTm = -2;

            vm.vSIdCartaFianza = $('#vSIdCartaFianza').val();

            if (vm.CmbTipoRenovacionTm == "-1" || vm.CmbTipoRenovacionTm == "0") {
                UtilsFactory.Alerta('#divAlertRevertirRenovacionRev', 'danger', 'Seleccione el Tipo de Renovación', 5);
                AlertaError();
            }
            else if (vm.vSIdCartaFianza != '' || (vm.vSIdCartaFianza).length > 2) {

                var maestra = {
                    IdCartaFianzastr: vm.vSIdCartaFianza,
                    IdTipoAccionTm: vIdTipoAccionTm,
                    IdRenovarTm: vIdRenovacionTm,

                    FechaRespuestaFinalGcSm: '01/01/1753',
                    FechaVencimientoRenovacion: '01/01/1753',
                    ValidacionContratoGcSm: '-',
                    SustentoRenovacion: '-',
                    Observacion: '-',
                    PeriodoMesRenovacion: 0

                };

                var promise = MaestroCartaFianzaService.ActualizaARenovacionCartaFianza(maestra);

                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        vm.Id = Respuesta.TipoRespuesta
                        UtilsFactory.Alerta('#divAlertRevertirRenovacionRev', 'danger', Respuesta.Mensaje, 20);
                        //AlertaError();
                    } else {
                        vm.Id = Respuesta.TipoRespuesta;
                        UtilsFactory.Alerta('#divAlertRevertirRenovacionRev', 'success', Respuesta.Mensaje, 5);
                        $('#ModalRenovacion').modal('hide');

                        BuscarCartaFianzaRenovacion();
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertRevertirRenovacionRev', 'danger', MensajesUI.DatosError, 5);
                });

            } else {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRevertirRenovacionRev', 'danger', 'debe selecionar al menos 1 item', 5);
                //AlertaError();
            }

        };

        function RenovarCartaFianza() {

            var vIdTipoAccionTm = 0;
            vm.vSIdCartaFianza = $('#vSIdCartaFianza').val();

            if (vm.CmbTipoRenovacionTm == "-1" || vm.CmbTipoRenovacionTm == "0") {
                UtilsFactory.Alerta('#divAlertRegistrarRenovacionSol', 'danger', 'Seleccione el Tipo de Renovación', 5);
            }
            else if (vm.vSIdCartaFianza != '' || (vm.vSIdCartaFianza).length > 2) {
                blockUI.start();
                if (vm.CmbTipoRenovacionTm == 1 || vm.CmbTipoRenovacionTm == 2) {
                    var vIdTipoAccionTm = 4;
                } else if (vm.CmbTipoRenovacionTm == 5) {
                    var vIdTipoAccionTm = 3;
                    vm.CmbTipoRenovacionTm = 4;
                }

                var maestra = {
                    IdCartaFianzastr: vm.vSIdCartaFianza,
                    IdTipoAccionTm: vIdTipoAccionTm,
                    IdRenovarTm: vm.CmbTipoRenovacionTm,

                    FechaRespuestaFinalGcSm: (vm.FechaRespuestaFinalGcSm == undefined) ? '01/01/1753' : vm.FechaRespuestaFinalGcSm,
                    FechaVencimientoRenovacion: (vm.FechaVencimientoRenovacion == undefined) ? '01/01/1753' : vm.FechaVencimientoRenovacion,
                    ValidacionContratoGcSm: (vm.ValidacionContratoGcSm == undefined) ? '-' : vm.ValidacionContratoGcSm,
                    SustentoRenovacion: vm.SustentoRenovacion == undefined ? '-' : vm.SustentoRenovacion,
                    Observacion: vm.Observacion == undefined ? '-' : vm.Observacion,
                    PeriodoMesRenovacion: vm.PeriodoMesRenovacion == undefined ? 0 : vm.PeriodoMesRenovacion
                };

                var promise = RenovacionCartaFianzaService.ActualizaARenovacionCartaFianza(maestra);

                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        vm.Id = Respuesta.TipoRespuesta
                        UtilsFactory.Alerta('#divAlertRegistrarRenovacionSol', 'danger', Respuesta.Mensaje, 20);
                    } else {
                        vm.Id = Respuesta.TipoRespuesta;
                        UtilsFactory.Alerta('#divAlertRegistrarRenovacionSol', 'success', Respuesta.Mensaje, 5);
                        $('#ModalRenovacion').modal('hide');

                        BuscarCartaFianzaRenovacion();
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertRegistrarRenovacionSol', 'danger', MensajesUI.DatosError, 5);
                });

            } else {

                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRegistrarRenovacionSol', 'danger', 'debe selecionar al menos 1 item', 5);
            }

        };

        function Renovar() {

            if (vm.CmbTipoRenovacionTm == "1") {
                $('#RenovarSi').css('visibility', 'hidden');
                $('#RenovarSi').css('display', 'none');
                //$('FechaVencimientoRenovacion').attr('visibility', 'disabled');
                //$('FechaVencimientoRenovacion').attr('display', 'none');
            } else if (vm.CmbTipoRenovacionTm == "5") {
                $('#RenovarSi').css("visibility", '');
                $('#RenovarSi').css("display", '');
                //$('FechaVencimientoRenovacion').prop("visibility", 'hidden');
                //$('FechaVencimientoRenovacion').prop("display", 'none');
            }
        }       

        /*************************Modal de Requerida**********************************/
        function DetalleRequerida(numerofila) {
            vm.IdTipoRequeridaTm = -1;
            vm.FechaRespuestaFinalGcSm = null;
            vm.FechaEjecucion = null;
            vm.FechaDesestimarRequerimiento = null;
            vm.MotivoCartaFianzaRequerida = null;

            let that = this;
            let cartaFianza = that.dtInstanceCartaFianzaRenovacion.dataTable.fnGetData(numerofila);
            // RequeridaCartaFianza(cartaFianzaDetalleRenovacion)

            vm.IdTipoRequeridaTm = cartaFianza.IdTipoRequeridaTm;

            var vFechaRenovacionEjecucionStr = cartaFianza.FechaRenovacionEjecucionStr;
            var vFechaRenovacionEjecucionStr = vFechaRenovacionEjecucionStr.substring(3, 5) + '-' + vFechaRenovacionEjecucionStr.substring(0, 2) + '-' + vFechaRenovacionEjecucionStr.substring(6, 10);
            vm.FechaRenovacionEjecucion = new Date(vFechaRenovacionEjecucionStr);



            var vFechaEjecucionStr = cartaFianza.FechaEjecucionStr;
            var vFechaEjecucionStr = vFechaEjecucionStr.substring(3, 5) + '-' + vFechaEjecucionStr.substring(0, 2) + '-' + vFechaEjecucionStr.substring(6, 10);
            vm.FechaEjecucion = new Date(vFechaEjecucionStr);


            var vFechaDesestimarRequerimientoStr = cartaFianza.FechaDesestimarRequerimientoStr;
            var vFechaDesestimarRequerimientoStr = vFechaDesestimarRequerimientoStr.substring(3, 5) + '-' + vFechaDesestimarRequerimientoStr.substring(0, 2) + '-' + vFechaDesestimarRequerimientoStr.substring(6, 10);
            vm.FechaDesestimarRequerimiento = new Date(vFechaDesestimarRequerimientoStr);


            vm.MotivoCartaFianzaRequerida = cartaFianza.MotivoCartaFianzaRequerida;


            if (vm.IdTipoRequeridaTm == null) { vm.IdTipoRequeridaTm = -1; }

            vm.IdCartaFianza = cartaFianza.IdCartaFianza;
            vm.numerofila = numerofila;
            $('#ModalRequerida').modal({
                keyboard: false
            });
            SelecionarFechaRequerida();

        }

        function RequeridaCartaFianza(NumeroFila) {
            let that = this;
            let cartaFianza = that.dtInstanceCartaFianzaRenovacion.dataTable.fnGetData(vm.numerofila);

            //|| ( existeFecha(vm.FechaDesestimarRequerimiento) == false && existeFecha(vm.FechaEjecucion)  == false) 
            if (vm.IdTipoRequeridaTm == 3 && existeFecha(vm.FechaRenovacionEjecucion) == false) {
                UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'danger', 'ingrese Fecha de Requerida (Renovación ó Ejecucón)', 5);
            }
            else if (vm.IdTipoRequeridaTm == 2 && existeFecha(vm.FechaDesestimarRequerimiento) == false) {
                UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'danger', 'ingrese Fecha de Desestimar Requerimiento', 5);
            }
            else if (vm.IdTipoRequeridaTm == 1 && existeFecha(vm.FechaEjecucion) == false) {
                UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'danger', 'ingrese Fecha de Ejecución', 5);
            }
            else if ((vm.IdTipoRequeridaTm != -1) && (vm.MotivoCartaFianzaRequerida == '' || vm.MotivoCartaFianzaRequerida == null || $('#MotivoCartaFianzaRequerida').val().length == 0)) {
                UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'danger', 'Ingrese alguna Observación', 5);
            }
            else {
                /*
                    vIdEstadoCartaFianzaTm
                    1	Descargada
                    2	Ejecutada
                    3	Recuperada
                    4	Vencida
                    5	Vigente
                vIdRenovarTm
                1	No
                2	No Aplica
                3	Por Atender
                4	Seguimiento
                5	Si
                6	Otros
                    1	Requerida:Ejecutada
                    2	Requerida:Desestimada
                    3	Requerida:Renovada
                */
                var vIdRenovarTm = 0;
                var vIdEstadoCartaFianzaTm = 0;
                var vIdTipoAccionTm = 0;
    
                if (vm.IdTipoRequeridaTm == 1) {//Ejecutada	1
                    vIdEstadoCartaFianzaTm = 2; // ejecutada 2.
                    vIdRenovarTm = 4;       // 2	No Aplica
                    vIdTipoAccionTm = 1;
                } else if (vm.IdTipoRequeridaTm == 2) {// Desestimada	2                    
                    vIdEstadoCartaFianzaTm = 5;
                    vIdRenovarTm = 4;
                    vIdTipoAccionTm = 1;
                } else if (vm.IdTipoRequeridaTm == 3) { // Renovada	3
                    vIdRenovarTm = 5; // 5	Si
                    vIdEstadoCartaFianzaTm = 5;  // 5	Vigente
                    vIdTipoAccionTm = 3;    //renovacion
                }

                var maestra = {
                    IdCartaFianza: vm.IdCartaFianza,
                    //IdTipoAccionTm: vIdTipoAccionTm,
                    //IdRenovarTm: vIdRenovarTm,
                    //IdEstadoCartaFianzaTm: vIdEstadoCartaFianzaTm,
                    IdTipoRequeridaTm: vm.IdTipoRequeridaTm,
                    FechaRenovacionEjecucion: vm.FechaRenovacionEjecucion,
                    FechaDesestimarRequerimiento: vm.FechaDesestimarRequerimiento,
                    FechaEjecucion: vm.FechaEjecucion,
                    MotivoCartaFianzaRequerida: vm.MotivoCartaFianzaRequerida

                };
                blockUI.start();
                var promise = RenovacionCartaFianzaService.ActualizarCartaFianzaRequerida(maestra);

                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        vm.Id = Respuesta.TipoRespuesta
                        UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'danger', Respuesta.Mensaje, 20);
                    } else {
                        vm.Id = Respuesta.TipoRespuesta;
                        UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'success', Respuesta.Mensaje, 5);
                        $('#ModalRequerida').modal('hide');

                        BuscarCartaFianzaRenovacion();
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'danger', MensajesUI.DatosError, 5);
                });that.dtInstanceCartaFianzaReno

            }

        };
        /*********************************************************************************/

        function CalculaFechaVencimiento(periodo){
			var fecha = new Date();		
			var fechan = new Date(fecha.setMonth((fecha.getMonth()+1) + periodo));
 			var mesn =((fechan.getMonth() == 0 )? 1: (fechan.getMonth()+1));
			var nFecha = formatted_string('00' ,mesn,0)+'-'+fechan.getDate() +'-'+ fechan.getFullYear();

			vm.FechaVencimientoRenovacion = new Date ( nFecha );
		
		}

        function formatted_string(pad, user_str, pad_pos) {
		    if (typeof user_str === 'undefined')
		        return pad;
		    if (pad_pos == 0 ) {
		        return (pad + user_str).slice(-pad.length);
		    }
		    else {
		        return (user_str + pad).substring(0, pad.length);
		    }
		}

        /**********************Modal Envio de Correo a Tesoreria***************************/

        function ObtenerDatosCorreoTesoreria(NumeroFila) {
            let that = this;
            let cartaFianza = that.dtInstanceCartaFianzaRenovacion.dataTable.fnGetData(NumeroFila);

            if (cartaFianza.IdTipoAccionTm == 3 && existeFecha(cartaFianza.FechaEntregaRenovacion) == false && cartaFianza.IdTipoSubEstadoCartaFianzaTm == 6) { cartaFianza.IdTipoSubEstadoCartaFianzaTm = 7; /*     7	Seguimiento Renovacion GC:Solicitud GC */ }

            if (cartaFianza.IdTipoAccionTm == 3 && existeFecha(cartaFianza.FechaEntregaRenovacion) == false) { cartaFianza.IdTipoSubEstadoCartaFianzaTm = 6; /* 6	Seguimiento Renovacion Tso:Solicitud Tso */ }
            if (cartaFianza.IdEstadoCartaFianzaTm == -1) { cartaFianza.IdTipoSubEstadoCartaFianzaTm = 5;/*      */ }

            var promise = CorreoService.ObtenerDatosCorreo(cartaFianza);

            vm.SetearDatosCorreo(promise, cartaFianza);

        }

        function ObtenerDatosCorreoGC(NumeroFila) {
            let that = this;
            let cartaFianza = that.dtInstanceCartaFianzaRenovacion.dataTable.fnGetData(NumeroFila);
            //cartaFianza.IdTipoAccionTm = 2;/*emision*/
            if (cartaFianza.IdTipoSubEstadoCartaFianzaTm == 2) { cartaFianza.IdTipoSubEstadoCartaFianzaTm = 3;/*Seguimiento GC:Escalado*/ }

            if (cartaFianza.IdTipoSubEstadoCartaFianzaTm == 1) { cartaFianza.IdTipoSubEstadoCartaFianzaTm = 2;/*Seguimiento GC:Reiterativo*/ }
  
           if (cartaFianza.IdTipoSubEstadoCartaFianzaTm == 8 || cartaFianza.IdTipoSubEstadoCartaFianzaTm == 9) { cartaFianza.IdTipoSubEstadoCartaFianzaTm = 1;/*Seguimiento GC:Seguimiento*/ }
          
            var promise = CorreoService.ObtenerDatosCorreoGC(cartaFianza);
            vm.SetearDatosCorreo(promise, cartaFianza);

        }

        function SetearDatosCorreo(promise, cartaFianza) {
            promise.then(
                //Success:
                function (response) {
                    var respuesta = $(response.data);

                    $injector.invoke(function ($compile) {
                        var div = $compile(respuesta);
                        var content = div($scope);
                        $("#ContenidoCorreo").html(content);

                        let respuestaCorreo = window.correo;
                        let controladorHijo = $scope.$$childTail.vm;

                        controladorHijo.Init();

                        controladorHijo.correo.IdCartaFianza = cartaFianza.IdCartaFianza;

                        controladorHijo.correo.IdTipoSubEstadoCartaFianzaTm = cartaFianza.IdTipoSubEstadoCartaFianzaTm;
                        controladorHijo.correo.IdRenovarTm = cartaFianza.IdRenovarTm;
                        controladorHijo.correo.IdEstadoCartaFianzaTm = cartaFianza.IdEstadoCartaFianzaTm;
                        controladorHijo.correo.IdTipoAccionTm = cartaFianza.IdTipoAccionTm;
                        controladorHijo.correo.IdColaborador = respuestaCorreo.IdUsuarioCreacion;
                        controladorHijo.correo.IdUsuarioCreacion = respuestaCorreo.IdUsuarioCreacion;
                        controladorHijo.correo.UsuarioEdicion = respuestaCorreo.UsuarioEdicion;

                        controladorHijo.correo.From = respuestaCorreo.From;
                        controladorHijo.correo.To = respuestaCorreo.EmailColaborador;
                        controladorHijo.correo.Copy = respuestaCorreo.From;
                        controladorHijo.correo.Subject = respuestaCorreo.Subject;
                        setTimeout(() => {
                            let sinAsignar = '<h3><strong>El Gerente de Cuenta elegido no corresponde a ninguna de las cartas fianzas a renovar</strong></h3>';
                            (respuestaCorreo.Body != null && respuestaCorreo.Body != "") ? tinymce.get('Body').setContent(respuestaCorreo.Body) : tinymce.get('Body').setContent(sinAsignar);
                            $('#ModalEnviarCorreoCartaFianza').modal();
                        }, 100);
                    });
                },
                //Error:
                function (response) {
                    blockUI.stop();
                });
        }

        $('#ModalEnviarCorreoCartaFianza').on('hidden.bs.modal', function () {
            tinymce.remove('textarea.texto-enriquecido');

        })

        function existeFecha(fecha) {

            if (fecha == 'Invalid Date' || fecha == undefined || fecha == null) {
                return false;
            }
            return true;
        }

        function AlertaError() {
            $('#ModalCartaFianzaRenovacion').scrollTop(0);
        }

        /**************************Solicitadud de renovacion*********************************/
        function DetalleCartaFianzaSolRenovacion(NumeroFila) {
            let that = this;
            vm.IdCartaFianza = "";
            vm.NumeroGarantia = "";
            vm.IdCliente = "";

            let cartaFianza = that.dtInstanceCartaFianzaRenovacion.dataTable.fnGetData(NumeroFila);
            $('#ModalCartaFianzaSolRenovacion').modal({
                keyboard: false
            });
            BuscarReporteRenovacionGarantia(cartaFianza);
        }

        function BuscarReporteRenovacionGarantia(cartaFianza) {
            blockUI.start();
            var parametros =
            "reporte=CartaFianzaRenovacionGarantia" +
            "&p_IdCliente=" + cartaFianza.IdCliente +
            "&p_IdCartaFianza=" + cartaFianza.IdCartaFianza +
            "&p_NumeroGarantia=" + cartaFianza.NumeroGarantia;

            $("[id$='FReporte']").attr('src', $urls.ApiReportes + parametros);
            $("#FReporte").height(1000);
            $timeout(function () {
                blockUI.stop();
            }, 2000);
        };

        /***********************habilitar o inhabilitar Fianza*************************/

        function SelecionarFechaRequerida() {

            if (vm.IdTipoRequeridaTm == 1) {
                document.getElementById("FechaDesestimarRequerimiento").disabled = true;
                document.getElementById("FechaRenovacionEjecucion").disabled = true;
                document.getElementById("FechaEjecucion").disabled = false;

            } else if (vm.IdTipoRequeridaTm == 2) {
                document.getElementById("FechaDesestimarRequerimiento").disabled = false;
                document.getElementById("FechaRenovacionEjecucion").disabled = true;
                document.getElementById("FechaEjecucion").disabled = true;

            } else if (vm.IdTipoRequeridaTm == 3) {
                document.getElementById("FechaDesestimarRequerimiento").disabled = true;
                document.getElementById("FechaRenovacionEjecucion").disabled = false;
                document.getElementById("FechaEjecucion").disabled = true;

            } else {
                document.getElementById("FechaDesestimarRequerimiento").disabled = true;
                document.getElementById("FechaRenovacionEjecucion").disabled = true;
                document.getElementById("FechaEjecucion").disabled = true;
            }
            
        }

        function SelecionarFechaRequeridaBloqueado() {

            document.getElementById("FechaDesestimarRequerimiento").disabled = true;
            document.getElementById("FechaRenovacionEjecucion").disabled = true;
            document.getElementById("FechaEjecucion").disabled = true;

        }

        /***********************Modal para Modificar Carta Fianza*************************/
        function CargaModalCartaFianzaId(NumeroFila) {
            let that = this;
            //vm.IdCartaFianza = "";
            //vm.NumeroGarantia = "";
            //vm.IdCliente = "";

            let cartaFianza = that.dtInstanceCartaFianzaRenovacion.dataTable.fnGetData(NumeroFila);
            vm.IdCartaFianza = cartaFianza.IdCartaFianza;
            vm.IdCliente = cartaFianza.IdCliente;
            var RegCartaFianza = {
                IdCartaFianza: cartaFianza.IdCartaFianza,
                IdCliente: cartaFianza.IdCliente
            };
            var promise = RegistroCartaFianzaService.ModalCartaFianzaRegistrar(RegCartaFianza);

            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCartaFianza").html(content);
                });

                $('#ModalCartaFianza').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        /************************** Enviar correo masivo Gerente Cuenta *********************************/
        function CargarGerenteCuenta() {
            blockUI.start();

            var maestra = {
                //IdCargo -> Recurso
                IdTipoColaboradorFianzaTm: 3,
                IdSupervisorColaborador: -1
            };

            var promise = MaestroCartaFianzaService.ListarColaborador(maestra);

            promise.then(
                function (response) {
                    blockUI.stop();

                    var Respuesta = response.data;

                    $("#IdColaboradorSearch").select2({
                        data: Respuesta,
                        placeholder: '--Seleccione--',
                        allowClear: true
                    });
                    $("#IdColaboradorSearch").val(-1).change();

                },
                function (response) {
                    blockUI.stop();
                });
        };

        function EnviarCorreoGC() {
            let IdColaborador = $("#IdColaboradorSearch").val();
            if (IdColaborador == null) {
                UtilsFactory.Alerta('#divAlertMaestro', 'danger', 'Selecionar un Gerente de Cuenta', 5);
                return;
            }
            let cartaFianza = {
                IdCartaFianza: 0,
                IdColaborador: IdColaborador
            }

            var promise = CorreoService.ObtenerDatosCorreoGC(cartaFianza);
            vm.SetearDatosCorreo(promise, cartaFianza);

        }


        vm.search.IdRenovarTm = '-1';
        vm.renovacion.IdRenovarTm = '-1';
        vm.renovacion.NumeroGarantiaReemplazo = "";
        CargarTipoRenovacion();
        BuscarCartaFianzaRenovacion();
        CargarEntidadBancaria();
        
        CargarTipoRenovacion();
        CargarTipoRequerida();
        CargarTipoValidacionRenovacion();
        CargarGerenteCuenta();
       
     
    }

})();
