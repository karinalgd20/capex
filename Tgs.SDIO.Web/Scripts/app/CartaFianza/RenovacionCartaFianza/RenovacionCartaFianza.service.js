﻿(function () {
    'use strict',
     angular
    .module('app.CartaFianza')
    .service('RenovacionCartaFianzaService', RenovacionCartaFianzaService);

    RenovacionCartaFianzaService.$inject = ['$http', 'URLS'];

    function RenovacionCartaFianzaService($http, $urls) {

        var service = {
            ListarCartaFianzaRenovacion: ListarCartaFianzaRenovacion,
            ActualizarCartaFianzaRenovacion: ActualizarCartaFianzaRenovacion,
            ObtenerDatosCorreoRenovacion: ObtenerDatosCorreoRenovacion,
            ActualizaARenovacionCartaFianza: ActualizaARenovacionCartaFianza,
            ActualizarCartaFianzaRequerida: ActualizarCartaFianzaRequerida,
        };

        return service;

        function ListarCartaFianzaRenovacion(cartaFianzaRenovacion) {

            return $http({
                url: $urls.ApiCartaFianza + "RenovacionCartaFianza/ListarCartaFianzaRenovacion",
                method: "POST",
                data: JSON.stringify(cartaFianzaRenovacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarCartaFianzaRequerida(cartaFianza) {

            return $http({
                url: $urls.ApiCartaFianza + "MaestroCartaFianza/ActualizarCartaFianzaRequerida",
                method: "POST",
                data: JSON.stringify(cartaFianza)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizaARenovacionCartaFianza(cartaFianza) {

            return $http({
                url: $urls.ApiCartaFianza + "MaestroCartaFianza/ActualizaARenovacionCartaFianza",
                method: "POST",
                data: JSON.stringify(cartaFianza)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarCartaFianzaRenovacion(cartaFianzaRenovacion) {

            return $http({
                url: $urls.ApiCartaFianza + "RenovacionCartaFianza/ActualizarCartaFianzaRenovacion",
                method: "POST",
                data: JSON.stringify(cartaFianzaRenovacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerDatosCorreoRenovacion(cartaFianzaRenovacion) {

            return $http({
                url: $urls.ApiCartaFianza + "RenovacionCartaFianza/ObtenerDatosCorreoRenovacion",
                method: "POST",
                data: JSON.stringify(cartaFianzaRenovacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();