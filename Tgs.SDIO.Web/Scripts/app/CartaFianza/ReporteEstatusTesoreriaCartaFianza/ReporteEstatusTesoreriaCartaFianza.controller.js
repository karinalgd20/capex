﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('ReporteEstatusTesoreriaCartaFianzaController', ReporteEstatusTesoreriaCartaFianzaController);

    ReporteEstatusTesoreriaCartaFianzaController.$inject = [
            'ReporteEstatusTesoreriaCartaFianzaService',
            'MaestroCartaFianzaService',
            'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];

    function ReporteEstatusTesoreriaCartaFianzaController(
            ReporteEstatusTesoreriaCartaFianzaService,
            MaestroCartaFianzaService,
            MaestraService,
            blockUI,
            UtilsFactory,
            DTColumnDefBuilder,
            DTOptionsBuilder,
            DTColumnBuilder,
            $timeout,
            MensajesUI,
            $urls,
            $scope,
            $compile,
            $modal,
            $injector) {

        vm = this;

        vm.CargarIndicadorColor = CargarIndicadorColor;
        vm.CargarEstadoTesoreria = CargarEstadoTesoreria;
        vm.CargarTipoAccion = CargarTipoAccion;
        vm.BuscarReporteEstatusTesoreria = BuscarReporteEstatusTesoreria;
        vm.LimpiarFiltros = LimpiarFiltros;

        vm.search = {};


        /************** Cargar Combo box **************/
        function CargarIndicadorColor() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 435
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaIndicadorColor = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarEstadoTesoreria() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 341,
                Valor2: '1'
            };
            var promise = MaestraService.ListarMaestra(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaEstadoTesoreria = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarTipoAccion() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 200
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaTipoAccion = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        /************** Buscar Reporte Sin Respuesta **************/
        function BuscarReporteEstatusTesoreria() {

            blockUI.start();

            let idSubEstado = (vm.search.IdSubEstado) ? vm.search.IdSubEstado : -1;
            let idTipoAccion = (vm.search.IdTipoAccion) ? vm.search.IdTipoAccion : -1;
            let fecha = (vm.search.Fecha) ? ConvertDateToString(vm.search.Fecha) : null;

            var parametros =
            "reporte=CartaFianzaEstatusTesoreria" +
            "&Indicador=" + vm.search.IndicadorColor +
            "&IdSubEstado=" + idSubEstado +
            "&IdTipoAccion=" + idTipoAccion +
            "&FechaCreacion=" + fecha;

            $("[id$='FReporte']").attr('src', $urls.ApiReportes + parametros);

            $("#FReporte").height(800);

            $timeout(function () {
                blockUI.stop();
            }, 2000);
        };

        function LimpiarFiltros() {
            vm.search = {};
            vm.search.IndicadorColor = '-1';
            vm.search.IdSubEstado = '-1';
            vm.search.IdTipoAccion = '-1';
        }

        /************** Otras funciones **************/
        function ConvertDateToString(fecha) {
            let año = fecha.getFullYear();
            let mes = ('0' + (fecha.getMonth() + 1).toString()).slice(-2);
            let dia = ('0' + fecha.getDate()).slice(-2);
            return dia + '/' + mes + '/' + año;
        }

        vm.CargarIndicadorColor();
        vm.search.IndicadorColor = '1';
        vm.CargarEstadoTesoreria();
        vm.search.IdSubEstado = '-1';
        vm.CargarTipoAccion();
        vm.search.IdTipoAccion = '-1';
        vm.BuscarReporteEstatusTesoreria();
    }

})();