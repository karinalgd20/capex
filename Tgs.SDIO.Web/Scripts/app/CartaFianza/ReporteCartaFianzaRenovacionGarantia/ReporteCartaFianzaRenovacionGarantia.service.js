﻿(function () {
    'use strict',
     angular
    .module('app.CartaFianza')
    .service('ReporteCartaFianzaRenovacionGarantiaService', ReporteCartaFianzaRenovacionGarantiaService);

    ReporteCartaFianzaRenovacionGarantiaService.$inject = ['$http', 'URLS'];


    function ReporteCartaFianzaRenovacionGarantiaService($http, $urls) {

        var service = {
         MostarReporteSolRenovacion : MostarReporteSolRenovacion
        };

        return service;

        function MostarReporteSolRenovacion(cartaFianzaRenovacion) {

            return $http({
                url: $urls.ApiCartaFianza + "ReporteCartaFianzaRenovacionGarantia/Index",
                method: "POST",
                data: JSON.stringify(cartaFianzaRenovacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();