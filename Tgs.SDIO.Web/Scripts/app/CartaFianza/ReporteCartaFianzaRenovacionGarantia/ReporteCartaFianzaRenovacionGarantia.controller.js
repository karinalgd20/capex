﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('ReporteCartaFianzaRenovacionGarantiaController', ReporteCartaFianzaRenovacionGarantiaController);

    ReporteCartaFianzaRenovacionGarantiaController.$inject = [
            'ReporteCartaFianzaRenovacionGarantiaService',
            'MaestraService',
            'ClienteService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];

    function ReporteCartaFianzaRenovacionGarantiaController(
            ReporteCartaFianzaRenovacionGarantiaService,
            MaestraService,
            ClienteService,
            blockUI,
            UtilsFactory,
            DTColumnDefBuilder,
            DTOptionsBuilder,
            DTColumnBuilder,
            $timeout,
            MensajesUI,
            $urls,
            $scope,
            $compile,
            $modal,
            $injector) {

        vm = this;

        vm.BuscarReporteRenovacionGarantia = BuscarReporteRenovacionGarantia;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.search = {};
        vm.CargarCliente = CargarCliente;

        function BuscarReporteRenovacionGarantia() {
            blockUI.start();
            var parametros =
            "reporte=CartaFianzaRenovacionGarantia" +
            "&p_IdCliente=" + $scope.$parent.vm.IdCliente +
            "&p_IdCartaFianza=" + $scope.$parent.vm.IdCartaFianza +
            "&p_NumeroGarantia=" + $scope.$parent.vm.NumeroGarantia;

            $("[id$='FReporte']").attr('src',  UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);

            $("#FReporte").height(950);

            $timeout(function () {
                blockUI.stop();
            }, 2000);
        };


        function LimpiarFiltros() {
            vm.search = {};
            vm.search.IdCliente = '-1';
        }

         
        vm.BuscarReporteRenovacionGarantia();
        
    }

})();