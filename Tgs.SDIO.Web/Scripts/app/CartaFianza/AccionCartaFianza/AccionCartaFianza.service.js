﻿(function () {
    'use strict',
     angular
    .module('app.CartaFianza')
    .service('AccionCartaFianzaService', AccionCartaFianzaService);

    AccionCartaFianzaService.$inject = ['$http', 'URLS'];


    function AccionCartaFianzaService($http, $urls) {


        var service = {

            ModalCartaFianzaAccion: ModalCartaFianzaAccion,
            ListaCartaFianzaAccionId: ListaCartaFianzaAccionId,
            InsertaCartaFianzaDetalleAccion: InsertaCartaFianzaDetalleAccion,
            ActualizaCartaFianzaDetalleAccion: ActualizaCartaFianzaDetalleAccion
        };
        return service;



        function ModalCartaFianzaAccion(dto) {
            
            return $http({
                url: $urls.ApiCartaFianza + "AccionCartaFianza/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };


      function ListaCartaFianzaAccionId(dto) {
            
            return $http({
                url: $urls.ApiCartaFianza + "AccionCartaFianza/ListaCartaFianzaDetalleAccionId",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
      };

      function InsertaCartaFianzaDetalleAccion(dto) {
         
          return $http({
              url: $urls.ApiCartaFianza + "AccionCartaFianza/InsertaCartaFianzaDetalleAccion",
              method: "POST",
              data: JSON.stringify(dto)
          }).then(DatosCompletados);

          function DatosCompletados(response) {
              return response;
          }
      };

      function ActualizaCartaFianzaDetalleAccion(dto) {
         
          return $http({
              url: $urls.ApiCartaFianza + "AccionCartaFianza/ActualizaCartaFianzaDetalleAccion",
              method: "POST",
              data: JSON.stringify(dto)
          }).then(DatosCompletados);

          function DatosCompletados(response) {
              return response;
          }
      };

        
    }



})();