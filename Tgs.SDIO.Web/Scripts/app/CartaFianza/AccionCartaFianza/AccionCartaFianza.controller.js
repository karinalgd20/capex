﻿(function () {
    'use strict'
    angular
   .module('app.CartaFianza')
   .controller('AccionCartaFianzaController', AccionCartaFianzaController);

    AccionCartaFianzaController.$inject = ['AccionCartaFianzaService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTColumnDefBuilder', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function AccionCartaFianzaController(AccionCartaFianzaService, MaestraService, blockUI, UtilsFactory, DTColumnDefBuilder, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector)
    {

        var vm = this;

        vm.CargarTipoAccion = CargarTipoAccion;
        vm.CargarTipoVencimiento = CargarTipoVencimiento;
        vm.GrabarCartaFianzaAcciones = GrabarCartaFianzaAcciones;
        vm.SelecionarCartaFianzaAccionId = SelecionarCartaFianzaAccionId;
        vm.Validar = Validar;

        vm.ListTipoVencimiento = [];
        vm.CmbEstadoVencimientoTm = '-1';

        vm.ListTipoAccion = [];
        vm.CmbTipoAccion = '-1';

        vm.FechaEmision = new Date();
        vm.FechaVencimiento = new Date();

        vm.dtInstanceCartaFianzaAcciones = {};
        vm.dtColumnsCartaFianzaAcciones = [
        DTColumnBuilder.newColumn('IdCartaFianza').notVisible(),
        DTColumnBuilder.newColumn('IdTipoAccionTm').notVisible(),
        DTColumnBuilder.newColumn('DesTipoAccion').withTitle('Tipo_Acción').notSortable().withOption('width', '8%'),
        DTColumnBuilder.newColumn('Observacion').withTitle('    Observación     ').notSortable().withOption('width', '50%'),
        DTColumnBuilder.newColumn('UsuarioCreacion').withTitle('Usu.Creación').notSortable().withOption('width', '5%'),
        DTColumnBuilder.newColumn('FechaRegistroStr').withTitle('Fec.Registro').notSortable().withOption('width', '4%'),
        DTColumnBuilder.newColumn('FechaCreacionStr').withTitle('Fec.Creación').notSortable().withOption('width', '4%'),
        //DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCartaFianzaAcciones).withOption('width', '8%')
        ];



        function AccionesCartaFianzaAcciones(data, type, full, meta) {

            var respuesta = "";// "<center><div class='col-xs-6 col-sm-6'> <a title='Selecionar' id='tab-button' class='btn btn-primary'  data-dismiss='modal' ng-click='vm.SelecionarCartaFianzaAccionId(\"" + data.IdCliente + "\",\"" + data.NumeroIdentificadorFiscal + "\",\"" + data.Descripcion + "\");'>" + "<span class='glyphicon glyphicon-ok'></span></a></div> </center>";
            return respuesta;
        };

        function BuscarCartaFianzaAciones() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptionsCartaFianzaAcciones = DTOptionsBuilder                 
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                       .withFnServerData(BuscarCartaFianzaAcionesPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
 
            }, 500);
        };

        function BuscarCartaFianzaAcionesPaginado(sSource, aoData, fnCallback, oSettings) {
            vm.IdCartaFianza = $scope.$parent.vm.IdCartaFianza;
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var size = aoData[4].value;
            var pageNumber = (start + size) / size;
            var cartaFianzaAcciones = {
                IdCartaFianza:  $scope.$parent.vm.IdCartaFianza
                //IdCliente: $scope.$parent.vm.IdCliente,              
                //Indice: pageNumber,
                //Tamanio: size
            };

            var promise = AccionCartaFianzaService.ListaCartaFianzaAccionId(cartaFianzaAcciones);
            promise.then(function (response) {
               
                var records = {
                    'draw': draw,
                    'recordsTotal': 20 ,//response.data.TotalItemCount,
                    'recordsFiltered': 10,//response.data.TotalItemCount,
                    'data': response.data
                };
                blockUI.stop();
                fnCallback(records);
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsCartaFianzaAcciones = DTOptionsBuilder
            .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('scrollCollapse', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('scrollX', true)
                .withOption('paging', false);

        };

        function SelecionarCartaFianzaAccionId() {


        }


        function Validar() {
            var dif = ( vm.FechaVencimiento.getYear() - vm.FechaEmision.getYear() ) *12 +( vm.FechaVencimiento.getMonth() - vm.FechaEmision.getMonth() );
            //numberOfMonths = (year2 - year1) * 12 + (month2 - month1) - 1; 
            if (vm.FechaVencimiento <= vm.FechaEmision || vm.FechaEmision >= vm.FechaVencimiento) {
              UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'La fecha de Vencimiento no debe ser Menor a la Fecha de Emisión o la Fecha de Emisión no debe ser Mayor a la Fecha de Vencimiento', 20);
            } else

                if (dif < 1) {
            
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'la Fecha de Vencimiento debe ser mayor a Fecha de Emisión', 20);
            }

            else {
            GrabarCartaFianzaAcciones();
            }
        /**/
        
        }

        function GrabarCartaFianzaAcciones() {

            var cartafianzaAcciones = {
                IdCartaFianza: vm.IdCartaFianza,

                IdTipoAccionTm: vm.CmbTipoAccion,
                FechaEmision: vm.FechaEmision,
                FechaVencimiento: vm.FechaVencimiento,  
                Observacion: vm.Observacion               

            };

            var promise = (vm.IdCartaFianza == 0) ? AccionCartaFianzaService.InsertaCartaFianzaDetalleAccion(cartafianzaAcciones) : AccionCartaFianzaService.ActualizaCartaFianzaDetalleAccion(cartafianzaAcciones);
            promise.then(function (response) {

                blockUI.stop();

                var Respuesta = response.data;

                if (Respuesta.TipoRespuesta != 0) {
                    UtilsFactory.Alerta('#divAlertRegistrar', 'danger', Respuesta.Mensaje, 20);
                } else {
                    vm.Id = Respuesta.Id;
           
                    UtilsFactory.Alerta('#divAlertRegistrar', 'success', Respuesta.Mensaje, 5);
                    alert(Respuesta.Mensaje);
                    $scope.$parent.vm.BuscarCartaFianza();
                    $scope.$parent.vm.CerrarPopup();
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', MensajesUI.DatosError, 5);
            });
        }

        function CargarTipoVencimiento() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 204
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoVencimiento = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarTipoAccion() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 200
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoAccion = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }



        CargarTipoAccion();
        CargarTipoVencimiento();
        BuscarCartaFianzaAciones();
    }

})();