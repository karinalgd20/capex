﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('ReporteEstatusRecuperoCartaFianzaController', ReporteEstatusRecuperoCartaFianzaController);

    ReporteEstatusRecuperoCartaFianzaController.$inject = [
            'ReporteEstatusRecuperoCartaFianzaService',
            'MaestraService',
            'ClienteService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];

    function ReporteEstatusRecuperoCartaFianzaController(
            ReporteEstatusRecuperoCartaFianzaService,
            MaestraService,
            ClienteService,
            blockUI,
            UtilsFactory,
            DTColumnDefBuilder,
            DTOptionsBuilder,
            DTColumnBuilder,
            $timeout,
            MensajesUI,
            $urls,
            $scope,
            $compile,
            $modal,
            $injector) {

        vm = this;

        vm.CargarEntidadBancaria = CargarEntidadBancaria;
        vm.BuscarReporteEstatusRecupero = BuscarReporteEstatusRecupero;
        vm.CargarCliente = CargarCliente;
        vm.LimpiarFiltros = LimpiarFiltros;

        vm.search = {};

        function BuscarReporteEstatusRecupero() {
            if ($scope.formularioBusqueda.$valid) {
                blockUI.start();
                let fechaInicio = ConvertDateToString(vm.search.FechaInicio);
                let fechaFin = ConvertDateToString(vm.search.FechaFin);
                let fechaVencimiento = (vm.search.FechaVencimiento) ? ConvertDateToString(vm.search.FechaVencimiento) : null;
                let idCliente = (vm.search.Cliente && vm.search.Cliente.IdCliente) ? vm.search.Cliente.IdCliente : -1;
                let idBanco = (vm.search.IdBanco) ? vm.search.IdBanco : -1;

                var parametros =
                "reporte=CartaFianzaEstatusRecupero" +
                "&FechaInicio=" + fechaInicio +
                "&FechaFin=" + fechaFin +
                "&FechaVencimiento=" + fechaVencimiento +
                "&IdCliente=" + idCliente +
                "&IdBanco=" + idBanco;

                $("[id$='FReporte']").attr('src', $urls.ApiReportes + parametros);

                $("#FReporte").height(500);

                $timeout(function () {
                    blockUI.stop();
                }, 2000);
            } else {
                $("#FReporte").removeAttr('src');
            }
        };

        /****************************** Combos Box *********************************/
        function CargarEntidadBancaria() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 239
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListEntidadBancaria = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarCliente(Cliente) {
            if (parseInt(Cliente.length) < 5) {
                return false;
            }
            var ClienteDto = {
                Descripcion: Cliente
            };
            return ClienteService.ListarClientePorDescripcion(ClienteDto).then(function (response) {
                return response.data;
            });
        };

        function LimpiarFiltros() {
            vm.search = {};
        }

        /************** Otras funciones **************/
        function ConvertDateToString(fecha) {
            let año = fecha.getFullYear();
            let mes = ('0' + (fecha.getMonth() + 1).toString()).slice(-2);
            let dia = ('0' + fecha.getDate()).slice(-2);
            return año + mes;
        }

        vm.search.FechaInicio = new Date();
        vm.search.FechaFin = new Date();
        vm.CargarEntidadBancaria();
        vm.search.IdBanco = '-1';
        setTimeout(() => {
            vm.BuscarReporteEstatusRecupero();
        }, 200);
    }

})();