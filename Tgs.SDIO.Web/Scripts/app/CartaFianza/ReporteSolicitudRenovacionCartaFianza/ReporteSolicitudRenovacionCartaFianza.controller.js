﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('ReporteSolicitudRenovacionCartaFianzaController', ReporteSolicitudRenovacionCartaFianzaController);

    ReporteSolicitudRenovacionCartaFianzaController.$inject = [
            'ReporteSolicitudRenovacionCartaFianzaService',
            'ClienteCartaFianzaService',
            'ClienteService',
            'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];

    function ReporteSolicitudRenovacionCartaFianzaController(
            ReporteSolicitudRenovacionCartaFianzaService,
            ClienteCartaFianzaService,
            ClienteService,
            MaestraService,
            blockUI,
            UtilsFactory,
            DTColumnDefBuilder,
            DTOptionsBuilder,
            DTColumnBuilder,
            $timeout,
            MensajesUI,
            $urls,
            $scope,
            $compile,
            $modal,
            $injector) {

        vm = this;

        vm.ListCliente = [];

        vm.BuscarReporteSolRenovacion = BuscarReporteSolRenovacion;
      

  
        vm.SelectCliente = SelectCliente;
        vm.fillTextbox = fillTextbox;
        vm.ObtenerCliente = ObtenerCliente;
        vm.LimpiarCampoCliente =LimpiarCampoCliente;
        vm.ListarCliente = ListarCliente;

   

        function BuscarReporteSolRenovacion() {
            blockUI.start();
            if (vm.IdCliente == null || vm.IdCliente == -6 || vm.IdCliente == "undefined") {
                vm.IdCliente = "-1";
                vm.NumeroGarantia = null;
            }
         


            var parametros =
            "reporte=CartaFianzaRenovacionGarantia" +
            "&p_IdCliente=" + vm.IdCliente +
            "&p_IdCartaFianza=-1" +
            "&p_NumeroGarantia=" + vm.NumeroGarantia;

            $("[id$='FReporte']").attr('src', $urls.ApiReportes + parametros);

            $("#FReporte").height(950);

            $timeout(function () {
                blockUI.stop();
            }, 2000);
        };

        function LimpiarFiltros() {
            vm.NumeroGarantia = '';
            vm.NumeroIdentificadorFiscal = '';
            vm.NombreCliente = '';
            vm.IdCliente = '-6';
        };


        function SelectCliente() {
            vm.IdCliente = 0;
 
            vm.ListCliente = null;
            if (vm.NombreCliente.length > 5) {
                ListarCliente();
                vm.heightCliente = "";
            }
        }
      
        function LimpiarCampoCliente() {
            vm.IdCliente = 0;
            vm.NumeroIdentificadorFiscal = '';
        }

        function fillTextbox(string, Id) {
            LimpiarCampoCliente();
            vm.NombreCliente = string;
            vm.IdCliente = Id;
            vm.ListCliente = null;
            ObtenerCliente(vm.NombreCliente);
        }

        function ObtenerCliente(descripcion) {
            var cliente = {
                IdEstado: 1,
                descripcion: descripcion,

            };
            var promise = ClienteService.ListarClientePorDescripcion(cliente);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                if (Respuesta != null) {
                    vm.NumeroIdentificadorFiscal = Respuesta[0].NumeroIdentificadorFiscal;
                   // var IdDireccionComercial = Respuesta[0].IdDireccionComercial;
                    //var IdSector = Respuesta[0].IdSector;
                   // ObtenerDireccionComercial(IdDireccionComercial);
                   // ObtenerSector(IdSector);

                }

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarCliente() {
            if (vm.NombreCliente != null || vm.NombreCliente != "") {
                var cliente = {
                    IdEstado: 1,
                    Descripcion: vm.NombreCliente,

                };
                var promise = ClienteService.ListarClientePorDescripcion(cliente);

                promise.then(function (resultado) {
                    blockUI.stop();
                    if (Respuesta != null) {
                        vm.heightCliente = 350;
                    }

                    var Respuesta = resultado.data;
                    var output = [];
                    angular.forEach(Respuesta, function (cliente) {
                        output.push(cliente);
                    });
                    vm.ListCliente = output;

                }, function (response) {
                    blockUI.stop();

                });
            }
        }

       
       vm.BuscarReporteSolRenovacion();
    }

})();