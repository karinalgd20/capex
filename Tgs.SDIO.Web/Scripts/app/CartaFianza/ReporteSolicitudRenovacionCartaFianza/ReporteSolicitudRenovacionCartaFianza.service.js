﻿(function () {
    'use strict',
     angular
    .module('app.CartaFianza')
    .service('ReporteSolicitudRenovacionCartaFianzaService', ReporteSolicitudRenovacionCartaFianzaService);

    ReporteSolicitudRenovacionCartaFianzaService.$inject = ['$http', 'URLS'];


    function ReporteSolicitudRenovacionCartaFianzaService($http, $urls) {

        var service = {
            MostarReporteSolRenovacion : MostarReporteSolRenovacion
        };

        return service;


        function MostarReporteSolRenovacion(cartaFianzaRenovacion) {

            return $http({
                url: $urls.ApiCartaFianza + "ReporteSolicitudRenovacionCartaFianza/Index",
                method: "POST",
                data: JSON.stringify(cartaFianzaRenovacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();