﻿(function () {
    'use strict',
     angular
    .module('app.CartaFianza')
    .service('RecuperoCartaFianzaService', RecuperoCartaFianzaService);

    RecuperoCartaFianzaService.$inject = ['$http', 'URLS'];


    function RecuperoCartaFianzaService($http, $urls) {

        var service = {
            ListarCartaFianzaRecupero: ListarCartaFianzaRecupero,
            ActualizarCartaFianzaRecupero: ActualizarCartaFianzaRecupero,
            ListaCartaFianzaDetalleSeguimiento: ListaCartaFianzaDetalleSeguimiento,
            InsertaCartaFianzaSeguimiento: InsertaCartaFianzaSeguimiento
        };

        return service;

        function ListarCartaFianzaRecupero(cartaFianzaRecupero) {

            return $http({
                url: $urls.ApiCartaFianza + "RecuperoCartaFianza/ListarCartaFianzaRecupero",
                method: "POST",
                data: JSON.stringify(cartaFianzaRecupero)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarCartaFianzaRecupero(cartaFianzaRecupero) {

            return $http({
                url: $urls.ApiCartaFianza + "RecuperoCartaFianza/ActualizarCartaFianzaRecupero",
                method: "POST",
                data: JSON.stringify(cartaFianzaRecupero)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaCartaFianzaDetalleSeguimiento(cartaFianzaRecupero) {

            return $http({
                url: $urls.ApiCartaFianza + "RecuperoCartaFianza/ListaCartaFianzaDetalleSeguimiento",
                method: "POST",
                data: JSON.stringify(cartaFianzaRecupero)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function InsertaCartaFianzaSeguimiento(cartaFianzaRecupero) {

            return $http({
                url: $urls.ApiCartaFianza + "RecuperoCartaFianza/InsertaCartaFianzaSeguimiento",
                method: "POST",
                data: JSON.stringify(cartaFianzaRecupero)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }    
})();