﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('RecuperoCartaFianzaController', RecuperoCartaFianzaController);

    RecuperoCartaFianzaController.$inject = [
            'RecuperoCartaFianzaService',
            'MaestroCartaFianzaService',
            'RenovacionCartaFianzaService',
            'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];

    function RecuperoCartaFianzaController(
            RecuperoCartaFianzaService,
            MaestroCartaFianzaService,
            RenovacionCartaFianzaService,
            MaestraService,
            blockUI,
            UtilsFactory,
            DTColumnDefBuilder,
            DTOptionsBuilder,
            DTColumnBuilder,
            $timeout,
            MensajesUI,
            $urls,
            $scope,
            $compile,
            $modal,
            $injector) {

        vm = this;

        vm.BuscarCartaFianzaRecupero = BuscarCartaFianzaRecupero;
        vm.CargarStatusRecupero = CargarStatusRecupero;
        vm.CargarEntidadBancaria = CargarEntidadBancaria;
        vm.CargarColadoradorACargo = CargarColadoradorACargo;
        vm.CargarModalCartaFianzaRecupero = CargarModalCartaFianzaRecupero;
        vm.GuardarCartaFianzaRecupero = GuardarCartaFianzaRecupero;
        vm.CerrarPopup = CerrarPopup;
        vm.verDetalleSeguimiento = verDetalleSeguimiento;
        vm.GuardarCartaFianzaSeguimiento = GuardarCartaFianzaSeguimiento;
        vm.ValidarCartaFianzaSeguimiento = ValidarCartaFianzaSeguimiento;
        vm.EditarSeguimiento = EditarSeguimiento;
        vm.EliminarSeguimiento = EliminarSeguimiento;
        vm.EditarCartaFianzaSeguimiento = EditarCartaFianzaSeguimiento;
        vm.RevertirRecupero = RevertirRecupero;
        vm.VerificarItemSelecionadosCartaFianza = VerificarItemSelecionadosCartaFianza;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.FechaSolicitudRecuperoChange = FechaSolicitudRecuperoChange;
        vm.FechaRecuperoChange = FechaRecuperoChange;
        vm.ComentarioChange = ComentarioChange;

        vm.search = {};
        vm.recupero = {};
        vm.seguimiento = {};
        vm.listaSeguimiento = null;
        vm.tablaSeguimiento = null;
        vm.cartaFianzaRecupero = null;
        vm.habilitarEditar = true;

        /******************************************* Tabla - Registro de Carta Fianza *******************************************/
        vm.dtInstanceCartaFianzaRecupero = {};

        var titleHtml = '<input type="checkbox" ng-model="vm.selectAll" ng-click="vm.toggleAll(vm.selected)">';

        vm.dtColumnsCartaFianzaRecupero = [
            DTColumnBuilder.newColumn(null).withTitle(titleHtml)
                    .notSortable()
                    .renderWith(function (data, type, full, meta) {
                        vm.selected[full.IdCartaFianzaDetalleRecupero] = false;
                        if (!data.IdCartaFianzaInicial || data.IdCartaFianzaInicial == null || data.IdCartaFianzaInicial == '') return '<input type="checkbox"  ng-model="vm.selected[\'' + data.IdCartaFianzaDetalleRecupero + '\']" ng-click="vm.toggleOne(vm.selected)">';
                        else return null;
                    }).notSortable(),

             DTColumnBuilder.newColumn('IdCartaFianza').notVisible(),
             DTColumnBuilder.newColumn('IdCartaFianzaDetalleRecupero').notVisible(),
             DTColumnBuilder.newColumn('IdCartaFianzaDetalleSeguimiento').notVisible(),

             DTColumnBuilder.newColumn('NumeroGarantia').withTitle('N°.Garantía'),
             DTColumnBuilder.newColumn('ImporteStr').withTitle('Importe').notSortable(),
             DTColumnBuilder.newColumn('Cliente').withTitle('Cliente'),
             DTColumnBuilder.newColumn('EntidadBancaria').withTitle('Banco'),
             DTColumnBuilder.newColumn('FechaFinServicioStr').notVisible(),

             DTColumnBuilder.newColumn('FechaSolicitudRecuperoStr').withTitle('Sol.Recupero'),
             DTColumnBuilder.newColumn('FechaRecuperoStr').withTitle('recuperamos_Fianza'),
             DTColumnBuilder.newColumn('FechaDevolucionTesoreriaStr').withTitle('devolución_Tesorería'),

             DTColumnBuilder.newColumn('StatusRecupero').withTitle('Recupero'),
             DTColumnBuilder.newColumn('Comentario').withTitle('Seguimiento'),
             DTColumnBuilder.newColumn('ColadoradorACargo').withTitle('A.cargo'),

             DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCartaFianza)
        ];

        function toggleAll(selectedItems) {
            for (var CodigoSolicitud in selectedItems) {
                if (selectedItems.hasOwnProperty(CodigoSolicitud)) {
                    selectedItems[CodigoSolicitud] = vm.selectAll;
                }
            }
        }

        function toggleOne(selectedItems) {
            for (var CodigoSolicitud in selectedItems) {

                if (selectedItems.hasOwnProperty(CodigoSolicitud)) {
                    if (!selectedItems[CodigoSolicitud]) {
                        vm.selectAll = false;
                        return;
                    }
                }
            }
            vm.selectAll = true;
        }

        function AccionesCartaFianza(data, type, full, meta) {
            let NumeroFila = meta.row;
            var respuesta = "";
            respuesta = respuesta + "<center><div class='form-group'>";
            respuesta = respuesta + " <a title='Editar' ng-click='vm.CargarModalCartaFianzaRecupero(\"" + NumeroFila + "\");'>" + "<span class='glyphicon fa fa-pencil' style='color: #3949ab; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + " <a title='Seguimiento' ng-click='vm.verDetalleSeguimiento(\"" + NumeroFila + "\");'>" + "<span class='glyphicon fa fa-tasks' style='color:#3949ab; margin-left: 1px;'></span></a>";
            respuesta = respuesta + "</div></center>";
            return respuesta;
        };//

        /***********************Carga Incial de Carta Fianza Recupero *************************/
        function BuscarCartaFianzaRecupero() {
            blockUI.start();
            vm.selected = {};
            LimpiarGrillaRecupero();
            $timeout(function () {
                vm.dtOptionsCartaFianzaRecupero = DTOptionsBuilder
                     .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                       .withFnServerData(BuscarCartaFianzaRecuperoPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)

                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })

                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarCartaFianzaRecuperoPaginado(sSource, aoData, fnCallback, oSettings) {

            var draw = aoData[0].value;
            var start = aoData[3].value;
            var size = aoData[4].value;
            var pageNumber = (start + size) / size;

            var columnNumber = aoData[2].value[0].column;
            var orderType = aoData[2].value[0].dir;
            var columnName = aoData[1].value[columnNumber].data

            vm.search.Indice = pageNumber;
            vm.search.Tamanio = size;
            vm.search.ColumnName = (columnName == null) ? "IdCartaFianzaDetalleRecupero" : columnName;
            vm.search.OrderType = orderType;

            var cartaFianza = vm.search;
            vm.selected = {};

            var promise = RecuperoCartaFianzaService.ListarCartaFianzaRecupero(cartaFianza);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListCartaFianzaRecupero
                };
                blockUI.stop();
                fnCallback(records);
            }, function (response) {
                blockUI.stop();
                LimpiarGrillaRecupero();
            });
            $("#tbCartaFianzaRecupero").removeClass("dtr-inline");
        };

        function LimpiarGrillaRecupero() {
            vm.dtOptionsCartaFianzaRecupero = DTOptionsBuilder
            .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('destroy', true)
                .withOption('scrollCollapse', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('scrollX', true)
                .withOption('paging', false);

        };

        function LimpiarFiltros() {
            vm.search = {};
            vm.search.IdEstadoRecuperacionCartaFianzaTm = '-1';
            vm.search.IdBanco = '-1';
        }

        /****************************** Combos Box *********************************/
        function CargarEntidadBancaria() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 239
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListEntidadBancaria = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarStatusRecupero() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 225
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListStatusRecupero = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarColadoradorACargo() {
            var maestra = {
                IdTipoColaboradorFianzaTm: 2
            };

            var promise = MaestroCartaFianzaService.ListarColaborador(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                $(".ListaColaboradorACargo").select2({
                    data: Respuesta,
                    placeholder: {
                        id: '-1',
                        text: '--Seleccione--',
                    },
                    allowClear: true,
                    width: 220
                });

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarModalCartaFianzaRecupero(NumeroFila) {
            let that = this;
            let cartaFianza = that.dtInstanceCartaFianzaRecupero.dataTable.fnGetData(NumeroFila);

            vm.IdentidificadoFiscal = cartaFianza.NumeroIdentificadorFiscal;
            vm.NombreCliente = cartaFianza.Cliente;

            vm.recupero.IdCartaFianza = cartaFianza.IdCartaFianza;
            vm.recupero.IdCartaFianzaDetalleRecupero = cartaFianza.IdCartaFianzaDetalleRecupero;
            vm.recupero.IdCartaFianzaDetalleSeguimiento = cartaFianza.IdCartaFianzaDetalleSeguimiento
            vm.recupero.FechaSolicitudRecupero = ConvertDate(cartaFianza.FechaSolicitudRecuperoStr);
            vm.recupero.FechaRecupero = ConvertDate(cartaFianza.FechaRecuperoStr);
            vm.recupero.FechaDevolucionTesoreria = ConvertDate(cartaFianza.FechaDevolucionTesoreriaStr);
            vm.recupero.Comentario = cartaFianza.Comentario;
            vm.recupero.IdEstadoRecuperacionCartaFianzaTm = cartaFianza.IdEstadoRecuperacionCartaFianzaTm;
            $("#IdColaboradorACargo").val(cartaFianza.IdColaboradorACargo).change();

            $('#ModalCartaFianzaRecupero').modal({
                keyboard: false
            });

        }

        /****************************** Mantenimiento *********************************/
        function GuardarCartaFianzaRecupero(cartaFianzaRecupero) {
            if (true) {
                blockUI.start();

                var promise = RecuperoCartaFianzaService.ActualizarCartaFianzaRecupero(cartaFianzaRecupero);

                promise.then(
                    //Success:
                    function (response) {
                        blockUI.stop();
                        var Respuesta = response.data;

                        if (Respuesta.TipoRespuesta != 0) {
                            UtilsFactory.Alerta('#divAlertRegistrarRecupero', 'danger', Respuesta.Mensaje, 20);
                        } else {
                            UtilsFactory.Alerta('#divAlertRegistrarRecupero', 'success', Respuesta.Mensaje, 3);
                            vm.BuscarCartaFianzaRecupero();
                            setTimeout(function () { vm.CerrarPopup(); }, 3000);

                        }
                    },
                    //Error:
                    function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlertRegistrarRecupero', 'danger', MensajesUI.DatosError, 5);
                    });
            }
        }

        /****************************** Ver Detalle Seguimiento *********************************/
        function verDetalleSeguimiento(NumeroFila) {
            let that = this;
            vm.cartaFianzaRecupero = that.dtInstanceCartaFianzaRecupero.dataTable.fnGetData(NumeroFila);
            vm.cartaFianzaRecupero.Indice = 1;
            vm.cartaFianzaRecupero.Tamanio = 100;

            blockUI.start();
            LimpiarSeguimiento();

            let viewModal = true;
            AgregarDatosDatatable(viewModal);
            
        }

        function GuardarCartaFianzaSeguimiento(cartaFianzaSeguimiento) {
            if (vm.ValidarCartaFianzaSeguimiento(cartaFianzaSeguimiento)) {
                blockUI.start();

                var promise = RecuperoCartaFianzaService.InsertaCartaFianzaSeguimiento(cartaFianzaSeguimiento);

                promise.then(
                    //Success:
                    function (response) {
                        blockUI.stop();
                        var Respuesta = response.data;

                        if (Respuesta.TipoRespuesta != 0) {
                            UtilsFactory.Alerta('#divAlertRegistrarSeguimiento', 'danger', Respuesta.Mensaje, 20);
                        } else {
                            UtilsFactory.Alerta('#divAlertRegistrarSeguimiento', 'success', Respuesta.Mensaje, 3);
                           
                            let viewModal = false;
                            AgregarDatosDatatable(viewModal);

                            LimpiarSeguimiento();
                            BuscarCartaFianzaRecupero();
                        }
                    },
                    //Error:
                    function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlertRegistrarSeguimiento', 'danger', MensajesUI.DatosError, 5);
                    });

            }

        }

        function AgregarDatosDatatable(viewModal) {
            var promise = RecuperoCartaFianzaService.ListaCartaFianzaDetalleSeguimiento(vm.cartaFianzaRecupero);
            
            promise.then(
                //Success:
                function (response) {
                    blockUI.stop();
                    vm.listaSeguimiento = response.data.ListCartaFianzaSeguimiento;
                    vm.seguimiento.IdCartaFianza = vm.cartaFianzaRecupero.IdCartaFianza;

                    vm.tablaSeguimiento = $("#tableCartaFianzaSeguimiento").DataTable();
                    vm.tablaSeguimiento.clear().draw();

                    if (vm.listaSeguimiento.length > 0) {
                        vm.listaSeguimiento.forEach((seguimiento, key) => {
                            let accionesSeguimiento = AccionesCartaFianzaSeguimiento(key);
                            vm.tablaSeguimiento.row.add([key + 1, seguimiento.StatusRecupero, seguimiento.Comentario, seguimiento.ColadoradorACargo, accionesSeguimiento]).draw();
                        });
                    }

                    if (viewModal) {
                        $('#ModalCartaFianzaSeguimiento').modal({
                            keyboard: false
                        });
                    }
                },
                //Error:
                function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertRegistrarRecupero', 'danger', MensajesUI.DatosError, 5);
                });
        }

        function AccionesCartaFianzaSeguimiento(key) {
            var respuesta = "";
            respuesta = respuesta + "<center><div class='form-group'>";
            respuesta = respuesta + " <a title='Editar' onclick='vm.EditarSeguimiento(\"" + key + "\");'>" + "<span class='glyphicon fa fa-pencil' style='color: #3949ab; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + " <a title='Eliminar' onclick='vm.EliminarSeguimiento(\"" + key + "\");'>" + "<span class='glyphicon fa fa-trash-o' style='color:#3949ab; margin-left: 1px;'></span></a>";
            respuesta = respuesta + "</div></center>";
            return respuesta;
        };

        function ValidarCartaFianzaSeguimiento(seg) {
            var response = false;

            if (!seg.Comentario || seg.Comentario.length < 4) {
                UtilsFactory.Alerta('#divAlertRegistrarSeguimiento', 'danger', 'Ingrese al menos un Comentario', 20);
            }
            else {
                response = true;
            }

            return response;
        }

        function LimpiarSeguimiento() {
            vm.seguimiento.Comentario = "";
            vm.seguimiento.IdEstadoRecuperacionCartaFianzaTm = '-1';
            $("#IdColaboradorACargoSeg").val(-1).change().val(-1);
        }

        function EditarSeguimiento(NumeroFila) {
            let respuesta = vm.listaSeguimiento;
            let fila = respuesta[NumeroFila];
            vm.habilitarEditar = false;
            vm.seguimiento.IdCartaFianza = fila.IdCartaFianza;
            vm.seguimiento.IdCartaFianzaDetalleSeguimiento = fila.IdCartaFianzaDetalleSeguimiento;

            vm.seguimiento.Comentario = fila.Comentario;
            vm.seguimiento.IdEstadoRecuperacionCartaFianzaTm = fila.IdEstadoRecuperacionCartaFianzaTm;
            $("#IdColaboradorACargoSeg").val(fila.IdColaboradorACargo).change();
        }

        function EliminarSeguimiento(NumeroFila) {
            let respuesta = vm.listaSeguimiento;
            let fila = respuesta[NumeroFila];
            fila.Eliminado = 1;

            vm.EditarCartaFianzaSeguimiento(fila);

        }

        function EditarCartaFianzaSeguimiento(cartaFianzaSeguimiento) {
                GuardarCartaFianzaRecupero(cartaFianzaSeguimiento);
                setTimeout(() => {
                    let viewModal = false;
                    AgregarDatosDatatable(viewModal);
                    LimpiarSeguimiento();
                    vm.habilitarEditar = true;
                }, 500);
        }

        /*********************** Revertir Recupero *************************/
        function VerificarItemSelecionadosCartaFianza() {
            vm.vSIdCartaFianza = '';
            for (var CodigoSolicitud in vm.selected) {
                if (vm.selected.hasOwnProperty(CodigoSolicitud)) {
                    if (vm.selected[CodigoSolicitud]) {
                        vm.vSIdCartaFianza = vm.vSIdCartaFianza + "" + CodigoSolicitud + ",";
                    }
                }
            }
            console.log(vm.vSIdCartaFianza);
            $('#ModalRevertirRecupero').modal({
                keyboard: false
            });

        }

        function RevertirRecupero() {
            blockUI.start();
            var vIdTipoAccionTm = 4;
            var vIdRecuperoTm = -2

            if (vm.vSIdCartaFianza != '' || (vm.vSIdCartaFianza).length > 2) {
                //  vm.CmbTipoRecuperoTm = -2;//PARA REVETIR
                var maestra = {
                    IdCartaFianzastr: vm.vSIdCartaFianza,
                    IdTipoAccionTm: vIdTipoAccionTm,
                    IdRenovarTm: vIdRecuperoTm,

                    FechaRespuestaFinalGcSm: '01/01/1753' ,
                    FechaVencimientoRenovacion : '01/01/1753' ,
                    ValidacionContratoGcSm : '-',
                    SustentoRenovacion : '-',
                    Observacion : '-',
                    PeriodoMesRenovacion : 0

                };

                var promise = RenovacionCartaFianzaService.ActualizaARenovacionCartaFianza(maestra);

                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        vm.Id = Respuesta.TipoRespuesta
                        UtilsFactory.Alerta('#divAlertRevertirRecupero', 'danger', Respuesta.Mensaje, 20);
                    } else {
                        vm.Id = Respuesta.TipoRespuesta;
                        UtilsFactory.Alerta('#divAlertRevertirRecupero', 'success', Respuesta.Mensaje, 5);
                        $('#ModalRevertirRecupero').modal('hide');

                        BuscarCartaFianzaRecupero();

                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertRevertirRecupero', 'danger', MensajesUI.DatosError, 5);
                });

            } else {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRevertirRecupero', 'danger', 'debe selecionar al menos 1 item', 5);
            }

        };

        /*********************** Eventos Change *************************/
        function FechaSolicitudRecuperoChange() {
            if ((vm.recupero.Comentario == null || vm.recupero.Comentario == "") && (vm.recupero.FechaRecupero == null || vm.recupero.FechaRecupero == "Invalid Date")) {
                vm.recupero.IdEstadoRecuperacionCartaFianzaTm = 2;
            }
        }

        function FechaRecuperoChange() {
            vm.recupero.IdEstadoRecuperacionCartaFianzaTm = 4;
        }

        function ComentarioChange() {
            if (vm.recupero.FechaRecupero == null || vm.recupero.FechaRecupero == "Invalid Date") {
                vm.recupero.IdEstadoRecuperacionCartaFianzaTm = 6;
            }
        }

        /*********************** Otras funciones *************************/
        function CerrarPopup() {
            $('#ModalCartaFianzaRecupero').modal('hide');
        };

        function ConvertDate(fecha) {
            let fechaSplit = fecha.split("/");
            let fechaFormat = fechaSplit[2] + "/" + fechaSplit[1] + "/" + fechaSplit[0];
            return new Date(fechaFormat);
        }

        $.fn.modal.Constructor.prototype.enforceFocus = function () { };

        vm.search.IdEstadoRecuperacionCartaFianzaTm = '-1';
        vm.search.IdBanco = '-1';
        vm.CargarEntidadBancaria();
        vm.CargarStatusRecupero();
        vm.CargarColadoradorACargo();
        BuscarCartaFianzaRecupero();

    }

})();