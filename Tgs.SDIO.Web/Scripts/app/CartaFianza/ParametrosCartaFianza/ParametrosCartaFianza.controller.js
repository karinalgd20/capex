﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('ParametrosCartaFianzaController', ParametrosCartaFianzaController);

    ParametrosCartaFianzaController.$inject = [
            'ParametrosCartaFianzaService',
            'MaestroCartaFianzaService',
            'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];

    function ParametrosCartaFianzaController(
            ParametrosCartaFianzaService,
            MaestroCartaFianzaService,
            MaestraService,
            blockUI,
            UtilsFactory,
            DTColumnDefBuilder,
            DTOptionsBuilder,
            DTColumnBuilder,
            $timeout,
            MensajesUI,
            $urls,
            $scope,
            $compile,
            $modal,
            $injector) {

        vm = this;
        vm.search = {};
        vm.param = {};
        vm.delete = {};
        vm.listaParametros = null;
        vm.deshabilitarNuevoParametro = true;
        vm.deshabilitarInputValor = true;
        vm.deshabilitarInputDescripcion = true;
        vm.deshabilitarInputIdOpcion = true;

        vm.EditarParametro = EditarParametro;
        vm.NuevoParametro = NuevoParametro;
        vm.GuardarParamentros = GuardarParamentros;
        vm.EliminarSeguimiento = EliminarSeguimiento;
        vm.LimpiarParametros = LimpiarParametros;
        vm.CerrarPopup = CerrarPopup;
        vm.habilitarContenido = habilitarContenido;

        vm.CargarTtablaParametros = CargarTtablaParametros;

        var tablaParametros = $("#tablaParametros").DataTable({
            pageLength: 10,
            lengthMenu: [[10, 20, 50, -1], [10, 20, 50, 'Todos']],
            columnDefs: [{ "width": "5%", "targets": 0 }, { "width": "5%", "targets": 3 }]
        });

        /***************** Cargar tablas de Parametros ***************/
        function CargarTtablaParametros() {
            blockUI.start();

            var maestra = {
                IdEstado: 1,
                IdRelacion: vm.search.IdRelacion
            };

            var promise = MaestraService.ObtenerMaestra(maestra);

            promise.then((response) => {
                blockUI.stop();
                vm.listaParametros = response.data;

                tablaParametros.clear().draw();

                if (vm.listaParametros.length > 0) {
                    vm.listaParametros.forEach((datos, key) => {
                        let accionesSeguimiento = AccionesCartaFianzaSeguimiento(key);
                        tablaParametros.row.add([key + 1, datos.Descripcion, datos.IdOpcion, accionesSeguimiento]).draw();
                    });
                }

                vm.habilitarContenido();

            });

        }

        function AccionesCartaFianzaSeguimiento(key) {
            var respuesta = "";
            respuesta = respuesta + "<center><div class='form-group'>";
            respuesta = respuesta + " <a title='Editar' onclick='vm.EditarParametro(\"" + key + "\");'>" + "<span class='glyphicon fa fa-pencil' style='color: #3949ab; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + " <a title='Eliminar' onclick='vm.EliminarSeguimiento(\"" + key + "\");'>" + "<span class='glyphicon fa fa-trash-o' style='color:#3949ab; margin-left: 1px;'></span></a>";
            respuesta = respuesta + "</div></center>";
            return respuesta;
        };

        /***************** Mentenimiento ***************/
        function EditarParametro(NumeroFila) {
            let fila = vm.listaParametros[NumeroFila];
            vm.param.IdMaestra = fila.IdMaestra;
            vm.param.Descripcion = fila.Descripcion;
            vm.param.Comentario = fila.Comentario;
            vm.param.Valor = fila.Valor;
            vm.param.IdOpcion = fila.IdOpcion;
            vm.param.IdEstado = 1;
            $("#Color").change();

            $(".tituloModal").text("ACTUALIZAR");
            $('#ModalRegistroParametros').modal({
                keyboard: false
            });
        }

        function EliminarSeguimiento(NumeroFila) {
            let fila = vm.listaParametros[NumeroFila];
            vm.delete = fila;
            vm.delete.IdEstado = 0;

            $('#ModalEliminiarParametro').modal({
                keyboard: false
            });
        }

        function NuevoParametro() {
            vm.LimpiarParametros();
            $(".tituloModal").text("NUEVO");
            vm.param.IdMaestra = 0;
            vm.param.IdRelacion = vm.search.IdRelacion;
            let maxOption = vm.listaParametros.map((param) => { return param.IdOpcion; });
            vm.param.IdOpcion = (Math.max.apply(null, maxOption) + 1);
            vm.param.Valor = vm.param.IdOpcion;
            $('#ModalRegistroParametros').modal({
                keyboard: false
            });
        }

        function LimpiarParametros() {
            vm.param = {};
        }

        function CerrarPopup() {
            $('#ModalRegistroParametros').modal('hide');
            $('#ModalEliminiarParametro').modal('hide');
        }

        /***************** Guardar Asignaciones ***************/
        function GuardarParamentros(parametros, divAlerta) {
            blockUI.start();

            var promise = null;
            
            if (parametros.IdMaestra > 0) {
                promise = MaestraService.ActualizarMaestra(parametros);
            } else {
                vm.param.Comentario = parametros.Descripcion;
                promise = MaestraService.RegistrarMaestra(parametros);
            }
            
            promise.then(
                //Success:
                function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;

                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#' + divAlerta, 'danger', Respuesta.Mensaje, 5);
                    } else {
                        UtilsFactory.Alerta('#' + divAlerta, 'success', Respuesta.Mensaje, 3);
                        CargarTtablaParametros();
                        setTimeout(() => { vm.CerrarPopup(); $("#" + divAlerta).empty(); }, 3000);
                    }
                },
                //Error:
                function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#' + divAlerta, 'danger', MensajesUI.DatosError, 5);
                });

        }

        /***************** Otras funciones ***************/
        function habilitarContenido() {
            if (vm.search.IdRelacion == 183 || vm.search.IdRelacion == 186 || vm.search.IdRelacion == 189 || vm.search.IdRelacion == 194) {
                vm.deshabilitarNuevoParametro = false;
                vm.deshabilitarInputValor = true;
                vm.deshabilitarInputDescripcion = false;
                vm.deshabilitarInputIdOpcion = true;
            } else if (vm.search.IdRelacion == 212 || vm.search.IdRelacion == 225) {
                vm.deshabilitarNuevoParametro = true;
                vm.deshabilitarInputValor = true;
                vm.deshabilitarInputDescripcion = false;
                vm.deshabilitarInputIdOpcion = true;
            }else {
                vm.deshabilitarNuevoParametro = true;
                vm.deshabilitarInputValor = true;
                vm.deshabilitarInputDescripcion = true;
                vm.deshabilitarInputIdOpcion = false;
            }
        }

        /***************** Inicializar funciones ***************/
        vm.search.IdRelacion = '0';
        vm.CargarTtablaParametros();
    }

})();