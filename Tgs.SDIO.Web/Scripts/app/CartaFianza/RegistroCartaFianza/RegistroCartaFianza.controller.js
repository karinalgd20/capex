﻿(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('RegistroCartaFianzaController', RegistroCartaFianzaController);

    RegistroCartaFianzaController.$inject = ['RegistroCartaFianzaService','AccionCartaFianzaService',  'MaestraService', 'blockUI', 'UtilsFactory', 'DTColumnDefBuilder', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistroCartaFianzaController(RegistroCartaFianzaService, AccionCartaFianzaService, MaestraService, blockUI, UtilsFactory, DTColumnDefBuilder, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.IdCliente = 0;
        vm.IdCartaFianza = 0;
        vm.Id = 0;

        vm.ListTipoProyecto = [];
        vm.CmbTipoContratoTm = '-1';
       
        vm.ListTipoAccion = [];
        vm.CmbTipoAccion = '1';

        vm.ListEmpresaAdjudicada = [];
        vm.CmbEmpresaAdjudicadaTm = '-1';

        vm.ListTipoGarantia = [];
        vm.CmbTipoGarantiaTm = '-1';

        vm.ListEntidadBancaria = [];
        vm.CmbBanco = '-1';

        vm.ListTipoMoneda = [];
        vm.CmbTipoMoneda = '-1';

        vm.ListTipoProceso = [];
        vm.CmbTipoProcessoTm = '-1';

        vm.regCartaFianza = [];
        
        vm.GrabarCartaFianza = GrabarCartaFianza;
        vm.BuscarCartaFianzaId = BuscarCartaFianzaId;
        vm.ActivarPanelGarantia = ActivarPanelGarantia;
        vm.EnviarRegistroCartaFianza = EnviarRegistroCartaFianza;
        vm.eventoRadio = eventoRadio;
    
        //vm.InsertaAccionesCartaFianzaErrada = InsertaAccionesCartaFianzaErrada;
        //vm.EnvioAccionCartaFianzaErrada = EnvioAccionCartaFianzaErrada;
        vm.existeFecha = existeFecha;

        vm.LimpiarFiltros = LimpiarFiltros;
        // vm.optionsRadios = 0;
    
        vm.AlertaError = AlertaError();

        vm.ModalAdvertenciaAsingacionCerrar = ModalAdvertenciaAsingacionCerrar;
       
       
        /******************************************* Graba o Actualiza Registro de Carta Fianza *******************************************/

        function GrabarCartaFianza() {
            var vFechaFirmaServicioContrato = null;
            var vFechaFinServicioContrato = null;

            if (vm.CmbTipoAccion == 0 || vm.CmbTipoAccion == null || vm.CmbTipoAccion == -1) {
               
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'Selecione un tipo de Acción', 20);
                 AlertaError();
            
            } else if (vm.CmbTipoMoneda == 1 && (vm.ImporteCartaFianzaSoles <= 0 || vm.ImporteCartaFianzaSoles == null)) {

                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'Ingrese un Monto en Soles mayor a 0', 20);
                    AlertaError()
            } else if (vm.CmbTipoMoneda == 2 && (vm.ImporteCartaFianzaDolares <= 0 || vm.ImporteCartaFianzaDolares == null)) {

                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'Ingrese un Monto en Dolares mayor a 0 ', 20);
                    AlertaError();
            } else if (vm.CmbTipoMoneda == 0 || vm.CmbTipoMoneda == null || vm.CmbTipoMoneda == -1) {

                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'Selecione un Tipo de Moneda ', 20);
                    AlertaError();
            } else if (vm.CmbTipoContratoTm == 0 || vm.CmbTipoContratoTm == null || vm.CmbTipoContratoTm == -1) {

                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'Selecione un Tipo de Contrato ', 20);
                    AlertaError();
            } else if ((vm.FechaFinServicioContrato != null && vm.FechaFirmaServicioContrato != null) && (vm.FechaFinServicioContrato <= vm.FechaFirmaServicioContrato)) {

                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'La fecha fin de servicio debe ser mayor a la fecha de firma del contrato', 20);
                    AlertaError();
            } else if ((vm.FechaVigenciaBanco != null && vm.FechaVencimientoBanco != null) && (vm.FechaVencimientoBanco <= vm.FechaVigenciaBanco)) {
              
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'La fecha fin de vencimiento debe ser mayor  a la fecha de firma del contrato', 20);
                    AlertaError();
            }else if ($('#Servicio').val().length == 0 || vm.Servicio == null || vm.Servicio == '') {

                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'Ingrese la Descripción del Servicio ', 20);
                    AlertaError();
            }
            
            else if ( document.getElementById("optionsRadios2").checked ==1 &&  ($('#MotivoCartaFianzaErrada').val().length == 0 || vm.MotivoCartaFianzaErrada == null || vm.MotivoCartaFianzaErrada == '')) {

                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'Ingrese el Motivo de la C.F errada ', 20);
                    AlertaError();
            }
             

            else {
                var v_emision = 0;
    
                if (  ( vm.NumeroGarantia.length > 2) ) {   v_emision = v_emision + 1 ;   }
                if (  (vm.CmbBanco != -1)  )  {   v_emision = v_emision + 1 ;   }
                if (   vm.existeFecha(vm.FechaVencimientoBanco) != false  )   {   v_emision = v_emision + 1 ;   }
                 
                if ( v_emision >=1 && v_emision < 3 ) {
                    UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'El sistema requerire Fecha Renovación, Banco y Número de Garantía ', 20);
                    AlertaError();
                
                } else if (vm.CmbTipoMoneda == 1 || vm.CmbTipoMoneda == 2) {
                    EnviarRegistroCartaFianza();
                } else {
                    UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'El sistema Solo Acepta Soles o Dolar Como tipo de moneda', 20);
                    AlertaError();
                }

            }
        }
        
        function EnviarRegistroCartaFianza() {
            var checkbox = document.getElementById("ClienteEspecial");

            var checkbox1 = document.getElementById("SeguimientoImportante");

            if (vm.existeFecha(vm.FechaVencimientoBanco) !=false &&  vm.CmbTipoAccion == 2)
            {
                vm.IdRenovarTm = 4; //seguimiento 
                vm.IdEstadoCartaFianzaTm = 5;
                vm.IdTipoSubEstadoCartaFianzaTm = 8;
            }
            else
            {
                vm.IdEstadoCartaFianzaTm = 6;
                vm.IdRenovarTm = 3; //por atender
                vm.IdTipoSubEstadoCartaFianzaTm = -1;
            }

            if (vm.optionsRadios == 1 &&  vm.CmbTipoAccion == 1) {
                vm.IdTipoSubEstadoCartaFianzaTm = 12 //12	Envio de CF a Correción:CartaFianza Errada                              
            }

            if(existeFecha(vm.FechaCorrecionTesoreria)!=false &&  vm.CmbTipoAccion == 1){
                vm.IdTipoSubEstadoCartaFianzaTm = 11; //11	Fecha de Recepción:CartaFianza Errada
            }


            if (vm.CmbTipoAccion == 1 && vm.NumeroGarantia != null && vm.CmbBanco != -1 && vm.existeFecha(vm.FechaVencimientoBanco) != false) {

                vm.CmbTipoAccion = 2;
            }


            var cartafianza = {
                IdCartaFianza: vm.IdCartaFianza,
                IdCliente: vm.IdCliente,
                NumeroOportunidad: vm.NumeroOportunidad,
                IdTipoContratoTm: vm.CmbTipoContratoTm,
                NumeroContrato: vm.NumeroContrato,
                IdProcesoTm: vm.CmbTipoProcessoTm,

                NumeroProceso : vm.NumeroProceso,

                Servicio : vm.Servicio,
                DescripcionServicioCartaFianza: vm.DescripcionServicioCartaFianza,
                FechaFirmaServicioContrato: vm.FechaFirmaServicioContrato,

                FechaFinServicioContrato: vm.FechaFinServicioContrato,
                IdEmpresaAdjudicadaTm: vm.CmbEmpresaAdjudicadaTm,

                IdTipoGarantiaTm: vm.CmbTipoGarantiaTm,
                NumeroGarantia: vm.NumeroGarantia,
                IdTipoMonedaTm: vm.CmbTipoMoneda,
                ImporteCartaFianzaSoles: vm.ImporteCartaFianzaSoles,
                ImporteCartaFianzaDolares: vm.ImporteCartaFianzaDolares,
                IdBanco: vm.CmbBanco,

                FechaEmisionBanco: vm.FechaEmisionBanco,
                FechaVigenciaBanco: vm.FechaVigenciaBanco,
                FechaVencimientoBanco: vm.FechaVencimientoBanco,

                IdTipoAccionTm: vm.CmbTipoAccion,
                IdRenovarTm: vm.IdRenovarTm,
                //IdEstadoVencimientoTm   :   vm.IdEstadoVencimientoTm,
                IdEstadoCartaFianzaTm: vm.IdEstadoCartaFianzaTm,
                IdTipoSubEstadoCartaFianzaTm : vm.IdTipoSubEstadoCartaFianzaTm,

                ClienteEspecial: checkbox.checked,//vm.ClienteEspecial,
                SeguimientoImportante: checkbox1.checked,// vm.SeguimientoImportante,
                Observacion: vm.Observacion,
                Incidencia: vm.Incidencia,
                IdEstado: vm.optionsRadios,

                IdUsuarioCreacion: vm.IdUsuarioCreacion,

                FechaCorrecionTesoreria: vm.FechaCorrecionTesoreria,
                FechaRecepcionTesoreria: vm.FechaRecepcionTesoreria,
                MotivoCartaFianzaErrada : vm.MotivoCartaFianzaErrada 

            };
            blockUI.start();
            var promise = (vm.IdCartaFianza == 0) ? RegistroCartaFianzaService.InsertarRegistroCartaFianza(cartafianza) : RegistroCartaFianzaService.ActualizarCartaFianza(cartafianza);
            promise.then(function (response) {
                blockUI.stop();
                var Respuesta = response.data;

                if (Respuesta.TipoRespuesta != 0) {
                    UtilsFactory.Alerta('#divAlertRegistrar', 'danger', Respuesta.Mensaje, 20);
                      
                } else {
                    vm.Id = Respuesta.Id;
                    if (vm.Id != 0) {
                        // InactivarCampos();
                    }
                    AlertaError();
                    UtilsFactory.Alerta('#divAlertRegistrar', 'success', Respuesta.Mensaje, 10);
                   // alert(Respuesta.Mensaje);
                    var time = 1500;
                    if ((vm.NumeroGarantia != '' || vm.NumeroGarantia != null) && (vm.CmbBanco != -1) && vm.existeFecha(vm.FechaVencimientoBanco) != false) {
                        time = 5000;
                        UtilsFactory.Alerta('#divAlertRegistrar', 'warning', Respuesta.Mensaje +';<br/> verifique si está cliente Fue Asignada a un Gerente C.', 20);
                    }



                    setTimeout(() => {
                        $scope.$parent.vm.CerrarPopup();
                        try{
                          $scope.$parent.vm.BuscarCartaFianza();
                        }catch(e){}
                        try {
                            $scope.$parent.vm.BuscarCartaFianzaRenovacion();
                        } catch (e) { }
                        
                        if (vm.optionsRadios == 1) {
                        cartafianza.IdTipoSubEstadoCartaFianzaTm = 5
                        $scope.$parent.vm.ObtenerDatosCorreoCF_Errada(cartafianza);
                    }

                    }, time);
                   
                 
                    
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', MensajesUI.DatosError, 5);
              
            });

          
        }

        function eventoRadio(rb) {
            var radio1 = document.getElementById("optionsRadios1");
            var radio2 = document.getElementById("optionsRadios2");
            switch (rb) {
                case "si":
                    radio2.checked = 1;
                    radio1.checked = 0;
                    vm.optionsRadios = 1;
                    document.getElementById("MotivoCartaFianzaErrada").disabled = false;
                    break;
                case "no":
                    radio1.checked = 1;
                    radio2.checked = 0;
                    vm.optionsRadios = 0;
                    document.getElementById("MotivoCartaFianzaErrada").disabled = true;
                    break;
              
            }
        }
        /************Carga Inical de Combos**********/

        function CargarTipoContrato() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 186
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoContrato = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarTipoAccion() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 200
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoAccion = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
      
        function CargarEmpresaAdjudicada() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 189
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListEmpresaAdjudicada = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarTipoGarantia() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 194

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoGarantia = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarEntidadBancaria() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 239

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListEntidadBancaria = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarEstadoCartaFianza() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 212

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                // vm.ListEntidadBancaria = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarTipoMoneda() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 98

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoMoneda = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarTipoProceso() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 269

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoProceso = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function BuscarCartaFianzaId() {

            if ($scope.$parent.vm.IdCliente != 0) {

                vm.NombreCliente = $scope.$parent.vm.BNombreCliente;
                vm.IdentificadorFiscal = $scope.$parent.vm.BNumeroIdentificador;
                vm.IdCliente = $scope.$parent.vm.IdCliente;

            } else {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'Selecionar Un Cliente Primero', 20);

            }

            if ($scope.$parent.vm.IdCartaFianza != 0) {

                var RegCartaFianza = {
                    IdCartaFianza: $scope.$parent.vm.IdCartaFianza,
                    IdCliente: $scope.$parent.vm.IdCliente
                };
       
                var promise = RegistroCartaFianzaService.ListarCartaFianzaId(RegCartaFianza);

                promise.then(function (resultado) {
                    blockUI.stop();
                    //debugger;
                    var Respuesta = resultado.data;
                
                    vm.IdCartaFianza = Respuesta[0].IdCartaFianza;
                    vm.CmbTipoAccion = Respuesta[0].IdTipoAccionTm;

                    vm.IdCliente = Respuesta[0].IdCliente;
                    vm.IdentificadorFiscal = Respuesta[0].NumeroIdentificadorFiscal;
                    vm.NombreCliente = Respuesta[0].NombreCliente;

                    vm.NumeroOportunidad = Respuesta[0].NumeroOportunidad;
                    vm.CmbTipoContratoTm = Respuesta[0].IdTipoContratoTm;
                    vm.NumeroContrato = Respuesta[0].NumeroContrato;
                    vm.CmbTipoProcessoTm = Respuesta[0].IdProcesoTm;
                    vm.NumeroProceso = Respuesta[0].NumeroProceso;
                    vm.Servicio = Respuesta[0].Servicio,
                    vm.DescripcionServicioCartaFianza = Respuesta[0].DescripcionServicioCartaFianza;

                    var vFechaFirmaServicioContrato = Respuesta[0].FechaFirmaServicioContratostr;
                    var vFechaFirmaServicioContrato = vFechaFirmaServicioContrato.substring(3, 5)+'-'+ vFechaFirmaServicioContrato.substring(0, 2) + '-' + vFechaFirmaServicioContrato.substring(6, 10);
                    vm.FechaFirmaServicioContrato = new Date(vFechaFirmaServicioContrato);

                    var vFechaFinServicioContrato = Respuesta[0].FechaFinServicioContratostr;
                    var vFechaFinServicioContrato = vFechaFinServicioContrato.substring(3, 5) + '-' + vFechaFinServicioContrato.substring(0, 2) + '-' + vFechaFinServicioContrato.substring(6, 10);

                    vm.FechaFinServicioContrato = new Date(vFechaFinServicioContrato);
                    vm.CmbTipoAccion = Respuesta[0].IdTipoAccionTm;

                    vm.CmbEmpresaAdjudicadaTm = Respuesta[0].IdEmpresaAdjudicadaTm;
                    vm.CmbTipoGarantiaTm = Respuesta[0].IdTipoGarantiaTm;
                    vm.NumeroGarantia = Respuesta[0].NumeroGarantia;
                    vm.CmbBanco = Respuesta[0].IdBanco;

                    var vFechaEmisionBanco = Respuesta[0].FechaEmisionBancostr;
                    var vFechaEmisionBanco = vFechaEmisionBanco.substring(3, 5) + '-' + vFechaEmisionBanco.substring(0, 2) + '-' + vFechaEmisionBanco.substring(6, 10);
                    vm.FechaEmisionBanco = new Date(vFechaEmisionBanco);

                    var vFechaVigenciaBanco = Respuesta[0].FechaVigenciaBancostr;
                    var vFechaVigenciaBanco = vFechaVigenciaBanco.substring(3, 5) + '-' + vFechaVigenciaBanco.substring(0, 2) + '-' + vFechaVigenciaBanco.substring(6, 10);
                    vm.FechaVigenciaBanco = new Date(vFechaVigenciaBanco);

                    var vFechaVencimientoBanco = Respuesta[0].FechaVencimientoBancostr;
                    var vFechaVencimientoBanco = vFechaVencimientoBanco.substring(3, 5) + '-' + vFechaVencimientoBanco.substring(0, 2) + '-' + vFechaVencimientoBanco.substring(6, 10);
                    vm.FechaVencimientoBanco = new Date(vFechaVencimientoBanco);

                    var vFechaCorrecionTesoreria = Respuesta[0].FechaCorrecionTesoreriaStr;
                    var vFechaCorrecionTesoreria = vFechaCorrecionTesoreria.substring(3, 5) + '-' + vFechaCorrecionTesoreria.substring(0, 2) + '-' + vFechaCorrecionTesoreria.substring(6, 10);
                    vm.FechaCorrecionTesoreria = new Date(vFechaCorrecionTesoreria);

                    var vFechaRecepcionTesoreria = Respuesta[0].FechaRecepcionTesoreriaStr;
                    var vFechaRecepcionTesoreria = vFechaRecepcionTesoreria.substring(3, 5) + '-' + vFechaRecepcionTesoreria.substring(0, 2) + '-' + vFechaRecepcionTesoreria.substring(6, 10);
                    vm.FechaRecepcionTesoreria = new Date(vFechaRecepcionTesoreria);

                    var fechadescargo = new Date(vFechaVencimientoBanco);
                    if (Respuesta[0].FechaVencimientoBancostr != null || Respuesta[0].FechaVencimientoBancostr !='') {
                        vm.FechaDescargoBanco =new Date( fechadescargo.setDate( fechadescargo.getDate() + 15));
                    }

                    vm.CmbTipoMoneda = Respuesta[0].IdTipoMonedaTm;

                    vm.ImporteCartaFianzaSoles = Respuesta[0].ImporteCartaFianzaSoles 
                    vm.ImporteCartaFianzaDolares = Respuesta[0].ImporteCartaFianzaDolares 
                    vm.Incidencia = Respuesta[0].Incidencia;
                    vm.Observacion = Respuesta[0].Observacion;
                    vm.ClienteEspecial = Respuesta[0].ClienteEspecial;
                    vm.SeguimientoImportante = Respuesta[0].SeguimientoImportante;
     
                    vm.IdEstadoCartaFianzaTm = Respuesta[0].IdEstadoCartaFianzaTm;
                    vm.IdRenovarTm = Respuesta[0].IdRenovarTm;

                    vm.MotivoCartaFianzaErrada = Respuesta[0].MotivoCartaFianzaErrada;
                      
                    var checkbox = document.getElementById("ClienteEspecial");
                    checkbox.checked = Respuesta[0].ClienteEspecial;

                    var checkbox1 = document.getElementById("SeguimientoImportante");
                    checkbox1.checked = Respuesta[0].SeguimientoImportante;

                    var radio1 = document.getElementById("optionsRadios1");
                    var radio2 = document.getElementById("optionsRadios2");

                    if (Respuesta[0].IdEstado == 1) {
                        radio2.checked = 1;
                        radio1.checked = 0;
                        vm.optionsRadios = Respuesta[0].IdEstado;
                           
                                 
                    } else {

                        radio1.checked = 1;
                        radio2.checked = 0;
                        $('FechaCorrecionTesoreria').attr('disabled', 'disabled');
                        $('FechaRecepcionTesoreria').attr('disabled', 'disabled');
                        $('MotivoCartaFianzaErrada').attr('disabled', 'disabled');
                        $('FechaCorrecionTesoreria').prop("disabled", true);
                    }
              
                }, function (response) {
                    blockUI.stop();

                });
            } else {

                vm.CmbTipoAccion = '1';
                
            }


        }
     
        function ActivarPanelGarantia() {

            $('NumeroGarantia').attr('disabled', 'disabled');

        }

        function existeFecha(fecha) {
            if (fecha == 'Invalid Date' || fecha == undefined || fecha == null) {
                return false;
            }
            return true;
        }

        function LimpiarFiltros() {
            vm.NumeroIdentificador = null;
            vm.NombreCliente = null;
            vm.NumeroContrato = null;
            vm.NumeroGarantia = null;
            vm.ImporteCartaFianzaSoles = null;
        }

        function AlertaError() {
                $('#ModalCartaFianza').scrollTop(0);
        }

        function ModalAdvertenciaAsingacionCerrar() {

            $('#ModalAdvertenciaAsingacion').modal('hide');
        }

        /************Carga Iniciales**********/

        vm.NumeroGarantia = "";
        LimpiarFiltros();
        CargarTipoGarantia();
        CargarEmpresaAdjudicada();
        CargarTipoAccion();
        CargarTipoContrato();
        CargarEntidadBancaria();
        CargarTipoMoneda();
        CargarTipoProceso();

        ActivarPanelGarantia();

        BuscarCartaFianzaId();

      



    }



})();