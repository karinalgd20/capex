﻿(function () {
    'use strict',
     angular
    .module('app.CartaFianza')
    .service('RegistroCartaFianzaService', RegistroCartaFianzaService);
 
    RegistroCartaFianzaService.$inject = ['$http', 'URLS'];


    function RegistroCartaFianzaService($http, $urls) {
      
        var service = {
            ListarCartaFianzaId: ListarCartaFianzaId,
            ModalCartaFianzaRegistrar: ModalCartaFianzaRegistrar,
            InsertarRegistroCartaFianza: InsertarRegistroCartaFianza,
            ActualizarCartaFianza: ActualizarCartaFianza

        };

        return service;



             function ListarCartaFianzaId(cartaFianza) {

                    return $http({
                        url: $urls.ApiCartaFianza + "RegistroCartaFianza/ListarCartaFianzaId",
                        method: "POST",
                        data: JSON.stringify(cartaFianza)
                    }).then(DatosCompletados);

                    function DatosCompletados(resultado) {
                        return resultado;
                    }
                };

            function ModalCartaFianzaRegistrar(dto) {
            //regCartaFianza = dto;
           // console.log(regCartaFianza);
            return $http({
                url: $urls.ApiCartaFianza + "RegistroCartaFianza/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
          };

            function InsertarRegistroCartaFianza(casoNegocio) {
              return $http({
                  url: $urls.ApiCartaFianza + "RegistroCartaFianza/InsertaCartaFianza",
                  method: "POST",
                  data: JSON.stringify(casoNegocio)
              }).then(DatosCompletados);

              function DatosCompletados(response) {
                  return response;
              }
          };

            function ActualizarCartaFianza(casoNegocio)
            {
              return $http({
                  url: $urls.ApiCartaFianza + "RegistroCartaFianza/ActualizaCartaFianza",
                  method: "POST",
                  data: JSON.stringify(casoNegocio)
              }).then(DatosCompletados);

              function DatosCompletados(response) {
                  return response;
              }
          };





    }



})();