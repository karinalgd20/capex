﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('ReporteConsolidadoCartaFianzaController', ReporteConsolidadoCartaFianzaController);

    ReporteConsolidadoCartaFianzaController.$inject = [
            'ReporteConsolidadoCartaFianzaService',
            'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];

    function ReporteConsolidadoCartaFianzaController(
            ReporteConsolidadoCartaFianzaService,
            MaestraService,
            blockUI,
            UtilsFactory,
            DTColumnDefBuilder,
            DTOptionsBuilder,
            DTColumnBuilder,
            $timeout,
            MensajesUI,
            $urls,
            $scope,
            $compile,
            $modal,
            $injector) {

        vm = this;

        vm.BuscarReporteConsolidado = BuscarReporteConsolidado;
        vm.LimpiarFiltros = LimpiarFiltros;

        vm.search = {};
        /************** Cargar Combo box **************/
        function CargarEstado() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 212
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaEstado = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarTipoAccion() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 200
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaTipoAccion = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function BuscarReporteConsolidado() {
            var cadena = "";

            blockUI.start();

            var parametros =
            "reporte=CartaFianzaConsolidado" +
            "&NumeroOportunidad=" + vm.search.NumeroOportunidad +
            "&NumeroGarantia=" + vm.search.NumeroGarantia +
            "&NombreCliente=" + vm.search.NombreCliente +
            "&Importe=" + vm.search.Importe +
            "&IdEstado=" + vm.search.IdEstado +
            "&IdTipoAccion=" + vm.search.IdTipoAccion;

            $("[id$='FReporte']").attr('src', $urls.ApiReportes + parametros);

            $("#FReporte").height(800);

            $timeout(function () {
                blockUI.stop();
            }, 2000);
        };

        function LimpiarFiltros() {
            vm.search = {};
            vm.search.IdEstado = -1;
            vm.search.IdTipoAccion = -1;
        }

        vm.search.IdEstado = -1;
        vm.search.IdTipoAccion = -1;
        CargarEstado();
        CargarTipoAccion();
        vm.BuscarReporteConsolidado();
    }

})();