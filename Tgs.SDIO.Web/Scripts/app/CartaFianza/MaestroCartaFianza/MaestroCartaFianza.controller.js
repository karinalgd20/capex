﻿var vm;
(function () {
    'use strict'
    angular
   .module('app.CartaFianza')
   .controller('MaestroCartaFianzaController', MaestroCartaFianzaController);

    MaestroCartaFianzaController.$inject = [
        'MaestroCartaFianzaService'
        , 'ClienteCartaFianzaService'
        , 'RegistroCartaFianzaService'
        , 'CorreoService'
        , 'MaestraService'
        , 'AccionCartaFianzaService'
        , 'blockUI'
        , 'UtilsFactory'
        , 'DTColumnDefBuilder'
        , 'DTOptionsBuilder'
        , 'DTColumnBuilder'
        , '$timeout'
        , 'MensajesUI'
        , 'URLS'
        , '$scope'
        , '$compile'
        , '$modal'
        , '$injector'
    ];

    function MaestroCartaFianzaController(
        MaestroCartaFianzaService
        , ClienteCartaFianzaService
        , RegistroCartaFianzaService
        , CorreoService
        , MaestraService
        , AccionCartaFianzaService
        , blockUI
        , UtilsFactory
        , DTColumnDefBuilder
        , DTOptionsBuilder
        , DTColumnBuilder
        , $timeout
        , MensajesUI
        , $urls
        , $scope
        , $compile
        , $modal
        , $injector
        ) {

        vm = this;

        vm.BuscarCartaFianza = BuscarCartaFianza;
        vm.CargaModalCartaFianzaRegistro = CargaModalCartaFianzaRegistro;

        vm.CargaModalCartaFianzaId = CargaModalCartaFianzaId;
        vm.CargaModalCartaBuscarCliente = CargaModalCartaBuscarCliente;
        vm.DetalleCartaFianzaAccion = DetalleCartaFianzaAccion;

        vm.LimpiarFiltros = LimpiarFiltros;
        vm.CerrarPopup = CerrarPopup;
        vm.RenovarCartaFianza = RenovarCartaFianza;
        vm.VerificarItemSelecionadosCartaFianza = VerificarItemSelecionadosCartaFianza;

        vm.CambiaIconoEmailGerente = CambiaIconoEmailGerente;
        vm.CambiaIconoIndicador = CambiaIconoIndicador;

        vm.CambiaIconoIndicadorTsr = CambiaIconoIndicadorTsr;
        vm.CambiaIconoEmailTesoreria = CambiaIconoEmailTesoreria;

        vm.ObtenerDatosCorreo = ObtenerDatosCorreo;
        vm.ObtenerDatosCorreoGC = ObtenerDatosCorreoGC;
        vm.SetearDatosCorreo = SetearDatosCorreo;
        vm.ObtenerDatosCorreoCF_Errada = ObtenerDatosCorreoCF_Errada;

        vm.DetalleRequerida = DetalleRequerida;
        vm.RequeridaCartaFianza = RequeridaCartaFianza;

        vm.CargarTipoRequerida = CargarTipoRequerida;
        vm.SelecionarFechaRequerida = SelecionarFechaRequerida;
        vm.SelecionarFechaRequeridaBloqueado = SelecionarFechaRequeridaBloqueado;

        vm.CargarTipoEstadoCartaFianza = CargarTipoEstadoCartaFianza;
        vm.Renovar = Renovar;

        vm.CalculaFechaVencimiento = CalculaFechaVencimiento;

        vm.correo = {};

        vm.ListTipoRequerida = [];
        vm.IdTipoRequeridaTm = -1;

        vm.ListTipoRenovacion = [];
        vm.CmbTipoRenovacionTm = '-1';


        vm.ListaEstadoCartaFianzaTm = [];
        vm.IdEstadoCartaFianzaTm = '-1';
        vm.IdEstadoCartaFianzaTmb = '-1';

        vm.ListTipoValidacionRenovacion = [];
        vm.CmbTipoValidacionRenovacionTm = '-1';

        vm.monthDiff = monthDiff;

        vm.ListaCartaFianza = [];
        vm.IdCartaFianza = 0;
        vm.Descripcion = '';
        vm.IdCliente = 0;
        vm.Id = 0;
        var RegCartaFianza = [];

        vm.selected = {};
        vm.selectAll = false;
        vm.toggleAll = toggleAll;
        vm.toggleOne = toggleOne;

        var RegRenovarCartaFianza = [];
        // vm.vSIdCartaFianza = '';
        vm.color;




        /******************************************* Tabla - Registro de Carta Fianza *******************************************/
        // vm.dataMasiva = [];
        vm.dtInstanceCartaFianza = {};

        vm.NumeroIdentificadorFiscal = '';
        vm.NumeroContrato = '';
        vm.NombreCliente = '';

        var titleHtml = '<input type="checkbox" ng-model="vm.selectAll" ng-click="vm.toggleAll(vm.selected)">';

        vm.dtColumnsCartaFianza = [
      DTColumnBuilder.newColumn(null).withTitle(titleHtml)
               .notSortable().withOption('width', '0.5%')
               .renderWith(function (data, type, full, meta) {
                   let NumeroFila = meta.row;

                   vm.selected[full.IdCartaFianza] = false;
                   if (data.FechaVencimientoBancostr != "") {
                       return '<input type="checkbox"  ng-model="vm.selected[\'' + data.IdCartaFianza + '\']" ng-click="vm.toggleOne(vm.selected)">';
                   } else {
                       if ((existeFecha(data.FechaEnvioTso) != false) && (!data.IdCartaFianzaInicial || data.IdCartaFianzaInicial == null || data.IdCartaFianzaInicial == '')) {
                           return "<center><a> <div class='col-sm-2'> <span id='indicardorTsr" + data.IdCartaFianza + "' ng-click='vm.ObtenerDatosCorreo(" + NumeroFila + ")' title='Solicitud de envio a Tesoreria para emision C.F' ng-mouseover='vm.CambiaIconoEmailTesoreria(" + data.IdCartaFianza + ")'    ng-mouseleave='vm.CambiaIconoIndicadorTsr(" + data.IdCartaFianza + "," + data.IdIndicadorSemaforoColorTpsTesoreria + ")'  style='color:" + (data.FechaSolitudTesoriaStr == '' ? '' : data.IdIndicadorSemaforoColorTpsTesoreria) + ";'    class='" + (data.FechaSolitudTesoriaStr == '' ? '' : 'glyphicon glyphicon-circle-arrow-up') + "' ></span>  <div class='col-sm-2'> " + data.IdIndicadorSemaforoTpsTesoreria + " </div></a></div></center>";
                       } else {
                           return "<center> <a title='Enviar Sol. tso.' ng-click='vm.ObtenerDatosCorreo(" + NumeroFila + ")'><span class='glyphicon fa fa-envelope' style='color: #3949ab; margin-left: 1px;'></span></a>    </center>";
                       }
                   }
               }),

        DTColumnBuilder.newColumn(null).withTitle('Vcto.Banco').notSortable().renderWith(AccionesCartaFianzaIndicardor).withOption('width', '5%'),
        DTColumnBuilder.newColumn('FechaVencimientoBancostr').withTitle('Vcto_Banco').notSortable().withOption('width', '8%'),

        DTColumnBuilder.newColumn('IdCartaFianza').notVisible(),
        DTColumnBuilder.newColumn('IdCliente').notVisible(),
        DTColumnBuilder.newColumn('NumeroIdentificadorFiscal').withTitle('RUC/NIF').withOption('width', '6%').notSortable(),
        DTColumnBuilder.newColumn('NombreCliente').withTitle('Cliente').withOption('width', '15%'),

        DTColumnBuilder.newColumn('NumeroOportunidad').withTitle('Oportunidad').withOption('width', '8%'),
        DTColumnBuilder.newColumn('NumeroGarantia').withTitle('N°.Garantía').withOption('width', '8%'),
        DTColumnBuilder.newColumn('ImporteFianzaStr').withTitle('Importe').withOption('width', '10%'),//.renderWith(render),

        DTColumnBuilder.newColumn('DesTipoContrato').withTitle('Tip.Contrato').withOption('width', '10%').notSortable(),

        DTColumnBuilder.newColumn('DesProcesoTm').withTitle('Proceso').notSortable().withOption('width', '8%'),

        DTColumnBuilder.newColumn('IdEstadoCartaFianzaTm').notVisible(),
        DTColumnBuilder.newColumn('DesEstadoCartaFianzaTm').withTitle('Estado_C.F').notSortable().withOption('width', '4.5%'),

         DTColumnBuilder.newColumn('IdTipoAccionTm').withTitle('IdTipoAccionTm').notVisible(),
        DTColumnBuilder.newColumn('IdRenovarTm').withTitle('IdRenovarTm').notVisible(),
        DTColumnBuilder.newColumn('DesRenovarTm').withTitle('Renovar?').notSortable().withOption('width', '4%'),
         DTColumnBuilder.newColumn('desTipoRequeridaTm').withTitle('Requerida').notSortable().withOption('width', '4%'),

        DTColumnBuilder.newColumn('IdTipoSubEstadoCartaFianzaTm').withTitle('IdTipoSubEstadoCartaFianzaTm').notVisible(),
        DTColumnBuilder.newColumn('DesTipoSubEstadoCartaFianzaTm').withTitle('Sub.Estado_C.F').notSortable().withOption('width', '8%'),

        DTColumnBuilder.newColumn('FechaEmisionBancostr').withTitle('Emision_Banco').notSortable().withOption('width', '8%'),
        DTColumnBuilder.newColumn('FechaVigenciaBancostr').withTitle('Vigencia_Banco').notSortable().withOption('width', '8%'),
   

        DTColumnBuilder.newColumn('FechaRenovacionEjecucionStr').notVisible(),
        DTColumnBuilder.newColumn('FechaDesestimarRequerimientoStr').notVisible(),
        DTColumnBuilder.newColumn('FechaEjecucionStr').notVisible(),
        DTColumnBuilder.newColumn('IdTipoRequeridaTm').notVisible(),
        DTColumnBuilder.newColumn('MotivoCartaFianzaRequerida').notVisible(),

        DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCartaFianza).withOption('width', '5%')
        ];

        //text-align: left;
        function toggleAll(selectedItems) {
            for (var CodigoSolicitud in selectedItems) {
                if (selectedItems.hasOwnProperty(CodigoSolicitud)) {
                    selectedItems[CodigoSolicitud] = vm.selectAll;
                }
            }
        }

        function toggleOne(selectedItems) {
            for (var CodigoSolicitud in selectedItems) {
                if (selectedItems.hasOwnProperty(CodigoSolicitud)) {
                    if (!selectedItems[CodigoSolicitud]) {
                        vm.selectAll = false;
                        return;
                    }
                }
            }
            vm.selectAll = true;
        }

        function AccionesCartaFianzaIndicardor(data, type, full, meta) {
            let NumeroFila = meta.row;
            var respuesta = "<center> "; // ng-click='vm.ObtenerDatosCorreoGC(" + NumeroFila + ")'  title='Enviar Correo G.C' ng-mouseover='vm.CambiaIconoEmailGerente(" + data.IdCartaFianza + ")'  
            respuesta = respuesta + "<div class='col-sm-2'><span id='indicardor" + data.IdCartaFianza + "'  ng-mouseleave='vm.CambiaIconoIndicador(" + data.IdCartaFianza + "," + data.IdIndicadorSemaforoColor + ")'  style='color:" + (data.FechaVencimientoBancostr == '' ? '' : data.IdIndicadorSemaforoColor) + ";'    class='" + (data.FechaVencimientoBancostr == '' ? '' : 'glyphicon glyphicon-circle-arrow-up') + "' ></span></a></div>";
            respuesta = respuesta + "<div class='col-sm-2'> " + (data.FechaVencimientoBancostr == '' ? '' : data.IdIndicadorSemaforo) + " </div> </center>";
            return respuesta;
        };

        function AccionesCartaFianza(data, type, full, meta) {
            let NumeroFila = meta.row;
            var respuesta = "";
            respuesta = respuesta + "<center><div class='form-group'>";
            respuesta = respuesta + "<a title='Editar'   ng-click='vm.CargaModalCartaFianzaId(\"" + data.IdCliente + "\",\"" + data.IdCartaFianza + "\");'>" + "<span class='glyphicon glyphicon-pencil' style='color: #3949ab; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + "<a title='Accion'   ng-click='vm.DetalleCartaFianzaAccion(\"" + data.IdCliente + "\",\"" + data.IdCartaFianza + "\");'>" + "<span class='glyphicon glyphicon-list-alt' style='color: #3949ab; margin-left: 1px;'></span></a>";
            respuesta = respuesta + "<a title='Requerida'   ng-click='vm.DetalleRequerida(" + NumeroFila + ");'>" + "<span class='glyphicon fa fa-retweet' style='color:#3949ab; margin-left: 5px;'></span></a>";

            respuesta = respuesta + "</div></center>";
            return respuesta;
        };

        function RenovarAccionesCartaFianza(data, type, full, meta) {
            var respuesta = "";
            respuesta = respuesta + "<center>";
            respuesta = respuesta + "<input type='checkbox' id='ckRegRenovar[]' ng-model='vm.ckRegRenovar'  title='Renovar' value='' ng-true-value='1' ng-false-value='0'  ng-click='vm.RenovarCartaFianza(\"" + data.IdCliente + "\",\"" + data.IdCartaFianza + "\")'>";
            respuesta = respuesta + "</center>";
            return respuesta;
        };
        /***********************Carga Incial de Carta Fianza*************************/
        function BuscarCartaFianza() {
            blockUI.start();
            vm.selected = {};
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptionsCartaFianza = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withFnServerData(BuscarCartaFianzaPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)

                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarCartaFianzaPaginado(sSource, aoData, fnCallback, oSettings) {

            var draw = aoData[0].value;
            var start = aoData[3].value;
            var size = aoData[4].value;
            var pageNumber = (start + size) / size;
            var cartaFianza = {
                IdCartaFianza: vm.IdCartaFianza,
                NumeroContrato: vm.NumeroContrato,
                NombreCliente: vm.NombreCliente,
                NumeroIdentificadorFiscal: vm.NumeroIdentificador,

                NumeroGarantia: vm.NumeroGarantia,
                ImporteCartaFianzaSoles: vm.ImporteCartaFianzaSoles,
                FechaVencimientoBanco: vm.FechaVencimiento,
                IdEstadoCartaFianzaTm: vm.IdEstadoCartaFianzaTm,
                IdTipoRequeridaTm: vm.IdEstadoCartaFianzaTmb,

                SortName: aoData[1].value[oSettings.aLastSort[0].col].data,
                sort: oSettings.aaSorting[0]._idx,
                Indice: pageNumber,
                Tamanio: size
            };

            vm.selected = {};

            var promise = MaestroCartaFianzaService.ListarCartaFianza(cartaFianza);
            promise.then(function (response) {
                //  debugger;
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListCartaFianza
                };
                blockUI.stop();
                fnCallback(records);
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsCartaFianza = DTOptionsBuilder
            .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('scrollCollapse', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('scrollX', true)
                .withOption('paging', false);

        };

        /***********************Modal de Registro de Carta Fianza*************************/
        function CargaModalCartaFianzaRegistro() {
            if (vm.IdCliente != 0) {
                vm.IdCartaFianza = 0;
                vm.IdCliente = vm.IdCliente;
                var dto = {
                    IdCartaFianza: 0,
                    IdCliente: vm.IdCliente
                };

                var promise = RegistroCartaFianzaService.ModalCartaFianzaRegistrar(dto);
                promise.then(function (response) {
                    var respuesta = $(response.data);

                    $injector.invoke(function ($compile) {
                        var div = $compile(respuesta);
                        var content = div($scope);
                        $("#ContenidoCartaFianza").html(content);
                    });

                    $('#ModalCartaFianza').modal({
                        keyboard: false
                    });

                }, function (response) {
                    blockUI.stop();
                });

            } else {
                UtilsFactory.Alerta('#divAlertMaestro', 'danger', 'Selecionar un Cliente', 20);
            }
        };

        /***********************Modal para Modificar Carta Fianza*************************/
        function CargaModalCartaFianzaId(IdCliente, IdCartaFianza) {
            //debugger;
            vm.IdCartaFianza = IdCartaFianza;
            vm.IdCliente = IdCliente;
            RegCartaFianza = {
                IdCartaFianza: IdCartaFianza,
                IdCliente: IdCliente
            };
            var promise = RegistroCartaFianzaService.ModalCartaFianzaRegistrar(RegCartaFianza);

            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCartaFianza").html(content);
                });

                $('#ModalCartaFianza').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        /***********************Modal de Buscar Cliente*************************/
        function CargaModalCartaBuscarCliente() {
            vm.IdCliente = 0;
            var RegCartaFianza = {
                IdCartaFianza: 0,
                IdCliente: 0
            };

            var promise = ClienteCartaFianzaService.ModalCartaFianzaBuscarCliente(RegCartaFianza);

            promise.then(function (response) {

                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCartaFianzaCliente").html(content);
                });

                $('#ModalCartaFianzaCliente').modal({
                    keyboard: false,
                    backdrop: 'static'

                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CerrarPopup() {
          
            $('#ModalCartaFianza').modal('hide');
            $('#ModalEnviarCorreoCartaFianza').modal('hide');
        };

        function LimpiarFiltros() {
            vm.NumeroIdentificador = ''
            vm.NumeroIdentificadorFiscal = '';
            vm.NumeroContrato = '';
            vm.NombreCliente = '';
            vm.IdCliente = 0;

            vm.FechaVencimiento = null;
            vm.ImporteCartaFianzaSoles = null;
            vm.NumeroGarantia = null;
        };

        /***********************Modal de Acciones*************************/
        function DetalleCartaFianzaAccion(IdCliente, IdCartaFianza) {
            vm.IdCartaFianza = IdCartaFianza;
            vm.IdCliente = IdCliente;
            var RegCartaFianzaAccion = {
                IdCartaFianza: IdCartaFianza,
                IdCliente: IdCliente
            };

            var promise = AccionCartaFianzaService.ModalCartaFianzaAccion(RegCartaFianzaAccion);

            promise.then(function (response) {

                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCartaFianzaAcciones").html(content);
                });

                $('#ModalCartaFianzaAccion').modal({
                    keyboard: false,
                    backdrop: 'static'

                });
            }, function (response) {
                blockUI.stop();
            });

        }

        function VerificarItemSelecionadosCartaFianza() {
            vm.vSIdCartaFianza = '';
            vm.Id = -1;
            for (var CodigoSolicitud in vm.selected) {
                if (vm.selected.hasOwnProperty(CodigoSolicitud)) {
                    if (vm.selected[CodigoSolicitud]) {
                        vm.vSIdCartaFianza = vm.vSIdCartaFianza + "" + CodigoSolicitud + ",";
                    }
                }
            }
            if (vm.vSIdCartaFianza != "") {
                console.log(vm.vSIdCartaFianza);
                $('#ModalRenovacion').modal({
                    keyboard: false
                });

                if (vm.Id == 0) {

                    BuscarCartaFianza();
                }
            }else {
                   UtilsFactory.Alerta('#divAlertMaestro', 'danger', 'Selecione item de la Lista de Fianzas', 5);
        
            }
          

        }

        function RenovarCartaFianza() {

            var vIdTipoAccionTm = 0;
            vm.vSIdCartaFianza = $('#vSIdCartaFianza').val();

            if (vm.CmbTipoRenovacionTm == "-1" || vm.CmbTipoRenovacionTm == "0") {
                UtilsFactory.Alerta('#divAlertRegistrarRenovacion', 'danger', 'Seleccione el Tipo de Renovación', 5);
            }
            else if (vm.vSIdCartaFianza != '' || (vm.vSIdCartaFianza).length > 2) {
                blockUI.start();
                if (vm.CmbTipoRenovacionTm == 1 || vm.CmbTipoRenovacionTm == 2) {
                    var vIdTipoAccionTm = 4;
                } else if (vm.CmbTipoRenovacionTm == 5) {
                    var vIdTipoAccionTm = 3;
                    vm.CmbTipoRenovacionTm = 4;
                }

                var maestra = {
                    IdCartaFianzastr: vm.vSIdCartaFianza,
                    IdTipoAccionTm: vIdTipoAccionTm,
                    IdRenovarTm: vm.CmbTipoRenovacionTm,

                    FechaRespuestaFinalGcSm: (vm.FechaRespuestaFinalGcSm == undefined) ? '01/01/1753' : vm.FechaRespuestaFinalGcSm,
                    FechaVencimientoRenovacion: (vm.FechaVencimientoRenovacion == undefined) ? '01/01/1753' : vm.FechaVencimientoRenovacion,
                    ValidacionContratoGcSm: (vm.ValidacionContratoGcSm == undefined) ? '-' : vm.ValidacionContratoGcSm,
                    SustentoRenovacion: vm.SustentoRenovacion == undefined ? '-' : vm.SustentoRenovacion,
                    Observacion: vm.Observacion == undefined ? '-' : vm.Observacion,
                    PeriodoMesRenovacion: vm.PeriodoMesRenovacion == undefined ? 0 : vm.PeriodoMesRenovacion
                };

                var promise = MaestroCartaFianzaService.ActualizaARenovacionCartaFianza(maestra);

                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        vm.Id = Respuesta.TipoRespuesta
                        UtilsFactory.Alerta('#divAlertRegistrarRenovacion', 'danger', Respuesta.Mensaje, 20);
                    } else {
                        vm.Id = Respuesta.TipoRespuesta;
                        UtilsFactory.Alerta('#divAlertRegistrarRenovacion', 'success', Respuesta.Mensaje, 5);
                        $('#ModalRenovacion').modal('hide');

                        BuscarCartaFianza();
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertRegistrarRenovacion', 'danger', MensajesUI.DatosError, 5);
                });

            } else {

                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRegistrarRenovacion', 'danger', 'debe selecionar al menos 1 item', 5);
            }

        };

        function Renovar() {

            if (vm.CmbTipoRenovacionTm == "1") {

                $('#RenovarSi').css('visibility', 'hidden');
                $('#RenovarSi').css('display', 'none');
                //$('FechaVencimientoRenovacion').attr('visibility', 'disabled');
                //$('FechaVencimientoRenovacion').attr('display', 'none');


            } else if (vm.CmbTipoRenovacionTm == "5") {

                $('#RenovarSi').css("visibility", '');
                $('#RenovarSi').css("display", '');
                //$('FechaVencimientoRenovacion').prop("visibility", 'hidden');
                //$('FechaVencimientoRenovacion').prop("display", 'none');
            }
        }

        function monthDiff(d2) {
            var d1 = new Date();
            var months;
            if (d2 != null) {
                months = (d2.getFullYear() - d1.getFullYear()) * 12; months -= d1.getMonth() + 1; months += d2.getMonth();
                if (d2.getDate() >= d1.getDate())
                    months++;
                vm.PeriodoMesRenovacion = (months <= 0 ? 0 : months);
            }



        }

        function CargarTipoRenovacion() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 218
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoRenovacion = [
                     { Codigo: "-1", Descripcion: "Selecione" },
                    { Codigo: "1", Descripcion: "No" },
                { Codigo: "5", Descripcion: "Si" }
                ]; //UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarTipoValidacionRenovacion() {

            /* var maestra = {
                 IdEstado: 1,
                 IdRelacion: 218
             };
             var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
 
             promise.then(function (resultado) {
                 blockUI.stop();
 
                 var Respuesta = resultado.data;*/
            vm.ListTipoValidacionRenovacion = [
                 { Codigo: "-1", Descripcion: "Selecione" },
                { Codigo: "1", Descripcion: "Validación C.G y S.M" },
            { Codigo: "2", Descripcion: "Validación de Contrato" },
             { Codigo: "3", Descripcion: "A sol entidad" }
            ]; //UtilsFactory.AgregarItemSelect(Respuesta);

            /*}, function (response) {
                blockUI.stop();

            });*/
        };

        function CargarTipoRequerida() {

           var maestra = {
               IdEstado: 1,
               IdRelacion: 390
           };
           var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

           promise.then(function (resultado) {
               blockUI.stop();

               var Respuesta = resultado.data;
               vm.ListTipoRequerida = UtilsFactory.AgregarItemSelect(Respuesta);

           }, function (response) {
               blockUI.stop();

           });
       };

        function CargarTipoEstadoCartaFianza() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 212
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaEstadoCartaFianzaTm = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        /**********************Modal Envio de Correo a Tesoreria***************************/
        function ObtenerDatosCorreoCF_Errada(cartaFianza) {
           
           var promise = CorreoService.ObtenerDatosCorreo(cartaFianza);
            vm.SetearDatosCorreo(promise, cartaFianza);

        }



        function ObtenerDatosCorreo(NumeroFila) {
            let cartaFianza = vm.dtInstanceCartaFianza.dataTable.fnGetData(NumeroFila);

            if (cartaFianza.IdTipoSubEstadoCartaFianzaTm == -1 || cartaFianza.IdTipoSubEstadoCartaFianzaTm == 0 || cartaFianza.IdTipoSubEstadoCartaFianzaTm == null) { cartaFianza.IdTipoSubEstadoCartaFianzaTm = 4;/*Seguimiento Tso:Solicitud Tso*/ }
            if (cartaFianza.IdTipoSubEstadoCartaFianzaTm == 4 && cartaFianza.IdEstado == 1) { cartaFianza.IdTipoSubEstadoCartaFianzaTm = 5;/*Seguimiento Tso:Solicitud Tso Errada*/ }

            var promise = CorreoService.ObtenerDatosCorreo(cartaFianza);
            vm.SetearDatosCorreo(promise, cartaFianza);

        }

        function ObtenerDatosCorreoGC(NumeroFila) {
            let cartaFianza = vm.dtInstanceCartaFianza.dataTable.fnGetData(NumeroFila);
            //cartaFianza.IdTipoAccionTm = 2;/*emision*/
            if (cartaFianza.IdTipoSubEstadoCartaFianzaTm == 2) { cartaFianza.IdTipoSubEstadoCartaFianzaTm = 3;/*Seguimiento GC:Escalado*/ }

            if (cartaFianza.IdTipoSubEstadoCartaFianzaTm == 1) { cartaFianza.IdTipoSubEstadoCartaFianzaTm = 2;/*Seguimiento GC:Reiterativo*/ }

            if (cartaFianza.IdTipoSubEstadoCartaFianzaTm == 8 || cartaFianza.IdTipoSubEstadoCartaFianzaTm == 9) { cartaFianza.IdTipoSubEstadoCartaFianzaTm = 1;/*Seguimiento GC:Seguimiento*/ }
          
            var promise = CorreoService.ObtenerDatosCorreoGC(cartaFianza);
            vm.SetearDatosCorreo(promise, cartaFianza);

        }

        function SetearDatosCorreo(promise, cartaFianza) {
            promise.then(
                //Success:
                function (response) {
                    var respuesta = $(response.data);

                    $injector.invoke(function ($compile) {
                        var div = $compile(respuesta);
                        var content = div($scope);
                        $("#ContenidoCorreo").html(content);

                        let respuestaCorreo = window.correo;
                        let controladorHijo = $scope.$$childTail.vm;

                        controladorHijo.Init();

                        controladorHijo.correo.IdCartaFianza = cartaFianza.IdCartaFianza;

                        controladorHijo.correo.IdTipoSubEstadoCartaFianzaTm = cartaFianza.IdTipoSubEstadoCartaFianzaTm;
                        controladorHijo.correo.IdRenovarTm = cartaFianza.IdRenovarTm;
                        controladorHijo.correo.IdEstadoCartaFianzaTm = cartaFianza.IdEstadoCartaFianzaTm;
                        controladorHijo.correo.IdTipoAccionTm = cartaFianza.IdTipoAccionTm;
                        controladorHijo.correo.IdColaborador = respuestaCorreo.IdUsuarioCreacion;
                        controladorHijo.correo.IdUsuarioCreacion = respuestaCorreo.IdUsuarioCreacion;
                        controladorHijo.correo.UsuarioEdicion = respuestaCorreo.UsuarioEdicion;

                        controladorHijo.correo.From = respuestaCorreo.From;
                        controladorHijo.correo.To = respuestaCorreo.EmailColaborador;
                        controladorHijo.correo.Copy = respuestaCorreo.From;
                        controladorHijo.correo.Subject = respuestaCorreo.Subject;
                        setTimeout(() => {
                            (respuestaCorreo.Body != null && respuestaCorreo.Body != "") ? tinymce.get('Body').setContent(respuestaCorreo.Body) : tinymce.get('Body').setContent('');
                            $('#ModalEnviarCorreoCartaFianza').modal();
                        },100);
                    });
                },
                //Error:
                function (response) {
                    blockUI.stop();
                });
        }

      
        /*************************Modal de Requerida**********************************/

        $('#ModalEnviarCorreoCartaFianza').on('hidden.bs.modal', function () {
            tinymce.remove('textarea.texto-enriquecido');
        })

        /*************************Modal de Requerida**********************************/
        function DetalleRequerida(numerofila) {
            vm.IdTipoRequeridaTm = -1;
            vm.FechaRespuestaFinalGcSm = null;
            vm.FechaEjecucion = null;
            vm.FechaDesestimarRequerimiento = null;
            vm.MotivoCartaFianzaRequerida = null;

            let that = this;
            let cartaFianza = that.dtInstanceCartaFianza.dataTable.fnGetData(numerofila);
            // RequeridaCartaFianza(cartaFianzaDetalleRenovacion)

            vm.IdTipoRequeridaTm = cartaFianza.IdTipoRequeridaTm;

            var vFechaRenovacionEjecucionStr = cartaFianza.FechaRenovacionEjecucionStr;
            var vFechaRenovacionEjecucionStr = vFechaRenovacionEjecucionStr.substring(3, 5) + '-' + vFechaRenovacionEjecucionStr.substring(0, 2) + '-' + vFechaRenovacionEjecucionStr.substring(6, 10);
            vm.FechaRenovacionEjecucion = new Date(vFechaRenovacionEjecucionStr);



            var vFechaEjecucionStr = cartaFianza.FechaEjecucionStr;
            var vFechaEjecucionStr = vFechaEjecucionStr.substring(3, 5) + '-' + vFechaEjecucionStr.substring(0, 2) + '-' + vFechaEjecucionStr.substring(6, 10);
            vm.FechaEjecucion = new Date(vFechaEjecucionStr);


            var vFechaDesestimarRequerimientoStr = cartaFianza.FechaDesestimarRequerimientoStr;
            var vFechaDesestimarRequerimientoStr = vFechaDesestimarRequerimientoStr.substring(3, 5) + '-' + vFechaDesestimarRequerimientoStr.substring(0, 2) + '-' + vFechaDesestimarRequerimientoStr.substring(6, 10);
            vm.FechaDesestimarRequerimiento = new Date(vFechaDesestimarRequerimientoStr);


            vm.MotivoCartaFianzaRequerida = cartaFianza.MotivoCartaFianzaRequerida;


            if (vm.IdTipoRequeridaTm == null) { vm.IdTipoRequeridaTm = -1; }

            vm.IdCartaFianza = cartaFianza.IdCartaFianza;
            vm.numerofila = numerofila;
            $('#ModalRequerida').modal({
                keyboard: false
            });
            SelecionarFechaRequerida();

        }

        function RequeridaCartaFianza(NumeroFila) {
            let that = this;
            let cartaFianza = that.dtInstanceCartaFianza.dataTable.fnGetData(vm.numerofila);

            //|| ( existeFecha(vm.FechaDesestimarRequerimiento) == false && existeFecha(vm.FechaEjecucion)  == false) 
            if (vm.IdTipoRequeridaTm == 3 && existeFecha(vm.FechaRenovacionEjecucion) == false) {
                UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'danger', 'ingrese Fecha de Requerida (Renovación ó Ejecución)', 5);
            }
            else if (vm.IdTipoRequeridaTm == 2 && existeFecha(vm.FechaDesestimarRequerimiento) == false) {
                UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'danger', 'ingrese Fecha de Desestimar Requerimiento', 5);
            }
            else if (vm.IdTipoRequeridaTm == 1 && existeFecha(vm.FechaEjecucion) == false) {
                UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'danger', 'ingrese Fecha de Ejecución', 5);
            }
            else if ((vm.IdTipoRequeridaTm != -1) && (vm.MotivoCartaFianzaRequerida == '' || vm.MotivoCartaFianzaRequerida == null || $('#MotivoCartaFianzaRequerida').val().length == 0)) {
                UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'danger', 'Ingrese alguna Observación', 5);
            }
            else {
                /*
                    vIdEstadoCartaFianzaTm
                    1	Descargada
                    2	Ejecutada
                    3	Recuperada
                    4	Vencida
                    5	Vigente
                vIdRenovarTm
                1	No
                2	No Aplica
                3	Por Atender
                4	Seguimiento
                5	Si
                6	Otros
                    1	Requerida:Ejecutada
                    2	Requerida:Desestimada
                    3	Requerida:Renovada
                */
                var vIdRenovarTm = 0;
                var vIdEstadoCartaFianzaTm = 0;
                var vIdTipoAccionTm = 0;
                //if (existeFecha(vm.FechaDesestimarRequerimiento) != false) { vIdEstadoCartaFianzaTm = 3; }
                //if (existeFecha(vm.FechaRenovacionEjecucion) != false) { vIdEstadoCartaFianzaTm = 2; }

                if (vm.IdTipoRequeridaTm == 1) {//Ejecutada	1
                    vIdEstadoCartaFianzaTm = 2; // ejecutada 2.
                    vIdRenovarTm = 4;       // 2	No Aplica
                    vIdTipoAccionTm = 1;
                } else if (vm.IdTipoRequeridaTm == 2) {// Desestimada	2                    
                    vIdEstadoCartaFianzaTm = 5;
                    vIdRenovarTm = 4;
                    vIdTipoAccionTm = 1;
                } else if (vm.IdTipoRequeridaTm == 3) { // Renovada	3
                    vIdRenovarTm = 5; // 5	Si
                    vIdEstadoCartaFianzaTm = 5;  // 5	Vigente
                    vIdTipoAccionTm = 3;    //renovacion
                }

                var maestra = {
                    IdCartaFianza: vm.IdCartaFianza,
                    //IdTipoAccionTm: vIdTipoAccionTm,
                    //IdRenovarTm: vIdRenovarTm,
                    //IdEstadoCartaFianzaTm: vIdEstadoCartaFianzaTm,
                    IdTipoRequeridaTm: vm.IdTipoRequeridaTm,
                    FechaRenovacionEjecucion: vm.FechaRenovacionEjecucion,
                    FechaDesestimarRequerimiento: vm.FechaDesestimarRequerimiento,
                    FechaEjecucion: vm.FechaEjecucion,
                    MotivoCartaFianzaRequerida: vm.MotivoCartaFianzaRequerida

                };
                blockUI.start();
                var promise = MaestroCartaFianzaService.ActualizarCartaFianzaRequerida(maestra);

                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        vm.Id = Respuesta.TipoRespuesta
                        UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'danger', Respuesta.Mensaje, 20);
                    } else {
                         $('#ModalCartaFianza').scrollTop(0);

                        vm.Id = Respuesta.TipoRespuesta;
                        UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'success', Respuesta.Mensaje, 5);
                       
                        setTimeout(() => {  $('#ModalRequerida').modal('hide'); $("#" + divAlerta).empty(); }, 2000);
                        BuscarCartaFianza();
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertRegistrarRequerida', 'danger', MensajesUI.DatosError, 5);
                });

            }

        };

        /********************** Tesoreria cambio de boton de imagen indicado a mail***************************/
       
        function CambiaIconoEmailGerente(object) {
            if ($('#indicardor' + object + '').hasClass('glyphicon glyphicon-circle-arrow-up')) {

                $('#indicardor' + object + '').removeClass('glyphicon glyphicon-circle-arrow-up');
                $('#indicardor' + object + '').addClass('glyphicon fa fa-envelope');
                vm.color = $('#indicardor' + object + '').css('color');
                $('#indicardor' + object + '').css('color', '#3949ab');
            }

        }

        function CambiaIconoIndicador(object, color) {
            if ($('#indicardor' + object + '').hasClass('glyphicon fa fa-envelope')) {

                $('#indicardor' + object + '').removeClass('glyphicon fa fa-envelope');
                $('#indicardor' + object + '').addClass('glyphicon glyphicon-circle-arrow-up');
                $('#indicardor' + object + '').css('color', vm.color);

            }

        }

        function CambiaIconoEmailTesoreria(object) {
            if ($('#indicardorTsr' + object + '').hasClass('glyphicon glyphicon-circle-arrow-up')) {

                $('#indicardorTsr' + object + '').removeClass('glyphicon glyphicon-circle-arrow-up');
                $('#indicardorTsr' + object + '').addClass('glyphicon fa fa-envelope');
                vm.color = $('#indicardorTsr' + object + '').css('color');
                $('#indicardorTsr' + object + '').css('color', '#3949ab');
            }
            //CambiaIconoIndicador(object, color);
        }

        function CambiaIconoIndicadorTsr(object, color) {

            if ($('#indicardorTsr' + object + '').hasClass('glyphicon fa fa-envelope')) {

                $('#indicardorTsr' + object + '').removeClass('glyphicon fa fa-envelope');
                $('#indicardorTsr' + object + '').addClass('glyphicon glyphicon-circle-arrow-up');
                $('#indicardorTsr' + object + '').css('color', vm.color);
            }

        }

        function existeFecha(fecha) {

            if (fecha == 'Invalid Date' || fecha == undefined || fecha == null) {
                return false;
            }
            return true;
        }
	
		function CalculaFechaVencimiento(periodo){
			var fecha = new Date();		
			var fechan = new Date(fecha.setMonth((fecha.getMonth()+1) + periodo));
 			var mesn =((fechan.getMonth() == 0 )? 1: (fechan.getMonth()+1));
			var nFecha = formatted_string('00' ,mesn,0)+'-'+fechan.getDate() +'-'+ fechan.getFullYear();

			vm.FechaVencimientoRenovacion = new Date ( nFecha );
		
		}

		function formatted_string(pad, user_str, pad_pos) {
		    if (typeof user_str === 'undefined')
		        return pad;
		    if (pad_pos == 0 ) {
		        return (pad + user_str).slice(-pad.length);
		    }
		    else {
		        return (user_str + pad).substring(0, pad.length);
		    }
		}
		
        /***********************Requerida*************************/

        function SelecionarFechaRequerida() {

            if (vm.IdTipoRequeridaTm == 1) {
                document.getElementById("FechaDesestimarRequerimiento").disabled = true;
                document.getElementById("FechaRenovacionEjecucion").disabled = true;
                document.getElementById("FechaEjecucion").disabled = false;

            } else if (vm.IdTipoRequeridaTm == 2) {
                document.getElementById("FechaDesestimarRequerimiento").disabled = false;
                document.getElementById("FechaRenovacionEjecucion").disabled = true;
                document.getElementById("FechaEjecucion").disabled = true;
               // vm.FechaEjecucion = null;

            } else if (vm.IdTipoRequeridaTm == 3) {
                document.getElementById("FechaDesestimarRequerimiento").disabled = true;
                document.getElementById("FechaRenovacionEjecucion").disabled = false;
                document.getElementById("FechaEjecucion").disabled = true;

            } else {
                document.getElementById("FechaDesestimarRequerimiento").disabled = true;
                document.getElementById("FechaRenovacionEjecucion").disabled = true;
                document.getElementById("FechaEjecucion").disabled = true;
            }
            
        }


        function SelecionarFechaRequeridaBloqueado() {

            document.getElementById("FechaDesestimarRequerimiento").disabled = true;
            document.getElementById("FechaRenovacionEjecucion").disabled = true;
            document.getElementById("FechaEjecucion").disabled = true;

        }

        /******************************************* Load *******************************************/

        //vm.FechaDesestimarRequerimiento = ;

        CargarTipoEstadoCartaFianza();

        SelecionarFechaRequeridaBloqueado();
        BuscarCartaFianza();
        CargarTipoRenovacion();
        CargarTipoRequerida();
        CargarTipoValidacionRenovacion();


    }

})();