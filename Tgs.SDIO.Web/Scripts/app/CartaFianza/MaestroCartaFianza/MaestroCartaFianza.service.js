﻿(function () {
    'use strict',
     angular
    .module('app.CartaFianza')
    .service('MaestroCartaFianzaService', MaestroCartaFianzaService);

    MaestroCartaFianzaService.$inject = ['$http', 'URLS'];

    function MaestroCartaFianzaService($http, $urls) {

        var service = {
            ListarCartaFianza: ListarCartaFianza,
            //RegistrarCartaFianza: RegistrarCartaFianza,
            ActualizarCartaFianzaRequerida: ActualizarCartaFianzaRequerida,
           
            ObtenerDatosCorreo: ObtenerDatosCorreo,
            EnviarCorreoCartaFianza: EnviarCorreoCartaFianza,
            ListarColaborador: ListarColaborador,
            ListaCartaFianzaCombo: ListaCartaFianzaCombo,
            CartasFianzasSinRespuesta: CartasFianzasSinRespuesta,
            GuardarRespuestaGerenteCuenta: GuardarRespuestaGerenteCuenta,
            GuardarConfirmacionGerenteCuenta: GuardarConfirmacionGerenteCuenta
        };

        return service;

        function ListarCartaFianza(cartaFianza) {

            return $http({
                url: $urls.ApiCartaFianza + "MaestroCartaFianza/ListarCartaFianza",
                method: "POST",
                data: JSON.stringify(cartaFianza)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;

            }
        };

        function ObtenerDatosCorreo(cartaFianza) {

            return $http({
                url: $urls.ApiCartaFianza + "MaestroCartaFianza/ObtenerDatosCorreo",
                method: "POST",
                data: JSON.stringify(cartaFianza)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        


        function EnviarCorreoCartaFianza(datosCorreo) {

            return $http({
                url: $urls.ApiCartaFianza + "MaestroCartaFianza/EnviarCorreoCartaFianza",
                method: "POST",
                data: JSON.stringify(datosCorreo)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };



         function ActualizarCartaFianzaRequerida(cartaFianza) {

            return $http({
                url: $urls.ApiCartaFianza + "MaestroCartaFianza/ActualizarCartaFianzaRequerida",
                method: "POST",
                data: JSON.stringify(cartaFianza)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
         };

         function ListarColaborador(cartaFianzaRecupero) {

             return $http({
                 url: $urls.ApiCartaFianza + "MaestroCartaFianza/ListarColaborador",
                 method: "POST",
                 data: JSON.stringify(cartaFianzaRecupero)
             }).then(DatosCompletados);

             function DatosCompletados(resultado) {
                 return resultado;
             }
         };

         function ListaCartaFianzaCombo(cartaFianzaRecupero) {

             return $http({
                 url: $urls.ApiCartaFianza + "MaestroCartaFianza/ListaCartaFianzaCombo",
                 method: "POST",
                 data: JSON.stringify(cartaFianzaRecupero)
             }).then(DatosCompletados);

             function DatosCompletados(resultado) {
                 return resultado;
             }
         };

         function CartasFianzasSinRespuesta(cartaFianza) {

             return $http({
                 url: $urls.ApiCartaFianza + "MaestroCartaFianza/CartasFianzasSinRespuesta",
                 method: "POST",
                 data: JSON.stringify(cartaFianza)
             }).then(DatosCompletados);

             function DatosCompletados(resultado) {
                 return resultado;
             }
         };

         function GuardarRespuestaGerenteCuenta(cartaFianzaRenovacion) {

             return $http({
                 url: $urls.ApiCartaFianza + "MaestroCartaFianza/GuardarRespuestaGerenteCuenta",
                 method: "POST",
                 data: JSON.stringify(cartaFianzaRenovacion)
             }).then(DatosCompletados);

             function DatosCompletados(resultado) {
                 return resultado;
             }
         };

         function GuardarConfirmacionGerenteCuenta(cartaFianzaRenovacion) {

             return $http({
                 url: $urls.ApiCartaFianza + "MaestroCartaFianza/GuardarConfirmacionGerenteCuenta",
                 method: "POST",
                 data: JSON.stringify(cartaFianzaRenovacion)
             }).then(DatosCompletados);

             function DatosCompletados(resultado) {
                 return resultado;
             }
         };

    }

})();