﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('AsignarColaboradorCartaFianzaController', AsignarColaboradorCartaFianzaController);

    AsignarColaboradorCartaFianzaController.$inject = [
            'AsignarColaboradorCartaFianzaService',
            'MaestroCartaFianzaService',
            'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];

    function AsignarColaboradorCartaFianzaController(
            AsignarColaboradorCartaFianzaService,
            MaestroCartaFianzaService,
            MaestraService,
            blockUI,
            UtilsFactory,
            DTColumnDefBuilder,
            DTOptionsBuilder,
            DTColumnBuilder,
            $timeout,
            MensajesUI,
            $urls,
            $scope,
            $compile,
            $modal,
            $injector) {

        vm = this;

        vm.CargarSupervisor = CargarSupervisor;
        vm.CargarGerenteCuenta = CargarGerenteCuenta;
        vm.SupervisorChange = SupervisorChange;
        vm.GerenteCuentaChange = GerenteCuentaChange;
        vm.CargarCartaFianza = CargarCartaFianza;
        vm.CargarTesorera = CargarTesorera;

        vm.LimpiarFormularioSup = LimpiarFormularioSup;
        vm.LimpiarFormularioGer = LimpiarFormularioGer;

        vm.GuardarSupervisorAGerenteCuenta = GuardarSupervisorAGerenteCuenta;
        vm.GuardarGerenteCuentaACartaFianza = GuardarGerenteCuentaACartaFianza;
        vm.ValidarSupervisorAGerenteCuenta = ValidarSupervisorAGerenteCuenta;
        vm.ValidarGerenteCuentaACartaFianza = ValidarGerenteCuentaACartaFianza;
        vm.ActualizarTesorera = ActualizarTesorera;
        vm.ValidarTesorera = ValidarTesorera;

        vm.CargarTablaSupervisorAGerenteCuenta = CargarTablaSupervisorAGerenteCuenta;
        vm.CargarTablaGerenteCuentaACartaFianza = CargarTablaGerenteCuentaACartaFianza;

        vm.supervisor = {};
        vm.gercuenta = {};
        vm.tesorera = {};
        vm.GerentesCuenta = {};
        vm.CartaFianza = {};

        var tablaSupervisorAGerenteCuenta = $("#tablaSupervisorAGerenteCuenta").DataTable({
            pageLength: 5,
            lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
            columnDefs: [{ "width": "5%", "targets": 0 }, { "width": "35%", "targets": 1 }]
        });

        var tablaGerenteCuentaACartaFianza = $("#tablaGerenteCuentaACartaFianza").DataTable({
            pageLength: 5,
            lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
            columnDefs: [{ "width": "5%", "targets": 0 }, { "width": "35%", "targets": 1 }]
        });


        /***************** Cargar combos ***************/
        function CargarSupervisor() {
            blockUI.start();

            var maestra = {
                //IdCargo -> Recurso
                IdTipoColaboradorFianzaTm: 41
            };

            var promise = MaestroCartaFianzaService.ListarColaborador(maestra);

            promise.then(
                function (response) {
                    blockUI.stop();

                    var Respuesta = response.data;

                    $("#IdSupervisorSup").select2({
                        data: Respuesta,
                        placeholder: '--Seleccione--',
                        allowClear: true
                    });
                    $("#IdSupervisorSup").val(-1).change();

                },
                function (response) {
                    blockUI.stop();
                });
        };

        function CargarGerenteCuenta(selector, IdSupervisor, IdCargo) {
            blockUI.start();
            IdCargo = (IdCargo) ? IdCargo : 3; //Valor por defecto 3->Gerente de Cuenta

            var maestra = {
                //IdCargo -> Recurso
                IdTipoColaboradorFianzaTm: IdCargo,
                IdSupervisorColaborador: IdSupervisor
            };

            var promise = MaestroCartaFianzaService.ListarColaborador(maestra);

            promise.then(
                function (response) {
                    blockUI.stop();

                    var Respuesta = response.data;
                    if (IdSupervisor == -1) {
                        vm.GerentesCuenta[selector] = Respuesta;
                    }

                    $("#" + selector).select2({
                        data: Respuesta,
                        placeholder: '--Seleccione--',
                        allowClear: true
                    });

                    if (Respuesta.length > 0) {
                        let idsValidos = Respuesta.filter((datos) => {
                            return datos.IdSupervisorColaborador == IdSupervisor;
                        });

                        let idsRespuestas = idsValidos.map((datos) => {
                            return datos.id;
                        });
                        idsRespuestas = (IdSupervisor == -1 || IdSupervisor == null) ? IdSupervisor : idsRespuestas;
                        $("#" + selector).val(idsRespuestas).change();
                    } else {
                        $("#" + selector).val(-1).change();
                    }
                },
                function (response) {
                    blockUI.stop();
                });
        };

        function CargarCartaFianza(selector, IdColaborador) {
            blockUI.start();

            var maestra = {
                IdColaborador: IdColaborador
            };

            var promise = MaestroCartaFianzaService.ListaCartaFianzaCombo(maestra);

            promise.then(
                function (response) {
                    blockUI.stop();

                    var Respuesta = response.data;
                    if (IdColaborador == -1) {
                        vm.CartaFianza[selector] = Respuesta;
                    }

                    $("#" + selector).select2({
                        data: Respuesta,
                        placeholder: '--Seleccione--',
                        allowClear: true
                    });

                    if (Respuesta.length > 0) {
                        let idsValidos = Respuesta.filter((datos) => {
                            return datos.IdColaborador == IdColaborador;
                        });

                        let idsRespuestas = idsValidos.map((datos) => {
                            return datos.id;
                        });
                        idsRespuestas = (IdColaborador == -1 || IdColaborador == null) ? IdColaborador : idsRespuestas;
                        $("#" + selector).val(idsRespuestas).change();
                    } else {
                        $("#" + selector).val(-1).change();
                    }
                },
                function (response) {
                    blockUI.stop();
                });
        };

        function CargarTesorera() {
            blockUI.start();

            var maestra = {
                //IdCargo -> Recurso
                IdTipoColaboradorFianzaTm: 24
            };

            var promise = MaestroCartaFianzaService.ListarColaborador(maestra);

            promise.then(
                function (response) {
                    blockUI.stop();

                    var Respuesta = response.data;

                    $("#IdTesorera").select2({
                        data: Respuesta,
                        placeholder: '--Seleccione--',
                        allowClear: true
                    });

                    let tesoreraActual = Respuesta.filter((tesorera) => {
                        return tesorera.IdEstado == 1;
                    })[0];

                    $("#IdTesorera").val(tesoreraActual.id).change();

                },
                function (response) {
                    blockUI.stop();
                });
        };

        function LimpiarFormularioSup() {
            $("#IdGerenteCuentaSup").val(-1).change();
            $("#IdSupervisorSup").val(-1).change();
        }

        function LimpiarFormularioGer() {
            $("#IdClienteGer").val(-1).change();
            $("#IdServicesManagerGer").val(-1).change();
            $("#IdGerenteCuentaGer").val(-1).change();
        }

        /***************** Eventos de cambio ***************/
        function SupervisorChange() {
            $('#IdGerenteCuentaSup').empty();
            vm.CargarGerenteCuenta('IdGerenteCuentaSup', vm.supervisor.IdSupervisorColaborador);
        }

        function GerenteCuentaChange() {
            $('#IdClienteGer').empty();
            $('#IdServicesManagerGer').empty();
            vm.CargarCartaFianza('IdClienteGer', vm.gercuenta.IdColaborador);
            vm.CargarGerenteCuenta('IdServicesManagerGer', vm.gercuenta.IdColaborador, 40);
        }

        /***************** Guardar Asignaciones ***************/
        function GuardarSupervisorAGerenteCuenta() {
            if (vm.ValidarSupervisorAGerenteCuenta(vm.supervisor)) {
                vm.supervisor.IdColaboradorStr = ($("#IdGerenteCuentaSup").val() != null) ? ($("#IdGerenteCuentaSup").val().toString()) : 0;
                blockUI.start();

                var promise = AsignarColaboradorCartaFianzaService.AsignarSupervisorAGerenteCuenta(vm.supervisor);

                promise.then(
                    //Success:
                    function (response) {
                        blockUI.stop();
                        var Respuesta = response.data;

                        if (Respuesta.TipoRespuesta != 0) {
                            UtilsFactory.Alerta('#divAlertSupervisorAGerenteCuenta', 'danger', Respuesta.Mensaje, 5);
                        } else {
                            UtilsFactory.Alerta('#divAlertSupervisorAGerenteCuenta', 'success', Respuesta.Mensaje, 3);
                            vm.LimpiarFormularioSup();
                            CargarTablaSupervisorAGerenteCuenta();
                        }
                    },
                    //Error:
                    function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlertSupervisorAGerenteCuenta', 'danger', MensajesUI.DatosError, 5);
                    });
            }

        }

        function GuardarGerenteCuentaACartaFianza() {
            if (vm.ValidarGerenteCuentaACartaFianza(vm.gercuenta)) {
                vm.gercuenta.IdClienteStr = ($("#IdClienteGer").val() != null) ? ($("#IdClienteGer").val().toString()) : 0;
                vm.gercuenta.IdColaboradorStr = ($("#IdServicesManagerGer").val() != null) ? ($("#IdServicesManagerGer").val().toString()) : 0;
                blockUI.start();

                var promise = AsignarColaboradorCartaFianzaService.AsignarGerenteCuentaACartaFianza(vm.gercuenta);

                promise.then(
                    //Success:
                    function (response) {
                        blockUI.stop();
                        var Respuesta = response.data;

                        if (Respuesta.TipoRespuesta != 0) {
                            UtilsFactory.Alerta('#divAlertGerenteCuentaACartaFianza', 'danger', Respuesta.Mensaje, 5);
                        } else {
                            UtilsFactory.Alerta('#divAlertGerenteCuentaACartaFianza', 'success', Respuesta.Mensaje, 3);
                            vm.LimpiarFormularioGer();
                            CargarTablaGerenteCuentaACartaFianza();
                        }
                    },
                    //Error:
                    function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlertGerenteCuentaACartaFianza', 'danger', MensajesUI.DatosError, 5);
                    });
            }
        }

        function ActualizarTesorera() {
            if (vm.ValidarTesorera()) {
                vm.tesorera.IdColaborador = $("#IdTesorera").val();
                blockUI.start();

                var promise = AsignarColaboradorCartaFianzaService.ActualizarTesorera(vm.tesorera);

                promise.then(
                    //Success:
                    function (response) {
                        blockUI.stop();
                        var Respuesta = response.data;

                        if (Respuesta.TipoRespuesta != 0) {
                            UtilsFactory.Alerta('#divAlertActualizarTesorera', 'danger', Respuesta.Mensaje, 5);
                        } else {
                            UtilsFactory.Alerta('#divAlertActualizarTesorera', 'success', Respuesta.Mensaje, 3);
                        }
                    },
                    //Error:
                    function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlertActualizarTesorera', 'danger', MensajesUI.DatosError, 5);
                    });
            }
        }

        /***************** Validaciones ***************/
        function ValidarSupervisorAGerenteCuenta(sup) {
            var response = false;

            if (sup.IdSupervisorColaborador == undefined || sup.IdSupervisorColaborador == null) {
                UtilsFactory.Alerta('#divAlertSupervisorAGerenteCuenta', 'danger', 'Selecione un Supervisor', 10);
            }
            else {
                response = true;
            }

            return response;
        }

        function ValidarGerenteCuentaACartaFianza(ger) {
            var response = false;

            if (ger.IdColaborador == undefined || ger.IdColaborador == null) {
                UtilsFactory.Alerta('#divAlertGerenteCuentaACartaFianza', 'danger', 'Selecione un Gerente de Cuenta', 10);
            }
            else {
                response = true;
            }

            return response;
        }

        function ValidarTesorera(tes) {
            var response = false;

            if ($("#IdTesorera").val() == null) {
                UtilsFactory.Alerta('#divAlertActualizarTesorera', 'danger', 'Selecione una Tesorera', 10);
            }
            else {
                response = true;
            }

            return response;
        }

        /***************** Cargar tablas de Seguimiento ***************/
        function CargarTablaSupervisorAGerenteCuenta() {
            blockUI.start();

            var maestra = {
                //IdCargo -> Recurso
                IdTipoColaboradorFianzaTm: 3,
                IdSupervisorColaborador: -1
            };

            var promise = MaestroCartaFianzaService.ListarColaborador(maestra);

            promise.then((response) => {
                blockUI.stop();
                var Respuesta = response.data;

                tablaSupervisorAGerenteCuenta.clear().draw();

                if (Respuesta.length > 0) {
                    Respuesta.forEach((datos, key) => {
                        tablaSupervisorAGerenteCuenta.row.add([key + 1, datos.NombreCompleto, datos.text]).draw();
                    });
                }

            });

        }

        function CargarTablaGerenteCuentaACartaFianza() {
            blockUI.start();

            var maestra = {
                IdColaborador: -1
            };

            var promise = MaestroCartaFianzaService.ListaCartaFianzaCombo(maestra);

            promise.then((response) => {
                blockUI.stop();
                var Respuesta = response.data;

                tablaGerenteCuentaACartaFianza.clear().draw();

                if (Respuesta.length > 0) {
                    Respuesta.forEach((datos, key) => {
                        tablaGerenteCuentaACartaFianza.row.add([key + 1, datos.NombreCompleto, datos.text]).draw();
                    });
                }

            });
        }

        /***************** Inicializar funciones ***************/
        vm.CargarSupervisor();
        vm.CargarGerenteCuenta('IdGerenteCuentaSup', -2);
        vm.CargarGerenteCuenta('IdGerenteCuentaGer', -1);
        vm.CargarGerenteCuenta('IdServicesManagerGer', -1, 40);
        vm.CargarCartaFianza('IdClienteGer', -2);
        vm.CargarTesorera();

        vm.CargarTablaSupervisorAGerenteCuenta();
        vm.CargarTablaGerenteCuentaACartaFianza();
    }

})();