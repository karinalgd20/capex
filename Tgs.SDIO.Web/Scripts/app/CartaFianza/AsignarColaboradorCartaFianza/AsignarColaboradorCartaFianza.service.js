﻿(function () {
    'use strict',
     angular
    .module('app.CartaFianza')
    .service('AsignarColaboradorCartaFianzaService', AsignarColaboradorCartaFianzaService);

    AsignarColaboradorCartaFianzaService.$inject = ['$http', 'URLS'];


    function AsignarColaboradorCartaFianzaService($http, $urls) {

        var service = {
            AsignarSupervisorAGerenteCuenta: AsignarSupervisorAGerenteCuenta,
            AsignarGerenteCuentaACartaFianza: AsignarGerenteCuentaACartaFianza,
            ActualizarTesorera: ActualizarTesorera

        };

        return service;

        function AsignarSupervisorAGerenteCuenta(cartaFianzaColaborador) {

            return $http({
                url: $urls.ApiCartaFianza + "AsignarColaboradorCartaFianza/AsignarSupervisorAGerenteCuenta",
                method: "POST",
                data: JSON.stringify(cartaFianzaColaborador)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function AsignarGerenteCuentaACartaFianza(cartaFianzaColaborador) {

            return $http({
                url: $urls.ApiCartaFianza + "AsignarColaboradorCartaFianza/AsignarGerenteCuentaACartaFianza",
                method: "POST",
                data: JSON.stringify(cartaFianzaColaborador)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarTesorera(cartaFianzaColaborador) {

            return $http({
                url: $urls.ApiCartaFianza + "AsignarColaboradorCartaFianza/ActualizarTesorera",
                method: "POST",
                data: JSON.stringify(cartaFianzaColaborador)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();