﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('FormularioGerentesCuentaController', FormularioGerentesCuentaController);

    FormularioGerentesCuentaController.$inject = [
            'FormularioGerentesCuentaService',
            'MaestroCartaFianzaService',
            'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];

    function FormularioGerentesCuentaController(
            FormularioGerentesCuentaService,
            MaestroCartaFianzaService,
            MaestraService,
            blockUI,
            UtilsFactory,
            DTColumnDefBuilder,
            DTOptionsBuilder,
            DTColumnBuilder,
            $timeout,
            MensajesUI,
            $urls,
            $scope,
            $compile,
            $modal,
            $injector) {

        vm = this;
        vm.CargarTablaSinRespuesta = CargarTablaSinRespuesta;
        vm.GuardarCartaFianza = GuardarCartaFianza;
        vm.ModalRespuestaGerenteCuentaGC = ModalRespuestaGerenteCuentaGC;
        vm.GuardarRespuestaGerenteCuentaGC = GuardarRespuestaGerenteCuentaGC;
        vm.InputRenovarChange = InputRenovarChange;

        vm.listaSinRespuesta = {};

        var tablaSinRespuesta = $("#tablaSinRespuesta").DataTable({
            pageLength: 10,
            lengthMenu: [[10, 20, 50, -1], [10, 20, 50, 'Todos']],
            searching: false,
            //scrollX: true,
            bSort: false,
            //columnDefs: [{ "width": "20%", "targets": 8 }]
        });

        /***************** Cargar Acciones inputs ***************/
        function CargarTablaSinRespuesta() {
            blockUI.start();

            var maestra = {
                IdEstado: 3,
                IdColaborador: $("#IdGerenteCuenta").val(),
                FechaVencimientoBanco: null,
                IdRenovarTm: -1
            };

            var promise = MaestroCartaFianzaService.CartasFianzasSinRespuesta(maestra);

            promise.then((response) => {
                blockUI.stop();
                vm.listaSinRespuesta = response.data;

                tablaSinRespuesta.clear().draw();

                if (vm.listaSinRespuesta.length > 0) {
                    vm.listaSinRespuesta.forEach((datos, key) => {
                        let columnaRenovar = ColumnaRenovar(datos.IdCartaFianza);
                        let columnaPeridoRenovacion = ColumnaPeridoRenovacion(datos.IdCartaFianza);
                        let columnaSustento = ColumnaSustento(datos.IdCartaFianza);
                        let columnaGuardar = ColumnaGuardar(datos.IdCartaFianza);
                        tablaSinRespuesta.row.add([datos.FechaVencimientoBancostr,
                                                   datos.NumeroGarantia,
                                                   datos.NombreCliente,
                                                   datos.NumeroContrato,
                                                   datos.DescripcionServicioCartaFianza,
                                                   columnaRenovar,
                                                   columnaPeridoRenovacion,
                                                   columnaSustento
                                                   //columnaGuardar
                        ]).draw();
                    });
                    checkboxStyle();

                }
            });

        }

        function ColumnaRenovar(Id) {
            var respuesta = '';
            respuesta = respuesta + '<center><div>';
            respuesta = respuesta + '<input type="checkbox" checked data-toggle="toggle" id="InputRenovar' + Id + '" class="checkboxStyle" onchange="vm.InputRenovarChange(' + Id + ')">';
            respuesta = respuesta + '</div></center>';
            return respuesta;
        };

        function ColumnaPeridoRenovacion(Id) {
            var respuesta = '';
            respuesta = respuesta + '<center><div style="width: 80px !important;">';
            respuesta = respuesta + '<input type="number" class="form-control InputPeridoRenovacionClass" id="InputPeridoRenovacion' + Id + '" min="0" max="300" step="1" pattern="^[0-9]+" onchange="vm.GuardarCartaFianza(' + Id + ')">';
            respuesta = respuesta + '</div></center>';
            return respuesta;
        };

        function ColumnaSustento(Id) {
            var respuesta = '';
            respuesta = respuesta + '<center><div style="width: 300px !important;">';
            respuesta = respuesta + '<input type="text" class="form-control InputSustentoClass" id="InputSustento' + Id + '" onchange="vm.GuardarCartaFianza(' + Id + ')">';
            respuesta = respuesta + '</div></center>';
            return respuesta;
        };

        function ColumnaGuardar(Id) {
            var respuesta = '';
            respuesta = respuesta + '<center><div class="">';
            respuesta = respuesta + '<a class="btn btn-primary btn-lg" id="InputGuardar' + Id + '" onclick="vm.GuardarCartaFianza(' + Id + ')"><i class="fa fa-save"></i></a>';
            respuesta = respuesta + '</div></center>';
            return respuesta;
        };

        function InputRenovarChange(Id) {
            if ($('#InputRenovar' + Id).prop('checked')) {
                $('#InputPeridoRenovacion' + Id).removeAttr('disabled');
            } else {
                $('#InputPeridoRenovacion' + Id).attr('disabled', '');
                $('#InputPeridoRenovacion' + Id).val('');
            }

        }

        /***************** Guardar Asignaciones ***************/
        function GuardarCartaFianza(IdCartaFianza) {
            if (ValidarInputs(IdCartaFianza)) {
                blockUI.start();
                let cartaFianza = {
                    IdCartaFianza: IdCartaFianza,
                    IdColaborador: $("#IdGerenteCuenta").val(),
                    IdRenovarTm: ($('#InputRenovar' + IdCartaFianza).prop('checked')) ? 5 : 1,
                    PeriodoMesRenovacion: $('#InputPeridoRenovacion' + IdCartaFianza).val(),
                    SustentoRenovacion: $('#InputSustento' + IdCartaFianza).val()
                };

                var promise = MaestroCartaFianzaService.GuardarRespuestaGerenteCuenta(cartaFianza);

                promise.then(
                    //Success:
                    function (response) {
                        blockUI.stop();
                        var Respuesta = response.data;

                        if (Respuesta.TipoRespuesta != 0) {
                            UtilsFactory.Alerta('#divAlertFormularioGC', 'danger', Respuesta.Mensaje, 5);
                        } else {
                            //UtilsFactory.Alerta('#divAlertFormularioGC', 'success', Respuesta.Mensaje, 3);
                        }
                    },
                    //Error:
                    function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlertFormularioGC', 'danger', MensajesUI.DatosError, 5);
                    });
            }


        }

        function ValidarInputs(IdCartaFianza) {
            var response = false;

            //if ($('#InputPeridoRenovacion' + IdCartaFianza).val() == "") {
            //    UtilsFactory.Alerta('#divAlertFormularioGC', 'danger', 'Escriba un Periodo de Renovación', 5);
            //}
            if ($('#InputSustento' + IdCartaFianza).val() == "") {
                UtilsFactory.Alerta('#divAlertFormularioGC', 'warning', 'Escriba un Sustento', 5);
            }
            else {
                response = true;
            }

            return response;
        }

        function ModalRespuestaGerenteCuentaGC() {
            if (ValidarTodosInptus()) {
                $("#ModalGuardarRespuesta").modal();
            }
        }

        function GuardarRespuestaGerenteCuentaGC() {
            blockUI.start();
            let cartaFianza = {
                IdColaborador: $("#IdGerenteCuenta").val()
            };

            var promise = MaestroCartaFianzaService.GuardarConfirmacionGerenteCuenta(cartaFianza);

            promise.then(
                //Success:
                function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;

                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#divAlertGuardarRespuesta', 'danger', Respuesta.Mensaje, 5);
                    } else {
                        UtilsFactory.Alerta('#divAlertGuardarRespuesta', 'success', Respuesta.Mensaje, 3);
                        vm.CargarTablaSinRespuesta();
                        setTimeout(() => { $("#ModalGuardarRespuesta").modal('hide');; }, 3000);
                    }
                },
                //Error:
                function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertGuardarRespuesta', 'danger', MensajesUI.DatosError, 5);
                });
        }

        function ValidarTodosInptus() {
            var response = false;
            var response1 = false;
            var response2 = true;
            var allInputsSustento = $(".InputSustentoClass");
            var allInputsRenovar = $(".checkboxStyle");
            var allInputsPerRenov = $(".InputPeridoRenovacionClass");

            for (let i = 0; i < allInputsSustento.length; i++) {
                if (allInputsSustento[i].value.length > 0) {
                    response1 = true;
                }
            }

            for (let i = 0; i < allInputsSustento.length; i++) {
                if (allInputsRenovar[i].checked) {
                    if (allInputsPerRenov[i].value.length == 0) {
                        response2 = false;
                    }
                }
            }

            if (!response1) {
                UtilsFactory.Alerta('#divAlertFormularioGC', 'warning', 'Debe completar al menos un Sustento de la tabla', 5);
            }

            if (!response2) {
                UtilsFactory.Alerta('#divAlertFormularioGC', 'warning', 'Debe completar al menos un Periodo Renovacion de la tabla', 5);
            }

            response = response1 && response2;

            return response;
        }

        /***************** Otras funciones ***************/
        function checkboxStyle() {
            $('.checkboxStyle').bootstrapToggle({
                on: 'Si',
                off: 'No'
            });
        }

        /***************** Inicializar funciones ***************/
        vm.CargarTablaSinRespuesta();


    }

})();