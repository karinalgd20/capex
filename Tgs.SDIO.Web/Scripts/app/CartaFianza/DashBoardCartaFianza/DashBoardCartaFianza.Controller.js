﻿(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('DashBoardCartaFianzaController', DashBoardCartaFianzaController);

    DashBoardCartaFianzaController.$inject = [
                                                'blockUI',
                                                '$timeout',
                                                'UtilsFactory',
                                                'URLS',
                                                'DashBoardCartaFianzaService'

    ];

    function DashBoardCartaFianzaController(

                                    blockUI,
                                    $timeout,
                                    UtilsFactory,
                                    $urls,
                                    DashBoardCartaFianzaService

                                    ) {
        var vm = this;
        vm.MostrarDashboard = MostrarDashboard;
        vm.IniciarSpinner = IniciarSpinner;
        vm.CargaListaMeses = CargaListaMeses;
        vm.Graficos = Graficos;
        vm.CargaPopupPastelMostrar = CargaPopupPastelMostrar;
        vm.CargaPopupPastelQuitar = CargaPopupPastelQuitar;

        vm.CF_CartaFianzaTotal = 0;
        vm.CF_SolicitudEmision = 0;
        vm.CF_Emision_Vigente = 0;
        vm.CF_Renovacion = 0;
        vm.CF_SolicitudEmision = 0;

        vm.CF_SolicitudEmision_Pendiente = 0;
        vm.CF_Emision_EnviadoGC_Seguimiento = 0;
        vm.CF_Renovacion = 0;
        vm.CF_SolicitudEmision = 0;

        vm.CF_SolicitudEmision_EnviadoTso = 0;
        vm.CF_Emision_EnviadoGC_Reiterativo = 0;
        vm.CF_Renovacion = 0;
        vm.CF_SolicitudEmision = 0;

        vm.CF_SolicitudEmision_EnviadoTso_Errada = 0;
        vm.CF_Emision_EnviadoGC_Escalado = 0;
        vm.CF_Renovacion = 0;
        vm.CF_SolicitudEmision = 0;

        vm.CF_RenovacionEmision_RecepcionTso = 0;

        vm.ListaEmision = [];
        vm.ListaRenovacion = [];
        vm.ListaRecupero = [];



        vm.ListMeses = [];
        vm.CmbMeses = "-1";
        $('#spinner4').spinner({ value: (new Date().getFullYear()), step: 1, min: 2000, max: 2200 });

        function Graficos() {         

            var fechaActual = new Date();
            var anioActual = $('#spinner4').spinner('value') != 0 ? $('#spinner4').spinner('value') : fechaActual.getFullYear();
            var mesActual = vm.CmbMeses != 0 ? vm.CmbMeses : (fechaActual.getMonth() + 1);
            var dashboard =
            {
                Anio: anioActual,
                mes: mesActual
            };

            var promise = DashBoardCartaFianzaService.ListaDatos_DashoBoard_CartaFianza_Pie(dashboard);

            promise.then(function (resultado) {
                vm.ListaEmision = [];
                vm.ListaRenovacion = [];
                vm.ListaRecupero = [];

                var result = resultado.data;

                result.forEach(function (element) {
                            
                    if (element.IdTipoAccionTm == 1) {
                     
                        var Lista = {
                            value: element.Conteo,
                            label: element.Descripcion
                            }                       
                            vm.ListaEmision.push(Lista);             
                        }

                    if (element.IdTipoAccionTm == 3) {
                            var Lista = {
                                value: element.Conteo,
                                label: element.Descripcion
                            }                            
                            vm.ListaRenovacion.push(Lista);
                        }

                    if (element.IdTipoAccionTm == 4) {
                             var Lista = {
                                 value: element.Conteo,
                                 label: element.Descripcion
                             }                             
                             vm.ListaRecupero.push(Lista);                
                    }

                });

                Morris.Donut({             
                    element: 'donut-emision',
                    data: vm.ListaEmision,
                    backgroundColor: '#ccc',
                    labelColor: '#060',
                    pointSize: 10,
                    lineWidth: 10,
                    // hideHover: 'auto',
                    colors: [
                         '#148F77',
                         '#17A589',
                         '#1ABC9C',
                         '#48C9B0'
                    ],

                  formatter: function (x) { return x + "%"}
                   });

                Morris.Donut({
                element: 'donut-renovacion',
                data: vm.ListaRenovacion,
                backgroundColor: '#ccc',
                labelColor: '#060',
                pointSize: 0,
                lineWidth: 0,
                colors: [
                     '#CA6F1E',
                     '#E67E22',
                     '#EB984E',
                     '#F0B27A'
                ],
                formatter: function (x) { return x + "%" }
            });

                Morris.Donut({
                element: 'donut-recupero',
                data: vm.ListaRecupero,
                backgroundColor: '#ccc',
                labelColor: '#060',
                pointSize: 0,
                lineWidth: 0,
                    colors: [
                     '#7FB3D5',
                     '#5DADE2',
                     '#3498DB',
                     '#2E86C1'
                ],
                formatter: function (x) { return x + "%" }
            });
               
               

                blockUI.stop();
            }, function (response) {
                blockUI.stop();

            });
            blockUI.start();
   
            $timeout(function () {
                blockUI.stop();
            }, 2000);
    
         
        }



        function IniciarSpinner() {

            $('#spinner4').spinner({ value: (new Date().getFullYear()), step: 1, min: 2000, max: 2200 });
            vm.anio = (new Date().getFullYear());
        }

        function MostrarDashboard() {
            var fechaActual = new Date();

            var anioActual = $('#spinner4').spinner('value') != 0 ? $('#spinner4').spinner('value') : fechaActual.getFullYear();
            var mesActual = vm.CmbMeses != 0 ? vm.CmbMeses : (fechaActual.getMonth() + 1);
            var dashboard =
            {
                Anio: anioActual,
                mes: mesActual
            };

            var promise = DashBoardCartaFianzaService.ListaDatos_DashoBoard_CartaFianza(dashboard);

            promise.then(function (resultado) {

                var result = resultado.data;

                vm.CF_CartaFianzaTotal = result.CF_CartaFianzaTotal;
                vm.CF_SolicitudEmision = result.CF_SolicitudEmision;
                vm.CF_SolicitudEmision_Pendiente = result.CF_SolicitudEmision_Pendiente;
                vm.CF_SolicitudEmision_EnviadoTso = result.CF_SolicitudEmision_EnviadoTso;
                vm.CF_SolicitudEmision_EnviadoTso_Errada = result.CF_SolicitudEmision_EnviadoTso_Errada;

                vm.CF_SolicitudEmision_FechaCorrecion = result.CF_SolicitudEmision_FechaCorrecion;
                vm.CF_SolicitudEmision_FechaRecepcionCorrecion = result.CF_SolicitudEmision_FechaRecepcionCorrecion;


                vm.CF_Emision_Vigente = result.CF_Emision_Vigente;
                vm.CF_Emision_EnviadoGC_Seguimiento = result.CF_Emision_EnviadoGC_Seguimiento;
                vm.CF_Emision_EnviadoGC_Reiterativo = result.CF_Emision_EnviadoGC_Reiterativo;
                vm.CF_Emision_EnviadoGC_Escalado = result.CF_Emision_EnviadoGC_Escalado;

                vm.CF_Renovacion = result.CF_Renovacion;
                vm.CF_RenovacionEmision_EnviadoTso = result.CF_RenovacionEmision_EnviadoTso;
                vm.CF_RenovacionEmision_RecepcionTso = result.CF_RenovacionEmision_RecepcionTso;
                vm.CF_RenovacionEmision_EntregadoClienteTso = result.CF_RenovacionEmision_EntregadoClienteTso;
                vm.CF_RenovacionEmision_XEntregarClienteTso = result.CF_RenovacionEmision_XEntregarClienteTso;

                vm.CF_RenovacionEmision_GestionXTesoreria = result.CF_RenovacionEmision_GestionXTesoreria;

                vm.CF_RenovacionEmision_Pendiente = result.CF_RenovacionEmision_Pendiente;

                vm.CF_Recupero = result.CF_Recupero;
                vm.CF_Recupero_Ejecutada = result.CF_Recupero_Ejecutada;
                vm.CF_Recupero_Devuelta = result.CF_Recupero_Devuelta;
                vm.CF_Recupero_xSol_Recuperada = result.CF_Recupero_xSol_Recuperada;

                vm.CF_Recupero_conSolicitudDevolucion = result.CF_Recupero_conSolicitudDevolucion;
                vm.CF_Recupero_Recuperada = result.CF_Recupero_Recuperada;
                vm.CF_Recupero_ProcesoRecupero = result.CF_Recupero_ProcesoRecupero;
                vm.CF_Recupero_PorRecojer = result.CF_Recupero_PorRecojer;
                vm.CF_Recupero_Descargadas = result.CF_Recupero_Descargadas;

                blockUI.stop();

            }, function (response) {
                blockUI.stop();

            });

            blockUI.start();
      
            $timeout(function () {
                blockUI.stop();
            }, 2000);


        }


        function CargaListaMeses() {
            vm.ListMeses = [
             { Codigo: "-1", Descripcion: "Selecione" },
                    { Codigo: "1", Descripcion: "Enero" },
                    { Codigo: "2", Descripcion: "Febrero" },
                    { Codigo: "3", Descripcion: "Marzo" },
                    { Codigo: "4", Descripcion: "Abril" },
                    { Codigo: "5", Descripcion: "Mayo" },
                    { Codigo: "6", Descripcion: "Junio" },
                    { Codigo: "7", Descripcion: "Julio" },
                    { Codigo: "8", Descripcion: "Agosto" },
                    { Codigo: "9", Descripcion: "Setiembre" },
                    { Codigo: "10", Descripcion: "Octubre" },
                    { Codigo: "11", Descripcion: "Noviembre" },
                    { Codigo: "12", Descripcion: "Diciembre" }
            ];
            vm.CmbMeses = (new Date().getMonth() + 1);
        }


        function CargaPopupPastelMostrar(valor) {
         //$("#emision").css({ top: $("#emision").offset().top + $("#emision").height(), left: $("#emision").offset().left });
            $("#emision").show();
            $("#renovacion").show();
            $("#recupero").show();
        }

        function CargaPopupPastelQuitar(valor) {
            //$('#'+valor).css({ top: $("#emision").offset().top + $("#emision").height(), left: $("#emision").offset().left });
            $('#' + valor).hide();
        }


        MostrarDashboard();
        CargaListaMeses();
        IniciarSpinner();

        Graficos();

    }
})();

