﻿(function () {
    'use strict',
     angular
    .module('app.CartaFianza')
    .service('DashBoardCartaFianzaService', DashBoardCartaFianzaService);

    DashBoardCartaFianzaService.$inject = ['$http', 'URLS'];

    function DashBoardCartaFianzaService($http, $urls) {

        var service = {
            ListaDatos_DashoBoard_CartaFianza:ListaDatos_DashoBoard_CartaFianza   ,
            ListaDatos_DashoBoard_CartaFianza_Pie:ListaDatos_DashoBoard_CartaFianza_Pie
        };

        return service;


        function ListaDatos_DashoBoard_CartaFianza(DashoardDtoRequest) {
            return $http({
                url: $urls.ApiCartaFianza + "DashBoardCartaFianza/ListaDatos_DashoBoard_CartaFianza",
                method: "POST",
                data: JSON.stringify(DashoardDtoRequest)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


           function ListaDatos_DashoBoard_CartaFianza_Pie(DashoardDtoRequest) {
            return $http({
                url: $urls.ApiCartaFianza + "DashBoardCartaFianza/ListaDatos_DashoBoard_CartaFianza_Pie",
                method: "POST",
                data: JSON.stringify(DashoardDtoRequest)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();