﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('ReporteCartaFianzaSinRespuestaController', ReporteCartaFianzaSinRespuestaController);

    ReporteCartaFianzaSinRespuestaController.$inject = [
            'ReporteCartaFianzaSinRespuestaService',
            'MaestroCartaFianzaService',
            'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];

    function ReporteCartaFianzaSinRespuestaController(
            ReporteCartaFianzaSinRespuestaService,
            MaestroCartaFianzaService,
            MaestraService,
            blockUI,
            UtilsFactory,
            DTColumnDefBuilder,
            DTOptionsBuilder,
            DTColumnBuilder,
            $timeout,
            MensajesUI,
            $urls,
            $scope,
            $compile,
            $modal,
            $injector) {

        vm = this;

        vm.CargarIndicadorColor = CargarIndicadorColor;
        vm.BuscarReporteSinRespuesta = BuscarReporteSinRespuesta;
        vm.LimpiarFiltros = LimpiarFiltros;

        vm.search = {};


        /************** Cargar Combo box **************/
        function CargarIndicadorColor() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 440
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaIndicadorColor = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarTipoRenovar() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 218
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListaRenovar = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarGerenteCuenta() {
            blockUI.start();

            var maestra = {
                //IdCargo -> Recurso
                IdTipoColaboradorFianzaTm: 3,
                IdSupervisorColaborador: -1
            };

            var promise = MaestroCartaFianzaService.ListarColaborador(maestra);

            promise.then(
                function (response) {
                    blockUI.stop();

                    var Respuesta = response.data;

                    $("#IdGerenteCuenta").select2({
                        data: Respuesta,
                        placeholder: '--Seleccione--',
                        allowClear: true
                    });
                    $("#IdGerenteCuenta").val(-1).change();

                },
                function (response) {
                    blockUI.stop();
                });
        };

        /************** Buscar Reporte Sin Respuesta **************/
        function BuscarReporteSinRespuesta() {

            blockUI.start();

            let gerenteCuenta = ($("#IdGerenteCuenta").val() != null) ? ($("#IdGerenteCuenta").val().toString()) : -1;
            let fechaVencimiento = (vm.search.FechaVencimiento) ? ConvertDateToString(vm.search.FechaVencimiento) : null;
            let idRenovarTm = (vm.search.IdRenovarTm) ? vm.search.IdRenovarTm : -1;

            var parametros =
            "reporte=CartaFianzaSinRespuesta" +
            "&Indicador=" + vm.search.IndicadorColor +
            "&IdGerenteCuenta=" + gerenteCuenta +
            "&FechaVencimiento=" + fechaVencimiento +
            "&IdRenovarTm=" + idRenovarTm;

            $("[id$='FReporte']").attr('src', $urls.ApiReportes + parametros);

            $("#FReporte").height(800);

            $timeout(function () {
                blockUI.stop();
            }, 2000);
        };

        function LimpiarFiltros() {
            vm.search = {};
        }

        /************** Otras funciones **************/
        function ConvertDateToString(fecha) {
            let año = fecha.getFullYear();
            let mes = ('0' + (fecha.getMonth() + 1).toString()).slice(-2);
            let dia = ('0' + fecha.getDate()).slice(-2);
            return dia + '/' + mes + '/' + año;
        }

        CargarIndicadorColor();
        CargarGerenteCuenta();
        vm.search.IndicadorColor = '1';
        BuscarReporteSinRespuesta();
        CargarTipoRenovar();
        vm.search.IdRenovarTm = '-1';
    }

})();