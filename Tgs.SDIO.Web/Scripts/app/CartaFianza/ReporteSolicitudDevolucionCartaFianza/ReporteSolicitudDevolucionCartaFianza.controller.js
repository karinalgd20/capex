﻿var vm;

(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('ReporteSolicitudDevolucionCartaFianzaController', ReporteSolicitudDevolucionCartaFianzaController);

    ReporteSolicitudDevolucionCartaFianzaController.$inject = [
            'ReporteSolicitudDevolucionCartaFianzaService',
            'ClienteCartaFianzaService',
            'MaestraService',
             'ClienteService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];

    function ReporteSolicitudDevolucionCartaFianzaController(
            ReporteSolicitudDevolucionCartaFianzaService,
             ClienteCartaFianzaService,
            MaestraService,
            ClienteService,
            blockUI,
            UtilsFactory,
            DTColumnDefBuilder,
            DTOptionsBuilder,
            DTColumnBuilder,
            $timeout,
            MensajesUI,
            $urls,
            $scope,
            $compile,
            $modal,
            $injector) {

        vm = this;

        vm.BuscarReporteSolDevolucion = BuscarReporteSolDevolucion;
        
        vm.LimpiarFiltros = LimpiarFiltros;

        vm.SelectCliente = SelectCliente;
        vm.LimpiarCampoCliente = LimpiarCampoCliente;
        vm.fillTextbox = fillTextbox;
        vm.ObtenerCliente = ObtenerCliente;
        vm.ListarCliente = ListarCliente;



         
        
        function BuscarReporteSolDevolucion() {
            blockUI.start();

            if (vm.IdCliente == null || vm.IdCliente == -5 || vm.IdCliente == "undefined") {
                vm.IdCliente = "0";
            }

            var parametros =
            "reporte=CartaFianzaReporteSolicitudDevolucion" +
            "&p_IdCliente=" + vm.IdCliente +
            "&p_numeroGarantia=" + ((vm.NumeroGarantia == undefined) ? '' : vm.NumeroGarantia) ;

            $("[id$='FReporte']").attr('src', $urls.ApiReportes + parametros);

            $("#FReporte").height(1000);

            $timeout(function () {
                blockUI.stop();
            }, 2000);
        };

        function LimpiarFiltros() {
            vm.NumeroGarantia = '';
            vm.NumeroIdentificadorFiscal = '';
            vm.NombreCliente = '';
            vm.IdCliente = '-5';
        }

        function SelectCliente() {
            vm.IdCliente = 0;

            vm.ListCliente = null;
            if (vm.NombreCliente.length > 5) {
                ListarCliente();
                vm.heightCliente = "";
            }
        }

        function LimpiarCampoCliente() {
            vm.IdCliente = 0;
            vm.NumeroIdentificadorFiscal = '';
        }

        function fillTextbox(string, Id) {
            LimpiarCampoCliente();
            vm.NombreCliente = string;
            vm.IdCliente = Id;
            vm.ListCliente = null;
            ObtenerCliente(vm.NombreCliente);
        }

        function ObtenerCliente(descripcion) {
            var cliente = {
                IdEstado: 1,
                descripcion: descripcion,

            };
            var promise = ClienteService.ListarClientePorDescripcion(cliente);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                if (Respuesta != null) {
                    vm.NumeroIdentificadorFiscal = Respuesta[0].NumeroIdentificadorFiscal;
                    // var IdDireccionComercial = Respuesta[0].IdDireccionComercial;
                    //var IdSector = Respuesta[0].IdSector;
                    // ObtenerDireccionComercial(IdDireccionComercial);
                    // ObtenerSector(IdSector);

                }

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarCliente() {
            if (vm.NombreCliente != null || vm.NombreCliente != "") {
                var cliente = {
                    IdEstado: 1,
                    Descripcion: vm.NombreCliente,

                };
                var promise = ClienteService.ListarClientePorDescripcion(cliente);

                promise.then(function (resultado) {
                    blockUI.stop();
                    if (Respuesta != null) {
                        vm.heightCliente = 350;
                    }

                    var Respuesta = resultado.data;
                    var output = [];
                    angular.forEach(Respuesta, function (cliente) {
                        output.push(cliente);
                    });
                    vm.ListCliente = output;

                }, function (response) {
                    blockUI.stop();

                });
            }
        }

        vm.BuscarReporteSolDevolucion();
    }

})();