﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('SolarWindService', SolarWindService);

    SolarWindService.$inject = ['$http', 'URLS'];

    function SolarWindService($http, $urls) {

        var service = {
            ModalSolarWind: ModalSolarWind,
            CantidadSolarWind: CantidadSolarWind
        };

        return service;

        function ModalSolarWind(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SolarWind/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function CantidadSolarWind(dto) {
            return $http({ 
                url: $urls.ApiOportunidad + "SolarWind/CantidadSolarWind",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };


    }
})();