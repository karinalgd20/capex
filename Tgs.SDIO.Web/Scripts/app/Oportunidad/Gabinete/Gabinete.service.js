﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('GabineteService', GabineteService);

    GabineteService.$inject = ['$http', 'URLS'];

    function GabineteService($http, $urls) {

        var service = {
            ModalGabinete: ModalGabinete,
            CantidadGabinete: CantidadGabinete
        };

        return service;

        function ModalGabinete(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Gabinete/Index",
                method: "POST",
                data: JSON.stringify(dto) 
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function CantidadGabinete(dto) {
            return $http({
                url: $urls.ApiOportunidad + "ECapex/CantidadCapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

    }
})();