﻿(function () {
    'use strict',
    angular
        .module('app.Oportunidad')
        .service('ServicioSedeInstalacionService', ServicioSedeInstalacionService);

    ServicioSedeInstalacionService.$inject = ['$http', 'URLS'];

    function ServicioSedeInstalacionService($http, $urls) {
        var service = {
            RegistrarServicioSedeInstalacion: RegistrarServicioSedeInstalacion
        };

        return service;

        function RegistrarServicioSedeInstalacion(request) {
            return $http({
                url: $urls.ApiOportunidad + "ServicioSedeInstalacion/RegistrarServicioSedeInstalacion",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();