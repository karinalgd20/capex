﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('FContableService', FContableService);

    FContableService.$inject = ['$http', 'URLS'];

    function FContableService($http, $urls) {

        var service = {
            VistaFContable: VistaFContable
        };
         
        return service;

        function VistaFContable(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FContable/Index",
                method: "GET",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
              
            function DatosCompletados(response) {  
                return response;
            }
        };

    }
})();