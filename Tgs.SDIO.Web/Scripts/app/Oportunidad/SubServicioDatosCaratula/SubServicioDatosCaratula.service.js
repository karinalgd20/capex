﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('SubServicioDatosCaratulaService', SubServicioDatosCaratulaService);

    SubServicioDatosCaratulaService.$inject = ['$http', 'URLS'];

    function SubServicioDatosCaratulaService($http, $urls) {
        var service = {
            ListarSubServicioDatoCaratula: ListarSubServicioDatoCaratula,
            ActualizarSubServicioDatoCaratula: ActualizarSubServicioDatoCaratula,
            RegistrarSubServicioDatosCaratula: RegistrarSubServicioDatosCaratula
        };

        return service;

        function RegistrarSubServicioDatosCaratula(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatosCaratula/RegistrarSubServicioDatosCaratula",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarSubServicioDatoCaratula(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatosCaratula/ActualizarSubServicioDatoCaratula",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) { 
                return resultado;
            }
        };

        function ListarSubServicioDatoCaratula(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatosCaratula/ListarSubServicioDatoCaratula",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();