﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('EquipoSeguridadController', EquipoSeguridadController);

    EquipoSeguridadController.$inject = ['EquipoSeguridadService','FlujoCajaService','EcapexService','SubServicioDatosCapexService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile'];

    function EquipoSeguridadController(EquipoSeguridadService, FlujoCajaService,EcapexService,SubServicioDatosCapexService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile) {

        var vm = this;
        vm.tabFlujoCaja = true;
        vm.IdFlujoCaja = 0;
        vm.IdOportunidadLineaNegocio = 0;
        vm.IdSubServicioDatosCapex = 0;
        vm.IdFlujoCajaConfiguracion = 0;
        vm.IdPeriodos = 0;
        vm.CargarEquipoSeguridad = CargarEquipoSeguridad;
        vm.GrabarConceptoEquipoSeguridad = GrabarConceptoEquipoSeguridad;
        vm.ObtieneCostoPreOperativo = ObtieneCostoPreOperativo;

        vm.ListarTipo = ListarTipo;
        vm.ListTipo = [];
        vm.IdTipo = "-1";
        /******************************************* Metodos *******************************************/

        function ListarTipo() {

            var pestanaGrupoTipo = {
                IdGrupo: 10
            };
            var promise = EcapexService.ListarPestanaGrupoTipo(pestanaGrupoTipo);
            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.IdTipo = "-1";

                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }
        function CargarEquipoSeguridad() {

            var dto = {
                IdSubServicioDatosCapex: $scope.$parent.vm.dto.IdSubServicioDatosCapex,
                IdGrupo: 10
            }; 

            var promise = FlujoCajaService.ObtenerDetalleOportunidadEcapex(dto); 
            promise.then(function (response) {

                console.log(response.data);
                var result = response.data;
           
                vm.Equipo = result.SubServicio;
                vm.Cruce = result.Cruce;
                vm.AEReducido = result.AEReducido;
                vm.Tipo = result.Tipo;
                vm.Marca = result.Marca;
                vm.MesesAntiguedad = result.MesesAntiguedad;
                vm.Cantidad = result.Cantidad;
                vm.CostoUnitarioAntiguo = result.CostoUnitarioAntiguo;
                vm.ValorResidualSoles = result.ValorResidualSoles;
                vm.CostoUnitario = result.CostoUnitario;
                vm.CapexDolares = result.CapexDolares;
                vm.CapexSoles = result.CapexSoles;
                vm.TotalCapex = result.TotalCapex;
                vm.MesRecupero = result.MesRecupero;
                vm.MesComprometido = result.MesComprometido;
                vm.MesCertificado = result.MesCertificado;
                

                ObtieneCostoPreOperativo();

                vm.IdFlujoCaja = result.IdFlujoCaja;
                vm.IdOportunidadLineaNegocio = result.IdOportunidadLineaNegocio;
                vm.IdSubServicioDatosCapex = result.IdSubServicioDatosCapex;
                vm.IdFlujoCajaConfiguracion = result.IdFlujoCajaConfiguracion;

                vm.IdPeriodos = result.IdPeriodos==0?"1":result.IdPeriodos;
               // $scope.$$childHead.vm.Meses = result.TiempoProyecto;
                $scope.$$childHead.vm.ObtenerFlujoCajaConfiguracion();

                       if(result.IdTipoSubServicio> 0){ListarTipoPorId(result.IdTipoSubServicio)}else{ListarTipo();}
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });

        };
                   function ListarTipoPorId(int) {

                var pestanaGrupoTipo = {
                    IdGrupo: 10
                };
                var promise = EcapexService.ListarPestanaGrupoTipo(pestanaGrupoTipo);
                promise.then(function (resultado) {
                    blockUI.stop();

                    var Respuesta = resultado.data;

                    vm.IdTipo = int;
                    vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);

                }, function (response) {
                    blockUI.stop();
                });
            }
        function GrabarConceptoEquipoSeguridad() {

            var flujocaja = {
                IdFlujoCaja: vm.IdFlujoCaja,
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                Cantidad: vm.Cantidad,
                CostoUnitario: vm.CapexSoles,
                IdTipoSubServicio: vm.IdTipo
            };


            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();

                var promise = FlujoCajaService.ActualizarOportunidadFlujoCaja(flujocaja);
                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#lblAlerta_EquiposSeguridad', 'danger', MensajesUI.DatosError, 20);
                    } else {

                        var capex = {
                            IdFlujoCaja: vm.IdFlujoCaja,
                            IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                            Cruce: vm.Cruce,
                            AEReducido: vm.AEReducido,
                            Tipo: vm.Tipo,
                            Marca: vm.Marca,
                            MesesAntiguedad: vm.MesesAntiguedad,
                            Cantidad: vm.Cantidad,
                            CostoUnitarioAntiguo: vm.CostoUnitarioAntiguo,
                            ValorResidualSoles: vm.ValorResidualSoles,
                            CostoUnitario: vm.CostoUnitario,
                            CapexDolares: vm.CapexDolares,
                            CapexSoles: vm.CapexSoles,
                            TotalCapex: vm.TotalCapex,
                            MesRecupero: vm.MesRecupero,
                            MesComprometido: vm.MesComprometido,
                            MesCertificado: vm.MesCertificado
                        };

                        var promise = SubServicioDatosCapexService.ActualizarSubServicioDatosCapex(capex);
                        promise.then(function (response) {

                            var Respuesta = response.data;
                            if (Respuesta.TipoRespuesta != 0) {
                                UtilsFactory.Alerta('#lblAlerta_EquiposSeguridad', 'danger', Respuesta.Mensaje, 20);
                            } else {

                                UtilsFactory.Alerta('#lblAlerta_EquiposSeguridad', 'success', Respuesta.Mensaje, 10);
                                $scope.$$childTail.vm.GrabarFlujoCajaConfiguracion();
                                $scope.$parent.vm.BuscarEquipoSeguridad();
                            }

                        }, function (response) {
                            blockUI.stop();
                            UtilsFactory.Alerta('#lblAlerta_EquiposSeguridad', 'danger', MensajesUI.DatosError, 5);
                        });
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_EquiposSeguridad', 'danger', MensajesUI.DatosError, 5);
                });
            };
        };

        /******************************************* LOAD *******************************************/
        CargarEquipoSeguridad();
        vm.IdGrupo = 10;

        vm.CuSoles = 50;
        vm.Cantidad = 3;
        vm.Antiguiedad = 1;
        vm.CuAntiguiedad = 8;
        vm.MesesAntiguedad = 5;
        vm.ValorResidual = 0;
        vm.Indice = 1;
        vm.CapexDolares = 0;
        vm.CapexSoles = 0;
        vm.TotalCapex = 0;

        vm.CantidadEquipoSeguridadComplete = CantidadEquipoSeguridadComplete;

        function CantidadEquipoSeguridadComplete() {
            var dto = {
                IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                IdGrupo: vm.IdGrupo,
                IdEstado: 1,
                Cu: vm.CostoUnitario,
                Cantidad: vm.Cantidad,
                CuAntiguiedad: vm.CostoUnitarioAntiguo,
                MesesAntiguedad: vm.MesesAntiguedad,
                Indice: vm.Indice,
                IdTipoSubServicio: vm.IdTipo
            };

            var promise = EquipoSeguridadService.CantidadEquipoSeguridad(dto);
            promise.then(function (response) {
                vm.ValorResidualSoles = response.data.ValorResidualSoles;
                vm.CapexDolares = response.data.CapexDolares;
                vm.CapexSoles = response.data.CapexSoles;
                vm.TotalCapex = response.data.TotalCapex;
                ObtieneCostoPreOperativo();
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        };

        function ObtieneCostoPreOperativo() {
            $scope.$$childHead.vm.CostoPreOperativo = vm.CapexDolares;
        };


    }

})();