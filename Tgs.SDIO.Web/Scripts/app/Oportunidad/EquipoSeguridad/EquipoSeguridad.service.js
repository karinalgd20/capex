﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('EquipoSeguridadService', EquipoSeguridadService);

    EquipoSeguridadService.$inject = ['$http', 'URLS'];

    function EquipoSeguridadService($http, $urls) {

        var service = {
            ModalEquipoSeguridad: ModalEquipoSeguridad,
            CantidadEquipoSeguridad: CantidadEquipoSeguridad
        };

        return service;

        function ModalEquipoSeguridad(dto) {
            return $http({
                url: $urls.ApiOportunidad + "EquipoSeguridad/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response; 
            }
        };

        function CantidadEquipoSeguridad(dto) {
            return $http({
                url: $urls.ApiOportunidad + "EquipoSeguridad/CantidadEquipoSeguridad",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };


    }
})();