﻿(function () {
    'use strict',

    angular
        .module('app.Oportunidad')
        .service('PlantillaImplantacionService', PlantillaImplantacionService);

    PlantillaImplantacionService.$inject = ['$http', 'URLS'];

    function PlantillaImplantacionService($http, $urls) {
        var service = {
            RegistrarPlantillaImplantacion: RegistrarPlantillaImplantacion,
            InactivarPlantillaImplantacion: InactivarPlantillaImplantacion,
            ListarPlantillaImplantacionPaginado: ListarPlantillaImplantacionPaginado
        };

        return service;

        function RegistrarPlantillaImplantacion(request) {
            return $http({
                url: $urls.ApiOportunidad + "PlantillaImplantacion/RegistrarPlantillaImplantacion",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function InactivarPlantillaImplantacion(request) {
            return $http({
                url: $urls.ApiOportunidad + "PlantillaImplantacion/InactivarPlantillaImplantacion",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarPlantillaImplantacionPaginado(filtro) {
            return $http({
                url: $urls.ApiOportunidad + "PlantillaImplantacion/ListarPlantillaImplantacionPaginado",
                method: "POST",
                data: JSON.stringify(filtro)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();