﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('HardwareService', HardwareService);

    HardwareService.$inject = ['$http', 'URLS'];

    function HardwareService($http, $urls) {

        var service = {
            ModalHardware: ModalHardware,
            CantidadHardware: CantidadHardware
        };

        return service;

        function ModalHardware(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Hardware/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function CantidadHardware(dto) {
            return $http({
                url: $urls.ApiOportunidad + "ECapex/CantidadCapex", 
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
    }
})();