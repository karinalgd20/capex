﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('EquiposEspecialesService', EquiposEspecialesService);

    EquiposEspecialesService.$inject = ['$http', 'URLS'];

    function EquiposEspecialesService($http, $urls) {

        var service = {
            ModalEquiposEspeciales: ModalEquiposEspeciales,
            CantidadEquiposEstudiosEsp: CantidadEquiposEstudiosEsp
        };
         
        return service;

        function ModalEquiposEspeciales(dto) {
            return $http({
                url: $urls.ApiOportunidad + "EquiposEspeciales/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
              
            function DatosCompletados(response) {  
                return response;
            }
        };

        function CantidadEquiposEstudiosEsp(dto) {
            return $http({
                url: $urls.ApiOportunidad + "EquiposEspeciales/CantidadEquiposEsp",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

    }
})();