﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('RegistrarPlantillaImplantacionService', RegistrarPlantillaImplantacionService);

    RegistrarPlantillaImplantacionService.$inject = ['$http', 'URLS'];

    function RegistrarPlantillaImplantacionService($http, $urls) {
        var service = {
            ModalPlantillaImplantacion: ModalPlantillaImplantacion
        };

        return service;

        function ModalPlantillaImplantacion() {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarPlantillaImplantacion/Index",
                method: "POST",
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();