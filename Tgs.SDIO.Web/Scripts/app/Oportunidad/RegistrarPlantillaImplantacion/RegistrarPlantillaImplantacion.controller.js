﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('RegistrarPlantillaImplantacion', RegistrarPlantillaImplantacion);

    RegistrarPlantillaImplantacion.$inject =
        [
        'MaestraService',
        'ServicioGrupoService',
        'MedioService',
        'CostoService',
        'OportunidadFlujoCajaService',
        'PlantillaImplantacionService',
        'blockUI',
        'UtilsFactory',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        '$timeout',
        'MensajesUI',
        'URLS',
        '$scope',
        '$compile',
        '$injector'
        ];

    function RegistrarPlantillaImplantacion
        (
        MaestraService,
        ServicioGrupoService,
        MedioService,
        CostoService,
        OportunidadFlujoCajaService,
        PlantillaImplantacionService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile,
        $injector
        ) {

        var vm = this;

        vm.Id = 0;
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;
        vm.IdFlujoCaja = $scope.$parent.vm.IdFlujoCaja;
        vm.IdTipoEnlaceCircuitoDatos = '-1';
        vm.IdMedioCircuitoDatos = '-1';
        vm.IdServicioGrupoCircuitoDatos = '-1';
        vm.IdTipoCircuitoDatos = '-1';
        vm.NumeroCircuitoDatos = '';
        vm.IdCostoCircuitoDatos = '-1';
        vm.ConectadoCircuitoDatos = '';
        vm.LargaDistanciaNacionalCircuitoDatos = '';
        vm.IdVozTos5CaudalAntiguo = '-1';
        vm.IdVozTos4CaudalAntiguo = '-1';
        vm.IdVozTos3CaudalAntiguo = '-1';
        vm.IdVozTos2CaudalAntiguo = '-1';
        vm.IdVozTos1CaudalAntiguo = '-1';
        vm.IdVozTos0CaudalAntiguo = '-1';
        vm.UltimaMillaCircuitoDatos = '';
        vm.VrfCircuitoDatos = '';
        vm.EquipoCpeCircuitoDatos = '';
        vm.EquipoTerminalCircuitoDatos = '';
        vm.IdAccionIsis = '-1';
        vm.IdTipoEnlaceServicioOfertado = '-1';
        vm.IdMedioServicioOfertado = '-1';
        vm.IdServicioGrupoServicioOfertado = '-1';
        vm.IdCostoServicioOfertado = '-1';
        vm.IdTipoServicioOfertado = '-1';
        vm.NumeroServicioOfertado = '';
        vm.LargaDistanciaNacionalServicioOfertado = '';
        vm.IdVozTos5ServicioOfertado = '-1';
        vm.IdVozTos4ServicioOfertado = '-1';
        vm.IdVozTos3ServicioOfertado = '-1';
        vm.IdVozTos2ServicioOfertado = '-1';
        vm.IdVozTos1ServicioOfertado = '-1';
        vm.IdVozTos0ServicioOfertado = '-1';
        vm.UltimaMillaServicioOfertado = '';
        vm.VrfServicioOfertado = '';
        vm.EquipoCpeServicioOfertado = '';
        vm.ObservacionServicioOfertado = '';
        vm.EquipoTerminalServicioOfertado = '';
        vm.GarantizadoBw = '';
        vm.Propiedad = '';
        vm.RecursoTransporte = '';
        vm.RouterSwitchCentralTelefonica = '';
        vm.ComponentesRouter = '';
        vm.TipoAntena = '';
        vm.SegmentoSatelital = '';
        vm.CoordenadasUbicacion = '';
        vm.PozoTierra = '';
        vm.Ups = '';
        vm.EquipoTelefonico = '';
        vm.IdEstado = 1;
        vm.enabledCD = true;
        vm.enabledSO = true;

        vm.ListTipoEnlaceCD = [];
        vm.ListTipoEnlaceSO = [];
        vm.ListTipoCD = [];
        vm.ListTipoSO = [];
        vm.ListServiciosGrupoCD = [];
        vm.ListServiciosGrupoSO = [];
        vm.ListMedioCD = [];
        vm.ListMedioSO = [];
        vm.ListBwCD = [];
        vm.ListVozTos5CA = [];
        vm.ListVozTos4CA = [];
        vm.ListVozTos3CA = [];
        vm.ListVozTos2CA = [];
        vm.ListVozTos1CA = [];
        vm.ListVozTos0CA = [];
        vm.ListAccionIsis = [];

        vm.ListarCostoCD = ListarCostoCD;
        vm.ListarCostoSO = ListarCostoSO;
        vm.RegistrarPlantillaImplantacion = RegistrarPlantillaImplantacion;
        vm.SelectIndexCD = SelectIndexCD;
        vm.SelectIndexSO = SelectIndexSO;

        /* Inicio: Load */
        LlenarCombos();
        /* Fin: Load */

        function LlenarCombos() {
            ListarPrincipalCircuitoDigital();
            ListarTipoServicio();
            ListarServicioGrupo();
            ListarMedio();
            ListarCostoCD();
            ListarAccionIsis();
            //ListarCostoSO();
            ObtenerServicioPorIdFlujoCaja();
        }

        function ListarPrincipalCircuitoDigital() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 89
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoEnlaceCD = UtilsFactory.AgregarItemSelect(Respuesta);
                vm.ListTipoEnlaceSO = vm.ListTipoEnlaceCD;

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarTipoServicio() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 540
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoCD = UtilsFactory.AgregarItemSelect(Respuesta);
                vm.ListTipoSO = vm.ListTipoCD;

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarServicioGrupo() {
            var grupo = {};
            var promise = ServicioGrupoService.ListarServicioGrupo(grupo);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListServiciosGrupoCD = UtilsFactory.AgregarItemSelect(Respuesta);
                vm.ListServiciosGrupoSO = vm.ListServiciosGrupoCD;

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarMedio() {
            var medio = { IdEstado: 1 };
            var promise = MedioService.ListarMedio(medio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListMedioCD = UtilsFactory.AgregarItemSelect(Respuesta);
                vm.ListMedioSO = vm.ListMedioCD;

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarCostoCD() {
            var dto = {
                IdMedio: vm.IdMedioCircuitoDatos,
                IdServicioGrupo: vm.IdServicioGrupoCircuitoDatos,
                IdTipoCosto: 3,
                IdEstado: 1
            };
            var promise = CostoService.ListarCostoPorMedioServicioGrupo(dto);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListBwCD = UtilsFactory.AgregarItemSelectCosto(Respuesta);
                vm.ListVozTos5CA = vm.ListBwCD;
                vm.ListVozTos4CA = vm.ListBwCD;
                vm.ListVozTos3CA = vm.ListBwCD;
                vm.ListVozTos2CA = vm.ListBwCD;
                vm.ListVozTos1CA = vm.ListBwCD;
                vm.ListVozTos0CA = vm.ListBwCD;

                vm.LargaDistanciaNacionalCircuitoDatos = '';
                vm.IdCostoCircuitoDatos = '-1';
                vm.IdVozTos5CaudalAntiguo = '-1';
                vm.IdVozTos4CaudalAntiguo = '-1';
                vm.IdVozTos3CaudalAntiguo = '-1';
                vm.IdVozTos2CaudalAntiguo = '-1';
                vm.IdVozTos1CaudalAntiguo = '-1';
                vm.IdVozTos0CaudalAntiguo = '-1';

                if (vm.IdServicioGrupoCircuitoDatos == 2 ||
                    vm.IdServicioGrupoCircuitoDatos == 3) {
                    vm.enabledCD = false;
                } else {
                    vm.enabledCD = true;
                }

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarAccionIsis() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 543
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListAccionIsis = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarCostoSO() {
            var dto = {
                IdMedio: vm.IdMedioServicioOfertado,
                IdServicioGrupo: vm.IdServicioGrupoServicioOfertado,
                IdTipoCosto: 3,
                IdEstado: 1
            };
            var promise = CostoService.ListarCostoPorMedioServicioGrupo(dto);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListBwSO = UtilsFactory.AgregarItemSelectCosto(Respuesta);
                vm.ListVozTos5SO = vm.ListBwSO;
                vm.ListVozTos4SO = vm.ListBwSO;
                vm.ListVozTos3SO = vm.ListBwSO;
                vm.ListVozTos2SO = vm.ListBwSO;
                vm.ListVozTos1SO = vm.ListBwSO;
                vm.ListVozTos0SO = vm.ListBwSO;

                vm.IdVozTos5ServicioOfertado = '-1';
                vm.IdVozTos4ServicioOfertado = '-1';
                vm.IdVozTos3ServicioOfertado = '-1';
                vm.IdVozTos2ServicioOfertado = '-1';
                vm.IdVozTos1ServicioOfertado = '-1';
                vm.IdVozTos0ServicioOfertado = '-1';

                if (vm.IdServicioGrupoServicioOfertado == 2 ||
                    vm.IdServicioGrupoServicioOfertado == 3) {
                    vm.enabledSO = false;
                } else {
                    vm.enabledSO = true;
                }

            }, function (response) {
                blockUI.stop();
            });
        }

        function RegistrarPlantillaImplantacion() {
            if (!ValidarCampos())
                return;

            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();

                var dto = {
                    IdOportunidad: vm.IdOportunidad,
                    IdTipoEnlaceCircuitoDatos: vm.IdTipoEnlaceCircuitoDatos,
                    IdMedioCircuitoDatos: vm.IdMedioCircuitoDatos,
                    IdServicioGrupoCircuitoDatos: vm.IdServicioGrupoCircuitoDatos,
                    IdTipoCircuitoDatos: vm.IdTipoCircuitoDatos,
                    NumeroCircuitoDatos: vm.NumeroCircuitoDatos,
                    IdCostoCircuitoDatos: vm.IdCostoCircuitoDatos,
                    ConectadoCircuitoDatos: vm.ConectadoCircuitoDatos,
                    LargaDistanciaNacionalCircuitoDatos: vm.LargaDistanciaNacionalCircuitoDatos,
                    IdVozTos5CaudalAntiguo: vm.IdVozTos5CaudalAntiguo,
                    IdVozTos4CaudalAntiguo: vm.IdVozTos4CaudalAntiguo,
                    IdVozTos3CaudalAntiguo: vm.IdVozTos3CaudalAntiguo,
                    IdVozTos2CaudalAntiguo: vm.IdVozTos2CaudalAntiguo,
                    IdVozTos1CaudalAntiguo: vm.IdVozTos1CaudalAntiguo,
                    IdVozTos0CaudalAntiguo: vm.IdVozTos0CaudalAntiguo,
                    UltimaMillaCircuitoDatos: vm.UltimaMillaCircuitoDatos,
                    VrfCircuitoDatos: vm.VrfCircuitoDatos,
                    EquipoCpeCircuitoDatos: vm.EquipoCpeCircuitoDatos,
                    EquipoTerminalCircuitoDatos: vm.EquipoTerminalCircuitoDatos,
                    IdAccionIsis: vm.IdAccionIsis,
                    IdTipoEnlaceServicioOfertado: vm.IdTipoEnlaceServicioOfertado,
                    IdMedioServicioOfertado: vm.IdMedioServicioOfertado,
                    IdServicioGrupoServicioOfertado: vm.IdServicioGrupoServicioOfertado,
                    IdCostoServicioOfertado: vm.IdCostoServicioOfertado,
                    IdTipoServicioOfertado: vm.IdTipoServicioOfertado,
                    NumeroServicioOfertado: vm.NumeroServicioOfertado,
                    LargaDistanciaNacionalServicioOfertado: vm.LargaDistanciaNacionalServicioOfertado,
                    IdVozTos5ServicioOfertado: vm.IdVozTos5ServicioOfertado,
                    IdVozTos4ServicioOfertado: vm.IdVozTos4ServicioOfertado,
                    IdVozTos3ServicioOfertado: vm.IdVozTos3ServicioOfertado,
                    IdVozTos2ServicioOfertado: vm.IdVozTos2ServicioOfertado,
                    IdVozTos1ServicioOfertado: vm.IdVozTos1ServicioOfertado,
                    IdVozTos0ServicioOfertado: vm.IdVozTos0ServicioOfertado,
                    UltimaMillaServicioOfertado: vm.UltimaMillaServicioOfertado,
                    VrfServicioOfertado: vm.VrfServicioOfertado,
                    EquipoCpeServicioOfertado: vm.EquipoCpeServicioOfertado,
                    ObservacionServicioOfertado: vm.ObservacionServicioOfertado,
                    EquipoTerminalServicioOfertado: vm.EquipoTerminalServicioOfertado,
                    GarantizadoBw: vm.GarantizadoBw,
                    Propiedad: vm.Propiedad,
                    RecursoTransporte: vm.RecursoTransporte,
                    RouterSwitchCentralTelefonica: vm.RouterSwitchCentralTelefonica,
                    ComponentesRouter: vm.ComponentesRouter,
                    TipoAntena: vm.TipoAntena,
                    SegmentoSatelital: vm.SegmentoSatelital,
                    CoordenadasUbicacion: vm.CoordenadasUbicacion,
                    PozoTierra: vm.PozoTierra,
                    Ups: vm.Ups,
                    EquipoTelefonico: vm.EquipoTelefonico,
                    IdEstado: vm.IdEstado
                };

                var promise = PlantillaImplantacionService.RegistrarPlantillaImplantacion(dto);

                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    vm.Id = Respuesta.Id;
                    UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'success', Respuesta.Mensaje, 5);
                    $scope.$parent.vm.ListarPlantillaImplantacion();
                    $("#ModalRegistrarPlantillaImplantacion").modal('hide');

                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'danger', MensajesUI.DatosError, 5);
                });
            }
        }

        function ObtenerServicioPorIdFlujoCaja() {
            var dto = { IdFlujoCaja: vm.IdFlujoCaja };
            var promise = OportunidadFlujoCajaService.ObtenerServicioPorIdFlujoCaja(dto);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.IdTipoEnlaceServicioOfertado = Respuesta.IdTipoEnlace;
                vm.IdServicioGrupoServicioOfertado = Respuesta.IdServicioGrupo;
                vm.IdMedioServicioOfertado = Respuesta.IdMedio;
                ListarCostoSO();
                vm.IdCostoServicioOfertado = Respuesta.IdCosto;
                vm.LargaDistanciaNacionalServicioOfertado = Respuesta.Descripcion;

            }, function (response) {
                blockUI.stop();
            });
        }

        function SelectIndexCD() {
            vm.LargaDistanciaNacionalCircuitoDatos = '';
            var select = document.getElementById('ddlCD');
            if (select.selectedIndex != 0) {
                var text = select.options[select.selectedIndex].innerText;
                vm.LargaDistanciaNacionalCircuitoDatos = text;
            }
        }

        function SelectIndexSO() {
            vm.LargaDistanciaNacionalServicioOfertado = '';
            var select = document.getElementById('ddlSO');
            if (select.selectedIndex != 0) {
                var text = select.options[select.selectedIndex].innerText;
                vm.LargaDistanciaNacionalServicioOfertado = text;
            }
        }

        function ValidarCampos() {
            if (vm.IdTipoEnlaceCircuitoDatos == undefined || vm.IdTipoEnlaceCircuitoDatos == '-1') {
                UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'warning', 'Seleccione el tipo de enlace para el circuito datos', 5);
                return;
            }

            if (vm.IdServicioGrupoCircuitoDatos == undefined || vm.IdServicioGrupoCircuitoDatos == '-1') {
                UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'warning', 'Seleccione el tipo servicio para el circuito datos', 5);
                return;
            }

            if (vm.IdMedioCircuitoDatos == undefined || vm.IdMedioCircuitoDatos == '-1') {
                UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'warning', 'Seleccione el medio para el circuito datos', 5);
                return;
            }

            if (vm.IdCostoCircuitoDatos == undefined || vm.IdCostoCircuitoDatos == '-1') {
                UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'warning', 'Seleccione el BW para el circuito datos', 5);
                return;
            }

            if (vm.IdTipoCircuitoDatos == undefined || vm.IdTipoCircuitoDatos == '-1') {
                UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'warning', 'Seleccione el tipo de CD para el circuito datos', 5);
                return;
            }

            if (vm.IdAccionIsis == undefined || vm.IdAccionIsis == '-1') {
                UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'warning', 'Seleccione la Acción ISIS', 5);
                return;
            }

            if (vm.IdTipoEnlaceServicioOfertado == undefined || vm.IdTipoEnlaceServicioOfertado == '-1') {
                UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'warning', 'Seleccione el tipo de enlace para el servicio ofertado', 5);
                return;
            }

            if (vm.IdServicioGrupoServicioOfertado == undefined || vm.IdServicioGrupoServicioOfertado == '-1') {
                UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'warning', 'Seleccione el tipo servicio para el servicio ofertado', 5);
                return;
            }

            if (vm.IdMedioServicioOfertado == undefined || vm.IdMedioServicioOfertado == '-1') {
                UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'warning', 'Seleccione el medio para el servicio ofertado', 5);
                return;
            }

            if (vm.IdCostoServicioOfertado == undefined || vm.IdCostoServicioOfertado == '-1') {
                UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'warning', 'Seleccione el BW para el servicio ofertado', 5);
                return;
            }

            if (vm.IdTipoServicioOfertado == undefined || vm.IdTipoServicioOfertado == '-1') {
                UtilsFactory.Alerta('#divAlertPlantillaImplantacion', 'warning', 'Seleccione el tipo de CD para el servicio ofertado', 5);
                return;
            }

            return true;
        }
    }
})();