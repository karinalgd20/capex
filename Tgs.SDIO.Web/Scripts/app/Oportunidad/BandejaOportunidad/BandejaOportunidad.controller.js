﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('BandejaOportunidad', BandejaOportunidad);

    BandejaOportunidad.$inject = ['BandejaOportunidadService', 'RegistrarOportunidadService', 'ClienteService', 'MaestraService','LineaNegocioService', 'blockUI', 'UtilsFactory', '$timeout', 'MensajesUI', 'URLS', '$scope', '$injector'];

          function BandejaOportunidad(BandejaOportunidadService,RegistrarOportunidadService, ClienteService , MaestraService,LineaNegocioService , blockUI, UtilsFactory, $timeout, MensajesUI, $urls, $scope, $injector) {

        var vm = this;
        vm.Tamanio=10;
        vm.Indice=1;
        var total = 0;
        var totalPage = 0;

        if (jsonListOportunidadCabecera.TotalItemCount>0)
        {
            totalPage=jsonListOportunidadCabecera.TotalItemCount % 10;
            total = Math.trunc(jsonListOportunidadCabecera.TotalItemCount / 10);

            if (total > 0) {
                $scope.total = total ;
                if (totalPage > 0) {
                    $scope.total = $scope.total + 1;
                }
            }
            else {
                $scope.total = 1;
            }
         //   $scope.total = jsonListOportunidadCabecera.TotalItemCount;
        }
     
        $scope.DoCtrlPagingAct = function (text, page, pageSize, total) {

           // vm.Tamanio = pageSize;
            vm.Tamanio = 10;
            vm.Indice = page;

            BuscarOportunidad();
        };
        //<a class="btn btn-sm btn-primary btn-space right" href="{{vm.EnlaceRegistrar}}0"><span class="glyphicon glyphicon-list-alt"></span> Nuevo</a>
        vm.Nuevo = $urls.ApiOportunidad + "OportunidadContenedor/Index/";
        vm.NuevaVersion = NuevaVersion;
        vm.SelectedFiles = "";
        vm.Version = Version;
        vm.DetalleVersiones=true;
        vm.BuscarOportunidad = BuscarOportunidad;
        vm.EliminarOportunidad = EliminarOportunidad;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.DescripcionSegmento = '';
        vm.Descripcion = '';
        vm.DescripcionCliente = '';
        vm.IdOportunidad = "";
        vm.MensajeDeAlerta = "";
        vm.Progres = 0;
        vm.SelectCliente = SelectCliente;
        vm.SelectGerente = SelectGerente;
        vm.SelectDireccion = SelectDireccion;     
        vm.DatosPorVersion = DatosPorVersion;
        vm.RegistrarVersionOportunidad = RegistrarVersionOportunidad;
        vm.OportunidadInactivar = OportunidadInactivar;
        vm.OportunidadGanador = OportunidadGanador;
        vm.ListVersiones = [];
        vm.numSalesForce = "";
        vm.setPage = setPage;
        vm.pageChanged = pageChanged;
        vm.setItemsPerPage = setItemsPerPage;

    
        vm.ListOportunidadCabecera = [];
        vm.ListOportunidadCabecera = jsonListOportunidadCabecera.ListOportunidadPaginadoDtoResponse;
        vm.ListLineaNegocio = [];
        vm.ListMaestra = [];
        vm.ListCliente = [];

        vm.DescripcionOportunidad = [];

        vm.ListGerente = [];
        vm.ListDireccion = [];



        vm.fillTextbox = fillTextbox;
        vm.IdCliente = 0;

        vm.NumeroSalesForce = "";
        //vm.IdGerente = "-1";
        //vm.IdDireccion = "-1";
        //vm.IdUsuario = "-1";

        vm.IdUsuario = 0;
        //vm.IdGerente = 0;
        //vm.IdDireccion = 0;
    


        vm.FechaCreacion = "";
        vm.FechaEdicion = "";
        vm.IdEstadoOportunidad = "-1";
        vm.IdLineaNegocio = "-1";

        vm.OptionPreVenta = "";
        vm.OptionPreVenta = !jsonOptionPreVenta;

        //vm.viewby = 10;
        //vm.totalItems = vm.ListOportunidadCabecera.length;
        //vm.currentPage = 1;
        //vm.itemsPerPage = vm.viewby;
        //vm.maxSize = 4;
        vm.Oportunidad = "";
        vm.NomCompletUsuCrea = "";
        vm.NomCompletUsuMod = "";
        vm.NomCompletPreventa = "";
        vm.IdOportunidad = 0;
        //acciones
        listarEstadoOportunidad();
        listarLineaNegocio();
            //vm.ListDireccion = SelectDireccion(jsonListDireccion);

            //vm.ListGerente = SelectGerente(jsonListGerente);
        function listarEstadoOportunidad() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 11

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListMaestra = UtilsFactory.AgregarItemSelect(Respuesta);
                vm.IdEstadoOportunidad = "-1";
            }, function (response) {
                blockUI.stop();
            });
        }
        function listarLineaNegocio() {

            var lineaNegocio = {
                IdEstado: 1,
              

            };
            var promise = LineaNegocioService.ListarLineaNegocios(lineaNegocio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);
                vm.IdLineaNegocio = "-1";
            }, function (response) {
                blockUI.stop();
            });
        }
        function NuevaVersion(Id) {

          var oportunidad = {
                IdOportunidad: Id,

            };
          var promise = RegistrarOportunidadService.NuevaVersion(oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                UtilsFactory.Alerta('#divAlert', 'success', 'Se Registro Nueva Version', 5);
                BuscarOportunidad();
                vm.DetalleVersiones = false;
                Version(Id);
            }, function (response) {
                blockUI.stop();

            });

        }
        function setPage(pageNo) {
            vm.currentPage = pageNo;
        }
        function pageChanged() {
            //console.log('Page changed to: ' + vm.currentPage);
        }
        function setItemsPerPage(num) {
            vm.itemsPerPage = num;
            vm.currentPage = parseInt(vm.totalItems / 10); //reset to first page
        }
        function EliminarOportunidad(IdOportunidad) {
            vm.btnEliminar = true;
            $("#idArchivo").val("");
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            modalpopupConfirm.modal('show');
            $("#idArchivo").val(IdOportunidad);
        }
        function SelectCliente() {
            if (vm.Descripcion.length > 5) {
                ListarCliente();
            }


        }
        function ListarCliente() {

            var cliente = {
                IdEstado: 1,
                Descripcion: vm.Descripcion,

            };
            var promise = ClienteService.ListarClientePorDescripcion(cliente);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;



                var output = [];
                angular.forEach(Respuesta, function (cliente) {

                    output.push(cliente);

                });
                vm.ListCliente = output;


            }, function (response) {
                blockUI.stop();

            });
        }
        function fillTextbox(string, Id) {

            vm.Descripcion = string;
            vm.IdCliente = Id;
            vm.ListCliente = null;
        
        }
        function RegistrarVersionOportunidad() {

       

            var Oportunidad = {
                IdLineaNegocio: vm.IdOportunidad,

            };
            var promise = BandejaOportunidadService.RegistrarVersionOportunidad(Oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                blockUI.stop();


            }, function (response) {
                blockUI.stop();

            });
        }
        function OportunidadInactivar() {

          

            var Oportunidad = {
                IdLineaNegocio: vm.IdOportunidad,

            };
            var promise = BandejaOportunidadService.OportunidadInactivar(Oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                blockUI.stop();


            }, function (response) {
                blockUI.stop();

            });
        }
        function OportunidadGanador() {

         

            var Oportunidad = {
                IdLineaNegocio: vm.IdOportunidad,

            };
            var promise = BandejaOportunidadService.OportunidadGanador(Oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                blockUI.stop();


            }, function (response) {
                blockUI.stop();

            });
        }
        function BuscarOportunidad() {
            var IdEstado = "";
            IdEstado = vm.IdEstadoOportunidad;
            var IdLineaNegocio="";
            IdLineaNegocio = vm.IdLineaNegocio;
            if (vm.IdEstadoOportunidad == "-1") {
                IdEstado = 0;
            }
            if (vm.IdLineaNegocio == "-1") {
                IdLineaNegocio = 5;
            }
            //if (vm.IdUsuario == "-1") {
            //    vm.IdUsuario = 0;
            //}
            if (vm.Descripcion == "") {
                vm.IdCliente = 0;
            }


            var oportunidad = {
                IdLineaNegocio: IdLineaNegocio,
                IdCliente: vm.IdCliente,
                Oportunidad: vm.Descripcion,
                FechaCreacion:vm.FechaCreacion,
                FechaEdicion:vm.FechaEdicion,
                NumeroSalesForce: vm.NumeroSalesForce,
                Tamanio: vm.Tamanio,
                Indice:vm.Indice,
                IdEstado: IdEstado
            };
            var promise = BandejaOportunidadService.ListarOportunidadCabecera(oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListOportunidadCabecera = Respuesta.ListOportunidadPaginadoDtoResponse;

            var totalPage = Respuesta.TotalItemCount % 10;
                var total = Math.trunc(Respuesta.TotalItemCount / 10);

            if (total > 0) {
                $scope.total = total ;
                if (totalPage > 0) {
                    $scope.total = $scope.total + 1;
                }
            }
            else {
                $scope.total = 1;
            }

            }, function (response) {


            });
        }
        function BuscarOportunidadGanadora() {
            var IdLineaNegocio = "";
            IdLineaNegocio = vm.IdLineaNegocio;
            
            if (vm.IdLineaNegocio == "-1") {
                IdLineaNegocio = 5;
            }

            if (vm.Descripcion == "") {
                vm.IdCliente = 0;
            }

            var oportunidad = {
                IdLineaNegocio: IdLineaNegocio,
                IdCliente: vm.IdCliente,
                Oportunidad: vm.Descripcion,
                FechaCreacion: vm.FechaCreacion,
                FechaEdicion: vm.FechaEdicion,
                NumeroSalesForce: vm.NumeroSalesForce,
                Tamanio: vm.Tamanio,
                Indice: vm.Indice,
                IdEstado: 6
            };
            var promise = BandejaOportunidadService.ListarOportunidadCabecera(oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListOportunidadCabecera = Respuesta.ListOportunidadPaginadoDtoResponse;
                $scope.total = Respuesta.TotalItemCount;
                blockUI.stop();

            }, function (response) {
                blockUI.stop();

            });
        }
        function Version(Id) {
            // blockUI.start();

            var Oportunidad = {
                IdOportunidad: Id
            };
            var promise = BandejaOportunidadService.ObtenerVersiones(Oportunidad);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListVersiones = Respuesta;
                vm.DetalleVersiones=false;
                $("#itemProyecto").css("background", "white");

                
                //var estado = "";
                //switch (data[key][0].IdEstado) {
                //    case 0:
                //        estado = "Eliminado";
                //        document.getElementById("acciones").style.display = "none";
                //        break;
                //    case 1:
                //        estado = "En Proceso"
                //        document.getElementById("acciones").style.display = "block";
                //        break;
                //    case 2:
                //        estado = "Ganador"
                //        document.getElementById("acciones").style.display = "none";
                //        break;
                //    case 3:
                //        estado = "Desestimado"
                //        document.getElementById("acciones").style.display = "none";
                //        break;
                //}
                //document.getElementById("Estado").innerHTML = estado;

                vm.DescripcionOportunidad = Respuesta.slice(0, 1);
                var dato = vm.DescripcionOportunidad;

                vm.IdOportunidad = dato[0].IdOportunidad;
                vm.Oportunidad = dato[0].Descripcion;

                vm.NomCompletPreventa = dato[0].NomCompletPreventa;
                vm.NomCompletProductManager = dato[0].NomCompletProductManager;
                vm.NomCompletAnalistaFinanciero = dato[0].NomCompletAnalistaFinanciero;
              
                vm.IdEstadoVersion = dato[0].IdEstado;
                ListarMaestraPorValor(vm.IdEstadoVersion);
               //
             
              
                $("#itemProyecto-"+Id).css("background", "red");

                $('#pie-chart').remove();
                GraficoOibda(dato[0].IdOportunidad);
                //     blockUI.stop();

            }, function (response) {
                blockUI.stop();

            });
        }
        function ListarMaestraPorValor(Id) {
            var maestra = {
                IdEstado: 1,
                Valor: Id,
                IdRelacion: 11

            };
            var promise = MaestraService.ListarMaestraPorValor(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.DescripcionEstado = Respuesta[0].Descripcion;
            }, function (response) {
                blockUI.stop();

            });
        }

        function DatosPorVersion(Id) {
            // blockUI.start();
            var Oportunidad = {
                IdOportunidad: Id
            };
            var promise = BandejaOportunidadService.DatosPorVersion(Oportunidad);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                // vm.ListVersiones = Respuesta;

                //var estado = "";
                //switch (data[key][0].IdEstado) {
                //    case 0:
                //        estado = "Eliminado";
                //        document.getElementById("acciones").style.display = "none";
                //        break;
                //    case 1:
                //        estado = "En Proceso"
                //        document.getElementById("acciones").style.display = "block";
                //        break;
                //    case 2:
                //        estado = "Ganador"
                //        document.getElementById("acciones").style.display = "none";
                //        break;
                //    case 3:
                //        estado = "Desestimado"
                //        document.getElementById("acciones").style.display = "none";
                //        break;
                //}
                //document.getElementById("Estado").innerHTML = estado;

                vm.Oportunidad = Respuesta.Descripcion;
                vm.NomCompletPreventa = Respuesta.NomCompletPreventa;
                vm.NomCompletUsuCrea = Respuesta.NomCompletUsuCrea;
                vm.NomCompletUsuMod = Respuesta.NomCompletUsuMod;

                vm.IdEstadoVersion = Respuesta.IdEstado;
                ListarMaestraPorValor(vm.IdEstadoVersion);
                GraficoOibda(Id);




                //     blockUI.stop();

            }, function (response) {
                blockUI.stop();

            });
        }
        function GraficoOibda(Id) {
            // blockUI.start();


            document.getElementById("demo").innerHTML = " <div id='pie-chart'></div>";
            var Oportunidad = {
                IdOportunidad: Id
            };
            var promise = BandejaOportunidadService.GraficoOibda(Oportunidad);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                var array = [];
                var i = 0;

                for (i = 0; i < Respuesta.length; i++) {
                    //  array[i] += Respuesta[i];

                    array.push(Respuesta[i].Color);
                }


                Morris.Donut({
                    element: 'pie-chart',
                    data: Respuesta,
                    colors: array
                });
                vm
                //     blockUI.stop();

            }, function (response) {
                blockUI.stop();

            });
        }
        function LimpiarFiltros() {
            vm.IdLineaNegocio = '-1';
            vm.NumeroSalesForce = "";
            vm.FechaCreacion = "";
            vm.FechaEdicion = "";
            vm.IdUsuario = "-1";
            vm.IdDireccion = "-1";
            vm.Descripcion = "";
            vm.IdEstadoOportunidad = "-1";
        
        }
        function SelectGerente(data) {
            var lista = data;
            var item = { "IdUsuario": 0, "NombresCompletos": "--Seleccione--" };
            lista.splice(0, 0, item);
            return lista;

        };
        function SelectDireccion(data) {
            var lista = data;
            var item = { "IdDireccion": 0, "Descripcion": "--Seleccione--" };
            lista.splice(0, 0, item);
            return lista;

        };
        function ToJavaScriptDate(value) { //To Parse Date from the Returned Parsed Date
            var pattern = /Date\(([^)]+)\)/;
            var results = pattern.exec(value);
            var dt = new Date(parseFloat(results[1]));
            return dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear();
        }
    }
})();