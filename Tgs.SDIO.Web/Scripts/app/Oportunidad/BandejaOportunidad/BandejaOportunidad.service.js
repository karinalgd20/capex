﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('BandejaOportunidadService', BandejaOportunidadService);

    BandejaOportunidadService.$inject = ['$http', 'URLS'];

    function BandejaOportunidadService($http, $urls) {

        var service = {
            ListarOportunidadCabecera: ListarOportunidadCabecera,
            ObtenerVersiones: ObtenerVersiones,
            DatosPorVersion: DatosPorVersion,
            GraficoOibda: GraficoOibda,
            RegistrarVersionOportunidad: RegistrarVersionOportunidad,
            OportunidadInactivar: OportunidadInactivar,
            OportunidadGanador: OportunidadGanador
        };
         
        return service;
        function RegistrarVersionOportunidad(oportunidad) {
            return $http({
                url: $urls.ApiOportunidad + "BandejaOportunidad/RegistrarVersionOportunidad",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function OportunidadInactivar(oportunidad) {
            return $http({
                url: $urls.ApiOportunidad + "BandejaOportunidad/OportunidadInactivar",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function OportunidadGanador(oportunidad) {
            return $http({
                url: $urls.ApiOportunidad + "BandejaOportunidad/OportunidadGanador",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListarOportunidadCabecera(request) {
            return $http({
                url: $urls.ApiOportunidad + "BandejaOportunidad/ListarOportunidadCabecera",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ObtenerVersiones(oportunidad) {
            return $http({
                url: $urls.ApiOportunidad + "BandejaOportunidad/ObtenerVersiones",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function DatosPorVersion(oportunidad) {
            return $http({
                url: $urls.ApiOportunidad + "BandejaOportunidad/DatosPorVersion",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function GraficoOibda(oportunidad) {
            return $http({
                url: $urls.ApiOportunidad + "BandejaOportunidad/GraficoOibda",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();