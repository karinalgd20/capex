﻿(function () {
    'use strict',
    angular
        .module('app.Oportunidad')
        .service('OportunidadFlujoCajaService', OportunidadFlujoCajaService);

    OportunidadFlujoCajaService.$inject = ['$http', 'URLS'];

    function OportunidadFlujoCajaService($http, $urls) {
        var service = {
            ListarServiciosAgrupados: ListarServiciosAgrupados,
            ObtenerServicioPorIdFlujoCaja: ObtenerServicioPorIdFlujoCaja
        };

        return service;

        function ListarServiciosAgrupados(request) {
            return $http({
                url: $urls.ApiOportunidad + "OportunidadFlujoCaja/ListarServiciosAgrupados",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ObtenerServicioPorIdFlujoCaja(request) {
            return $http({
                url: $urls.ApiOportunidad + "OportunidadFlujoCaja/ObtenerServicioPorIdFlujoCaja",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }
    }
})();