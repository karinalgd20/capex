﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('RegistrarOportunidadParametroService', RegistrarOportunidadParametroService);

    RegistrarOportunidadParametroService.$inject = ['$http', 'URLS'];

    function RegistrarOportunidadParametroService($http, $urls) {

        var service = {
      
            ModalOportunidadParametro: ModalOportunidadParametro,
            RegistrarOportunidadParametro:RegistrarOportunidadParametro,
            ObtenerOportunidadParametro: ObtenerOportunidadParametro,
            ActualizarOportunidadParametro:ActualizarOportunidadParametro
        };

        return service;
        function ActualizarOportunidadParametro(request) {

            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidadParametro/ActualizarOportunidadParametro",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ModalOportunidadParametro(request) {

            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidadParametro/Index",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
         
          function RegistrarOportunidadParametro(request) {

              return $http({
                  url: $urls.ApiOportunidad + "RegistrarOportunidadParametro/RegistrarOportunidadParametro",
                  method: "POST",
                  data: JSON.stringify(request)
              }).then(DatosCompletados);

              function DatosCompletados(resultado) {
                  return resultado;
              }
          };

          function ObtenerOportunidadParametro(request) {

            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidadParametro/ObtenerOportunidadParametro",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();