﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('RegistrarOportunidadParametro', RegistrarOportunidadParametro);

    RegistrarOportunidadParametro.$inject = ['RegistrarOportunidadParametroService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarOportunidadParametro(RegistrarOportunidadParametroService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.CargaModalOportunidadParametro = CargaModalOportunidadParametro;
        vm.ActualizarOportunidadParametro = ActualizarOportunidadParametro;
        vm.ObtenerOportunidadParametro = ObtenerOportunidadParametro;
      


        CargaModalOportunidadParametro();



        function CargaModalOportunidadParametro() {
            vm.IdOportunidadParametro = $scope.$parent.vm.IdOportunidadParametro;
            (vm.IdOportunidadParametro > 0) ? ObtenerOportunidadParametro() : CargaModal();


        }
        function CargaModal() {
            
           
        }



        function ListarMaestraPorIdRelacion() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 32

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListTipoServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }
        function ListarEstados() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 1

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListEstado = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ObtenerOportunidadParametro() {
            blockUI.start();

            var OportunidadParametro = {
                IdOportunidadParametro: vm.IdOportunidadParametro
            }

            var promise = RegistrarOportunidadParametroService.ObtenerOportunidadParametro(OportunidadParametro);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
               
               vm.Descripcion=Respuesta.Descripcion;
               vm.Valor=Respuesta.Valor



            }, function (response) {
                blockUI.stop();

            });
        }


        function ActualizarOportunidadParametro() {

         var OportunidadParametro = {
                IdOportunidadParametro: vm.IdOportunidadParametro,
                Valor:vm.Valor
            }



         var promise = RegistrarOportunidadParametroService.ActualizarOportunidadParametro(OportunidadParametro);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                UtilsFactory.Alerta('#divAlert_RegistrarOportunidadParametro', 'success', "Se actualizo con exito registro.", 5);

                  $scope.$parent.vm.BuscarParametro();
            }, function (response) {
                blockUI.stop();

            });
        }

    




    }
})();








