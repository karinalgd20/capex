﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('EcapexService', EcapexService);

    EcapexService.$inject = ['$http', 'URLS'];

    function EcapexService($http, $urls) {

        var service = {
            ListarECapex: ListarECapex,
            VistaEcapex: VistaEcapex,
            ListarPestanaGrupoTipo:ListarPestanaGrupoTipo
        };

        return service;

        function ListarECapex(dto) {
            return $http({
                url: $urls.ApiOportunidad + "ECapex/ListarCapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function VistaEcapex() {
            return $http({
                url: $urls.ApiOportunidad + "ECapex/Index",
                method: "GET"
            }).then(DatosCompletados);
 
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarPestanaGrupoTipo(dto) {
            return $http({
                url: $urls.ApiOportunidad + "ECapex/ListarPestanaGrupoTipo",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
 
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();