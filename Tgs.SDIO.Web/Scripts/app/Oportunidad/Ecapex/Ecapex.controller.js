﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('ECapexController', ECapexController);

    ECapexController.$inject = ['EcapexService', 'EstudiosEspecialesService', 'FlujoCajaService', 'EquiposEspecialesService',
        'LineaNegocioService', 'RoutersService', 'ModemsService', 'EquipoSeguridadService', 'HardwareService', 'SoftwareService',
        'GabineteService', 'GhzService', 'SolarWindService', 'SmartVpnService', 'SatelitalesService','OportunidadContenedorService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function ECapexController(EcapexService, EstudiosEspecialesService, FlujoCajaService, EquiposEspecialesService,
        LineaNegocioService, RoutersService, ModemsService, EquipoSeguridadService, HardwareService, SoftwareService,
        GabineteService, GhzService, SolarWindService, SmartVpnService, SatelitalesService,OportunidadContenedorService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.BuscarEstudiosEspeciales = BuscarEstudiosEspeciales;
        vm.BuscarEquiposEspeciales = BuscarEquiposEspeciales;
        vm.BuscarRouter = BuscarRouter;
        vm.BuscarModems = BuscarModems;
        vm.BuscarEquipoSeguridad = BuscarEquipoSeguridad;
        vm.BuscarHardware = BuscarHardware;
        vm.BuscarSoftware = BuscarSoftware;
        vm.BuscarGabinetes = BuscarGabinetes;
        vm.BuscarGhz = BuscarGhz;
        vm.BuscarSolarWind = BuscarSolarWind;
        vm.BuscarSmartVPN = BuscarSmartVPN;
        vm.BuscarSatelitales = BuscarSatelitales;

        vm.CargaModalEstudiosEspeciales = CargaModalEstudiosEspeciales;
        vm.CargaModalEquiposEspeciales = CargaModalEquiposEspeciales;
        vm.CargaModalRouter = CargaModalRouter;
        vm.CargaModalModems = CargaModalModems;
        vm.CargaModalEquipoSeguridad = CargaModalEquipoSeguridad;
        vm.CargaModalHardware = CargaModalHardware;
        vm.CargaModalSoftware = CargaModalSoftware;
        vm.CargaModalGabinete = CargaModalGabinete;
        vm.CargaModalGhz = CargaModalGhz;
        vm.CargaModalSolarWind = CargaModalSolarWind;
        vm.CargaModalSmart = CargaModalSmart;
        vm.CargaModalSatelitales = CargaModalSatelitales;
        vm.Oportunidad = $scope.$parent.vm.ObjOportunidad;
        vm.IdOportunidadLineaNegocio = vm.Oportunidad.IdOportunidadLineaNegocio;
        // vm.IdOportunidad = vm.Oportunidad.IdOportunidadLineaNegocio;
        vm.dto = null;
        vm.BuscarEcapex = BuscarEcapex;
        vm.Eliminar = Eliminar;


        // $scope.$parent.vm.TabPorPerfilOportunidadDatos();
       // vm.EditarDatoTable = $scope.$parent.vm.EditarDatoTable;
          vm.EditarDatoTable = true;
          TabPorPerfilOportunidadDatos();
          function TabPorPerfilOportunidadDatos() {


              var promise = OportunidadContenedorService.TabPorPerfilOportunidadDatos();

              promise.then(function (resultado) {
                  blockUI.stop();
                  var Respuesta = resultado.data;


                  vm.EditarDatoTable = !Respuesta.EditarDatoTable;

              }, function (response) {
                  blockUI.stop();

              });
          }
        /******************************************* Estudios Especiales *******************************************/



        function Eliminar(IdGrupo, IdSubServicioDatosCapex) {

            var objFlujoCaja = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdGrupo: IdGrupo
            };

            if (confirm('¿Estas seguro que desea eliminar?')) {

                var promise = FlujoCajaService.InhabilitarOportunidadEcapex(objFlujoCaja);
                promise.then(function (response) {

                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        blockUI.stop();

                    } else {

                        //  UtilsFactory.Alerta('#lblAlerta_EstudiosEspeciales', 'success', Respuesta.Mensaje, 10);


                        if (IdGrupo == 6) {
                            blockUI.stop(); BuscarEstudiosEspeciales();
                            UtilsFactory.Alerta('#lblAlerta_EstudiosEspeciales', 'success', 'Se Elimino Correctamente', 10);
                        }
                        if (IdGrupo == 7) {
                            blockUI.stop(); BuscarEquiposEspeciales();
                            UtilsFactory.Alerta('#lblAlerta_EquiposEspeciales', 'success', 'Se Elimino Correctamente', 10);
                        }
                        if (IdGrupo == 8) {
                            blockUI.stop(); BuscarRouter();
                            UtilsFactory.Alerta('#lblAlerta_Router', 'success', 'Se Elimino Correctamente', 10);
                        }
                        if (IdGrupo == 9) {
                            blockUI.stop(); BuscarModems();
                            UtilsFactory.Alerta('#lblAlerta_Modems', 'success', 'Se Elimino Correctamente', 10);
                        }
                        if (IdGrupo == 10) {
                            blockUI.stop(); BuscarEquipoSeguridad();
                            UtilsFactory.Alerta('#lblAlerta_EquipoSeguridad', 'success', 'Se Elimino Correctamente', 10);
                        }
                        if (IdGrupo == 11) {
                            blockUI.stop(); BuscarHardware();
                            UtilsFactory.Alerta('#lblAlerta_Hardware', 'success', 'Se Elimino Correctamente', 10);
                        }
                        if (IdGrupo == 12) {
                            blockUI.stop(); BuscarSoftware();
                            UtilsFactory.Alerta('#lblAlerta_Software', 'success', 'Se Elimino Correctamente', 10);
                        }
                        if (IdGrupo == 13) {
                            blockUI.stop(); BuscarGabinetes();
                            UtilsFactory.Alerta('#lblAlerta_Gabinetes', 'success', 'Se Elimino Correctamente', 10);
                        }
                        if (IdGrupo == 14) {
                            blockUI.stop(); BuscarGhz();
                            UtilsFactory.Alerta('#lblAlerta_Ghz', 'success', 'Se Elimino Correctamente', 10);
                        }
                        if (IdGrupo == 15) {
                            blockUI.stop(); BuscarSolarWind();
                            UtilsFactory.Alerta('#lblAlerta_SolarWind', 'success', 'Se Elimino Correctamente', 10);
                        }
                        if (IdGrupo == 16) {
                            blockUI.stop(); BuscarSmartVPN();
                            UtilsFactory.Alerta('#lblAlerta_SmartVPN', 'success', 'Se Elimino Correctamente', 10);
                        }
                        if (IdGrupo == 17) {
                            blockUI.stop(); BuscarSatelitales();
                            UtilsFactory.Alerta('#lblAlerta_Satelitales', 'success', 'Se Elimino Correctamente', 10);
                        }


                    }

                }, function (response) {
                    blockUI.stop();
                    //UtilsFactory.Alerta('#lblAlertaEliminarCaratula', 'danger', MensajesUI.DatosError, 5);
                });



            }
            else {
                blockUI.stop();

            }
        };



        vm.dataMasiva = [];
        vm.dtInstanceEstudiosEspeciales = {};

        vm.dtColumnsEstudiosEspeciales = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCapex').notVisible(),
         DTColumnBuilder.newColumn('IdServicioSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Tipo').notSortable(),
        // DTColumnBuilder.newColumn('Cruce').withTitle('Cruce').notSortable(),
        // DTColumnBuilder.newColumn('AEReducido').withTitle('A.E. Reducido').notSortable(),
        //DTColumnBuilder.newColumn('Circuito').withTitle('Circuito').notSortable(),
         DTColumnBuilder.newColumn('SISEGO').withTitle('Sisego').notSortable(),
        // DTColumnBuilder.newColumn('MesesAntiguedad').withTitle('Antigüedad').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitarioAntiguo').withTitle('C.U. Antiguo').notSortable(),
       //  DTColumnBuilder.newColumn('ValorResidualSoles').withTitle('Valor Residual').notSortable(),
        // DTColumnBuilder.newColumn('CostoUnitario').withTitle('C.U.').notSortable(),
       //  DTColumnBuilder.newColumn('CapexDolares').withTitle('Capex (USD)').notSortable(),
        // DTColumnBuilder.newColumn('CapexSoles').withTitle('Capex Total (S/.)').notSortable(),
         DTColumnBuilder.newColumn('TotalCapex').withTitle('Total Capex').notSortable(),
       //  DTColumnBuilder.newColumn('MesRecupero').withTitle('Mes de RQ (Mes-Año)').notSortable(),
        // DTColumnBuilder.newColumn('MesComprometido').withTitle('Mes de Comprometido').notSortable(),
        // DTColumnBuilder.newColumn('MesCertificado').withTitle('Mes Certificado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaEstudiosEspeciales)

        ];

        function AccionesBusquedaEstudiosEspeciales(data, type, full, meta) {

            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm' id='tab-button' ng-hide=vm.EditarDatoTable  data-toggle='modal' ng-click='vm.CargaModalEstudiosEspeciales(\"" + data.IdSubServicioDatosCapex + "\",\"" + data.IdServicioSubServicio + "\"," + data.IdSubServicio + " );'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-hide=vm.EditarDatoTable ng-click='vm.Eliminar(\"" + 6 + "\"," + data.IdSubServicioDatosCapex + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";

            return respuesta;
        };

        function BuscarEstudiosEspeciales() {
            LimpiarGrillaEstudiosEspeciales();

            $timeout(function () {
                vm.dtOptionsEstudiosEspeciales = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarEstudiosEspecialesPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarEstudiosEspecialesPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var capex = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 6,
                Indice: pageNumber,
                Tamanio: length
            };
            console.log(capex);
            var promise = FlujoCajaService.ListarDetalleOportunidadEcapex(capex);
            promise.then(function (response) {

                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaEstudiosEspeciales();
            });
        };


        /******************************************* Equipos Especiales *******************************************/
        vm.dtInstanceEquiposEspeciales = {};

        vm.dtColumnsEquiposEspeciales = [
         DTColumnBuilder.newColumn('IdSubServicioDatosCapex').notVisible(),
         DTColumnBuilder.newColumn('IdServicioSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Tipo').notSortable(),
         //DTColumnBuilder.newColumn('Cruce').withTitle('Cruce').notSortable(),
      //   DTColumnBuilder.newColumn('AEReducido').withTitle('A.E. Reducido').notSortable(),
       //  DTColumnBuilder.newColumn('Circuito').withTitle('Circuito').notSortable(),
     //    DTColumnBuilder.newColumn('Modelo').withTitle('Modelo').notSortable(),
         DTColumnBuilder.newColumn('MesesAntiguedad').withTitle('Antigüedad').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitarioAntiguo').withTitle('C.U. Antiguo').notSortable(),
         DTColumnBuilder.newColumn('ValorResidualSoles').withTitle('Valor Residual').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitario').withTitle('C.U.').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('Capex (USD)').notSortable(),
         DTColumnBuilder.newColumn('CapexSoles').withTitle('Capex Total (S/.)').notSortable(),
         DTColumnBuilder.newColumn('TotalCapex').withTitle('Total Capex').notSortable(),
        // DTColumnBuilder.newColumn('MesRecupero').withTitle('Mes de RQ (Mes-Año)').notSortable(),
       //  DTColumnBuilder.newColumn('MesComprometido').withTitle('Mes de Comprometido').notSortable(),
      //   DTColumnBuilder.newColumn('MesCertificado').withTitle('Mes Certificado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaEquiposEspeciales)

        ];

        function AccionesBusquedaEquiposEspeciales(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.CargaModalEquiposEspeciales(\"" + data.IdSubServicioDatosCapex + "\",\"" + data.IdServicioSubServicio + "\"," + data.IdSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.Eliminar(\"" + 7 + "\"," + data.IdSubServicioDatosCapex + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarEquiposEspeciales() {

            LimpiarGrillaEquiposEspeciales();

            $timeout(function () {
                vm.dtOptionsEquiposEspeciales = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarEquiposEspecialesPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarEquiposEspecialesPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var capex = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 7,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = FlujoCajaService.ListarDetalleOportunidadEcapex(capex);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaEquiposEspeciales();
            });
        };

        /******************************************* Router *******************************************/
        vm.dtInstanceRouter = {};

        vm.dtColumnsRouter = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCapex').notVisible(),
         DTColumnBuilder.newColumn('IdServicioSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Tipo').notSortable(),
        // DTColumnBuilder.newColumn('Cruce').withTitle('Cruce').notSortable(),
       //  DTColumnBuilder.newColumn('AEReducido').withTitle('A.E. Reducido').notSortable(),
        // DTColumnBuilder.newColumn('Circuito').withTitle('Circuito').notSortable(),
        // DTColumnBuilder.newColumn('Modelo').withTitle('Modelo').notSortable(),
         DTColumnBuilder.newColumn('MesesAntiguedad').withTitle('Antigüedad').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitarioAntiguo').withTitle('C.U. Antiguo').notSortable(),
         DTColumnBuilder.newColumn('ValorResidualSoles').withTitle('Valor Residual').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitario').withTitle('C.U.').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('Capex (USD)').notSortable(),
         DTColumnBuilder.newColumn('CapexSoles').withTitle('Capex Total (S/.)').notSortable(),
         DTColumnBuilder.newColumn('TotalCapex').withTitle('Total Capex').notSortable(),
        //DTColumnBuilder.newColumn('MesRecupero').withTitle('Mes de RQ (Mes-Año)').notSortable(),
        //DTColumnBuilder.newColumn('MesComprometido').withTitle('Mes de Comprometido').notSortable(),
        //DTColumnBuilder.newColumn('MesCertificado').withTitle('Mes Certificado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaRouter)

        ];

        function AccionesBusquedaRouter(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.CargaModalRouter(\"" + data.IdSubServicioDatosCapex + "\",\"" + data.IdServicioSubServicio + "\"," + data.IdSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.Eliminar(\"" + 8 + "\"," + data.IdSubServicioDatosCapex + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarRouter() {

            LimpiarGrillaRouter();

            $timeout(function () {
                vm.dtOptionsRouter = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarRouterPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarRouterPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var capex = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 8,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = FlujoCajaService.ListarDetalleOportunidadEcapex(capex);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaRouter();
            });
        };

        /******************************************* Modems *******************************************/
        vm.dtInstanceModems = {};

        vm.dtColumnsModems = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCapex').notVisible(),
         DTColumnBuilder.newColumn('IdServicioSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Equipo').notSortable(),
        // DTColumnBuilder.newColumn('Cruce').withTitle('Cruce').notSortable(),
        // DTColumnBuilder.newColumn('AEReducido').withTitle('A.E. Reducido').notSortable(),
         ////DTColumnBuilder.newColumn('Circuito').withTitle('Circuito').notSortable(),
        // DTColumnBuilder.newColumn('Modelo').withTitle('Modelo').notSortable(),
         DTColumnBuilder.newColumn('MesesAntiguedad').withTitle('Antigüedad').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitarioAntiguo').withTitle('C.U. Antiguo').notSortable(),
         DTColumnBuilder.newColumn('ValorResidualSoles').withTitle('Valor Residual').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitario').withTitle('C.U.').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('Capex (USD)').notSortable(),
         DTColumnBuilder.newColumn('CapexSoles').withTitle('Capex Total (S/.)').notSortable(),
         DTColumnBuilder.newColumn('TotalCapex').withTitle('Total Capex').notSortable(),
       //  DTColumnBuilder.newColumn('MesRecupero').withTitle('Mes de RQ (Mes-Año)').notSortable(),
        // DTColumnBuilder.newColumn('MesComprometido').withTitle('Mes de Comprometido').notSortable(),
        // DTColumnBuilder.newColumn('MesCertificado').withTitle('Mes Certificado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaModems)

        ];

        function AccionesBusquedaModems(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.CargaModalModems(\"" + data.IdSubServicioDatosCapex + "\",\"" + data.IdServicioSubServicio + "\"," + data.IdSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.Eliminar(\"" + 9 + "\"," + data.IdSubServicioDatosCapex + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarModems() {

            LimpiarGrillaModems();

            $timeout(function () {
                vm.dtOptionsModems = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarModemsPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarModemsPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var capex = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 9,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = FlujoCajaService.ListarDetalleOportunidadEcapex(capex);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaModems();
            });
        };


        /******************************************* Equipo de Seguridad *******************************************/
        vm.dtInstanceESeguridad = {};

        vm.dtColumnsESeguridad = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCapex').notVisible(),
         DTColumnBuilder.newColumn('IdServicioSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Equipo').notSortable(),
        // DTColumnBuilder.newColumn('Cruce').withTitle('Cruce').notSortable(),
        // DTColumnBuilder.newColumn('AEReducido').withTitle('A.E. Reducido').notSortable(),
         //DTColumnBuilder.newColumn('TipoEquipo').withTitle('Tipo de Equipo').notSortable(),
         //DTColumnBuilder.newColumn('Modelo').withTitle('Marca-Modelo').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('MesesAntiguedad').withTitle('Antigüedad').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitarioAntiguo').withTitle('C.U. Antiguo').notSortable(),
         DTColumnBuilder.newColumn('ValorResidualSoles').withTitle('Valor Residual').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitario').withTitle('C.U.').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('Capex (USD)').notSortable(),
         DTColumnBuilder.newColumn('CapexSoles').withTitle('Capex Total (S/.)').notSortable(),
         DTColumnBuilder.newColumn('TotalCapex').withTitle('Total Capex').notSortable(),
         //DTColumnBuilder.newColumn('MesRecupero').withTitle('Mes de RQ (Mes-Año)').notSortable(),
         //DTColumnBuilder.newColumn('MesComprometido').withTitle('Mes de Comprometido').notSortable(),
         //DTColumnBuilder.newColumn('MesCertificado').withTitle('Mes Certificado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaEquipoSeguridad)

        ];

        function AccionesBusquedaEquipoSeguridad(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.CargaModalEquipoSeguridad(\"" + data.IdSubServicioDatosCapex + "\",\"" + data.IdServicioSubServicio + "\"," + data.IdSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.Eliminar(\"" + 10 + "\"," + data.IdSubServicioDatosCapex + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarEquipoSeguridad() {

            LimpiarGrillaESeguridad();

            $timeout(function () {
                vm.dtOptionsESeguridad = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarEquipoSeguridadPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarEquipoSeguridadPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var capex = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 10,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = FlujoCajaService.ListarDetalleOportunidadEcapex(capex);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaESeguridad();
            });
        };


        /******************************************* Hardware *******************************************/
        vm.dtInstanceHardware = {};

        vm.dtColumnsHardware = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCapex').notVisible(),
         DTColumnBuilder.newColumn('IdServicioSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Equipo').notSortable(),
      //   DTColumnBuilder.newColumn('Cruce').withTitle('Cruce').notSortable(),
      //   DTColumnBuilder.newColumn('AEReducido').withTitle('A.E. Reducido').notSortable(),
        // DTColumnBuilder.newColumn('TipoEquipo').withTitle('Tipo de Hardware').notSortable(),
        // DTColumnBuilder.newColumn('Modelo').withTitle('Modelo').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('MesesAntiguedad').withTitle('Antigüedad').notSortable(),      
         DTColumnBuilder.newColumn('CostoUnitarioAntiguo').withTitle('C.U. Antiguo').notSortable(),
         DTColumnBuilder.newColumn('ValorResidualSoles').withTitle('Valor Residual').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitario').withTitle('C.U.').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('Capex (USD)').notSortable(),
         DTColumnBuilder.newColumn('CapexSoles').withTitle('Capex Total (S/.)').notSortable(),
         DTColumnBuilder.newColumn('TotalCapex').withTitle('Total Capex').notSortable(),
         //DTColumnBuilder.newColumn('MesRecupero').withTitle('Mes de RQ (Mes-Año)').notSortable(),
         //DTColumnBuilder.newColumn('MesComprometido').withTitle('Mes de Comprometido').notSortable(),
         //DTColumnBuilder.newColumn('MesCertificado').withTitle('Mes Certificado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaHardware)

        ];

        function AccionesBusquedaHardware(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.CargaModalHardware(\"" + data.IdSubServicioDatosCapex + "\",\"" + data.IdServicioSubServicio + "\"," + data.IdSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.Eliminar(\"" + 11 + "\"," + data.IdSubServicioDatosCapex + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarHardware() {

            LimpiarGrillaHardware();

            $timeout(function () {
                vm.dtOptionsHardware = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarHardwarePaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarHardwarePaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var capex = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 11,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = FlujoCajaService.ListarDetalleOportunidadEcapex(capex);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaHardware();
            });
        };

        /******************************************* Software *******************************************/
        vm.dtInstanceSoftware = {};

        vm.dtColumnsSoftware = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCapex').notVisible(),
         DTColumnBuilder.newColumn('IdServicioSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Equipo').notSortable(),
        // DTColumnBuilder.newColumn('Cruce').withTitle('Cruce').notSortable(),
        // DTColumnBuilder.newColumn('AEReducido').withTitle('A.E. Reducido').notSortable(),
        // DTColumnBuilder.newColumn('Marca').withTitle('Marca').notSortable(),
        // DTColumnBuilder.newColumn('Modelo').withTitle('Modelo').notSortable(),
          DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('MesesAntiguedad').withTitle('Antigüedad').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitarioAntiguo').withTitle('C.U. Antiguo').notSortable(),
         DTColumnBuilder.newColumn('ValorResidualSoles').withTitle('Valor Residual').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitario').withTitle('C.U.').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('Capex (USD)').notSortable(),
         DTColumnBuilder.newColumn('CapexSoles').withTitle('Capex Total (S/.)').notSortable(),
         DTColumnBuilder.newColumn('TotalCapex').withTitle('Total Capex').notSortable(),
         //DTColumnBuilder.newColumn('MesRecupero').withTitle('Mes de RQ (Mes-Año)').notSortable(),
         //DTColumnBuilder.newColumn('MesComprometido').withTitle('Mes de Comprometido').notSortable(),
         //DTColumnBuilder.newColumn('MesCertificado').withTitle('Mes Certificado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaSoftware)

        ];

        function AccionesBusquedaSoftware(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.CargaModalSoftware(\"" + data.IdSubServicioDatosCapex + "\",\"" + data.IdServicioSubServicio + "\"," + data.IdSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.Eliminar(\"" + 12 + "\"," + data.IdSubServicioDatosCapex + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarSoftware() {

            LimpiarGrillaSoftware();

            $timeout(function () {
                vm.dtOptionsSoftware = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarSoftwarePaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarSoftwarePaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var capex = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 12,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = FlujoCajaService.ListarDetalleOportunidadEcapex(capex);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaSoftware();
            });
        };

        /******************************************* Gabinetes *******************************************/
        vm.dtInstanceGabinetes = {};

        vm.dtColumnsGabinetes = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCapex').notVisible(),
         DTColumnBuilder.newColumn('IdServicioSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Equipo').notSortable(),
       //  DTColumnBuilder.newColumn('Cruce').withTitle('Cruce').notSortable(),
        // DTColumnBuilder.newColumn('AEReducido').withTitle('A.E. Reducido').notSortable(),
        // DTColumnBuilder.newColumn('Marca').withTitle('Marca').notSortable(),
        // DTColumnBuilder.newColumn('Modelo').withTitle('Modelo').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('MesesAntiguedad').withTitle('Antigüedad').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitarioAntiguo').withTitle('C.U. Antiguo').notSortable(),
         DTColumnBuilder.newColumn('ValorResidualSoles').withTitle('Valor Residual').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitario').withTitle('C.U.').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('Capex (USD)').notSortable(),
         DTColumnBuilder.newColumn('CapexSoles').withTitle('Capex Total (S/.)').notSortable(),
         DTColumnBuilder.newColumn('TotalCapex').withTitle('Total Capex').notSortable(),
        // DTColumnBuilder.newColumn('MesRecupero').withTitle('Mes de RQ (Mes-Año)').notSortable(),
        // DTColumnBuilder.newColumn('MesComprometido').withTitle('Mes de Comprometido').notSortable(),
         //DTColumnBuilder.newColumn('MesCertificado').withTitle('Mes Certificado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaGabinetes)

        ];

        function AccionesBusquedaGabinetes(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.CargaModalGabinete(\"" + data.IdSubServicioDatosCapex + "\",\"" + data.IdServicioSubServicio + "\"," + data.IdSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.Eliminar(\"" + 13 + "\"," + data.IdSubServicioDatosCapex + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarGabinetes() {

            LimpiarGrillaGabinetes();

            $timeout(function () {
                vm.dtOptionsGabinetes = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarGabinetesPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarGabinetesPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var capex = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 13,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = FlujoCajaService.ListarDetalleOportunidadEcapex(capex);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaGabinetes();
            });
        };


        /******************************************* Ghz *******************************************/
        vm.dtInstanceGhz = {};

        vm.dtColumnsGhz = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCapex').notVisible(),
         DTColumnBuilder.newColumn('IdServicioSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Equipo').notSortable(),
         //DTColumnBuilder.newColumn('Cruce').withTitle('Cruce').notSortable(),
         //DTColumnBuilder.newColumn('AEReducido').withTitle('A.E. Reducido').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('MesesAntiguedad').withTitle('Antigüedad').notSortable(),         
         DTColumnBuilder.newColumn('CostoUnitarioAntiguo').withTitle('C.U. Antiguo').notSortable(),
         DTColumnBuilder.newColumn('ValorResidualSoles').withTitle('Valor Residual').notSortable(),
         DTColumnBuilder.newColumn('CostoUnitario').withTitle('C.U.').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('Capex (USD)').notSortable(),
         DTColumnBuilder.newColumn('CapexSoles').withTitle('Capex Total (S/.)').notSortable(),
         DTColumnBuilder.newColumn('TotalCapex').withTitle('Total Capex').notSortable(),
         //DTColumnBuilder.newColumn('MesRecupero').withTitle('Mes de RQ (Mes-Año)').notSortable(),
        // DTColumnBuilder.newColumn('MesComprometido').withTitle('Mes de Comprometido').notSortable(),
         //DTColumnBuilder.newColumn('MesCertificado').withTitle('Mes Certificado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaGhz)

        ];

        function AccionesBusquedaGhz(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.CargaModalGhz(\"" + data.IdSubServicioDatosCapex + "\",\"" + data.IdServicioSubServicio + "\"," + data.IdSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.Eliminar(\"" + 14 + "\"," + data.IdSubServicioDatosCapex + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarGhz() {

            LimpiarGrillaGhz();

            $timeout(function () {
                vm.dtOptionsGhz = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarGhzPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarGhzPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var capex = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 14,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = FlujoCajaService.ListarDetalleOportunidadEcapex(capex);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaGhz();
            });
        };


        /******************************************* SolarWind *******************************************/
        vm.dtInstanceSolarWind = {};

        vm.dtColumnsSolarWind = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCapex').notVisible(),
         DTColumnBuilder.newColumn('IdServicioSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Equipo').notSortable(),
        // DTColumnBuilder.newColumn('Cruce').withTitle('Cruce').notSortable(),
         //DTColumnBuilder.newColumn('AEReducido').withTitle('A.E. Reducido').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('Capex (USD)').notSortable(),
         DTColumnBuilder.newColumn('CapexSoles').withTitle('Capex Total (S/.)').notSortable(),
         DTColumnBuilder.newColumn('TotalCapex').withTitle('Total Capex').notSortable(),
        // DTColumnBuilder.newColumn('MesRecupero').withTitle('Mes de RQ (Mes-Año)').notSortable(),
         //DTColumnBuilder.newColumn('MesComprometido').withTitle('Mes de Comprometido').notSortable(),
         //DTColumnBuilder.newColumn('MesCertificado').withTitle('Mes Certificado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaSolarWind)

        ];

        function AccionesBusquedaSolarWind(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.CargaModalSolarWind(\"" + data.IdSubServicioDatosCapex + "\",\"" + data.IdServicioSubServicio + "\"," + data.IdSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.Eliminar(\"" + 15 + "\"," + data.IdSubServicioDatosCapex + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarSolarWind() {

            LimpiarGrillaSolarWind();

            $timeout(function () {
                vm.dtOptionsSolarWind = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarSolarWindPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarSolarWindPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var capex = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 15,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = FlujoCajaService.ListarDetalleOportunidadEcapex(capex);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaSolarWind();
            });
        };

        /******************************************* SmartVPN *******************************************/
        vm.dtInstanceSmartVPN = {};

        vm.dtColumnsSmartVPN = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCapex').notVisible(),
         DTColumnBuilder.newColumn('IdServicioSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Equipo').notSortable(),
       //  DTColumnBuilder.newColumn('Cruce').withTitle('Cruce').notSortable(),
       //  DTColumnBuilder.newColumn('AEReducido').withTitle('A.E. Reducido').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('Capex (USD)').notSortable(),
         DTColumnBuilder.newColumn('CapexSoles').withTitle('Capex Total (S/.)').notSortable(),
         DTColumnBuilder.newColumn('TotalCapex').withTitle('Total Capex').notSortable(),
        // DTColumnBuilder.newColumn('MesRecupero').withTitle('Mes de RQ (Mes-Año)').notSortable(),
         //DTColumnBuilder.newColumn('MesComprometido').withTitle('Mes de Comprometido').notSortable(),
         //DTColumnBuilder.newColumn('MesCertificado').withTitle('Mes Certificado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaSmartVPN)

        ];

        function AccionesBusquedaSmartVPN(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.CargaModalSmart(\"" + data.IdSubServicioDatosCapex + "\",\"" + data.IdServicioSubServicio + "\"," + data.IdSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.Eliminar(\"" + 16 + "\"," + data.IdSubServicioDatosCapex + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarSmartVPN() {

            LimpiarGrillaSmartVPN();

            $timeout(function () {
                vm.dtOptionsSmartVPN = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarSmartVPNPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarSmartVPNPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var capex = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 16,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = FlujoCajaService.ListarDetalleOportunidadEcapex(capex);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaSmartVPN();
            });
        };


        /******************************************* Satelitales *******************************************/
        vm.dtInstanceSatelitales = {};

        vm.dtColumnsSatelitales = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCapex').notVisible(),
         DTColumnBuilder.newColumn('IdServicioSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('Servicio').withTitle('Servicio').notSortable(),
        // DTColumnBuilder.newColumn('Cruce').withTitle('Cruce').notSortable(),
       //  DTColumnBuilder.newColumn('AEReducido').withTitle('A.E. Reducido').notSortable(),
       //  DTColumnBuilder.newColumn('Circuito').withTitle('N° Circuito').notSortable(),
        // DTColumnBuilder.newColumn('Combo').withTitle('Combo').notSortable(),
        // DTColumnBuilder.newColumn('Medio').withTitle('Medio').notSortable(),
       //  DTColumnBuilder.newColumn('TipoEquipo').withTitle('Tipo').notSortable(),
        // DTColumnBuilder.newColumn('Garantizado').withTitle('Garantizado').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('CodigoModelo').withTitle('BW').notSortable(),
         DTColumnBuilder.newColumn('MesesAntiguedad').withTitle('Antigüedad').notSortable(),
         DTColumnBuilder.newColumn('CapexDolares').withTitle('Capex Total (USD)').notSortable(),
         DTColumnBuilder.newColumn('CapexSoles').withTitle('Capex Total (S/.)').notSortable(),
         DTColumnBuilder.newColumn('Instalacion').withTitle('Capex Instalación (S/.)').notSortable(),
         DTColumnBuilder.newColumn('TotalCapex').withTitle('Total Capex').notSortable(),
         //DTColumnBuilder.newColumn('MesRecupero').withTitle('Mes de RQ (Mes-Año)').notSortable(),
         //DTColumnBuilder.newColumn('MesComprometido').withTitle('Mes de Comprometido').notSortable(),
         //DTColumnBuilder.newColumn('MesCertificado').withTitle('Mes Certificado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaSatelitales)

        ];

        function AccionesBusquedaSatelitales(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.CargaModalSatelitales(\"" + data.IdSubServicioDatosCapex + "\",\"" + data.IdServicioSubServicio + "\"," + data.IdSubServicio + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-hide=vm.EditarDatoTable class='btn '  data-toggle='modal' ng-click='vm.Eliminar(\"" + 17 + "\"," + data.IdSubServicioDatosCapex + ");' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarSatelitales() {

            LimpiarGrillaSatelitales();

            $timeout(function () {
                vm.dtOptionsSatelitales = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarSatelitalesPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarSatelitalesPaginado(sSource, aoData, fnCallback, oSettings) {

            console.log(aoData);

            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var capex = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 17,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = FlujoCajaService.ListarDetalleOportunidadEcapex(capex);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaSatelitales();
            });
        };



        /******************************************* Funciones *******************************************/



        function LimpiarGrillaEstudiosEspeciales() {
            vm.dtOptionsEstudiosEspeciales = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        };
        function LimpiarGrillaEquiposEspeciales() {
            vm.dtOptionsEquiposEspeciales = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };
        function LimpiarGrillaRouter() {
            vm.dtOptionsRouter = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };
        function LimpiarGrillaModems() {
            vm.dtOptionsModems = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };
        function LimpiarGrillaESeguridad() {
            vm.dtOptionsESeguridad = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };
        function LimpiarGrillaHardware() {
            vm.dtOptionsHardware = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };
        function LimpiarGrillaSoftware() {
            vm.dtOptionsSoftware = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };
        function LimpiarGrillaGabinetes() {
            vm.dtOptionsGabinetes = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };
        function LimpiarGrillaGhz() {
            vm.dtOptionsGhz = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };
        function LimpiarGrillaSolarWind() {
            vm.dtOptionsSolarWind = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };
        function LimpiarGrillaSmartVPN() {
            vm.dtOptionsSmartVPN = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };
        function LimpiarGrillaSatelitales() {
            vm.dtOptionsSatelitales = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);


        };

        function CargaModalEstudiosEspeciales(IdSubServicioDatosCapex, IdServicioSubServicio, IdSubServicio, IdOportunidad) {

            vm.dto = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdServicioSubServicio: IdServicioSubServicio,
                IdSubServicio: IdSubServicio,
                IdOportunidad: IdOportunidad
            };

            console.log(vm.dto);

            var promise = EstudiosEspecialesService.ModalEstudiosEspeciales(vm.dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoEstudiosEspeciales").html(content);
                });

                $('#ModalEstudiosEspeciales').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaModalEquiposEspeciales(IdSubServicioDatosCapex, IdServicioSubServicio, IdSubServicio) {

            vm.dto = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdServicioSubServicio: IdServicioSubServicio,
                IdSubServicio: IdSubServicio
            };

            var promise = EquiposEspecialesService.ModalEquiposEspeciales();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoEquiposEspeciales").html(content);
                });

                $('#ModalEquiposEspeciales').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };
        function CargaModalRouter(IdSubServicioDatosCapex, IdServicioSubServicio, IdSubServicio) {

            vm.dto = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdServicioSubServicio: IdServicioSubServicio,
                IdSubServicio: IdSubServicio
            };

            var promise = RoutersService.ModalRouter();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoRouter").html(content);
                });

                $('#ModalRouter').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaModalModems(IdSubServicioDatosCapex, IdServicioSubServicio, IdSubServicio) {

            vm.dto = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdServicioSubServicio: IdServicioSubServicio,
                IdSubServicio: IdSubServicio
            };

            var promise = ModemsService.ModalModems();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoModems").html(content);
                });

                $('#ModalModems').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaModalEquipoSeguridad(IdSubServicioDatosCapex, IdServicioSubServicio, IdSubServicio) {

            vm.dto = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdServicioSubServicio: IdServicioSubServicio,
                IdSubServicio: IdSubServicio
            };

            var promise = EquipoSeguridadService.ModalEquipoSeguridad();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoEquipoSeguridad").html(content);
                });

                $('#ModalEquipoSeguridad').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaModalHardware(IdSubServicioDatosCapex, IdServicioSubServicio, IdSubServicio) {

            vm.dto = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdServicioSubServicio: IdServicioSubServicio,
                IdSubServicio: IdSubServicio
            };

            var promise = HardwareService.ModalHardware();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoHardware").html(content);
                });

                $('#ModalHardware').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaModalSoftware(IdSubServicioDatosCapex, IdServicioSubServicio, IdSubServicio) {

            vm.dto = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdServicioSubServicio: IdServicioSubServicio,
                IdSubServicio: IdSubServicio
            };

            var promise = SoftwareService.ModalSoftware();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoSoftware").html(content);
                });

                $('#ModalSoftware').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaModalGabinete(IdSubServicioDatosCapex, IdServicioSubServicio, IdSubServicio) {

            vm.dto = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdServicioSubServicio: IdServicioSubServicio,
                IdSubServicio: IdSubServicio
            };

            var promise = GabineteService.ModalGabinete();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoGabinete").html(content);
                });

                $('#ModalGabinete').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaModalGhz(IdSubServicioDatosCapex, IdServicioSubServicio, IdSubServicio) {

            vm.dto = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdServicioSubServicio: IdServicioSubServicio,
                IdSubServicio: IdSubServicio
            };

            var promise = GhzService.ModalGhz();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoGhz").html(content);
                });

                $('#ModalGhz').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaModalSolarWind(IdSubServicioDatosCapex, IdServicioSubServicio, IdSubServicio) {

            vm.dto = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdServicioSubServicio: IdServicioSubServicio,
                IdSubServicio: IdSubServicio
            };

            var promise = SolarWindService.ModalSolarWind();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoSolarWind").html(content);
                });

                $('#ModalSolarWind').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaModalSmart(IdSubServicioDatosCapex, IdServicioSubServicio, IdSubServicio) {

            vm.dto = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdServicioSubServicio: IdServicioSubServicio,
                IdSubServicio: IdSubServicio
            };

            var promise = SmartVpnService.ModalSmart();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoSmart").html(content);
                });

                $('#ModalSmart').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaModalSatelitales(IdSubServicioDatosCapex, IdServicioSubServicio, IdSubServicio) {

            vm.dto = {
                IdSubServicioDatosCapex: IdSubServicioDatosCapex,
                IdServicioSubServicio: IdServicioSubServicio,
                IdSubServicio: IdSubServicio
            };

            var promise = SatelitalesService.ModalSatelitales();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoSatelitales").html(content);
                });

                $('#ModalSatelitales').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };


        function BuscarEcapex() {
            BuscarEstudiosEspeciales();
            BuscarEquiposEspeciales();
            BuscarRouter();
            BuscarModems();
            BuscarEquipoSeguridad();
            BuscarHardware();
            BuscarSoftware();
            BuscarGabinetes();
            BuscarGhz();
            BuscarSolarWind();
            BuscarSmartVPN();
            BuscarSatelitales();
        };

        function CargarVista() {
            // if (vm.IdOportunidadLineaNegocio > 0) {
            BuscarEcapex();
            //}
        };

        /******************************************* LOAD *******************************************/

        CargarVista();

    }
})();



