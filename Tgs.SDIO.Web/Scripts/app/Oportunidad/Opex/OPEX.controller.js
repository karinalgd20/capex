﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('OPEXController', OPEXController);

    OPEXController.$inject = ['OPEXService', 'MaestraService', 'FlujoCajaService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function OPEXController(OPEXService, MaestraService, FlujoCajaService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.tabFlujoCaja = true;
        vm.idOPEX = 0;

        vm.servicio = "";
        vm.cantidad = 0;
        vm.numeroMeses = 0;
        vm.mesInicio = 0;
        vm.montoUnitarioMensual = 0;
        vm.montoTotalMensual = 0;

        vm.GrabarOPEX = GrabarOPEX;

        vm.ListTipo = [];
        vm.IdTipo = "-1";

        vm.IdFlujoCaja = $scope.$parent.vm.Oportunidad.IdFlujoCaja;
        vm.IdOportunidadLineaNegocio = $scope.$parent.vm.IdOportunidadLineaNegocio;
        vm.IdSubServicioDatosCaratula = 0;
        vm.IdFlujoCajaConfiguracion = 0;
        vm.IdPeriodos = 0;
        vm.negativo = "-1";
        vm.CalculaPxQ = CalculaPxQ;
        vm.ObtieneCostoPreOperativo = ObtieneCostoPreOperativo;
        /******************************************* OPEX *******************************************/

        function GrabarOPEX() {

            var flujocaja = {
                IdFlujoCaja: vm.IdFlujoCaja,
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                Cantidad: vm.cantidad,
                CostoUnitario: 0
            };


            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();

                var promise = FlujoCajaService.ActualizarOportunidadFlujoCaja(flujocaja);
                promise.then(function (response) {
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#lblAlerta_OPEX_Modal', 'danger', MensajesUI.DatosError, 20);
                    } else {

                        var caratula = {
                            IdFlujoCaja: vm.IdFlujoCaja,
                            IdSubServicioDatosCaratula: vm.IdSubServicioDatosCaratula,
                            Cantidad: vm.cantidad,
                            //NumeroMeses: vm.numeroMeses,
                            MontoUnitarioMensual: vm.montoUnitarioMensual,
                            MontoTotalMensual: vm.montoTotalMensual,
                            //NumeroMesInicioGasto: vm.mesInicio,
                            IdEstado: 1
                        };

                        var promise = FlujoCajaService.ActualizarSubServicioCaratula(caratula);
                        promise.then(function (response) {

                            var Respuesta = response.data;
                            if (Respuesta.TipoRespuesta != 0) {
                                blockUI.stop();
                                UtilsFactory.Alerta('#lblAlerta_OPEX_Modal', 'danger', Respuesta.Mensaje, 20);
                            } else {
                                blockUI.stop();
                                UtilsFactory.Alerta('#lblAlerta_OPEX_Modal', 'success', Respuesta.Mensaje, 10);
                                $scope.$$childTail.vm.GrabarFlujoCajaConfiguracion();
                                $scope.$parent.vm.BuscarCaratula();
                            }

                        }, function (response) {
                            blockUI.stop();
                            UtilsFactory.Alerta('#lblAlerta_OPEX_Modal', 'danger', MensajesUI.DatosError, 5);
                        });
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_OPEX_Modal', 'danger', MensajesUI.DatosError, 5);
                });
            };
        }

        /******************************************* Funciones *******************************************/

        function CargarOPEX() {
            var dto = {
                IdSubServicioDatosCaratula: $scope.$parent.vm.dto.IdSubServicioDatosCaratula,
                IdGrupo: 3
            };

            var promise = FlujoCajaService.ObtenerDetalleOportunidadCaratula(dto);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;

                vm.servicio = respuesta.SubServicio;
                vm.cantidad = respuesta.Cantidad;
                //vm.numeroMeses = respuesta.Meses;
                //vm.mesInicio = respuesta.NumeroMesInicioGasto;
                vm.montoUnitarioMensual = respuesta.MontoUnitarioMensual;
                vm.montoTotalMensual = respuesta.MontoTotalMensual;

                CalculaPxQ();

                vm.IdFlujoCaja = respuesta.IdFlujoCaja;
                vm.IdSubServicioDatosCaratula = respuesta.IdSubServicioDatosCaratula;
                vm.IdFlujoCajaConfiguracion = respuesta.IdFlujoCajaConfiguracion;
                vm.IdPeriodos = respuesta.IdPeriodos == 0 ? "1" : respuesta.IdPeriodos;

                //$scope.$$childTail.vm.Meses = respuesta.TiempoProyecto;
                $scope.$$childTail.vm.ObtenerFlujoCajaConfiguracion();

            }, function (response) {
                blockUI.stop();

            });
        };
        function ListarTipoPorId(int) {

            var filtroTipo = {
                IdRelacion: 32
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipo);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.IdTipo = int;
                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function CargarTipo() {

            var filtroTipo = {
                IdRelacion: 32
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipo);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.IdTipo = "-1";
                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CalculaPxQ() {

            var cantidad = (vm.cantidad == null) ? 0 : vm.cantidad;
            var monto = (vm.montoUnitarioMensual == null) ? 0 : vm.montoUnitarioMensual;
            vm.montoTotalMensual = (cantidad * monto);

            ObtieneCostoPreOperativo();
        };

        function ObtieneCostoPreOperativo() {

            $scope.$$childTail.vm.CostoPreOperativo = vm.montoTotalMensual;
        };

        /******************************************* LOAD *******************************************/
        CargarTipo();
        CargarOPEX();
    }
})();



