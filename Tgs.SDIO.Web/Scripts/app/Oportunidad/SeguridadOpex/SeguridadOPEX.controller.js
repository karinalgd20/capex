﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('SeguridadOPEXController', SeguridadOPEXController);

    SeguridadOPEXController.$inject = ['SeguridadOPEXService', 'FlujoCajaService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function SeguridadOPEXController(SeguridadOPEXService, FlujoCajaService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.tabFlujoCaja = true;
        vm.idSOPEX = 0;
        vm.servicio = "";
        vm.cantidad = 0;
        vm.numeroMeses = 0;
        vm.precioUnitario = 0;
        vm.montoPxQ = 0;
        vm.factor = 0;
        vm.valorCuota = 0;
        vm.mesInicioGasto = 0;

        vm.GrabarSOPEX = GrabarSOPEX;

        vm.ListTipo = [];
        vm.IdTipo = "-1";

        vm.IdFlujoCaja = $scope.$parent.vm.Oportunidad.IdFlujoCaja;
        vm.IdOportunidadLineaNegocio = $scope.$parent.vm.IdOportunidadLineaNegocio;
        vm.IdSubServicioDatosCaratula = 0;
        vm.IdFlujoCajaConfiguracion = 0;
        vm.IdPeriodos = 0;
        vm.negativo = "-1";
        vm.CalculaValorCuota = CalculaValorCuota;
        vm.CalculaPxQ = CalculaPxQ;
        vm.ObtieneCostoPreOperativo = ObtieneCostoPreOperativo;
        /******************************************* ServicioCMI *******************************************/

        function GrabarSOPEX() {
            var flujocaja = {
                IdFlujoCaja: vm.IdFlujoCaja,
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                Cantidad: vm.cantidad,
                CostoUnitario: 0
            };


            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();

                var promise = FlujoCajaService.ActualizarOportunidadFlujoCaja(flujocaja);
                promise.then(function (response) {
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#lblAlerta_SOPEX', 'danger', MensajesUI.DatosError, 20);
                    } else {

                        var caratula = {
                            IdFlujoCaja: vm.IdFlujoCaja,
                            IdSubServicioDatosCaratula: vm.IdSubServicioDatosCaratula,
                            IdTipo: vm.IdTipo,
                            Cantidad: vm.cantidad,
                            //NumeroMeses: vm.numeroMeses,
                            MontoUnitarioMensual: vm.precioUnitario,
                            MontoTotalMensual: vm.montoPxQ,
                            Factor: vm.factor,
                            ValorCuota: vm.valorCuota,
                            //NumeroMesInicioGasto: vm.mesInicioGasto,
                            IdEstado:1
                        };

                        var promise = FlujoCajaService.ActualizarSubServicioCaratula(caratula);
                        promise.then(function (response) {

                            var Respuesta = response.data;
                            if (Respuesta.TipoRespuesta != 0) {
                                blockUI.stop();
                                UtilsFactory.Alerta('#lblAlerta_SOPEX', 'danger', Respuesta.Mensaje, 20);
                            } else {
                                blockUI.stop();
                                UtilsFactory.Alerta('#lblAlerta_SOPEX', 'success', Respuesta.Mensaje, 10);
                                $scope.$$childTail.vm.GrabarFlujoCajaConfiguracion();
                                $scope.$parent.vm.BuscarCaratula();
                            }

                        }, function (response) {
                            blockUI.stop();
                            UtilsFactory.Alerta('#lblAlerta_SOPEX', 'danger', MensajesUI.DatosError, 5);
                        });
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_SOPEX', 'danger', MensajesUI.DatosError, 5);
                });
            };
        }

        /******************************************* Funciones *******************************************/

        function CargarSOPEX() {
            var dto = {
                IdSubServicioDatosCaratula: $scope.$parent.vm.dto.IdSubServicioDatosCaratula,
                IdGrupo: 5
            };

            var promise = FlujoCajaService.ObtenerDetalleOportunidadCaratula(dto);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;
                vm.cantidad = respuesta.Cantidad;
                //vm.numeroMeses = respuesta.NumeroMeses;
                vm.precioUnitario = respuesta.MontoUnitarioMensual;
                vm.factor = respuesta.Factor;
                vm.valorCuota = respuesta.ValorCuota;
                vm.IdTipo = respuesta.IdTipoCosto;
                //vm.mesInicioGasto = respuesta.NumeroMesInicioGasto;
                vm.servicio = respuesta.SubServicio;
                vm.montoPxQ = respuesta.MontoTotalMensual;
                CargarTipo();
                ObtieneCostoPreOperativo();

                vm.IdFlujoCaja = respuesta.IdFlujoCaja;
                vm.IdSubServicioDatosCaratula = respuesta.IdSubServicioDatosCaratula;
                vm.IdFlujoCajaConfiguracion = respuesta.IdFlujoCajaConfiguracion;
                vm.IdPeriodos = respuesta.IdPeriodos==0?"1":respuesta.IdPeriodos;
               // $scope.$$childTail.vm.Meses = respuesta.TiempoProyecto;
                $scope.$$childTail.vm.ObtenerFlujoCajaConfiguracion();

                if (respuesta.IdTipoCosto > 0) { ListarTipoPorId(respuesta.IdTipoCosto) }
            }, function (response) {
                blockUI.stop();

            });
        };

        function ListarTipoPorId(int) {

            var filtroTipo = {
                IdRelacion: 32
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipo);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.IdTipo =int;
                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        function CargarTipo() {

            var filtroTipo = {
                IdRelacion: 32
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipo);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.IdTipo = "-1";
                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CalculaPxQ() {

            var cantidad = (vm.cantidad == null) ? 0 : vm.cantidad;
            var monto = (vm.precioUnitario == null) ? 0 : vm.precioUnitario;
            vm.montoPxQ = (cantidad * monto);
            CalculaValorCuota();
            ObtieneCostoPreOperativo();
        };

        function CalculaValorCuota() {

            var monto = (vm.montoPxQ == null) ? 0 : vm.montoPxQ;
            var factor = (vm.factor == null) ? 0 : vm.factor;
            vm.valorCuota = ((monto * factor) / 100);
        };

        function ObtieneCostoPreOperativo() {

            $scope.$$childTail.vm.CostoPreOperativo = vm.valorCuota;
        };

        /******************************************* LOAD *******************************************/
    
        CargarSOPEX();
    }
})();



