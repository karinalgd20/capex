﻿(function () {
    'use strict',
    angular
        .module('app.Oportunidad')
        .service('CotizacionService', CotizacionService);

    CotizacionService.$inject = ['$http', 'URLS'];

    function CotizacionService($http, $urls) {
        var service = {
            ListarCotizacionPaginado: ListarCotizacionPaginado,
        };

        return service;

        function ListarCotizacionPaginado(Cotizacion) {
            return $http({
                url: $urls.ApiOportunidad + "Cotizacion/ListarCotizacionPaginado",
                method: "POST",
                data: JSON.stringify(Cotizacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})(); 