﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('RegistrarSedeServicioController', RegistrarSedeServicioController);

    RegistrarSedeServicioController.$inject = ['RegistrarSedeServicioService', 'MaestraService','SedeService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarSedeServicioController(RegistrarSedeServicioService, MaestraService,SedeService,
    blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

      

        vm.Id = 0;
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;

        vm.dtInstanciaSede= [];

        vm.dtColumnsSedeModal =
            [
            DTColumnBuilder.newColumn('Id').notVisible(),
            DTColumnBuilder.newColumn('IdSede').notVisible(),
            DTColumnBuilder.newColumn('IdServicioSedeInstalacion').notVisible(),
            DTColumnBuilder.newColumn('TipoSede').withTitle('Tipo Sede').notSortable(),
            DTColumnBuilder.newColumn('Direccion').withTitle('Dirección').notSortable(),
            DTColumnBuilder.newColumn('Servicio').withTitle('Servicios').notSortable(),
            DTColumnBuilder.newColumn('Configurado').withTitle('Configurado').notSortable(),
            DTColumnBuilder.newColumn('CodSisego').withTitle('Código Sisego').notSortable(),
            DTColumnBuilder.newColumn('Departamento').withTitle('Departamento').notSortable(),
            DTColumnBuilder.newColumn('Provincia').withTitle('Provincia').notSortable(),
            DTColumnBuilder.newColumn('Distrito').withTitle('Distrito').notSortable(),
            DTColumnBuilder.newColumn('Estado').withTitle('Estado').notSortable(),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')

            ];


        function AccionesBusqueda(data, type, full, meta) {
            var respuesta = "";
            //respuesta = respuesta + " <a title='Editar'   ng-click='vm.CargaModal(\"" + data.Id + "\"," + data.IdSede + ");'>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            //respuesta = respuesta + "<a title='Eliminar Sede Instalacion'  ng-click='vm.EliminarSede(\"" + data.Id + "\"," + data.IdSede + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6; margin-left: 1px;'></span></a>";
            respuesta = respuesta + "<a title='Eliminar Servicio Instalacion'  ng-click='vm.EliminarServicios(" + data.IdServicioSedeInstalacion + ");'>" + "<span class='glyphicon glyphicon-remove-circle' style='color: #00A4B6; margin-left: 5px;'></span></a>";

            return respuesta;
        }

        function BuscarSede() {
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptionsServiciosModal = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarSedePaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarSedePaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var sede = {
                IdOportunidad: vm.IdOportunidad,
                Indice: pageNumber,
                Tamanio: length,
                IdEstado: vm.IdEstado
            };

            var promise = SedeService.ListarSedeInstalacion(sede);
            promise.then(function (response) {

                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListSedeDto
                };
                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };
                function LimpiarGrilla() {
            vm.dtOptionsServiciosModal = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
        };
    }
})();



