﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('RegistrarSedeServicioService', RegistrarSedeServicioService);

    RegistrarSedeServicioService.$inject = ['$http', 'URLS'];

    function RegistrarSedeServicioService($http, $urls) {

        var service = {
         VistaSede:VistaSede
        };

        return service;

      
        function VistaSede() {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarSedeServicio/Index",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

     
    }
})();