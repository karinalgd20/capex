﻿(function () {
    'use strict'
    angular
        .module('app.Oportunidad')
        .controller('BandejaEstudios', BandejaEstudios);

    BandejaEstudios.$inject =
        [
        'EstudiosService',
        'blockUI',
        'UtilsFactory',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        'MensajesUI',
        '$timeout',
        'URLS',
        '$scope',
        '$compile',
        '$modal',
        '$injector'
        ];

    function BandejaEstudios
        (
        EstudiosService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        MensajesUI,
        $timeout,
        $urls,
        $scope,
        $compile,
        $modal,
        $injector
        ) {

        var vm = this;
        vm.TituloModal = '';
        vm.IdSede = $scope.$parent.vm.IdSede;
        vm.IdSedeInstalacion = $scope.$parent.vm.IdSedeInstalacion;



        vm.dtColumns = [
         DTColumnBuilder.newColumn('Id').notVisible(),
         DTColumnBuilder.newColumn('IdSedeInstalacion').withTitle('IdSedeInstalacion').notSortable(),
         DTColumnBuilder.newColumn('Estudio').withTitle('Estudio').notSortable(),
         DTColumnBuilder.newColumn('Departamento').withTitle('Departamento').notSortable(),
         DTColumnBuilder.newColumn('TipoRequerimiento').withTitle('TipoRequerimiento').notSortable(),
         DTColumnBuilder.newColumn('Nodo').withTitle('Nodo').notSortable(),
         DTColumnBuilder.newColumn('Dias').withTitle('Dias').notSortable(),
         DTColumnBuilder.newColumn('TotalSoles').withTitle('TotalSoles').notSortable(),
         DTColumnBuilder.newColumn('TotalDolares').withTitle('TotalDolares').notSortable(),
         DTColumnBuilder.newColumn('Responsable').withTitle('Responsable').notSortable(),
         DTColumnBuilder.newColumn('FacilidadesTecnicas').withTitle('FacilidadesTecnicas').notSortable(),
         DTColumnBuilder.newColumn('EstadoSisego').withTitle('EstadoSisego').notSortable(),
        ];

        vm.ListarEstudios = ListarEstudios;
        LimpiarGrilla();
        ListarEstudios();


        function ListarEstudios() {
            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(ListarEstudiosPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }

        function ListarEstudiosPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var Estudios = {
                IdSedeInstalacion: $scope.$parent.vm.IdSedeInstalacion,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = EstudiosService.ListarEstudiosPaginado(Estudios);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaEstudios
                };

                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
    }
})();