﻿(function () {
    'use strict',
    angular
    .module('app.Oportunidad')
    .service('BandejaEstudiosService', BandejaEstudiosService);

    BandejaEstudiosService.$inject = ['$http', 'URLS'];

    function BandejaEstudiosService($http, $urls) {

        var service = {
            VistaBandejaEstudios: VistaBandejaEstudios
        };

        return service;

        function VistaBandejaEstudios() {
            return $http({
                url: $urls.ApiOportunidad + "BandejaEstudios/Index",
                method: "GET",
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
    }
})();