﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    //.factory('servicioPasoParametros', function(){
    //    return {
    //        data: {
    //            parametro: '0'
    //        }
    //    }
    //})servicioPasoParametros
    .controller('RegistrarOportunidad', RegistrarOportunidad);

    RegistrarOportunidad.$inject = ['RegistrarOportunidadService', 'ClienteService', 'SectorService', 'MaestraService', 'DireccionComercialService', 'LineaNegocioService','SalesForceConsolidadoCabeceraService','SalesForceConsolidadoDetalleService',
     'OportunidadContenedorService', 
        'blockUI', 'UtilsFactory', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$injector'];
    function RegistrarOportunidad(RegistrarOportunidadService,  ClienteService, SectorService, MaestraService, DireccionComercialService, LineaNegocioService,SalesForceConsolidadoCabeceraService ,SalesForceConsolidadoDetalleService,
        OportunidadContenedorService,
         blockUI, UtilsFactory, $timeout, MensajesUI, $urls, $scope, $compile, $injector) {

        var vm = this;

        //<a class="btn btn-sm btn-primary btn-space right" href="{{vm.EnlaceRegistrar}}0"><span class="glyphicon glyphicon-list-alt"></span> Nuevo</a>
        vm.EnlaceRegistrar = $urls.ApiOportunidad + "OportunidadContenedor/Index/";
        vm.Nuevo = $urls.ApiOportunidad + "OportunidadContenedor/Index/";
        vm.Editar = $urls.ApiOportunidad + "OportunidadContenedor/Index/";

        vm.Descripcion = '';
        vm.SelectCliente = SelectCliente;
        vm.SelectNumeroSalesForce = SelectNumeroSalesForce;
        vm.fillTextbox = fillTextbox;
        vm.fillTextboxNumeroSalesForce = fillTextboxNumeroSalesForce;
        vm.ObtenerCliente = ObtenerCliente;
        vm.ObtenerSector = ObtenerSector;
        vm.ObtenerDireccionComercial = ObtenerDireccionComercial;
        vm.RegistrarOportunidad = RegistrarOportunidad;
        vm.ObtenerOportunidadFlujoEstadoId=ObtenerOportunidadFlujoEstadoId;
        vm.Preventa = Preventa;
        vm.Preventa = "";    
        
        vm.CargarTipoProyecto = CargarTipoProyecto;
        vm.CargarTipoServicio = CargarTipoServicio;
        vm.CargarAnalistaFIN = CargarAnalistaFIN;
        vm.CargarProductManager = CargarProductManager;
        vm.CargarOpciones = CargarOpciones;
        vm.CargarNumeroCaso = CargarNumeroCaso;
        vm.CargaOportunidad=CargaOportunidad;
        vm.ListCliente=[]; 

        vm.ListTipoProyecto = [];
        vm.ListTipoServicio = [];
        vm.ListLineaNegocio = [];
        vm.ListProductManager=[];
        vm.ListAnalistaFin = [];
        vm.ListcoordinadorFin = [];
        vm.ListNumeroCaso = [];
        vm.ListNumeroSalesForce=[];
        
        
        vm.IdTipoServicio = "-1";
        vm.IdTipoProyecto = "-1";
        vm.IdLineaNegocio = "-1";
        vm.IdAnalistaFin="-1";
        vm.IdProductManager = "-1";
        vm.IdCoordinadorFin = "-1";

        vm.NumeroCaso = "-1";

        vm.Sector = "";
        vm.GerenteComercial = "";
        vm.DireccionComercial = "";
        vm.IdOportunidad = 0;
        vm.IdEstadoOportunidad=0;
        vm.IdOportunidadLineaNegocio=0;
        vm.ObjOportunidad = [];
        vm.ObjOportunidad = jsonOportunidad;
        
        vm.IdEstado = "";
        vm.ListEstado = [];
        

        vm.OptionAnalistaFin = "";
     
        vm.OptionCoordinadorFin = "";
        
        vm.OptionPreVenta = "";
     
        vm.DescripcionEstado = "";

        ListarLineaNegocio();
        CargarTipoProyecto();
        CargarTipoServicio();
        CargarAnalistaFIN();
        CargarProductManager();
        CoordinadorFIN();
        
        vm.OptionAnalistaFin = false;
        vm.ListEstado = false;
        vm.AccionGrabar = false;
        vm.OptionPreVenta = false;
        vm.EditarDato = true;

        CargaOportunidad();


  
        function TabPorPerfilOportunidadDatos() {


            var promise = OportunidadContenedorService.TabPorPerfilOportunidadDatos();

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
             vm.OptionCoordinadorFin = !Respuesta.OptionCoordinadorFin;
           
              vm.OptionAnalistaFin = !Respuesta.OptionAnalistaFin;
              vm.ListEstado = Respuesta.ListaAccionPerfil;
               vm.AccionGrabar =!Respuesta.AccionGrabar;
               vm.OptionPreVenta = !Respuesta.OptionPreVenta;
               vm.EditarDato = !Respuesta.EditarDato;

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargaOportunidad() {

            ListarLineaNegocio();
            CargarTipoProyecto();
            CargarTipoServicio();
            CargarAnalistaFIN();
            CargarProductManager();
            CoordinadorFIN();
         
            CargarOpciones();
            Preventa();


            if (vm.ObjOportunidad != 0) {

                if (vm.ObjOportunidad.IdAnalistaFinanciero == null) {
                    vm.ObjOportunidad.IdAnalistaFinanciero = "-1";
                }

                vm.IdOportunidad = vm.ObjOportunidad.IdOportunidad;
                vm.IdCliente = vm.ObjOportunidad.IdCliente;
                vm.Oportunidad = vm.ObjOportunidad.Descripcion;
                vm.NumeroSalesForce = vm.ObjOportunidad.NumeroSalesForce;
               
                vm.NumeroCaso = vm.ObjOportunidad.NumeroCaso;
                ObtenerNumeroCaso(vm.NumeroCaso );
                vm.fecha = vm.ObjOportunidad.FechaOportunidad;
                vm.AlcanceProyecto = vm.ObjOportunidad.Alcance;
                vm.meses = vm.ObjOportunidad.Periodo;
                vm.Implantacion = vm.ObjOportunidad.TiempoImplantacion;
                vm.IdTipoServicio = vm.ObjOportunidad.IdTipoServicio;
                vm.IdLineaNegocio = vm.ObjOportunidad.IdLineaNegocio;
                vm.IdAnalistaFin = vm.ObjOportunidad.IdAnalistaFinanciero;
                vm.IdProductManager = vm.ObjOportunidad.IdProductManager;
                vm.IdTipoProyecto = vm.ObjOportunidad.IdTipoProyecto;
                vm.IdTipoCambio = vm.ObjOportunidad.IdTipoCambio;
                vm.IdProyectoAnterior = vm.ObjOportunidad.IdProyectoAnterior;
                vm.IdCoordinadorFin = vm.ObjOportunidad.IdCoordinadorFinanciero;
                vm.IdEstado = vm.ObjOportunidad.IdEstado;
                vm.IdEstadoOportunidad = vm.ObjOportunidad.IdEstado;
                vm.IdOportunidadLineaNegocio = vm.ObjOportunidad.IdOportunidadLineaNegocio;
                CargarClientePorId();
                ListarMaestraPorValor(vm.IdEstado);
            }
         
                 
             TabPorPerfilOportunidadDatos();
        }

        function CargarOpciones() {
        //var Respuesta = vm.ListEstado;

        //vm.IdEstado = "-1";
        //vm.ListEstado= UtilsFactory.AgregarItemSelect(Respuesta);

        }
       
        function CargarClientePorId() {
            var cliente={
                IdCliente:vm.IdCliente,
                IdEstado:1
            }

            var promise = ClienteService.ObtenerCliente(cliente);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.Descripcion = Respuesta.Descripcion;
                vm.GerenteComercial = Respuesta.GerenteComercial;
                var IdDireccionComercial = Respuesta.IdDireccionComercial;
                var IdSector = Respuesta.IdSector;
                ObtenerDireccionComercial(IdDireccionComercial);
                ObtenerSector(IdSector);
         
            }, function (response) {
                blockUI.stop();

            });
        }
        function LimpiarCampoSalesFore() {
            
            vm.Oportunidad = "";
            vm.meses = "";
            vm.IdCliente = 0;
            vm.Descripcion = "";
            vm.GerenteComercial = "";
            vm.Sector = "";
            vm.DireccionComercial = "";

            $('#idNumeroCaso').attr('disabled', 'disabled');

            vm.NumeroCaso = "-1";
          
   
        }
        function SelectCliente() {
            vm.IdCliente = 0;
            vm.GerenteComercial = "";
            vm.Sector = "";
            vm.DireccionComercial = "";
            vm.ListCliente = null;
            if (vm.Descripcion.length > 5) {
                ListarCliente();
                vm.heightCliente = "";
            }
        }

        function Preventa() {
            var promise = RegistrarOportunidadService.Preventa();

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                vm.Preventa = Respuesta;

            }, function (response) {
                blockUI.stop();

            });
        }

        function CoordinadorFIN() {
            var usuario_Admin = {
                CodigoPerfil: "CoFiNa"

            };
            var promise = RegistrarOportunidadService.ListarUsuariosPorPerfil(usuario_Admin);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                vm.ListcoordinadorFin = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarAnalistaFIN() {
            var usuario_Admin = {
                CodigoPerfil: "ANLFIN"

            };

            var promise = RegistrarOportunidadService.ListarUsuariosPorPerfil(usuario_Admin);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                vm.ListAnalistaFin = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function CargarProductManager() {
            var usuario_Admin = {
                CodigoPerfil :"PDM"
                };

            var promise = RegistrarOportunidadService.ListarUsuariosPorPerfil(usuario_Admin);
         
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListProductManager = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarLineaNegocio() {
            var cliente = {
                IdEstado: 1

            };
            var promise = LineaNegocioService.ListarLineaNegocios(cliente);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function ListarCliente() {
            if (vm.Descripcion != null || vm.Descripcion!="") {
                var cliente = {
                    IdEstado: 1,
                    Descripcion: vm.Descripcion,

                };
                var promise = ClienteService.ListarClientePorDescripcion(cliente);

                promise.then(function (resultado) {
                    blockUI.stop();
                    if (Respuesta != null) {
                        vm.heightCliente = 350;
                    }
                 
                    var Respuesta = resultado.data;
                    var output = [];
                    angular.forEach(Respuesta, function (cliente) {
                        output.push(cliente);
                    });
                    vm.ListCliente = output;

                }, function (response) {
                    blockUI.stop();

                });
            }
        }
        function fillTextbox(string, Id) {
            LimpiarCampoCliente();
            vm.Descripcion = string;
            vm.IdCliente = Id;
            vm.ListCliente = null;
            ObtenerCliente(vm.Descripcion);
        }

        function ObtenerCliente(descripcion) {
            var cliente = {
                IdEstado: 1,
                descripcion: descripcion,

            };
            var promise =ClienteService.ListarClientePorDescripcion(cliente);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                if (Respuesta != null) {
                    vm.GerenteComercial = Respuesta[0].GerenteComercial;
                    var IdDireccionComercial = Respuesta[0].IdDireccionComercial;
                    var IdSector = Respuesta[0].IdSector;
                    ObtenerDireccionComercial(IdDireccionComercial);
                    ObtenerSector(IdSector);

                }
            
            }, function (response) {
                blockUI.stop();

            });
        }
        function ObtenerSector(Id) {
            var sector = {
                IdEstado: 1,
                IdSector: Id,

            };
            var promise = SectorService.ObtenerSector(sector);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                vm.Sector = Respuesta.Descripcion;

            }, function (response) {
                blockUI.stop();

            });
        }
        function ObtenerDireccionComercial(Id) {
            var direccionComercial={
                IdEstado: 1,
                IdDireccion:Id

            }; 
            var promise = DireccionComercialService.ObtenerDireccionComercial(direccionComercial);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;                
                vm.DireccionComercial = Respuesta.Descripcion;

            }, function (response) {
                blockUI.stop();

            });
        }

        
        function ListarMaestraPorValor(Id) {
            var maestra={
                IdEstado: 1,
                Valor:Id,
                IdRelacion:11

            };
            var promise = MaestraService.ListarMaestraPorValor(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.DescripcionEstado=Respuesta[0].Descripcion;
            }, function (response) {
                blockUI.stop();

            });
        }
        function CargarTipoProyecto() {
            var maestra={
                IdEstado: 1,
                IdRelacion:7

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoProyecto = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function CargarTipoServicio() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 14

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                
                var Respuesta = resultado.data;
                vm.ListTipoServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function ObtenerOportunidadFlujoEstadoId(Id) {
            var oportunidadFlujoEstado = {
                IdOportunidad: Id
            }

            var promise = RegistrarOportunidadService.ObtenerOportunidadFlujoEstadoId(oportunidadFlujoEstado);
            promise.then(function (resultado) {
    

                var Respuesta = resultado.data;
                vm.IdEstadoOportunidad = Respuesta.IdEstado;
             
            }, function (response) {
               

            });
        }
        function RegistrarOportunidad(IdEstado) {
            vm.IdEstado=IdEstado;
            var IdSession = 0;
            var confirmacion = true;
            var IdAnalistaFin = vm.IdAnalistaFin;
            var IdProductManager = vm.IdProductManager;
            var IdCoordinadorFin = vm.IdCoordinadorFin;
            
            if (vm.IdCoordinadorFin == "-1") {
                IdCoordinadorFin = null;
            }
            if (vm.IdProductManager == "-1") {
                IdProductManager = null;
            } 
         
            if (vm.IdAnalistaFin == "-1") {
                IdAnalistaFin = null;
            }
            
            //if (vm.Implantacion == "") {
            //    var confirmacion = false;
            //    UtilsFactory.Alerta('#divAlert', 'warning', 'Ingrese Tiempo Implantación', 5);
            //}
            if (vm.IdTipoServicio == "-1") {
                var confirmacion = false;
                UtilsFactory.Alerta('#divAlert', 'warning', 'Seleccione Tipo Servicio', 5);
            }
            if (vm.IdLineaNegocio == "-1") {
                var confirmacion = false;
                UtilsFactory.Alerta('#divAlert', 'warning', 'Seleccione Linea de Negocio', 5);
            }
            if (vm.IdEstado == "-1") {
                var confirmacion = false;
                UtilsFactory.Alerta('#divAlert', 'warning', 'Seleccione Estado', 5);
            }

            if (vm.NumeroSalesForce == "" || vm.NumeroSalesForce == null || vm.NumeroSalesForce == 0) {
                var confirmacion = false;
                UtilsFactory.Alerta('#divAlert', 'warning', 'Ingrese NumeroSalesForce', 5);
            }

            if (vm.Oportunidad == "" || vm.Oportunidad == null || vm.Oportunidad == 0) {
                var confirmacion = false;
                UtilsFactory.Alerta('#divAlert', 'warning', 'Ingrese Descripción', 5);
            }
            if (vm.meses == "" || vm.meses == null || vm.meses == 0) {
                var confirmacion = false;
                UtilsFactory.Alerta('#divAlert', 'warning', 'Ingrese Periodo', 5);
            }

            if (vm.IdCliente == "" || vm.IdCliente == null || vm.IdCliente == 0) {
                var confirmacion = false;
                UtilsFactory.Alerta('#divAlert', 'warning', 'Ingrese Cliente', 5);
            }
            if (vm.fecha == "" || vm.fecha == null) {
                var confirmacion = false;
                UtilsFactory.Alerta('#divAlert', 'warning', 'Ingrese Fecha', 5);
            }

            var promise = OportunidadContenedorService.ObtenerIdOportunidadSession();
            promise.then(function (resultado) {

                var Respuesta = resultado.data;
                blockUI.stop();

                IdSession = Respuesta;


                vm.IdOportunidad = IdSession;
                var IdEstado = vm.IdEstado;

    
                if (IdSession != 0) {
                    var oportunidadFlujoEstado = {
                        IdEstado: vm.IdEstado,
                        IdOportunidad: vm.IdOportunidad
                    }

                    var promise = RegistrarOportunidadService.RegistrarOportunidadFlujoEstado(oportunidadFlujoEstado);
                    promise.then(function (resultado) {
                        blockUI.stop();

                        var Respuesta = resultado.data;
                        ObtenerOportunidadFlujoEstadoId(vm.IdOportunidad);
                        vm.IdEstadoOportunidad = Respuesta.Id;
                        if (vm.IdEstado == vm.IdEstadoOportunidad && vm.IdEstado != "1") {
                            vm.IdEstado = vm.IdEstadoOportunidad;
                            if (vm.IdEstado == 2) {
                                vm.AccionGrabar = false;
                            }

                            ListarMaestraPorValor(vm.IdEstado);
                        }
                        else {
                            var confirmacion = false;
                        }

                    }, function (response) {
                        blockUI.stop();

                    });
                }
             


                if (confirmacion) {
                    var oportunidad = {
                        IdOportunidad: vm.IdOportunidad,
                        IdCliente: vm.IdCliente,
                        Descripcion: vm.Oportunidad,
                        NumeroSalesForce: vm.NumeroSalesForce,
                        NumeroCaso: vm.NumeroCaso,
                        Fecha: vm.fecha,
                        Alcance: vm.AlcanceProyecto,
                        Periodo: vm.meses,
                        TiempoImplantacion: vm.Implantacion,
                        IdTipoServicio: vm.IdTipoServicio,
                        IdEstado: IdEstado,
                        IdAnalistaFinanciero: IdAnalistaFin,
                        IdProductManager: IdProductManager,
                        IdTipoProyecto: vm.IdTipoProyecto,
                        IdTipoCambio: vm.IdTipoCambio,
                        IdProyectoAnterior: vm.IdProyectoAnterior,
                        IdCoordinadorFinanciero: IdCoordinadorFin

                    };


                    var promise = (vm.IdOportunidad == 0) ? RegistrarOportunidadService.RegistrarOportunidad(oportunidad) : RegistrarOportunidadService.ActualizarOportunidad(oportunidad);
                    promise.then(function (resultado) {

                        var Respuesta = resultado.data;
                        blockUI.stop();

                        vm.IdOportunidad = Respuesta.Id

                        OportunidadContenedorService.RegistroIdOportunidadSession(vm.IdOportunidad);

                        // servicioPasoParametros.data.parametro = vm.IdOportunidad;

                        vm.AccionGrabar = !Respuesta.GrabarAccion;
                        ListarMaestraPorValor(vm.IdEstado);
                        RegistrarOportunidadFlujoEstado();
                        RegistrarOportunidadLineaNegocio();

                        UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);

                    }, function (response) {
                        blockUI.stop();

                    });
                }




            }, function (response) {
                blockUI.stop();

            });

          
            
        }
         function RegistrarOportunidadFlujoEstado() {


          var oportunidadFlujoEstado = {
                    IdEstado:vm.IdEstado,
                    IdOportunidad:vm.IdOportunidad
               
                }

                if(vm.ObjOportunidad == 0){
                   var promise =  RegistrarOportunidadService.RegistrarOportunidadFlujoEstado(oportunidadFlujoEstado);
                promise.then(function (resultado) {
                    blockUI.stop();

                    var Respuesta = resultado.data;
                    ObtenerOportunidadFlujoEstadoId(vm.IdOportunidad);


                    ListarMaestraPorValor(vm.IdEstado);

                }, function (response) {
                    blockUI.stop();

                });


             }

             
       
         }

        function RegistrarOportunidadLineaNegocio() {


            var oportunidadLineaNegocio = {
                IdOportunidadLineaNegocio:vm.IdOportunidadLineaNegocio,
                IdOportunidad:vm.IdOportunidad,
                IdLineaNegocio:vm.IdLineaNegocio

            };


            var promise = (vm.ObjOportunidad == 0) ? RegistrarOportunidadService.RegistrarOportunidadLineaNegocio(oportunidadLineaNegocio) : RegistrarOportunidadService.ActualizarOportunidadLineaNegocio(oportunidadLineaNegocio);
            promise.then(function (resultado) {
                blockUI.stop();
                
                var Respuesta = resultado.data;
                vm.IdOportunidadLineaNegocio = Respuesta.Id;

                var oportunidadLineaNegocio = {
                    IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio
                };
                if (Respuesta.Id!=0) {
                var promise = RegistrarOportunidadService.InsertarConfiguracionFlujo(oportunidadLineaNegocio);
                promise.then(function (resultado) {
                    blockUI.stop();

                }, function (response) {
                    blockUI.stop();
                });
               }
                $scope.$parent.vm.IdOportunidadLineaNegocio = vm.IdOportunidadLineaNegocio;
                $scope.$parent.vm.ValidaOportunidad;

            }, function (response) {
                blockUI.stop();

            });
        }

        function LimpiarFiltros() {
            vm.Nombre = '';
            vm.Descripcion = '';
            LimpiarGrilla();
        }
          var item = { "Codigo": "-1", "Descripcion": "--Seleccione--" };
        vm.ListNumeroCaso.splice(0, 0, item);
       
      
        function CargarListNUmeroCaso() {

             var item = { "Codigo": "-1", "Descripcion": "--Seleccione--" };
        vm.ListNumeroCaso.splice(0, 0, item);
        }

        function SelectNumeroSalesForce() {
            LimpiarCampoSalesFore();
      
      
            if (vm.NumeroSalesForce.length > 5) {
                ListarNumeroSalesForce();
            }
        }

        function fillTextboxNumeroSalesForce(string) {
            vm.NumeroSalesForce = string;
            vm.ListNumeroSalesForce = null;
            CargarNumeroCaso();
        }

        function ListarNumeroSalesForce() {
            if (vm.NumeroSalesForce != null || vm.NumeroSalesForce != "") {
                var salesForceConsolidadoCabecera = {
                    IdOportunidad: vm.NumeroSalesForce

                };
                var promise = SalesForceConsolidadoCabeceraService.ListarNumeroSalesForcePorIdOportunidad(salesForceConsolidadoCabecera);

                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta != null) {
                        vm.height=350;
                        }
               
                    var output = [];
                    angular.forEach(Respuesta, function (salesForceConsolidadoCabecera) {
                        output.push(salesForceConsolidadoCabecera);
                    });
                    vm.ListNumeroSalesForce = output;

                }, function (response) {
                    blockUI.stop();

                });
            }
        }
        function LimpiarCampoCliente() {
            vm.IdCliente = 0;
            vm.GerenteComercial = "";
            vm.Sector = "";
            vm.DireccionComercial = "";
        }
        function CargarNumeroCaso() {
            vm.height = "";

              var salesForceConsolidadoCabecera = {
                IdOportunidad: vm.NumeroSalesForce

            };
              var promise = SalesForceConsolidadoDetalleService.ListaNumerodeCasoPorNumeroSalesForceDetalle(salesForceConsolidadoCabecera);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                if (Respuesta.length > 0) {
                    $('#idNumeroCaso').removeAttr('disabled');
                    vm.ListNumeroCaso=[];
                    vm.ListNumeroCaso = UtilsFactory.AgregarItemSelect(Respuesta);
                    vm.NumeroCaso="-1";
                    ObtenerSalesForceConsolidadoDetalle();
                }
                else {
                    $('#idNumeroCaso').attr('disabled', 'disabled');
                  vm.NumeroCaso = "-1";
                  vm.Oportunidad="";
                  vm.meses ="";

                  vm.Sector="";
                  vm.GerenteComercial="";
                  vm.DireccionComercial="";
                  vm.Descripcion="";
                }

            }, function (response) {
                blockUI.stop();

            });
           
         
        }
        function ObtenerNumeroCaso(NumeroCaso) {

            var salesForceConsolidadoCabecera = {
                IdOportunidad: vm.NumeroSalesForce

            };
            var promise = SalesForceConsolidadoDetalleService.ListaNumerodeCasoPorNumeroSalesForceDetalle(salesForceConsolidadoCabecera);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
              
                  //  $('#idNumeroCaso').removeAttr('disabled');
                  
                    vm.ListNumeroCaso = UtilsFactory.AgregarItemSelect(Respuesta);
                    vm.NumeroCaso = parseInt(NumeroCaso);
     

            }, function (response) {
                blockUI.stop();

            });


        }
        function ObtenerSalesForceConsolidadoDetalle() {
          
                var salesForceConsolidadoDetalle = {
                    IdOportunidad: vm.NumeroSalesForce
                };
                var promise = SalesForceConsolidadoDetalleService.ObtenerSalesForceConsolidadoDetalle(salesForceConsolidadoDetalle);
                promise.then(function (resultado) {
                    blockUI.stop();

                    var Respuesta = resultado.data;
                  if(Respuesta!=null)
                  {
                      vm.Oportunidad=Respuesta.NombreOportunidad!=null?Respuesta.NombreOportunidad:"";
                      vm.meses = Respuesta.DuracionContrato>0?Respuesta.DuracionContrato:"";
                      vm.AccionGrabar = Respuesta.EstadoAccion;                    
                      var IdCliente = Respuesta.IdCliente > 0 ? Respuesta.IdCliente : vm.IdCliente = "";
                      if (IdCliente > 0) {                         
                          var cliente = {
                            IdCliente: IdCliente,
                            IdEstado:1
                             };
                               var promise = ClienteService.ObtenerCliente(cliente);
                               promise.then(function (resultado) {
                                   blockUI.stop();
                                   var Respuesta = resultado.data;                                  
                                   var CodigoCliente = Respuesta.CodigoCliente;
                                   if (CodigoCliente != "" && CodigoCliente!=null) {
                                       ObtenerClienteCodigo(CodigoCliente);
                                       vm.ListCliente = null;
                                   }
                                



                               }, function (response) {
                                   blockUI.stop();

                               });
                               }
                   }
                }, function (response) {
                    blockUI.stop();

                });
          

        }

        
        function ObtenerClienteCodigo(codigo) {

            var cliente = {
                IdEstado: 1,
                CodigoCliente: codigo

            };
            var promise = ClienteService.ObtenerClientePorCodigo(cliente);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                if (Respuesta != null) {
                    vm.Descripcion = Respuesta.Descripcion;
                    vm.IdCliente = Respuesta.IdCliente;
                    vm.GerenteComercial = Respuesta.GerenteComercial;
                    var IdDireccionComercial = Respuesta.IdDireccionComercial;
                    var IdSector = Respuesta.IdSector;
                    ObtenerDireccionComercial(IdDireccionComercial);
                    ObtenerSector(IdSector);

                }
            }, function (response) {
                blockUI.stop();

            });


        }
            function zeroFill( number, width )
            {
            width -= number.toString().length;
            if ( width > 0 )
            {
            return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
            }
            return number + ""; // siempre devuelve tipo cadena
            }


    }

})();