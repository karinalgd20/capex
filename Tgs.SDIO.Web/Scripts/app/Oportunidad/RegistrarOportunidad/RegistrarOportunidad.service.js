﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('RegistrarOportunidadService', RegistrarOportunidadService);

    RegistrarOportunidadService.$inject = ['$http', 'URLS'];

    function RegistrarOportunidadService($http, $urls) {

        var service = {
         
            ListarOportunidadPaginado: ListarOportunidadPaginado,
            RegistrarOportunidad: RegistrarOportunidad,
            ObtenerOportunidad:ObtenerOportunidad,
            EliminarOportunidad: EliminarOportunidad,
            ListarUsuariosPorPerfil: ListarUsuariosPorPerfil,
            Preventa:Preventa,
            ActualizarOportunidad:ActualizarOportunidad,
            ObtenerOportunidadLineaNegocio:ObtenerOportunidadLineaNegocio,
            ActualizarOportunidadLineaNegocio:ActualizarOportunidadLineaNegocio,
            RegistrarOportunidadLineaNegocio:RegistrarOportunidadLineaNegocio,
            ObtenerOportunidadFlujoEstadoId:ObtenerOportunidadFlujoEstadoId,
            ActualizarOportunidadFlujoEstado:ActualizarOportunidadFlujoEstado,
            RegistrarOportunidadFlujoEstado: RegistrarOportunidadFlujoEstado,
            NuevaVersion:NuevaVersion,
            InsertarConfiguracionFlujo: InsertarConfiguracionFlujo
               
        };

        return service;

        function NuevaVersion(request) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/NuevaVersion",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados); 

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ObtenerOportunidad(request) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/ObtenerOportunidad",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function RegistrarOportunidadFlujoEstado(request) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/RegistrarOportunidadFlujoEstado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ActualizarOportunidadFlujoEstado(request) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/ActualizarOportunidadFlujoEstado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ObtenerOportunidadFlujoEstadoId(request) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/ObtenerOportunidadFlujoEstadoId",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ActualizarOportunidad(request) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/ActualizarOportunidad",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
        function Preventa() {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/Preventa",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarUsuariosPorPerfil(usuario_Admin) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/ListarUsuariosPorPerfil",
                method: "POST",
                data: JSON.stringify(usuario_Admin)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


        function ListarOportunidadPaginado(oportunidad) {
            return $http({
                url: $urls.ApiOportunidad+ "RegistrarOportunidad/ListarOportunidad",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarOportunidad(oportunidad) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/EliminarOportunidad",
                method: "POST",
                data: JSON.stringify(RegistrarOportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
     
        function RegistrarOportunidad(oportunidad) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/RegistrarOportunidad",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarOportunidadLineaNegocio(oportunidadLineaNegocio) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/RegistrarOportunidadLineaNegocio",
                method: "POST",
                data: JSON.stringify(oportunidadLineaNegocio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarOportunidadLineaNegocio(oportunidadLineaNegocio) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/ActualizarOportunidadLineaNegocio",
                method: "POST",
                data: JSON.stringify(oportunidadLineaNegocio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerOportunidadLineaNegocio(oportunidadLineaNegocio) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/ObtenerOportunidadLineaNegocio",
                method: "POST",
                data: JSON.stringify(oportunidadLineaNegocio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function InsertarConfiguracionFlujo(oportunidadLineaNegocio) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarOportunidad/InsertarConfiguracionFlujo",
                method: "POST",
                data: JSON.stringify(oportunidadLineaNegocio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
    }
})();