﻿(function () {
    'use strict',
    angular
    .module('app.Oportunidad')
    .service('BandejaCotizacionService', BandejaCotizacionService);

    BandejaCotizacionService.$inject = ['$http', 'URLS'];

    function BandejaCotizacionService($http, $urls) {

        var service = {
            VistaBandejaCotizacion: VistaBandejaCotizacion
        };

        return service;

        function VistaBandejaCotizacion() {
            return $http({
                url: $urls.ApiOportunidad + "BandejaCotizacion/Index",
                method: "GET",
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
    }
})();