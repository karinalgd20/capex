﻿(function () {
    'use strict'
    angular
        .module('app.Oportunidad')
        .controller('BandejaCotizacion', BandejaCotizacion);

    BandejaCotizacion.$inject =
        [
        'CotizacionService',
        'blockUI',
        'UtilsFactory',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        'MensajesUI',
        '$timeout',
        'URLS',
        '$scope',
        '$compile',
        '$modal',
        '$injector'
        ];

    function BandejaCotizacion
        (
        CotizacionService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        MensajesUI,
        $timeout,
        $urls,
        $scope,
        $compile,
        $modal,
        $injector
        ) {

        var vm = this;
        vm.TituloModal = '';

        vm.dtColumns = [
         DTColumnBuilder.newColumn('Id').notVisible(),
         DTColumnBuilder.newColumn('IdSedeInstalacion').withTitle('IdSedeInstalacion').notSortable(),
         DTColumnBuilder.newColumn('CodSisegoCotizacion').withTitle('Cod. Sisego').notSortable(),
         DTColumnBuilder.newColumn('Nombre').withTitle('Nombre').notSortable(),
         DTColumnBuilder.newColumn('TotalSoles').withTitle('Total Soles').notSortable(),
         DTColumnBuilder.newColumn('TotalDolares').withTitle('Total Dolares').notSortable(),
         DTColumnBuilder.newColumn('Dias').withTitle('Dias Ejecución').notSortable(),
         DTColumnBuilder.newColumn('strFechaEnvioPresupuesto').withTitle('Fecha Envío Presupuesto').notSortable(),
         DTColumnBuilder.newColumn('EstadoCotizacion').withTitle('EstadoCotizacion').notSortable(),
        ];

        vm.ListarCotizacion = ListarCotizacion;
        LimpiarGrilla();
        ListarCotizacion();


        function ListarCotizacion() {
            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(ListarCotizacionPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }

        function ListarCotizacionPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var Cotizacion = {
                IdSedeInstalacion: $scope.$parent.vm.IdSedeInstalacion,
                IdSede: $scope.$parent.vm.IdSede,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = CotizacionService.ListarCotizacionPaginado(Cotizacion);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaCotizacion
                };

                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
    }
})();