﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('RegistrarOportunidadTipoCambioService', RegistrarOportunidadTipoCambioService);

    RegistrarOportunidadTipoCambioService.$inject = ['$http', 'URLS'];

    function RegistrarOportunidadTipoCambioService($http, $urls) {

        var service = {

            ModalOportunidadTipoCambio: ModalOportunidadTipoCambio,
            RegistrarOportunidadTipoCambio: RegistrarOportunidadTipoCambio,
            ObtenerOportunidadTipoCambio: ObtenerOportunidadTipoCambio,
            ActualizarOportunidadTipoCambio: ActualizarOportunidadTipoCambio
        };

        return service;
        function ActualizarOportunidadTipoCambio(request) {

            return $http({
                url: $urls.ApiOportunidad + "RegistrarTipoCambio/ActualizarOportunidadTipoCambioDetalle",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ModalOportunidadTipoCambio(request) {

            return $http({
                url: $urls.ApiOportunidad + "RegistrarTipoCambio/Index",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarOportunidadTipoCambio(request) {

            return $http({
                url: $urls.ApiOportunidad + "RegistrarTipoCambio/RegistrarOportunidadTipoCambio",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerOportunidadTipoCambio(request) {

            return $http({
                url: $urls.ApiOportunidad + "RegistrarTipoCambio/ObtenerOportunidadTipoCambioDetalle",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();