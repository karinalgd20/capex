﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('RegistrarOportunidadTipoCambio', RegistrarOportunidadTipoCambio);

    RegistrarOportunidadTipoCambio.$inject = ['RegistrarOportunidadTipoCambioService','MaestraService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarOportunidadTipoCambio(RegistrarOportunidadTipoCambioService,MaestraService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.CargaModalOportunidadTipoCambio = CargaModalOportunidadTipoCambio;
        vm.ActualizarOportunidadTipoCambio = ActualizarOportunidadTipoCambio;
        vm.ObtenerOportunidadTipoCambio = ObtenerOportunidadTipoCambio;



        CargaModalOportunidadTipoCambio();
        vm.Anio = 0;
        vm.Monto = 0;

        ListarTipoMoneda();
        ListarTipoCosto();
        function CargaModalOportunidadTipoCambio() {
            vm.IdTipoCambioDetalle = $scope.$parent.vm.IdTipoCambioDetalle;
            (vm.IdTipoCambioDetalle > 0) ? ObtenerOportunidadTipoCambio() : CargaModal();


        }
        function CargaModal() {


        }


        
        function ListarTipoCosto() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 58

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListTipoCosto = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }
        function ListarTipoMoneda() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 98

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListTipoMoneda = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ObtenerOportunidadTipoCambio() {
            blockUI.start();
           

            var OportunidadTipoCambio = {
                IdTipoCambioDetalle: vm.IdTipoCambioDetalle,
                IdEstado:1
            }

            var promise = RegistrarOportunidadTipoCambioService.ObtenerOportunidadTipoCambio(OportunidadTipoCambio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.IdTipoCosto = Respuesta.IdTipificacion;
                vm.IdTipoMoneda = Respuesta.IdMoneda;

                vm.Anio = Respuesta.Anio;
                vm.Monto = Respuesta.Monto



            }, function (response) {
                blockUI.stop();

            });
        }


        function ActualizarOportunidadTipoCambio() {

            var oportunidadTipoCambio = {
                IdTipoCambioDetalle:vm.IdTipoCambioDetalle,
                Anio: vm.Anio,
                Monto: vm.Monto
            }



            var promise = RegistrarOportunidadTipoCambioService.ActualizarOportunidadTipoCambio(oportunidadTipoCambio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                UtilsFactory.Alerta('#divAlert_RegistrarOportunidadTipoCambio', 'success', "Se actualizo con exito registro.", 5);

                $scope.$parent.vm.BuscarOportunidadTipoCambio();
            }, function (response) {
                blockUI.stop();

            });
        }






    }
})();








