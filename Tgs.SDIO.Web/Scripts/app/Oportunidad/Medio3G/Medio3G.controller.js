﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('Medio3GController', Medio3GController);

    Medio3GController.$inject = ['Medio3GService', 'FlujoCajaService','MedioService', 'MaestraService','CostoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function Medio3GController(Medio3GService, FlujoCajaService, MedioService, MaestraService,CostoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.tabFlujoCaja = true;
        vm.idCircuito = 0;
        vm.idOportunidadLineaNegocio = 0;

        vm.servicio = "";
        vm.circuito = "";
        vm.cantidad = 0;
        vm.BW = "";

        vm.GrabarMedio3G = GrabarMedio3G;

        vm.ListMedio = [];
        vm.IdMedio = "8";

        vm.ListTipoEnlace = [];
        vm.IdTipoEnlace = "-1";

        vm.ListLocalidad = [];
        vm.IdLocalidad = "-1";

        vm.IdFlujoCaja = $scope.$parent.vm.Oportunidad.IdFlujoCaja;
        vm.IdOportunidadLineaNegocio = $scope.$parent.vm.IdOportunidadLineaNegocio;
        vm.IdSubServicioDatosCapex = 0;
        vm.IdFlujoCajaConfiguracion = 0;
        vm.IdPeriodos = 0; 

        vm.ListBW = [];
        vm.IdOportunidadCosto = -1;
        vm.OportunidadCosto = 0;
        vm.CargarBW = CargarBW;
        vm.negativo = "-1";
        vm.ListaModelos = [];
        vm.ObtieneCostoPreOperativo = ObtieneCostoPreOperativo;
        vm.MontoBW = 0;
        /******************************************* ServicioCMI *******************************************/

        function GrabarMedio3G() {

            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();
                var costo = {
                    IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                    IdCosto: vm.IdOportunidadCosto,
                    IdOportunidadCosto: vm.OportunidadCosto,
                    Monto: vm.MontoBW
                };
                var IdOportunidadCosto = 0;

                var promise = FlujoCajaService.RegistrarOportunidadCosto(costo);
                promise.then(function (response) {
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#lblAlerta_Circuito', 'danger', MensajesUI.DatosError, 20);

                    } else {
                        IdOportunidadCosto = Respuesta.Id;

                        var flujocaja = {
                            IdFlujoCaja: vm.IdFlujoCaja,
                            IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                            Cantidad: vm.cantidad,
                            CostoUnitario: 0,
                            IdOportunidadCosto: IdOportunidadCosto
                        };

                        var promise = FlujoCajaService.ActualizarOportunidadFlujoCaja(flujocaja);
                        promise.then(function (response) {
                            var Respuesta = response.data;
                            if (Respuesta.TipoRespuesta != 0) {
                                blockUI.stop();
                                UtilsFactory.Alerta('#lblAlerta_Medio3G', 'danger', MensajesUI.DatosError, 20);
                            } else {

                                var caratula = {
                                    IdFlujoCaja: vm.IdFlujoCaja,
                                    IdSubServicioDatosCaratula: vm.IdSubServicioDatosCaratula,
                                    Circuito: vm.circuito,
                                    IdMedio: vm.IdMedio,
                                    IdTipoEnlace: vm.IdTipoEnlace,
                                    IdLocalidad: vm.IdLocalidad,
                                    Cantidad: vm.cantidad,
                                    IdEstado:1
                                };

                                var promise = FlujoCajaService.ActualizarSubServicioCaratula(caratula);
                                $scope.$$childTail.vm.GrabarFlujoCajaConfiguracion();

                                promise.then(function (response) {

                                    var Respuesta = response.data;
                                    if (Respuesta.TipoRespuesta != 0) {
                                        blockUI.stop();
                                        UtilsFactory.Alerta('#lblAlerta_Medio3G', 'danger', Respuesta.Mensaje, 20);
                                    } else {
                                        blockUI.stop();
                                        UtilsFactory.Alerta('#lblAlerta_Medio3G', 'success', Respuesta.Mensaje, 10);
                                        $scope.$$childTail.vm.GrabarFlujoCajaConfiguracion();
                                        $scope.$parent.vm.BuscarCaratula();
                                    }

                                }, function (response) {
                                    blockUI.stop();
                                    UtilsFactory.Alerta('#lblAlerta_Medio3G', 'danger', MensajesUI.DatosError, 5);
                                });
                            }
                        }, function (response) {
                            blockUI.stop();
                            UtilsFactory.Alerta('#lblAlerta_Medio3G', 'danger', MensajesUI.DatosError, 5);
                        });


                    }

                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Circuito', 'danger', MensajesUI.DatosError, 5);
                });

            };
        }

        /******************************************* Funciones *******************************************/

        function CargarMedio3G() {
            var dto = {
                IdSubServicioDatosCaratula: $scope.$parent.vm.dto.IdSubServicioDatosCaratula,
                IdGrupo: 2
            };

            var promise = FlujoCajaService.ObtenerDetalleOportunidadCaratula(dto);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;

                vm.servicio = respuesta.SubServicio;
                vm.IdMedio = (respuesta.IdMedio == 0) ? vm.negativo : respuesta.IdMedio;
                CargarBW();
                vm.IdTipoEnlace = (respuesta.IdTipoEnlace == 0) ? vm.negativo : respuesta.IdTipoEnlace;
                vm.IdLocalidad = (respuesta.IdLocalidad == 0) ? vm.negativo : respuesta.IdLocalidad;
                vm.cantidad = respuesta.Cantidad;
                vm.circuito = respuesta.Circuito;

                vm.IdOportunidadCosto = (respuesta.IdCosto == 0 || respuesta.IdCosto == null) ? vm.negativo : respuesta.IdCosto;
                vm.OportunidadCosto = respuesta.IdOportunidadCosto;
                vm.IdFlujoCaja = respuesta.IdFlujoCaja;
                vm.IdSubServicioDatosCaratula = respuesta.IdSubServicioDatosCaratula;
                vm.IdFlujoCajaConfiguracion = respuesta.IdFlujoCajaConfiguracion;
                vm.IdPeriodos = respuesta.IdPeriodos==0?"2":respuesta.IdPeriodos;
                
                //$scope.$$childTail.vm.Meses = respuesta.TiempoProyecto;
                $scope.$$childTail.vm.ObtenerFlujoCajaConfiguracion();

            }, function (response) {
                blockUI.stop();

            });
        };

        function ListarMedio() {
            var medio = {
                IdEstado: 1
            };
            var promise = MedioService.ListarMedio(medio);
            
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListMedio = UtilsFactory.AgregarItemSelect(Respuesta);
                UtilsFactory.DeshabilitarElemento("#ddlMedio");
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarBW() {
            var medio = {
                IdMedio: vm.IdMedio,
                IdTipoCosto: 3
            };
            var promise = CostoService.ListarCostos(medio);
            promise.then(function (resultado) {
                vm.ListaModelos = resultado.data;
                vm.ListBW = UtilsFactory.AgregarItemSelect(vm.ListaModelos);
                ObtieneCostoPreOperativo();
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarTipoEnlace() {
            var filtroTipoEnlace = {
                IdRelacion: 89
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipoEnlace);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoEnlace = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarLocalidad() {
            var filtroLocalidad = {
                IdRelacion: 95
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroLocalidad);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListLocalidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarCombos() {

            ListarMedio();
            CargarTipoEnlace();
            CargarLocalidad();
        };

        function ObtieneCostoPreOperativo() {
            var monto = 0;
            for (var i = 0; i < vm.ListaModelos.length; i++) {
                if (vm.ListaModelos[i].IdCosto == vm.IdOportunidadCosto) {
                    vm.MontoBW = vm.ListaModelos[i].Monto;
                    monto = vm.ListaModelos[i].Monto;
                    break;
                }
            }
            var cantidad = (vm.cantidad == null) ? 0 : vm.cantidad;
            $scope.$$childTail.vm.CostoPreOperativo = (cantidad * monto);
        };

        /******************************************* LOAD *******************************************/
        CargarCombos();
        CargarMedio3G();
    }
})();



