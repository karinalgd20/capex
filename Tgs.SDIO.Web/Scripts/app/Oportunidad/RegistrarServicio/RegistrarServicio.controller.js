﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('RegistrarServicioController', RegistrarServicioController);

    RegistrarServicioController.$inject = ['RegistrarServicioService', 'ServicioSubServicioService', 'ServicioService', 'FlujoCajaService',
   'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarServicioController(RegistrarServicioService, ServicioSubServicioService, ServicioService, FlujoCajaService, 
    blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.ListarServicios = ListarServicios;
        vm.Descripcion = '';
        vm.ListServicio = [];
        vm.IdServicio = '-1';
        vm.BuscarSubServicios = BuscarSubServicios;


        vm.selectedServicio = {};
        vm.IdServicioSubServicio = "";
        vm.selectAllServicio = false;
        vm.toggleAllServicio = toggleAllServicio;
        vm.toggleOneServicio = toggleOneServicio;
        vm.RegistrarServicioComponente = RegistrarServicioComponente;


        var titleHtmlServicio = '<input type="checkbox" ng-model="vm.selectAllServicio" ng-click="vm.toggleAllServicio(vm.selectAllServicio, vm.selectedServicio)">';
        /******************************************* Tabla *******************************************/
        vm.dtInstanciaServicioModal = [];
        vm.dtOptionsServiciosModal = {};
        vm.dtColumnsServiciosModal = [


         DTColumnBuilder.newColumn(null).withTitle(titleHtmlServicio)
        .notSortable().withOption('width', '3%')
        .renderWith(function (data, type, full, meta) {
            vm.selectedServicio[full.IdServicioSubServicio] = false;
            return '<input type="checkbox" ng-model="vm.selectedServicio[\'' + data.IdServicioSubServicio + '\']" ng-click="vm.toggleOneServicio(vm.selectedServicio)">';
        }),
         DTColumnBuilder.newColumn('DescripcionSubServicio').withTitle('SubServicio').notSortable(),
         DTColumnBuilder.newColumn('Pestana').withTitle('Pestaña').notSortable(),
         DTColumnBuilder.newColumn('Grupo').withTitle('Grupo').notSortable(),
         DTColumnBuilder.newColumn('DescripcionTipoCosto').withTitle('Tipo Costo').notSortable()
        ];



        function toggleAllServicio(selectAllServicio, selectedServicioItems) {
            for (var IdServicioSubServicio in selectedServicioItems) {
                if (selectedServicioItems.hasOwnProperty(IdServicioSubServicio)) {
                    selectedServicioItems[IdServicioSubServicio] = selectAllServicio;
                }
            }
        }

        function toggleOneServicio(selectedServicioItems) {
            for (var IdServicioSubServicio in selectedServicioItems) {
                if (selectedServicioItems.hasOwnProperty(IdServicioSubServicio)) {
                    if (!selectedServicioItems[IdServicioSubServicio]) {
                        vm.selectAllServicio = false;
                        return;
                    }
                }
            }
            vm.selectAllServicio = true;
        }

        function BuscarSubServicios() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsServiciosModal = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarSubServiciosPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('headerCallback', function (header) {
                        $compile.headerCompiled = true;
                        $compile(angular.element(header).contents())($scope);
                    });
            }, 500);
        };

        function BuscarSubServiciosPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var servicio = {
                IdServicio: vm.IdServicio,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ServicioSubServicioService.ListarServicioSubServicioGrupos(servicio);
            promise.then(function (response) {

                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListServicioSubServicioDtoResponse
                };

                blockUI.stop();
                fnCallback(records);
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsServiciosModal = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
        };




        function RegistrarServicioComponente() {

            $scope.$parent.vm.TabPorPerfilOportunidadDatos();
            var confirmar = true;
            if (vm.IdServicio == "-1") {
                UtilsFactory.Alerta('#lblAlerta_AgregarServicio', 'danger', "Seleccione el Servicio", 5);
                confirmar = false;
            }


            var Servicio = {
                IdServicio: vm.IdServicio,
                ListServicioSelect: ObtenerIdSeleccionado(vm.selectedServicio),
                IdOportunidadLineaNegocio: $scope.$parent.$parent.vm.IdOportunidadLineaNegocio

            }
            


            if (confirmar) {
                blockUI.start();
                var promisePreventa = FlujoCajaService.RegistrarServicioComponente(Servicio);

                promisePreventa.then(function (resultadoPreventa) {
                    var RespuestaPreventa = resultadoPreventa.data;
                    if (RespuestaPreventa.TipoRespuesta == 0) {

                        $('#ModalAgregarServicio').modal('hide');
                        blockUI.stop();
                        UtilsFactory.Alerta('#lblAlerta_AgregarServicio', 'success', MensajesUI.DatosOk, 5);
                        $scope.$parent.vm.CargaECapex();
                        $scope.$parent.vm.CargaCaratula();
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#lblAlerta_AgregarServicio', 'danger', "No se pudo registrar la Servicio.", 5);
                    }

                }, function (response) {
                    UtilsFactory.Alerta('#lblAlerta_AgregarServicio', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            } 
        }

        function ObtenerIdSeleccionado(dato) {
            var array = [];
            for (var i in dato) {
                if (dato[i] == true) {
                    array.push({ "id": i });
                }
            }
            return array;
        }

        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#txtNombre', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtRuc', 'Ninguno');
        }

        /******************************************* Funciones *******************************************/

        function ListarServicios() {
            var servicio = {
                IdEstado: 1
            };
            var promise = ServicioService.ListarServicios(servicio);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        };


  
        /******************************************* Load *******************************************/
        ListarServicios();
        BuscarSubServicios();
    }
})();