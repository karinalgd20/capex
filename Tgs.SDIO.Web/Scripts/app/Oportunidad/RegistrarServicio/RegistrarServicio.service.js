﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('RegistrarServicioService', RegistrarServicioService);

    RegistrarServicioService.$inject = ['$http', 'URLS'];

    function RegistrarServicioService($http, $urls) {

        var service = {
            ModalServicios: ModalServicios
        };

        return service;


        function ModalServicios(request) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarServicio/Index",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})(); 