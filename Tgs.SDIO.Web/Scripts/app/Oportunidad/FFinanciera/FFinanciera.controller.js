﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('FFinancieraController', FFinancieraController);

    FFinancieraController.$inject = ['FlujoCajaService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function FFinancieraController(FlujoCajaService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.Oportunidad = $scope.$parent.vm.ObjOportunidad;
        vm.IdOportunidadLineaNegocio = vm.Oportunidad.IdOportunidadLineaNegocio;
        vm.IdLineaNegocio = vm.Oportunidad.IdLineaNegocio;
        vm.IdUsuario = vm.Oportunidad.IdUsuarioCreacion;
        vm.IdTipoFicha = 2;

        vm.GenerarProyectado = GenerarProyectado;

        function GenerarProyectado() {
            if (vm.IdLineaNegocio == undefined || vm.IdLineaNegocio <= 0) {
                alert("seleccione una linea");
                return;
            }
            blockUI.start();
            var dto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdUsuarioCreacion: vm.IdUsuario,
                TipoFicha: vm.IdTipoFicha
            };

            var parametros = "reporte=FlujoCajaProyectado&IdOportunidadLineaNegocio=" + vm.IdOportunidadLineaNegocio +
                "&IdEstado=" + 1 +
                "&TipoFicha=" + vm.IdTipoFicha;

            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
            vm.FlagMostrarReporte = true;

            blockUI.stop();
            UtilsFactory.Alerta('#lblAlerta_FFinanciera', 'success', MensajesUI.DetalleGeneradoOk, 5);

        };

        function Cargar() {

        };


        /******************************************* LOAD *******************************************/
        Cargar();
    }
})();



