﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('FFinancieraService', FFinancieraService);

    FFinancieraService.$inject = ['$http', 'URLS'];

    function FFinancieraService($http, $urls) {

        var service = {
            VistaFFinanciera: VistaFFinanciera
        };
         
        return service;

        function VistaFFinanciera(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FFinanciera/Index",
                method: "GET",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
              
            function DatosCompletados(response) {  
                return response;
            }
        };

    }
})();