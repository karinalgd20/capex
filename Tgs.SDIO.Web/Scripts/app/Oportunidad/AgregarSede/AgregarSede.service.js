﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('AgregarSedeService', AgregarSedeService);

    AgregarSedeService.$inject = ['$http', 'URLS'];

    function AgregarSedeService($http, $urls) {

        var service = {
            VistaAgregarSede: VistaAgregarSede,
            ModalEditar: ModalEditar,
            Obtener: Obtener,
            ListarSedeCliente: ListarSedeCliente,
            Registrar: Registrar,
            obtenerClientePorIdOportunidad: obtenerClientePorIdOportunidad
        };

        return service;

        function VistaAgregarSede() {
            return $http({
                url: $urls.ApiOportunidad + "AgregarSede/Index",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalEditar(dto) {
            return $http({
                url: $urls.ApiOportunidad + "AgregarSede/Editar",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function Obtener(dto) {
            return $http({
                url: $urls.ApiOportunidad + "AgregarSede/ObtenerSedePorIdOportunidad",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function Registrar(dto) {
            return $http({
                url: $urls.ApiOportunidad + "AgregarSede/RegistrarSede",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarSedeCliente(dto) {
            return $http({
                url: $urls.ApiOportunidad + "AgregarSede/ListarSedeCliente",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function obtenerClientePorIdOportunidad(dto) {
            return $http({
                url: $urls.ApiOportunidad + "AgregarSede/obtenerClientePorIdOportunidad",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        

    }
})();