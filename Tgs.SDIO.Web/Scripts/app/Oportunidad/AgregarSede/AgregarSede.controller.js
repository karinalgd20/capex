﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('AgregarSedeController', AgregarSedeController);

    AgregarSedeController.$inject = ['AgregarSedeService', 'SedeService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function AgregarSedeController(AgregarSedeService, SedeService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad
        vm.Descripcion = '';
        vm.ListServicio = [];
        vm.IdServicio = '-1';
        vm.BuscarSedeCliente = BuscarSedeCliente;
        vm.selectedSede = {};
        vm.Id = "";
        vm.selectAllSede = false;
        vm.toggleAllSede = toggleAllSede;
        vm.toggleOneSede = toggleOneSede;
        vm.AgregarSedeInstalacion = AgregarSedeInstalacion;


        var titleHtmlSede = '<input type="checkbox" ng-model="vm.selectAllSede" ng-click="vm.toggleAllSede(vm.selectAllSede, vm.selectedSede)">';

        /******************************************* Tabla *******************************************/
        vm.dtInstanciaServicioModal = [];
        vm.dtOptionsServiciosModal = {};
        vm.dtColumnsSedeModal = [

                    DTColumnBuilder.newColumn(null).withTitle(titleHtmlSede)
            .notSortable().withOption('width', '3%')
            .renderWith(function (data, type, full, meta) {
                vm.selectedSede[full.Id] = false;
                return '<input type="checkbox" ng-model="vm.selectedSede[\'' + data.Id + '\']" ng-click="vm.toggleOneSede(vm.selectedSede)">';
            }),

         DTColumnBuilder.newColumn('Id').withTitle('Id Sede').notSortable(),
         DTColumnBuilder.newColumn('Direccion').withTitle('Dirección').notSortable(),
         DTColumnBuilder.newColumn('Departamento').withTitle('Departamento').notSortable(),
         DTColumnBuilder.newColumn('Provincia').withTitle('Provincia').notSortable(),
         DTColumnBuilder.newColumn('Distrito').withTitle('Distrito').notSortable(),
        ];

        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#txtNombre', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtRuc', 'Ninguno');
        }

        function ValidarCampos() {
            var mensaje = "";
            if (JSON.stringify(vm.selectedSede).indexOf("true") == -1) {
                mensaje = mensaje + "Seleccione Sede" + '<br/>';
                UtilsFactory.InputBorderColor('#gridSedeCliente', 'Rojo');
            }
            return mensaje;
        }


        function toggleAllSede(selectAllSede, selectedSedeItems) {
            for (var Id in selectedSedeItems) {
                if (selectedSedeItems.hasOwnProperty(Id)) {
                    selectedSedeItems[Id] = selectAllSede;
                }
            }
        }

        function toggleOneSede(selectedSedeItems) {
            for (var Id in selectedSedeItems) {
                if (selectedSedeItems.hasOwnProperty(Id)) {
                    if (!selectedSedeItems[Id]) {
                        vm.selectAllSede = false;
                        return;
                    }
                }
            }
            vm.selectAllSede = true;
        }

        function BuscarSedeCliente() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsServiciosModal = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarSedePaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
               .withOption('headerCallback', function (header) {
                   $compile.headerCompiled = true;
                   $compile(angular.element(header).contents())($scope);
               });
            }, 500);
        };

        function BuscarSedePaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var oportunidad = {
                IdOportunidad: vm.IdOportunidad,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = AgregarSedeService.ListarSedeCliente(oportunidad);
            promise.then(function (response) {
            obtenerClientePorIdOportunidad();
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListSedeDto
                };

                blockUI.stop();
                fnCallback(records);
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsServiciosModal = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
        };

        function AgregarSedeInstalacion() {
            var Sede = {
                    IdOportunidad: vm.IdOportunidad,
                    ListSedeSelect: ObtenerIdSeleccionado(vm.selectedSede),
                    IdEstado: vm.IdEstado,
            }
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promisePreventa = SedeService.AgregarSedeInstalacion(Sede);

                promisePreventa.then(function (resultadoPreventa) {
                    var RespuestaPreventa = resultadoPreventa.data;
                    if (RespuestaPreventa.TipoRespuesta == 0) {
                        $scope.$parent.vm.BuscarListaSedeInstalacion();
                        $('#ModalAgregarSede').modal('hide');
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar la Sede de Instalación.", 5);
                    }

                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            } else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }

        function ObtenerIdSeleccionado(dato) {
            var array = [];
            for (var i in dato) {
                if (dato[i] == true) {
                    array.push({ "id": i });
                }
            }
            return array;
        }

        /******************************************* Funciones *******************************************/

        function obtenerClientePorIdOportunidad() {
            blockUI.start();

           var oportunidad = {
                IdOportunidad: vm.IdOportunidad,
            };

           var promise = AgregarSedeService.obtenerClientePorIdOportunidad(oportunidad);

                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                    vm.Nombre = Respuesta.Descripcion;
                    vm.Ruc = Respuesta.NumeroIdentificadorFiscal;

                }, function (response) {
                    blockUI.stop();
                });
        };
    }
})();