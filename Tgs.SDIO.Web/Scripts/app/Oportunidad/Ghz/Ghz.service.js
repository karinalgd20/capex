﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('GhzService', GhzService);

    GhzService.$inject = ['$http', 'URLS'];

    function GhzService($http, $urls) {

        var service = {
            ModalGhz: ModalGhz,
            CantidadGhz: CantidadGhz
        };

        return service;

        function ModalGhz(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Ghz/Index",
                method: "POST",
                data: JSON.stringify(dto) 
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function CantidadGhz(dto) {
            return $http({
                url: $urls.ApiOportunidad + "ECapex/CantidadCapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };        

    }
})();