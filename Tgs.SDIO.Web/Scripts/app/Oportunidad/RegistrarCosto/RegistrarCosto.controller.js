﻿(function () {
    'use strict'

    angular
    .module('app.Comun')
    .controller('RegistrarCostoController', RegistrarCostoController);

    RegistrarCostoController.$inject = ['RegistrarCostoService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarCostoController(RegistrarCostoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.TitleModelo = "";
        vm.TitleCostoSegmentoSatelital = "";
        vm.TitleDescripcion = "";
        vm.TitleInstalacion = "";
        vm.TitleGarantizado = "";
        vm.TitleVelocidadSubidaKBPS = "";


        vm.CodigoModelo = "";
        vm.Modelo = "";
        vm.Descripcion = "";
        vm.Montom = "";
        vm.Instalacion = "";
        vm.CostoSegmentoSatelital = "";


        vm.calculoMonto = calculoMonto;
          function LimpiarValidacion() {

            vm.CodigoModelo = "";
            vm.Modelo = "";
            vm.Descripcion = "";
            vm.Montom = "";
            vm.Instalacion = "";
            vm.CostoSegmentoSatelital = "";
            vm.One = true;
            vm.Two = true;
            vm.Three = true;
            vm.Four = true;
            vm.Five = true;
            vm.Six = true;
            vm.Seven = true;
            vm.Eigth = true;
            vm.Nine = true;
            vm.Teen = true;
            vm.Eleven = true;
            vm.Twelve = true;
            vm.Thirteen = true;
            vm.Fourteen = true;
            vm.Fifteen= true;

              


            vm.Onetxt = false;
            vm.Twotxt = false;
            vm.Threetxt = false;
            vm.Fourtxt = false;
            vm.Fivetxt = false;
            vm.Sixtxt = false;
            vm.Seventxt = false;
            vm.Eigthtxt = false;
            vm.Ninetxt = false;
            vm.Teentxt = false;
            vm.Eleventxt = false;
          

        }

        vm.CargaModalCosto = CargaModalCosto;

        vm.RegistrarCosto = RegistrarCosto;
        CargaModalCosto();
        
        function CargaModalCosto() {
            vm.IdOportunidadCosto = $scope.$parent.vm.IdOportunidadCosto;
            vm.IdTipoCosto = $scope.$parent.vm.IdTipoCosto; 
            ValidarTipoCosto();

            (vm.IdOportunidadCosto > 0) ? ObtenerCosto() : CargaModal();
        }
        function CargaModal() {        
        vm.Monto="";
           
        }

         function calculoMonto() {        
             if (vm.IdTipoCosto == "7") {
                 if (vm.PorcentajeGarantizado > 0) {
                     vm.Monto = vm.Monto * vm.PorcentajeGarantizado;
                 }
           
             }
         

             if (vm.IdTipoCosto == "8") {
                 if (vm.ProcentajeSobresuscripcion > 0 && vm.PorcentajeGarantizado>0) {
                     if (vm.Descripcion > 0) {
                        vm.VelocBajada = vm.Descripcion * vm.PorcentajeGarantizado * vm.ProcentajeSobresuscripcion;
                     }
                     if (vm.VelocidadSubidaKBPS > 0) {

                         vm.VelocSubida = vm.VelocidadSubidaKBPS * vm.PorcentajeGarantizado * vm.ProcentajeSobresuscripcion;
                     }
                   
                 }
                   vm.InvTotal = vm.vm.InvAntenaHubUSD + vm.AntenaCasaClienteUSD;
             }
          

        
        }

        function ValidarTipoCosto() {
            LimpiarValidacion();
   
  
            if (vm.IdTipoCosto == "1") {
                vm.Seven = false;
                vm.Three = false;
                vm.Four = false;
                vm.Five = false;
                vm.One = false;
                vm.Eigth = false;
                vm.TitleModelo = "Modelo";
                vm.TitleCostoSegmentoSatelital = "Costo Desistalación";
                vm.TitleDescripcion = "Descripción";
                vm.TitleInstalacion = "Costo Instalación";
               
                vm.Seventxt = true;
                vm.Onetxt = true;
                vm.Fourtxt = true;
            }
            if (vm.IdTipoCosto == "2") {
         
                vm.Four = false;
                vm.Eigth = false;
                vm.TitleDescripcion = "Descripción";
                vm.Eigthtxt = true;
            }
            if (vm.IdTipoCosto == "3") {
                vm.TitleDescripcion = "Velocidad";
                vm.TitleVelocidadSubidaKBPS = "Numero";
                vm.Four = false;
                vm.Eigth = false;
                vm.Eleven = false;

                vm.Eigthtxt = true;
            }
            if (vm.IdTipoCosto == "4") {
          
            }
            if (vm.IdTipoCosto == "5") {
                vm.Four = false;
                vm.Eigth = false;
                vm.TitleDescripcion = "Descripción";

                vm.Eigthtxt = true;
            }
            if (vm.IdTipoCosto == "6") {
                vm.Four = false;
                vm.Eigth = false;
                vm.TitleDescripcion = "Descripción";

                vm.Eigthtxt = true;
            }
            if (vm.IdTipoCosto == "7") {
                vm.Four = false;
                vm.Eigth = false;
                vm.Nine = false;
                vm.Eleven = false;
                vm.TitleGarantizado = "Capacidad Uso";
                vm.TitleDescripcion = "Descripción";
                vm.TitleVelocidadSubidaKBPS = "Año";

                vm.Ninetxt = true;
          }
          if (vm.IdTipoCosto == "8") {
              vm.TitleCostoSegmentoSatelital = "Costo Segmento Satelital (US$)";
              vm.TitleDescripcion = "Velocidad Bajada (Kbps)";
              vm.TitleInstalacion = "Instalación (US$) (*)";
              vm.TitleGarantizado = "Capacidad";
              vm.TitleVelocidadSubidaKBPS = "Velocidad Subida (Kbps)";

              vm.Six = false;
              
              

              vm.Five = false;

              vm.Three = false;
              vm.Four = false;
              vm.Nine = false;
              vm.Teen = false;
              vm.Eleven = false;

              vm.Two = false;
              vm.Thirteen = false;
              vm.Fourteen = false;

              vm.Ninetxt = true;
              vm.Teentxt = true;
              vm.Threetxt = true;
              vm.Sixtxt = true;
              vm.Twotxt = true;
              vm.Fivetxt = true;
         
          }
          if (vm.IdTipoCosto == "9") {
              vm.TitleModelo = "Unidad de Consumo";
              vm.TitleCostoSegmentoSatelital = "Costo";
              vm.TitleDescripcion = "Tarifas para Operadores";
              vm.Three = false;
              vm.Four = false;
              vm.One = false;

              vm.Threetxt = true;
          }
          if (vm.IdTipoCosto == "10") {
              vm.TitleModelo = "Unidad de Consumo";
              vm.TitleCostoSegmentoSatelital = "Costo";
              vm.TitleDescripcion = "Tarifas para Operadores";
              vm.Three = false;
              vm.Four = false;
              vm.One = false;


              vm.Threetxt = true;
          }

        }

        function RegistrarCosto() {


            blockUI.start();
            var confirmacion = true;
        
            if (confirmacion) {

                var costo = {
                    IdOportunidadCosto: vm.IdOportunidadCosto,
                    IdTipocosto:vm.IdTipoCosto,
                    CodigoModelo: vm.CodigoModelo,
                    Modelo: vm.Modelo,
                    Descripcion: vm.Descripcion,
                    Monto: vm.Monto,
                    Instalacion: vm.Instalacion,
                    CostoSegmentoSatelital:vm.CostoSegmentoSatelital

                };


                var promise = RegistrarCostoService.ActualizarOportunidadCostoPorLineaNegocio(costo);
                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                   blockUI.stop();

                   vm.IdOportunidadCosto = Respuesta.Id;

                    UtilsFactory.Alerta('#divAlert_Registrar', 'success', Respuesta.Mensaje, 5);


                }, function (response) {
                    blockUI.stop();

                });
            }

            blockUI.stop();



        }
        function ObtenerCosto() {
            blockUI.start();

            var costo = {
                IdOportunidadCosto: vm.IdOportunidadCosto
            }

            var promise = RegistrarCostoService.ObtenerOportunidadCosto(costo);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.Monto = Respuesta.Monto;
                vm.CodigoModelo = Respuesta.CodigoModelo;
                vm.Descripcion = Respuesta.Descripcion;
                vm.Monto = Respuesta.Monto;
                vm.Instalacion = Respuesta.Instalacion;
                vm.CostoSegmentoSatelital = Respuesta.CostoSegmentoSatelital;
                vm.VelocidadSubidaKBPS = Respuesta.VelocidadSubidaKBPS;
                vm.PorcentajeGarantizado = Respuesta.PorcentajeGarantizado;
                vm.InvAntenaHubUSD = Respuesta.InvAntenaHubUSD;
                vm.AntenaCasaClienteUSD = Respuesta.AntenaCasaClienteUSD;
                vm.IdTipoCosto = Respuesta.IdTipoCosto.toString();

                if (vm.IdTipoCosto == "7") {
                    if (vm.PorcentajeGarantizado > 0) {
                        vm.Monto = vm.Monto * vm.PorcentajeGarantizado;
                    }

                }


                if (vm.IdTipoCosto == "8") {
                    if (vm.ProcentajeSobresuscripcion > 0 && vm.PorcentajeGarantizado > 0) {
                        if (vm.Descripcion > 0) {
                            vm.VelocBajada = vm.Descripcion * vm.PorcentajeGarantizado * vm.ProcentajeSobresuscripcion;
                        }
                        if (vm.VelocidadSubidaKBPS > 0) {

                            vm.VelocSubida = vm.VelocidadSubidaKBPS * vm.PorcentajeGarantizado * vm.ProcentajeSobresuscripcion;
                        }

                    }
                    vm.InvTotal = vm.vm.InvAntenaHubUSD + vm.AntenaCasaClienteUSD;
                }





            }, function (response) {
                blockUI.stop();

            });
        }
    }
})();








