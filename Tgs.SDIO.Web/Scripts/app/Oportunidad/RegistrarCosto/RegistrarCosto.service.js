﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('RegistrarCostoService', RegistrarCostoService);

    RegistrarCostoService.$inject = ['$http', 'URLS'];

    function RegistrarCostoService($http, $urls) {

        var service = {

            ModalCosto: ModalCosto,
            ActualizarOportunidadCosto: ActualizarOportunidadCosto,
            RegistrarOportunidadCostoPorLineaNegocio: RegistrarOportunidadCostoPorLineaNegocio,
            ActualizarOportunidadCostoPorLineaNegocio:ActualizarOportunidadCostoPorLineaNegocio,
            ObtenerOportunidadCosto: ObtenerOportunidadCosto

        };

        return service;

        function ModalCosto(Costo) {

            return $http({
                url: $urls.ApiOportunidad + "RegistrarCosto/Index",
                method: "POST",
                data: JSON.stringify(Costo)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function RegistrarOportunidadCostoPorLineaNegocio(oportunidadCosto) {

            return $http({
                url: $urls.ApiOportunidad + "RegistrarCosto/RegistrarOportunidadCostoPorLineaNegocio",
                method: "POST",
                data: JSON.stringify(oportunidadCosto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
        function ActualizarOportunidadCostoPorLineaNegocio(oportunidadCosto) {

            return $http({
                url: $urls.ApiOportunidad + "RegistrarCosto/ActualizarOportunidadCostoPorLineaNegocio",
                method: "POST",
                data: JSON.stringify(oportunidadCosto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ActualizarOportunidadCosto(oportunidadCosto) {

            return $http({
                url: $urls.ApiOportunidad + "RegistrarCosto/ActualizarOportunidadCosto",
                method: "POST",
                data: JSON.stringify(oportunidadCosto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ObtenerOportunidadCosto(oportunidadCosto) {

            return $http({
                url: $urls.ApiOportunidad + "RegistrarCosto/ObtenerOportunidadCosto",
                method: "POST",
                data: JSON.stringify(oportunidadCosto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        
    }

})();