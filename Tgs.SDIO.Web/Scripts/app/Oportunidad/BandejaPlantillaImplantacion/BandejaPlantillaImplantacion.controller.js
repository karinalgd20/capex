﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('BandejaPlantillaImplantacion', BandejaPlantillaImplantacion);

    BandejaPlantillaImplantacion.$inject =
    [
        'OportunidadContenedorService',
        'ClienteService',
        'SectorService',
        'DireccionComercialService',
        'SedeService',
        'SISEGOService',
        'blockUI',
        'UtilsFactory',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        '$timeout',
        'MensajesUI',
        'URLS',
        '$scope',
        '$compile',
        '$injector'
    ];

    function BandejaPlantillaImplantacion
        (
        OportunidadContenedorService,
        ClienteService,
        SectorService,
        DireccionComercialService,
        SedeService,
        SISEGOService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile,
        $injector
        ) {

        var vm = this;
        vm.IdSedeInstalacion = 0;
        vm.IdSede = 0;


        vm.ObjOportunidad = [];
        vm.ObjOportunidad = jsonOportunidad;
        vm.IdOportunidad = 0;
        vm.NumeroSalesForce = '';
        vm.DescripcionOportunidad = '';
        vm.NumeroCaso = '';
        vm.Version = 0;
        vm.DescripcionCliente = '';
        vm.GerenteComercial = '';
        vm.Sector = '';
        vm.DireccionComercial = '';
        vm.IdSector = 0;
        vm.IdDireccionComercial = 0;
        vm.dtColumns = [
         DTColumnBuilder.newColumn('IdSede').notVisible(),
         DTColumnBuilder.newColumn('IdSedeInstalacion').notVisible(),
         DTColumnBuilder.newColumn('TipoSede').withTitle('Tipo Sede').notSortable(),
         DTColumnBuilder.newColumn('Direccion').withTitle('Direccion').notSortable(),
         DTColumnBuilder.newColumn('Departamento').withTitle('Departamento').notSortable(),
         DTColumnBuilder.newColumn('Provincia').withTitle('Provincia').notSortable(),
         DTColumnBuilder.newColumn('Distrito').withTitle('Distrito').notSortable(),
         DTColumnBuilder.newColumn('ServicioGrupo').withTitle('Servicio').notSortable(),
         DTColumnBuilder.newColumn('Costo').withTitle('BW(Mbps,Kbps)').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesSede)
        ];

        vm.SeleccionarServicio = SeleccionarServicio;
        vm.CargaModalSede = CargaModalSede;

        LimpiarGrilla();

        if (vm.ObjOportunidad != "") {
            vm.IdOportunidad = vm.ObjOportunidad.IdOportunidad;
            vm.NumeroSalesForce = vm.ObjOportunidad.NumeroSalesForce;
            vm.DescripcionOportunidad = vm.ObjOportunidad.Descripcion;
            vm.NumeroCaso = vm.ObjOportunidad.NumeroCaso;
            vm.Version = vm.ObjOportunidad.Version;
            OportunidadContenedorService.RegistroIdOportunidadSession(vm.IdOportunidad);
            ObtenerCliente();
        }

        function ObtenerCliente() {
            blockUI.start();

            var cliente = {
                IdCliente: vm.ObjOportunidad.IdCliente,
                IdEstado: 1
            };

            var promise = ClienteService.ObtenerCliente(cliente);
            promise.then(function (resultado) {
                blockUI.stop();

                cliente = resultado.data;
                vm.DescripcionCliente = cliente.Descripcion;
                vm.GerenteComercial = cliente.GerenteComercial;
                vm.IdSector = cliente.IdSector;
                vm.IdDireccionComercial = cliente.IdDireccionComercial;
                ObtenerSector();
                ObtenerDireccionComercial();
                ListarSede();

            }, function (response) {
                blockUI.stop();
            });
        }

        function ObtenerSector() {
            var sector = {
                IdSector: vm.IdSector,
                IdEstado: 1
            };

            var promise = SectorService.ObtenerSector(sector);
            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.Sector = Respuesta.Descripcion;
            }, function (response) {
                blockUI.stop();
            });
        }

        function ObtenerDireccionComercial() {
            var direccionComercial = {
                IdDireccion: vm.IdDireccionComercial,
                IdEstado: 1
            };

            var promise = DireccionComercialService.ObtenerDireccionComercial(direccionComercial);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.DireccionComercial = Respuesta.Descripcion;

            }, function (response) {
                blockUI.stop();
            });
        }

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }

        function AccionesSede(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'><a id='tab-button' class='btn btn-sm' class='btn' ng-click='vm.CargaModalSede(\"" + data.IdSedeInstalacion + "\"," + data.IdSede + ");' title='Datos Instalación'>" + "<span class='fa fa-eye fa-lg'></span></a></div> " +
            "<div class='col-xs-4 col-sm-1'><a id='tab-button' class='btn btn-sm' class='btn' ng-click='vm.SeleccionarServicio(" + data.IdFlujoCaja + ");' href='../Detalle/" + data.IdSede + "' title='Circuitos Digitales'>" + "<span class='fa fa-wrench fa-lg'></span></a></div>";

            /*
            return "<div class='col-xs-2 col-sm-1'><a id='tab-button' class='btn btn-sm' class='btn' ng-click='vm.CargaModalSede();' title='Datos Instalación'>" + "<span class='fa fa-eye fa-lg'></span></a></div> " +
            "<div class='col-xs-4 col-sm-1'><a id='tab-button' class='btn btn-sm' class='btn' ng-click='vm.SeleccionarServicio(" + data.IdFlujoCaja + ");' href='../Detalle/" + data.IdSede + "' title='Circuitos Digitales'>" + "<span class='fa fa-wrench fa-lg'></span></a></div>";
            */
        }

        function ListarSede() {
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(ListarSedeServiciosPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }

        function ListarSedeServiciosPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var sede = {
                IdOportunidad: vm.IdOportunidad,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = SedeService.ListarSedeServiciosPaginado(sede);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListSedeDto
                };

                fnCallback(result)

            }, function (response) {
                LimpiarGrilla();
            });
        }

        function SeleccionarServicio(IdFlujoCaja) {
            OportunidadContenedorService.RegistroIdFlujoCajaSession(IdFlujoCaja);
        }

        //function CargaModalSede() {
        //    alert('Carga modal...');
        //}

        function CargaModalSede(IdSedeInstalacion, IdSede) {
            blockUI.start();

            vm.IdSedeInstalacion = IdSedeInstalacion;
            vm.IdSede = IdSede;

            var dto = {
                IdSedeInstalacion: IdSedeInstalacion,
                IdSede: IdSede
            }

            var promise = SISEGOService.ModalSede(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);

                    $("#ContenidoSede").html(content);
                    blockUI.stop();
                });

                $('#ModalRegistrarSede').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });
        }
    }
})();