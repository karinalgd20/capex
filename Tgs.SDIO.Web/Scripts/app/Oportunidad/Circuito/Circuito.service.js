﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('CircuitoService', CircuitoService);

    CircuitoService.$inject = ['$http', 'URLS'];

    function CircuitoService($http, $urls) {

        var service = {
            ModalEditar: ModalEditar,
            Obtener: Obtener,
            ListarCircuitoPorServicioCMI, ListarCircuitoPorServicioCMI,
            ModalCircuito: ModalCircuito
        };

        return service;

        function ModalEditar(dto) {
            var preUrl = "";

            if (dto.IdTipoModal == 1)
                preUrl = $urls.ApiOportunidad + "Circuito/Index";
            else
                preUrl = $urls.ApiOportunidad + "Hardware/Index";

            return $http({
                url: preUrl,
                method: "POST",
                data: JSON.stringify() 
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ListarCircuitoPorServicioCMI(servicioCMI) {
            debugger
            return $http({
                url: $urls.ApiComun + "Circuito/ListarCircuitoPorServicioCMI",
                method: "POST",
                data: JSON.stringify(servicioCMI)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function Obtener(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Circuito/Obtener",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalCircuito(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Circuito/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };


    }
})();