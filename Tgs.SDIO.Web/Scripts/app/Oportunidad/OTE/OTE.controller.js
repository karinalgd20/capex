﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('OTEController', OTEController);

    OTEController.$inject = ['OTEService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile'];

    function OTEController(OTEService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile) {

        var vm = this;



        /******************************************* ServicioCMI *******************************************/
        vm.dataMasiva = [];
        vm.dtInstanceServicioCMI = {};

        vm.dtColumnsServicioCMI = [

         DTColumnBuilder.newColumn('CodigoCMI').notVisible(),
         DTColumnBuilder.newColumn('GerenciaProducto').notVisible(),
         DTColumnBuilder.newColumn('Linea').withTitle('Linea').notSortable(),
         DTColumnBuilder.newColumn('SubLinea').withTitle('Tipo').notSortable(),
         DTColumnBuilder.newColumn('ServicioCMI').withTitle('ServicioCMI').notSortable(),
         DTColumnBuilder.newColumn('Producto_AF').withTitle('Producto AF').notSortable(),
         DTColumnBuilder.newColumn('Procentaje').withTitle('Procentaje').notSortable(),
         DTColumnBuilder.newColumn('CentroCosto').withTitle('Centro de Costo').notSortable()
        ];

        //function AccionesServicioCMI(data, type, full, meta) {

        //    var respuesta = "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm' id='tab-button' onclick='CargaModalServicioCMI(\"" + data.IdConceptoDatosCapex + "\",\"" + data.IdServicioConcepto + "\"," + data.IdConcepto + ");'>" + "<span class='fa fa-edit'></span></a></div> ";
        //    respuesta = respuesta + "<div class='col-xs-4 col-sm-1'><a title='Cancelar' class='btn btn-sm'  id='tab-button' onclick='CargaModalServicioCMI(\"" + data.IdConceptoDatosCapex + "\",\"" + data.IdServicioConcepto + "\"," + data.IdConcepto + ");' >" + "<span class='fa fa-trash-o'></span></a></div>";
        //    return respuesta; 
        //};

        function BuscarServicioCMI() {

            $timeout(function () {
                vm.dtOptionsServicioCMI = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarServicioCMIPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarServicioCMIPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;

            var capex = {
                IdLineaNegocio: 5
            };

            var promise = OTEService.ListarServicioCMIPorLineaNegocio(capex);
            promise.then(function (response) {

                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };

                blockUI.stop();
                fnCallback(records);
                $("[id$='frmCaratula']")[0].style.height = "";
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };


        /******************************************* LOAD *******************************************/
        //BuscarServicioCMI();
    }

})();