﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('OTEService', OTEService);

    OTEService.$inject = ['$http', 'URLS'];

    function OTEService($http, $urls) {

        var service = {
            VistaOTE: VistaOTE
        };
         
        return service;

        function VistaOTE(dto) {
            return $http({
                url: $urls.ApiOportunidad + "OTE/Index",
                method: "GET",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
              
            function DatosCompletados(response) {  
                return response;
            }
        };

    }
})();