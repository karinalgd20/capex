﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('CaratulaService', CaratulaService);

    CaratulaService.$inject = ['$http', 'URLS'];

    function CaratulaService($http, $urls) {

        var modalEnum = { "CIRCUITOS": 1, "MEDIO3G": 2, "OPEX": 3, "ROUTERSONDA": 4, "SOPEX": 5 };

        var service = {
            ModalEditar: ModalEditar,
            VistaCaratula: VistaCaratula,
            ExportCaratula:ExportCaratula
        };

        return service;

        function ExportCaratula() {


            return $http({
                url: $urls.ApiOportunidad + "Caratula/ExportCaratula",
                method: "GET",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
        function VistaCaratula() {
            return $http({
                url: $urls.ApiOportunidad + "Caratula/Index",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalEditar(idTipoModal) {
            var preUrl = $urls.ApiOportunidad;

            switch (idTipoModal) {
                case modalEnum.CIRCUITOS: preUrl = preUrl + "Circuito/Index"; break;
                case modalEnum.MEDIO3G: preUrl = preUrl + "Medio3G/Index"; break;
                case modalEnum.OPEX: preUrl = preUrl + "OPEX/Index"; break;
                case modalEnum.ROUTERSONDA: preUrl = preUrl + "RouterSonda/Index"; break;
                case modalEnum.SOPEX: preUrl = preUrl + "SeguridadOPEX/Index"; break;
            }

            return $http({
                url: preUrl,
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
    }
})();