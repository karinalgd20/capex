﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('CaratulaController', CaratulaController);

    CaratulaController.$inject = ['CaratulaService', 'OportunidadServicioCMIService', 'FlujoCajaService', 'CircuitoService','OportunidadContenedorService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function CaratulaController(CaratulaService, OportunidadServicioCMIService, FlujoCajaService, CircuitoService,OportunidadContenedorService,
    blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        var modalEnum = { "CIRCUITOS": 1, "MEDIO3G": 2, "OPEX": 3, "ROUTER": 4, "SOPEX": 5 };
        Object.freeze(modalEnum);
        vm.Oportunidad = $scope.$parent.vm.ObjOportunidad;
        vm.AccionGrabar = $scope.$parent.vm.AccionGrabar;
        vm.tituloModal = "";
        vm.idGenerico = 0;
        vm.IdOportunidadLineaNegocio = vm.Oportunidad.IdOportunidadLineaNegocio;
        vm.BuscarServicioCMI = BuscarServicioCMI;
        vm.CargaModal = CargaModal;
        vm.Test = Test;
        vm.IdSubServicioDatosCaratula = 0;
        vm.BuscarCaratula = BuscarCaratula; 
        vm.CargaModalCircuito = CargaModalCircuito;
        vm.dto = null;
        vm.BuscarCircuitos = BuscarCircuitos;
        vm.BuscarMedio3G = BuscarMedio3G;
        vm.BuscarOpex = BuscarOpex;
        vm.BuscarRouterSonda = BuscarRouterSonda;
        vm.BuscarSeguridadOpex = BuscarSeguridadOpex;

        vm.Eliminar = Eliminar;

        vm.ExportCaratula=ExportCaratula;
  
        function ExportCaratula() {


            var promise = CaratulaService.ExportCaratula();

            promise.then(function (resultado) {
        
                var Respuesta = resultado.data;




            }, function (response) {
                blockUI.stop();

            });
        }





     //   $scope.$parent.vm.TabPorPerfilOportunidadDatos();
        vm.EditarDatoTable = true;
        TabPorPerfilOportunidadDatos();
        function TabPorPerfilOportunidadDatos() {


            var promise = OportunidadContenedorService.TabPorPerfilOportunidadDatos();

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
     

                vm.EditarDatoTable = !Respuesta.EditarDatoTable;

            }, function (response) {
                blockUI.stop();

            });
        }

       


        /******************************************* ServicioCMI *******************************************/
        vm.dataMasiva = [];
        vm.dtInstanceServicioCMI = {};

        vm.dtColumnsServicioCMI = [
         DTColumnBuilder.newColumn('IdServicioCMI').notVisible(),
         DTColumnBuilder.newColumn('GerenciaProducto').withTitle('Gerencia').notSortable(),
         DTColumnBuilder.newColumn('Linea').withTitle('Linea').notSortable(),
         DTColumnBuilder.newColumn('SubLinea').withTitle('SubLinea').notSortable(),
         DTColumnBuilder.newColumn('DescripcionCMI').withTitle('Servicio CMI').notSortable(),
         DTColumnBuilder.newColumn('Producto_AF').withTitle('Producto AF').notSortable(),
         DTColumnBuilder.newColumn('Porcentaje').withTitle('Porcentaje').notSortable(),
         DTColumnBuilder.newColumn('CentroCosto').withTitle('Centro de Costo').notSortable()
        ];

        function BuscarServicioCMI() {

            LimpiarGrillaServicioCMI();

            $timeout(function () {
                vm.dtOptionsServicioCMI = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarServicioCMIPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarServicioCMIPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var dto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = OportunidadServicioCMIService.ListarServicioCMIPorLineaNegocio(dto);
            promise.then(function (response) {

                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadServicioCMIDtoResponse
                };

                blockUI.stop();
                fnCallback(records);
            }, function (response) {
                blockUI.stop();
                LimpiarGrillaServicioCMI();
            });
        };


        /******************************************* Circuitos *******************************************/
        vm.dtInstanceCircuitos = {};

        vm.dtColumnsCircuitos = [
         DTColumnBuilder.newColumn('IdSubServicioDatosCaratula').notVisible(),
         DTColumnBuilder.newColumn('IdServicioCMI').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Servicio').notSortable(),
         DTColumnBuilder.newColumn('Circuito').withTitle('N° Circuito').notSortable(),
         DTColumnBuilder.newColumn('Medio').withTitle('Medio').notSortable(),
         DTColumnBuilder.newColumn('TipoEnlace').withTitle('Tipo Enlace').notSortable(),
         DTColumnBuilder.newColumn('ActivoPasivo').withTitle('Activo/Pasivo').notSortable(),
         DTColumnBuilder.newColumn('Localidad').withTitle('Localidad').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('Costo').withTitle('BW').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCircuitos)

        ];
        //ng-hide='" +vm.AccionGrabar + "'
        function AccionesBusquedaCircuitos(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm' id='tab-button'  ng-hide=vm.EditarDatoTable  data-toggle='modal'  ng-click='vm.CargaModalCircuito(\"" + data.IdSubServicioDatosCaratula + "\");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Eliminar' class='btn btn-sm' id='tab-button'  ng-hide=vm.EditarDatoTable class='btn ' ng-click='vm.Eliminar(" + 1 + "," + data.IdSubServicioDatosCaratula + ")' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarCircuitos() {

            LimpiarGrillaCircuitos();

            $timeout(function () {
                vm.dtOptionsCircuitos = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarCircuitosPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(6);
            }, 500);
        };

        function BuscarCircuitosPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var dto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 1,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = FlujoCajaService.ListarDetalleOportunidadCaratula(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaCircuitos();
            });
        };

        /******************************************* Medio3G *******************************************/
        vm.dtInstanceMedio3G = {};

        vm.dtColumnsMedio3G = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCaratula').notVisible(),
         DTColumnBuilder.newColumn('IdServicioCMI').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Servicio').notSortable(),
         DTColumnBuilder.newColumn('Circuito').withTitle('N° Circuito').notSortable(),
         DTColumnBuilder.newColumn('Medio').withTitle('Medio').notSortable(),
         DTColumnBuilder.newColumn('TipoEnlace').withTitle('Tipo Enlace').notSortable(),
         DTColumnBuilder.newColumn('Localidad').withTitle('Localidad').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('Costo').withTitle('BW').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaMedio3G)

        ];

        function AccionesBusquedaMedio3G(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm' id='tab-button'  ng-hide=vm.EditarDatoTable class='btn ' ng-click='vm.CargaModal(" + modalEnum.MEDIO3G + ", " + data.IdSubServicioDatosCaratula + ")'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button'  ng-hide=vm.EditarDatoTable class='btn ' ng-click='vm.Eliminar(" + 2 + "," + data.IdSubServicioDatosCaratula + ")' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarMedio3G() {

            LimpiarGrillaMedio3G();

            $timeout(function () {
                vm.dtOptionsMedio3G = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarMedio3GPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarMedio3GPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start +length) / length;

            var dto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 2,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = FlujoCajaService.ListarDetalleOportunidadCaratula(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaMedio3G();
            });
        };

        /******************************************* Opex *******************************************/
        vm.dtInstanceOpex = {};

        vm.dtColumnsOpex = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCaratula').notVisible(),
         DTColumnBuilder.newColumn('IdServicioCMI').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Componente').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('NumeroMeses').withTitle('N° Meses').notSortable(),
         DTColumnBuilder.newColumn('MontoUnitarioMensual').withTitle('Monto Mensual').notSortable(),
         DTColumnBuilder.newColumn('MontoTotalMensual').withTitle('Monto Total Mensual').notSortable(),
         DTColumnBuilder.newColumn('NumeroMesInicioGasto').withTitle('N° mes Inicio').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaOpex)

        ];

        function AccionesBusquedaOpex(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button'  ng-hide=vm.EditarDatoTable class='btn ' ng-click='vm.CargaModal(" + modalEnum.OPEX + ", " + data.IdSubServicioDatosCaratula + ")'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button'  ng-hide=vm.EditarDatoTable class='btn 'ng-click='vm.Eliminar(" + 3 + "," + data.IdSubServicioDatosCaratula + ")' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarOpex() {

            LimpiarGrillaOpex();

            $timeout(function () {
                vm.dtOptionsOpex = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarOpexPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarOpexPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start +length) / length;

            var dto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 3,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = FlujoCajaService.ListarDetalleOportunidadCaratula(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaOpex();
            });
        };
       



        /******************************************* Router Sonda *******************************************/
        vm.dtInstanceRouterSonda = {};

        vm.dtColumnsRouterSonda = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCaratula').notVisible(),
         DTColumnBuilder.newColumn('IdServicioCMI').notVisible(),
         DTColumnBuilder.newColumn('CodigoModelo').withTitle('Modelo').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('FlagRenovacion').withTitle('FlagRenovacion').notSortable(),
         DTColumnBuilder.newColumn('Instalacion').withTitle('Instalacion').notSortable(),
         DTColumnBuilder.newColumn('Desinstalacion').withTitle('Desinstalacion').notSortable(),
         DTColumnBuilder.newColumn('PU').withTitle('P.U($)').notSortable(),
         DTColumnBuilder.newColumn('Alquiler').withTitle('Alquiler').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaRouterSonda)

        ];

        function AccionesBusquedaRouterSonda(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm'   id='tab-button'  ng-hide=vm.EditarDatoTable class='btn ' ng-click='vm.CargaModal(" + modalEnum.ROUTER + ", " + data.IdSubServicioDatosCaratula + ")'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm'  id='tab-button'  ng-hide=vm.EditarDatoTable class='btn ' ng-click='vm.Eliminar(" + 4+ "," + data.IdSubServicioDatosCaratula + ")' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarRouterSonda() {

            LimpiarGrillaRouterSonda();

            $timeout(function () {
                vm.dtOptionsRouterSonda = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarRouterSondaPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarRouterSondaPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var dto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 4,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = FlujoCajaService.ListarDetalleOportunidadCaratula(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaRouterSonda();
            });
        };


        /******************************************* Seguridad Opex *******************************************/
        vm.dtInstanceSeguridadOpex = {};

        vm.dtColumnsSeguridadOpex = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCaratula').notVisible(),
         DTColumnBuilder.newColumn('IdServicioCMI').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Componente').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('NumeroMeses').withTitle('Número Meses').notSortable(),
         DTColumnBuilder.newColumn('MontoUnitarioMensual').withTitle('Monto Unitario').notSortable(),
         DTColumnBuilder.newColumn('MontoTotalMensual').withTitle('MontoPxQ').notSortable(),
         DTColumnBuilder.newColumn('Factor').withTitle('Factor').notSortable(),
         DTColumnBuilder.newColumn('ValorCuota').withTitle('Valor Cuota').notSortable(),
         DTColumnBuilder.newColumn('NumeroMesInicioGasto').withTitle('Mes Inicio').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaSeguridadOpex)

        ];

        function AccionesBusquedaSeguridadOpex(data, type, full, meta) {
            var respuesta = "<div class='col-xs-2 col-sm-2'> <a title='Editar' class='btn btn-sm' id='tab-button'  ng-hide=vm.EditarDatoTable class='btn ' ng-click='vm.CargaModal(" + modalEnum.SOPEX + ", " + data.IdSubServicioDatosCaratula + ");'>" + "<span class='fa fa-edit fa-lg'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-2 col-sm-2'><a title='Cancelar' class='btn btn-sm' id='tab-button'  ng-hide=vm.EditarDatoTable class='btn ' ng-click='vm.Eliminar(" + 5+ "," + data.IdSubServicioDatosCaratula + ")' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarSeguridadOpex() {

            LimpiarGrillaSeguridadOpex();

            $timeout(function () {
                vm.dtOptionsSeguridadOpex = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarSeguridadOpexPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarSeguridadOpexPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var dto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdGrupo: 5,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = FlujoCajaService.ListarDetalleOportunidadCaratula(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaSeguridadOpex();
            });
        };
        function Eliminar(IdGrupo,IdSubServicioDatosCaratula) {

            var objFlujoCaja = {
                IdSubServicioDatosCaratula: IdSubServicioDatosCaratula,
                IdGrupo: IdGrupo
            };

 


            if (confirm('¿Estas seguro que desea eliminar?')) {


                var promise = FlujoCajaService.InhabilitarOportunidadFlujoCaja(objFlujoCaja);
                promise.then(function (response) {

                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        blockUI.stop();
                     
                    } else {
                
                       

                if (IdGrupo == 1) {
                    blockUI.stop(); BuscarCircuitos();
                    UtilsFactory.Alerta('#lblAlerta_Circuitos', 'success', 'Se Elimino Correctamente', 10);
                }
                if (IdGrupo == 2) {
                    blockUI.stop();BuscarMedio3G();
                    UtilsFactory.Alerta('#lblAlerta_Medio3G', 'success','Se Elimino Correctamente', 10);
                }
                if (IdGrupo == 3) {
                    blockUI.stop(); BuscarOpex();
                    UtilsFactory.Alerta('#lblAlerta_Opex', 'success', 'Se Elimino Correctamente', 10);
                }
                if (IdGrupo == 4) {
                    blockUI.stop(); BuscarRouterSonda();
                    UtilsFactory.Alerta('#lblAlerta_RouterSonda', 'success', 'Se Elimino Correctamente', 10);
                }
                if (IdGrupo == 5) {
                    blockUI.stop(); BuscarSeguridadOpex();
                    UtilsFactory.Alerta('#lblAlerta_SeguridadOpex', 'success', 'Se Elimino Correctamente', 10);
                }


                    }

                }, function (response) {
                    blockUI.stop();
                  //  UtilsFactory.Alerta('#lblAlertaEliminarCaratula', 'danger', MensajesUI.DatosError, 5);
                });
                


            }
        else{
                blockUI.stop();

            }
        };


        /******************************************* Funciones *******************************************/

        function LimpiarGrillaServicioCMI() {
            vm.dtOptionsServicioCMI = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        };

        function LimpiarGrillaCircuitos() {
            vm.dtOptionsCircuitos = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };
            function LimpiarGrillaMedio3G () {
            vm.dtOptionsMedio3G = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
            };
            function LimpiarGrillaOpex() {
            vm.dtOptionsOpex = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

            };
            function LimpiarGrillaRouterSonda () {
            vm.dtOptionsRouterSonda = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

            };
            function LimpiarGrillaSeguridadOpex () {
            vm.dtOptionsSeguridadOpex = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };

        function CargaModal(id, idEntidad) {
            vm.IdSubServicioDatosCaratula = idEntidad;
            var idTipoModal = parseInt(id);

            switch (idTipoModal) {
                case modalEnum.CIRCUITOS: vm.tituloModal = "Circuitos"; break;
                case modalEnum.MEDIO3G: vm.tituloModal = "Medio 3G"; break;
                case modalEnum.OPEX: vm.tituloModal = "OPEX"; break;
                case modalEnum.ROUTER: vm.tituloModal = "Router Sonda"; break;
                case modalEnum.SOPEX: vm.tituloModal = "Seguridad OPEX"; break;
            }
            
            vm.dto = {
                IdSubServicioDatosCaratula: idEntidad
            };

            var promise = CaratulaService.ModalEditar(idTipoModal);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);

                    $("#ContenidoModal").html(content);
                });

                $('#ModalGenerico').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function Test(idOportunidadLineaNegocio, idUsuario, tipoFicha) {
            var dto = {
                IdOportunidadLineaNegocio: idOportunidadLineaNegocio,
                IdUsuarioCreacion: idUsuario,
                TipoFicha: tipoFicha
            };

            var promise = CaratulaService.Test(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

            }, function (response) {
                blockUI.stop();
            });

        };

        function BuscarCaratula() {
            BuscarServicioCMI();
            BuscarCircuitos();
            BuscarMedio3G();
            BuscarOpex();
            BuscarRouterSonda();
            BuscarSeguridadOpex();
        };

        function CargarVista() {

            BuscarCaratula();
        };

        function CargaModalCircuito(IdSubServicioDatosCaratula) {

            vm.dto = {
                IdSubServicioDatosCaratula: IdSubServicioDatosCaratula
            };

            var promise = CircuitoService.ModalCircuito();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCircuitos").html(content);
                });

                $('#ModalCircuitos').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        
        /******************************************* LOAD *******************************************/
        CargarVista();

    }
})();



