﻿(function () {
    'use strict',
    angular
        .module('app.Oportunidad')
        .service('RegistrarContactoService', RegistrarContactoService);

    RegistrarContactoService.$inject = ['$http', 'URLS'];

    function RegistrarContactoService($http, $urls) {
        var service = {
            ModalContacto: ModalContacto
        };

        return service;

        function ModalContacto(contacto) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarContacto/Index",
                method: "POST",
                data: JSON.stringify(contacto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();