﻿(function () {
    'use strict'
    angular
        .module('app.Oportunidad')
        .controller('RegistrarContacto', RegistrarContacto);

    RegistrarContacto.$inject =
        [
            'ContactoService',
            'blockUI',
            'UtilsFactory',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'
        ];

    function RegistrarContacto
        (
        ContactoService,
        blockUI,
        UtilsFactory,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile,
        $modal,
        $injector
        ) {

        var vm = this;

        vm.Id = $scope.$parent.vm.Id;
        vm.IdSede = $scope.$parent.vm.IdSede;
        vm.NombreApellidos = '';
        vm.NumeroTelefono = '';
        vm.NumeroCelular = '';
        vm.CorreoElectronico = '';
        vm.Cargo = '';
        
        CargaModalContacto();
        vm.RegistrarContacto = RegistrarContacto;

        function ValidarCampos() {
            if (vm.IdSede <= 0 || vm.IdSede == undefined) {
                UtilsFactory.Alerta('#divAlertRegistrarContacto', 'warning', 'No tiene local asignado', 5);
                return;
            }

            if (vm.NombreApellidos.trim() == "" || vm.NombreApellidos == undefined) {
                UtilsFactory.Alerta('#divAlertRegistrarContacto', 'warning', 'Ingrese el nombre y apellido', 5);
                return;
            }

            if (vm.NumeroTelefono == undefined || vm.NumeroTelefono.trim() == '') {
                UtilsFactory.Alerta('#divAlertRegistrarContacto', 'warning', 'Ingrese el numero de telefono', 5);
                return;
            }

            return true;
        }

        function RegistrarContacto() {
            if (!ValidarCampos())
                return;

            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();

                var contacto = {
                    Id: vm.Id,
                    IdSede: vm.IdSede,
                    NombreApellidos: vm.NombreApellidos,
                    NumeroTelefono: vm.NumeroTelefono,
                    NumeroCelular: vm.NumeroCelular,
                    CorreoElectronico: vm.CorreoElectronico,
                    Cargo: vm.Cargo,
                    IdEstado: 1
                };

                var promise = (vm.Id == 0) ?
                    ContactoService.RegistrarContacto(contacto) :
                    ContactoService.ActualizarContacto(contacto);

                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                    vm.Id = Respuesta.Id;
                    UtilsFactory.Alerta('#divAlertRegistrarContacto', 'success', Respuesta.Mensaje, 5);
                    //$scope.$parent.vm.dtInstance.rerender();

                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertRegistrarContacto', 'danger', MensajesUI.DatosError, 5);
                });
            }
        }

        function CargaModalContacto() {
            vm.Id = $scope.$parent.vm.Id;
            if (vm.Id > 0) ObtenerContactoPorId();
        }

        function ObtenerContactoPorId() {
            blockUI.start();

            var contacto = { Id: vm.Id };
            var promise = ContactoService.ObtenerContactoPorId(contacto);

            promise.then(function (resultado) {
                blockUI.stop();

                contacto = resultado.data;
                vm.Id = contacto.Id;
                vm.NombreApellidos = contacto.NombreApellidos;
                vm.NumeroTelefono = contacto.NumeroTelefono;
                vm.NumeroCelular = contacto.NumeroCelular;
                vm.CorreoElectronico = contacto.CorreoElectronico;
                vm.Cargo = contacto.Cargo;
                vm.IdProbabilidad = riesgoProyecto.IdProbabilidad;
                vm.IdImpacto = riesgoProyecto.IdImpacto;

            }, function (response) {
                blockUI.stop();
            });
        }
    }
})();