﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('SISEGOService', SISEGOService);

    SISEGOService.$inject = ['$http', 'URLS'];

    function SISEGOService($http, $urls) {

        var service = {
            VistaSISEGO: VistaSISEGO,
            ModalEditar: ModalEditar,
            ModalSede: ModalSede,
            ModalSedeAgregar: ModalSedeAgregar,
            EliminarSede: EliminarSede,
            EliminarServicios: EliminarServicios
        };

        return service;

        function VistaSISEGO() {
            return $http({
                url: $urls.ApiOportunidad + "SISEGO/Index",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalEditar(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SISEGO/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ModalSede(dto) {
            debugger;
            return $http({
                url: $urls.ApiOportunidad + "Sede/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
        
        function ModalSedeAgregar() {
            return $http({
                url: $urls.ApiOportunidad + "AgregarSede/Editar",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function EliminarSede(Sede) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/EliminarSede",
                method: "POST",
                data: JSON.stringify(Sede)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarServicios(Sede) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/EliminarServicios",
                method: "POST",
                data: JSON.stringify(Sede)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();