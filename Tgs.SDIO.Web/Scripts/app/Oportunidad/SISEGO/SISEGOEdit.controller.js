﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('SISEGOEditController', SISEGOEditController);

    SISEGOEditController.$inject = ['SISEGOService', 'MaestraService', 'blockUI', 'UtilsFactory', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function SISEGOEditController(SISEGOService, MaestraService, blockUI, UtilsFactory, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.TituloModal = "Editar";
        vm.Id = 0;
        vm.IdSISEGO = $scope.$parent.vm.Id;
        vm.Cumplimiento = false;
        
        vm.GrabarAgenda = GrabarAgenda;

        /******************************************* SISEGO *******************************************/
        
        function GrabarAgenda() {
            if (!ValidarDatos())
                return;

            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();
                var dto = {
                    IdSISEGO: vm.IdSISEGO,
                    FechaAcuerdo: vm.FechaAcuerdo,
                    DescripcionPunto: vm.DescripcionPunto,
                    Responsable: vm.Responsable,
                    Cumplimiento: vm.Cumplimiento
                };

                var promise = SISEGOService.RegistrarSISEGODetalle(dto);
                promise.then(function (response) {
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#lblAlerta_SISEGODetalle', 'danger', MensajesUI.DatosError, 20);
                    } else {
                        UtilsFactory.Alerta('#lblAlerta_SISEGODetalle', 'success', response.data.Mensaje, 10);
                        $scope.$parent.vm.dtInstanceSISEGO.rerender();
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_SISEGODetalle', 'danger', MensajesUI.DatosError, 5);
                });

            };
        }

        /******************************************* Funciones *******************************************/
        function ValidarDatos() {
            if (vm.FechaAcuerdo == undefined || vm.FechaAcuerdo.trim() == '') {
                UtilsFactory.Alerta('#lblAlerta_SISEGODetalle', 'warning', 'Ingrese la fecha del acuerdo', 5);
                return;
            }

            if (vm.DescripcionPunto == undefined || vm.DescripcionPunto.trim() == '') {
                UtilsFactory.Alerta('#lblAlerta_SISEGODetalle', 'warning', 'Ingrese la descripcion del punto', 5);
                return;
            }

            if (vm.Responsable == undefined || vm.Responsable.trim() == '') {
                UtilsFactory.Alerta('#lblAlerta_SISEGODetalle', 'warning', 'Ingrese la descripcion del punto', 5);
                return;
            }
            
            if (new Date() > new Date(vm.FechaAcuerdo)) {
                UtilsFactory.Alerta('#lblAlerta_SISEGODetalle', 'warning', 'La fecha no puede ser anterior a la actual', 5);
                return;
            }

            return true;
        }

        function CargarSISEGODetalle() {
            
            var dto = {
                Id: vm.Id,
                IdSISEGO: vm.IdSISEGO
            };

            var promise = SISEGOService.ObtenerSISEGODetalle(dto);

            promise.then(function (resultado) {
                var respuesta = resultado.data;
                if (respuesta != null && respuesta != '') {
                    vm.FechaAcuerdo = respuesta.FechaAcuerdo;
                    vm.DescripcionPunto = respuesta.DescripcionPunto;
                    vm.Responsable = respuesta.Responsable;
                    Cumplimiento = respuesta.Cumplimiento;

                    UtilsFactory.Alerta('#lblAlerta_SISEGODetalle', 'success', respuesta.Mensaje, 10);
                    $scope.$$childHead.vm.ObtenerFlujoCajaConfiguracion();
                }
                blockUI.stop();
                
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarTipoSISEGO() {
            var filtro = {
                IdRelacion: 278
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoSISEGO = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };
        
        /******************************************* LOAD *******************************************/
        CargarTipoSISEGO();
        //CargarSISEGODetalle();

    }
})();



