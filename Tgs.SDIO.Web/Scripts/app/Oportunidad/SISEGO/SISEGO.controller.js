﻿(function () {
    'use strict'
    angular
        .module('app.Oportunidad')
        .controller('SISEGOController', SISEGOController);

    SISEGOController.$inject =
        [
        'SedeService',
        'SISEGOService',
        'MaestraService',
        'AgregarSedeService',
        'BandejaContactoService',
        'BandejaEstudiosService',
        'BandejaCotizacionService',
        'BandejaServiciosAgrupadosService',
        'RegistrarOportunidadService',
        'blockUI',
        'UtilsFactory',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        '$timeout',
        'MensajesUI',
        'URLS',
        '$scope',
        '$compile',
        '$modal',
        '$injector'
        ];

    function SISEGOController
        (
        SedeService,
        SISEGOService,
        MaestraService,
        AgregarSedeService,
        BandejaContactoService,
        BandejaEstudiosService,
        BandejaCotizacionService,
        BandejaServiciosAgrupadosService,
        RegistrarOportunidadService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile,
        $modal,
        $injector
        ) {

        var vm = this;

        vm.TituloModal = "Agregar Sede";
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;
        vm.Id = $scope.$parent.vm.Id;
        vm.IdSedeInstalacion = $scope.$parent.vm.IdSedeInstalacion;
        vm.IdSede = $scope.$parent.vm.IdSede;
        vm.IdServicioSedeInstalacion = $scope.$parent.vm.IdServicioSedeInstalacion;
        vm.selectedSede = {};
        vm.IdEstado = 1;
        vm.IdEstadoOportunidad = 0;
        vm.hideBoton = true;
        vm.ListTipoSISEGO = [];
        vm.IdTipoSISEGO = "-1";
        vm.dtInstanciaServicioModal = [];
        vm.toggleAllSede = toggleAllSede;
        vm.toggleOneSede = toggleOneSede;
        var titleHtmlSede = '<input type="checkbox" ng-model="vm.selectAllSede" ng-click="vm.toggleAllSede(vm.selectAllSede, vm.selectedSede)">';

        vm.dtColumnsSedeModal =
            [
                DTColumnBuilder.newColumn(null).withTitle(titleHtmlSede).notSortable().withOption('width', '3%')
                    .renderWith(function (data, type, full, meta) {
                        vm.selectedSede[full.Id] = false;
                        return '<input type="checkbox" ng-model="vm.selectedSede[\'' + data.Id + '\']" ng-click="vm.toggleOneSede(vm.selectedSede)">';
                    }),

                DTColumnBuilder.newColumn('Id').notVisible(),
                DTColumnBuilder.newColumn('IdSede').notVisible(),
                DTColumnBuilder.newColumn('IdServicioSedeInstalacion').notVisible(),
                DTColumnBuilder.newColumn('TipoSede').withTitle('Tipo Sede').notSortable(),
                DTColumnBuilder.newColumn('Direccion').withTitle('Dirección').notSortable(),
                DTColumnBuilder.newColumn('Servicio').withTitle('Servicios').notSortable(),
                DTColumnBuilder.newColumn('Configurado').withTitle('Configurado').notSortable(),
                DTColumnBuilder.newColumn('CodSisego').withTitle('Código Sisego').notSortable(),
                DTColumnBuilder.newColumn('Departamento').withTitle('Departamento').notSortable(),
                DTColumnBuilder.newColumn('Provincia').withTitle('Provincia').notSortable(),
                DTColumnBuilder.newColumn('Distrito').withTitle('Distrito').notSortable(),
                DTColumnBuilder.newColumn('Estado').withTitle('Estado').notSortable(),
                DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
            ];

        vm.CargaModal = CargaModal;
        vm.EliminarSede = EliminarSede;
        vm.EliminarServicios = EliminarServicios;
        vm.VistaBandejaContacto = VistaBandejaContacto;
        vm.VistaBandejaEstudios = VistaBandejaEstudios;
        vm.VistaBandejaCotizacion = VistaBandejaCotizacion;
        vm.VistaServiciosAgrupados = VistaServiciosAgrupados;
        vm.CargaModalAgregarSede = CargaModalAgregarSede;
        vm.BuscarListaSedeInstalacion = BuscarListaSedeInstalacion;

        function AccionesBusqueda(data, type, full, meta) {
            var respuesta = "";
            respuesta = respuesta + " <a title='Editar' ng-click='vm.CargaModal(\"" + data.Id + "\"," + data.IdSede + ");'>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + " <a title='Eliminar Sede Instalacion' ng-hide=" + vm.hideBoton + " ng-click='vm.EliminarSede(\"" + data.Id + "\",\"" + data.IdSede + "\"," + data.IdServicioSedeInstalacion + ");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6; margin-left: 1px;'></span></a>";
            respuesta = respuesta + " <a title='Eliminar Servicio Instalacion' ng-hide=" + vm.hideBoton + " ng-click='vm.EliminarServicios(" + data.IdServicioSedeInstalacion + ");'>" + "<span class='glyphicon glyphicon-remove-circle' style='color: #00A4B6; margin-left: 5px;'></span></a>";
            return respuesta;
        }

        function toggleOneSede(selectedSedeItems) {
            for (var IdOportunidad in selectedSedeItems) {
                if (selectedSedeItems.hasOwnProperty(IdOportunidad)) {
                    if (!selectedSedeItems[IdOportunidad]) {
                        vm.selectAllSede = false;
                        return;
                    }
                }
            }
            vm.selectAllSede = true;
        }

        function toggleAllSede(selectAllSede, selectedSedeItems) {
            for (var IdOportunidad in selectedSedeItems) {
                if (selectedSedeItems.hasOwnProperty(IdOportunidad)) {
                    selectedSedeItems[IdOportunidad] = selectAllSede;
                }
            }
        }

        function LimpiarGrilla() {
            vm.dtOptionsServiciosModal = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
        };

        function CargaModal(Id, IdSede) {
            blockUI.start();
            vm.IdSedeInstalacion = Id;
            vm.IdSede = IdSede;

            var dto = {
                IdSedeInstalacion: Id,
                IdSede: IdSede
            }

            var promise = SISEGOService.ModalSede(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);

                    $("#ContenidoSede").html(content);
                    blockUI.stop();
                });

                $('#ModalRegistrarSede').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });
        }

        function CargaModalAgregarSede() {
            blockUI.start();
            var promise = SISEGOService.ModalSedeAgregar();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);

                    $("#ContenidoAgregarSede").html(content);
                    blockUI.stop();
                });
                $('#ModalAgregarSede').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });
        };

        function VistaBandejaContacto() {
            var promise = BandejaContactoService.VistaBandejaContacto();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#contactos").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        }

        function VistaBandejaEstudios() {
            var promise = BandejaEstudiosService.VistaBandejaEstudios();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#estudios").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        }

        function VistaBandejaCotizacion() {
            var promise = BandejaCotizacionService.VistaBandejaCotizacion();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#cotizacion").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        }

        function VistaServiciosAgrupados() {
            var promise = BandejaServiciosAgrupadosService.VistaBandejaServiciosAgrupados();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#servicios").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        }

        function BuscarListaSedeInstalacion() {
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptionsServiciosModal = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarSedePaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarSedePaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var oportunidad = {
                IdOportunidad: vm.IdOportunidad,
                Indice: pageNumber,
                Tamanio: length,
                IdEstado: vm.IdEstado
            };

            var promise = SedeService.ListarSedeInstalacion(oportunidad);
            promise.then(function (response) {

                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListSedeDto
                };
                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function EliminarSede(Id, IdSede, IdServicioSedeInstalacion) {
            blockUI.start();
            if (IdServicioSedeInstalacion != null) {
                UtilsFactory.Alerta('#divAlertContenedor', 'danger', "No se puede eliminar la sede de instalación  ya que tiene asignada un servicio", 5);
                blockUI.stop();
            }
            else{
                if (confirm('¿Estas seguro que desea eliminar la sede de instalación?')) {
                    blockUI.start();
                    var objConcepto = {
                        IdSedeInstalacion: Id,
                        IdSede: IdSede
                    }
                    var promise = SISEGOService.EliminarSede(objConcepto);
                    promise.then(function (resultado) {
                        blockUI.stop();
                        var Respuesta = resultado.data;
                        if (Respuesta.TipoRespuesta == 0) {
                            UtilsFactory.Alerta('#divAlertContenedor', 'success', MensajesUI.DatosDeleteOk, 5);
                            BuscarListaSedeInstalacion()
                        } else {
                            UtilsFactory.Alerta('#divAlertContenedor', 'danger', "No se pudo eliminar el registro.", 5);
                        }
                        modalpopupConfirm.modal('hide');
                    }, function (response) {
                        UtilsFactory.Alerta('#divAlertContenedor', 'danger', MensajesUI.DatosError, 5);
                        blockUI.stop();
                    });
                }
                else {
                    blockUI.stop();
                }
            }
        }

        function EliminarServicios(IdServicioSedeInstalacion) {
            blockUI.start();
            if (IdServicioSedeInstalacion == null) {
                UtilsFactory.Alerta('#divAlertContenedor', 'danger', "No se puede eliminar el servicio ya que la sede de instalación no tiene asignada un servicio", 5);
                blockUI.stop();
            }
            else
                {
                if (confirm('¿Estas seguro que desea eliminar el servicio?')) {
                    blockUI.start();


                    var objConcepto = {
                        IdServicioSedeInstalacion: IdServicioSedeInstalacion
                    }
                
                    var promise = SISEGOService.EliminarServicios(objConcepto);
                    promise.then(function (resultado) {
                        blockUI.stop();
                        var Respuesta = resultado.data;
                        if (Respuesta.TipoRespuesta == 0) {
                            UtilsFactory.Alerta('#divAlertContenedor', 'success', MensajesUI.DatosDeleteOk, 5);
                            BuscarListaSedeInstalacion()
                        } else {
                            UtilsFactory.Alerta('#divAlertContenedor', 'danger', "No se pudo eliminar el registro.", 5);
                        }
                        modalpopupConfirm.modal('hide');
                    }, function (response) {
                        UtilsFactory.Alerta('#divAlertContenedor', 'danger', MensajesUI.DatosError, 5);
                        blockUI.stop();
                    });
                }
                else {
                    blockUI.stop();
                }
            }
        }

        function ObtenerOportunidadFlujoEstadoId() {
            var oportunidadFlujoEstado = {
                IdOportunidad: vm.IdOportunidad
            }

            var promise = RegistrarOportunidadService.ObtenerOportunidadFlujoEstadoId(oportunidadFlujoEstado);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.IdEstadoOportunidad = Respuesta.IdEstado;
                if (vm.IdEstadoOportunidad == 6) {
                    vm.hideBoton = true;
                } else {
                    vm.hideBoton = false;
                }

            }, function (response) {
            });
        }

        /******************************************* LOAD *******************************************/
        LimpiarGrilla();
        BuscarListaSedeInstalacion();
        ObtenerOportunidadFlujoEstadoId();
    }
})();