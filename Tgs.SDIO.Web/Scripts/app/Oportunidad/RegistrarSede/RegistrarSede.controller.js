﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('RegistrarSede', RegistrarSede);
    RegistrarSede.$inject = ['MaestraService', 'SedeService','UbigeoService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];
    function RegistrarSede(MaestraService, SedeService,UbigeoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
      
        vm.IdTipoEnlace = "-1";
        vm.IdTipoSede = "-1";
        vm.Codigo = "";
        vm.IdAccesoCliente = "-1";
        vm.IdTendidoExterno = "-1";
        vm.NumeroPisos = "";
        vm.CodigoDepartamento = "-1";
        vm.CodigoProvincia = "-1";
        vm.CodigoDistrito = "-1";
        vm.IdTipoVia = "-1";
        vm.Latitud = "";
        vm.Longitud = "";
        vm.Direccion = "";
        vm.IdTipoServicio = "-1";
        vm.Piso = "";
        vm.Interior = "";
        vm.Manzana = "";
        vm.Lote = "";
        vm.IdEstado = "1";
        vm.ValidarCampos = ValidarCampos;
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        vm.CargaModalSede = CargaModalSede;
        CargaModalSede();
        vm.RegistrarSede = RegistrarSede;
        vm.CargarProvincia = CargarProvincia;
        vm.CargarDistrito = CargarDistrito;
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;
        vm.IdSede = $scope.$parent.vm.IdSede;
        vm.IdSedeInstalacion = $scope.$parent.vm.IdSedeInstalacion;
        vm.hideBoton = $scope.$parent.vm.hideBoton;
        vm.BuscarListaSedeInstalacion = $scope.$parent.vm.BuscarListaSedeInstalacion();
        vm.geocode = geocode;
        vm.geocodeCoordenadas = geocodeCoordenadas;
        //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si es existente.
        function CargaModalSede() {
            
            vm.IdSedeInstalacion = $scope.$parent.vm.IdSedeInstalacion;
            vm.IdSede = $scope.$parent.vm.IdSede;
            if (vm.IdSedeInstalacion > 0) {
                ObtenerSede();
            } else {
                CargaModal();
            }
        }

        function ObtenerSede() {
            blockUI.start();
            var Sede = {
                IdSedeInstalacion: vm.IdSedeInstalacion,
                IdSede: vm.IdSede
            }
            var promise = SedeService.ObtenerSedePorId(Sede);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdSede = vm.IdSede;
                vm.IdTipoEnlace = Respuesta.IdTipoEnlace;
                vm.Codigo = Respuesta.Codigo;
                vm.IdTipoSede = Respuesta.IdTipoSede;
                vm.IdAccesoCliente = Respuesta.IdAccesoCliente;
                vm.IdTendidoExterno = Respuesta.IdTendidoExterno;
                vm.NumeroPisos = Respuesta.NumeroPisos;
                vm.CodigoDepartamento = parseInt(Respuesta.Departamento);
                vm.CodigoProvincia = parseInt(Respuesta.Provincia);
                vm.CodigoDistrito = parseInt(Respuesta.Distrito);
                CargarProvincia(vm.CodigoDepartamento);
                CargarDistrito(vm.CodigoProvincia)
                vm.IdTipoVia = Respuesta.IdTipoVia;
                vm.Latitud = Respuesta.Latitud;
                vm.Longitud = Respuesta.Longitud;
                vm.Direccion = Respuesta.Direccion;
                vm.IdTipoServicio = Respuesta.IdTipoServicio;
                vm.Piso = Respuesta.Piso;
                vm.Interior = Respuesta.Interior;
                vm.Manzana = Respuesta.Manzana;
                vm.Lote = Respuesta.Lote;
                vm.IdEstado = Respuesta.IdEstado;
                vm.IdSedeInstalacion = vm.IdSedeInstalacion;
                vm.DesRequerimiento = Respuesta.DesRequerimiento;
                vm.FlgCotizacionReferencial = Respuesta.FlgCotizacionReferencial;
                vm.FlgRequisitosEspeciales = Respuesta.FlgRequisitosEspeciales;
            }, function (response) {
                blockUI.stop();

            });
        }

        function zeroFill(number, width) {
            width -= number.toString().length;
            if (width > 0) {
                return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
            }
            return number + ""; // siempre devuelve tipo cadena
        }

        function CargaModal() {
            vm.Idsede = "";
            vm.IdTipoEnlace = "-1";
            vm.Codigo = "";
            vm.IdTipoSede = "-1";
            vm.IdAccesoCliente = "-1";
            vm.IdTendidoExterno = "-1";
            vm.NumeroPisos = "";
            vm.CodigoDepartamento = "-1";
            vm.CodigoProvincia = "-1";
            vm.CodigoDistrito = "-1";
            vm.IdTipoVia = "-1";
            vm.Latitud = "";
            vm.Longitud = "";
            vm.Direccion = "";
            vm.IdTipoServicio = "-1";
            vm.Piso = "";
            vm.Interior = "";
            vm.Manzana = "";
            vm.Lote = "";
            vm.IdEstado = "1";
            vm.DesRequerimiento = "";
            vm.FlgCotizacionReferencial = "";
            vm.FlgRequisitosEspeciales = "";
            vm.IdSedeInstalacion = "";
        }

        function geocode() {
            debugger

            //var departamento = document.getElementById("dllCodigoDepartamento").value;
            //var provincia = document.getElementById("dllCodigoProvincia").value;
            var distrito = document.getElementById("dllCodigoDistrito").value;
            var Sede = {
                CodigoDistrito: vm.CodigoDistrito
            }

            blockUI.start();
            var promisePreventa =SedeService.DireccionBuscar(Sede);

            promisePreventa.then(function (resultadoPreventa) {
                var RespuestaPreventa = resultadoPreventa.data;
                var address = document.getElementById("address").value + ',' + RespuestaPreventa.LocalizacionSede;
                blockUI.stop();
                debugger
                geocoder.geocode({
                    'address': address,
                    'partialmatch': true
                }, geocodeResult);


            }, function (response) {
                UtilsFactory.Alerta('#divAlertRegistrarSede', 'danger', MensajesUI.DatosError, 5);
                blockUI.stop();
            });
        }

        function geocodeCoordenadas() {
            debugger

            //var departamento = document.getElementById("dllCodigoDepartamento").value;
            //var provincia = document.getElementById("dllCodigoProvincia").value;
            var distrito = document.getElementById("dllCodigoDistrito").value;
            var Sede = {
                CodigoDistrito: vm.CodigoDistrito
            }

            blockUI.start();
            var promisePreventa = SedeService.DireccionBuscar(Sede);

            promisePreventa.then(function (resultadoPreventa) {
                var RespuestaPreventa = resultadoPreventa.data;
                var address = document.getElementById("address").value + ',' + RespuestaPreventa.LocalizacionSede;
                blockUI.stop();
                debugger
                geocoder.geocode({
                    'address': address,
                    'partialmatch': true
                }, geocodeResultCoordenadas);


            }, function (response) {
                UtilsFactory.Alerta('#divAlertRegistrarSede', 'danger', MensajesUI.DatosError, 5);
                blockUI.stop();
            });
        }


        function RegistrarSede() {
            vm.Latitud = document.getElementById("lat").value;
            vm.Longitud = document.getElementById("lng").value;

            debugger
                var Sede = {
                    IdOportunidad: vm.IdOportunidad,
                    Id: vm.IdSede,
                    IdTipoEnlace: vm.IdTipoEnlace,
                    Codigo: vm.Codigo,
                    IdTipoSede: vm.IdTipoSede,
                    IdAccesoCliente: vm.IdAccesoCliente,
                    IdTendidoExterno: vm.IdTendidoExterno,
                    NumeroPisos: vm.NumeroPisos,
                    IdUbigeo: vm.IdUbigeo,
                    IdTipoVia: vm.IdTipoVia,
                    Latitud: vm.Latitud,
                    Longitud: vm.Longitud,
                    Direccion: vm.Direccion,
                    IdTipoServicio: vm.IdTipoServicio,
                    Piso: vm.Piso,
                    Interior: vm.Interior,
                    Manzana: vm.Manzana,
                    Lote: vm.Lote,
                    IdEstado: vm.IdEstado,
                    CodigoDepartamento: vm.CodigoDepartamento,
                    CodigoProvincia: vm.CodigoProvincia,
                    CodigoDistrito: vm.CodigoDistrito,
                    DesRequerimiento: vm.DesRequerimiento,
                    FlgCotizacionReferencial: vm.FlgCotizacionReferencial,
                    FlgRequisitosEspeciales: vm.FlgRequisitosEspeciales,
                    IdSedeInstalacion: vm.IdSedeInstalacion,
                }
            debugger;
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promisePreventa = (vm.IdSede > 0) ? SedeService.ActualizarSede(Sede) : SedeService.RegistrarSede(Sede);

                promisePreventa.then(function (resultadoPreventa) {
                    var RespuestaPreventa = resultadoPreventa.data;
                    if (RespuestaPreventa.TipoRespuesta == 0) {
                        vm.IdSede = RespuestaPreventa.Id;
                        vm.IdSedeInstalacion = RespuestaPreventa.IdDetalle;
                        $scope.$parent.vm.IdSedeInstalacion = vm.IdSedeInstalacion;
                        $scope.$parent.vm.IdSede = vm.IdSede;

                        //$scope.$parent.vm.BuscarSede();
                        //$('#ModalRegistrarSede').modal('hide');
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlertRegistrarSede', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlertRegistrarSede', 'danger', "No se pudo registrar el Sede.", 5);
                    }

                }, function (response) {
                    UtilsFactory.Alerta('#divAlertRegistrarSede', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            } else {

                UtilsFactory.Alerta('#divAlertRegistrarSede', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }

       

        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.Codigo) == "") {
                mensaje = mensaje + "Ingrese el Codigo de la Sede del Cliente" + '<br/>';
                UtilsFactory.InputBorderColor('#txtSedeCliente', 'Rojo');
            }
            if ($.trim(vm.IdTipoEnlace) == "-1") {
                mensaje = mensaje + "Elija Tipo de Enlace" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdTipoEnlace', 'Rojo');
            }
            if ($.trim(vm.CodigoDepartamento) == "-1") {
                mensaje = mensaje + "Elija el Departamento" + '<br/>';
                UtilsFactory.InputBorderColor('#dllCodigoDepartamento', 'Rojo');
            }
            if ($.trim(vm.CodigoProvincia) == "-1") {
                mensaje = mensaje + "Elija la Provincia" + '<br/>';
                UtilsFactory.InputBorderColor('#dllCodigoProvincia', 'Rojo');
            }
            if ($.trim(vm.CodigoDistrito) == "-1") {
                mensaje = mensaje + "Elija el distrito" + '<br/>';
                UtilsFactory.InputBorderColor('#dllCodigoDistrito', 'Rojo');
            }
            if ($.trim(vm.Direccion) == "-1") {
                mensaje = mensaje + "Ingrese la direccion" + '<br/>';
                UtilsFactory.InputBorderColor('#txtDireccion', 'Rojo');
            }
            return mensaje;
        }

        function ObtenerIdSeleccionado(dato) {
             var array = [];
             for (var i in dato) {
                 if(dato[i]==true){
                     array.push({ "id": i});
                 }
             }
             return array;
        }

        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#txtCodigo', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdTipoEnlace', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdTipoSede', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtSedeCliente', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtTipoSede', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdAccesoCliente', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdTendidoExterno', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtNroPisos', 'Ninguno');
            UtilsFactory.InputBorderColor('#dllCodigoDepartamento', 'Ninguno');
            UtilsFactory.InputBorderColor('#dllCodigoProvincia', 'Ninguno');
            UtilsFactory.InputBorderColor('#dllCodigoDistrito', 'Ninguno');
            UtilsFactory.InputBorderColor('#dllIdTipoVia', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtLatitud', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtLongitud', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtDireccion', 'Ninguno');
            UtilsFactory.InputBorderColor('#dllIdTipoServicio', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtPiso', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtInterior', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtManzana', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtLote', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtDesRequerimiento', 'Ninguno');
            UtilsFactory.InputBorderColor('#chckFlgCotizacionReferencial', 'Ninguno');
            UtilsFactory.InputBorderColor('#chckFlgRequisitosEspeciales', 'Ninguno');
        }

        function CargarAccesoCliente() {
            var filtro = {
                IdRelacion: 427
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListAccesoCliente = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarTendidoExterno() {
            var filtro = {
                IdRelacion: 430
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTendidoExterno = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarTipoEnlace() {
            var filtro = {
                IdRelacion: 89
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoEnlace = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarTipoSede() {
            var filtro = {
                IdRelacion: 406
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoSede = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function CargarTipoVia() {
            var filtro = {
                IdRelacion: 445
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoVia = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarTipoServicio() {
            var filtro = {
                IdRelacion: 7
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoServicio = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarDepartamento() {
            var promise = UbigeoService.ListarComboUbigeoDepartamento();
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListDepartamento = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarProvincia(CodigoDepartamento) {

            var codDepartamento = zeroFill(CodigoDepartamento, 2);
            if (codDepartamento != "-1") {
                var departamento = {
                    CodigoDepartamento: codDepartamento
                };
                var promise = UbigeoService.ListarComboUbigeoProvincia(departamento);
                promise.then(function (resultado) {
                    if (resultado == "") {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Provincias", 5);
                    } else {
                        var Respuesta = resultado.data;
                        vm.ListProvincia = UtilsFactory.AgregarItemSelect(Respuesta);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
            }
        }

        function CargarDistrito(CodigoProvincia) {

            var codProvincia = zeroFill(CodigoProvincia, 4);
            if (codProvincia != "-1") {
                var provincia = {
                    CodigoProvincia: codProvincia
                };
                var promise = UbigeoService.ListarComboUbigeoDistrito(provincia);
                promise.then(function (resultado) {
                    if (resultado == "") {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Distritos", 5);
                    } else {
                        var Respuesta = resultado.data;
                        vm.ListDistrito = UtilsFactory.AgregarItemSelect(Respuesta);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
            }
        }

        function CargarCombos() {
            CargarTipoEnlace();
            CargarTipoSede();
            CargarTipoVia();
            CargarTipoServicio();
            CargarDepartamento();
            CargarAccesoCliente();
            CargarTendidoExterno();
            vm.ListProvincia = UtilsFactory.AgregarItemSelect([]);
            vm.ListDistrito = UtilsFactory.AgregarItemSelect([]);
        }
        CargarCombos();

    }
})();