﻿(function () {
    'use strict',
    angular
        .module('app.Oportunidad')
        .service('EstudiosService', EstudiosService);

    EstudiosService.$inject = ['$http', 'URLS'];

    function EstudiosService($http, $urls) {
        var service = {
            ListarEstudiosPaginado: ListarEstudiosPaginado,
        };

        return service;

        function ListarEstudiosPaginado(Estudios) {
            return $http({
                url: $urls.ApiOportunidad + "Estudios/ListarEstudiosPaginado",
                method: "POST",
                data: JSON.stringify(Estudios)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();