﻿(function () {
    'use strict'
    angular
        .module('app.Oportunidad')
        .controller('BandejaContacto', BandejaContacto);
    
    BandejaContacto.$inject =
        [
        'ContactoService',
        'RegistrarContactoService',
        'blockUI',
        'UtilsFactory',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        'MensajesUI',
        '$timeout',
        'URLS',
        '$scope',
        '$compile',
        '$modal',
        '$injector'
        ];

    function BandejaContacto
        (
        ContactoService,
        RegistrarContactoService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        MensajesUI,
        $timeout,
        $urls,
        $scope,
        $compile,
        $modal,
        $injector
        ) {

        var vm = this;
        vm.TituloModal = '';
        vm.Id = 0;
        vm.IdSede = $scope.$parent.vm.IdSede;
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;
        vm.NombreApellidos = '';
        vm.NumeroTelefono = '';
        vm.NumeroCelular = '';
        vm.CorreoElectronico = '';
        vm.Cargo = '';
        vm.dtColumns = [
         DTColumnBuilder.newColumn('Id').notVisible(),
         DTColumnBuilder.newColumn('NombreApellidos').withTitle('Contacto').notSortable(),
         DTColumnBuilder.newColumn('NumeroTelefono').withTitle('Telefono').notSortable(),
         DTColumnBuilder.newColumn('NumeroCelular').withTitle('Celular').notSortable(),
         DTColumnBuilder.newColumn('Cargo').withTitle('Cargo').notSortable(),
         DTColumnBuilder.newColumn('CorreoElectronico').withTitle('Correo').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesContacto)
        ];
        vm.ListarContacto = ListarContacto;
        vm.RegistrarContacto = RegistrarContacto;
        vm.InactivarContacto = InactivarContacto;
        vm.ObtenerContactoPorId = ObtenerContactoPorId;
        vm.ModalContacto = ModalContacto;

        LimpiarGrilla();
        ListarContacto();

        function ModalContacto(Id) {
            vm.Id = Id;
            vm.TituloModal = (vm.Id == 0) ? 'REGISTRAR CONTACTO' : 'MODIFICAR CONTACTO';
            var contacto = { Id: vm.Id };

            var promise = RegistrarContactoService.ModalContacto(contacto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoContacto").html(content);
                });

                $('#ModalRegistrarContacto').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });
        }

        function AccionesContacto(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'><a id='tab-button' class='btn btn-sm' class='btn' ng-click='vm.ObtenerContactoPorId(" + data.Id + ");' title='Editar'>" + "<span class='fa fa-edit fa-lg'></span></a></div> " +
            "<div class='col-xs-4 col-sm-1'><a id='tab-button' class='btn btn-sm'  class='btn ' ng-click='vm.InactivarContacto(" + data.Id + ");' title='Inactivar'>" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
        }

        function InactivarContacto(Id) {
            if (confirm('¿Estas seguro de eliminar el registro ?')) {
                blockUI.start();

                vm.Id = Id;
                var contacto = { Id: vm.Id };
                var promise = ContactoService.InactivarContacto(contacto);

                promise.then(function (resultado) {
                    blockUI.stop();

                    var Respuesta = resultado.data;
                    vm.Id = Respuesta.Id;
                    UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
                    ListarContacto();

                }, function (response) {
                    blockUI.stop();
                });
            }
        }

        function ListarContacto() {
            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(ListarContactoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }

        function ListarContactoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var contacto = {
                IdSede: vm.IdSede,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ContactoService.ListarContactoPaginado(contacto);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaContactos
                };

                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }

        function LimpiarDatos() {
            vm.Id = 0;
            vm.NombreApellidos = '';
            vm.NumeroTelefono = '';
            vm.NumeroCelular = '';
            vm.CorreoElectronico = '';
            vm.Cargo = '';
        }

        function ValidarCampos() {
            if (vm.IdSede <= 0 || vm.IdSede == undefined) {
                UtilsFactory.Alerta('#divAlertRegistrarContacto', 'warning', 'No tiene local asignado', 5);
                return;
            }

            if (vm.NombreApellidos.trim() == "" || vm.NombreApellidos == undefined) {
                UtilsFactory.Alerta('#divAlertRegistrarContacto', 'warning', 'Ingrese el nombre y apellido', 5);
                return;
            }

            if (vm.NumeroTelefono == undefined || vm.NumeroTelefono.trim() == '') {
                UtilsFactory.Alerta('#divAlertRegistrarContacto', 'warning', 'Ingrese el numero de telefono', 5);
                return;
            }

            return true;
        }

        function RegistrarContacto() {
            if (!ValidarCampos())
                return;

            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();

                var contacto = {
                    Id: vm.Id,
                    IdSede: vm.IdSede,
                    NombreApellidos: vm.NombreApellidos,
                    NumeroTelefono: vm.NumeroTelefono,
                    NumeroCelular: vm.NumeroCelular,
                    CorreoElectronico: vm.CorreoElectronico,
                    Cargo: vm.Cargo,
                    IdEstado: 1
                };

                var promise = (vm.Id == 0) ?
                    ContactoService.RegistrarContacto(contacto) :
                    ContactoService.ActualizarContacto(contacto);

                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    vm.Id = Respuesta.Id;
                    UtilsFactory.Alerta('#divAlertRegistrarContacto', 'success', Respuesta.Mensaje, 5);
                    LimpiarDatos();
                    ListarContacto();

                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#divAlertRegistrarContacto', 'danger', MensajesUI.DatosError, 5);
                });
            }
        }

        function ObtenerContactoPorId(Id) {
            blockUI.start();

            var contacto = { Id: Id };
            var promise = ContactoService.ObtenerContactoPorId(contacto);

            promise.then(function (resultado) {
                blockUI.stop();

                contacto = resultado.data;
                vm.Id = contacto.Id;
                vm.NombreApellidos = contacto.NombreApellidos;
                vm.NumeroTelefono = contacto.NumeroTelefono;
                vm.NumeroCelular = contacto.NumeroCelular;
                vm.CorreoElectronico = contacto.CorreoElectronico;
                vm.Cargo = contacto.Cargo;
                vm.IdProbabilidad = riesgoProyecto.IdProbabilidad;
                vm.IdImpacto = riesgoProyecto.IdImpacto;

            }, function (response) {
                blockUI.stop();
            });
        }
    }
})();