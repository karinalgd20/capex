﻿(function () {
    'use strict',
    angular
    .module('app.Oportunidad')
    .service('BandejaContactoService', BandejaContactoService);

    BandejaContactoService.$inject = ['$http', 'URLS'];

    function BandejaContactoService($http, $urls) {

        var service = {
            VistaBandejaContacto: VistaBandejaContacto
        };

        return service;

        function VistaBandejaContacto() {
            return $http({
                url: $urls.ApiOportunidad + "BandejaContacto/Index",
                method: "GET",
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
    }
})();