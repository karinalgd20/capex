﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('OportunidadContenedorController', OportunidadContenedorController);

    OportunidadContenedorController.$inject = ['OportunidadContenedorService', 
    'LineaNegocioService',
    'CasoNegocioService', 
    'FlujoCajaService',
    'RegistrarOportunidadService',
    'RegistrarServicioService',
    'CaratulaService',
    'EcapexService', 
    'ConfiguracionAdicionalService',
    'FContableService',
    'FFinancieraService',
    'OTEService',
    'VisitaService',
    'SISEGOService',
    'SedeService',
    //'servicioPasoParametros',
    'RiesgoProyectoService',
    'BandejaCostoService',
    'RegistrarComponenteService',
    'RegistrarCasoNegocioService',
    'IndicadoresService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function OportunidadContenedorController(OportunidadContenedorService,
        LineaNegocioService,
        CasoNegocioService,
        FlujoCajaService,
        RegistrarOportunidadService,
        RegistrarServicioService,
        CaratulaService,
        EcapexService,
        ConfiguracionAdicionalService,
        FContableService,
        FFinancieraService,
        OTEService,
        VisitaService,
        SISEGOService,
        SedeService,
      //  servicioPasoParametros,
        RiesgoProyectoService,
        BandejaCostoService,
        RegistrarComponenteService,
        RegistrarCasoNegocioService,
        IndicadoresService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.ListLineaNegocio = [];
        vm.ListCasoNegocio = [];
        vm.IdLineaNegocio = '-1';
        vm.IdCaso = '-1';
        vm.IdOportunidadLineaNegocio = '-1';
        vm.ListarOportunidadLineaNegocio = [];
        vm.ListarCasoNegocio = ListarCasoNegocio;
        vm.ObjOportunidad = [];
        vm.ObjOportunidad = jsonOportunidad;
        vm.CargarCasoNegocio = CargarCasoNegocio;
        vm.EliminarCasoNegocio = EliminarCasoNegocio;
        vm.IdOportunidad = 0;

        vm.TabPorPerfilOportunidadDatos = TabPorPerfilOportunidadDatos;
        //Permition Tab
        

        vm.Tabs = false;
        vm.TabResumen = true;
        //Detalle Tab
        vm.TabOCosto = true;
        vm.TabFCFinanciero = true;
        vm.TabFCContable = true;
        vm.TabCAdicional = true;
        vm.TabIndicadores = true;

        vm.TabCapex = true;
        vm.TabCaratula = true;
        vm.TabDocumento = true;
        vm.tabFlujoCaja=true;

        vm.EditarDatoTable= true;
        TabPorPerfilOportunidadDatos();
        function TabPorPerfilOportunidadDatos() {


            var promise = OportunidadContenedorService.TabPorPerfilOportunidadDatos();

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.TabResumen = Respuesta.TabResumen;
                //Detalle Tab
                vm.TabOCosto = Respuesta.TabOCosto;
                vm.TabFCFinanciero = Respuesta.TabFCFinanciero;
                vm.TabFCContable = Respuesta.TabFCContable;
                vm.TabCAdicional = Respuesta.TabCAdicional;
                vm.TabIndicadores = Respuesta.TabIndicadores;

                vm.TabCapex = Respuesta.TabCapex;
                vm.TabCaratula = Respuesta.TabCaratula;
                vm.TabDocumento = Respuesta.TabDocumento;
                vm.Tabs = true;

                vm.EditarDatoTable = !Respuesta.EditarDatoTable;

            }, function (response) {
                blockUI.stop();

            });
        }




      




        if (vm.ObjOportunidad != "") {
            vm.IdOportunidadLineaNegocio = vm.ObjOportunidad.IdOportunidadLineaNegocio;
            vm.IdLineaNegocio = vm.ObjOportunidad.IdLineaNegocio;
            vm.IdOportunidad = vm.ObjOportunidad.IdOportunidad;
         
           
            
        }
        vm.CargaModalCasoNegocio = CargaModalCasoNegocio;
        vm.CargaModalServicio = CargaModalServicio;
        vm.ModalRegistrarComponente = ModalRegistrarComponente;


        vm.ValidaOportunidad = ValidaOportunidad;
        vm.CargaDetalle = CargaDetalle;
        vm.CargaPreImplantacion = CargaPreImplantacion;
        vm.CargaSISEGO = CargaSISEGO;
        vm.CargaCaratula = CargaCaratula;
        vm.CargaECapex = CargaECapex;
        vm.CargaCAdicional = CargaCAdicional;
        vm.CargaFContable = CargaFContable;
        vm.CargaFFinanciera = CargaFFinanciera;
        vm.CargaOTE = CargaOTE;
        vm.CargaRiesgoProyecto = CargaRiesgoProyecto;
        vm.CargaVisitas = CargaVisitas;
        vm.CargaServiciosSISEGO = CargaServiciosSISEGO;
        vm.CargaSedes = CargaSedes;
        vm.CargaOportunidadCosto = CargaOportunidadCosto;
        vm.CargaIndicadores = CargaIndicadores;
        /******************************************* Metodos *******************************************/

        function CargaIndicadores() {

            var promise = IndicadoresService.VistaIndicador();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#indicadores").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaDetalle() {

            var promise = OportunidadContenedorService.VistaDetalle();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#detalle").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaPreImplantacion() {
            var promise = OportunidadContenedorService.VistaPreImplantacion();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#preImplantacion").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaSISEGO() {
            var promise = OportunidadContenedorService.VistaSISEGO();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#sisego").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaCaratula() {

            var promise = CaratulaService.VistaCaratula();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#caratula").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaECapex() {

            var promise = EcapexService.VistaEcapex();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ecapex").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaCAdicional() {
            var promise = ConfiguracionAdicionalService.VistaConfiguracionAdicional();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#configuracionAdicional").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        };

        function CargaFContable() {
            var promise = FContableService.VistaFContable();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#contable").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        };

        function CargaFFinanciera() {
            var promise = FFinancieraService.VistaFFinanciera();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#financiero").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaOTE() {
            var promise = OTEService.VistaOTE();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ote").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaRiesgoProyecto() {
            var promise = RiesgoProyectoService.VistaRiesgoProyecto();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#riesgo").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        };

        function CargaVisitas() {
            var promise = VisitaService.VistaVisita();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#visitas").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });
        };

        function CargaServiciosSISEGO() {
            var promise = SISEGOService.VistaSISEGO();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#serviciosSISEGO").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };
        
        function CargaSedes() {
            var promise = SedeService.VistaSede();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#sedes").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };
        function CargaOportunidadCosto() {
            var promise = BandejaCostoService.VistaBandejaCosto();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#oportunidadcosto").html(content);
                });

            }, function (response) {
                blockUI.stop();
            });

        };


        function ValidaOportunidad() {
            if (vm.IdOportunidadLineaNegocio > 0) {

                CargaCaratula();
                CargaECapex();
            }
        };

        function ListarLineaNegocio() {

            var linea = {
                IdEstado: 1
            };
            var promise = LineaNegocioService.ListarLineaNegocios(linea);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarCasoNegocio() {

            var casonegocio = {
                IdLineaNegocio: vm.IdLineaNegocio,
                IdEstado: 1
            };

            var promise = CasoNegocioService.ListarCasoNegocioDefecto(casonegocio);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListCasoNegocio = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {

                blockUI.stop();

            });
        };

        function ListarOportunidadLineaNegocio() {
            
            var IdSession = 0;

          //  if (servicioPasoParametros.data.parametro != null && servicioPasoParametros.data.parametro != '0') {
            //OportunidadContenedorService.VistaDetalleCargado(servicioPasoParametros.data.parametro);
            var promise = OportunidadContenedorService.ObtenerIdOportunidadSession();
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
               
                IdSession = Respuesta;


                if (IdSession!=0) {
                    var dto = {
                        IdOportunidad: IdSession
                        // servicioPasoParametros.data.parametro 
                    };

                    var promise = RegistrarOportunidadService.ObtenerOportunidadLineaNegocio(dto);

                    promise.then(function (resultado) {
                        blockUI.stop();
                        var Respuesta = resultado.data;
                        vm.ObjOportunidad = Respuesta;

                        vm.IdOportunidadLineaNegocio = vm.ObjOportunidad.IdOportunidadLineaNegocio;
                      
                        vm.IdLineaNegocio = vm.ObjOportunidad.IdLineaNegocio;
                        vm.IdOportunidad = vm.ObjOportunidad.IdOportunidad;

                        var oportunidadLineaNegocio = {
                            IdOportunidad: vm.IdOportunidad
                        };

                        var promise = OportunidadContenedorService.ListarOportunidadLineaNegocio(oportunidadLineaNegocio);

                        promise.then(function (resultado) {
                            blockUI.stop();
                            var Respuesta = resultado.data;
                            vm.ListOportunidadLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);
                            if (vm.ObjOportunidad.IdOportunidadLineaNegocio > 0) {
                                vm.IdOportunidadLineaNegocio = vm.ObjOportunidad.IdOportunidadLineaNegocio;
                           //     $scope.$parent.vm.IdOportunidadLineaNegocio = vm.IdOportunidadLineaNegocio;

                            }
                            vm.ListarCasoNegocio();

                        }, function (response) {
                            blockUI.stop();
                        });

                    }, function (response) {
                        blockUI.stop();
                    });
                } 
          //  else {
                //    var oportunidadLineaNegocio = {
                //        IdOportunidad: vm.IdOportunidad
                //    };

                //    var promise = OportunidadContenedorService.ListarOportunidadLineaNegocio(oportunidadLineaNegocio);

                //    promise.then(function (resultado) {
                //        blockUI.stop();
                //        var Respuesta = resultado.data;
                //        vm.ListOportunidadLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);
                //        if (vm.IdOportunidadLineaNegocio > 0) {
                //            vm.IdOportunidadLineaNegocio = vm.ObjOportunidad.IdOportunidadLineaNegocio;
                //        }

                //    }, function (response) {
                //        blockUI.stop();
                //    });
                //}

            }, function (response) {

                blockUI.stop();

            });

          
        };

        function CargarCasoNegocio() {

            var casonegocio = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdCasoNegocio: vm.IdCaso,
                IdLineaNegocio: vm.IdLineaNegocio
            };

            if (confirm('¿Estas seguro de Generar el caso de negocio?')) {
                var promise = FlujoCajaService.GeneraCasoNegocio(casonegocio);
                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta != 0) {
                        UtilsFactory.Alerta('#alertContenedor', 'danger', Respuesta.Mensaje, 20);
                    } else {

                        UtilsFactory.Alerta('#alertContenedor', 'success', "Se genero el caso correctamente.", 10);
                        ValidaOportunidad();
                        UtilsFactory.DeshabilitarElemento('#CmbCasoNegocio');
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#alertContenedor', 'danger', MensajesUI.DatosError, 5);
                });
            }
            else {
                blockUI.stop();
            }
        };

        function EliminarCasoNegocio() {

            var casonegocio = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdCasoNegocio: vm.IdCaso
            };

            if (confirm('¿Estas seguro de eliminar el Caso de Negocio?')) {
                var promise = FlujoCajaService.EliminarCasoNegocio(casonegocio);
                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta > 0) {
                        ValidaOportunidad();
                        UtilsFactory.Alerta('#alertContenedor', 'success', Respuesta.Mensaje, 10);
                        
                    } else {
                        UtilsFactory.Alerta('#alertContenedor', 'danger', Respuesta.Mensaje, 20);
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#alertContenedor', 'danger', MensajesUI.DatosError, 5);
                });
            }
            else {
                blockUI.stop();
            }
        }

        function CargaModalServicio() {

            var promise = RegistrarServicioService.ModalServicios();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoServicios").html(content);
                });

                $('#ModalServicios').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };
        function CargaModalCasoNegocio() {

            var promise = RegistrarCasoNegocioService.ModalCasoNegocio();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCasoNegocio").html(content);
                });

                $('#ModalCasoNegocio').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };
        function ModalRegistrarComponente() {

            var promise = RegistrarComponenteService.ModalRegistrarComponente();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoComponente").html(content);
                });

                $('#ModalComponente').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };


        function Test() {
          //  alert(servicioPasoParametros.data.parametro);
        }

        ListarOportunidadLineaNegocio();
        ListarLineaNegocio();
        ListarCasoNegocio();
    }

})();