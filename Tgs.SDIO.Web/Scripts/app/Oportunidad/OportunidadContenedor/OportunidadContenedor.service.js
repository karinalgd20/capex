﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('OportunidadContenedorService', OportunidadContenedorService);

    OportunidadContenedorService.$inject = ['$http', 'URLS'];

    function OportunidadContenedorService($http, $urls) {

        var service = {
            VistaDetalle: VistaDetalle,
            VistaPreImplantacion: VistaPreImplantacion,
            VistaSISEGO: VistaSISEGO,
            ListarOportunidadLineaNegocio: ListarOportunidadLineaNegocio,
            RegistroIdOportunidadSession: RegistroIdOportunidadSession,
            ObtenerIdOportunidadSession: ObtenerIdOportunidadSession,
            TabPorPerfilOportunidadDatos: TabPorPerfilOportunidadDatos,
            RegistroIdFlujoCajaSession: RegistroIdFlujoCajaSession,
            ObtenerIdFlujoCajaSession: ObtenerIdFlujoCajaSession
        };

        return service;

        function RegistroIdOportunidadSession(Id) {
            return $http({
                url: $urls.ApiOportunidad + "OportunidadContenedor/RegistroIdOportunidadSession",
                method: "POST",
                params: { Id: Id}

            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function TabPorPerfilOportunidadDatos() {
            return $http({
                url: $urls.ApiOportunidad + "OportunidadContenedor/TabPorPerfilOportunidadDatos",
                method: "POST"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerIdOportunidadSession() {
            return $http({
                url: $urls.ApiOportunidad + "OportunidadContenedor/ObtenerIdOportunidadSession",
                method: "POST"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function VistaDetalle() {
            return $http({
                url: $urls.ApiOportunidad + "OportunidadContenedor/Detalle",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function VistaPreImplantacion() {
            return $http({
                url: $urls.ApiOportunidad + "OportunidadContenedor/PreImplantacion", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function VistaSISEGO() {
            return $http({
                url: $urls.ApiOportunidad + "OportunidadContenedor/SISEGO", 
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarOportunidadLineaNegocio(request) {
            return $http({
                url: $urls.ApiOportunidad + "OportunidadContenedor/ListarLineaNegocioPorIdOportunidad",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistroIdFlujoCajaSession(Id) {
            return $http({
                url: $urls.ApiOportunidad + "OportunidadContenedor/RegistroIdFlujoCajaSession",
                method: "POST",
                params: { Id: Id }

            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerIdFlujoCajaSession() {
            return $http({
                url: $urls.ApiOportunidad + "OportunidadContenedor/ObtenerIdFlujoCajaSession",
                method: "POST"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();