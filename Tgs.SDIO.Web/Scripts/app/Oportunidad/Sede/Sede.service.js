﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('SedeService', SedeService);

    SedeService.$inject = ['$http', 'URLS'];

    function SedeService($http, $urls) {

        var service = {
            VistaSede: VistaSede,
            ModalEditar: ModalEditar,
            Obtener: Obtener,
            ListarSede: ListarSede,
            RegistrarSede: RegistrarSede,
            ListarSedeInstalacion: ListarSedeInstalacion,
            AgregarSedeInstalacion: AgregarSedeInstalacion,
            ObtenerSedePorId: ObtenerSedePorId,
            ActualizarSede: ActualizarSede,
            ListarSedeServiciosPaginado: ListarSedeServiciosPaginado,
            DireccionBuscar: DireccionBuscar
        };

        return service;


        function DireccionBuscar(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/DireccionBuscar",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ActualizarSede(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/ActualizarSede",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function AgregarSedeInstalacion(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/AgregarSedeInstalacion",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function RegistrarSede(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/RegistrarSede",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function VistaSede() {
            return $http({
                url: $urls.ApiOportunidad + "Sede/Index",
                method: "GET"
            }).then(DatosCompletados);
 
            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ModalEditar(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        }

        function Obtener(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/ObtenerSedePorIdOportunidad",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ListarSede(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/ListarSubservicioPaginado",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ObtenerSedePorId(recurso) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/ObtenerSedePorId",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ListarSedeInstalacion(recurso) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/ListarSedeInstalacion",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function ListarSedeServiciosPaginado(request) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/ListarSedeServiciosPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }

        function DetalleSedePorId(request) {
            return $http({
                url: $urls.ApiOportunidad + "Sede/DetalleSedePorId",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }
    }
})();