﻿var marker;
var coords = {};
var geocoder = null;
var map;

initMap = function (map_lat, map_lon, map_zoom) {
    debugger

    geocoder = new google.maps.Geocoder();

    //usamos la api para geolocalizar el usuario
    latitude = document.getElementById('lat').value;
    longitude = document.getElementById('lng').value;

    map_lat = typeof latitude !== 'undefined' ? latitude : "-12.125496518888895";
    map_lon = typeof longitude !== 'undefined' ? longitude : "-77.02936288724351";

    var coords = {
        lng: map_lon,
        lat: map_lat
    };

    setMapa(coords, map); //pasamos las coordenadasal metodo para crear el mapa

}

function setMapa(coords) {
    //Se crea una nueva instancia del objeto mapa
    debugger
    var myOptions = {
        zoom: 18,
        center: new google.maps.LatLng(coords.lat, coords.lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(coords.lat, coords.lng),
    });

    document.getElementById('lat').value = typeof coords.lat !== 'undefined' ? coords.lat : coords.lat;
    document.getElementById('lng').value = typeof coords.lng !== 'undefined' ? coords.lng : coords.lng;

    marker.addListener('dragend', function (event) {
        debugger
        document.getElementById('lat').value = typeof this.getPosition().lat() !== 'undefined' ? this.getPosition().lat() : coords.lat;
        document.getElementById('lng').value = typeof this.getPosition().lat() !== 'undefined' ? this.getPosition().lng() : coords.lng;
    });
}

function updatePosition(latLng) {
    debugger
    jQuery('#lat').val(latLng.lat());
    jQuery('#long').val(latLng.lng());

}


function geocodeResult(results, status) {
    debugger
    //obtengo la direccion del formulario
    var direccion = document.getElementById("address").value;
    var distrito = jQuery("#dllCodigoDistrito option:selected").text();
    var address = direccion + " " + distrito;
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == 'OK') {

            var myOptions = {
                zoom: 18,
                center: new google.maps.LatLng(latitude, longitude),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

            map.setCenter(results[0].geometry.location);
            marker.setPosition(results[0].geometry.location);
            debugger
            document.getElementById("lat").value = results[0].geometry.location.lat();
            document.getElementById("lng").value = results[0].geometry.location.lng();

            google.maps.event.addListener(marker, 'dragend', function () {
                updatePosition(marker.getPosition());
            });

            marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                draggable: true

            });

            marker.addListener('dragend', function (event) {
            debugger
            document.getElementById('lat').value = typeof this.getPosition().lat() !== 'undefined' ? this.getPosition().lat() : coords.lat;
            document.getElementById('lng').value = typeof this.getPosition().lat() !== 'undefined' ? this.getPosition().lng() : coords.lng;

            });

        } else {
            alert("No podemos encontrar la direcci&oacute;n, error: " + status);
        }
    });
}

function geocodeResultCoordenadas(results, status) {
    debugger
    var latitud = document.getElementById("lat").value;
    var longitud = document.getElementById("lng").value;
    var coordena = {
        lng: longitud,
        lat: latitud
    };

    if (coordena.lng != "" && coordena.lat != "") {
        setMapa(coordena);
        //document.getElementById("address").value = results[0].formatted_address;
    }

    //geocoder.geocode({ 'address': address }, function (results, status) {
    //    if (status == 'OK') {
    //        debugger
    //        var myOptions = {
    //            zoom: 18,
    //            center: new google.maps.LatLng(coordena.lat, coordena.lng),
    //            mapTypeId: google.maps.MapTypeId.ROADMAP
    //        };

    //        var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

    //        map.setCenter(results[0].geometry.location);
    //        marker.setPosition(results[0].geometry.location);
    //        debugger
    //        document.getElementById("lat").value = coordena.lat;
    //        document.getElementById("lng").value = coordena.lng;

    //        google.maps.event.addListener(marker, 'dragend', function () {
    //            updatePosition(marker.getPosition());
    //        });

    //        marker = new google.maps.Marker({
    //            map: map,
    //            position: new google.maps.LatLng(coords.lat, coords.lng),
    //            draggable: true

    //        });

    //        marker.addListener('dragend', function (event) {
    //            debugger
    //            document.getElementById('lat').value = typeof this.getPosition().lat() !== 'undefined' ? this.getPosition().lat() : coords.lat;
    //            document.getElementById('lng').value = typeof this.getPosition().lat() !== 'undefined' ? this.getPosition().lng() : coords.lng;

    //        });
    //        debugger
    //        document.getElementById('address').innerHTML = results[0].formatted_address;

    //    } else {
    //        alert("No podemos encontrar la direcci&oacute;n, error: " + status);
    //    }
    //});
}