﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('SedeController', SedeController);

    SedeController.$inject = ['SedeService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function SedeController(SedeService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.TituloModal = "Editar Sede";

        vm.Id = 0;
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;

        vm.ListTipoSede = [];
        vm.IdTipoSede = "-1";

        vm.BuscarSede = BuscarSede;
        vm.CargaModal = CargaModal;

        /******************************************* Sede *******************************************/
        vm.dtInstanceSede = {};

        vm.dtColumnsSede = [
         DTColumnBuilder.newColumn('Id').notVisible(),
         DTColumnBuilder.newColumn(null).notSortable().renderWith(DrawCheckBox),
         DTColumnBuilder.newColumn('Sede').withTitle('Sede').notSortable(),
         DTColumnBuilder.newColumn('Servicio').withTitle('Servicio').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaSede)

        ];

        function AccionesBusquedaSede(data, type, full, meta) {
            var respuesta = "<div class='col-xs-3 col-sm-3'><a title='Eliminar' class='btn btn-sm'  id='tab-button' class='btn ' ng-click='vm.EliminarSedeDetalle(" + data.Id + ")' >" + "<span class='fa fa-trash-o'></span></a></div>";
            return respuesta;
        };

        function DrawCheckBox(data, type, full, meta) {
            return "<div class='col-xs-3 col-sm-3'> <input type='checkbox' ng-model='" + data.FlagSeleccionado + "' /> </div> ";
        };

        function BuscarSede() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsSede = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarSedePaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarSedePaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var records = {
                'draw': draw,
                'recordsTotal': 0,
                'recordsFiltered': 0,
                'data': {}
            };

            blockUI.stop();
            fnCallback(records);

        };

        function CargarSede() {
            if (vm.IdOportunidad != null && vm.IdOportunidad != 0) {
                var dto = {
                    IdOportunidad: vm.IdOportunidad
                };

                var promise = SedeService.Obtener(dto);
                promise.then(function (response) {
                    if (response.data != null && response.data != '') {
                        vm.Id = response.data.Id;
                        vm.Sedente = response.data.PreVentaSedente;
                        vm.Sededo = response.data.ClienteSededo;
                        vm.FechaSede = UtilsFactory.ToJavaScriptDate(response.data.Fecha);
                        vm.HoraInicio = response.data.HoraInicio.Hours + ":" + response.data.HoraInicio.Minutes;
                        vm.HoraFin = vm.HoraFin = response.data.HoraFin.Hours + ":" + response.data.HoraFin.Minutes;
                        vm.IdTipoSede = response.data.IdTipo;
                        vm.dtInstanceSede.rerender();
                    }
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    LimpiarGrilla();
                });

            }
        };
        /******************************************* Funciones *******************************************/

        function ValidarDatos() {
            if (vm.Sedente == undefined || vm.Sedente == '') {
                UtilsFactory.Alerta('#lblAlerta_Sede', 'warning', 'Ingrese el visitante', 5);
                return;
            }

            if (vm.Sededo == undefined || vm.Sededo == '') {
                UtilsFactory.Alerta('#lblAlerta_Sede', 'warning', 'Ingrese al cliente visitado', 5);
                return;
            }

            return true;
        }

        function LimpiarGrilla() {
            vm.dtOptionsSede = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', true)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function CargaModal(idSede) {
            blockUI.start();
            var dto = {
                IdSede: idSede
            }

            var promise = SedeService.ModalEditar(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);

                    $("#ContenidoSede").html(content);
                    blockUI.stop();
                });

                $('#ModalSede').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargarTipoSede() {
            var filtro = {
                IdRelacion: 278
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoSede = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        /******************************************* LOAD *******************************************/
        BuscarSede();
        
    }
})();



