﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('SedeEditController', SedeEditController);

    SedeEditController.$inject = ['SedeService', 'MaestraService', 'UbigeoService', 'blockUI', 'UtilsFactory', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function SedeEditController(SedeService, MaestraService, UbigeoService, blockUI, UtilsFactory, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.TituloModal = "Editar";
        vm.Id = 0;
        //vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;
        vm.IdTipoEnlace = '-1';
        vm.IdTipoSede = '-1';
        vm.IdTipoVia = '-1';
        vm.IdTipoServicio = '-1';
        vm.CodigoDepartamento = '-1';
        vm.CodigoProvincia = '-1';
        vm.CodigoDistrito = '-1';
        
        vm.GrabarSede = GrabarSede;
        vm.CargarProvincia = CargarProvincia;
        vm.CargarDistrito = CargarDistrito;
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;
        /******************************************* Sede *******************************************/
        
        function GrabarSede() {
            vm.Latitud = document.getElementById('latitud').value;
            vm.Longitud = document.getElementById('longitud').value;

            if (!ValidarDatos())
                return;

            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();
                var dto = {
                    Id: vm.Id,
                    IdTipoEnlace: vm.IdTipoEnlace,
                    IdTipoSede: vm.IdTipoSede,
                    Sede: vm.Sede,
                    AccesoCliente: vm.AccesoCliente,
                    TendidoExterno: vm.TendidoExterno,
                    NumeroPisos: vm.NumeroPisos,
                    CodigoDepartamento: vm.CodigoDepartamento,
                    CodigoProvincia: vm.CodigoProvincia,
                    CodigoDistrito: vm.CodigoDistrito,
                    IdTipoVia: vm.IdTipoVia,
                    Direccion: vm.Direccion,
                    Latitud: vm.Latitud,
                    Longitud: vm.Longitud

                };

                var promise = SedeService.RegistrarSede(dto);
                promise.then(function (response) {
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'danger', MensajesUI.DatosError, 20);
                    } else {
                        UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'success', response.data.Mensaje, 10);
                        $scope.$parent.vm.dtInstanceSede.rerender();
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'danger', MensajesUI.DatosError, 5);
                });

            };
        }

        /******************************************* Funciones *******************************************/
        function ValidarDatos() {
            if (vm.IdTipoEnlace == '-1') {
                UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Seleccione el tipo de enlace', 5);
                return;
            }

            if (vm.Sede == undefined || vm.Sede.trim() == '') {
                UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Ingrese la sede', 5);
                return;
            }

            //Esto es un inputtext o sera un combo?
            //if (vm.IdTipoSede == '-1') {
            //    UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Seleccione el tipo de sede', 5);
            //    return;
            //}

            if (vm.AccesoCliente == undefined || vm.AccesoCliente.trim() == '') {
                UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Ingrese el acceso', 5);
                return;
            }
                        
            if (vm.TendidoExterno == undefined || vm.TendidoExterno.trim() == '') {
                UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Ingrese el tendido externo', 5);
                return;
            }
            
            if (vm.NumeroPisos == undefined || vm.NumeroPisos.trim() == '') {
                UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Ingrese el número de pisos', 5);
                return;
            }

            if (vm.CodigoDepartamento == '-1') {
                UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Seleccione el departamento', 5);
                return;
            }

            if (vm.CodigoProvincia == '-1') {
                UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Seleccione la provincia', 5);
                return;
            }

            if (vm.CodigoDistrito == '-1') {
                UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Seleccione el distrito', 5);
                return;
            }

            if (vm.IdTipoVia == '-1') {
                UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Seleccione el tipo de via', 5);
                return;
            }

            if (vm.Direccion == undefined || vm.Direccion.trim() == '') {
                UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Ingrese la dirección', 5);
                return;
            }

            if (vm.IdTipoServicio == '-1') {
                UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Seleccione el tipo de servicio', 5);
                return;
            }

            if (vm.Latitud == undefined || vm.Latitud == '') {
                UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'warning', 'Seleccione la ubicacion en el mapa', 5);
                return;
            }

            return true;
        }

        function CargarSedeDetalle() {
            
            var dto = {
                Id: vm.Id,
                IdSede: vm.IdSede
            };

            var promise = SedeService.ObtenerSedeDetalle(dto);

            promise.then(function (resultado) {
                var respuesta = resultado.data;
                if (respuesta != null && respuesta != '') {
                    vm.FechaAcuerdo = respuesta.FechaAcuerdo;
                    vm.DescripcionPunto = respuesta.DescripcionPunto;
                    vm.Responsable = respuesta.Responsable;
                    Cumplimiento = respuesta.Cumplimiento;

                    UtilsFactory.Alerta('#lblAlerta_SedeDetalle', 'success', respuesta.Mensaje, 10);
                    $scope.$$childHead.vm.ObtenerFlujoCajaConfiguracion();
                }
                blockUI.stop();
                
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarTipoEnlace() {
            var filtro = {
                IdRelacion: 89
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoEnlace = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

       function CargarTipoSede() {
            var filtro = {
                IdRelacion: 406
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoSede = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };


        function CargarTipoVia() {
            var filtro = {
                IdRelacion: 281
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoVia = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };
        
        function CargarTipoServicio() {
            var filtro = {
                IdRelacion: 7
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoServicio = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };
        function zeroFill(number, width) {
            width -= number.toString().length;
            if (width > 0) {
                return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
            }
            return number + ""; // siempre devuelve tipo cadena
        }

        function CargarDepartamento() {
            var promise = UbigeoService.ListarComboUbigeoDepartamento();
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListDepartamento = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarProvincia(CodigoDepartamento) {
        
             var codDepartamento= zeroFill(CodigoDepartamento, 2);
            if (codDepartamento != "-1") {
                var departamento = {
                    CodigoDepartamento: codDepartamento
                };
                var promise = UbigeoService.ListarComboUbigeoProvincia(departamento);
                promise.then(function (resultado) {
                    if (resultado == "") {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Provincias", 5);
                    } else {
                        var Respuesta = resultado.data;
                        vm.ListProvincia = UtilsFactory.AgregarItemSelect(Respuesta);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
            }
        }

        function CargarDistrito(CodigoProvincia) {

            var codProvincia = zeroFill(CodigoProvincia, 4);
            if (codProvincia != "-1") {
                var provincia = {
                    CodigoProvincia: codProvincia
                };
                var promise = UbigeoService.ListarComboUbigeoDistrito(provincia);
                promise.then(function (resultado) {
                    if (resultado == "") {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Distritos", 5);
                    } else {
                        var Respuesta = resultado.data;
                        vm.ListDistrito = UtilsFactory.AgregarItemSelect(Respuesta);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
            }
        }

        function CargarCombos(){
            CargarTipoEnlace();
            CargarTipoSede();
            CargarTipoVia();
            CargarTipoServicio();
            CargarDepartamento();
            vm.ListProvincia = UtilsFactory.AgregarItemSelect([]);
            vm.ListDistrito = UtilsFactory.AgregarItemSelect([]);
        }

        /******************************************* LOAD *******************************************/
        
        CargarCombos();
        //CargarSedeDetalle();

    }
})();



