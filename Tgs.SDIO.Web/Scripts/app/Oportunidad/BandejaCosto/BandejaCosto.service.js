﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('BandejaCostoService', BandejaCostoService);

    BandejaCostoService.$inject = ['$http', 'URLS'];

    function BandejaCostoService($http, $urls) {

        var service = {
            VistaBandejaCosto: VistaBandejaCosto,
            ListaOportunidadCostoPaginado: ListaOportunidadCostoPaginado
        };

        return service;

        function VistaBandejaCosto() {
            return $http({
                url: $urls.ApiOportunidad + "BandejaCosto/Index",
                method: "GET"
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

          function ListaOportunidadCostoPaginado(oportunidadCosto) {

            return $http({
                url: $urls.ApiOportunidad + "BandejaCosto/ListaOportunidadCostoPaginado",
                method: "POST",
                data: JSON.stringify(oportunidadCosto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();