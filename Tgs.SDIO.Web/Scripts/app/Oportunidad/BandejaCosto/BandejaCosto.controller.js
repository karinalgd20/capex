﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('BandejaCostoController', BandejaCostoController);

    BandejaCostoController.$inject = ['BandejaCostoService','RegistrarCostoService','MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function BandejaCostoController(BandejaCostoService,RegistrarCostoService,MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;


        vm.ModalCosto = ModalCosto;
        vm.BuscarCosto = BuscarCosto;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.IdOportunidadCosto = 0;
        vm.ListTipoCosto=[];
        vm.IdTipoCosto="-1";

        vm.IdOportunidadLineaNegocio = $scope.$parent.$parent.vm.IdOportunidadLineaNegocio;

        ListarMaestraPorIdRelacion();
        function ModalCosto(IdOportunidadCosto) {
            vm.IdOportunidadCosto = IdOportunidadCosto;
            debugger
            var Costo = {
                IdOportunidadCosto: IdOportunidadCosto
            };

            var promise = RegistrarCostoService.ModalCosto(Costo);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCosto").html(content);
                });

                $('#ModalRegistrarCosto').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        //

   vm.dtColumns = [
            
                   DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
                   DTColumnBuilder.newColumn('Monto').withTitle('Monto').notSortable(),
                
                   DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCosto)
            ];
  
        /*--------------------------------------*/
       
        /******************************************* Metodos *******************************************/
        LimpiarGrilla();
        function AccionesBusquedaCosto(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-2'><a title='Editar' class='btn btn-sm'  id='tab-button' ng-click='vm.ModalCosto(" + data.IdOportunidadCosto + "," + data.IdTipoCosto + ");' >" + "<span class='fa fa-edit'></span></a></div>";
        }


        
        function cargarGrilla() {
         
            vm.dtColumns = null;
           

            if (vm.IdTipoCosto == "1") {
          
            
               
            vm.dtColumns = [
                   DTColumnBuilder.newColumn('CodigoModelo').withTitle('Codigo').notSortable(),
                   DTColumnBuilder.newColumn('Modelo').withTitle('Modelo').notSortable(),
                   DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
                   DTColumnBuilder.newColumn('Monto').withTitle('Monto').notSortable(),
                   DTColumnBuilder.newColumn('Instalacion').withTitle('Instalacion').notSortable(),
                   DTColumnBuilder.newColumn('CostoSegmentoSatelital').withTitle('Satelital').notSortable(),
                   DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCosto)
            ];
        }
            if (vm.IdTipoCosto == "2") {
           
            vm.dtColumns = [                 
                   DTColumnBuilder.newColumn('Monto').withTitle('Monto').notSortable(),            
                   DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
                   DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCosto)
            ];
        }
        if (vm.IdTipoCosto == "3") {
           
            vm.dtColumns = [
                   DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
                   DTColumnBuilder.newColumn('Monto').withTitle('Monto').notSortable(),
                   DTColumnBuilder.newColumn('CostoSegmentoSatelital').withTitle('Satelital').notSortable(),
                   DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCosto)
            ];
        }
        if (vm.IdTipoCosto == "5") {
          
           
        
            vm.dtColumns = [
                   DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
                   DTColumnBuilder.newColumn('Monto').withTitle('Monto').notSortable(),
                   DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCosto)
            ];
        }
        if (vm.IdTipoCosto == "6") {
           
            vm.dtColumns = [

                   DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
                   DTColumnBuilder.newColumn('Monto').withTitle('Monto').notSortable(),
                   DTColumnBuilder.newColumn('CostoSegmentoSatelital').withTitle('Satelital').notSortable(),
                   DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCosto)
            ];
        }
        if (vm.IdTipoCosto == "7") {
           
           
            vm.dtColumns = [

                   DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
                   DTColumnBuilder.newColumn('Monto').withTitle('Monto').notSortable(),
                   DTColumnBuilder.newColumn('VelocidadSubidaKBPS').withTitle('VelocidadSubidaKBPS').notSortable(),
                    DTColumnBuilder.newColumn('PorcentajeGarantizado').withTitle('PorcentajeGarantizado').notSortable(),
                   DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCosto)
            ];
        }
        if (vm.IdTipoCosto == "8") {
           
            vm.dtColumns = [
                   DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),

                  DTColumnBuilder.newColumn('VelocidadSubidaKBPS').withTitle('VelocidadSubidaKBPS').notSortable(),
                   DTColumnBuilder.newColumn('PorcentajeGarantizado').withTitle('PorcentajeGarantizado').notSortable(),
                   DTColumnBuilder.newColumn('PorcentajeSobresuscripcion').withTitle('PorcentajeSobresuscripcion').notSortable(),
                   DTColumnBuilder.newColumn('CostoSegmentoSatelital').withTitle('CostoSegmentoSatelital').notSortable(),
                   DTColumnBuilder.newColumn('AntenaCasaClienteUSD').withTitle('AntenaCasaClienteUSD').notSortable(),
                   DTColumnBuilder.newColumn('Instalacion').withTitle('Instalacion').notSortable(),
                   DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCosto)
            ];
        }
        if (vm.IdTipoCosto == "9") {
       
            vm.dtColumns = [
                   DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
                   DTColumnBuilder.newColumn('Modelo').withTitle('Modelo').notSortable(),
                   DTColumnBuilder.newColumn('CostoSegmentoSatelital').withTitle('CostoSegmentoSatelital').notSortable(),
                   DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCosto)
            ];
        } if (vm.IdTipoCosto == "10") {
           
            vm.dtColumns = [
                   DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
                   DTColumnBuilder.newColumn('Modelo').withTitle('Modelo').notSortable(),
                   DTColumnBuilder.newColumn('CostoSegmentoSatelital').withTitle('CostoSegmentoSatelital').notSortable(),
                   DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCosto)
            ];
        }
      if(vm.IdTipo=="-1") {
    
          
            vm.dtColumns = [
                  DTColumnBuilder.newColumn('Monto').withTitle('Monto').notSortable(),
                   DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
                   DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCosto)
            ];
        }

            }
       

        function BuscarCosto() {
            blockUI.start();
         
       
            LimpiarGrilla();
          
           
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarCostoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }
        function BuscarCostoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var costo = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdTipoCosto:vm.IdTipoCosto,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = BandejaCostoService.ListaOportunidadCostoPaginado(costo);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListOportunidadCostoDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        function LimpiarFiltros() {

            vm.IdTipoCosto = "-1";
            LimpiarGrilla();
        }
        
          function ListarMaestraPorIdRelacion() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 349

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListTipoCosto = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }
        function EliminarCosto(IdCosto) {

            var Costo = {
                IdCosto: IdCosto
            };

            if (confirm('¿Estas seguro de eliminar el Costo?')) {
                var promise = BandejaCostoService.EliminarCosto(Costo);
                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#alertCasoNegocio', 'danger', Respuesta.Mensaje, 20);
                    } else {

                        UtilsFactory.Alerta('#alertCasoNegocio', 'success', Respuesta.Mensaje, 10);
                        BuscarCosto();
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#alertCasoNegocio', 'danger', MensajesUI.DatosError, 5);
                });
            }
            else {
                blockUI.stop();
            }
        }

    }
})();



