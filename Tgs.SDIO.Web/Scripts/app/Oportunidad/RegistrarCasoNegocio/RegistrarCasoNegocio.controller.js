﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('RegistrarCasoNegocioController', RegistrarCasoNegocioController);

    RegistrarCasoNegocioController.$inject = ['RegistrarCasoNegocioService', 'CasoNegocioService', 'FlujoCajaService','CasoNegocioServicioService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarCasoNegocioController(RegistrarCasoNegocioService, CasoNegocioService, FlujoCajaService,CasoNegocioServicioService,
    blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.ListarCasoNegocio = ListarCasoNegocio;
        vm.Descripcion = '';
        vm.ListCasoNegocio = [];
        vm.IdCasoNegocio = '-1';
        vm.BuscarCasoNegocio = BuscarCasoNegocio;
      


        vm.selectedCasoNegocio = {};
        vm.IdServicioSubServicio = "";
        vm.selectAllCasoNegocio = false;
        vm.toggleAllCasoNegocio = toggleAllCasoNegocio;
        vm.toggleOneCasoNegocio = toggleOneCasoNegocio;
        vm.RegistrarCasoNegocioServicio = RegistrarCasoNegocioServicio;


        var titleHtmlCasoNegocio = '<input type="checkbox" ng-model="vm.selectAllCasoNegocio" ng-click="vm.toggleAllCasoNegocio(vm.selectAllCasoNegocio, vm.selectedCasoNegocio)">';
        /******************************************* Tabla *******************************************/
        vm.dtInstanciaCasoNegocioModal = [];
        vm.dtOptionsCasoNegocioModal = {};
        vm.dtColumnsCasoNegocioModal = [


         DTColumnBuilder.newColumn(null).withTitle(titleHtmlCasoNegocio)
        .notSortable().withOption('width', '3%')
        .renderWith(function (data, type, full, meta) {
            vm.selectedCasoNegocio[full.IdServicioSubServicio] = false;
            return '<input type="checkbox" ng-model="vm.selectedCasoNegocio[\'' + data.IdServicioSubServicio + '\']" ng-click="vm.toggleOneCasoNegocio(vm.selectedCasoNegocio)">';
        }),
         DTColumnBuilder.newColumn('DescripcionServicio').withTitle('Servicio').notSortable(),
         DTColumnBuilder.newColumn('DescripcionSubServicio').withTitle('Componente').notSortable(),

        ];



        function toggleAllCasoNegocio(selectAllCasoNegocio, selectedCasoNegocioItems) {
            for (var IdServicioSubServicio in selectedCasoNegocioItems) {
                if (selectedCasoNegocioItems.hasOwnProperty(IdServicioSubServicio)) {
                    selectedCasoNegocioItems[IdServicioSubServicio] = selectAllCasoNegocio;
                }
            }
        }

        function toggleOneCasoNegocio(selectedCasoNegocioItems) {
            for (var IdServicioSubServicio in selectedCasoNegocioItems) {
                if (selectedCasoNegocioItems.hasOwnProperty(IdServicioSubServicio)) {
                    if (!selectedCasoNegocioItems[IdServicioSubServicio]) {
                        vm.selectAllCasoNegocio = false;
                        return;
                    }
                }
            }
            vm.selectAllCasoNegocio = true;
        }

        function BuscarCasoNegocio() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsCasoNegocioModal = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarCasoNegocioPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('headerCallback', function (header) {
                        $compile.headerCompiled = true;
                        $compile(angular.element(header).contents())($scope);
                    });
            }, 500);
        };

        function BuscarCasoNegocioPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var CasoNegocio = {
                IdCasoNegocio: vm.IdCasoNegocio,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = CasoNegocioServicioService.ListPaginadoCasoNegocioServicio(CasoNegocio);
            promise.then(function (response) {

                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListCasoNegocioServicioDtoResponse
                };

                blockUI.stop();
                fnCallback(records);
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsCasoNegocioModal = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
        };



        function ValidarCampos() {
            var mensaje = "";
            if (JSON.stringify(vm.selectedCasoNegocio).indexOf("true") == -1) {
                mensaje = mensaje + "Seleccione Componente" + '<br/>';
                UtilsFactory.InputBorderColor('#gridSedeCliente', 'Rojo');
            }
            return mensaje;
        }

        function RegistrarCasoNegocioServicio() {



            var casoNegocio = {
                IdCasoNegocio: vm.IdCasoNegocio,
                ListServicioSelect: ObtenerIdSeleccionado(vm.selectedCasoNegocio),
                IdOportunidadLineaNegocio: $scope.$parent.$parent.vm.IdOportunidadLineaNegocio

            }
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promisePreventa = FlujoCajaService.RegistrarCasoNegocioServicio(casoNegocio);

                promisePreventa.then(function (resultadoPreventa) {
                    var RespuestaPreventa = resultadoPreventa.data;
                    if (RespuestaPreventa.TipoRespuesta == 0) {

                        $('#ModalAgregarCasoNegocio').modal('hide');
                        blockUI.stop();
                        UtilsFactory.Alerta('#lblAlerta_AgregarCasoNegocio', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        blockUI.stop();
                        UtilsFactory.Alerta('#lblAlerta_AgregarCasoNegocio', 'danger', "No se pudo registrar la CasoNegocio.", 5);
                    }

                }, function (response) {
                    UtilsFactory.Alerta('#lblAlerta_AgregarCasoNegocio', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            } else {

                UtilsFactory.Alerta('#lblAlerta_AgregarCasoNegocio', 'danger', mensaje, 5);
                $timeout(function () {

                }, 3000);
            }
        }

        function ObtenerIdSeleccionado(dato) {
            var array = [];
            for (var i in dato) {
                if (dato[i] == true) {
                    array.push({ "id": i });
                }
            }
            return array;
        }

        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#txtNombre', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtRuc', 'Ninguno');
        }

        /******************************************* Funciones *******************************************/

        function ListarCasoNegocio() {
            var casoNegocio = {
                IdLineaNegocio: $scope.$parent.$parent.vm.IdLineaNegocio,
                IdEstado: 1
            };
            var promise = CasoNegocioService.ListarCasoNegocioDefecto(casoNegocio);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListCasoNegocio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        };



        /******************************************* Load *******************************************/
        ListarCasoNegocio();
    }
})();