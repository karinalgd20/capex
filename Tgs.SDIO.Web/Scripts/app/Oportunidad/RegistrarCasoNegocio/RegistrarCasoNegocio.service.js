﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('RegistrarCasoNegocioService', RegistrarCasoNegocioService);

    RegistrarCasoNegocioService.$inject = ['$http', 'URLS'];

    function RegistrarCasoNegocioService($http, $urls) {

        var service = {
            ModalCasoNegocio: ModalCasoNegocio
        };

        return service;


        function ModalCasoNegocio(request) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarCasoNegocio/Index",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})(); 