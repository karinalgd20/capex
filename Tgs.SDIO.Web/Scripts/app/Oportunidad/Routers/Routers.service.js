﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('RoutersService', RoutersService);

    RoutersService.$inject = ['$http', 'URLS'];

    function RoutersService($http, $urls) {

        var service = {
            ModalRouter: ModalRouter,
            CantidadRouters: CantidadRouters
        };

        return service;

        function ModalRouter(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Routers/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function CantidadRouters(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Routers/CantidadRouters", 
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

    }
})();