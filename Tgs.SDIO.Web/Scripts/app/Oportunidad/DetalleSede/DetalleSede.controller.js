﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('DetalleSede', DetalleSede);

    DetalleSede.$inject =
        [
        'OportunidadContenedorService',
        'ContactoService',
        'RegistrarPlantillaImplantacionService',
        'PlantillaImplantacionService',
        'blockUI',
        'UtilsFactory',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        '$timeout',
        'MensajesUI',
        'URLS',
        '$scope',
        '$compile',
        '$injector'
        ];

    function DetalleSede
        (
        OportunidadContenedorService,
        ContactoService,
        RegistrarPlantillaImplantacionService,
        PlantillaImplantacionService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile,
        $injector
        ) {

        var vm = this;
        vm.ListarPlantillaImplantacion = ListarPlantillaImplantacion;
        vm.ModalPlantillaImplantacion = ModalPlantillaImplantacion;
        vm.InactivarPlantillaImplantacion = InactivarPlantillaImplantacion;

        vm.ObjSede = [];
        vm.ObjSede = jsonSede;
        vm.IdSede = 0;
        vm.TipoSede = '';
        vm.Direccion = '';
        vm.Departamento = '';
        vm.Provincia = '';
        vm.Distrito = '';
        vm.IdOportunidad = 0;
        vm.IdFlujoCaja = 0;

        vm.dtColumns = [
         DTColumnBuilder.newColumn('Id').notVisible(),
         DTColumnBuilder.newColumn('NombreApellidos').withTitle('Nombre y Apellidos').notSortable(),
         DTColumnBuilder.newColumn('NumeroTelefono').withTitle('Telefono').notSortable(),
         DTColumnBuilder.newColumn('NumeroCelular').withTitle('Celular').notSortable(),
         DTColumnBuilder.newColumn('Cargo').withTitle('Cargo').notSortable(),
         DTColumnBuilder.newColumn('CorreoElectronico').withTitle('Correo Electronico').notSortable()
        ];

        vm.dtColumnsPlantillaImplantacion = [
         DTColumnBuilder.newColumn('Id').notVisible(),
         DTColumnBuilder.newColumn('DescripcionTipoEnlaceCircuitoDatos').withTitle('Principal / Contigencia').notSortable(),
         DTColumnBuilder.newColumn('DescripcionTipoCircuitoDatos').withTitle('Tipo de CD').notSortable(),
         DTColumnBuilder.newColumn('NumeroCircuitoDatos').withTitle('Número de CD').notSortable(),
         DTColumnBuilder.newColumn('ConectadoCircuitoDatos').withTitle('conectado al CDK/CDO/CDR/CDW').notSortable(),
         DTColumnBuilder.newColumn('DescripcionServicioGrupoCircuitoDatos').withTitle('Tipo de Servicio').notSortable(),
         DTColumnBuilder.newColumn('DescripcionCostoCircuitoDatos').withTitle('BW (Mbps, Kbps)').notSortable(),
         DTColumnBuilder.newColumn('LargaDistanciaNacionalCircuitoDatos').withTitle('Caudal LDN').notSortable(),
         DTColumnBuilder.newColumn('DescripcionVozTos5CaudalAntiguo').withTitle('Voz TOS 5').notSortable(),
         DTColumnBuilder.newColumn('DescripcionVozTos4CaudalAntiguo').withTitle('Video TOS 4').notSortable(),
         DTColumnBuilder.newColumn('DescripcionVozTos3CaudalAntiguo').withTitle('Platinum TOS 3').notSortable(),
         DTColumnBuilder.newColumn('DescripcionVozTos2CaudalAntiguo').withTitle('Oro TOS 2').notSortable(),
         DTColumnBuilder.newColumn('DescripcionVozTos1CaudalAntiguo').withTitle('Plata TOS 1').notSortable(),
         DTColumnBuilder.newColumn('DescripcionVozTos0CaudalAntiguo').withTitle('Bronce TOS 0').notSortable(),
         DTColumnBuilder.newColumn('UltimaMillaCircuitoDatos').withTitle('Ultima Milla').notSortable(),
         DTColumnBuilder.newColumn('EquipoCpeCircuitoDatos').withTitle('Equipo CPE').notSortable(),
         DTColumnBuilder.newColumn('EquipoTerminalCircuitoDatos').withTitle('Equipo terminal (EFM, MODEM,TELCO, DATACOM)').notSortable(),
         DTColumnBuilder.newColumn('VrfCircuitoDatos').withTitle('VRF').notSortable(),
         DTColumnBuilder.newColumn('DescripcionAccionIsis').withTitle('Accion ISIS').notSortable(),
         DTColumnBuilder.newColumn('DescripcionTipoEnlaceServicioOfertado').withTitle('Principal / Contingencia').notSortable(),
         DTColumnBuilder.newColumn('DescripcionTipoServicioOfertado').withTitle('Tipo de CD').notSortable(),
         DTColumnBuilder.newColumn('NumeroServicioOfertado').withTitle('Número de CD').notSortable(),
         DTColumnBuilder.newColumn('DescripcionServicioGrupoServicioOfertado').withTitle('Tipo de Servicio').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesPlantillaImplantacion)
        ];

        LimpiarGrilla();
        LimpiarGrillaPlantillaImplantacion();

        if (vm.ObjSede != '') {
            vm.IdSede = vm.ObjSede.IdSede;
            vm.TipoSede = vm.ObjSede.TipoSede;
            vm.Direccion = vm.ObjSede.Direccion;
            vm.Departamento = vm.ObjSede.Departamento;
            vm.Provincia = vm.ObjSede.Provincia;
            vm.Distrito = vm.ObjSede.Distrito;
            ObtenerIdFlujoCajaSession();
            ListarContacto();
            ObtenerIdOportunidadSession();
            ListarPlantillaImplantacion();

        }

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }

        function LimpiarGrillaPlantillaImplantacion() {
            vm.dtOptionsPlantillaImplantacion = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }

        function ListarContacto() {
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(ListarContactoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }

        function ListarPlantillaImplantacion() {
            LimpiarGrillaPlantillaImplantacion();

            $timeout(function () {
                vm.dtOptionsPlantillaImplantacion = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(ListarPlantillaImplantacionPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }

        function ListarContactoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var contacto = {
                IdSede: vm.IdSede,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ContactoService.ListarContactoPaginado(contacto);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaContactos
                };

                fnCallback(result)

            }, function (response) {
                LimpiarGrilla();
            });
        }

        function ListarPlantillaImplantacionPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var filtro = {
                IdOportunidad: vm.IdOportunidad,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = PlantillaImplantacionService.ListarPlantillaImplantacionPaginado(filtro);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaPlantillaImplantacion
                };

                fnCallback(result)

            }, function (response) {
                LimpiarGrillaPlantillaImplantacion();
            });
        }

        function AccionesPlantillaImplantacion(data, type, full, meta) {
            return "<div class='col-xs-4 col-sm-1'><a class='btn btn-sm' class='btn' ng-click='vm.InactivarPlantillaImplantacion(" + data.Id + ");' title='Inactivar'>" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
        }

        function InactivarPlantillaImplantacion(Id) {
            if (confirm('¿Estas seguro de eliminar el registro ?')) {
                blockUI.start();

                var request = {
                    Id: Id,
                    IdEstado: 0
                };
                var promise = PlantillaImplantacionService.InactivarPlantillaImplantacion(request);

                promise.then(function (resultado) {
                    blockUI.stop();

                    var Respuesta = resultado.data;
                    vm.Id = Respuesta.Id;
                    UtilsFactory.Alerta('#divAlertInactivarPlantillaImplantacion', 'success', Respuesta.Mensaje, 5);
                    ListarPlantillaImplantacion();

                }, function (response) {
                    blockUI.stop();
                });
            }
        }

        function ModalPlantillaImplantacion() {
            var promise = RegistrarPlantillaImplantacionService.ModalPlantillaImplantacion();
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoPlantillaImplantacion").html(content);
                });

                $('#ModalRegistrarPlantillaImplantacion').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        }

        function ObtenerIdOportunidadSession() {
            var promise = OportunidadContenedorService.ObtenerIdOportunidadSession();

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.IdOportunidad = Respuesta;

            }, function (response) {
                blockUI.stop();
            });
        }

        function ObtenerIdFlujoCajaSession() {
            var promise = OportunidadContenedorService.ObtenerIdFlujoCajaSession();

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.IdFlujoCaja = Respuesta;

            }, function (response) {
                blockUI.stop();
            });
        }
    }
})();