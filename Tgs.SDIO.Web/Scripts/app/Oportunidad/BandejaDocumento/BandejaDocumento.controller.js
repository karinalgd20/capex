﻿(function () {
    'use strict'

    angular
    .module('app.BandejaDocumento')
    .controller('BandejaDocumento', BandejaDocumento);

    BandejaDocumento.$inject = ['BandejaDocumentoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];

    function BandejaDocumento(BandejaDocumentoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {

        var vm = this;

        //<a class="btn btn-sm btn-primary btn-space right" href="{{vm.EnlaceRegistrar}}0"><span class="glyphicon glyphicon-list-alt"></span> Nuevo</a>
        vm.Descargar = $urls.ApiBandejaDocumento + "DocumentoRegistrar/Index/";
        vm.UploadFile = UploadFile;
        vm.UploadFileIndividual = UploadFileIndividual;
        vm.fileNameChanged = fileNameChanged;
        vm.SelectedFiles = "";
        vm.BuscarDocumento = BuscarDocumento;

        vm.EliminarDocumento = EliminarDocumento;
        vm.AceptarEliminarDocumento = AceptarEliminarDocumento; 
        vm.LimpiarFiltros = LimpiarFiltros; 
        vm.DescripcionSegmento = '';
        vm.Descripcion = '';
        vm.IdDocumento = "";
        vm.MensajeDeAlerta = "";
        LimpiarGrilla();
        vm.Progres = 0;

        vm.dtColumns = [
         DTColumnBuilder.newColumn('IdArchivo').withTitle('Codigo').notSortable(),
          DTColumnBuilder.newColumn('NombreArchivo').withTitle('Nombre').notSortable(),
          DTColumnBuilder.newColumn('Descripcion').withTitle('Descripción').notSortable(),
          DTColumnBuilder.newColumn('IdEstado').withTitle('Estado').notSortable(),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaDocumento)
        ];


        function EliminarDocumento(IdDocumento) {
            vm.btnEliminar = true;
            $("#idArchivo").val("");
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            modalpopupConfirm.modal('show');
            $("#idArchivo").val(IdDocumento);
        }

        function AceptarEliminarDocumento() {
            var ddlIdarchivo = angular.element(document.getElementById("idArchivo"));
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            var archivo = {
                IdArchivo: ddlIdarchivo.val()
            }
            blockUI.start();
            var promise = BandejaDocumentoService.EliminarDocumento(archivo);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.ResponseID == 0) {
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                    BuscarDocumento()
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', Respuesta.ResponseMSG, 5);
                }
                modalpopupConfirm.modal('hide');
            }, function (response) {
                blockUI.stop();
            });

        }


        function LimpiarModal() {
            var ddlEstado = angular.element(document.getElementById("ddlEstado"));
            vm.txtDescripcion = '';
            vm.txtOrdenVisual = '';
            vm.btnActualizar = false;
            vm.btnAgregar = true;
        }

        function BuscarDocumento() {
            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(ListarDocumentoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }
        function ListarDocumentoPaginado(sSource, aoData, fnCallback, oSettings) {

            //var draw = aoData[0].value;
            //var start = aoData[3].value;
            //var length = aoData[4].value;

            var draw = 1;
            var start = 1;
            var length = 3;
            //var archivo = {
            //    IdEstado: 1,

            //    Indice: start,
            //    Tamanio: length
            //};
            var archivo = {
                IdEstado: 1
            };

            var promise = BandejaDocumentoService.ListarDocumentoPaginado(archivo);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                var result = {
                    'draw': 1,
                    'recordsTotal': 3,
                    'recordsFiltered': 4,
                    'data': resultado.data
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        //Cargar Archivos//




        vm.fileList = [];
        vm.curFile;
        vm.ImageProperty = {
            file: ''
        }


        function fileNameChanged(element) {
            vm.fileList = [];
            // get the files
            var files = element.files;
            for (var i = 0; i < files.length; i++) {
                vm.ImageProperty.file = files[i];

                vm.fileList.push($scope.ImageProperty);
                vm.ImageProperty = {};
                vm.$apply();

            }

        }
        function UploadFile() {
            for (var i = 0; i < vm.fileList.length; i++) {

                UploadFileIndividual(vm.fileList[i].file,
                                            vm.fileList[i].file.name,
                                            vm.fileList[i].file.type,
                                            vm.fileList[i].file.size,
                                            i);
            }

        }

        function UploadFileIndividual(fileToUpload, name, type, size, index) {
            //Create XMLHttpRequest Object
            var reqObj = new XMLHttpRequest();

            //event Handler
            reqObj.upload.addEventListener("progress", uploadProgress, false)
            reqObj.addEventListener("load", uploadComplete, false)
            reqObj.addEventListener("error", uploadFailed, false)
            reqObj.addEventListener("abort", uploadCanceled, false)


            //open the object and set method of call(get/post), url to call, isasynchronous(true/False)
            reqObj.open("POST", "BandejaDocumento/BandejaDocumento/UploadFiles", true);

            //set Content-Type at request header.For file upload it's value must be multipart/form-data
            reqObj.setRequestHeader("Content-Type", "multipart/form-data");

            //Set Other header like file name,size and type
            reqObj.setRequestHeader('X-File-Name', name);
            reqObj.setRequestHeader('X-File-Type', type);
            reqObj.setRequestHeader('X-File-Size', size);


            // send the file
            reqObj.send(fileToUpload);

            function uploadProgress(evt) {
                if (evt.lengthComputable) {

                    var uploadProgressCount = Math.round(evt.loaded * 100 / evt.total);

                    document.getElementById('P' + index).innerHTML = uploadProgressCount;

                    if (uploadProgressCount == 100) {
                        document.getElementById('P' + index).innerHTML =
                       '<i class="fa fa-refresh fa-spin" style="color:maroon;"></i>';
                    }

                }
            }

            function uploadComplete(evt) {
                /* This event is raised when the server  back a response */

                document.getElementById('P' + index).innerHTML = 'Saved';
                $scope.NoOfFileSaved++;
                $scope.$apply();
            }

            function uploadFailed(evt) {
                document.getElementById('P' + index).innerHTML = 'Upload Failed..';
            }

            function uploadCanceled(evt) {

                document.getElementById('P' + index).innerHTML = 'Canceled....';
            }

        }

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }

        function AccionesBusquedaDocumento(data, type, full, meta) {
            return "<div class='col-xs-6 col-sm-6'><a title='Editar' class='btn btn-primary'  id='tab-button' href='" + vm.Descargar + data.IdDocumento + "' class='btn' style='background-color: #d5d807;' >" + "<span class='fa fa-edit fa-lg'></span>" + "</a></div> " +
            "<div class='col-xs-6 col-sm-6'> <a title='Eliminar' class='btn btn-primary'   id='tab-button' class='btn '  onclick='EliminarDocumento(" + data.IdDocumento + ");'>" + "<span class='fa fa-trash-o fa-lg'></span>" + "</a> </div>";
        }
        function LimpiarFiltros() {
            vm.Nombre = '';
            vm.Descripcion = '';
            LimpiarGrilla();
        }


    }
})();