﻿(function () {
    'use strict',
     angular
    .module('app.BandejaDocumento')
    .service('BandejaDocumentoService', BandejaDocumentoService);

    BandejaDocumentoService.$inject = ['$http', 'URLS'];

    function BandejaDocumentoService($http, $urls) {

        var service = {
            CargarArchivo: CargarArchivo,
            ListarBandejaDocumentoPaginado: ListarBandejaDocumentoPaginado,
            EliminarBandejaDocumento:EliminarBandejaDocumento,
            RegistrarBandejaDocumento:RegistrarBandejaDocumento           
        };

        return service;



        function CargarArchivo(archivo) {
            return $http({
                url: $urls.ApiBandejaDocumento + "DocumentoBandeja/Upload",
                method: "POST",
                data: JSON.stringify(archivo) 
            }).then(DatosCompletados);

            function DatosCompletados(archivo) {
                return archivo;
            }
        };



        function ListarBandejaDocumentoPaginado(BandejaDocumento) {
            return $http({
                url: $urls.ApiBandejaDocumento + "BandejaDocumentoBandeja/ListarBandejaDocumento",
                method: "POST",
                data: JSON.stringify(BandejaDocumento)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarBandejaDocumento(BandejaDocumento) {
            return $http({
                url: $urls.ApiBandejaDocumento + "BandejaDocumentoBandeja/EliminarBandejaDocumento",
                method: "POST",
                data: JSON.stringify(BandejaDocumento)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarBandejaDocumento(BandejaDocumento) {
            return $http({
                url: $urls.ApiBandejaDocumento + "BandejaDocumentoRegistrar/RegistrarBandejaDocumento",
                method: "POST",
                data: JSON.stringify(BandejaDocumento)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();