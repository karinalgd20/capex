﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('EstudiosEspecialesController', EstudiosEspecialesController);

    EstudiosEspecialesController.$inject = ['EstudiosEspecialesService','LineaConceptoCmiService', 'FlujoCajaService', 'EcapexService', 'SubServicioDatosCapexService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$injector'];

    function EstudiosEspecialesController(EstudiosEspecialesService, LineaConceptoCmiService, FlujoCajaService,EcapexService, SubServicioDatosCapexService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile,$injector) {

        var vm = this;
        vm.tabFlujoCaja = true;
        vm.CantidadEstudioEsp = CantidadEstudioEsp;
        vm.CantidadEstudiosEspecialesComplete = CantidadEstudiosEspecialesComplete;
        vm.GrabarEstudiosEspeciales = GrabarEstudiosEspeciales;
        vm.CargarEstudioEspecial = CargarEstudioEspecial;
        vm.IdFlujoCaja = 0; 
        vm.IdGrupo = 6;
        vm.Antiguedad = 0;
        vm.IdOportunidadLineaNegocio = 0;
        vm.IdSubServicioDatosCapex = 0;
        vm.IdFlujoCajaConfiguracion = 0;
        vm.IdPeriodos = 0;
        vm.ObtieneCostoPreOperativo = ObtieneCostoPreOperativo;
        vm.ListarTipo = ListarTipo;
        vm.ListTipo = [];
        vm.IdTipo = "-1";

        /******************************************* Metodos *******************************************/

            function ListarTipo() {

                var pestanaGrupoTipo = {
                    IdGrupo: 6
               };
                 var promise = EcapexService.ListarPestanaGrupoTipo(pestanaGrupoTipo);
                 promise.then(function (resultado) {
                 blockUI.stop();

                  var Respuesta = resultado.data;

                   vm.IdTipo = "-1";
                    vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);
            
                }, function (response) {
                    blockUI.stop();
                });
            }
 
            function CargarEstudioEspecial() {

                var dto = {
                    IdSubServicioDatosCapex: $scope.$parent.vm.dto.IdSubServicioDatosCapex,
                    IdGrupo: 6
                };  

                var promise = FlujoCajaService.ObtenerDetalleOportunidadEcapex(dto);
                promise.then(function (response) {

                    console.log(response.data);
                    var result = response.data;
                        ListarTipo();
            
                    vm.SubServicio = result.SubServicio;
                    vm.Cruce = result.Cruce;
                    vm.AEReducido = result.AEReducido;
                    vm.Circuito = result.Circuito;
                    vm.Sisego = result.SISEGO;
                    vm.Antiguedad = result.MesesAntiguedad;
                    vm.Cantidad = result.Cantidad;
                    vm.CuAntiguo = result.CostoUnitarioAntiguo;
                    vm.ValorResidual = result.ValorResidualSoles;
                    vm.CuSoles = result.CostoUnitario;
                    vm.CapexDolares = result.CapexDolares;
                    vm.CapexSoles = result.CapexSoles;
                    vm.TotalCapex = result.TotalCapex;
                    vm.MesRq = result.MesRecupero;
                    vm.MesComprometido = result.MesComprometido;
                    vm.MesCertificado = result.MesCertificado;
                
                    vm.IdFlujoCaja = result.IdFlujoCaja;
                    vm.IdOportunidadLineaNegocio = result.IdOportunidadLineaNegocio;
                    vm.IdSubServicioDatosCapex = result.IdSubServicioDatosCapex;
                    vm.IdFlujoCajaConfiguracion = result.IdFlujoCajaConfiguracion;
                    // vm.IdPeriodos = result.IdPeriodos;
                    vm.IdPeriodos = result.IdPeriodos==0?"1":result.IdPeriodos;
                    ObtieneCostoPreOperativo();

                    // $scope.$$childHead.vm.Meses = result.TiempoProyecto;
                    $scope.$$childHead.vm.ObtenerFlujoCajaConfiguracion();
                
                        if(result.IdTipoSubServicio> 0){ListarTipoPorId(result.IdTipoSubServicio)}else{ListarTipo();}
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                });

            };
                   function ListarTipoPorId(int) {

                var pestanaGrupoTipo = {
                    IdGrupo: 6
                };
                var promise = EcapexService.ListarPestanaGrupoTipo(pestanaGrupoTipo);
                promise.then(function (resultado) {
                    blockUI.stop();

                    var Respuesta = resultado.data;

                    vm.IdTipo = int;
                    vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);

                }, function (response) {
                    blockUI.stop();
                });
            }
        function GrabarEstudiosEspeciales() {

            var flujocaja = {
                IdFlujoCaja: vm.IdFlujoCaja,
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                Cantidad: vm.Cantidad,
                CostoUnitario: vm.CapexSoles,
                IdTipoSubServicio: vm.IdTipo
            };


            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();

                var promise = FlujoCajaService.ActualizarOportunidadFlujoCaja(flujocaja);
                promise.then(function (response) {
                 
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                      blockUI.stop();
                        UtilsFactory.Alerta('#lblAlerta_EstudiosEspecialesModal', 'danger', MensajesUI.DatosError, 20);
                    } else {

                        var capex = {
                            IdFlujoCaja: vm.IdFlujoCaja,
                            IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                            Cruce: vm.Cruce,
                            AEReducido: vm.AEReducido,
                            Circuito: vm.Circuito,
                            SISEGO: vm.Sisego,
                            Antiguedad: vm.Antiguedad,
                            Cantidad: vm.Cantidad,
                            CostoUnitarioAntiguo: vm.CuAntiguo,
                            ValorResidualSoles: vm.ValorResidual,
                            CostoUnitario: vm.CuSoles,
                            CapexDolares: vm.CapexDolares,
                            CapexSoles: vm.CapexSoles,
                            TotalCapex: vm.TotalCapex,
                            MesRecupero: vm.MesRq,
                            MesComprometido: vm.MesComprometido,
                            MesCertificado: vm.MesCertificado,
                            IdTipoSubServicio: vm.IdTipo,
                            MesesAntiguedad:vm.Antiguedad 
                        };

                        var promise = SubServicioDatosCapexService.ActualizarSubServicioDatosCapex(capex);
                        promise.then(function (response) {
                            blockUI.stop();
                            var Respuesta = response.data;
                            if (Respuesta.TipoRespuesta != 0) {
                                UtilsFactory.Alerta('#lblAlerta_EstudiosEspecialesModal', 'danger', Respuesta.Mensaje, 20);
                            } else {
                                $scope.$parent.vm.BuscarEstudiosEspeciales();
                                $scope.$$childTail.vm.GrabarFlujoCajaConfiguracion();
                                UtilsFactory.Alerta('#lblAlerta_EstudiosEspecialesModal', 'success', Respuesta.Mensaje, 10);
                            }

                        }, function (response) {
                            blockUI.stop();
                            UtilsFactory.Alerta('#lblAlerta_EstudiosEspecialesModal', 'danger', MensajesUI.DatosError, 5);
                        });
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_EstudiosEspecialesModal', 'danger', MensajesUI.DatosError, 5);
                });
            };
        };


        function CantidadEstudioEsp() {
            vm.Cantidad = vm.Sisego != "" ? 1 : 0;
        };

        function CantidadEstudiosEspecialesComplete() {
            var dto = {
                IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                IdGrupo: vm.IdGrupo,
                IdEstado: 1,
                Cu: vm.CuSoles,
                Cantidad: vm.Cantidad,
                CuAntiguiedad: vm.CuAntiguo,
                MesesAntiguedad: vm.Antiguedad,
                Indice: vm.Indice,
                IdTipoSubServicio: vm.IdTipo
              //  IdOportunidad: vm.IdOportunidad
            };

            var promise = EstudiosEspecialesService.CantidadEstudiosEsp(dto);
            promise.then(function (response) {
                vm.ValorResidual = response.data.ValorResidualSoles;
                vm.CapexDolares = response.data.CapexDolares;
                vm.CapexSoles = response.data.CapexSoles;
                vm.TotalCapex = response.data.TotalCapex;
                 if(vm.IdTipo==4){

                        vm.CuSoles = response.data.CostoUnitario
                    }
                ObtieneCostoPreOperativo();

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        };

        function ObtieneCostoPreOperativo() {
            $scope.$$childHead.vm.CostoPreOperativo = vm.CapexDolares;
        };

        /******************************************* LOAD *******************************************/

        CargarEstudioEspecial();

    }

})();