﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('EstudiosEspecialesService', EstudiosEspecialesService);

    EstudiosEspecialesService.$inject = ['$http', 'URLS'];

    function EstudiosEspecialesService($http, $urls) {

        var service = {
            ModalEstudiosEspeciales: ModalEstudiosEspeciales,
            CantidadEstudiosEsp: CantidadEstudiosEsp,
            RegistrarEstudiosEspeciales: RegistrarEstudiosEspeciales
        };

        return service;

        function ModalEstudiosEspeciales(dto) {
            return $http({
                url: $urls.ApiOportunidad + "EstudiosEspeciales/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) { 
                return response;
            }
        };

        function CantidadEstudiosEsp(dto) {
            return $http({
                url: $urls.ApiOportunidad + "EstudiosEspeciales/CantidadEstudiosEsp",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function RegistrarEstudiosEspeciales(dto) {
            return $http({
                url: $urls.ApiOportunidad + "EstudiosEspeciales/RegistrarEstudiosEspeciales",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

          
    }
})();