﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('ConfiguracionAdicionalController', ConfiguracionAdicionalController);

    ConfiguracionAdicionalController.$inject = ['ConfiguracionAdicionalService', 'FlujoCajaService', 'RegistrarOportunidadParametroService',
        'RegistrarOportunidadTipoCambioService','MaestraService',
        'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function ConfiguracionAdicionalController(ConfiguracionAdicionalService, FlujoCajaService, RegistrarOportunidadParametroService,
        RegistrarOportunidadTipoCambioService,MaestraService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        

        /* Modal Registar */
        vm.ModalOportunidadParametro = ModalOportunidadParametro;

        function ModalOportunidadParametro(IdOportunidadParametro) {
            vm.IdOportunidadParametro = IdOportunidadParametro;
            debugger
            var OportunidadParametro = {
                IdOportunidadParametro: IdOportunidadParametro
            };

            var promise = RegistrarOportunidadParametroService.ModalOportunidadParametro(OportunidadParametro);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoOportunidadParametro").html(content);
                });

                $('#ModalRegistrarOportunidadParametro').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        /*--------------------------------------*/
        vm.ModalOportunidadTipoCambio = ModalOportunidadTipoCambio;

        function ModalOportunidadTipoCambio(IdTipoCambioDetalle) {
            vm.IdTipoCambioDetalle = IdTipoCambioDetalle;
            debugger
            var OportunidadTipoCambio = {
                IdTipoCambioDetalle: IdTipoCambioDetalle
            };

            var promise = RegistrarOportunidadTipoCambioService.ModalOportunidadTipoCambio(OportunidadTipoCambio);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoOportunidadTipoCambio").html(content);
                });

                $('#ModalRegistrarOportunidadTipoCambio').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };
        //vm.Oportunidad = $scope.$parent.vm.ObjOportunidad;
        vm.TituloModal = "Editar";
        //vm.IdOportunidadLineaNegocio = vm.Oportunidad.IdOportunidadLineaNegocio;
        //vm.IdOportunidadLineaNegocio = 2;
        //vm.IdSubServicio= $scope.$parent.vm.IdSubServicioactual;
        vm.Oportunidad = $scope.$parent.vm.ObjOportunidad;
        vm.IdOportunidadLineaNegocio = vm.Oportunidad.IdOportunidadLineaNegocio;
        vm.IdSubServicioActual = 0;
        vm.IdFlujoCajaActual = 0;

        vm.IdPeriodosActual = 0;
        vm.BuscarOportunidadTipoCambio = BuscarOportunidadTipoCambio;
        vm.BuscarConfiguracionAdicional = BuscarConfiguracionAdicional;
        vm.CargaModal = CargaModal;
        vm.BuscarParametro = BuscarParametro;
        BuscarParametro();
      

        //IdSubServicio = ss.IdSubServicio,
        //IdFlujoCaja = ofc.IdFlujoCaja,
        //IdFlujoCajaConfiguracion = fcc.IdFlujoCajaConfiguracion,
        //IdPeriodos = ofc.IdPeriodos,
        //SubServicio = ss.Descripcion,
        //IdTipoCosto = ofc.IdTipoCosto,
        //CostoPreOperativo = fcc.CostoPreOperativo

        vm.ListTipoMoneda = [];
        vm.IdMoneda = "-1";

        vm.ListTipoMoneda_2 = [];
        vm.IdMoneda_2 = "-1";
        vm.IdOportunidad = $scope.$parent.$parent.vm.Oportunidad.IdOportunidad;

         ListarTipoMoneda();
         ObtenerFlujoCajaConfiguracionUnico();

        function ListarTipoMoneda() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 98

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListTipoMoneda = UtilsFactory.AgregarItemSelect(Respuesta);
                vm.ListTipoMoneda_2 = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function ObtenerOportunidad() {
            var oportunidad = {
                IdOportunidad: vm.IdOportunidad
            }

            var promise = RegistrarOportunidadService.ObtenerOportunidad(oportunidad);
            promise.then(function (resultado) {


                var Respuesta = resultado.data;
                var TiempoProyecto= Respuesta.TiempoProyecto;



            }, function (response) {


            });
        }


        
        function ObtenerOportunidadTipoCambioPorLineaNegocio() {




            var flujocaja = {

                IdFlujoCajaConfiguracion: vm.IdOportunidadLineaNegocio,
                IdSubServicio: 1
            };


            var promise = ConfiguracionAdicionalService.ObtenerOportunidadTipoCambioPorLineaNegocio(flujocaja);
            promise.then(function (response) {

                var Respuesta = response.data;
                vm.IdMoneda = (Respuesta.IdMoneda == null) ? "-1" : Respuesta.IdMoneda;
                // vm.Ponderacion = (Respuesta.Ponderacion == 0) ? 100 : Respuesta.Ponderacion;
                vm.CostoPreOperativoUnico = Respuesta.CostoPreOperativo;


            }, function (response) {
                blockUI.stop();

            });
 
        };

        function ObtenerFlujoCajaConfiguracionUnico() {


            

            var flujocaja = {
                
                IdFlujoCajaConfiguracion: vm.IdOportunidadLineaNegocio,
                IdSubServicio:1
            };


            var promise = FlujoCajaService.ObtenerSubServicio(flujocaja);
            promise.then(function (response) {

                var Respuesta = response.data;
                vm.IdMoneda = (Respuesta.IdMoneda == null) ? "-1" : Respuesta.IdMoneda;
               // vm.Ponderacion = (Respuesta.Ponderacion == 0) ? 100 : Respuesta.Ponderacion;
                vm.CostoPreOperativoUnico =  Respuesta.CostoPreOperativo;


            }, function (response) {
                blockUI.stop();

            });


            debugger
        
    
                var flujocaja = {
                    IdFlujoCaja: $scope.$parent.vm.IdFlujoCaja,
                    IdFlujoCajaConfiguracion: Id,
               
                };

               
                    var promise = FlujoCajaService.ObtenerOportunidadFlujoCajaConfiguracion(flujocaja);
                    promise.then(function (response) {
                        var Respuesta = response.data;
                        vm.IdMoneda = (Respuesta.IdMoneda == null) ? "-1" : Respuesta.IdMoneda;
                        vm.Ponderacion = (Respuesta.Ponderacion == 0) ? 100 : Respuesta.Ponderacion;
                        vm.CostoPreOperativo = (Respuesta.CostoPreOperativo == 0) ? vm.CostoPreOperativo : Respuesta.CostoPreOperativo;
                        vm.IdInicioProyecto = (Respuesta.Inicio == 0 || Respuesta.Inicio == null) ? vm.negativo : Respuesta.Inicio;
                        debugger
                        if (Respuesta.Meses == null) {

                            ObtenerOportunidad(vm.IdOportunidad);
                        }
                        else {
                            vm.Meses = Respuesta.Meses;
                        }


                    }, function (response) {
                        blockUI.stop();

                    });


               
          

        };
        /******************************************* ConfiguracionAdicional *******************************************/
        vm.dtInstanceConfiguracionAdicional = {};

        vm.dtColumnsConfiguracionAdicional = [

         DTColumnBuilder.newColumn('IdSubServicio').notVisible(),
         DTColumnBuilder.newColumn('IdFlujoCaja').notVisible(),
         DTColumnBuilder.newColumn('IdFlujoCajaConfiguracion').notVisible(),
         DTColumnBuilder.newColumn('IdPeriodos').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('SubServicio').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaConfiguracionAdicional)

        ];

        function AccionesBusquedaConfiguracionAdicional(data, type, full, meta) {
            var respuesta = "<div class='col-xs-3 col-sm-3'> <a title='Editar' class='btn btn-sm' id='tab-button' class='btn ' ng-click='vm.CargaModal(" + data.IdSubServicio + ", " + data.IdFlujoCaja + ", " + data.IdFlujoCajaConfiguracion + ", " + data.IdPeriodos + ")'><span class='fa fa-edit fa-lg'></span></a></div> ";
            //respuesta = respuesta + "<div class='col-xs-3 col-sm-3'><a title='Cancelar' class='btn btn-sm'  id='tab-button' class='btn ' ng-click='vm.EliminarConfiguracionAdicional()' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarConfiguracionAdicional() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsConfiguracionAdicional = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarConfiguracionAdicionalPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarConfiguracionAdicionalPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var dto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdEstado: 1,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = ConfiguracionAdicionalService.ListarConfiguracionAdicional(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };



        //ListaOportunidadParametroPaginado
        vm.dtInstanceParametro = {};

        vm.dtColumnsParametro = [
        
        DTColumnBuilder.newColumn('IdOportunidadParametro').notVisible(),
         DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
         DTColumnBuilder.newColumn('FloatValor').withTitle('Valor').notSortable(),
        DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaParametro)

        ];

        function AccionesBusquedaParametro(data, type, full, meta) {
            var respuesta = "<div class='col-xs-3 col-sm-3'> <a title='Editar' class='btn btn-sm' id='tab-button' class='btn ' ng-click='vm.ModalOportunidadParametro(" + data.IdOportunidadParametro + ")'><span class='fa fa-edit fa-lg'></span></a></div> ";
            //respuesta = respuesta + "<div class='col-xs-3 col-sm-3'><a title='Cancelar' class='btn btn-sm'  id='tab-button' class='btn ' ng-click='vm.EliminarParametro()' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };

        function BuscarParametro() {

            LimpiarGrillaParametro();

            $timeout(function () {
                vm.dtOptionsParametro = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarParametroPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                      .withOption('createdRow', function (row, data, dataIndex) {
                          $compile(angular.element(row).contents())($scope);
                      })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarParametroPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var dto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdEstado: 1,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = ConfiguracionAdicionalService.ListaOportunidadParametroPaginado(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadParametroDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaParametro();
            });
        };

      // 

        LimpiarGrillaTipoCambio();
        BuscarOportunidadTipoCambio();
        vm.dtInstanceTipocambio = {};

      
       vm.dtColumnsTipoCambio = [

            DTColumnBuilder.newColumn('IdTipoCambioDetalle').notVisible(),           
            DTColumnBuilder.newColumn('Tipificacion').withTitle('Tipificacion').notSortable(),
             DTColumnBuilder.newColumn('Moneda').withTitle('Moneda').notSortable(),
             DTColumnBuilder.newColumn('Anio').withTitle('Anio').notSortable(),
             DTColumnBuilder.newColumn('Monto').withTitle('Monto').notSortable(),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaTipoCambio)

            ];
        
       

        function AccionesBusquedaTipoCambio(data, type, full, meta) {
            var respuesta = "<div class='col-xs-3 col-sm-3'> <a title='Editar' class='btn btn-sm' id='tab-button' class='btn ' ng-click='vm.ModalOportunidadTipoCambio(" + data.IdTipoCambioDetalle + ")'><span class='fa fa-edit fa-lg'></span></a></div> ";
          //  respuesta = respuesta + "<div class='col-xs-3 col-sm-3'><a title='Cancelar' class='btn btn-sm'  id='tab-button' class='btn ' ng-click='vm.EliminarTipoCambio(" + data.IdTipoCambioDetalle + ")' >" + "<span class='fa fa-trash-o fa-lg'></span></a></div>";
            return respuesta;
        };


        function BuscarOportunidadTipoCambio() {

            LimpiarGrillaTipoCambio();

            $timeout(function () {
                vm.dtOptionsTipoCambio = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withOption('order', [])
                    .withFnServerData(BuscarOportunidadTipoCambioPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarOportunidadTipoCambioPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var dto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdEstado: 1,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = ConfiguracionAdicionalService.ListaOportunidadTipoCambioPaginado(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListOportunidadTipoCambioDtoResponse
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaTipoCambio();
            });
        };

        function LimpiarGrillaTipoCambio (){ 
            vm.dtOptionsTipoCambio = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };

        /******************************************* Funciones *******************************************/

          function LimpiarGrillaParametro() {
            vm.dtOptionsParametro = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };

        function LimpiarGrilla() {
            vm.dtOptionsConfiguracionAdicional = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', false)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };

        function CargaModal(idSubServicio, idFlujoCaja, idFlujoCajaConfiguracion, idPeriodos) {
            vm.IdSubServicioactual = idSubServicio;
            vm.IdFlujoCajaActual = idFlujoCaja;
            vm.IdFlujoCajaConfiguracionActual = idFlujoCajaConfiguracion;
            vm.IdPeriodosActual = idPeriodos

            var dto = {
                IdSubServicio: vm.IdSubServicioactual
            }            

            var promise = ConfiguracionAdicionalService.ModalEditar(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);

                    $("#ContenidoConfiguracionAdicional").html(content);
                    blockUI.stop();
                });

                $('#ModalConfiguracionAdicional').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };
                
        /******************************************* LOAD *******************************************/
               
        BuscarConfiguracionAdicional();

    }
})();



