﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('ConfiguracionAdicionalService', ConfiguracionAdicionalService);

    ConfiguracionAdicionalService.$inject = ['$http', 'URLS'];

    function ConfiguracionAdicionalService($http, $urls) {

        var service = {
            VistaConfiguracionAdicional: VistaConfiguracionAdicional,
            ModalEditar: ModalEditar,
            Obtener: Obtener,
            ListarConfiguracionAdicional: ListarConfiguracionAdicional,
            ListaOportunidadParametroPaginado :ListaOportunidadParametroPaginado,
            ListaOportunidadTipoCambioPaginado:ListaOportunidadTipoCambioPaginado,
            ObtenerOportunidadTipoCambioDetalle:ObtenerOportunidadTipoCambioDetalle,
            ObtenerOportunidadTipoCambioPorLineaNegocio: ObtenerOportunidadTipoCambioPorLineaNegocio
        };

        return service;
        function ObtenerOportunidadTipoCambioPorLineaNegocio(request) {
            return $http({
                url: $urls.ApiOportunidad + "ConfiguracionAdicional/ObtenerOportunidadTipoCambioPorLineaNegocio",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
        function ObtenerOportunidadTipoCambioDetalle(request) {
            return $http({
                url: $urls.ApiOportunidad + "ConfiguracionAdicional/ObtenerOportunidadTipoCambioDetalle",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ListaOportunidadTipoCambioPaginado(request) {
            return $http({
                url: $urls.ApiOportunidad + "ConfiguracionAdicional/ListaOportunidadTipoCambioPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
        function VistaConfiguracionAdicional() {
            return $http({
                url: $urls.ApiOportunidad + "ConfiguracionAdicional/Index",
                method: "GET"
            }).then(DatosCompletados);
 
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListaOportunidadParametroPaginado(request) {
            return $http({
                url: $urls.ApiOportunidad + "ConfiguracionAdicional/ListaOportunidadParametroPaginado",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ModalEditar(dto) {
            return $http({
                url: $urls.ApiOportunidad + "ConfiguracionAdicional/Editar",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function Obtener(dto) {
            return $http({
                url: $urls.ApiOportunidad + "ConfiguracionAdicional/Obtener",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarConfiguracionAdicional(dto) {
            return $http({
                url: $urls.ApiOportunidad + "ConfiguracionAdicional/ListarSubservicioPaginado",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();