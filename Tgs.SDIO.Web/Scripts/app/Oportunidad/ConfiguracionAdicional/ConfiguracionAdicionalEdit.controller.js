﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('ConfiguracionAdicionalEditController', ConfiguracionAdicionalEditController);

    ConfiguracionAdicionalEditController.$inject = ['SubServicioDatosService', 'MaestraService', 'FlujoCajaService', 'blockUI', 'UtilsFactory', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function ConfiguracionAdicionalEditController(SubServicioDatosService, MaestraService, FlujoCajaService, blockUI, UtilsFactory, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.TituloModal = "Editar";
        vm.IdSubServicio = 0;
        vm.IdOportunidadLineaNegocio = 0;
        vm.Monto = -1;
        vm.IdFlujoCaja = 0;
        vm.IdFlujoCajaConfiguracion = 0;

        vm.ListTipoCosto = [];
        vm.IdTipoCosto = "-1";
        vm.CostoPreOperativo=0;
        vm.GrabarConfiguracionAdicional = GrabarConfiguracionAdicional;

        /******************************************* ConfiguracionAdicional *******************************************/
        
        function GrabarConfiguracionAdicional() {
            var dto = {
                IdFlujoCaja: vm.IdFlujoCaja,
                IdFlujoCajaConfiguracion: vm.IdFlujoCajaConfiguracion
            };

            var fcc;
            var promise = FlujoCajaService.ObtenerOportunidadFlujoCajaConfiguracion(dto);
            promise.then(function (response) {
                blockUI.stop();
                fcc = response.data;
                
                if (fcc.CostoPreOperativo !=0) {
                  
                    var promise = FlujoCajaService.ActualizarOportunidadFlujoCajaConfiguracion(fcc);
                    promise.then(function (response) {
                        var Respuesta = response.data;
                        blockUI.stop();
                        if (Respuesta.TipoRespuesta == 0) {
                            UtilsFactory.Alerta('#lblAlerta_ConfiguracionAdicional', 'success', Respuesta.Mensaje, 10);
                        }else{
                            UtilsFactory.Alerta('#lblAlerta_ConfiguracionAdicional', 'danger', 'Error al grabar', 20);
                        }
                    }, function (response) {
                        blockUI.stop();
                    });
                } else {
                    UtilsFactory.Alerta('#lblAlerta_ConfiguracionAdicional', 'warning', 'Ningun cambio que guardar', 5);
                }
            }, function (response) {
                blockUI.stop();
            });
        }

        /******************************************* Funciones *******************************************/
        function CargarConfiguracionAdicional() {
            vm.IdSubServicio = $scope.$parent.vm.IdSubServicioactual;
            vm.IdFlujoCaja = $scope.$parent.vm.IdFlujoCajaActual;
            vm.IdFlujoCajaConfiguracion = $scope.$parent.vm.IdFlujoCajaConfiguracionActual;
            vm.IdPeriodos = $scope.$parent.vm.IdPeriodosActual;
            vm.IdOportunidadLineaNegocio = $scope.$parent.vm.IdOportunidadLineaNegocio;
            
            var dto = {
                IdSubServicio: vm.IdSubServicio,
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio
            };

            var promise = SubServicioDatosService.ObtenerSubServicioDatos(dto);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;

                vm.SubServicio = respuesta.SubServicio;
                vm.CostoPreOperativo = respuesta.CostoPreOperativo;
                vm.IdTipoCosto = respuesta.IdTipoCosto;
                vm.IdPeriodos = respuesta.IdPeriodos;
                
               var obj=  $scope.$$childHead.vm.ObtenerFlujoCajaConfiguracion();
            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarTipoCosto() {
            var filtroTipoCosto = {
                IdRelacion: 4
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipoCosto);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoCosto = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarCombos() {
            CargarTipoCosto();
        };

        /******************************************* LOAD *******************************************/
        CargarCombos();
        CargarConfiguracionAdicional();

    }
})();



