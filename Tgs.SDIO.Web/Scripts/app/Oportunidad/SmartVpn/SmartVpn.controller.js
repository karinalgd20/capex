﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('SmartVpnController', SmartVpnController);

    SmartVpnController.$inject = ['SmartVpnService','FlujoCajaService','SubServicioDatosCapexService','EcapexService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile'];

    function SmartVpnController(SmartVpnService, FlujoCajaService,SubServicioDatosCapexService,EcapexService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile) {

        var vm = this;
        vm.tabFlujoCaja = true;
        vm.IdGrupo = 16;
        vm.indice = 1;

        vm.IdFlujoCaja = 0;
        vm.IdOportunidadLineaNegocio = 0;
        vm.IdSubServicioDatosCapex = 0;
        vm.IdFlujoCajaConfiguracion = 0; 
        vm.IdPeriodos = 0;
        vm.CargarSmartVpn = CargarSmartVpn;
        vm.GrabarConceptoSmartVPN = GrabarConceptoSmartVPN;
        vm.ObtieneCostoPreOperativo = ObtieneCostoPreOperativo;

        vm.ListarTipo = ListarTipo;
        vm.ListTipo = [];
        vm.IdTipo = "-1";
        /******************************************* Metodos *******************************************/

        function ListarTipo() {

            var pestanaGrupoTipo = {
                IdGrupo: 16
            };
            var promise = EcapexService.ListarPestanaGrupoTipo(pestanaGrupoTipo);
            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.IdTipo = "-1";

                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);
        
            }, function (response) {
                blockUI.stop();
            });
        }
        function CargarSmartVpn() {

            var dto = {
                IdSubServicioDatosCapex: $scope.$parent.vm.dto.IdSubServicioDatosCapex,
                IdGrupo: 16
            };

            var promise = FlujoCajaService.ObtenerDetalleOportunidadEcapex(dto);
            promise.then(function (response) {

                console.log(response.data);
                var result = response.data;
              
                vm.Equipo = result.SubServicio;
                vm.Cruce = result.Cruce;
                vm.AEReducido = result.AEReducido;
                vm.Cantidad = result.Cantidad;
                vm.CapexDolares = result.CapexDolares;
                vm.CapexSoles = result.CapexSoles;
                vm.TotalCapex = result.TotalCapex;
                vm.MesRecupero = result.MesRecupero;
                vm.MesComprometido = result.MesComprometido;
                vm.MesCertificado = result.MesCertificado;
                

                ObtieneCostoPreOperativo();
                vm.IdFlujoCaja = result.IdFlujoCaja;
                vm.IdOportunidadLineaNegocio = result.IdOportunidadLineaNegocio;
                vm.IdSubServicioDatosCapex = result.IdSubServicioDatosCapex;
                vm.IdFlujoCajaConfiguracion = result.IdFlujoCajaConfiguracion;
               
                vm.IdPeriodos = result.IdPeriodos==0?"1":result.IdPeriodos;
               // $scope.$$childHead.vm.Meses = result.TiempoProyecto;
                $scope.$$childHead.vm.ObtenerFlujoCajaConfiguracion();

                 if(result.IdTipoSubServicio> 0){ListarTipoPorId(result.IdTipoSubServicio)}else{ListarTipo();}
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });

        };
          function ListarTipoPorId(int) {

                var pestanaGrupoTipo = {
                    IdGrupo: 16
                };
                var promise = EcapexService.ListarPestanaGrupoTipo(pestanaGrupoTipo);
                promise.then(function (resultado) {
                    blockUI.stop();

                    var Respuesta = resultado.data;

                    vm.IdTipo = int;
                    vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);

                }, function (response) {
                    blockUI.stop();
                });
            }

        function GrabarConceptoSmartVPN() {

            var flujocaja = {
                IdFlujoCaja: vm.IdFlujoCaja,
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                Cantidad: vm.Cantidad,
                CostoUnitario: vm.CapexSoles,
                IdTipoSubServicio: vm.IdTipo 

            };


            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();

                var promise = FlujoCajaService.ActualizarOportunidadFlujoCaja(flujocaja);
                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#lblAlerta_Smart', 'danger', MensajesUI.DatosError, 20);
                    } else {

                        var capex = {
                            IdFlujoCaja: vm.IdFlujoCaja,
                            IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                            Cruce: vm.Cruce,
                            AEReducido: vm.AEReducido,
                            Cantidad: vm.Cantidad,
                            CapexDolares: vm.CapexDolares,
                            CapexSoles: vm.CapexSoles,
                            TotalCapex: vm.TotalCapex,
                            MesRecupero: vm.MesRecupero,
                            MesComprometido: vm.MesComprometido,
                            MesCertificado: vm.MesCertificado
                        };
                        
                        var promise = SubServicioDatosCapexService.ActualizarSubServicioDatosCapex(capex);
                        promise.then(function (response) {

                            var Respuesta = response.data;
                            if (Respuesta.TipoRespuesta != 0) {
                                UtilsFactory.Alerta('#lblAlerta_Smart', 'danger', Respuesta.Mensaje, 20);
                            } else {

                                UtilsFactory.Alerta('#lblAlerta_Smart', 'success', Respuesta.Mensaje, 10);
                                $scope.$$childTail.vm.GrabarFlujoCajaConfiguracion();
                                $scope.$parent.vm.BuscarSmartVPN();
                            }

                        }, function (response) {
                            blockUI.stop();
                            UtilsFactory.Alerta('#lblAlerta_Smart', 'danger', MensajesUI.DatosError, 5);
                        });
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Smart', 'danger', MensajesUI.DatosError, 5);
                });
            };
        };

        /******************************************* LOAD *******************************************/
        CargarSmartVpn();

        vm.CantidadSmartVPNComplete = CantidadSmartVPNComplete;

        function CantidadSmartVPNComplete() {
            var dto = {
                IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                IdGrupo: vm.IdGrupo,
                IdEstado: 1,
                Cu: vm.CuSoles,
                Cantidad: vm.Cantidad,
                CuAntiguiedad: vm.CuAntiguiedad,
                MesesAntiguedad: vm.Antiguedad,
                Indice: vm.indice,
                IdTipoSubServicio: vm.IdTipo
            };

            var promise = SmartVpnService.CantidadSmartVpn(dto);
            promise.then(function (response) {
                vm.CapexDolares = response.data.CapexDolares;
                vm.CapexSoles = response.data.CapexSoles;
                vm.TotalCapex = response.data.TotalCapex;
                ObtieneCostoPreOperativo();
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        };

        function ObtieneCostoPreOperativo() {
            $scope.$$childHead.vm.CostoPreOperativo = vm.CapexDolares;
        };

    }

})();