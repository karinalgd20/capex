﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('SmartVpnService', SmartVpnService);

    SmartVpnService.$inject = ['$http', 'URLS'];

    function SmartVpnService($http, $urls) {

        var service = {
            ModalSmart: ModalSmart,
            CantidadSmartVpn: CantidadSmartVpn
        };

        return service;

        function ModalSmart(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SmartVpn/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
 
        function CantidadSmartVpn(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SmartVpn/CantidadSmartVpn",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

    }
})();