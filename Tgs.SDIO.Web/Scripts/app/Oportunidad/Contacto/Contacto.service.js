﻿(function () {
    'use strict',
    angular
        .module('app.Oportunidad')
        .service('ContactoService', ContactoService);

    ContactoService.$inject = ['$http', 'URLS'];

    function ContactoService($http, $urls) {
        var service = {
            RegistrarContacto: RegistrarContacto,
            ActualizarContacto: ActualizarContacto,
            InactivarContacto: InactivarContacto,
            ListarContactoPaginado: ListarContactoPaginado,
            ObtenerContactoPorId: ObtenerContactoPorId
        };

        return service;

        function RegistrarContacto(contacto) {
            return $http({
                url: $urls.ApiOportunidad + "Contacto/RegistrarContacto",
                method: "POST",
                data: JSON.stringify(contacto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarContacto(contacto) {
            return $http({
                url: $urls.ApiOportunidad + "Contacto/ActualizarContacto",
                method: "POST",
                data: JSON.stringify(contacto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function InactivarContacto(contacto) {
            return $http({
                url: $urls.ApiOportunidad + "Contacto/InactivarContacto",
                method: "POST",
                data: JSON.stringify(contacto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarContactoPaginado(contacto) {
            return $http({
                url: $urls.ApiOportunidad + "Contacto/ListarContactoPaginado",
                method: "POST",
                data: JSON.stringify(contacto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerContactoPorId(contacto) {
            return $http({
                url: $urls.ApiOportunidad + "Contacto/ObtenerContactoPorId",
                method: "POST",
                data: JSON.stringify(contacto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();