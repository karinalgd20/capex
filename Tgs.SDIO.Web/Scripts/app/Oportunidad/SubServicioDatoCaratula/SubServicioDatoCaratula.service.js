﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('SubServicioDatoCaratulaService', SubServicioDatoCaratulaService);

    SubServicioDatoCaratulaService.$inject = ['$http', 'URLS'];

    function SubServicioDatoCaratulaService($http, $urls) {

        var service = {
            RegistrarSubServicioDatoCaratula: RegistrarSubServicioDatoCaratula,
            ActualizarSubServicioDatoCaratula: ActualizarSubServicioDatoCaratula,
            ListarSubServicioDatoCaratula: ListarSubServicioDatoCaratula
        };

        return service;

        function RegistrarSubServicioDatoCaratula(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatoCaratula/RegistrarSubServicioDatoCaratula",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarSubServicioDatoCaratula(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatoCaratula/ActualizarSubServicioDatoCaratula",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) { 
                return resultado;
            }
        };

        function ListarSubServicioDatoCaratula(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatoCaratula/ListarSubServicioDatoCaratula",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();
