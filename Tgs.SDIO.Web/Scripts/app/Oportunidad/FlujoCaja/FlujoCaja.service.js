﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('FlujoCajaService', FlujoCajaService);

    FlujoCajaService.$inject = ['$http', 'URLS'];

    function FlujoCajaService($http, $urls) {

        var service = {
            ModalFlujoCaja: ModalFlujoCaja,
            ListarAnoMesProyecto: ListarAnoMesProyecto,
            RegistrarOportunidadFlujoCaja: RegistrarOportunidadFlujoCaja,
            ActualizarOportunidadFlujoCaja: ActualizarOportunidadFlujoCaja,
            ObtenerOportunidadFlujoCaja: ObtenerOportunidadFlujoCaja,
            RegistrarOportunidadFlujoCajaConfiguracion: RegistrarOportunidadFlujoCajaConfiguracion,
            ActualizarOportunidadFlujoCajaConfiguracion: ActualizarOportunidadFlujoCajaConfiguracion,
            ObtenerOportunidadFlujoCajaConfiguracion: ObtenerOportunidadFlujoCajaConfiguracion,
            GeneraCasoNegocio: GeneraCasoNegocio,
            GeneraServicio: GeneraServicio,
            EliminarCasoNegocio: EliminarCasoNegocio,
            ListarDetalleOportunidadCaratula: ListarDetalleOportunidadCaratula, 
            ListarDetalleOportunidadEcapex: ListarDetalleOportunidadEcapex,
            ObtenerDetalleOportunidadEcapex: ObtenerDetalleOportunidadEcapex,
            ListaOportunidadFlujoCajaConfiguracion: ListaOportunidadFlujoCajaConfiguracion,
            ActualizarPeriodoOportunidadFlujoCaja: ActualizarPeriodoOportunidadFlujoCaja,
            ActualizarOportunidadCosto: ActualizarOportunidadCosto,
            GenerarFlujoCajaDetalle: GenerarFlujoCajaDetalle,
            RegistrarOportunidadCosto: RegistrarOportunidadCosto,
            ObtenerDetalleOportunidadCaratula: ObtenerDetalleOportunidadCaratula,
            ActualizarSubServicioCaratula: ActualizarSubServicioCaratula,
            InhabilitarOportunidadFlujoCaja: InhabilitarOportunidadFlujoCaja,
            InhabilitarOportunidadEcapex:InhabilitarOportunidadEcapex,
            ListaServicioPorOportunidadLineaNegocio :ListaServicioPorOportunidadLineaNegocio,
            RegistrarComponente:RegistrarComponente,
            RegistrarServicioComponente: RegistrarServicioComponente,
            RegistrarCasoNegocioServicio:RegistrarCasoNegocioServicio,
            ListarPorIdFlujoCaja:ListarPorIdFlujoCaja
        };

        return service;
         function ListarPorIdFlujoCaja(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ListarPorIdFlujoCaja",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
         function RegistrarComponente(dto) {
             return $http({
                 url: $urls.ApiOportunidad + "FlujoCaja/RegistrarComponente",
                 method: "POST",
                 data: JSON.stringify(dto)
             }).then(DatosCompletados);
             function DatosCompletados(resultado) {
                 return resultado;
             }
         };

        function RegistrarServicioComponente(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/RegistrarServicioComponente",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        

        function RegistrarCasoNegocioServicio(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/RegistrarCasoNegocioServicio",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListaServicioPorOportunidadLineaNegocio(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ListaServicioPorOportunidadLineaNegocio",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function InhabilitarOportunidadEcapex(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/InhabilitarOportunidadEcapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function InhabilitarOportunidadFlujoCaja(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/InhabilitarOportunidadFlujoCaja",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ModalFlujoCaja() {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/Index",
                method: "POST"
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ListarAnoMesProyecto(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ListarAnoMesProyecto",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarOportunidadFlujoCaja(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/RegistrarOportunidadFlujoCaja",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarOportunidadFlujoCaja(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ActualizarOportunidadFlujoCaja",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerOportunidadFlujoCaja(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ObtenerOportunidadFlujoCaja",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };


        function RegistrarOportunidadFlujoCajaConfiguracion(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/RegistrarOportunidadFlujoCajaConfiguracion",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarOportunidadFlujoCajaConfiguracion(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ActualizarOportunidadFlujoCajaConfiguracion",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerOportunidadFlujoCajaConfiguracion(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ObtenerOportunidadFlujoCajaConfiguracion",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function GeneraCasoNegocio(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/GeneraCasoNegocio",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function GeneraServicio(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/GeneraServicio",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function EliminarCasoNegocio(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/EliminarCasoNegocio",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarDetalleOportunidadCaratula(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ListarDetalleOportunidadCaratula",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarDetalleOportunidadEcapex(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ListarDetalleOportunidadEcapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerDetalleOportunidadEcapex(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ObtenerDetalleOportunidadEcapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaOportunidadFlujoCajaConfiguracion(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ListaOportunidadFlujoCajaConfiguracion",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarPeriodoOportunidadFlujoCaja(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ActualizarPeriodoOportunidadFlujoCaja",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarOportunidadCosto(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ActualizarOportunidadCosto",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function GenerarFlujoCajaDetalle(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/GenerarFlujoCajaDetalle",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarOportunidadCosto(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/RegistrarOportunidadCosto",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    
        function ObtenerDetalleOportunidadCaratula(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ObtenerDetalleOportunidadCaratula",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarSubServicioCaratula(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ActualizarSubServicioCaratula",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();