﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('SubServicioDatosService', SubServicioDatosService);

    SubServicioDatosService.$inject = ['$http', 'URLS'];

    function SubServicioDatosService($http, $urls) {

        var service = {
            ListarConfiguracionAdicional: ListarConfiguracionAdicional,
            ObtenerSubServicioDatos: ObtenerSubServicioDatos,
            ActualizarSubServicioDatos: ActualizarSubServicioDatos
        };

        return service;
        
        function ObtenerSubServicioDatos(dto) {
            return $http({
                url: $urls.ApiOportunidad + "ConfiguracionAdicional/Obtener",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        }; 

        function ActualizarSubServicioDatos(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatos/ActualizarSubServicioDatos",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarConfiguracionAdicional(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatos/ListarConfiguracionAdicional",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();