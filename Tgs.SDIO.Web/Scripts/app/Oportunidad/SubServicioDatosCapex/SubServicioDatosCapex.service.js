﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('SubServicioDatosCapexService', SubServicioDatosCapexService);

    SubServicioDatosCapexService.$inject = ['$http', 'URLS'];

    function SubServicioDatosCapexService($http, $urls) {

        var service = {
            RegistrarSubServicioDatosCapex: RegistrarSubServicioDatosCapex,
            ActualizarSubServicioDatosCapex: ActualizarSubServicioDatosCapex,
            ListarSubServicioDatosCapex: ListarSubServicioDatosCapex,
            ObtenerSubServicioDatosCapex: ObtenerSubServicioDatosCapex
        };

        return service;

        function RegistrarSubServicioDatosCapex(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatosCapex/RegistrarSubServicioDatosCapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) { 
                return resultado;
            }
        };

        function ActualizarSubServicioDatosCapex(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatosCapex/ActualizarSubServicioDatosCapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarSubServicioDatosCapex(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatosCapex/ListarSubServicioDatosCapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerSubServicioDatosCapex(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatosCapex/ObtenerSubServicioDatosCapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();