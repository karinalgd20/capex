﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('RiesgosEditController', RiesgosEditController);

    RiesgosEditController.$inject = ['RiesgosService', 'MaestraService', 'FlujoCajaService', 'blockUI', 'UtilsFactory', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RiesgosEditController(RiesgosService, MaestraService, FlujoCajaService, blockUI, UtilsFactory, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.TituloModal = "Editar";
        vm.NivelRiesgo = "Ok";
        vm.ClassNivelRiesgo = "RiesgoClass1";
        vm.IdSubServicio = 0;
        vm.IdOportunidadLineaNegocio = 0;
        vm.Monto = -1;
        vm.IdFlujoCaja = 0;
        vm.IdFlujoCajaConfiguracion = 0;

        vm.ListProbabilidad = [];
        vm.IdProbabilidad = "-1";

        vm.ListImpacto = [];
        vm.IdImpacto = "-1";

        vm.GrabarRiesgo = GrabarRiesgo;
        vm.CalculaRiesgo = CalculaRiesgo;
        vm.Test = Test;

        /******************************************* Riesgos *******************************************/
        
        function GrabarRiesgo() {            

            var riesgo = {
                IdRiesgo: "0",
                Orden: "0",
                Fecha: vm.FechaDeteccion,
                Riesgo: vm.Descripcion,
                Consecencias: vm.Consecuencias,
                PlanAccion: vm.PlanAccion,
                Probabilidad: vm.IdProbabilidad,
                Impacto: vm.IdImpacto,
                Responsable: vm.Responsable,
                Estatus: vm.Status,
                FechaCierre: vm.FechaCierre,
                Continuo: vm.Continuo
            };

            $scope.$parent.vm.DataSource.push(riesgo);
            $scope.$parent.vm.BuscarRiesgos();
        }

        /******************************************* Funciones *******************************************/
        function CalculaRiesgo() {
            if (vm.IdProbabilidad == '-1') {
                vm.NivelRiesgo = "-";
                vm.ClassNivelRiesgo = "";
                return;
            }

            if (vm.IdImpacto == '-1') {
                vm.NivelRiesgo = "-";
                vm.ClassNivelRiesgo = "";
                return;
            }

            var impactoCalculado = parseInt(vm.IdProbabilidad) * parseInt(vm.IdImpacto);

            if (impactoCalculado > 15) {
                vm.NivelRiesgo = "Alto";
                vm.ClassNivelRiesgo = "RiesgoClass3";
            } else if (impactoCalculado > 5) {
                vm.NivelRiesgo = "Regular";
                vm.ClassNivelRiesgo = "RiesgoClass2";
            } else {
                vm.NivelRiesgo = "Bajo";
                vm.ClassNivelRiesgo = "RiesgoClass1";
            }

            //alert("ok");
        };

        function CargarRiesgos() {
            vm.IdSubServicio = $scope.$parent.vm.IdSubServicioactual;
            vm.IdFlujoCaja = $scope.$parent.vm.IdFlujoCajaActual;
            vm.IdFlujoCajaConfiguracion = $scope.$parent.vm.IdFlujoCajaConfiguracionActual;
            vm.IdPeriodos = $scope.$parent.vm.IdPeriodosActual;
            vm.IdOportunidadLineaNegocio = $scope.$parent.vm.IdOportunidadLineaNegocio;
            
            var dto = {
                IdSubServicio: vm.IdSubServicio,
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio
            };

            var promise = RiesgosService.Obtener(dto);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;

                vm.SubServicio = respuesta.SubServicio;
                vm.Monto = respuesta.CostoPreOperativo;
                vm.IdTipoCosto = respuesta.IdTipoCosto;
                vm.IdPeriodos = respuesta.IdPeriodos;
                
                $scope.$$childHead.vm.ObtenerFlujoCajaConfiguracion();
            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarProbabilidad() {
            //var filtroTipoCosto = {
            //    IdRelacion: 4
            //};
            //var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipoCosto);
            //promise.then(function (resultado) {
            //    var Respuesta = resultado.data;
            //    vm.ListTipoCosto = UtilsFactory.AgregarItemSelect(Respuesta);
            //}, function (response) {
            //    blockUI.stop();
            //});
            var data = [
                { Codigo: "0", Descripcion: "Cerrado" },
                { Codigo: "1", Descripcion: "Muy Bajo" },
                { Codigo: "2", Descripcion: "Bajo" },
                { Codigo: "3", Descripcion: "Medio" },
                { Codigo: "4", Descripcion: "Alto" },
                { Codigo: "5", Descripcion: "Muy Alto" }
            ];
            vm.ListProbabilidad = UtilsFactory.AgregarItemSelect(data);
        };

        function CargarImpacto() {
            //var filtroTipoCosto = {
            //    IdRelacion: 4
            //};
            //var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipoCosto);
            //promise.then(function (resultado) {
            //    var Respuesta = resultado.data;
            //    vm.ListTipoCosto = UtilsFactory.AgregarItemSelect(Respuesta);
            //}, function (response) {
            //    blockUI.stop();
            //});
            var data = [
                { Codigo: "0", Descripcion: "Cerrado" },
                { Codigo: "1", Descripcion: "Muy Bajo" },
                { Codigo: "2", Descripcion: "Bajo" },
                { Codigo: "3", Descripcion: "Medio" },
                { Codigo: "4", Descripcion: "Alto" },
                { Codigo: "5", Descripcion: "Muy Alto" }
            ];
            vm.ListImpacto = UtilsFactory.AgregarItemSelect(data);
        };

        function CargarCombos() {
            CargarProbabilidad();
            CargarImpacto();
        };

        function Test() {
            var op = 0;
        };
        

        /******************************************* LOAD *******************************************/
        CargarCombos();
        //CargarRiesgos();

    }
})();



