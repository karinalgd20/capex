﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('RiesgosController', RiesgosController);

    RiesgosController.$inject = ['RiesgosService', 'FlujoCajaService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RiesgosController(RiesgosService, FlujoCajaService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        //vm.Oportunidad = $scope.$parent.vm.ObjOportunidad;
        vm.TituloModal = "Editar";
        vm.RiesgoGlobal = "Ok";
        vm.ClassRiesgoGlobal = "RiesgoClass1";
        vm.DataSource = [
            { IdRiesgo: "0", Orden: "0", Fecha: "2018/08/18", Riesgo: "xxx", Consecencias: "No muchas", Probabilidad: "1", Impacto: "3" },
            { IdRiesgo: "0", Orden: "1", Fecha: "2018/08/18", Riesgo: "xxx2", Consecencias: "No muchas2", Probabilidad: "3", Impacto: "4" }
        ];
        var contador = vm.DataSource.length;
        
        //vm.IdOportunidadLineaNegocio = vm.Oportunidad.IdOportunidadLineaNegocio;
        //vm.IdOportunidadLineaNegocio = 2;
        //vm.Oportunidad = $scope.$parent.vm.ObjOportunidad;        

        vm.BuscarRiesgos = BuscarRiesgos;

        vm.CargaModal = CargaModal;
        vm.AgregarRiesgo = AgregarRiesgo;
        vm.EliminarRiesgo = EliminarRiesgo;

        /******************************************* Riesgos *******************************************/
        vm.dataMasiva = [];
        vm.dtInstanceRiesgos = {};

        vm.dtColumnsRiesgos = [

         DTColumnBuilder.newColumn('IdRiesgo').notVisible(),
         DTColumnBuilder.newColumn('Orden').notVisible(),
         DTColumnBuilder.newColumn('Fecha').withTitle('Fecha').notSortable(),
         DTColumnBuilder.newColumn('Riesgo').withTitle('Probabilidad').notSortable(),
         DTColumnBuilder.newColumn('Consecencias').withTitle('Probabilidad').notSortable(),
         DTColumnBuilder.newColumn('Probabilidad').withTitle('Probabilidad').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Nivel de riesgo').notSortable().renderWith(NivelDeRiesgo),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaRiesgos)

        ];

        function NivelDeRiesgo(data, type, full, meta) {
            var riesgoClass = "RiesgoClass1";
            var riesgoText = "ok";

            var impactoCalculado = parseInt(data.Probabilidad) * parseInt(data.Impacto);

            if (impactoCalculado > 15) {
                riesgoText = "Alto";
                riesgoClass = "RiesgoClass3";
            } else if (impactoCalculado > 5) {
                riesgoText = "Regular";
                riesgoClass = "RiesgoClass2";
            } else {
                riesgoText = "Bajo";
                riesgoClass = "RiesgoClass1";
            }

            var respuesta = "<a class='btn btn-primary btn-primary-tgestiona cabfiltrosBTN " + riesgoClass + "'>" + riesgoText + "</a>";
            return respuesta;
        };

        function AccionesBusquedaRiesgos(data, type, full, meta) {
            var respuesta = "<div class='col-xs-3 col-sm-3'> <a title='Editar' class='btn btn-sm' id='tab-button' class='btn ' ng-click='vm.CargaModal(" + data.Orden + ")'><span class='fa fa-edit'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-3 col-sm-3'><a title='Cancelar' class='btn btn-sm'  id='tab-button' class='btn ' ng-click='vm.EliminarRiesgo(" + data.Orden + ")' >" + "<span class='fa fa-trash-o'></span></a></div>";
            return respuesta;
        };

        function BuscarRiesgos() {
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsRiesgos = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarRiesgosPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarRiesgosPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var dto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdEstado: 1,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = RiesgosService.ListarRiesgos(dto);
            promise.then(function (response) {
                //vm.DataSource = response.data;

                var records = {
                    'draw': draw,
                    'recordsTotal': vm.DataSource.length,
                    'recordsFiltered': vm.DataSource.length,
                    'data': vm.DataSource
                };

                blockUI.stop();
                fnCallback(records);
                CalculaRiesgoGlobal();
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };


        /******************************************* Funciones *******************************************/

        function LimpiarGrilla() {
            vm.dtOptionsRiesgos = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', true)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };
        
        function AgregarRiesgo() {

            var nuevoRiesgo = { IdRiesgo: "0", Orden: contador, Fecha: "2018/08/18", Riesgo: "xxx", Consecencias: "No muchas", Probabilidad: "50" };
            contador++;

            vm.DataSource.push(nuevoRiesgo);
            vm.dtInstanceRiesgos.rerender();
        };

        function EliminarRiesgo(orden) {
            vm.DataSource.splice(orden, 1);
            vm.DataSource.forEach(function (element) {
                if (element.Orden >= orden) {
                    element.Orden = element.Orden - 1;
                }
            });
            contador = vm.DataSource.length;
            vm.dtInstanceRiesgos.rerender();
        };

        function CalculaRiesgoGlobal() {
            var contador = vm.DataSource.length;
            var riesgoTotal = 0;

            vm.DataSource.forEach(function (element) {
                riesgoTotal += parseInt(element.Probabilidad) * parseInt(element.Impacto);
            });
            
            var riesgoClass = "";
            var riesgoText = "";

            var impactoCalculado = riesgoTotal * 1.0 / contador;

            if (impactoCalculado > 15) {
                riesgoText = "Alto";
                riesgoClass = "RiesgoClass3";
            } else if (impactoCalculado > 5) {
                riesgoText = "Regular";
                riesgoClass = "RiesgoClass2";
            } else {
                riesgoText = "Bajo";
                riesgoClass = "RiesgoClass1";
            }

            vm.RiesgoGlobal = riesgoText;
            vm.ClassRiesgoGlobal = riesgoClass;
        };
        
        function CargaModal(orden) {
            //vm.IdSubServicioactual = idSubServicio;
            //vm.IdFlujoCajaActual = idFlujoCaja;
            //vm.IdFlujoCajaConfiguracionActual = idFlujoCajaConfiguracion;
            //vm.IdPeriodosActual = idPeriodos

            var dto = {
                IdRiesgo: 0
            }

            var promise = RiesgosService.ModalEditar(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);

                    $("#ContenidoRiesgos").html(content);
                    blockUI.stop();
                });

                $('#ModalRiesgos').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        /******************************************* LOAD *******************************************/
        BuscarRiesgos();
    }
})();



