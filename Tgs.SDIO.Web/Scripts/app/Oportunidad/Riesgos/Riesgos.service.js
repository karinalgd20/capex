﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('RiesgosService', RiesgosService);

    RiesgosService.$inject = ['$http', 'URLS'];

    function RiesgosService($http, $urls) {

        var service = {
            VistaRiesgos: VistaRiesgos,
            ModalEditar: ModalEditar,
            Obtener: Obtener,
            ListarRiesgos: ListarRiesgos
        };

        return service;

        function VistaRiesgos() {
            return $http({
                url: $urls.ApiOportunidad + "Riesgos/Index",
                method: "GET"
            }).then(DatosCompletados);
 
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalEditar(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Riesgos/Editar",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function Obtener(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Riesgos/Obtener",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarRiesgos(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Riesgos/ListarSubservicioPaginado",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();