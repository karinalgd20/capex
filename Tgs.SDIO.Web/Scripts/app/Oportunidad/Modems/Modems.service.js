﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('ModemsService', ModemsService);

    ModemsService.$inject = ['$http', 'URLS'];

    function ModemsService($http, $urls) {

        var service = {
            ModalModems: ModalModems,
            CantidadModems: CantidadModems
        };

        return service;

        function ModalModems(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Modems/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) { 
                return response;
            }
        };

        function CantidadModems(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Modems/CantidadModems",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

    }
})();