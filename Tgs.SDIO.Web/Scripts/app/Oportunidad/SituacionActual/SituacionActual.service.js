﻿(function () {
    'use strict',
     angular
        .module('app.Oportunidad')
        .service('SituacionActualService', SituacionActualService);

    SituacionActualService.$inject = ['$http', 'URLS'];

    function SituacionActualService($http, $urls) {
        var service =
        {
            RegistrarSituacionActual: RegistrarSituacionActual,
            ObtenerSituacionActualPorIdOportunidad: ObtenerSituacionActualPorIdOportunidad
        };

        return service;

        function RegistrarSituacionActual(request) {
            return $http({
                url: $urls.ApiOportunidad + "SituacionActual/RegistrarSituacionActual",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerSituacionActualPorIdOportunidad(request) {
            return $http({
                url: $urls.ApiOportunidad + "SituacionActual/ObtenerSituacionActualPorIdOportunidad",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();