﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('SatelitalesController', SatelitalesController);

    SatelitalesController.$inject = ['SatelitalesService', 'FlujoCajaService', 'SubServicioDatosCapexService', 'EcapexService', 'MedioService', 'MaestraService', 'CostoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function SatelitalesController(SatelitalesService, FlujoCajaService, SubServicioDatosCapexService, EcapexService, MedioService, MaestraService, CostoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.tabFlujoCaja = true;
        vm.IdGrupo = 17;
        vm.Indice = 1;

        vm.CantidadSatelitalesComplete = CantidadSatelitalesComplete;

        vm.IdFlujoCaja = 0;
        vm.IdOportunidadLineaNegocio = 0;
        vm.IdSubServicioDatosCapex = 0;
        vm.IdFlujoCajaConfiguracion = 0;
        vm.IdPeriodos = 0;
        vm.CargarSatelital = CargarSatelital;
        vm.GrabarConceptoSatelital = GrabarConceptoSatelital;
        vm.ObtieneCostoPreOperativo = ObtieneCostoPreOperativo;

        vm.ListBW = [];
        vm.IdOportunidadCosto = "";
        vm.OportunidadCosto = 0;
        vm.CargarBW = CargarBW;
        vm.negativo = "-1";
        vm.IdPeriodos = 0;

        vm.ListMedio = [];
        // vm.IdMedio ="5" ;
        vm.IdMedio = "-1";
        vm.ListTipoSatelital = [];
        vm.IdTipoSatelital = "-1";

        vm.ListaModelos = null;
        vm.Inversion = "";
        vm.Instalacion = "";
        vm.PorcentajeGarantizado = "";
        vm.CostoSegmentoSatelital = "";


        vm.ListGarantizado = [];
        vm.IdGarantizado = "-1";
        vm.IdMedioOtros = 0;

        vm.ListarTipo = ListarTipo;
        vm.ListTipo = [];
        vm.IdTipo = "-1";
        vm.MontoBW = 0;
        /******************************************* Metodos *******************************************/

        function ListarTipo() {

            var pestanaGrupoTipo = {
                IdGrupo: 17
            };
            var promise = EcapexService.ListarPestanaGrupoTipo(pestanaGrupoTipo);
            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                vm.IdTipo = "-1";
                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }
        function CargarSatelital() {

            var dto = {
                IdSubServicioDatosCapex: $scope.$parent.vm.dto.IdSubServicioDatosCapex,
                IdGrupo: 17
            };

            var promise = FlujoCajaService.ObtenerDetalleOportunidadEcapex(dto);
            promise.then(function (response) {

                var result = response.data;

                vm.IdOportunidadCosto = "";
                vm.IdTipoSatelital = 0;



                vm.Servicio = result.Servicio;
                vm.Cruce = result.Cruce;
                vm.AEReducido = result.AEReducido;
                vm.Circuito = result.Circuito;
                vm.Combo = result.Combo;
                // vm.IdMedio = (result.Medio == 0) ? vm.negativo : result.Medio;
                //   vm.IdMedio = 5;

                vm.IdMedio = (result.IdMedio == 0) ? vm.negativo : result.IdMedio;
                CargarBW();

                vm.IdTipoSatelital = (result.IdTipo == 0) ? vm.negativo : result.IdTipo;
                vm.Garantizado = result.Garantizado;
                vm.Cantidad = result.Cantidad;
                vm.MesesAntiguedad = result.MesesAntiguedad;
                vm.CapexDolares = result.CapexDolares;
                vm.CapexSoles = result.CapexSoles;
                vm.CapexInstalacion = result.Instalacion;
                vm.TotalCapex = result.TotalCapex;
                vm.MesRecupero = result.MesRecupero;
                vm.MesComprometido = result.MesComprometido;
                vm.MesCertificado = result.MesCertificado;



                vm.IdOportunidadCosto = (result.IdCosto == 0 || result.IdCosto == null) ? "" : result.IdCosto;
                vm.IdFlujoCaja = result.IdFlujoCaja;
                vm.IdOportunidadLineaNegocio = result.IdOportunidadLineaNegocio;
                vm.IdSubServicioDatosCapex = result.IdSubServicioDatosCapex;
                vm.IdFlujoCajaConfiguracion = result.IdFlujoCajaConfiguracion;

                vm.IdPeriodos = result.IdPeriodos == 0 ? "2" : result.IdPeriodos;
                vm.IdMedioOtros = result.IdMedio;


                // $scope.$$childTail.vm.Meses = result.TiempoProyecto;
                $scope.$$childTail.vm.ObtenerFlujoCajaConfiguracion();
                CargaOtros();


                if (vm.IdOportunidadCosto > 0) {

                    ObtieneCostoPreOperativoPorId();
                }




                if (result.IdTipoSubServicio > 0) { ListarTipoPorId(result.IdTipoSubServicio) } else { ListarTipo(); }
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });

        };
        function ListarTipoPorId(int) {

            var pestanaGrupoTipo = {
                IdGrupo: 17
            };
            var promise = EcapexService.ListarPestanaGrupoTipo(pestanaGrupoTipo);
            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                vm.IdTipo = int;
                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function GrabarConceptoSatelital() {


            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();
                var costo = {
                    IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                    IdCosto: vm.IdOportunidadCosto,
                    Monto: vm.MontoBW 
                };
                var IdOportunidadCosto = 0;

                var promise = FlujoCajaService.RegistrarOportunidadCosto(costo);
                promise.then(function (response) {
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#lblAlerta_Circuito', 'danger', MensajesUI.DatosError, 20);

                    } else {
                        IdOportunidadCosto = Respuesta.Id;

                        var flujocaja = {
                            IdFlujoCaja: vm.IdFlujoCaja,
                            IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                            Cantidad: vm.Cantidad,
                            CostoUnitario: 0,
                            IdOportunidadCosto: IdOportunidadCosto,
                            IdTipoSubServicio: vm.IdTipo
                        };


                        var promise = FlujoCajaService.ActualizarOportunidadFlujoCaja(flujocaja);
                        promise.then(function (response) {
                            blockUI.stop();
                            var Respuesta = response.data;
                            if (Respuesta.TipoRespuesta != 0) {
                                UtilsFactory.Alerta('#lblAlerta_SatelitalesModal', 'danger', MensajesUI.DatosError, 20);
                            } else {

                                var capex = {
                                    IdFlujoCaja: vm.IdFlujoCaja,
                                    IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                                    Cruce: vm.Cruce,
                                    AEReducido: vm.AEReducido,
                                    Circuito: vm.Circuito,
                                    Combo: vm.Combo,
                                    IdMedio: vm.IdMedio,
                                    IdTipo: vm.IdTipoSatelital,
                                    Garantizado: vm.Garantizado,
                                    Cantidad: vm.Cantidad,
                                    MesesAntiguedad: vm.MesesAntiguedad,
                                    CapexDolares: vm.CapexDolares,
                                    CapexSoles: vm.CapexSoles,
                                    CapexInstalacion: vm.CapexInstalacion,
                                    TotalCapex: vm.TotalCapex,
                                    MesRecupero: vm.MesRecupero,
                                    MesComprometido: vm.MesComprometido,
                                    MesCertificado: vm.MesCertificado
                                };

                                var promise = SubServicioDatosCapexService.ActualizarSubServicioDatosCapex(capex);
                                promise.then(function (response) {

                                    var Respuesta = response.data;
                                    if (Respuesta.TipoRespuesta != 0) {
                                        UtilsFactory.Alerta('#lblAlerta_SatelitalesModal', 'danger', Respuesta.Mensaje, 20);
                                    } else {

                                        UtilsFactory.Alerta('#lblAlerta_SatelitalesModal', 'success', Respuesta.Mensaje, 10);
                                        $scope.$$childTail.vm.GrabarFlujoCajaConfiguracion();
                                        $scope.$parent.vm.BuscarSatelitales();
                                    }

                                }, function (response) {
                                    blockUI.stop();
                                    UtilsFactory.Alerta('#lblAlerta_SatelitalesModal', 'danger', MensajesUI.DatosError, 5);
                                });
                            }
                        }, function (response) {
                            blockUI.stop();
                            UtilsFactory.Alerta('#lblAlerta_SatelitalesModal', 'danger', MensajesUI.DatosError, 5);
                        });
                    }

                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Circuito', 'danger', MensajesUI.DatosError, 5);
                });

            };
        };


        function CantidadSatelitalesComplete() {

            if (vm.Cantidad > 0 && vm.IdOportunidadCosto > 0) {
                var dto = {
                    IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                    IdGrupo: vm.IdGrupo,
                    IdEstado: 1,
                    Cu: vm.CuSoles,
                    Cantidad: vm.Cantidad,
                    CuAntiguiedad: vm.CuAntiguiedad,
                    MesesAntiguedad: vm.Antiguedad,
                    Indice: vm.Indice,
                    Inversion: vm.Inversion,
                    IdTipoSatelital: vm.IdTipoSatelital,
                    Instalacion: vm.Instalacion,
                    PorcentajeGarantizado: vm.PorcentajeGarantizado,
                    CostoSegmentoSatelital: vm.CostoSegmentoSatelital,
                    MesesAntiguedad: vm.MesesAntiguedad,
                    IdGarantizado: vm.IdGarantizado,
                    IdTipoSubServicio: vm.IdTipo
                };

                var promise = SatelitalesService.CantidadSatelitales(dto);
                promise.then(function (response) {

                    vm.valorResidualSoles = response.data.ValorResidualSoles;
                    vm.CapexDolares = response.data.CapexDolares;
                    vm.CapexSoles = response.data.CapexSoles;
                    vm.TotalCapex = response.data.TotalCapex;
                    vm.CapexInstalacion = response.data.CapexInstalacion;
                    ObtieneCostoPreOperativo();
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                });
            }
        };

        function ListarMedio() {
            var medio = {
                IdEstado: 1
            };
            var promise = MedioService.ListarMedio(medio);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListMedio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarTipoEnlace() {
            var filtroTipoEnlace = {
                IdRelacion: 89
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipoEnlace);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoSatelital = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarBW() {
            var medio = {
                IdMedio: vm.IdMedio,
                IdTipoCosto: 8
            };
            var promise = CostoService.ListarCostos(medio);
            promise.then(function (resultado) {
                vm.ListaModelos = resultado.data;
                vm.ListBW = vm.ListaModelos;

            }, function (response) {
                blockUI.stop();
            });
        };

        function ObtieneCostoPreOperativo() {

            for (var i = 0; i < vm.ListaModelos.length; i++) {
                if (vm.ListaModelos[i].IdCosto == vm.IdOportunidadCosto) {
                    //monto = vm.ListaModelos[i].Instalacion;
                    vm.Inversion = vm.ListaModelos[i].Monto;
                    vm.PorcentajeGarantizado = vm.ListaModelos[i].PorcentajeGarantizado;
                    vm.Instalacion = vm.ListaModelos[i].Instalacion;
                    $("#IdCodigoBW").val(vm.ListaModelos[i].Descripcion);
                    break;
                }
            }

            $scope.$$childTail.vm.CostoPreOperativo = vm.CapexDolares;
        };

        function ObtieneCostoPreOperativoPorId() {
            var medio = {
                IdMedio: vm.IdMedio,
                IdTipoCosto: 8
            };
            var promise = CostoService.ListarCostos(medio);
            promise.then(function (resultado) {
                vm.ListaModelos = resultado.data;
                for (var i = 0; i < vm.ListaModelos.length; i++) {
                    if (vm.ListaModelos[i].IdCosto == vm.IdOportunidadCosto) {
                        //monto = vm.ListaModelos[i].Instalacion;
                        vm.Inversion = vm.ListaModelos[i].Monto;
                        vm.PorcentajeGarantizado = vm.ListaModelos[i].PorcentajeGarantizado;
                        vm.Instalacion = vm.ListaModelos[i].Instalacion;
                        $("#IdCodigoBW").val(vm.ListaModelos[i].Descripcion);
                        break;
                    }
                }
                $scope.$$childTail.vm.CostoPreOperativo = vm.CapexDolares;

            }, function (response) {
                blockUI.stop();
            });


        };


        function CargarGarantizado() {
            var filtroTipoEnlace = {
                IdRelacion: 117
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipoEnlace);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListGarantizado = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarCombos() {
            CargarGarantizado();
            ListarMedio();
            CargarTipoEnlace();
        };

        function CargaOtros() {

            if (vm.IdMedioOtros == 6) {
                UtilsFactory.MostrarElemento("#idGarantizadoList");
                UtilsFactory.OcultarElemento("#idGarantizadoText");
            } else {
                UtilsFactory.MostrarElemento("#idGarantizadoText");
                UtilsFactory.OcultarElemento("#idGarantizadoList");
            }
        };

        /******************************************* LOAD *******************************************/
        CargarCombos();
        CargarSatelital();

    }

})();