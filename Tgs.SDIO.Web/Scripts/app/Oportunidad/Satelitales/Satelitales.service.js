﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('SatelitalesService', SatelitalesService);

    SatelitalesService.$inject = ['$http', 'URLS'];

    function SatelitalesService($http, $urls) {

        var service = {
            ModalSatelitales: ModalSatelitales,
            CantidadSatelitales: CantidadSatelitales
        };

        return service;

        function ModalSatelitales(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Satelitales/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
 
            function DatosCompletados(response) {
                return response;
            }
        };

        function CantidadSatelitales(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Satelitales/CantidadSatelitales",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

    }
})();