﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('RouterSondaController', RouterSondaController);

    RouterSondaController.$inject = ['RouterSondaService', 'FlujoCajaService', 'MaestraService', 'CostoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RouterSondaController(RouterSondaService, FlujoCajaService, MaestraService, CostoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.tabFlujoCaja = true;
        vm.idRSonda = 0;
        vm.cantidad = 0;
        vm.instalacion = 0;
        vm.desinstalacion = 0;
        vm.precioUnitario = 0;
        vm.alquiler = 0;

        vm.GrabarRouterSonda = GrabarRouterSonda;

        vm.ListTipo = [];
        vm.IdTipo = "-1";

        vm.IdFlujoCaja = $scope.$parent.vm.Oportunidad.IdFlujoCaja;
        vm.IdOportunidadLineaNegocio = $scope.$parent.vm.IdOportunidadLineaNegocio;
        vm.IdSubServicioDatosCaratula = 0;
        vm.IdFlujoCajaConfiguracion = 0;
        vm.IdPeriodos = 0;
        vm.negativo = "-1";

        vm.ListModelos = [];
        vm.IdOportunidadCosto = -1;
        vm.OportunidadCosto = 0;
        vm.CargarBW = CargarBW;
        vm.CargaCamposCosto = CargaCamposCosto;
        vm.CalculaPxQ = CalculaPxQ;
        vm.ObtieneCostoPreOperativo = ObtieneCostoPreOperativo;
        vm.MontoBW = 0;
        /******************************************* Metodos *******************************************/

        function GrabarRouterSonda() {

            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();

                var costo = {
                    IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                    IdCosto: vm.IdOportunidadCosto,
                    IdOportunidadCosto: vm.OportunidadCosto,
                    Monto: vm.MontoBW
                };
                var IdOportunidadCosto = 0;
                var promise = FlujoCajaService.RegistrarOportunidadCosto(costo);

                promise.then(function (response) {
                    var Respuesta = response.data;

                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#lblAlerta_RouterSonda', 'danger', MensajesUI.DatosError, 20);

                    } else {
                        IdOportunidadCosto = Respuesta.Id;

                        var flujocaja = {
                            IdFlujoCaja: vm.IdFlujoCaja,
                            IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                            Cantidad: vm.cantidad,
                            CostoUnitario: 0,
                            IdOportunidadCosto: IdOportunidadCosto
                        };

                        var promise = FlujoCajaService.ActualizarOportunidadFlujoCaja(flujocaja);
                        promise.then(function (response) {
                            var Respuesta = response.data;
                            if (Respuesta.TipoRespuesta != 0) {
                                blockUI.stop();
                                UtilsFactory.Alerta('#lblAlerta_RouterSonda', 'danger', MensajesUI.DatosError, 20);
                            } else {

                                var caratula = {
                                    IdFlujoCaja: vm.IdFlujoCaja,
                                    IdSubServicioDatosCaratula: vm.IdSubServicioDatosCaratula,
                                    IdTipo: vm.IdTipo,
                                    Instalacion: vm.instalacion,
                                    Desinstalacion: vm.desinstalacion,
                                    PU: vm.precioUnitario,
                                    Alquiler: vm.alquiler,
                                    FlagRenovacion: vm.IdTipo,
                                    IdEstado:1
                                };

                                var promise = FlujoCajaService.ActualizarSubServicioCaratula(caratula);
                                promise.then(function (response) {

                                    var Respuesta = response.data;
                                    if (Respuesta.TipoRespuesta != 0) {
                                        blockUI.stop();
                                        UtilsFactory.Alerta('#lblAlerta_RouterSonda', 'danger', Respuesta.Mensaje, 20);
                                    } else {
                                        blockUI.stop();
                                        UtilsFactory.Alerta('#lblAlerta_RouterSonda', 'success', Respuesta.Mensaje, 10);
                                        $scope.$$childTail.vm.GrabarFlujoCajaConfiguracion();
                                        $scope.$parent.vm.BuscarCaratula();
                                    }

                                }, function (response) {
                                    blockUI.stop();
                                    UtilsFactory.Alerta('#lblAlerta_RouterSonda', 'danger', MensajesUI.DatosError, 5);
                                });
                            }
                        }, function (response) {
                            blockUI.stop();
                            UtilsFactory.Alerta('#lblAlerta_RouterSonda', 'danger', MensajesUI.DatosError, 5);
                        });

                    }

                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Circuito', 'danger', MensajesUI.DatosError, 5);
                });
            };
        };

        function CargarRouterSonda() {
            var dto = {
                IdSubServicioDatosCaratula: $scope.$parent.vm.dto.IdSubServicioDatosCaratula,
                IdGrupo: 4
            };

            var promise = FlujoCajaService.ObtenerDetalleOportunidadCaratula(dto);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;
                vm.servicio = respuesta.CodigoModelo;
                vm.cantidad = respuesta.Cantidad;
                vm.instalacion = respuesta.Instalacion;
                vm.desinstalacion = respuesta.Desinstalacion;
                vm.precioUnitario = respuesta.PU;
                vm.alquiler = respuesta.Alquiler;

                vm.IdOportunidadCosto = (respuesta.IdCosto == 0 || respuesta.IdCosto == null) ? vm.negativo : respuesta.IdCosto;
                vm.IdCosto = vm.IdOportunidadCosto;
                CargarBW();
                vm.IdFlujoCaja = respuesta.IdFlujoCaja;
                vm.IdSubServicioDatosCaratula = respuesta.IdSubServicioDatosCaratula;
                vm.IdFlujoCajaConfiguracion = respuesta.IdFlujoCajaConfiguracion;
                vm.IdPeriodos = respuesta.IdPeriodos==0?"2":respuesta.IdPeriodos;
                vm.IdTipo = respuesta.FlagRenovacion;
                ObtieneCostoPreOperativo();
               // $scope.$$childTail.vm.Meses = respuesta.TiempoProyecto;
                $scope.$$childTail.vm.ObtenerFlujoCajaConfiguracion();

                
                if (respuesta.FlagRenovacion > 0) { ListarTipoPorId(respuesta.FlagRenovacion) } else { CargarTipo() }

            }, function (response) {
                blockUI.stop();

            });
        };

        function ListarTipoPorId(int) {

            var filtroTipo = {
                IdRelacion: 4
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipo);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.IdTipo = int;
                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }


        function CargarTipo() {

            var filtroTipo = {
                IdRelacion: 4
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipo);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.IdTipo = "-1";
                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarBW() {
            var medio = {
                IdMedio: 1,
                IdTipoCosto: 1
            };
            var promise = CostoService.ListarCosto(medio);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListModelos = UtilsFactory.AgregarItemSelect(Respuesta);
             
            }, function (response) {
                blockUI.stop();
            });
        };
     

        function CargaCamposCosto() {
          
                //var index = document.getElementById("ComboModelo").selectedIndex;
                //vm.precioUnitario = vm.ListModelos[index].Monto;
                //vm.instalacion = vm.ListModelos[index].Instalacion;
                //vm.desinstalacion = vm.ListModelos[index].CostoSegmentoSatelital;

                var medio = {
                    IdCosto: vm.IdOportunidadCosto,
                    
                };
                var promise = CostoService.ObtenerCosto(medio);
                promise.then(function (resultado) {
                    var Respuesta = resultado.data;
                    vm.precioUnitario = Respuesta.Monto;
                    vm.MontoBW = Respuesta.Monto;
                    vm.instalacion = Respuesta.Instalacion;
                    vm.desinstalacion = Respuesta.CostoSegmentoSatelital;
                    CalculaPxQ();
                }, function (response) {
                    blockUI.stop();
                });
        };

        function CalculaPxQ() {

            var cantidad = (vm.cantidad == null) ? 0 : vm.cantidad;
            var monto =  (vm.precioUnitario == null) ? 0 :  vm.precioUnitario; 
            vm.alquiler = (cantidad * monto);
            ObtieneCostoPreOperativo();
        };

        function ObtieneCostoPreOperativo() {

            $scope.$$childTail.vm.CostoPreOperativo = vm.alquiler;
        };

        /******************************************* LOAD *******************************************/
        
 
        CargarRouterSonda();
    }
})();



