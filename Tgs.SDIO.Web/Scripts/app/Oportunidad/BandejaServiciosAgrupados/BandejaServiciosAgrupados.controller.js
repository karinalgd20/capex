﻿(function () {
    'use strict'
    angular
        .module('app.Oportunidad')
        .controller('BandejaServiciosAgrupados', BandejaServiciosAgrupados);

    BandejaServiciosAgrupados.$inject =
        [
        'OportunidadFlujoCajaService',
        'ServicioSedeInstalacionService',
        'blockUI',
        'UtilsFactory',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        'MensajesUI',
        '$timeout',
        'URLS',
        '$scope',
        '$compile',
        '$modal',
        '$injector'
        ];

    function BandejaServiciosAgrupados
        (
        OportunidadFlujoCajaService,
        ServicioSedeInstalacionService,
        blockUI,
        UtilsFactory,
        DTOptionsBuilder,
        DTColumnBuilder,
        MensajesUI,
        $timeout,
        $urls,
        $scope,
        $compile,
        $modal,
        $injector
        ) {

        var vm = this;
        vm.Id = 0;
        vm.IdSedeInstalacion = $scope.$parent.vm.IdSedeInstalacion;
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;
        vm.selectedSede = {};

        vm.dtColumns =
            [
                DTColumnBuilder.newColumn(null).withTitle('').notSortable()
                    .renderWith(function (data, type, full, meta) {
                        vm.selectedSede[full.IdFlujoCaja] = false;
                        return '<input type="checkbox" ng-model="vm.selectedSede[\'' + data.IdFlujoCaja + '\']" ng-click="vm.toggleOneSede(vm.selectedSede)">';
                    }),
                    DTColumnBuilder.newColumn('IdFlujoCaja').notVisible(),
                    DTColumnBuilder.newColumn('ServicioGrupo').withTitle('Servicio').notSortable(),
                    DTColumnBuilder.newColumn('TipoEnlace').withTitle('Tipo Enalace').notSortable(),
                    DTColumnBuilder.newColumn('VelocidadBW').withTitle('Velocidad').notSortable(),
                    DTColumnBuilder.newColumn('Interfaz').withTitle('Interfaz').notSortable(),
                    DTColumnBuilder.newColumn('TipoServicioSISEGO').withTitle('Tipo Servicio').notSortable()
            ];

        vm.ListarServiciosAgrupados = ListarServiciosAgrupados;
        vm.AgregarServicioSedeInstalacion = AgregarServicioSedeInstalacion;

        LimpiarGrilla();
        ListarServiciosAgrupados();

        function ListarServiciosAgrupados() {
            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('order', [])
                .withFnServerData(ListarServiciosAgrupadosPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }

        function ListarServiciosAgrupadosPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var request = {
                IdOportunidad: vm.IdOportunidad,
                IdFlujoCaja: vm.IdSedeInstalacion,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = OportunidadFlujoCajaService.ListarServiciosAgrupados(request);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListOportunidadFlujoCajaDtoResponse
                };

                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', false)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }

        function toggleOneSede(selectedSedeItems) {
            for (var Id in selectedSedeItems) {
                if (selectedSedeItems.hasOwnProperty(Id)) {
                    if (!selectedSedeItems[Id]) {
                        vm.selectAllSede = false;
                        return;
                    }
                }
            }
            vm.selectAllSede = true;
        }

        function ValidarCampos() {
            var mensaje = "";
            if (JSON.stringify(vm.selectedSede).indexOf("true") == -1) {
                mensaje = mensaje + 'Seleccione un servicio';
                UtilsFactory.InputBorderColor('#gridServicios', 'Rojo');
            }
            return mensaje;
        }

        function ObtenerIdSeleccionado(dato) {
            var array = [];
            for (var i in dato) {
                if (dato[i] == true) {
                    array.push({ "id": i });
                }
            }
            return array;
        }

        function AgregarServicioSedeInstalacion() {
            var mensaje = ValidarCampos();

            if (mensaje == "") {
                if (confirm('¿Estas seguro de grabar el registro ?')) {
                    var request = {
                        IdSedeInstalacion: vm.IdSedeInstalacion,
                        IdEstado: 1,
                        ListaServicios: ObtenerIdSeleccionado(vm.selectedSede)
                    }

                    var promise = ServicioSedeInstalacionService.RegistrarServicioSedeInstalacion(request);
                    promise.then(function (response) {
                        var Respuesta = response.data;
                        if (Respuesta.TipoRespuesta != 0) {
                            UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 20);
                        } else {
                            UtilsFactory.Alerta('#divAlert', 'success', response.data.Mensaje, 10);
                            ListarServiciosAgrupados();
                        }
                    }, function (response) {
                        blockUI.stop();
                        UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    });
                }

            } else {
                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
            }
        }
    }
})();