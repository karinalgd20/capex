﻿(function () {
    'use strict',
    angular
    .module('app.Oportunidad')
    .service('BandejaServiciosAgrupadosService', BandejaServiciosAgrupadosService);

    BandejaServiciosAgrupadosService.$inject = ['$http', 'URLS'];

    function BandejaServiciosAgrupadosService($http, $urls) {

        var service = {
            VistaBandejaServiciosAgrupados: VistaBandejaServiciosAgrupados
        };

        return service;

        function VistaBandejaServiciosAgrupados() {
            return $http({
                url: $urls.ApiOportunidad + "BandejaServiciosAgrupados/Index",
                method: "GET",
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
    }
})();