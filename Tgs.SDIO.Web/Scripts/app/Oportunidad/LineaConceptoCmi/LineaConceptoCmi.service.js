﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('LineaConceptoCmiService', LineaConceptoCmiService);

    LineaConceptoCmiService.$inject = ['$http', 'URLS'];

    function LineaConceptoCmiService($http, $urls) {

        var service = {
            ListarConceptosCmi: ListarConceptosCmi
        };

        return service;

        function ListarConceptosCmi(dto) {
            return $http({
                url: $urls.ApiOportunidad + "LineaConceptoCmi/ListarConceptosCmi",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

    }
})(); 