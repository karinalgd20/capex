﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('VisitaEditController', VisitaEditController);

    VisitaEditController.$inject = ['VisitaService', 'MaestraService', 'blockUI', 'UtilsFactory', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function VisitaEditController(VisitaService, MaestraService, blockUI, UtilsFactory, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.TituloModal = "Editar";
        vm.Id = 0;
        vm.IdVisita = $scope.$parent.vm.Id;
        vm.Cumplimiento = false;
        
        vm.GrabarAgenda = GrabarAgenda;

        /******************************************* Visita *******************************************/
        
        function GrabarAgenda() {
            if (!ValidarDatos())
                return;

            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();
                var dto = {
                    IdVisita: vm.IdVisita,
                    FechaAcuerdo: vm.FechaAcuerdo,
                    DescripcionPunto: vm.DescripcionPunto,
                    Responsable: vm.Responsable,
                    Cumplimiento: vm.Cumplimiento
                };

                var promise = VisitaService.RegistrarVisitaDetalle(dto);
                promise.then(function (response) {
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#lblAlerta_VisitaDetalle', 'danger', MensajesUI.DatosError, 20);
                    } else {
                        UtilsFactory.Alerta('#lblAlerta_VisitaDetalle', 'success', response.data.Mensaje, 10);
                        $scope.$parent.vm.dtInstanceVisita.rerender();
                        $("#ModalVisita").modal('hide');
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_VisitaDetalle', 'danger', MensajesUI.DatosError, 5);
                });

            };
        }

        /******************************************* Funciones *******************************************/
        function ValidarDatos() {
            if (vm.FechaAcuerdo == undefined || vm.FechaAcuerdo.trim() == '') {
                UtilsFactory.Alerta('#lblAlerta_VisitaDetalle', 'warning', 'Ingrese la fecha del acuerdo', 5);
                return;
            }

            if (vm.DescripcionPunto == undefined || vm.DescripcionPunto.trim() == '') {
                UtilsFactory.Alerta('#lblAlerta_VisitaDetalle', 'warning', 'Ingrese la descripcion del punto', 5);
                return;
            }

            if (vm.Responsable == undefined || vm.Responsable.trim() == '') {
                UtilsFactory.Alerta('#lblAlerta_VisitaDetalle', 'warning', 'Ingrese la descripcion del punto', 5);
                return;
            }
            
            if (new Date() > new Date(vm.FechaAcuerdo)) {
                UtilsFactory.Alerta('#lblAlerta_VisitaDetalle', 'warning', 'La fecha no puede ser anterior a la actual', 5);
                return;
            }

            return true;
        }

        function CargarVisitaDetalle() {
            
            var dto = {
                Id: vm.Id,
                IdVisita: vm.IdVisita
            };

            var promise = VisitaService.ObtenerVisitaDetalle(dto);

            promise.then(function (resultado) {
                var respuesta = resultado.data;
                if (respuesta != null && respuesta != '') {
                    vm.FechaAcuerdo = respuesta.FechaAcuerdo;
                    vm.DescripcionPunto = respuesta.DescripcionPunto;
                    vm.Responsable = respuesta.Responsable;
                    Cumplimiento = respuesta.Cumplimiento;

                    UtilsFactory.Alerta('#lblAlerta_VisitaDetalle', 'success', respuesta.Mensaje, 10);
                    $scope.$$childHead.vm.ObtenerFlujoCajaConfiguracion();
                }
                blockUI.stop();
                
            }, function (response) {
                blockUI.stop();
            });
        };

        function CargarTipoVisita() {
            var filtro = {
                IdRelacion: 278
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoVisita = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };
        
        /******************************************* LOAD *******************************************/
        CargarTipoVisita();
        //CargarVisitaDetalle();

    }
})();



