﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('VisitaService', VisitaService);

    VisitaService.$inject = ['$http', 'URLS'];

    function VisitaService($http, $urls) {

        var service = {
            VistaVisita: VistaVisita,
            ModalEditar: ModalEditar,
            Obtener: Obtener,
            ObtenerVisitaDetalle: ObtenerVisitaDetalle,
            ListarVisita: ListarVisita,
            Registrar: Registrar,
            ListarVisitaDetalle: ListarVisitaDetalle,
            RegistrarVisitaDetalle: RegistrarVisitaDetalle,
            EliminarVisitaDetalle: EliminarVisitaDetalle
        };

        return service;

        function VistaVisita() {
            return $http({
                url: $urls.ApiOportunidad + "Visita/Index",
                method: "GET"
            }).then(DatosCompletados);
 
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalEditar(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Visita/Editar",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function Obtener(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Visita/ObtenerVisitaPorIdOportunidad",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerVisitaDetalle(dto) {
            return $http({
                url: $urls.ApiOportunidad + "VisitaDetalle/ObtenerVisitaDetallePorId",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function Registrar(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Visita/RegistrarVisita",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarVisitaDetalle(dto) {
            return $http({
                url: $urls.ApiOportunidad + "VisitaDetalle/RegistrarVisitaDetalle",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarVisitaDetalle(dto) {
            return $http({
                url: $urls.ApiOportunidad + "VisitaDetalle/ListarVisitaDetallePaginado",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
        function ListarVisita(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Visita/ListarSubservicioPaginado",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarVisitaDetalle(dto) {
            return $http({
                url: $urls.ApiOportunidad + "VisitaDetalle/InhabilitarVisitaDetalle",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();