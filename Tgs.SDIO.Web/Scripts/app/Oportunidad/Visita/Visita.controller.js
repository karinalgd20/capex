﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('VisitaController', VisitaController);

    VisitaController.$inject = ['VisitaService', 'FlujoCajaService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function VisitaController(VisitaService, FlujoCajaService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.TituloModal = "Editar Visita";

        vm.Id = 0;
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;

        vm.ListTipoVisita = [];
        vm.IdTipoVisita = "-1";

        vm.BuscarAgenda = BuscarAgenda;
        vm.RegistrarVisita = RegistrarVisita;
        vm.EliminarVisitaDetalle = EliminarVisitaDetalle;
        vm.CargaModal = CargaModal;

        /******************************************* Visita *******************************************/
        vm.dtInstanceVisita = {};

        vm.dtColumnsVisita = [
         DTColumnBuilder.newColumn('Id').notVisible(),
         //DTColumnBuilder.newColumn('Numero').withTitle('Número').notSortable(),
         DTColumnBuilder.newColumn('DescripcionPunto').withTitle('Descripción del punto').notSortable(),
         DTColumnBuilder.newColumn('Responsable').withTitle('Responsable').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Fecha').notSortable().renderWith(DrawDate),
         DTColumnBuilder.newColumn(null).withTitle('Cumplimiento').notSortable().renderWith(DrawCheckBox),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaVisita)

        ];

        function AccionesBusquedaVisita(data, type, full, meta) {
            //var respuesta = "<div class='col-xs-3 col-sm-3'> <a title='Editar' class='btn btn-sm' id='tab-button' class='btn ' ng-click='vm.CargaModal(" + data.IdVisita + ")'><span class='fa fa-edit'></span></a></div> ";
            var respuesta = "<div class='col-xs-3 col-sm-3'><a title='Eliminar' class='btn btn-sm'  id='tab-button' class='btn ' ng-click='vm.EliminarVisitaDetalle(" + data.Id + ")' >" + "<span class='fa fa-trash-o'></span></a></div>";
            return respuesta;
        };

        function DrawCheckBox(data, type, full, meta) {
            return "<div class='col-xs-3 col-sm-3'> <input type='checkbox' ng-checked='" + data.Cumplimiento + "' disabled='disabled' /> </div> ";
        };

        function DrawDate(data, type, full, meta) {
            return UtilsFactory.ToJavaScriptDate(data.FechaAcuerdo);
        };

        function BuscarAgenda() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsVisita = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarAgendaPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarAgendaPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var dto = {
                IdVisita: vm.Id,
                IdEstado: 1,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = VisitaService.ListarVisitaDetalle(dto);
            promise.then(function (response) {
                
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListaVisitasDetalle
                };

                blockUI.stop();
                fnCallback(records);

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function CargarVisita() {
            if (vm.IdOportunidad != null && vm.IdOportunidad != 0) {
                var dto = {
                    IdOportunidad: vm.IdOportunidad
                };

                var promise = VisitaService.Obtener(dto);
                promise.then(function (response) {
                    if (response.data != null && response.data != '') {
                        vm.Id = response.data.Id;
                        vm.Visitante = response.data.PreVentaVisitante;
                        vm.Visitado = response.data.ClienteVisitado;
                        vm.FechaVisita = UtilsFactory.ToJavaScriptDate(response.data.Fecha);
                        vm.HoraInicio = response.data.HoraInicio.Hours + ":" + response.data.HoraInicio.Minutes;
                        vm.HoraFin = vm.HoraFin = response.data.HoraFin.Hours + ":" + response.data.HoraFin.Minutes;
                        vm.IdTipoVisita = response.data.IdTipo;
                        vm.dtInstanceVisita.rerender();
                    }
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    LimpiarGrilla();
                });
                
            }
        };

        function RegistrarVisita() {

            if (!ValidarDatos())
                return;

            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();
                var dto = {
                    Id: vm.Id,
                    IdOportunidad: vm.IdOportunidad,
                    PreVentaVisitante: vm.Visitante,
                    ClienteVisitado: vm.Visitado,
                    IdTipo: vm.IdTipoVisita,
                    Fecha: vm.FechaVisita,
                    HoraInicio: vm.HoraInicio,
                    HoraFin: vm.HoraFin
                };

                var promise = VisitaService.Registrar(dto);

                promise.then(function (response) {
                    if (response.status == 200) {
                        vm.Id = response.data.Id;
                        UtilsFactory.Alerta('#lblAlerta_Visita', 'success', response.data.Mensaje, 10);
                    }
                    blockUI.stop();
                }, function (response) {
                    UtilsFactory.Alerta('#lblAlerta_Visita', 'danger', MensajesUI.DatosError, 20);
                    blockUI.stop();
                });
            };
        };

        function EliminarVisitaDetalle(id) {            
            if (confirm('¿Estas seguro de eliminar el registro ?')) {
                blockUI.start();
                var dto = {
                    Id: id
                };

                var promise = VisitaService.EliminarVisitaDetalle(dto);
                promise.then(function (response) {
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#lblAlerta_Visita', 'danger', MensajesUI.DatosError, 20);
                    } else {
                        UtilsFactory.Alerta('#lblAlerta_Visita', 'success', response.data.Mensaje, 20);
                        vm.dtInstanceVisita.rerender();
                    }
                    blockUI.stop();
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_Visita', 'danger', MensajesUI.DatosError, 5);
                });

            };
        };


        /******************************************* Funciones *******************************************/

        function ValidarDatos() {
            if (vm.Visitante == undefined || vm.Visitante == '') {
                UtilsFactory.Alerta('#lblAlerta_Visita', 'warning', 'Ingrese el visitante', 5);
                return;
            }

            if (vm.Visitado == undefined || vm.Visitado == '') {
                UtilsFactory.Alerta('#lblAlerta_Visita', 'warning', 'Ingrese al cliente visitado', 5);
                return;
            }

            if (vm.IdTipoVisita == '-1') {
                UtilsFactory.Alerta('#lblAlerta_Visita', 'warning', 'Seleccione el tipo de visita', 5);
                return;
            }

            if (vm.FechaVisita == undefined || vm.FechaVisita == '') {
                UtilsFactory.Alerta('#lblAlerta_Visita', 'warning', 'Seleccione la fecha de la visita', 5);
                return;
            }

            if (vm.HoraInicio == undefined || vm.HoraInicio == '') {
                UtilsFactory.Alerta('#lblAlerta_Visita', 'warning', 'Seleccione la hora de inicio', 5);
                return;
            }

            if (vm.HoraFin == undefined || vm.HoraFin == '') {
                UtilsFactory.Alerta('#lblAlerta_Visita', 'warning', 'Seleccione la hora de fin', 5);
                return;
            }

            if (new Date('1/1/1999 ' + vm.HoraInicio) > new Date('1/1/1999 ' + vm.HoraFin))
            {
                UtilsFactory.Alerta('#lblAlerta_Visita', 'warning', 'La hora de inicio no puede ser posterior a la de fin', 5);
                return;
            }

            return true;
        }

        function LimpiarGrilla() {
            vm.dtOptionsVisita = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', true)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };

        function CargaModal(idVisita) {
            blockUI.start();
            var dto = {
                IdVisita: idVisita
            }

            var promise = VisitaService.ModalEditar(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);

                    $("#ContenidoVisita").html(content);
                    blockUI.stop();
                });

                $('#ModalVisita').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargarTipoVisita() {
            var filtro = {
                IdRelacion: 278
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtro);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoVisita = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        /******************************************* LOAD *******************************************/
        CargarTipoVisita();        
        CargarVisita();
        BuscarAgenda();
    }
})();



