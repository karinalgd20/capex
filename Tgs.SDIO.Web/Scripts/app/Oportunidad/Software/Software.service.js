﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('SoftwareService', SoftwareService);

    SoftwareService.$inject = ['$http', 'URLS'];

    function SoftwareService($http, $urls) {

        var service = {
            ModalSoftware: ModalSoftware,
            CantidadSoftware: CantidadSoftware
        };

        return service;

        function ModalSoftware(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Software/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function CantidadSoftware(dto) {
            return $http({
                url: $urls.ApiOportunidad + "ECapex/CantidadCapex", 
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

    }
})();