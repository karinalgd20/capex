﻿(function () {
    'use strict',
    angular
    .module('app.Oportunidad')
    .service('RegistrarComponenteService', RegistrarComponenteService);

    RegistrarComponenteService.$inject = ['$http', 'URLS'];

    function RegistrarComponenteService($http, $urls) {

        var service = {
            ModalRegistrarComponente: ModalRegistrarComponente
        };

        return service;

         function ModalRegistrarComponente(request) {
            return $http({
                url: $urls.ApiOportunidad + "RegistrarComponente/Index",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();