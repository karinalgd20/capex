﻿(function () {
            'use strict'
            angular
            .module('app.Oportunidad')
            .controller('RegistrarComponenteController', RegistrarComponenteController);

            RegistrarComponenteController.$inject = ['RegistrarComponenteService','BandejaServicioService','FlujoCajaService', 'SubServicioService','MaestraService',
                 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

            function RegistrarComponenteController(RegistrarComponenteService,BandejaServicioService,FlujoCajaService,SubServicioService,MaestraService,
                 blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

                var vm = this;
        vm.IdOportunidad = $scope.$parent.vm.IdOportunidad;

      
     
        vm.RegistrarComponente = RegistrarComponente;

        vm.CargarComponente = CargarComponente;


        vm.IdFlujoCaja = "-1";
        vm.ListServicio = [];

        vm.IdGrupo = "-1";
        vm.ListGrupo = [];


        vm.IdSubServicio = "-1";
        vm.ListSubServicio = [];

        vm.IdTipoCosto="-1";
        vm.ListTipoCosto=[];

        vm.IdMoneda = "-1";
        vm.ListMoneda = [];


        function RegistrarComponente() {
            $scope.$parent.vm.TabPorPerfilOportunidadDatos();

              blockUI.start();
              var confirmacion = true;

              if (vm.IdGrupo == "-1") {
                  UtilsFactory.Alerta('#lblAlerta_FlujoCaja', 'danger', "Seleccione Grupo", 20);
                  confirmacion = false;
            }

              if (vm.IdSubServicio == "-1") {
                  UtilsFactory.Alerta('#lblAlerta_FlujoCaja', 'danger', "Seleccione Componente", 20);
                  confirmacion = false;
            }
            var IdFlujoCaja = vm.IdFlujoCaja;
            if (IdFlujoCaja == "-1") {
                IdFlujoCaja = null;
            }
            var IdMoneda = vm.IdMoneda;
            if (IdMoneda == "-1") {
                IdMoneda = null;
            }
            var IdTipoCosto = vm.IdTipoCosto;
            if (IdTipoCosto == "-1") {
                IdTipoCosto = null;
            }

            if (confirmacion) {
                var componente = {
                    IdFlujoCaja: IdFlujoCaja,
                    IdGrupo: vm.IdGrupo,
                    IdMoneda: IdMoneda,
                    IdTipoCosto: IdTipoCosto,
                    IdSubServicio: vm.IdSubServicio,
                    IdOportunidadLineaNegocio: $scope.$parent.$parent.vm.IdOportunidadLineaNegocio


                };
                var promise = FlujoCajaService.RegistrarComponente(componente);

                promise.then(function (resultado) {
                    blockUI.stop();

                    var Respuesta = resultado.data;

                    UtilsFactory.Alerta('#lblAlerta_FlujoCaja', 'success', MensajesUI.DatosOk, 20);
                    $scope.$parent.vm.CargaECapex();
                    $scope.$parent.vm.CargaCaratula();
                    
                }, function (response) {
                    blockUI.stop();

                });
            }
        }

        function ListarServicioGrupo() {

            var flujoCaja = {
               // IdOportunidad:vm.IdOportunidad
               IdOportunidadLineaNegocio: $scope.$parent.$parent.vm.IdOportunidadLineaNegocio
            };
            
            var promise = FlujoCajaService.ListarPorIdFlujoCaja(flujoCaja);
           // var promise = FlujoCajaService.ListaServicioPorOportunidadLineaNegocio(flujoCaja);
            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListServicio = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }


        function ListarPestanaGrupoSubServicio() {

            var pestanaGrupo = {
                IdEstado: 1,
                FlagServicio: 0
            };
            debugger
            var promise = BandejaServicioService.ListarPestanaGrupo(pestanaGrupo);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListGrupo = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        

        function CargarComponente() {
            var confirmacion = true;
            if (vm.IdGrupo == "-1") {
                confirmacion = false;
                vm.ListSubServicio = [];
                vm.IdSubServicio = "-1";
                vm.ListSubServicio = UtilsFactory.AgregarItemSelect(vm.ListSubServicio);
            }
            if (confirmacion) {
                vm.ListSubServicio = [];

                var subServicio = {
                    IdEstado: 1,
                    IdGrupo: vm.IdGrupo
                };
                var promise = SubServicioService.ListarSubServiciosPorIdGrupo(subServicio);

                promise.then(function (resultado) {
                    blockUI.stop();

                    var Respuesta = resultado.data;

                    if (Respuesta.length == 1) {
                        vm.IdSubServicio = Respuesta[0].Codigo;
                        vm.ListSubServicio = UtilsFactory.AgregarItemSelect(Respuesta);

                    }
                    else {
                        vm.IdSubServicio = "-1";
                        vm.ListSubServicio = UtilsFactory.AgregarItemSelect(Respuesta);
                    }
                   

                }, function (response) {
                    blockUI.stop();

                });
            }

        }
        function ListarTipoCosto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 153
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListTipoCosto = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarMoneda() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 98
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListMoneda = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        ListarMoneda();
        ListarServicioGrupo();
        ListarPestanaGrupoSubServicio();
        CargarComponente();
        ListarTipoCosto();
    }
})();