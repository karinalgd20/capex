﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('IndicadoresService', IndicadoresService);

    IndicadoresService.$inject = ['$http', 'URLS'];

    function IndicadoresService($http, $urls) {

        var service = {
            VistaIndicador: VistaIndicador
        };

        return service;

        function VistaIndicador(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Indicador/Index",
                method: "GET",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

    }
})();