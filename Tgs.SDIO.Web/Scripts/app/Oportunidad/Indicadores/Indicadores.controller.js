﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('IndicadorController', IndicadorController);

    IndicadorController.$inject = ['FlujoCajaService','IndicadoresService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function IndicadorController(FlujoCajaService, IndicadoresService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        vm.Oportunidad = $scope.$parent.vm.ObjOportunidad;
        vm.IdOportunidadLineaNegocio = vm.Oportunidad.IdOportunidadLineaNegocio;
        vm.IdLineaNegocio = vm.Oportunidad.IdLineaNegocio;
        vm.IdUsuario = vm.Oportunidad.IdUsuarioCreacion;
        vm.IdTipoFicha = 1;
        vm.FlagLinea = false;
        vm.FlagMostrarReporte = false;

        vm.GenerarIndicadores = GenerarIndicadores;

        function GenerarIndicadores() {
            if (vm.IdLineaNegocio == undefined || vm.IdLineaNegocio <= 0) {
                alert("Seleccione una Linea");
                return;
            }
            blockUI.start();
         
            var dto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                IdUsuarioCreacion: vm.IdUsuario,
                TipoFicha: vm.IdTipoFicha
            };

            var promise = FlujoCajaService.GenerarFlujoCajaDetalle(dto);
            promise.then(function (response) {
                if (response.status == 200) {

                    var parametros = "reporte=Indicadores&IdOportunidadLineaNegocio=" + vm.IdOportunidadLineaNegocio +
                        "&IdEstado=" + 1 +
                        "&TipoFicha=" + vm.IdTipoFicha;

                    $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
                    vm.FlagMostrarReporte = true;

                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_FContable', 'success', MensajesUI.DetalleGeneradoOk, 5);
                } else {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_FContable', 'danger', DetalleGeneradoError, 5);
                }

            }, function (response) {
                blockUI.stop();
            });
        };

        /******************************************* LOAD *******************************************/

    }
})();



