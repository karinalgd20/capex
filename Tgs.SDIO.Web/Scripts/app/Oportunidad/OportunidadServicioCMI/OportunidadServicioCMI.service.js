﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('OportunidadServicioCMIService', OportunidadServicioCMIService);

    OportunidadServicioCMIService.$inject = ['$http', 'URLS'];

    function OportunidadServicioCMIService($http, $urls) {

        var service = {
            ListarServicioCMIPorLineaNegocio: ListarServicioCMIPorLineaNegocio
        };

        return service;

        function ListarServicioCMIPorLineaNegocio(servicioCMI) {
           return $http({
                url: $urls.ApiComun + "OportunidadServicioCMI/ListarServicioCMIPorLineaNegocio",
                method: "POST",
                data: JSON.stringify(servicioCMI)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})(); 