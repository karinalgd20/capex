﻿(function () {
    'use strict'
    angular
        .module('app.PlantaExterna')
        .controller('DashboardPlantaExternaController', DashboardPlantaExternaController);

    DashboardPlantaExternaController.$inject = ['blockUI', 'UtilsFactory', '$timeout', 'MensajesUI', 'URLS'];

    function DashboardPlantaExternaController(blockUI, UtilsFactory, $timeout, MensajesUI, $urls) {

       var vm = this;

       MostrarDashboard();

       function MostrarDashboard() {

           var parametros = "reporte=DashBoardPlantaExterna";

           $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
       };
    }
})();