﻿(function () {
    'use strict'
    angular
    .module('app.PlantaExterna')
    .controller('RegistrarTipoCambioSisegoController', RegistrarTipoCambioSisegoController);

    RegistrarTipoCambioSisegoController.$inject = ['RegistrarTipoCambioSisegoService','MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarTipoCambioSisegoController(RegistrarTipoCambioSisegoService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.RegistrarTipoCambioSisego = RegistrarTipoCambioSisego;         
        vm.FechaFin = UtilsFactory.ObtenerFechaActual();
        vm.FechaInicio = UtilsFactory.ObtenerFechaActual();
        vm.Monto = 0; 
        vm.Id = 0;
        vm.IdEstado = "-1";
        vm.ListaEstados = [];
        vm.BloquearEstado= false;
        CargarVista();

        function CargarVista() {

            blockUI.start();

            vm.Id = $scope.$parent.vmTipo.Id;

            if (vm.Id > 0) {

                var tipoCambio = {
                    Id: vm.Id
                };

                var promise = RegistrarTipoCambioSisegoService.ObtenerTipoCambioSisego(tipoCambio);
                promise.then(function (resultado) {
                    var respuesta = resultado.data;
                    vm.Monto = respuesta.Monto;                    
                    vm.FechaInicio = moment(respuesta.FechaInicio).format("DD/MM/YYYY");
                    vm.FechaFin = moment(respuesta.FechaFin).format("DD/MM/YYYY"); 
                    vm.IdEstado = respuesta.IdEstado.toString();
                    blockUI.stop();                     
                }, function (response) {
                    blockUI.stop();
                });
            } else {
                vm.IdEstado = "1";
                vm.BloquearEstado = true;
            }
            
            ListarEstados(); 
        };

        function ListarEstados() {
            var maestra = {
                IdRelacion: 1
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;
                console.log(respuesta);
                vm.ListaEstados = UtilsFactory.AgregarItemSelect(respuesta);
            }, function (response) {
                blockUI.stop();
            });
        };

        function RegistrarTipoCambioSisego() {      

            if ($.trim(vm.FechaInicio) == "") {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese la fecha inicio.", 5);
                return;
            }

            if (!UtilsFactory.ValidarFecha(vm.FechaInicio)) {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese una fecha inicio valida.", 5);
                return;
            }

            if ($.trim(vm.FechaFin) == "") {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese la fecha fin.", 5);
                return;
            }

            if (!UtilsFactory.ValidarFecha(vm.FechaFin)) {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese una fecha fin valida.", 5);
                return;
            }

            if ($.trim(vm.Monto) == '' || vm.Monto == 0) {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese una tipo de cambio valido.", 5);
                return;
            }
            
            if ($.trim(vm.IdEstado) == '-1') {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "Ingrese un estado.", 5);
                return;
            }

            if (UtilsFactory.CompararFechas(vm.FechaInicio, vm.FechaFin)) { 
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', "La fecha de inicio debe ser menor que la fecha final.", 5);
                return;
            }

            blockUI.start();

            var tipoCambioDtoRequest = {
                Id:vm.Id,
                FechaFin: vm.FechaFin,
                FechaInicio: vm.FechaInicio,
                Monto: vm.Monto,
                IdEstado: vm.IdEstado
            };

            var promise = (tipoCambioDtoRequest.Id > 0) ? RegistrarTipoCambioSisegoService.ActualizarTipoCambioSisego(tipoCambioDtoRequest) : RegistrarTipoCambioSisegoService.RegistrarTipoCambioSisego(tipoCambioDtoRequest);

            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;

                vm.Id = respuesta.Id;

                if (respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlertRegistrar', 'success', respuesta.Mensaje, 10); 
                    $scope.$parent.vmTipo.ListarTipoCambioSisego();
                } else {
                    UtilsFactory.Alerta('#divAlertRegistrar', 'danger', respuesta.Mensaje, 10);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', MensajesUI.DatosError, 5);
            });
        }

    }

})();