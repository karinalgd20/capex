﻿(function () {
    'use strict',
     angular
    .module('app.PlantaExterna')
    .service('RegistrarTipoCambioSisegoService', RegistrarTipoCambioSisegoService);

    RegistrarTipoCambioSisegoService.$inject = ['$http', 'URLS'];

    function RegistrarTipoCambioSisegoService($http, $urls) {

        var service = {
            ModalRegistrarTipoCambioSisego: ModalRegistrarTipoCambioSisego,
            RegistrarTipoCambioSisego: RegistrarTipoCambioSisego,            
            ActualizarTipoCambioSisego: ActualizarTipoCambioSisego,
            ObtenerTipoCambioSisego: ObtenerTipoCambioSisego
        };

        return service;

        function ModalRegistrarTipoCambioSisego() {
            return $http({
                url: $urls.ApiPlantaExterna + "RegistrarTipoCambioSisego/Index",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarTipoCambioSisego(request) {
            return $http({
                url: $urls.ApiPlantaExterna + "RegistrarTipoCambioSisego/RegistrarTipoCambioSisego",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarTipoCambioSisego(request) {
            return $http({
                url: $urls.ApiPlantaExterna + "RegistrarTipoCambioSisego/ActualizarTipoCambioSisego",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerTipoCambioSisego(request) {
            return $http({
                url: $urls.ApiPlantaExterna + "RegistrarTipoCambioSisego/ObtenerTipoCambioSisego",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();