﻿(function () {
    'use strict'
    angular
        .module('app.PlantaExterna')
        .controller('BandejaTipoCambioSisegoController', BandejaTipoCambioSisegoController);

    BandejaTipoCambioSisegoController.$inject = ['BandejaTipoCambioSisegoService','RegistrarTipoCambioSisegoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function BandejaTipoCambioSisegoController(BandejaTipoCambioSisegoService, RegistrarTipoCambioSisegoService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vmTipo = this;
        vmTipo.ListarTipoCambioSisegoPaginado = ListarTipoCambioSisegoPaginado;
        vmTipo.ModalRegistrarTipoCambioSisego = ModalRegistrarTipoCambioSisego;
        vmTipo.LimpiarFiltros = LimpiarFiltros;

        vmTipo.FechaInicio = UtilsFactory.ObtenerFechaActual();
        vmTipo.FechaFin = UtilsFactory.AgregarDias(UtilsFactory.ObtenerFechaActual(),30);
        vmTipo.Id = 0;

        vmTipo.dtInstance = {};
        vmTipo.dtColumns = [ 
            DTColumnBuilder.newColumn('FechaInicio').withTitle('Fecha Inicio').renderWith(function (data, type, full) {
                return moment(data).format("DD/MM/YYYY");
            }).notSortable(),
             DTColumnBuilder.newColumn('FechaFin').withTitle('Fecha Fin').renderWith(function (data, type, full) {
                 return moment(data).format("DD/MM/YYYY");
             }).notSortable(), 
            DTColumnBuilder.newColumn('Monto').withTitle('Monto').notSortable().withOption('width', '30%'),
            DTColumnBuilder.newColumn('Estado').withTitle('Estado').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];

        LimpiarGrilla();
         
        function ModalRegistrarTipoCambioSisego(id) {
            vmTipo.Id = id;

            var promise = RegistrarTipoCambioSisegoService.ModalRegistrarTipoCambioSisego();
            promise.then(function (response) {
                var respuesta = $(response.data);
                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoTipoCambio").html(content);
                });

                $('#ModalTipoCambio').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });
        }; 

        function ListarTipoCambioSisegoPaginado() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vmTipo.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(ListarTipoCambioSisego)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                       .withOption('createdRow', function (row, data, dataIndex) {
                           $compile(angular.element(row).contents())($scope);
                       })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function ListarTipoCambioSisego(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var tipoCambioDtoRequest = {
                FechaInicio: vmTipo.FechaInicio,
                FechaFin: vmTipo.FechaFin,
                Indice: pageNumber,
                Tamanio: length
            };
              
            var promise = BandejaTipoCambioSisegoService.ListarTipoCambioSisego(tipoCambioDtoRequest);

            promise.then(function (resultado) {

                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListaTipoCambioDtoResponse
                };

                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vmTipo.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('paging', false);
        }

        function AccionesBusqueda(data, type, full, meta) {
            return "<div class='col-xs-6 col-sm-12'><a title='Editar' class='btn btn-primary'  id='tab-button' data-toggle='modal' ng-click='vmTipo.ModalRegistrarTipoCambioSisego(" + data.Id + ");'>" + "<span class='fa fa-edit fa-lg'></span></a>";
        };

        function LimpiarFiltros() {
            vmTipo.FechaInicio = UtilsFactory.ObtenerFechaActual();
            vmTipo.FechaFin = UtilsFactory.AgregarDias(UtilsFactory.ObtenerFechaActual(), 30);
            LimpiarGrilla();
        };
    }
})();