﻿(function () {
    'use strict',
     angular
    .module('app.PlantaExterna')
    .service('BandejaTipoCambioSisegoService', BandejaTipoCambioSisegoService);

    BandejaTipoCambioSisegoService.$inject = ['$http', 'URLS'];

    function BandejaTipoCambioSisegoService($http, $urls) {

        var service = { 
            ListarTipoCambioSisego: ListarTipoCambioSisego
        };

        return service; 

        function ListarTipoCambioSisego(request) {
            return $http({
                url: $urls.ApiPlantaExterna + "BandejaTipoCambioSisego/ListarTipoCambioSisego",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
 

    }
})();