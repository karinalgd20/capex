﻿(function () {
    'use strict',
     angular
    .module('app.PlantaExterna')
    .service('AccionEstrategicaService', AccionEstrategicaService);

    AccionEstrategicaService.$inject = ['$http', 'URLS'];

    function AccionEstrategicaService($http, $urls) {

        var service = { 
            ListarAccionEstrategica: ListarAccionEstrategica
        };

        return service;
 
        function ListarAccionEstrategica(request) {
            return $http({
                url: $urls.ApiPlantaExterna + "AccionEstrategica/ListarAccionEstrategica",
                method: "POST",
                data: JSON.stringify(request)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

 

    }

})();