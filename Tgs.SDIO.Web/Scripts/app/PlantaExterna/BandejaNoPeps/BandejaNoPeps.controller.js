﻿(function () {
    'use strict'
    angular
   .module('app.PlantaExterna')
   .controller('BandejaNoPepsController', BandejaNoPepsController);

    BandejaNoPepsController.$inject =
        ['BandejaNoPepsService',
         'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];
    
    function BandejaNoPepsController(
        BandejaNoPepsService,
     MaestraService,
        blockUI,
        UtilsFactory,
        DTColumnDefBuilder,
        DTOptionsBuilder,
        DTColumnBuilder,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile,
        $modal,
        $injector) {

        var vm = this;

        vm.BuscarNoPeps = BuscarNoPeps;
        vm.EliminarNoPeps = EliminarNoPeps;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.CerrarPopup = CerrarPopup;
        vm.CargaModalPepReg = CargaModalPepReg;
        vm.CargaModalPepEdi = CargaModalPepEdi;

        vm.Id = 0;
        vm.CodigoPep="";
        vm.color;         
        vm.dtInstanceNoPeps = {};
        vm.search = {};
        vm.Peps = '';

        vm.dtColumnsNoPeps = [
            DTColumnBuilder.newColumn('Id').withTitle('ID').withOption('width', '5%'),
            DTColumnBuilder.newColumn('CodigoPep').withTitle('Codigo Peps').withOption('width', '50%').notSortable(),
            DTColumnBuilder.newColumn('Estado').withTitle('Estado').withOption('width', '10%').notSortable(),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesNoPeps).withOption('width', '5%')
        ];

        LimpiarGrilla();

        function AccionesNoPeps(data, type, full, meta) {
            let NumeroFila = meta.row;
            var respuesta = "";
            respuesta = respuesta + "<center><div class='form-group'>";
            respuesta = respuesta + "<a title='Editar'   ng-click='vm.CargaModalPepEdi(\"" + data.Id + "\");'>" + "<span class='glyphicon glyphicon-pencil' style='color: #00A4B6; margin-left: 1px;'></span></a> ";
            respuesta = respuesta + "<a title='Accion'   ng-click='vm.EliminarNoPeps(\"" + data.Id + "\");'>" + "<span class='glyphicon glyphicon-trash' style='color: #00A4B6; margin-left: 1px;'></span></a>";
            respuesta = respuesta + "</div></center>";
            return respuesta;
        };
         
        function BuscarNoPeps() {
            vm.CodigoPep = (vm.CodigoPep == undefined ? "" : vm.CodigoPep)
            blockUI.start();
            vm.selected = {};
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptionsNoPeps = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', false)
                    .withFnServerData(BuscarNoPepsPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)

                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 1000);
        };

        function BuscarNoPepsPaginado(sSource, aoData, fnCallback, oSettings) {
            
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var size = aoData[4].value;
            
            var pageNumber = (start + size) / size;
            vm.search.Indice = pageNumber;
            vm.search.Tamanio = size;
            vm.search.CodigoPep = vm.CodigoPep
            var NoPeps = vm.search;

            vm.selected = {};

            var promise = BandejaNoPepsService.ListarNoPeps(NoPeps);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.TotalItemCount,
                    'recordsFiltered': response.data.TotalItemCount,
                    'data': response.data.ListaNoPeps
                };
                blockUI.stop();
                fnCallback(records);
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsNoPeps = DTOptionsBuilder
            .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', false)
                .withOption('destroy', true)
                .withOption('scrollCollapse', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('scrollX', true)
                .withOption('paging', false);
            
        };

        function CargaModalPepReg() {
            vm.Id=0;
            var dto = {
                Id: 0
            };

            var promise = BandejaNoPepsService.ModalNoPeps(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoNoPeps").html(content);
                });

                $('#ModalNoPeps').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargaModalPepEdi(Id) {
            vm.Id = Id;
            var dto = {
                Id: vm.Id
            };

            var promise = BandejaNoPepsService.ModalNoPeps(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoNoPeps").html(content);
                });

                $('#ModalNoPeps').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };
 
        function EliminarNoPeps(Item) {

            var sList = "";
            var msjerror = "";
            var msj = "";
            var tipo = 0;

                if(confirm("Esta seguro de eliminar el registro")){

                    var dto = {
                        Id: Item,
                        IdEstado: 0
                    };

                    var promise = BandejaNoPepsService.EliminarNoPeps(dto);
                    promise.then(function (response) {

                        var Respuesta = response.data;
                        if (Respuesta.TipoRespuesta != 0) {
                            UtilsFactory.Alerta('#alertCasoNegocio', 'danger', Respuesta.Mensaje, 20);
                        } else {

                            UtilsFactory.Alerta('#alertCasoNegocio', 'success', Respuesta.Mensaje, 10);
                            BuscarNoPeps();
                        }
                    
                    }, function (response) {
                        blockUI.stop();
                    });

            }
            
        } 

        function LimpiarFiltros() {
         vm.CodigoPep='';
        }

        function CerrarPopup() {
            $('.modal').modal('toggle');
        };
         
    }

})();