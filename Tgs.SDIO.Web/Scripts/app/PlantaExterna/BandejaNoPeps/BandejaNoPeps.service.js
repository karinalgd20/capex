﻿(function () {
    'use strict',
     angular
    .module('app.PlantaExterna')
    .service('BandejaNoPepsService', BandejaNoPepsService);

    BandejaNoPepsService.$inject = ['$http', 'URLS'];

    function BandejaNoPepsService($http, $urls) {

        var service = {
            ListarNoPeps: ListarNoPeps,
            EliminarNoPeps: EliminarNoPeps,
            ModalNoPeps: ModalNoPeps
        };

        return service;

        function ListarNoPeps(NoPeps) {

            return $http({
                url: $urls.ApiPlantaExterna + "BandejaNoPeps/ListarNoPeps",
                method: "POST",
                data: JSON.stringify(NoPeps)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarNoPeps(NoPeps) {

            return $http({
                url: $urls.ApiPlantaExterna + "BandejaNoPeps/EliminarNoPeps",
                method: "POST",
                data: JSON.stringify(NoPeps)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ModalNoPeps(dto) {
            return $http({
                url: $urls.ApiPlantaExterna + "RegistrarNoPeps/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };


    }

})();