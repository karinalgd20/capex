﻿(function () {
    'use strict'
    angular
    .module('app.PlantaExterna')
    .controller('ReporteConsolidadoSisegosController', ReporteConsolidadoSisegosController);

    ReporteConsolidadoSisegosController.$inject = ['ReporteConsolidadoSisegosService', 'AccionEstrategicaService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector', '$interval'];

    function ReporteConsolidadoSisegosController(ReporteConsolidadoSisegosService,AccionEstrategicaService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector, $interval) {

        var vm = this;
    
        vm.BuscarReporte = BuscarReporte;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.Sisego='';
        vm.Proyecto = '';
        vm.Pep1 = '';
        vm.Salesforce = '';
        vm.Anio = '';
        vm.AccionEstrategicaData = [];
        vm.AccionEstrategicaModel = [];


       ListarAccionEstrategica();

       function BuscarReporte() {
           var cadena = "";
 
          
           $.each(vm.AccionEstrategicaModel, function (i, item) {
               cadena += item.id;
               cadena += (cadena != "" ? "|" : "");
           }); 
           
            blockUI.start();
          
            var parametros ="reporte=ReporteConsolidadoSisegos";
            parametros += "&sisego=" + vm.Sisego;
            parametros += "&IdProyecto=" + vm.Proyecto;
            parametros += "&Pep1=" + vm.Pep1;
            parametros += "&Salesforce=" + vm.Salesforce;
            
 
            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
 
            $("#FReporte").height(1000);

            var promise = $interval(function () {
                if (document.getElementById("rvVisor_ctl10") != 'null') {
                    blockUI.stop();
                    $interval.cancel(promise);
                }
            }, 7000);
        };

        function LimpiarFiltros() {
            vm.Sisego = '';
            vm.Proyecto = '';
            vm.Pep1 = '';
            vm.Salesforce = '';
        };

        function ListarAccionEstrategica() {

            $(".caret").remove();

            var accionEstrategicaDtoRequest = {
                IdEstado:1
            };

            vm.AccionEstrategicaSettings = {
                scrollableHeight: '200px',
                showUncheckAll:true,
                scrollable: true,
                enableSearch: false,
                styleActive: true
            };
            var promise = AccionEstrategicaService.ListarAccionEstrategica(accionEstrategicaDtoRequest);
            promise.then(function (resultado) {
                vm.AccionEstrategicaData = resultado.data; 
            });
            
        }
       
    }

})();