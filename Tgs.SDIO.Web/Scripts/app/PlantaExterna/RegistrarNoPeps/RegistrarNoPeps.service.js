﻿(function () {
    'use strict',
     angular
    .module('app.PlantaExterna')
    .service('RegistrarNoPepsService', RegistrarNoPepsService);

    RegistrarNoPepsService.$inject = ['$http', 'URLS'];

    function RegistrarNoPepsService($http, $urls) {

        var service = {
            ObtenerNoPeps: ObtenerNoPeps,
            ActualizarNoPeps: ActualizarNoPeps,
            RegistrarNoPeps: RegistrarNoPeps,
        };

        return service;

        function ActualizarNoPeps(NoPeps) {

            return $http({
                url: $urls.ApiPlantaExterna + "RegistrarNoPeps/ActualizarNoPeps",
                method: "POST",
                data: JSON.stringify(NoPeps)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarNoPeps(NoPeps) {

            return $http({
                url: $urls.ApiPlantaExterna + "RegistrarNoPeps/RegistrarNoPeps",
                method: "POST",
                data: JSON.stringify(NoPeps)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerNoPeps(NoPeps) {
            return $http({
                url: $urls.ApiPlantaExterna + "RegistrarNoPeps/ObtenerNoPeps",
                method: "POST",
                data: JSON.stringify(NoPeps)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();