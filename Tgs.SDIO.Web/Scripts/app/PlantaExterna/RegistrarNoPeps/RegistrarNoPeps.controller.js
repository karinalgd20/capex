﻿(function () {
    'use strict'
    angular
   .module('app.PlantaExterna')
   .controller('RegistrarNoPepsController', RegistrarNoPepsController);

    RegistrarNoPepsController.$inject =
        ['RegistrarNoPepsService',
         'MaestraService',
            'blockUI',
            'UtilsFactory',
            'DTColumnDefBuilder',
            'DTOptionsBuilder',
            'DTColumnBuilder',
            '$timeout',
            'MensajesUI',
            'URLS',
            '$scope',
            '$compile',
            '$modal',
            '$injector'];
    
    function RegistrarNoPepsController(
        RegistrarNoPepsService,
     MaestraService,
        blockUI,
        UtilsFactory,
        DTColumnDefBuilder,
        DTOptionsBuilder,   
        DTColumnBuilder,
        $timeout,
        MensajesUI,
        $urls,
        $scope,
        $compile,
        $modal,
        $injector) {

        var vr = this;

        vr.NoPepsActualizar = NoPepsActualizar;
        vr.NoPepsRegistro = NoPepsRegistro;
        vr.DetalleNoPep = DetalleNoPep;
        vr.LimpiarFiltros = LimpiarFiltros;
        vr.CerrarPopup = CerrarPopup;

        vr.Id = 0;
        vr.CodigoPep = '';
        vr.color;
        vr.btnguardar = 'ng-hide';
        vr.btnregistrar = 'ng-hide';
        vr.Peps = '';

        CargarVista();
         
        function NoPepsRegistro() {
            vr.CodigoPep = (vr.CodigoPep == undefined ? "" : vr.CodigoPep);

            if (vr.CodigoPep != "") {
                vr.Id = 0;
                var dto = {
                    CodigoPep: vr.CodigoPep,
                    IdEstado: 1
                };

                var promise = RegistrarNoPepsService.RegistrarNoPeps(dto);
                promise.then(function (response) {
                  
                    var respuesta = response.data;

                    switch(respuesta.TipoRespuesta){
                        case 1:
                            UtilsFactory.Alerta('#divAlertMaestro', 'danger', respuesta.Mensaje, 10);
                            $scope.$parent.vm.BuscarNoPeps();
                            break;
                        case 0:
                            UtilsFactory.Alerta('#divAlertMaestro', 'success', respuesta.Mensaje, 5);
                            $scope.$parent.vm.BuscarNoPeps();
                            LimpiarFiltros();
                            break;
                        case 2:
                            UtilsFactory.Alerta('#divAlertMaestro', 'danger', 'Ya existe un Regitro con el mismo Codigo', 20);
                            $scope.$parent.vm.BuscarNoPeps();
                            break;
                        default:
                            UtilsFactory.Alerta('#divAlertMaestro', 'danger', 'Indefinido', 10);
                          $scope.$parent.vm.BuscarNoPeps();
                    }
                    $('#ModalNoPeps').modal({
                        keyboard: false
                    });
                }, function (response) {
                    blockUI.stop();
                });

           } else {
                alert("Debe Ingresar la Serie Peps");
               $("#RPeps").focus();
            }
        }; 

        function NoPepsActualizar(Id) { 
            vr.Id = Id;
            var RegNoPeps = {
                Id: Id,
                CodigoPep:vr.CodigoPep
            };
            var promise = RegistrarNoPepsService.ActualizarNoPeps(RegNoPeps);

            promise.then(function (response) {

                var respuesta = response.data;

                switch (respuesta.TipoRespuesta) {
                    case 1:
                        UtilsFactory.Alerta('#divAlertMaestro', 'danger', respuesta.Mensaje, 10);
                        $scope.$parent.vm.BuscarNoPeps();
                        break;
                    case 0:
                        UtilsFactory.Alerta('#divAlertMaestro', 'success', respuesta.Mensaje, 5);
                        $scope.$parent.vm.BuscarNoPeps();
                        break;
                    case 2:
                        UtilsFactory.Alerta('#divAlertMaestro', 'danger', 'Ya existe un Regitro con el mismo Codigo', 20);
                        $scope.$parent.vm.BuscarNoPeps();
                        break;
                    default:
                        UtilsFactory.Alerta('#divAlertMaestro', 'danger', 'Indefinido', 10);
                        $scope.$parent.vm.BuscarNoPeps();
                }

                $('#ModalNoPeps').modal({
                    keyboard: false,
                    backdrop: 'static'
                });

            }, function (response) {
                blockUI.stop();
            });

        };

        function DetalleNoPep(Id) {
            vr.Id = Id;
            var RegNoPeps = {
                Id: Id
            };

            var promise = RegistrarNoPepsService.ObtenerNoPeps(RegNoPeps);

            promise.then(function (response) {

                var respuesta = response.data;
                vr.CodigoPep = respuesta.CodigoPep;
                vr.Estado = respuesta.IdEstado;
                vr.Id = respuesta.Id;

                $('#ModalNoPeps').modal({
                    keyboard: false,
                    backdrop: 'static'

                });
            }, function (response) {
                blockUI.stop();
            });

        }

        function CargarVista() {
            vr.Id = $scope.$parent.vm.Id;
            setTimeout("$('#RCodigoPep').focus();", 500);
            if (vr.Id > 0) {
                vr.btnregistrar = 'ng-hide';
                vr.btnguardar = ''; 
                DetalleNoPep(vr.Id);
            } else {
                vr.btnregistrar = '';
                vr.btnguardar = 'ng-hide';
            }
        };

        function LimpiarFiltros() {
            vr.CodigoPep = '';
            $("#RCodigoPep").focus();
        }

        function CerrarPopup() {
            $scope.$parent.vm.CerrarPopup();            
        }; 

    }

})();