﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Tgs.SDIO.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapMvcAttributeRoutes();

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                            name: "Default",
                            url: "{controller}/{action}/{id}",
                            defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional }
                        ).DataTokens = new RouteValueDictionary(new { area = "Seguridad" });

            routes.MapRoute(
                name: "Principal",
                url: "Principal/{action}",
                defaults: new { controller = "Principal", action = "Index" }
            ); 

            routes.MapRoute(
                name: "SinAcceso",
                url: "{controller}/{action}",
                defaults: new { controller = "SinAcceso", action = "Index" }
            );


        }
    }
}
