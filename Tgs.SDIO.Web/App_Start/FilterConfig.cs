﻿using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            var globalFilterErrorAttribute = InitializeContainer.Container.GetInstance<RepErrorCatchAttribute>();
            filters.Add(new HandleErrorAttribute());
            filters.Add(globalFilterErrorAttribute);
        }
    }
}
