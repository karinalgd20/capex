﻿using System.Web;
using System.Web.Optimization;
using Tgs.SDIO.Web.App_Start.Bundles;
namespace Tgs.SDIO.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery/js").Include(
            "~/Scripts/jquery/jquery-{version}.js",
            "~/Scripts/jquery/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap/js").Include(
            "~/Scripts/js/bootstrap.min.js",
            "~/Scripts/respond.js"));
            bundles.Add(new ScriptBundle("~/bundles/modernizr/js").Include(
            "~/Scripts/modernizr-*"));
            bundles.Add(new ScriptBundle("~/bundles/Main/js").Include(
            "~/Scripts/js/summernote.js",
            "~/Scripts/js/jquery.auto-complete.js",
            "~/Scripts/js/bootstrap-filestyle.js",
            "~/Scripts/js/waves.js",
            "~/Scripts/js/metisMenu.min.js",
            "~/Scripts/js/jquery.slimscroll.js",
            "~/Scripts/js/jquery.app.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/angular/js").Include(
            "~/Scripts/angular/angular.js",
            "~/Scripts/angular/angular-route.js",
            "~/Scripts/angular/angular-animate.js",
            "~/Scripts/angular/angular-block-ui.js",
            "~/Scripts/angular/angular-sanitize.js",
            "~/Scripts/angular/angular-summernote.min.js",
            "~/Scripts/angular/angular-sanitize.js",
            "~/Scripts/angular/angular-ui.mask.js",
            "~/Scripts/Assets/angularjs-dropdown-multiselect/angularjs-dropdown-multiselect.js",
            "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
            "~/Scripts/js/lodash.min.js",
            "~/Scripts/angular/angular-ui.mask.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/Assets/js").Include(
            "~/Scripts/Assets/bootstrap-datepicker/moment.js",
            "~/Scripts/Assets/bootstrap-datepicker/es.js",
            "~/Scripts/Assets/bootstrap-datepicker/bootstrap-datetimepicker.js",
            "~/Scripts/Assets/bootstrap-datatables/jquery.dataTables.min.js",
            "~/Scripts/Assets/bootstrap-datatables/angular-datatables.js",
            //"~/Scripts/Assets/bootstrap-datatables/semantic.min.js",
            "~/Scripts/Assets/bootstrap-datatables/angular-datatables.util.js",
            "~/Scripts/Assets/bootstrap-datatables/angular-datatables.options.js",
            "~/Scripts/Assets/bootstrap-datatables/angular-datatables.instances.js",
            "~/Scripts/Assets/bootstrap-datatables/angular-datatables.factory.js",
            "~/Scripts/Assets/bootstrap-datatables/angular-datatables.renderer.js",
            "~/Scripts/Assets/bootstrap-datatables/angular-datatables.directive.js",
            "~/Scripts/Assets/bootstrap-datatables/dataTables.responsive.js",
             "~/Scripts/Assets/bootstrap-datatables/dataTables.select.min.js",
             "~/Scripts/Assets/tiny-mce/tinymce.min.js",
             "~/Scripts/Assets/tiny-mce/theme.min.js",
             "~/Scripts/Assets/bootstrap-fileinput/fileinput.min.js",
             "~/Scripts/Assets/bootstrap-fileinput/es.js",
              "~/Scripts/Assets/bootstrap-inputmask/bootstrap-inputmask.min.js",
              "~/Scripts/Assets/bootstrap-inputmask/ng-currency-mask.min.js",
              
             "~/Scripts/Assets/bootstrap-select2/select2.min.js",
             "~/Scripts/js/paging.js",

              "~/Scripts/Assets/fuelux/spinner.min.js",
             "~/Scripts/Assets/fuelux/tree.min.js"
            //EAAR
            //"~/Scripts/Assets/bootstrap-datatables/dataTables.fixedColumns.min.js"
            // "~/Scripts/Assets/bootstrap-datatables/angular-datatables.fixedcolumns.min.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/app/base/js").Include(
            "~/Scripts/app/Base/base.module.js",
            "~/Scripts/app/Base/base.constantes.js",
            "~/Scripts/app/Base/utils.factory.js",
            "~/Scripts/app/Base/Controles/js/datepicker.directive.js",
            "~/Scripts/app/Base/Controles/js/timepicker.directive.js",
            "~/Scripts/app/Base/Controles/js/angular-strap.compat.js",
            "~/Scripts/app/Base/Controles/js/angular-strap.js",
            "~/Scripts/app/Base/Controles/js/angular-strap.tpl.js",
            "~/Scripts/app/Base/Controles/js/alert.js",
            "~/Scripts/app/Base/Controles/js/uploadFile.directive.js",
            "~/Scripts/app/Base/Controles/js/tituloDatosObligatorios.directive.js",
            "~/Scripts/app/Base/Controles/js/SelectorMeses.js",
            "~/Scripts/app/Base/Controles/js/SelectorAnios.js",
            "~/Scripts/app/Base/Controles/js/tooltip.js",
            "~/Scripts/app/Base/Controles/js/parse-options.js",
            "~/Scripts/app/Base/Controles/js/typeahead.js",
            "~/Scripts/js/ui-bootstrap-tpls.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/Grafico/js").Include(
            "~/Scripts/js/morris.js",
            "~/Scripts/js/raphael-min.js",
            "~/Scripts/js/jquery.easypiechart.js",
            "~/Scripts/js/jquery.sparkline.js",
            "~/Scripts/js/jquery.flot.js",
            "~/Scripts/js/jquery.flot.tooltip.min.js",
            "~/Scripts/js/jquery.flot.pie.resize.js",
            "~/Scripts/js/jquery.flot.pie.js",
            "~/Scripts/js/jquery.flot.animator.min.js",
            "~/Scripts/js/jquery.flot.growraf.js",
            "~/Scripts/js/scripts.js"         


            ));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap/layout/js").Include(
            "~/Content/css/vendors/js/ui/tether.min.js",
            "~/Content/css/vendors/js/ui/perfect-scrollbar.jquery.min.js",
            "~/Content/css/vendors/js/ui/unison.min.js",
            "~/Content/css/vendors/js/ui/blockUI.min.js",
            "~/Content/css/vendors/js/ui/jquery.matchHeight-min.js",
            "~/Content/css/vendors/js/ui/screenfull.min.js",
            "~/Content/css/vendors/js/extensions/pace.min.js",
            "~/Content/css/vendors/js/ui/prism.min.js",
            "~/Content/css/js/core/app-menu.js",
            "~/Content/css/js/core/app.js",
            "~/Scripts/js/jquery-ui-1.10.1.custom.min.js",
            "~/Scripts/js/bootstrap.min.js",
            "~/Scripts/js/jquery.dcjqaccordion.2.7.js",
            "~/Scripts/js/jquery.scrollTo.min.js",
            "~/Scripts/js/jquery.slimscroll.js",
            "~/Scripts/js/jquery.nicescroll.js",
            "~/Scripts/js/skycons.js",
            "~/Scripts/js/jquery.scrollTo.js",
            "~/Scripts/js/clndr.js",
            "~/Scripts/js/jquery.easing.min.js",
            "~/Scripts/js/underscore-min.js",
            "~/Scripts/js/moment-2.2.1.js",
            "~/Scripts/js/evnt.calendar.init.js"));
            bundles.Add(new StyleBundle("~/bundles/bootstrap/Master/css").Include(
            "~/Content/css/bootstrap.css",
            "~/Content/css/fonts/icomoon.css",
            "~/Content/css/fonts/flag-icon-css/css/flag-icon.min.css",
            "~/Content/css/vendors/css/extensions/pace.css",
            "~/Content/css/vendors/css/ui/prism.min.css",
            "~/Content/css/bootstrap-extended.css",
            "~/Content/css/app.css",
            "~/Content/css/colors.css",
            "~/Content/css/core/menu/menu-types/vertical-menu.css",
            "~/Content/css/core/menu/menu-types/vertical-overlay-menu.css",
            "~/Content/css/style.css",
            "~/Content/plantilla/bs3/css/bootstrap.min.css",
            "~/Content/plantilla/css/bootstrap-reset.css",
            "~/Content/plantilla/css/style.css",
            "~/Content/plantilla/font-awesome/css/font-awesome.css",
            "~/Content/morris.css",
            "~/Content/plantilla/css/style-responsive.css",
            "~/Content/plantilla/css/table-responsive.css"
            ));
            bundles.Add(new StyleBundle("~/bundles/Assets/css").Include(
            "~/Content/Assets/bootstrap-datepicker/bootstrap-datetimepicker.css",
            "~/Content/Assets/bootstrap-datatables/jquery.dataTables.css",
            "~/Content/Assets/bootstrap-datatables/jquery.dataTables.min.css",
            "~/Content/Assets/bootstrap-datatables/angular-datatables.css",
            //"~/Content/Assets/bootstrap-datatables/semantic.min.css",
            "~/Content/Assets/tiny-mce/skin.min.css",
             "~/Content/Assets/tiny-mce/content.min.csss",  
            "~/Content/Assets/bootstrap-datatables/responsive.dataTables.css",
            "~/Content/Assets/bootstrap-fileinput/fileinput.min.css",
            "~/Content/Assets/bootstrap-select2/select2.min.css",
            "~/Content/angular-block-ui.min.css"
            ));
            bundles.Add(new StyleBundle("~/bundles/login/css").Include(
            "~/Content/login/css/animate.css",
            "~/Content/login/css/bootstrap.min.css",
            "~/Content/login/css/fontawesome-all.min.css",
            "~/Content/login/css/iofrm-style.css",
            "~/Content/login/css/iofrm-theme1.css",
            "~/Content/angular-block-ui.min.css"));
            bundles.Add(new ScriptBundle("~/bundles/login/js").Include(
            "~/Content/login/js/jquery.min.js",
            "~/Content/login/js/bootstrap.min.js",
            "~/Content/login/js/main.js",
            "~/Content/login/js/popper.min.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/app/seguridad/perfiles/js").Include(
            "~/Scripts/app/Seguridad/Login.module.js",
            "~/Scripts/app/Seguridad/Perfil.controller.js",
            "~/Scripts/app/Seguridad/Login.service.js"
            ));
            CartaFianzaBundleConfig.RegisterCartaFianzaBundleConfig(bundles);
            ComunBundleConfig.RegisterComunBundleConfig(bundles);
            FunnelBundleConfig.RegisterFunnelBundleConfig(bundles);
            NegocioBundleConfig.RegisterNegocioBundleConfig(bundles);
            SeguridadBundleConfig.RegisterSeguridadBundleConfig(bundles);
            OportunidadBundleConfig.RegisterOportunidadBundleConfig(bundles);
            TrazabilidadBundleConfig.RegisterTrazabilidadBundleConfig(bundles);
            PlantaExternaBundleConfig.RegisterPlantaExternaBundleConfig(bundles);
            ProyectoBundleConfig.RegisterProyectoBundleConfig(bundles);
            CapexBundleConfig.RegisterCapexBundleConfig(bundles);
            CompraBundleConfig.RegisterCompraBundleConfig(bundles);
            BundleTable.EnableOptimizations = false;
        }
    }
    public class CssRewriteUrlTransformWrapper : IItemTransform
    {
        public string Process(string includedVirtualPath, string input)
        {
            return new CssRewriteUrlTransform().Process("~" + VirtualPathUtility.ToAbsolute(includedVirtualPath), input);
        }
    }
}