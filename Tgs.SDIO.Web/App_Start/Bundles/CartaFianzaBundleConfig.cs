﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public class CartaFianzaBundleConfig
    {


        public static void RegisterCartaFianzaBundleConfig(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/MaestroCartaFianza/js").Include(
             "~/Scripts/app/CartaFianza/CartaFianza.module.js",
             "~/Scripts/app/Comun/Comun.module.js",
             "~/Scripts/app/Comun/Comun.service.js",
             "~/Scripts/app/Comun/Maestra/Maestra.service.js",
             "~/Scripts/app/Comun/Cliente/Cliente.service.js",
             "~/Scripts/app/Comun/Archivo/Archivo.service.js",

             "~/Scripts/app/Comun/Correo/Correo.controller.js",
             "~/Scripts/app/Comun/Correo/Correo.service.js",

             "~/Scripts/app/CartaFianza/MaestroCartaFianza/MaestroCartaFianza.controller.js",
             "~/Scripts/app/CartaFianza/MaestroCartaFianza/MaestroCartaFianza.service.js",

             "~/Scripts/app/CartaFianza/RegistroCartaFianza/RegistroCartaFianza.controller.js",
             "~/Scripts/app/CartaFianza/RegistroCartaFianza/RegistroCartaFianza.service.js",

             "~/Scripts/app/CartaFianza/ClienteCartaFianza/ClienteCartaFianza.controller.js",
             "~/Scripts/app/CartaFianza/ClienteCartaFianza/ClienteCartaFianza.service.js",

             "~/Scripts/app/CartaFianza/AccionCartaFianza/AccionCartaFianza.controller.js",
             "~/Scripts/app/CartaFianza/AccionCartaFianza/AccionCartaFianza.service.js"


         ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/RegistroCartaFianza/js").Include(
            "~/Scripts/app/CartaFianza/CartaFianza.module.js",
            "~/Scripts/app/Comun/Comun.module.js",
            "~/Scripts/app/Comun/Comun.service.js",
            "~/Scripts/app/Comun/Maestra/Maestra.service.js",
            "~/Scripts/app/CartaFianza/AccionCartaFianza/AccionCartaFianza.service.js",
            "~/Scripts/app/CartaFianza/RegistroCartaFianza/RegistroCartaFianza.controller.js",
            "~/Scripts/app/CartaFianza/RegistroCartaFianza/RegistroCartaFianza.service.js"
        ));


            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/ClienteCartaFianza/js").Include(
           "~/Scripts/app/CartaFianza/CartaFianza.module.js",
           "~/Scripts/app/Comun/Comun.module.js",
           "~/Scripts/app/Comun/Comun.service.js",
           "~/Scripts/app/Comun/Cliente/Cliente.service.js",
           "~/Scripts/app/CartaFianza/ClienteCartaFianza/ClienteCartaFianza.controller.js",
           "~/Scripts/app/CartaFianza/ClienteCartaFianza/ClienteCartaFianza.service.js"

       ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/AccionCartaFianza/js").Include(
            "~/Scripts/app/CartaFianza/CartaFianza.module.js",
            "~/Scripts/app/Comun/Comun.module.js",
            "~/Scripts/app/Comun/Comun.service.js",
            "~/Scripts/app/Comun/Cliente/Cliente.service.js",
            "~/Scripts/app/CartaFianza/AccionCartaFianza/AccionCartaFianza.controller.js",
            "~/Scripts/app/CartaFianza/AccionCartaFianza/AccionCartaFianza.service.js"

            ));

                  bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/RenovacionCartaFianza/js").Include(
           "~/Scripts/app/CartaFianza/CartaFianza.module.js",
           "~/Scripts/app/Comun/Comun.module.js",
           "~/Scripts/app/Comun/Comun.service.js",
           "~/Scripts/app/Comun/Maestra/Maestra.service.js",
           "~/Scripts/app/Comun/Archivo/Archivo.service.js",
           "~/Scripts/app/Comun/Cliente/Cliente.service.js",

           "~/Scripts/app/Comun/Correo/Correo.controller.js",
            "~/Scripts/app/Comun/Correo/Correo.service.js",

                         "~/Scripts/app/CartaFianza/RegistroCartaFianza/RegistroCartaFianza.controller.js",
             "~/Scripts/app/CartaFianza/RegistroCartaFianza/RegistroCartaFianza.service.js",

            "~/Scripts/app/CartaFianza/AccionCartaFianza/AccionCartaFianza.controller.js",
            "~/Scripts/app/CartaFianza/AccionCartaFianza/AccionCartaFianza.service.js",


                  "~/Scripts/app/CartaFianza/ReporteCartaFianzaRenovacionGarantia/ReporteCartaFianzaRenovacionGarantia.controller.js",
                  "~/Scripts/app/CartaFianza/ReporteCartaFianzaRenovacionGarantia/ReporteCartaFianzaRenovacionGarantia.service.js",
           "~/Scripts/app/CartaFianza/RenovacionCartaFianza/RenovacionCartaFianza.controller.js",
           "~/Scripts/app/CartaFianza/RenovacionCartaFianza/RenovacionCartaFianza.service.js",
           "~/Scripts/app/CartaFianza/MaestroCartaFianza/MaestroCartaFianza.service.js"

           ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/RecuperoCartaFianza/js").Include(
           "~/Scripts/app/CartaFianza/CartaFianza.module.js",
           "~/Scripts/app/Comun/Comun.module.js",
           "~/Scripts/app/Comun/Comun.service.js",
           "~/Scripts/app/Comun/Cliente/Cliente.service.js",
           "~/Scripts/app/Comun/Maestra/Maestra.service.js",

           "~/Scripts/app/CartaFianza/MaestroCartaFianza/MaestroCartaFianza.service.js",
           "~/Scripts/app/CartaFianza/RenovacionCartaFianza/RenovacionCartaFianza.service.js",
           "~/Scripts/app/CartaFianza/RecuperoCartaFianza/RecuperoCartaFianza.controller.js",
           "~/Scripts/app/CartaFianza/RecuperoCartaFianza/RecuperoCartaFianza.service.js"

           ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/AsignarColaboradorCartaFianza/js").Include(
           "~/Scripts/app/CartaFianza/CartaFianza.module.js",
           "~/Scripts/app/Comun/Comun.module.js",
           "~/Scripts/app/Comun/Comun.service.js",
           "~/Scripts/app/Comun/Cliente/Cliente.service.js",
           "~/Scripts/app/Comun/Maestra/Maestra.service.js",

           "~/Scripts/app/CartaFianza/MaestroCartaFianza/MaestroCartaFianza.service.js",
           "~/Scripts/app/CartaFianza/AsignarColaboradorCartaFianza/AsignarColaboradorCartaFianza.controller.js",
           "~/Scripts/app/CartaFianza/AsignarColaboradorCartaFianza/AsignarColaboradorCartaFianza.service.js"

           ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/ParametrosCartaFianza/js").Include(
           "~/Scripts/app/CartaFianza/CartaFianza.module.js",
           "~/Scripts/app/Comun/Comun.module.js",
           "~/Scripts/app/Comun/Comun.service.js",
           "~/Scripts/app/Comun/Cliente/Cliente.service.js",
           "~/Scripts/app/Comun/Maestra/Maestra.service.js",

           "~/Scripts/app/CartaFianza/MaestroCartaFianza/MaestroCartaFianza.service.js",
           "~/Scripts/app/CartaFianza/ParametrosCartaFianza/ParametrosCartaFianza.controller.js",
           "~/Scripts/app/CartaFianza/ParametrosCartaFianza/ParametrosCartaFianza.service.js"

           ));


            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/DashBoardCartaFianza/js").Include(
            "~/Scripts/app/CartaFianza/CartaFianza.module.js",
           "~/Scripts/app/Comun/Comun.module.js",
           "~/Scripts/app/Comun/Comun.service.js",
           "~/Scripts/app/Comun/Cliente/Cliente.service.js",
           "~/Scripts/app/Comun/Maestra/Maestra.service.js",

        "~/Scripts/app/CartaFianza/DashBoardCartaFianza/DashBoardCartaFianza.controller.js",
        "~/Scripts/app/CartaFianza/DashBoardCartaFianza/DashBoardCartaFianza.service.js"

        ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/ReporteConsolidadoCartaFianza/js").Include(
           "~/Scripts/app/CartaFianza/CartaFianza.module.js",
           "~/Scripts/app/Comun/Comun.module.js",
           "~/Scripts/app/Comun/Comun.service.js",
           "~/Scripts/app/Comun/Cliente/Cliente.service.js",
           "~/Scripts/app/Comun/Maestra/Maestra.service.js",

           "~/Scripts/app/CartaFianza/ReporteConsolidadoCartaFianza/ReporteConsolidadoCartaFianza.controller.js",
           "~/Scripts/app/CartaFianza/ReporteConsolidadoCartaFianza/ReporteConsolidadoCartaFianza.service.js"

           ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/ReporteEstatusRecuperoCartaFianza/js").Include(
           "~/Scripts/app/CartaFianza/CartaFianza.module.js",
           "~/Scripts/app/Comun/Comun.module.js",
           "~/Scripts/app/Comun/Comun.service.js",
           "~/Scripts/app/Comun/Cliente/Cliente.service.js",
           "~/Scripts/app/Comun/Maestra/Maestra.service.js",

           "~/Scripts/app/CartaFianza/ReporteEstatusRecuperoCartaFianza/ReporteEstatusRecuperoCartaFianza.controller.js",
           "~/Scripts/app/CartaFianza/ReporteEstatusRecuperoCartaFianza/ReporteEstatusRecuperoCartaFianza.service.js"

           ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/ReporteCartaFianzaSinRespuesta/js").Include(
           "~/Scripts/app/CartaFianza/CartaFianza.module.js",
           "~/Scripts/app/Comun/Comun.module.js",
           "~/Scripts/app/Comun/Comun.service.js",
           "~/Scripts/app/Comun/Cliente/Cliente.service.js",
           "~/Scripts/app/Comun/Maestra/Maestra.service.js",

           "~/Scripts/app/CartaFianza/MaestroCartaFianza/MaestroCartaFianza.service.js",
           "~/Scripts/app/CartaFianza/ReporteCartaFianzaSinRespuesta/ReporteCartaFianzaSinRespuesta.controller.js",
           "~/Scripts/app/CartaFianza/ReporteCartaFianzaSinRespuesta/ReporteCartaFianzaSinRespuesta.service.js"

           ));


            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/ReporteSolicitudDevolucionCartaFianza/js").Include(
                    "~/Scripts/app/CartaFianza/CartaFianza.module.js",
                    "~/Scripts/app/Comun/Comun.module.js",
                    "~/Scripts/app/Comun/Comun.service.js",
                     "~/Scripts/app/Comun/Cliente/Cliente.service.js",
                    "~/Scripts/app/Comun/Maestra/Maestra.service.js",

                    "~/Scripts/app/CartaFianza/ClienteCartaFianza/ClienteCartaFianza.controller.js",
                    "~/Scripts/app/CartaFianza/ClienteCartaFianza/ClienteCartaFianza.service.js",

                    "~/Scripts/app/CartaFianza/ReporteSolicitudDevolucionCartaFianza/ReporteSolicitudDevolucionCartaFianza.controller.js",
                    "~/Scripts/app/CartaFianza/ReporteSolicitudDevolucionCartaFianza/ReporteSolicitudDevolucionCartaFianza.service.js"

                    ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/ReporteSolicitudRenovacionCartaFianza/js").Include(
                    "~/Scripts/app/CartaFianza/CartaFianza.module.js",
                    "~/Scripts/app/Comun/Comun.module.js",
                    "~/Scripts/app/Comun/Comun.service.js",
                    "~/Scripts/app/Comun/Cliente/Cliente.service.js",
                    "~/Scripts/app/Comun/Maestra/Maestra.service.js",

                    "~/Scripts/app/CartaFianza/ClienteCartaFianza/ClienteCartaFianza.controller.js",
                    "~/Scripts/app/CartaFianza/ClienteCartaFianza/ClienteCartaFianza.service.js",

                    "~/Scripts/app/CartaFianza/ReporteSolicitudRenovacionCartaFianza/ReporteSolicitudRenovacionCartaFianza.controller.js",
                    "~/Scripts/app/CartaFianza/ReporteSolicitudRenovacionCartaFianza/ReporteSolicitudRenovacionCartaFianza.service.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/ReporteEstatusTesoreriaCartaFianza/js").Include(
                    "~/Scripts/app/CartaFianza/CartaFianza.module.js",
                    "~/Scripts/app/Comun/Comun.module.js",
                    "~/Scripts/app/Comun/Comun.service.js",
                    "~/Scripts/app/Comun/Cliente/Cliente.service.js",
                    "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                    
                    "~/Scripts/app/CartaFianza/MaestroCartaFianza/MaestroCartaFianza.service.js",
                    "~/Scripts/app/CartaFianza/ReporteEstatusTesoreriaCartaFianza/ReporteEstatusTesoreriaCartaFianza.controller.js",
                    "~/Scripts/app/CartaFianza/ReporteEstatusTesoreriaCartaFianza/ReporteEstatusTesoreriaCartaFianza.service.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/ReporteCartaFianzaRenovacionGarantia/js").Include(
                  "~/Scripts/app/CartaFianza/CartaFianza.module.js",
                  "~/Scripts/app/Comun/Comun.module.js",
                  "~/Scripts/app/Comun/Comun.service.js",
                  "~/Scripts/app/Comun/Cliente/Cliente.service.js",
                  "~/Scripts/app/Comun/Maestra/Maestra.service.js",          

                  "~/Scripts/app/CartaFianza/ReporteCartaFianzaRenovacionGarantia/ReporteCartaFianzaRenovacionGarantia.controller.js",
                  "~/Scripts/app/CartaFianza/ReporteCartaFianzaRenovacionGarantia/ReporteCartaFianzaRenovacionGarantia.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/FormularioGerentesCuenta/js").Include(
                    "~/Scripts/app/CartaFianza/CartaFianza.module.js",
                    "~/Scripts/app/Comun/Comun.module.js",
                    "~/Scripts/app/Comun/Comun.service.js",
                    "~/Scripts/app/Comun/Cliente/Cliente.service.js",
                    "~/Scripts/app/Comun/Maestra/Maestra.service.js",

                    "~/Scripts/app/CartaFianza/MaestroCartaFianza/MaestroCartaFianza.service.js",
                    "~/Scripts/app/CartaFianza/FormularioGerentesCuenta/FormularioGerentesCuenta.controller.js",
                    "~/Scripts/app/CartaFianza/FormularioGerentesCuenta/FormularioGerentesCuenta.service.js"
         ));

        }
    }
}