﻿using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public class TrazabilidadBundleConfig
    {
        public static void RegisterTrazabilidadBundleConfig(BundleCollection bundles)
        {            

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaObservacionOportunidad/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Comun/ConceptoSeguimiento/ConceptoSeguimiento.service.js",
             "~/Scripts/app/Trazabilidad/BandejaObservacionOportunidad/BandejaObservacionOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/BandejaObservacionOportunidad/BandejaObservacionOportunidad.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarObservacionOportunidad/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Comun/ConceptoSeguimiento/ConceptoSeguimiento.service.js",
             "~/Scripts/app/Trazabilidad/OportunidadST/OportunidadST.service.js",
             "~/Scripts/app/Trazabilidad/CasoOportunidad/CasoOportunidad.service.js",
              "~/Scripts/app/Trazabilidad/ObservacionOportunidadBusquedaOportunidadCaso/ObservacionOportunidadBusquedaOportunidadCaso.service.js",
             "~/Scripts/app/Trazabilidad/ObservacionOportunidadBusquedaOportunidadCaso/ObservacionOportunidadBusquedaOportunidadCaso.controller.js",
             "~/Scripts/app/Trazabilidad/RegistrarObservacionOportunidad/RegistrarObservacionOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarObservacionOportunidad/RegistrarObservacionOportunidad.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaRecursoOportunidad/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/BandejaRecursoOportunidad/BandejaRecursoOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/BandejaRecursoOportunidad/BandejaRecursoOportunidad.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarRecursoOportunidad/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Comun/RolRecurso/RolRecurso.service.js",
             "~/Scripts/app/Trazabilidad/OportunidadST/OportunidadST.service.js",
             "~/Scripts/app/Comun/Recurso/Recurso.service.js",
             "~/Scripts/app/Trazabilidad/RecursoOportunidadBusquedaOportunidad/RecursoOportunidadBusquedaOportunidad.controller.js",
             "~/Scripts/app/Trazabilidad/RecursoOportunidadBusquedaOportunidad/RecursoOportunidadBusquedaOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/RecursoOportunidadBusquedaRecurso/RecursoOportunidadBusquedaRecurso.controller.js",
             "~/Scripts/app/Trazabilidad/RecursoOportunidadBusquedaRecurso/RecursoOportunidadBusquedaRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarRecursoOportunidad/RegistrarRecursoOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarRecursoOportunidad/RegistrarRecursoOportunidad.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarRecursoOportunidadMasivo/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Comun/RolRecurso/RolRecurso.service.js",
             "~/Scripts/app/Trazabilidad/OportunidadST/OportunidadST.service.js",
             "~/Scripts/app/Comun/BandejaRecurso/BandejaRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarRecursoOportunidadMasivo/RegistrarRecursoOportunidadMasivo.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarRecursoOportunidadMasivo/RegistrarRecursoOportunidadMasivo.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaDatosPreventaMovil/js").Include(
             "~/Scripts/app/Comun/Archivo/Archivo.service.js",
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/EstadoCaso/EstadoCaso.service.js",
             "~/Scripts/app/Trazabilidad/TipoOportunidad/TipoOportunidad.service.js",
            "~/Scripts/app/Trazabilidad/DatosPreventaMovil/DatosPreventaMovil.service.js",
             "~/Scripts/app/Trazabilidad/CasoOportunidad/CasoOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/MotivoOportunidad/MotivoOportunidad.service.js",
             "~/Scripts/app/Comun/Sector/Sector.service.js",
             "~/Scripts/app/Trazabilidad/FuncionPropietario/FuncionPropietario.service.js",
             "~/Scripts/app/Trazabilidad/Cliente/Cliente.service.js",
             "~/Scripts/app/Trazabilidad/CargaExcelDatosPreventaMovil/CargaExcelDatosPreventaMovil.controller.js",
             "~/Scripts/app/Trazabilidad/CargaExcelDatosPreventaMovil/CargaExcelDatosPreventaMovil.service.js",
             "~/Scripts/app/Trazabilidad/DatosPreventaMovil/DatosPreventaMovil.service.js",
             "~/Scripts/app/Comun/Etapa/Etapa.service.js",
             "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
              "~/Scripts/app/Trazabilidad/TipoSolicitud/TipoSolicitud.service.js",
             "~/Scripts/app/Comun/SegmentosNegocios/SegmentosNegocios.service.js",
             "~/Scripts/app/Trazabilidad/OportunidadST/OportunidadST.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarDatosPreventaMovil/RegistrarDatosPreventaMovil.service.js",
            "~/Scripts/app/Trazabilidad/RegistrarDatosPreventaMovil/RegistrarDatosPreventaMovil.controller.js",
             "~/Scripts/app/Trazabilidad/BandejaDatosPreventaMovil/BandejaDatosPreventaMovil.service.js",
             "~/Scripts/app/Trazabilidad/BandejaDatosPreventaMovil/BandejaDatosPreventaMovil.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaDatosPostventaMovil/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/EstadoCaso/EstadoCaso.service.js",
             "~/Scripts/app/Trazabilidad/TipoOportunidad/TipoOportunidad.service.js",
            "~/Scripts/app/Trazabilidad/DatosPreventaMovil/DatosPreventaMovil.service.js",
             "~/Scripts/app/Trazabilidad/CasoOportunidad/CasoOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/MotivoOportunidad/MotivoOportunidad.service.js",
             "~/Scripts/app/Comun/Sector/Sector.service.js",
             "~/Scripts/app/Trazabilidad/FuncionPropietario/FuncionPropietario.service.js",
             "~/Scripts/app/Trazabilidad/Cliente/Cliente.service.js",
             "~/Scripts/app/Trazabilidad/DatosPostventaMovil/DatosPostventaMovil.service.js",
             "~/Scripts/app/Comun/Etapa/Etapa.service.js",
             "~/Scripts/app/Comun/SegmentosNegocios/SegmentosNegocios.service.js",
             "~/Scripts/app/Trazabilidad/OportunidadST/OportunidadST.service.js",
             "~/Scripts/app/Trazabilidad/BandejaDatosPostventaMovil/BandejaDatosPostventaMovil.service.js",
             "~/Scripts/app/Trazabilidad/BandejaDatosPostventaMovil/BandejaDatosPostventaMovil.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RptOportunidades/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/EstadoCaso/EstadoCaso.service.js",
             "~/Scripts/app/Trazabilidad/TipoOportunidad/TipoOportunidad.service.js",
             "~/Scripts/app/Comun/Etapa/Etapa.service.js",
             "~/Scripts/app/Comun/SegmentosNegocios/SegmentosNegocios.service.js",
             "~/Scripts/app/Trazabilidad/RptOportunidades/RptOportunidades.controller.js",
             "~/Scripts/app/Trazabilidad/RptOportunidades/RptOportunidades.service.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RptQuiebresOportunidades/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
             "~/Scripts/app/Comun/Etapa/Etapa.service.js",
             "~/Scripts/app/Comun/SegmentosNegocios/SegmentosNegocios.service.js",
             "~/Scripts/app/Comun/ConceptoSeguimiento/ConceptoSeguimiento.service.js",
             "~/Scripts/app/Trazabilidad/RptQuiebresOportunidades/RptQuiebresOportunidades.controller.js",
             "~/Scripts/app/Trazabilidad/RptQuiebresOportunidades/RptQuiebresOportunidades.service.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RptQuiebres/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
             "~/Scripts/app/Comun/Etapa/Etapa.service.js",
             "~/Scripts/app/Comun/SegmentosNegocios/SegmentosNegocios.service.js",
             "~/Scripts/app/Comun/ConceptoSeguimiento/ConceptoSeguimiento.service.js",
             "~/Scripts/app/Trazabilidad/RptQuiebresOportunidades/RptQuiebresOportunidades.controller.js",
             "~/Scripts/app/Trazabilidad/RptQuiebresOportunidades/RptQuiebresOportunidades.service.js",
             "~/Scripts/app/Trazabilidad/RptQuiebres/RptQuiebres.controller.js",
             "~/Scripts/app/Trazabilidad/RptQuiebres/RptQuiebres.service.js",
             "~/Scripts/app/Trazabilidad/RptQuiebresGrilla/RptQuiebresGrilla.controller.js",
             "~/Scripts/app/Trazabilidad/RptQuiebresGrilla/RptQuiebresGrilla.service.js"
            ));



            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RptQuiebresGrilla/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
             "~/Scripts/app/Comun/Etapa/Etapa.service.js",
             "~/Scripts/app/Comun/SegmentosNegocios/SegmentosNegocios.service.js",
             "~/Scripts/app/Trazabilidad/RptQuiebresGrilla/RptQuiebresGrilla.controller.js",
             "~/Scripts/app/Trazabilidad/RptQuiebresGrilla/RptQuiebresGrilla.service.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RptFCVGrilla/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
             "~/Scripts/app/Comun/Etapa/Etapa.service.js",
             "~/Scripts/app/Comun/Recurso/Recurso.service.js",
             "~/Scripts/app/Trazabilidad/RptFCVGrillaBusquedaRecurso/RptFCVGrillaBusquedaRecurso.controller.js",
             "~/Scripts/app/Trazabilidad/RptFCVGrillaBusquedaRecurso/RptFCVGrillaBusquedaRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RptFCVGrilla/RptFCVGrilla.controller.js",
             "~/Scripts/app/Trazabilidad/RptFCVGrilla/RptFCVGrilla.service.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RptFCV/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
             "~/Scripts/app/Comun/Etapa/Etapa.service.js",
             "~/Scripts/app/Comun/Recurso/Recurso.service.js",
             "~/Scripts/app/Trazabilidad/RptFCVGrillaBusquedaRecurso/RptFCVGrillaBusquedaRecurso.controller.js",
             "~/Scripts/app/Trazabilidad/RptFCVGrillaBusquedaRecurso/RptFCVGrillaBusquedaRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RptFCV/RptFCV.controller.js",
             "~/Scripts/app/Trazabilidad/RptFCV/RptFCV.service.js",
             "~/Scripts/app/Trazabilidad/RptFcv2GraficoBarrasBusquedaRecurso/RptFcv2GraficoBarrasBusquedaRecurso.controller.js",
             "~/Scripts/app/Trazabilidad/RptFcv2GraficoBarrasBusquedaRecurso/RptFcv2GraficoBarrasBusquedaRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RptFcv2GraficoBarras/RptFcv2GraficoBarras.controller.js",
             "~/Scripts/app/Trazabilidad/RptFcv2GraficoBarras/RptFcv2GraficoBarras.service.js"



            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RptOportunidadesAtendidas/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
             "~/Scripts/app/Comun/Etapa/Etapa.service.js",
             "~/Scripts/app/Comun/Recurso/Recurso.service.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasBusquedaRecurso/RptOportunidadesAtendidasBusquedaRecurso.controller.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasBusquedaRecurso/RptOportunidadesAtendidasBusquedaRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidas/RptOportunidadesAtendidas.controller.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidas/RptOportunidadesAtendidas.service.js"
            ));



            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RptOportunidadesAtendidasMultipleReporte/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
             "~/Scripts/app/Comun/Etapa/Etapa.service.js",
             "~/Scripts/app/Comun/Recurso/Recurso.service.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasBusquedaRecurso/RptOportunidadesAtendidasBusquedaRecurso.controller.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasBusquedaRecurso/RptOportunidadesAtendidasBusquedaRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasMultipleReporte/RptOportunidadesAtendidasMultipleReporte.controller.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasMultipleReporte/RptOportunidadesAtendidasMultipleReporte.service.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasAcumuladasBusquedaRecurso/RptOportunidadesAtendidasAcumuladasBusquedaRecurso.controller.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasAcumuladasBusquedaRecurso/RptOportunidadesAtendidasAcumuladasBusquedaRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasAcumuladas/RptOportunidadesAtendidasAcumuladas.controller.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasAcumuladas/RptOportunidadesAtendidasAcumuladas.service.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RptOportunidadesAtendidasAcumuladas/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
             "~/Scripts/app/Comun/Etapa/Etapa.service.js",
             "~/Scripts/app/Comun/Recurso/Recurso.service.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasAcumuladasBusquedaRecurso/RptOportunidadesAtendidasAcumuladasBusquedaRecurso.controller.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasAcumuladasBusquedaRecurso/RptOportunidadesAtendidasAcumuladasBusquedaRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasAcumuladas/RptOportunidadesAtendidasAcumuladas.controller.js",
             "~/Scripts/app/Trazabilidad/RptOportunidadesAtendidasAcumuladas/RptOportunidadesAtendidasAcumuladas.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RptFcv2GraficoBarras/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
             "~/Scripts/app/Comun/Etapa/Etapa.service.js",
             "~/Scripts/app/Trazabilidad/RptFcv2GraficoBarrasBusquedaRecurso/RptFcv2GraficoBarrasBusquedaRecurso.controller.js",
             "~/Scripts/app/Trazabilidad/RptFcv2GraficoBarrasBusquedaRecurso/RptFcv2GraficoBarrasBusquedaRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RptFcv2GraficoBarras/RptFcv2GraficoBarras.controller.js",
             "~/Scripts/app/Trazabilidad/RptFcv2GraficoBarras/RptFcv2GraficoBarras.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaTiempoAtencionEquipo/js").Include(
              "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
              "~/Scripts/app/Comun/EquipoTrabajo/EquipoTrabajo.service.js",
              "~/Scripts/app/Trazabilidad/BandejaTiempoAtencionEquipo/BandejaTiempoAtencionEquipo.service.js",
              "~/Scripts/app/Trazabilidad/BandejaTiempoAtencionEquipo/BandejaTiempoAtencionEquipo.controller.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarTiempoAtencionEquipo/js").Include(
              "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
              "~/Scripts/app/Comun/EquipoTrabajo/EquipoTrabajo.service.js",
              "~/Scripts/app/Trazabilidad/OportunidadST/OportunidadST.service.js",
             "~/Scripts/app/Trazabilidad/CasoOportunidad/CasoOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/TiempoAtencionEquipoBusquedaOportunidadCaso/TiempoAtencionEquipoBusquedaOportunidadCaso.service.js",
             "~/Scripts/app/Trazabilidad/TiempoAtencionEquipoBusquedaOportunidadCaso/TiempoAtencionEquipoBusquedaOportunidadCaso.controller.js",
              "~/Scripts/app/Trazabilidad/RegistrarTiempoAtencionEquipo/RegistrarTiempoAtencionEquipo.service.js",
              "~/Scripts/app/Trazabilidad/RegistrarTiempoAtencionEquipo/RegistrarTiempoAtencionEquipo.controller.js"
             ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/reporteDashboardTrazabilidad/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
                "~/Scripts/app/Trazabilidad/DashboardTrazabilidad/DashboardTrazabilidad.controller.js"
                ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RptSeguimientoPostventa/js").Include(
                 "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
                 "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
                 "~/Scripts/app/Trazabilidad/RptSeguimientoPostventa/RptSeguimientoPostventa.controller.js",
                 "~/Scripts/app/Trazabilidad/RptSeguimientoPostventa/RptSeguimientoPostventa.service.js"
                ));

        }
    }
}