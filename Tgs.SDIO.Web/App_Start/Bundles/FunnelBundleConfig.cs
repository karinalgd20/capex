﻿using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public class FunnelBundleConfig
    {
        public static void RegisterFunnelBundleConfig(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/reporteMadurez/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
                "~/Scripts/app/Comun/Sector/Sector.service.js",
                "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptMadurez/ReporteMadurez.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/ReportIngresoSectorPeriodo/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptIngresoSectorPeriodo/ReportIngresoSectorPeriodo.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/funnel/reportIngresoLineaNegocioPeriodo/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptIngresoLineaNegocioPeriodo/ReportIngresoLineaNegocioPeriodo.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/reporteProbabilidadPorMes/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptProbabilidadPorMes/ReporteProbabilidadPorMes.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/funnel/ReporteEvolutivoOportunidad/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptEvolutivoOportunidad/ReporteEvolutivoOportunidad.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/funnel/ReporteOfertasCliente/js").Include(
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/ReporteOfertasCliente.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/reporteLineaNegocioPeriodo/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptLineaNegocioPeriodo/ReporteLineaNegocioPeriodo.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/ReporteOfertasEstadoPeriodo/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
                "~/Scripts/app/Comun/Sector/Sector.service.js",
                "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptOfertasPorEstadoPeriodo/ReporteOfertasEstadoPeriodo.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/funnel/ReporteOfertasPorSector/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptOfertasPorSector/ReporteOfertasPorSector.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/funnel/Dashboard/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/Dashboard/Dashboard.controller.js",
                "~/Scripts/app/Funnel/Dashboard/Dashboard.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/ReporteEvolutivoOportunidad/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptEvolutivoOportunidad/ReporteEvolutivoOportunidad.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/ReporteCostosOportunidad/js").Include(
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Funnel/Funnel.module.js",
               "~/Scripts/app/Funnel/RptCostosOportunidad/ReporteCostosOportunidad.controller.js"
           ));

           bundles.Add(new ScriptBundle("~/bundles/app/funnel/reporteDashboardFunnelConsoilidado/js").Include(
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Comun/Recurso/Recurso.service.js",
               "~/Scripts/app/Funnel/Funnel.module.js",
               "~/Scripts/app/Funnel/DashboardFunnelConsolidado/DashboardFunnelConsolidado.controller.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/PreventaOportunidadGerente/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptPreventaOportunidadesGerente/RptPreventaOportunidadesGerente.controller.js",
                "~/Scripts/app/Funnel/RptPreventaOportunidadesGerente/RptPreventaOportunidadesGerente.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/PreventaOportunidadLider/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptPreventaOportunidadesLider/RptPreventaOportunidadesLider.controller.js",
                "~/Scripts/app/Funnel/RptPreventaOportunidadesLider/RptPreventaOportunidadesLider.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/PreventaOportunidadPreventa/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptPreventaOportunidadesPreventa/RptPreventaOportunidadesPreventa.controller.js",
                "~/Scripts/app/Funnel/RptPreventaOportunidadesPreventa/RptPreventaOportunidadesPreventa.service.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/rptOportunidadSectorGerentePreventa/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptOportunidadSectorGerentePreventa/RptOportunidadSectorGerentePreventa.controller.js",
                "~/Scripts/app/Funnel/RptOportunidadSectorGerentePreventa/RptOportunidadSectorGerentePreventa.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/RptRentabilidadGerente/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptRentabilidadGerente/RptRentabilidadGerente.controller.js",
                "~/Scripts/app/Funnel/RptRentabilidadGerente/RptRentabilidadGerente.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/RptOportunidadesPorLineaNegocio/js").Include(
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Funnel/Funnel.module.js",
               "~/Scripts/app/Funnel/RptOportunidadesPorLineaNegocio/RptOportunidadesPorLineaNegocio.controller.js",
               "~/Scripts/app/Funnel/RptOportunidadesPorLineaNegocio/RptOportunidadesPorLineaNegocio.service.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/RptMatrizEscalabilidadRentabilidad/js").Include(
              "~/Scripts/app/Comun/Comun.module.js",
              "~/Scripts/app/Funnel/Funnel.module.js",
              "~/Scripts/app/Funnel/RptMatrizEscalabilidadRentabilidad/RptMatrizEscalabilidadRentabilidad.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/RptProbabilidadFechaCierreController/js").Include(
             "~/Scripts/app/Comun/Comun.module.js",
             "~/Scripts/app/Funnel/Funnel.module.js",
             "~/Scripts/app/Funnel/RptProbabilidadFechaCierre/RptProbabilidadFechaCierre.controller.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/RptProbabilidadPorMesPopupController/js").Include(
             "~/Scripts/app/Comun/Comun.module.js",
             "~/Scripts/app/Funnel/Funnel.module.js",
             "~/Scripts/app/Funnel/RptProbabilidadPorMesPopup/RptProbabilidadPorMesPopup.controller.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/PvcOfertaSectorController/js").Include(
            "~/Scripts/app/Comun/Comun.module.js",
            "~/Scripts/app/Funnel/Funnel.module.js",
            "~/Scripts/app/Funnel/PvcOfertaSector/PvcOfertaSector.controller.js"
          ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/CargabilidadLineaNegocioController/js").Include(
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Funnel/Funnel.module.js",
               "~/Scripts/app/Funnel/RptCargabilidadLineaNegocio/RptCargabilidadLineaNegocio.controller.js"
          ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/PayBackVsFCVController/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/PayBackVsFCV/PayBackVsFCV.controller.js"
           ));

           bundles.Add(new ScriptBundle("~/bundles/app/funnel/RptProductoDashboard/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptProductoDashboard.controller.js"
             ));

        }
    }
}

