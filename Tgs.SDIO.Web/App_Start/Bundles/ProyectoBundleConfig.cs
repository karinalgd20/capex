﻿using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public static class ProyectoBundleConfig
    {
        public static void RegisterProyectoBundleConfig(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/app/Proyecto/Bandeja/js").Include(
                "~/Scripts/app/Proyecto/Proyecto.module.js",
                "~/Scripts/app/Proyecto/BandejaProyecto/BandejaProyecto.controller.js",
                "~/Scripts/app/Proyecto/BandejaProyecto/BandejaProyecto.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Proyecto/Registro/js").Include(
                "~/Scripts/app/Proyecto/Proyecto.module.js",
                "~/Scripts/app/Proyecto/RegistroProyecto/RegistroProyecto.controller.js",
                "~/Scripts/app/Proyecto/RegistroProyecto/RegistroProyecto.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaServicio/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/EtapaOportunidad/EtapaOportunidad.service.js"
            ));
        }
    }
}