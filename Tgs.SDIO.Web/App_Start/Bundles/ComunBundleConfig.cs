﻿using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public class ComunBundleConfig
    {
        public static void RegisterComunBundleConfig(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/app/comun/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Comun/Comun.service.js"
            ));

            //Servicio
            bundles.Add(new ScriptBundle("~/bundles/app/comun/BandejaServicio/js").Include(
                "~/Scripts/app/Comun/Medio/Medio.service.js",
                "~/Scripts/app/Comun/Cliente/Cliente.service.js",
                "~/Scripts/app/Comun/Depreciacion/Depreciacion.service.js",
                "~/Scripts/app/Comun/DireccionComercial/DireccionComercial.service.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Comun/Proveedor/Proveedor.service.js",
                "~/Scripts/app/Comun/Servicio/Servicio.service.js",
                "~/Scripts/app/Comun/ServicioCMI/ServicioCMI.service.js",
                "~/Scripts/app/Comun/SubServicio/SubServicio.service.js",
                "~/Scripts/app/Comun/ServicioSubServicio/ServicioSubServicio.service.js",
                "~/Scripts/app/Comun/ServicioGrupo/ServicioGrupo.service.js",
                "~/Scripts/app/Comun/BandejaServicio/BandejaServicio.controller.js",
                "~/Scripts/app/Comun/BandejaServicio/BandejaServicio.service.js"));

            bundles.Add(new ScriptBundle("~/bundles/app/Comun/BandejaRecurso/js").Include(
             "~/Scripts/app/Comun/Comun.module.js",
              "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
             "~/Scripts/app/Comun/BandejaRecurso/BandejaRecurso.service.js",
             "~/Scripts/app/Comun/BandejaRecurso/BandejaRecurso.controller.js",
             "~/Scripts/app/Comun/Recurso/Recurso.service.js",
             "~/Scripts/app/Comun/Maestra/Maestra.service.js",
             "~/Scripts/app/Comun/Area/Area.service.js",
             "~/Scripts/app/Comun/Empresa/Empresa.service.js",
             "~/Scripts/app/Comun/Cargo/Cargo.service.js",
             "~/Scripts/app/Comun/RolRecurso/RolRecurso.service.js",
              "~/Scripts/app/Comun/RegistrarRecurso/RegistrarRecurso.service.js",
             "~/Scripts/app/Comun/RegistrarRecurso/RegistrarRecurso.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/comun/BandejaSubServicio/js").Include(
            "~/Scripts/app/Comun/Comun/Depreciacion.service.js",
            "~/Scripts/app/Comun/Depreciacion/Depreciacion.service.js",
            "~/Scripts/app/Comun/Maestra/Maestra.service.js",
            "~/Scripts/app/Comun/SubServicio/SubServicio.service.js",
            "~/Scripts/app/Comun/Depreciacion/Depreciacion.service.js",
            "~/Scripts/app/Comun/RegistrarSubServicio/RegistrarSubServicio.service.js",
            "~/Scripts/app/Comun/RegistrarSubServicio/RegistrarSubServicio.controller.js",
            "~/Scripts/app/Comun/BandejaSubServicio/BandejaSubServicio.controller.js",
            "~/Scripts/app/Comun/BandejaSubServicio/BandejaSubServicio.service.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/app/comun/RegistrarSubServicio/js").Include(

        "~/Scripts/app/Comun/RegistrarSubServicio/RegistrarSubServicio.controller.js",
        "~/Scripts/app/Comun/RegistrarSubServicio/RegistrarSubServicio.service.js"
        ));


            //
            bundles.Add(new ScriptBundle("~/bundles/app/comun/casonegocio/js").Include(
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Comun/CasoNegocio/CasoNegocio.controller.js",
               "~/Scripts/app/Comun/CasoNegocio/CasoNegocio.service.js",
               "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
               "~/Scripts/app/Comun/RegistrarCasoNegocio/RegistrarCasoNegocio.controller.js",
               "~/Scripts/app/Comun/RegistrarCasoNegocio/RegistrarCasoNegocio.service.js",
               "~/Scripts/app/Comun/Servicio/Servicio.service.js",
               "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
               "~/Scripts/app/Comun/CasoNegocioServicio/CasoNegocioServicio.service.js"
           ));



            bundles.Add(new ScriptBundle("~/bundles/app/comun/registrocasonegocio/js").Include(
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Comun/RegistrarCasoNegocio/RegistrarCasoNegocio.controller.js",
               "~/Scripts/app/Comun/RegistrarCasoNegocio/RegistrarCasoNegocio.service.js",
               "~/Scripts/app/Comun/Servicio/Servicio.service.js",
               "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
               "~/Scripts/app/Comun/CasoNegocioServicio/CasoNegocioServicio.service.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/Comun/BandejaRolRecurso/js").Include(
             "~/Scripts/app/Comun/Comun.module.js",
             "~/Scripts/app/Comun/RolRecurso/RolRecurso.service.js",
              "~/Scripts/app/Comun/RegistrarRolRecurso/RegistrarRolRecurso.service.js",
               "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
              "~/Scripts/app/Comun/RegistrarRolRecurso/RegistrarRolRecurso.controller.js",
             "~/Scripts/app/Comun/BandejaRolRecurso/BandejaRolRecurso.service.js",
             "~/Scripts/app/Comun/BandejaRolRecurso/BandejaRolRecurso.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Comun/BandejaConceptoSeguimiento/js").Include(
             "~/Scripts/app/Comun/Comun.module.js",
                          "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
             "~/Scripts/app/Comun/ConceptoSeguimiento/ConceptoSeguimiento.service.js",
             "~/Scripts/app/Comun/RegistrarConceptoSeguimiento/RegistrarConceptoSeguimiento.service.js",
             "~/Scripts/app/Comun/RegistrarConceptoSeguimiento/RegistrarConceptoSeguimiento.controller.js",
             "~/Scripts/app/Comun/BandejaConceptoSeguimiento/BandejaConceptoSeguimiento.service.js",
             "~/Scripts/app/Comun/BandejaConceptoSeguimiento/BandejaConceptoSeguimiento.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Comun/BandejaActividad/js").Include(
              "~/Scripts/app/Comun/Actividad/Actividad.service.js",
              "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
             "~/Scripts/app/Comun/BandejaActividad/BandejaActividad.service.js",
              "~/Scripts/app/Comun/BandejaActividad/BandejaActividad.controller.js",
              "~/Scripts/app/Comun/SegmentosNegocios/SegmentosNegocios.service.js",
              "~/Scripts/app/Comun/Etapa/Etapa.service.js",
              "~/Scripts/app/Comun/Fase/Fase.service.js",
              "~/Scripts/app/Comun/AreaSeguimiento/AreaSeguimiento.service.js",
              "~/Scripts/js/lodash.min.js",
              "~/Scripts/app/Comun/RegistrarActividad/RegistrarActividad.service.js",
              "~/Scripts/app/Comun/RegistrarActividad/RegistrarActividad.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Comun/BandejaAreaSeguimiento/js").Include(
             "~/Scripts/js/lodash.min.js",
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Comun/SegmentosNegocios/SegmentosNegocios.service.js",
             "~/Scripts/app/Comun/AreaSeguimiento/AreaSeguimiento.service.js",
              "~/Scripts/Assets/file-upload/ng-file-upload.min.js",
              "~/Scripts/app/Comun/RegistrarAreaSeguimiento/RegistrarAreaSeguimiento.service.js",
              "~/Scripts/app/Comun/RegistrarAreaSeguimiento/RegistrarAreaSeguimiento.controller.js",
             "~/Scripts/app/Comun/BandejaAreaSeguimiento/BandejaAreaSeguimiento.service.js",
             "~/Scripts/app/Comun/BandejaAreaSeguimiento/BandejaAreaSeguimiento.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Comun/Etapa/js").Include(
                "~/Scripts/app/Comun/Etapa/Etapa.service.js"));

            bundles.Add(new ScriptBundle("~/bundles/app/Comun/RiesgoProyecto/js").Include(
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Comun/RiesgoProyecto/RiesgoProyecto.service.js",
                "~/Scripts/app/Comun/BandejaRiesgoProyecto/BandejaRiesgoProyecto.controller.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/comun/RegistrarRiesgoProyecto/js").Include(
                "~/Scripts/app/Comun/RegistrarRiesgoProyecto/RegistrarRiesgoProyecto.controller.js",
                "~/Scripts/app/Comun/RegistrarRiesgoProyecto/RegistrarRiesgoProyecto.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Comun/Cliente/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Comun/Cliente/Cliente.controller.js",
                "~/Scripts/app/Comun/Cliente/Cliente.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Comun/Proveedor/js").Include(
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Comun/Maestra/Maestra.service.js",
               "~/Scripts/app/Comun/Proveedor/Proveedor.controller.js",
               "~/Scripts/app/Comun/Proveedor/Proveedor.service.js",
               "~/Scripts/app/Comun/RegistrarProveedor/RegistrarProveedor.controller.js",
               "~/Scripts/app/Comun/RegistrarProveedor/RegistrarProveedor.service.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/app/Comun/Maestra/js").Include(
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Comun/Maestra/Maestra.controller.js",
               "~/Scripts/app/Comun/Maestra/Maestra.service.js",
               "~/Scripts/app/Comun/RegistrarMaestra/RegistrarMaestra.controller.js",
               "~/Scripts/app/Comun/RegistrarMaestra/RegistrarMaestra.service.js"
               ));
        }
    }
}