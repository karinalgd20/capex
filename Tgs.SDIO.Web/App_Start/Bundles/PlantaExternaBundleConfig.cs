﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public class PlantaExternaBundleConfig
    {


        public static void RegisterPlantaExternaBundleConfig(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/app/PlantaExterna/BandejaNoPeps/js").Include(
             "~/Scripts/app/PlantaExterna/PlantaExterna.module.js",
             "~/Scripts/app/Comun/Comun.module.js",
             "~/Scripts/app/Comun/Comun.service.js",
             "~/Scripts/app/Comun/Maestra/Maestra.service.js",
             "~/Scripts/app/Comun/Cliente/Cliente.service.js",

             "~/Scripts/app/PlantaExterna/BandejaNoPeps/BandejaNoPeps.controller.js",
             "~/Scripts/app/PlantaExterna/BandejaNoPeps/BandejaNoPeps.service.js",
             "~/Scripts/app/PlantaExterna/RegistrarNoPeps/RegistrarNoPeps.controller.js",
             "~/Scripts/app/PlantaExterna/RegistrarNoPeps/RegistrarNoPeps.service.js" 
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/PlantaExterna/ReporteConsolidado/js").Include(
                 "~/Scripts/app/PlantaExterna/PlantaExterna.module.js",
                 "~/Scripts/app/Comun/Comun.module.js",
                 "~/Scripts/app/Comun/Comun.service.js",
                  "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                    "~/Scripts/js/lodash.min.js",
                 "~/Scripts/app/PlantaExterna/AccionEstrategica/AccionEstrategica.service.js",
                 "~/Scripts/app/PlantaExterna/ReporteConsolidado/ReporteConsolidadoSisegos.controller.js",
                 "~/Scripts/app/PlantaExterna/ReporteConsolidado/ReporteConsolidadoSisegos.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/PlantaExterna/BandejaTipoCambioSisego/js").Include(              
               "~/Scripts/app/PlantaExterna/PlantaExterna.module.js",
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Comun/Maestra/Maestra.service.js",
               "~/Scripts/app/PlantaExterna/BandejaTipoCambioSisego/BandejaTipoCambioSisego.controller.js",
               "~/Scripts/app/PlantaExterna/BandejaTipoCambioSisego/BandejaTipoCambioSisego.service.js",
                "~/Scripts/app/PlantaExterna/RegistrarTipoCambioSisego/RegistrarTipoCambioSisego.controller.js",
               "~/Scripts/app/PlantaExterna/RegistrarTipoCambioSisego/RegistrarTipoCambioSisego.service.js"
          ));

            bundles.Add(new ScriptBundle("~/bundles/app/PlantaExterna/Dashboard/js").Include(
              "~/Scripts/app/PlantaExterna/PlantaExterna.module.js",
              "~/Scripts/app/Comun/Comun.module.js",
              "~/Scripts/app/Comun/Maestra/Maestra.service.js",
              "~/Scripts/app/PlantaExterna/Dashboard/DashboardPlantaExterna.controller.js"
         ));

        }
    }
}