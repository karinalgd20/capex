﻿using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public static class CompraBundleConfig
    {
        public static void RegisterCompraBundleConfig(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/app/Compra/Bandeja/js").Include(
                "~/Scripts/app/Compra/Compra.module.js",
                "~/Scripts/app/Compra/BandejaPeticionCompra/BandejaPeticionCompra.controller.js",
                "~/Scripts/app/Compra/BandejaPeticionCompra/BandejaPeticionCompra.service.js",
                "~/Scripts/app/Compra/RegistroPeticionCompra/RegistroPeticionCompra.controller.js",
                "~/Scripts/app/Compra/RegistroPeticionCompra/RegistroPeticionCompra.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Compra/DetalleCompra/js").Include(
                "~/Scripts/app/Compra/DetalleCompra/DetalleCompra.controller.js",
                "~/Scripts/app/Compra/DetalleCompra/DetalleCompra.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Compra/Soporte/js").Include(
                "~/Scripts/app/Compra/Soporte/Soporte.controller.js",
                "~/Scripts/app/Compra/Soporte/Soporte.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Compra/Aprobaciones/js").Include(
                "~/Scripts/app/Compra/Aprobaciones/Aprobaciones.controller.js",
                "~/Scripts/app/Compra/Aprobaciones/Aprobaciones.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Compra/ClienteProveedor/js").Include(
                "~/Scripts/app/Compra/ClienteProveedor/ClienteProveedor.controller.js",
                "~/Scripts/app/Compra/ClienteProveedor/ClienteProveedor.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Compra/BuscadoresPeticionCompra/js").Include(
                "~/Scripts/app/Compra/BuscadoresPeticionCompra/BuscadorPeticionCompra.controller.js",
                "~/Scripts/app/Compra/BuscadoresPeticionCompra/BuscadorPeticionCompra.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Compra/Gestion/js").Include(
                "~/Scripts/app/Compra/Gestion/Gestion.controller.js",
                "~/Scripts/app/Compra/Gestion/Gestion.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Compra/Anexo/js").Include(
                "~/Scripts/app/Compra/Anexo/Anexo.controller.js",
                "~/Scripts/app/Compra/Anexo/Anexo.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Compra/Modulo/js").Include(
               "~/Scripts/app/Compra/Compra.module.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/Compra/PrePeticionCompra/js").Include(
                "~/Scripts/app/Compra/Compra.module.js",
                "~/Scripts/app/Compra/PrePeticionCompra/PrePeticionCompra.service.js",
                "~/Scripts/app/Compra/PrePeticionCompra/PrePeticionCompra.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Compra/DetallePrePeticionCompra/js").Include(
                "~/Scripts/app/Compra/DetallePrePeticionCompra/DetallePrePeticionCompra.controller.js",
                "~/Scripts/app/Compra/DetallePrePeticionCompra/DetallePrePeticionCompra.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Compra/DetallePrePeticionCompraOrdinaria/js").Include(
                "~/Scripts/app/Compra/DetallePrePeticionCompraOrdinaria/DetallePrePeticionCompraOrdinaria.controller.js",
                "~/Scripts/app/Compra/DetallePrePeticionCompraOrdinaria/DetallePrePeticionCompraOrdinaria.service.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/Compra/BandejaPrePeticionCompra/js").Include(
                "~/Scripts/app/Compra/Compra.module.js",
                "~/Scripts/app/Compra/BandejaPrePeticionCompra/BandejaPrePeticionCompra.service.js",
                "~/Scripts/app/Compra/BandejaPrePeticionCompra/BandejaPrePeticionCompra.controller.js"
            ));
        }
    }
}