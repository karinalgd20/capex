﻿using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public static class OportunidadBundleConfig
    {
        public static void RegisterOportunidadBundleConfig(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/js").Include(
                "~/Scripts/app/Oportunidad/Oportunidad.module.js",
                "~/Scripts/app/Oportunidad/Oportunidad.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Oportunidad/Bandeja/js").Include(
                "~/Scripts/app/Oportunidad/Oportunidad.module.js",
                "~/Scripts/app/Oportunidad/BandejaOportunidad/BandejaOportunidad.controller.js",
                "~/Scripts/app/Oportunidad/BandejaOportunidad/BandejaOportunidad.service.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/app/Oportunidad/BandejaServicio/js").Include(
              "~/Scripts/app/Oportunidad/Oportunidad.module.js",
              "~/Scripts/app/Oportunidad/BandejaServicio/BandejaServicio.controller.js",
              "~/Scripts/app/Oportunidad/BandejaServicio/BandejaServicio.service.js"
              ));


            bundles.Add(new ScriptBundle("~/bundles/app/Oportunidad/BandejaDocumento/js").Include(
                "~/Scripts/app/Oportunidad/Oportunidad.module.js",
                "~/Scripts/app/Oportunidad/BandejaDocumento/BandejaDocumento.controller.js",
                "~/Scripts/app/Oportunidad/BandejaDocumento/BandejaDocumento.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/ecapex/js").Include(
                 "~/Scripts/app/Oportunidad/Oportunidad.module.js",
                 "~/Scripts/app/Oportunidad/Ecapex/Ecapex.controller.js",
                 "~/Scripts/app/Oportunidad/Ecapex/Ecapex.service.js",
                 "~/Scripts/app/Oportunidad/EstudiosEspeciales/EstudiosEspeciales.controller.js",
                 "~/Scripts/app/Oportunidad/EstudiosEspeciales/EstudiosEspeciales.service.js",
                 "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.controller.js",
                 "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.service.js",
                 "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                 "~/Scripts/app/Oportunidad/LineaConceptoCmi/LineaConceptoCmi.service.js",
                 "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.controller.js",
                 "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.service.js",

                "~/Scripts/app/Oportunidad/EquipoSeguridad/EquipoSeguridad.controller.js",
                "~/Scripts/app/Oportunidad/EquipoSeguridad/EquipoSeguridad.service.js",
                 "~/Scripts/app/Oportunidad/Gabinete/Gabinete.controller.js",
                "~/Scripts/app/Oportunidad/Gabinete/Gabinete.service.js",
                 "~/Scripts/app/Oportunidad/Ghz/Ghz.controller.js",
                "~/Scripts/app/Oportunidad/Ghz/Ghz.service.js",
                "~/Scripts/app/Oportunidad/Hardware/Hardware.controller.js",
                "~/Scripts/app/Oportunidad/Hardware/Hardware.service.js",
                "~/Scripts/app/Oportunidad/Medio3G/Medio3G.controller.js",
                "~/Scripts/app/Oportunidad/Medio3G/Medio3G.service.js",
                "~/Scripts/app/Oportunidad/Modems/Modems.controller.js",
                "~/Scripts/app/Oportunidad/Modems/Modems.service.js",
                "~/Scripts/app/Oportunidad/Routers/Routers.controller.js",
                "~/Scripts/app/Oportunidad/Routers/Routers.service.js",
                "~/Scripts/app/Oportunidad/Satelitales/Satelitales.controller.js",
                "~/Scripts/app/Oportunidad/Satelitales/Satelitales.service.js",
                "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOpex.controller.js",
                "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOpex.service.js",
                "~/Scripts/app/Oportunidad/SmartVpn/SmartVpn.controller.js",
                "~/Scripts/app/Oportunidad/SmartVpn/SmartVpn.service.js",
                "~/Scripts/app/Oportunidad/Software/Software.controller.js",
                "~/Scripts/app/Oportunidad/Software/Software.service.js",
                "~/Scripts/app/Oportunidad/SolarWind/SolarWind.controller.js",
                "~/Scripts/app/Oportunidad/SolarWind/SolarWind.service.js",
                "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.controller.js",
                "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.service.js",
                "~/Scripts/app/Comun/Costo/Costo.service.js"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/LineaConceptoCmi/js").Include(
                "~/Scripts/app/Oportunidad/Oportunidad.module.js",
                "~/Scripts/app/Oportunidad/LineaConceptoCmi/LineaConceptoCmi.service.js",
                "~/Scripts/app/Oportunidad/LineaConceptoCmi/LineaConceptoCmi.controller.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/estudiosEspeciales/js").Include(
                "~/Scripts/app/Oportunidad/Oportunidad.module.js",
                "~/Scripts/app/Oportunidad/EstudiosEspeciales/EstudiosEspeciales.controller.js",
                "~/Scripts/app/Oportunidad/EstudiosEspeciales/EstudiosEspeciales.service.js",
                "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.controller.js",
                "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.service.js",
                "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.controller.js",
                "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.service.js",
                "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.controller.js",
                "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.service.js"
                ).Include("~/bundles/app/oportunidad/LineaConceptoCmi/js"));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/equiposEspeciales/js").Include(
                "~/Scripts/app/Oportunidad/Oportunidad.module.js",
               "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.controller.js",
               "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.service.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/equiposeguridad/js").Include(
               "~/Scripts/app/Oportunidad/Oportunidad.module.js",
               "~/Scripts/app/Oportunidad/EquipoSeguridad.controller.js",
                "~/Scripts/app/Oportunidad/Oportunidad.service.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/gabinete/js").Include(
               "~/Scripts/app/Oportunidad/Oportunidad.module.js",
               "~/Scripts/app/Oportunidad/Gabinete.controller.js",
                "~/Scripts/app/Oportunidad/Oportunidad.service.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/ghz/js").Include(
               "~/Scripts/app/Oportunidad/Oportunidad.module.js",
               "~/Scripts/app/Oportunidad/Ghz.controller.js",
                "~/Scripts/app/Oportunidad/Oportunidad.service.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/hardware/js").Include(
              "~/Scripts/app/Oportunidad/Oportunidad.module.js",
              "~/Scripts/app/Oportunidad/Hardware.controller.js",
               "~/Scripts/app/Oportunidad/Oportunidad.service.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/modems/js").Include(
              "~/Scripts/app/Oportunidad/Oportunidad.module.js",
              "~/Scripts/app/Oportunidad/Modems.controller.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/routers/js").Include(
              "~/Scripts/app/Oportunidad/Oportunidad.module.js",
              "~/Scripts/app/Oportunidad/Routers.controller.js",
               "~/Scripts/app/Oportunidad/Routers.service.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/satelitales/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/Satelitales.controller.js",
              "~/Scripts/app/Oportunidad/Oportunidad.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/smartvpn/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/SmartVpn.controller.js",
              "~/Scripts/app/Oportunidad/Oportunidad.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/software/js").Include(
           "~/Scripts/app/Oportunidad/Oportunidad.module.js",
           "~/Scripts/app/Oportunidad/Software.controller.js",
            "~/Scripts/app/Oportunidad/Oportunidad.service.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/solarwind/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/SolarWind.controller.js",
             "~/Scripts/app/Oportunidad/Oportunidad.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/bandejaproyecto/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/BandejaProyecto/BandejaProyecto.controller.js",
             "~/Scripts/app/Oportunidad/BandejaProyecto/BandejaProyecto.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/caratula/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/Caratula/Caratula.controller.js",
             "~/Scripts/app/Oportunidad/Caratula/Caratula.service.js",
             "~/Scripts/app/Oportunidad/Circuito/Circuito.controller.js",
             "~/Scripts/app/Oportunidad/Circuito/Circuito.service.js",
             "~/Scripts/app/Oportunidad/Medio3G/Medio3G.controller.js",
             "~/Scripts/app/Oportunidad/Medio3G/Medio3G.service.js",
             "~/Scripts/app/Oportunidad/OportunidadServicioCMIService/OportunidadServicioCMIService.controller.js",
             "~/Scripts/app/Oportunidad/OportunidadServicioCMIService/OportunidadServicioCMIService.service.js",
             "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOPEX.controller.js",
             "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOPEX.service.js",
             "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.controller.js",
             "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.service.js",
             "~/Scripts/app/Comun/Medio/Medio.service.js",
             "~/Scripts/app/Comun/Maestra/Maestra.service.js",
             "~/Scripts/app/Oportunidad/SubServicioDatoCaratula/SubServicioDatoCaratula.controller.js",
             "~/Scripts/app/Oportunidad/SubServicioDatoCaratula/SubServicioDatoCaratula.service.js",
             "~/Scripts/app/Comun/Costo/Costo.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/registrarServicio/js").Include(
             "~/Scripts/app/Oportunidad/RegistrarServicio/RegistrarServicio.controller.js",
             "~/Scripts/app/Oportunidad/RegistrarServicio/RegistrarServicio.service.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/circuito/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/Circuito/Circuito.controller.js",
             "~/Scripts/app/Oportunidad/Circuito/Circuito.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/SOPEX/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/SeguridadOPEX/SeguridadOPEX.controller.js",
             "~/Scripts/app/Oportunidad/SeguridadOPEX/SeguridadOPEX.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/flujocaja/js").Include(
            "~/Scripts/app/Oportunidad/Oportunidad.module.js",
            "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.controller.js",
            "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/visita/js").Include(
            "~/Scripts/app/Oportunidad/Oportunidad.module.js",
            "~/Scripts/app/Oportunidad/Visita/Visita.controller.js",
            "~/Scripts/app/Oportunidad/Visita/VisitaEdit.controller.js",
            "~/Scripts/app/Oportunidad/Visita/Visita.service.js",
            "~/Scripts/app/Comun/Maestra/Maestra.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/SISEGO/js").Include(
            "~/Scripts/app/Oportunidad/Oportunidad.module.js",
            "~/Scripts/app/Oportunidad/SISEGO/SISEGO.controller.js",
            "~/Scripts/app/Oportunidad/SISEGO/SISEGOEdit.controller.js",
            "~/Scripts/app/Oportunidad/SISEGO/SISEGO.service.js",
            "~/Scripts/app/Comun/Maestra/Maestra.service.js",
            "~/Scripts/app/Oportunidad/BandejaContacto/BandejaContacto.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/contenedor/js").Include(
            "~/Scripts/app/Oportunidad/Oportunidad.module.js",
            "~/Scripts/app/Comun/Comun.module.js",
            "~/Scripts/app/Comun/Comun.service.js",
            "~/Scripts/app/Comun/Cliente/Cliente.service.js",
            "~/Scripts/app/Comun/Maestra/Maestra.service.js",
            "~/Scripts/app/Comun/Sector/Sector.service.js",
            "~/Scripts/app/Oportunidad/LineaConceptoCmi/LineaConceptoCmi.service.js",
            "~/Scripts/app/Comun/DireccionComercial/DireccionComercial.service.js",
            "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
            "~/Scripts/app/Oportunidad/OportunidadContenedor/OportunidadContenedor.controller.js",
            "~/Scripts/app/Oportunidad/OportunidadContenedor/OportunidadContenedor.service.js",
            //Caratula
            "~/Scripts/app/Oportunidad/Caratula/Caratula.controller.js",
            "~/Scripts/app/Oportunidad/Caratula/Caratula.service.js",
            "~/Scripts/app/Oportunidad/Circuito/Circuito.controller.js",
            "~/Scripts/app/Oportunidad/Circuito/Circuito.service.js",
            "~/Scripts/app/Oportunidad/SeguridadOPEX/SOPEX.controller.js",
            "~/Scripts/app/Oportunidad/SeguridadOPEX/SOPEX.service.js",
            "~/Scripts/app/Oportunidad/Medio3G/Medio3G.controller.js",
            "~/Scripts/app/Oportunidad/Medio3G/Medio3G.service.js",
            "~/Scripts/app/Oportunidad/OportunidadServicioCMI/OportunidadServicioCMI.controller.js",
            "~/Scripts/app/Oportunidad/OportunidadServicioCMI/OportunidadServicioCMI.service.js",
            "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOPEX.controller.js",
            "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOPEX.service.js",
            "~/Scripts/app/Oportunidad/Opex/OPEX.controller.js",
            "~/Scripts/app/Oportunidad/Opex/OPEX.service.js",
            "~/Scripts/app/Oportunidad/RouterSonda/RouterSonda.controller.js",
            "~/Scripts/app/Oportunidad/RouterSonda/RouterSonda.service.js",
            "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.controller.js",
            "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.service.js",
            "~/Scripts/app/Oportunidad/SubServicioDatos/SubServicioDatos.service.js",
            "~/Scripts/app/Comun/Medio/Medio.service.js",
            //
            "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.service.js",
            "~/Scripts/app/Oportunidad/Routers/Routers.service.js",
            "~/Scripts/app/Oportunidad/Ecapex/Ecapex.controller.js",
            "~/Scripts/app/Oportunidad/Ecapex/Ecapex.service.js",
            "~/Scripts/app/Oportunidad/EstudiosEspeciales/EstudiosEspeciales.controller.js",
            "~/Scripts/app/Oportunidad/EstudiosEspeciales/EstudiosEspeciales.service.js",

            "~/Scripts/app/Oportunidad/RegistrarOportunidad/RegistrarOportunidad.controller.js",
            "~/Scripts/app/Oportunidad/RegistrarOportunidad/RegistrarOportunidad.service.js",
            "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.controller.js",
            "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.service.js",
            "~/Scripts/app/Comun/CasoNegocio/CasoNegocio.service.js",

            "~/Scripts/app/Oportunidad/EquipoSeguridad/EquipoSeguridad.controller.js",
            "~/Scripts/app/Oportunidad/EquipoSeguridad/EquipoSeguridad.service.js",
             "~/Scripts/app/Oportunidad/Gabinete/Gabinete.controller.js",
            "~/Scripts/app/Oportunidad/Gabinete/Gabinete.service.js",
             "~/Scripts/app/Oportunidad/Ghz/Ghz.controller.js",
            "~/Scripts/app/Oportunidad/Ghz/Ghz.service.js",
            "~/Scripts/app/Oportunidad/Hardware/Hardware.controller.js",
            "~/Scripts/app/Oportunidad/Hardware/Hardware.service.js",
            "~/Scripts/app/Oportunidad/Medio3G/Medio3G.controller.js",
            "~/Scripts/app/Oportunidad/Medio3G/Medio3G.service.js",
            "~/Scripts/app/Oportunidad/Modems/Modems.controller.js",
            "~/Scripts/app/Oportunidad/Modems/Modems.service.js",
            "~/Scripts/app/Oportunidad/Routers/Routers.controller.js",
            "~/Scripts/app/Oportunidad/Routers/Routers.service.js",
            "~/Scripts/app/Oportunidad/Satelitales/Satelitales.controller.js",
            "~/Scripts/app/Oportunidad/Satelitales/Satelitales.service.js",
            "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOpex.controller.js",
            "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOpex.service.js",
            "~/Scripts/app/Oportunidad/SmartVpn/SmartVpn.controller.js",
            "~/Scripts/app/Oportunidad/SmartVpn/SmartVpn.service.js",
            "~/Scripts/app/Oportunidad/Software/Software.controller.js",
            "~/Scripts/app/Oportunidad/Software/Software.service.js",
            "~/Scripts/app/Oportunidad/SolarWind/SolarWind.controller.js",
            "~/Scripts/app/Oportunidad/SolarWind/SolarWind.service.js",
            "~/Scripts/app/Oportunidad/FContable/FContable.controller.js",
            "~/Scripts/app/Oportunidad/FContable/FContable.service.js",
            "~/Scripts/app/Oportunidad/FFinanciera/FFinanciera.controller.js",
            "~/Scripts/app/Oportunidad/FFinanciera/FFinanciera.service.js",
            "~/Scripts/app/Oportunidad/ConfiguracionAdicional/ConfiguracionAdicional.controller.js",
            "~/Scripts/app/Oportunidad/ConfiguracionAdicional/ConfiguracionAdicional.service.js",
            "~/Scripts/app/Oportunidad/ConfiguracionAdicional/ConfiguracionAdicionalEdit.controller.js",
            "~/Scripts/app/Oportunidad/SubServicioDatos/SubServicioDatos.service.js",
            "~/bundles/app/comun/maestra/js",
            "~/Scripts/app/Oportunidad/RegistrarOportunidad/RegistrarOportunidad.controller.js",
            "~/Scripts/app/Oportunidad/RegistrarOportunidad/RegistrarOportunidad.service.js",
            "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.controller.js",
            "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.service.js",
            "~/Scripts/app/Oportunidad/SubServicioDatoCaratula/SubServicioDatoCaratula.controller.js",
            "~/Scripts/app/Oportunidad/SubServicioDatoCaratula/SubServicioDatoCaratula.service.js",
            "~/Scripts/app/Oportunidad/RegistrarServicio/RegistrarServicio.controller.js",
            "~/Scripts/app/Oportunidad/RegistrarServicio/RegistrarServicio.service.js",
            "~/Scripts/app/Oportunidad/SituacionActual/SituacionActual.controller.js",
            "~/Scripts/app/Oportunidad/SituacionActual/SituacionActual.service.js",
            "~/Scripts/app/Oportunidad/OTE/OTE.controller.js",
            "~/Scripts/app/Oportunidad/OTE/OTE.service.js",
            "~/Scripts/app/Oportunidad/Riesgos/Riesgos.controller.js",
            "~/Scripts/app/Oportunidad/Riesgos/RiesgosEdit.controller.js",
            "~/Scripts/app/Oportunidad/Riesgos/Riesgos.service.js",
            "~/Scripts/app/Oportunidad/Visita/Visita.controller.js",
            "~/Scripts/app/Oportunidad/Visita/VisitaEdit.controller.js",
            "~/Scripts/app/Oportunidad/Visita/Visita.service.js",
            "~/Scripts/app/Oportunidad/SISEGO/SISEGO.controller.js",
            "~/Scripts/app/Oportunidad/SISEGO/SISEGO.service.js",
            "~/Scripts/app/Oportunidad/Sede/Sede.controller.js",
            "~/Scripts/app/Oportunidad/Sede/SedeEdit.controller.js",
            "~/Scripts/app/Oportunidad/Sede/Sede.service.js",
            "~/Scripts/app/Comun/Ubigeo/Ubigeo.service.js",
            "~/Scripts/app/Oportunidad/AgregarSede/AgregarSede.controller.js",
            "~/Scripts/app/Oportunidad/AgregarSede/AgregarSede.service.js",
            "~/Scripts/app/Oportunidad/RegistrarSede/RegistrarSede.controller.js",
            "~/Scripts/app/Oportunidad/RegistrarSede/RegistrarSede.service.js",
            //SalesForceConsolidadoCabecera
            "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
            "~/Scripts/app/Comun/SalesForceConsolidadoDetalle/SalesForceConsolidadoDetalle.service.js",
            //OportunidadCosto
            "~/Scripts/app/Oportunidad/BandejaCosto/BandejaCosto.controller.js",
            "~/Scripts/app/Oportunidad/BandejaCosto/BandejaCosto.service.js",
               "~/Scripts/app/Oportunidad/RegistrarCosto/RegistrarCosto.controller.js",
               "~/Scripts/app/Oportunidad/RegistrarCosto/RegistrarCosto.service.js",
             //Comun
             "~/Scripts/app/Comun/Costo/Costo.service.js",
             "~/Scripts/app/Comun/ServicioSubServicio/ServicioSubServicio.service.js",
             "~/Scripts/app/Comun/Servicio/Servicio.service.js",
             "~/Scripts/app/Comun/Archivo/Archivo.service.js",
             "~/Scripts/app/Comun/BandejaRiesgoProyecto/BandejaRiesgoProyecto.controller.js",
             "~/Scripts/app/Comun/RegistrarRiesgoProyecto/RegistrarRiesgoProyecto.controller.js",
             "~/Scripts/app/Comun/RegistrarRiesgoProyecto/RegistrarRiesgoProyecto.service.js",
             "~/Scripts/app/Comun/RiesgoProyecto/RiesgoProyecto.service.js",
             "~/Scripts/app/Oportunidad/Contacto/Contacto.service.js",
             "~/Scripts/app/Oportunidad/RegistrarContacto/RegistrarContacto.service.js",
             "~/Scripts/app/Oportunidad/RegistrarContacto/RegistrarContacto.controller.js",
             "~/Scripts/app/Oportunidad/BandejaContacto/BandejaContacto.service.js",
             "~/Scripts/app/Oportunidad/BandejaContacto/BandejaContacto.controller.js",
             "~/Scripts/app/Oportunidad/BandejaEstudios/BandejaEstudios.service.js",
             "~/Scripts/app/Oportunidad/BandejaEstudios/BandejaEstudios.controller.js",
             "~/Scripts/app/Oportunidad/Estudios/Estudios.service.js",


             "~/Scripts/app/Comun/ServicioGrupo/ServicioGrupo.service.js",
              "~/Scripts/app/Comun/SubServicio/SubServicio.service.js" ,
              "~/Scripts/app/Comun/BandejaServicio/BandejaServicio.service.js" , 
            "~/Scripts/app/Oportunidad/RegistrarComponente/RegistrarComponente.controller.js",
            "~/Scripts/app/Oportunidad/RegistrarComponente/RegistrarComponente.service.js",
            "~/Scripts/app/Oportunidad/RegistrarCasoNegocio/RegistrarCasoNegocio.controller.js" ,
              "~/Scripts/app/Oportunidad/RegistrarCasoNegocio/RegistrarCasoNegocio.service.js"   ,
             "~/Scripts/app/Oportunidad/OportunidadFlujoCaja/OportunidadFlujoCaja.service.js",
             "~/Scripts/app/Oportunidad/BandejaServiciosAgrupados/BandejaServiciosAgrupados.service.js",
             "~/Scripts/app/Oportunidad/BandejaServiciosAgrupados/BandejaServiciosAgrupados.controller.js",
             "~/Scripts/app/Oportunidad/ServicioSedeInstalacion/ServicioSedeInstalacion.service.js",
             "~/Scripts/app/Oportunidad/BandejaCotizacion/BandejaCotizacion.service.js",
             "~/Scripts/app/Oportunidad/BandejaCotizacion/BandejaCotizacion.controller.js",
             "~/Scripts/app/Oportunidad/Cotizacion/Cotizacion.service.js",
             "~/Scripts/app/Oportunidad/Indicadores/Indicadores.controller.js",
             "~/Scripts/app/Oportunidad/Indicadores/Indicadores.service.js" ,
              //OportunidadParametro
             "~/Scripts/app/Oportunidad/RegistrarParametro/RegistrarParametro.controller.js",
          "~/Scripts/app/Oportunidad/RegistrarParametro/RegistrarParametro.service.js" 

          ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/bandejaContacto/js").Include(
                "~/Scripts/app/Oportunidad/Contacto/Contacto.service.js",
                "~/Scripts/app/Oportunidad/RegistrarContacto/RegistrarContacto.service.js",
                "~/Scripts/app/Oportunidad/RegistrarContacto/RegistrarContacto.controller.js",
                "~/Scripts/app/Oportunidad/BandejaContacto/BandejaContacto.service.js",
                "~/Scripts/app/Oportunidad/BandejaContacto/BandejaContacto.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/Indicadores/js").Include(
               "~/Scripts/app/Oportunidad/Indicadores/Indicadores.controller.js",
             "~/Scripts/app/Oportunidad/Indicadores/Indicadores.service.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/bandejaPlantillaImplantacion/js").Include(
                "~/Scripts/app/Oportunidad/OportunidadContenedor/OportunidadContenedor.service.js",
                "~/Scripts/app/Comun/Cliente/Cliente.service.js",
                "~/Scripts/app/Comun/Sector/Sector.service.js",
                "~/Scripts/app/Comun/DireccionComercial/DireccionComercial.service.js",
                "~/Scripts/app/Oportunidad/Sede/Sede.service.js",
                "~/Scripts/app/Oportunidad/SISEGO/SISEGO.service.js",
                "~/Scripts/app/Oportunidad/SISEGO/SISEGO.controller.js",
                "~/Scripts/app/Oportunidad/AgregarSede/AgregarSede.service.js",
                "~/Scripts/app/Oportunidad/BandejaContacto/BandejaContacto.service.js",
                "~/Scripts/app/Oportunidad/BandejaEstudios/BandejaEstudios.service.js",
                "~/Scripts/app/Oportunidad/BandejaCotizacion/BandejaCotizacion.service.js",
                "~/Scripts/app/Oportunidad/BandejaServiciosAgrupados/BandejaServiciosAgrupados.service.js",
                "~/Scripts/app/Oportunidad/RegistrarOportunidad/RegistrarOportunidad.service.js",
                "~/Scripts/app/Oportunidad/RegistrarSede/RegistrarSede.controller.js",
                "~/Scripts/app/Oportunidad/BandejaServiciosAgrupados/BandejaServiciosAgrupados.controller.js",
                "~/Scripts/app/Oportunidad/OportunidadFlujoCaja/OportunidadFlujoCaja.service.js",
                "~/Scripts/app/Oportunidad/BandejaContacto/BandejaContacto.controller.js",
                "~/Scripts/app/Oportunidad/BandejaEstudios/BandejaEstudios.controller.js",
                "~/Scripts/app/Oportunidad/BandejaCotizacion/BandejaCotizacion.controller.js",
                "~/Scripts/app/Oportunidad/ServicioSedeInstalacion/ServicioSedeInstalacion.service.js",
                "~/Scripts/app/Oportunidad/Contacto/Contacto.service.js",
                "~/Scripts/app/Oportunidad/RegistrarContacto/RegistrarContacto.service.js",
                "~/Scripts/app/Oportunidad/Estudios/Estudios.service.js",
                "~/Scripts/app/Oportunidad/Cotizacion/Cotizacion.service.js",

                "~/Scripts/app/Oportunidad/BandejaPlantillaImplantacion/BandejaPlantillaImplantacion.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/detallePlantillaImplantacion/js").Include(
                "~/Scripts/app/Oportunidad/OportunidadContenedor/OportunidadContenedor.service.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Comun/ServicioGrupo/ServicioGrupo.service.js",
                "~/Scripts/app/Comun/Medio/Medio.service.js",
                "~/Scripts/app/Comun/Costo/Costo.service.js",
                "~/Scripts/app/Oportunidad/Contacto/Contacto.service.js",
                "~/Scripts/app/Oportunidad/PlantillaImplantacion/PlantillaImplantacion.service.js",
                "~/Scripts/app/Oportunidad/OportunidadFlujoCaja/OportunidadFlujoCaja.service.js",
                "~/Scripts/app/Oportunidad/RegistrarPlantillaImplantacion/RegistrarPlantillaImplantacion.service.js",
                "~/Scripts/app/Oportunidad/RegistrarPlantillaImplantacion/RegistrarPlantillaImplantacion.controller.js",
                "~/Scripts/app/Oportunidad/DetalleSede/DetalleSede.controller.js"
            ));
        }
    }
}