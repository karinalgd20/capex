﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public class CapexBundleConfig
    {
        public static void RegisterCapexBundleConfig(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/app/BandejaSolicitudCapex/js").Include(
                 "~/Scripts/app/Comun/Comun.module.js",
                 "~/Scripts/app/Capex/Capex.module.js",
               "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Comun/Cliente/Cliente.service.js",
               "~/Scripts/app/Capex/BandejaSolicitud/BandejaSolicitud.service.js",
                "~/Scripts/app/Capex/BandejaSolicitud/BandejaSolicitud.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/CapexContenedor/js").Include(
               "~/Scripts/app/Capex/Capex.module.js",
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
               "~/Scripts/app/Capex/CapexContenedor/CapexContenedor.controller.js",
               "~/Scripts/app/Capex/CapexContenedor/CapexContenedor.service.js",
               "~/Scripts/app/Capex/Proyecto/Proyecto.controller.js",
               "~/Scripts/app/Capex/Proyecto/Proyecto.service.js",
               "~/Scripts/app/Capex/RegistrarSolicitud/RegistrarSolicitud.controller.js",
               "~/Scripts/app/Capex/RegistrarSolicitud/RegistrarSolicitud.service.js",
               "~/Scripts/app/Capex/RegistrarProyecto/RegistrarProyecto.controller.js",
               "~/Scripts/app/Capex/RegistrarProyecto/RegistrarProyecto.service.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/RegistrarSolicitud/js").Include(
               "~/Scripts/app/Capex/Capex.module.js",
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
               "~/Scripts/app/Capex/RegistrarSolicitud/RegistrarSolicitud.controller.js",
               "~/Scripts/app/Capex/RegistrarSolicitud/RegistrarSolicitud.service.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/Proyecto/js").Include(
              "~/Scripts/app/Capex/Capex.module.js",
              "~/Scripts/app/Comun/Comun.module.js",
              "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
              "~/Scripts/app/Capex/Proyecto/Proyecto.controller.js",
               "~/Scripts/app/Capex/Proyecto/Proyecto.service.js"
          ));

            bundles.Add(new ScriptBundle("~/bundles/app/RegistrarProyecto/js").Include(
             "~/Scripts/app/Capex/Capex.module.js",
             "~/Scripts/app/Comun/Comun.module.js",
             "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
             "~/Scripts/app/Capex/RegistrarProyecto/RegistrarProyecto.controller.js",
              "~/Scripts/app/Capex/RegistrarProyecto/RegistrarProyecto.service.js"
         ));

        }
    }
}