﻿using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public class SeguridadBundleConfig
    {
        public static void RegisterSeguridadBundleConfig(BundleCollection bundles)
        {
             
            bundles.Add(new ScriptBundle("~/bundles/app/seguridad/login/js").Include(
                        "~/Scripts/app/Seguridad/Seguridad.module.js",
                        "~/Scripts/app/Seguridad/Login/Login.controller.js",
                         "~/Scripts/app/Seguridad/Login/Login.service.js",
                         "~/Scripts/app/Seguridad/CambioClave/CambioClave.controller.js",
                         "~/Scripts/app/Seguridad/CambioClave/CambioClave.service.js",
                         "~/Scripts/app/Seguridad/OlvidoClave/OlvidoClave.controller.js",
                         "~/Scripts/app/Seguridad/OlvidoClave/OlvidoClave.service.js",
                         "~/Scripts/app/Comun/Comun.module.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/app/seguridad/cambioclave/js").Include(
                        "~/Scripts/app/Seguridad/Seguridad.module.js",
                        "~/Scripts/app/Seguridad/CambioClave/CambioClave.controller.js",
                         "~/Scripts/app/Seguridad/CambioClave/CambioClave.service.js",
                         "~/Scripts/app/Comun/Comun.module.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/app/seguridad/olvidoclave/js").Include(
                        "~/Scripts/app/Seguridad/Seguridad.module.js",
                        "~/Scripts/app/Seguridad/OlvidoClave/OlvidoClave.controller.js",
                         "~/Scripts/app/Seguridad/OlvidoClave/OlvidoClave.service.js",
                         "~/Scripts/app/Comun/Comun.module.js"
                        ));
            
            bundles.Add(new ScriptBundle("~/bundles/app/seguridad/bandejaUsuario/js").Include(
                        "~/Scripts/app/Seguridad/Seguridad.module.js",
                        "~/Scripts/app/Seguridad/BandejaUsuario/BandejaUsuario.controller.js",
                        "~/Scripts/app/Seguridad/BandejaUsuario/BandejaUsuario.service.js",
                     
                        "~/Scripts/app/Seguridad/RegistrarUsuario/RegistrarUsuario.controller.js",
                        "~/Scripts/app/Seguridad/RegistrarUsuario/RegistrarUsuario.service.js",

                        "~/Scripts/app/Seguridad/RegistrarUsuarioPerfil/RegistrarUsuarioPerfil.controller.js",
                        "~/Scripts/app/Seguridad/RegistrarUsuarioPerfil/RegistrarUsuarioPerfil.service.js",

                     
                        "~/Scripts/app/Comun/Comun.module.js",
                        "~/Scripts/app/Comun/Area/Area.service.js",
                        "~/Scripts/app/Comun/Cargo/Cargo.service.js",

                        "~/Scripts/app/Comun/RegistrarRecursoLineaNegocio/RegistrarRecursoLineaNegocio.controller.js",
                        "~/Scripts/app/Comun/RegistrarRecursoLineaNegocio/RegistrarRecursoLineaNegocio.service.js",
                        "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
                        "~/Scripts/app/Comun/RegistrarRecursoJefe/RegistrarRecursoJefe.controller.js",
                        "~/Scripts/app/Comun/RegistrarRecursoJefe/RegistrarRecursoJefe.service.js"
                        ));

        
        }
    }
}