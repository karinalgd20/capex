﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public static class NegocioBundleConfig
    {
        public static void RegisterNegocioBundleConfig(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/app/negocio/js").Include(
                "~/Scripts/app/Negocio/Negocio.module.js",
                "~/Scripts/app/Negocio/NgBandejaOportunidades/NgBandejaOportunidades.controller.js",
                "~/Scripts/app/Negocio/NgBandejaOportunidades/NgBandejaOportunidades.service.js",
                "~/Scripts/app/Negocio/NgNuevaOportunidadGanadora/NgNuevaOportunidadGanadora.controller.js",
                "~/Scripts/app/Negocio/NgNuevaOportunidadGanadora/NgNuevaOportunidadGanadora.service.js",
                "~/Scripts/app/Negocio/ProcesarOportunidadGanadora/ProcesarOportunidadGanadora.controller.js",
                "~/Scripts/app/Negocio/ProcesarOportunidadGanadora/ProcesarOportunidadGanadora.service.js",
                "~/Scripts/app/Negocio/ProcesarOportunidadGanadora/ResumenServicioOferta.controller.js",
                "~/Scripts/app/Negocio/ProcesarOportunidadGanadora/ResumenServicioOferta.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/BandejaOportunidades/js").Include(
                "~/Scripts/app/Negocio/Negocio.module.js",
                "~/Scripts/app/Negocio/NgBandejaOportunidades/NgBandejaOportunidades.controller.js",
                "~/Scripts/app/Negocio/NgBandejaOportunidades/NgBandejaOportunidades.service.js"
            ));
        }
    }
}