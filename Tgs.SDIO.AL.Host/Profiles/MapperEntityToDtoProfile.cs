﻿using AutoMapper;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Host.Profiles
{
    public class MapperEntityToDtoProfile : Profile
    {
        public MapperEntityToDtoProfile()
        {
            CreateMap<SalesForceConsolidadoCabecera, SalesForceConsolidadoCabeceraListarDtoResponse>()
                .ForMember(dest => dest.NumeroDelCaso, opt => opt.MapFrom(prop => prop.NumeroDelCaso))
                .ForMember(dest => dest.PropietarioOportunidad, opt => opt.MapFrom(prop => prop.PropietarioOportunidad))
                .ForMember(dest => dest.TipologiaOportunidad, opt => opt.MapFrom(prop => prop.TipologiaOportunidad))
                .ForMember(dest => dest.NombreCliente, opt => opt.MapFrom(prop => prop.NombreCliente));

            #region TRAZABILIDAD MAP
            //CreateMap<Oportunidad, OportunidadDtoResponse>()
            //    .ForMember(dest => dest.IdOportunidad, opt => opt.MapFrom(prop => prop.IdOportunidad))
            //    .ForMember(dest => dest.IdOportunidadPadre, opt => opt.MapFrom(prop => prop.IdOportunidadPadre))
            //    .ForMember(dest => dest.IdOportunidadSF, opt => opt.MapFrom(prop => prop.IdOportunidadSF))
            //    .ForMember(dest => dest.IdOportunidadAux, opt => opt.MapFrom(prop => prop.IdOportunidadAux))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.IdTipoOportunidad, opt => opt.MapFrom(prop => prop.IdTipoOportunidad))
            //    .ForMember(dest => dest.IdMotivoOportunidad, opt => opt.MapFrom(prop => prop.IdMotivoOportunidad))
            //    .ForMember(dest => dest.IdTipoEntidadCliente, opt => opt.MapFrom(prop => prop.IdTipoEntidadCliente))
            //    .ForMember(dest => dest.IdCliente, opt => opt.MapFrom(prop => prop.IdCliente))
            //    .ForMember(dest => dest.IdSegmentoNegocio, opt => opt.MapFrom(prop => prop.IdSegmentoNegocio))
            //    .ForMember(dest => dest.Prioridad, opt => opt.MapFrom(prop => prop.Prioridad))
            //    .ForMember(dest => dest.ProbalidadExito, opt => opt.MapFrom(prop => prop.ProbalidadExito))
            //    .ForMember(dest => dest.PorcentajeCierre, opt => opt.MapFrom(prop => prop.PorcentajeCierre))
            //    .ForMember(dest => dest.PorcentajeCheckList, opt => opt.MapFrom(prop => prop.PorcentajeCheckList))
            //    .ForMember(dest => dest.IdFase, opt => opt.MapFrom(prop => prop.IdFase))
            //    .ForMember(dest => dest.IdEtapa, opt => opt.MapFrom(prop => prop.IdEtapa))
            //    .ForMember(dest => dest.FlgCapexMayor, opt => opt.MapFrom(prop => prop.FlgCapexMayor))
            //    .ForMember(dest => dest.ImporteCapex, opt => opt.MapFrom(prop => prop.ImporteCapex))
            //    .ForMember(dest => dest.FechaApertura, opt => opt.MapFrom(prop => prop.FechaApertura))
            //    .ForMember(dest => dest.FechaCierre, opt => opt.MapFrom(prop => prop.FechaCierre))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));


            //CreateMap<Retorno, RetornoResponseDto>()
            //    .ForMember(dest => dest.ResponseID, opt => opt.MapFrom(prop => prop.ResponseID))
            //    .ForMember(dest => dest.ResponseMSG, opt => opt.MapFrom(prop => prop.ResponseMSG));

            //CreateMap<AreaSeguimientoResponse, AreaSeguimientoResponseDto>();

            //CreateMap<AreaSeguimientoPaginadoResponse, AreaSeguimientoPaginadoResponseDto>()
            //    .ForMember(dest => dest.ListAreaSeguimientoDto, opt => opt.MapFrom(prop => prop.LisAreaSeguimiento))
            //    .ForMember(dest => dest.ListSegmentoNegocioDto, opt => opt.MapFrom(prop => prop.ListSegmentoNegocio))
            //    .ForMember(dest => dest.AreaSeguimientoDto, opt => opt.MapFrom(prop => prop.AreaSeguimiento))
            //    .ForMember(dest => dest.Indice, opt => opt.MapFrom(prop => prop.Indice))
            //    .ForMember(dest => dest.Tamanio, opt => opt.MapFrom(prop => prop.Tamanio))
            //    .ForMember(dest => dest.Total, opt => opt.MapFrom(prop => prop.Total));

            //CreateMap<ComboResponse, ComboResponseDto>()
            //    .ForMember(dest => dest.Codigo, opt => opt.MapFrom(prop => prop.Codigo))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion));


            //CreateMap<SegmentoNegocio, SegmentoNegocioDtoResponse>()
            //    .ForMember(dest => dest.IdSegmentoNegocio, opt => opt.MapFrom(prop => prop.IdSegmentoNegocio))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<AreaSegmentoNegocio, AreaSegmentoNegocioDtoResponse>()
            //    .ForMember(dest => dest.IdAreaSegmentoNegocio, opt => opt.MapFrom(prop => prop.IdAreaSegmentoNegocio))
            //    .ForMember(dest => dest.IdAreaSeguimiento, opt => opt.MapFrom(prop => prop.IdAreaSeguimiento))
            //    .ForMember(dest => dest.IdSegmentoNegocio, opt => opt.MapFrom(prop => prop.IdSegmentoNegocio))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<SeguimientoActividad, SeguimientoActividadDtoResponse>()
            //    .ForMember(dest => dest.IdSeguimiento, opt => opt.MapFrom(prop => prop.IdSeguimiento))
            //    .ForMember(dest => dest.IdOportunidad, opt => opt.MapFrom(prop => prop.IdOportunidad))
            //    .ForMember(dest => dest.IdCaso, opt => opt.MapFrom(prop => prop.IdCaso))
            //    .ForMember(dest => dest.FechaSeguimiento, opt => opt.MapFrom(prop => prop.FechaSeguimiento))
            //    .ForMember(dest => dest.IdAreaSeguimiento, opt => opt.MapFrom(prop => prop.IdAreaSeguimiento))
            //    .ForMember(dest => dest.IdActividad, opt => opt.MapFrom(prop => prop.IdActividad))
            //    .ForMember(dest => dest.IdEstadoEjecucion, opt => opt.MapFrom(prop => prop.IdEstadoEjecucion))
            //    .ForMember(dest => dest.FlgObservaciones, opt => opt.MapFrom(prop => prop.FlgObservaciones))
            //    .ForMember(dest => dest.FlgFilesAdjuntos, opt => opt.MapFrom(prop => prop.FlgFilesAdjuntos))
            //    .ForMember(dest => dest.FechaFinSeguimiento, opt => opt.MapFrom(prop => prop.FechaFinSeguimiento))
            //    .ForMember(dest => dest.IdEstadoCumplimiento, opt => opt.MapFrom(prop => prop.IdEstadoCumplimiento))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<ActividadOportunidad, ActividadOportunidadDtoResponse>()
            //    .ForMember(dest => dest.IdActividad, opt => opt.MapFrom(prop => prop.IdActividad))
            //    .ForMember(dest => dest.IdActividadPadre, opt => opt.MapFrom(prop => prop.IdActividadPadre))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.Predecesoras, opt => opt.MapFrom(prop => prop.Predecesoras))
            //    .ForMember(dest => dest.IdFase, opt => opt.MapFrom(prop => prop.IdFase))
            //    .ForMember(dest => dest.IdEtapa, opt => opt.MapFrom(prop => prop.IdEtapa))
            //    .ForMember(dest => dest.FlgCapexMayor, opt => opt.MapFrom(prop => prop.FlgCapexMayor))
            //    .ForMember(dest => dest.FlgCapexMenor, opt => opt.MapFrom(prop => prop.FlgCapexMenor))
            //    .ForMember(dest => dest.IdAreaSeguimiento, opt => opt.MapFrom(prop => prop.IdAreaSeguimiento))
            //    .ForMember(dest => dest.NumeroDiaCapexMenor, opt => opt.MapFrom(prop => prop.NumeroDiaCapexMenor))
            //    .ForMember(dest => dest.CantidadDiasCapexMenor, opt => opt.MapFrom(prop => prop.CantidadDiasCapexMenor))
            //    .ForMember(dest => dest.NumeroDiaCapexMayor, opt => opt.MapFrom(prop => prop.NumeroDiaCapexMayor))
            //    .ForMember(dest => dest.CantidadDiasCapexMayor, opt => opt.MapFrom(prop => prop.CantidadDiasCapexMayor))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<CasoOportunidad, CasoOportunidadDtoResponse>()
            //    .ForMember(dest => dest.IdCaso, opt => opt.MapFrom(prop => prop.IdCaso))
            //    .ForMember(dest => dest.IdOportunidad, opt => opt.MapFrom(prop => prop.IdOportunidad))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.IdCasoSF, opt => opt.MapFrom(prop => prop.IdCasoSF))
            //    .ForMember(dest => dest.IdCasoPadre, opt => opt.MapFrom(prop => prop.IdCasoPadre))
            //    .ForMember(dest => dest.IdTipoSolicitud, opt => opt.MapFrom(prop => prop.IdTipoSolicitud))
            //    .ForMember(dest => dest.Asunto, opt => opt.MapFrom(prop => prop.Asunto))
            //    .ForMember(dest => dest.Complejidad, opt => opt.MapFrom(prop => prop.Complejidad))
            //    .ForMember(dest => dest.Prioridad, opt => opt.MapFrom(prop => prop.Prioridad))
            //    .ForMember(dest => dest.SolucionCaso, opt => opt.MapFrom(prop => prop.SolucionCaso))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<Cliente, ClienteDtoResponse>()
            //    .ForMember(dest => dest.IdCliente, opt => opt.MapFrom(prop => prop.IdCliente))
            //    .ForMember(dest => dest.CodigoCliente, opt => opt.MapFrom(prop => prop.CodigoCliente))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.IdClienteGrupo, opt => opt.MapFrom(prop => prop.IdClienteGrupo))
            //    .ForMember(dest => dest.Grupo, opt => opt.MapFrom(prop => prop.Grupo))
            //    .ForMember(dest => dest.RUCCliente, opt => opt.MapFrom(prop => prop.RUCCliente))
            //    .ForMember(dest => dest.IdSector, opt => opt.MapFrom(prop => prop.IdSector))
            //    .ForMember(dest => dest.GerenteComercial, opt => opt.MapFrom(prop => prop.GerenteComercial))
            //    .ForMember(dest => dest.IdDireccionComercial, opt => opt.MapFrom(prop => prop.IdDireccionComercial))
            //    .ForMember(dest => dest.Lider, opt => opt.MapFrom(prop => prop.Lider))
            //    .ForMember(dest => dest.EstrategiaMovil, opt => opt.MapFrom(prop => prop.EstrategiaMovil))
            //    .ForMember(dest => dest.EstrategiaFija, opt => opt.MapFrom(prop => prop.EstrategiaFija))
            //    .ForMember(dest => dest.Campania, opt => opt.MapFrom(prop => prop.Campania))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<ConceptoSeguimiento, ConceptoSeguimientoDtoResponse>()
            //    .ForMember(dest => dest.IdConcepto, opt => opt.MapFrom(prop => prop.IdConcepto))
            //    .ForMember(dest => dest.IdConceptoPadre, opt => opt.MapFrom(prop => prop.IdConceptoPadre))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.Nivel, opt => opt.MapFrom(prop => prop.Nivel))
            //    .ForMember(dest => dest.OrdenVisual, opt => opt.MapFrom(prop => prop.OrdenVisual))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<DatosPreventaMovil, DatosPreventaMovilDtoResponse>()
            //    .ForMember(dest => dest.IdOportunidad, opt => opt.MapFrom(prop => prop.IdOportunidad))
            //    .ForMember(dest => dest.IdCaso, opt => opt.MapFrom(prop => prop.IdCaso))
            //    .ForMember(dest => dest.IdOperadorActual, opt => opt.MapFrom(prop => prop.IdOperadorActual))
            //    .ForMember(dest => dest.LineasActuales, opt => opt.MapFrom(prop => prop.LineasActuales))
            //    .ForMember(dest => dest.MesesContratoActual, opt => opt.MapFrom(prop => prop.MesesContratoActual))
            //    .ForMember(dest => dest.RecurrenteMensualActual, opt => opt.MapFrom(prop => prop.RecurrenteMensualActual))
            //    .ForMember(dest => dest.FCVActual, opt => opt.MapFrom(prop => prop.FCVActual))
            //    .ForMember(dest => dest.ArpuServicioActual, opt => opt.MapFrom(prop => prop.ArpuServicioActual))
            //    .ForMember(dest => dest.Lineas, opt => opt.MapFrom(prop => prop.Lineas))
            //    .ForMember(dest => dest.MesesContrato, opt => opt.MapFrom(prop => prop.MesesContrato))
            //    .ForMember(dest => dest.RecurrenteMensualVR, opt => opt.MapFrom(prop => prop.RecurrenteMensualVR))
            //    .ForMember(dest => dest.FCVVR, opt => opt.MapFrom(prop => prop.FCVVR))
            //    .ForMember(dest => dest.ArpuServicioVR, opt => opt.MapFrom(prop => prop.ArpuServicioVR))
            //    .ForMember(dest => dest.FechaPresentacion, opt => opt.MapFrom(prop => prop.FechaPresentacion))
            //    .ForMember(dest => dest.FechaBuenaPro, opt => opt.MapFrom(prop => prop.FechaBuenaPro))
            //    .ForMember(dest => dest.Observaciones, opt => opt.MapFrom(prop => prop.Observaciones))
            //    .ForMember(dest => dest.MontoCapex, opt => opt.MapFrom(prop => prop.MontoCapex))
            //    .ForMember(dest => dest.IdEstadoOportunidad, opt => opt.MapFrom(prop => prop.IdEstadoOportunidad))
            //    .ForMember(dest => dest.FCVMovistar, opt => opt.MapFrom(prop => prop.FCVMovistar))
            //    .ForMember(dest => dest.ArpuTotalMovistar, opt => opt.MapFrom(prop => prop.ArpuTotalMovistar))
            //    .ForMember(dest => dest.VanVai, opt => opt.MapFrom(prop => prop.VanVai))
            //    .ForMember(dest => dest.MesesRecupero, opt => opt.MapFrom(prop => prop.MesesRecupero))
            //    .ForMember(dest => dest.FCVClaro, opt => opt.MapFrom(prop => prop.FCVClaro))
            //    .ForMember(dest => dest.ArpuTotalClaro, opt => opt.MapFrom(prop => prop.ArpuTotalClaro))
            //    .ForMember(dest => dest.FCVEntel, opt => opt.MapFrom(prop => prop.FCVEntel))
            //    .ForMember(dest => dest.ArpuTotalEntel, opt => opt.MapFrom(prop => prop.ArpuTotalEntel))
            //    .ForMember(dest => dest.FCVViettel, opt => opt.MapFrom(prop => prop.FCVViettel))
            //    .ForMember(dest => dest.ArpuTotalViettel, opt => opt.MapFrom(prop => prop.ArpuTotalViettel))
            //    .ForMember(dest => dest.ArpuServicio, opt => opt.MapFrom(prop => prop.ArpuServicio))
            //    .ForMember(dest => dest.ArpuEquipos, opt => opt.MapFrom(prop => prop.ArpuEquipos))
            //    .ForMember(dest => dest.ModalidadEquipo, opt => opt.MapFrom(prop => prop.ModalidadEquipo))
            //    .ForMember(dest => dest.CostoPromedioEquipo, opt => opt.MapFrom(prop => prop.CostoPromedioEquipo))
            //    .ForMember(dest => dest.PorcentajeSubsidio, opt => opt.MapFrom(prop => prop.PorcentajeSubsidio))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<DocumentoAdjunto, DocumentoAdjuntoDtoResponse>()
            //    .ForMember(dest => dest.IdDocAdjunto, opt => opt.MapFrom(prop => prop.IdDocAdjunto))
            //    .ForMember(dest => dest.IdOportunidad, opt => opt.MapFrom(prop => prop.IdOportunidad))
            //    .ForMember(dest => dest.IdCaso, opt => opt.MapFrom(prop => prop.IdCaso))
            //    .ForMember(dest => dest.IdTipoDocumento, opt => opt.MapFrom(prop => prop.IdTipoDocumento))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.IdSeguimiento, opt => opt.MapFrom(prop => prop.IdSeguimiento))
            //    .ForMember(dest => dest.IdObservacion, opt => opt.MapFrom(prop => prop.IdObservacion))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<EtapaOportunidad, EtapaOportunidadDtoResponse>()
            //    .ForMember(dest => dest.IdEtapa, opt => opt.MapFrom(prop => prop.IdEtapa))
            //    .ForMember(dest => dest.IdFase, opt => opt.MapFrom(prop => prop.IdFase))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.OrdenVisual, opt => opt.MapFrom(prop => prop.OrdenVisual))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<FaseOportunidad, FaseOportunidadDtoResponse>()
            //    .ForMember(dest => dest.IdFase, opt => opt.MapFrom(prop => prop.IdFase))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.IdGrupoFase, opt => opt.MapFrom(prop => prop.IdGrupoFase))
            //    .ForMember(dest => dest.OrdenVisual, opt => opt.MapFrom(prop => prop.OrdenVisual))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<FileDocumentoAdjunto, FileDocumentoAdjuntoDtoResponse>()
            //    .ForMember(dest => dest.IdDocAdjunto, opt => opt.MapFrom(prop => prop.IdDocAdjunto))
            //    .ForMember(dest => dest.IdFileDocAdjunto, opt => opt.MapFrom(prop => prop.IdFileDocAdjunto))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.RutaFile, opt => opt.MapFrom(prop => prop.RutaFile))
            //    .ForMember(dest => dest.TipoFile, opt => opt.MapFrom(prop => prop.TipoFile))
            //    .ForMember(dest => dest.Tamanio, opt => opt.MapFrom(prop => prop.Tamanio))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<GrupoFaseOportunidad, GrupoFaseOportunidadDtoResponse>()
            //    .ForMember(dest => dest.IdGrupoFase, opt => opt.MapFrom(prop => prop.IdGrupoFase))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<MotivoOportunidad, MotivoOportunidadDtoResponse>()
            //    .ForMember(dest => dest.IdMotivoOportunidad, opt => opt.MapFrom(prop => prop.IdMotivoOportunidad))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<ObservacionOportunidad, ObservacionOportunidadDtoResponse>()
            //    .ForMember(dest => dest.IdObservacion, opt => opt.MapFrom(prop => prop.IdObservacion))
            //    .ForMember(dest => dest.IdOportunidad, opt => opt.MapFrom(prop => prop.IdOportunidad))
            //    .ForMember(dest => dest.IdCaso, opt => opt.MapFrom(prop => prop.IdCaso))
            //    .ForMember(dest => dest.IdSeguimiento, opt => opt.MapFrom(prop => prop.IdSeguimiento))
            //    .ForMember(dest => dest.FechaSeguimiento, opt => opt.MapFrom(prop => prop.FechaSeguimiento))
            //    .ForMember(dest => dest.IdConcepto, opt => opt.MapFrom(prop => prop.IdConcepto))
            //    .ForMember(dest => dest.Observaciones, opt => opt.MapFrom(prop => prop.Observaciones))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<Recurso, RecursoDtoResponse>()
            //    .ForMember(dest => dest.IdRecurso, opt => opt.MapFrom(prop => prop.IdRecurso))
            //    .ForMember(dest => dest.TipoRecurso, opt => opt.MapFrom(prop => prop.TipoRecurso))
            //    .ForMember(dest => dest.UserNameSF, opt => opt.MapFrom(prop => prop.UserNameSF))
            //    .ForMember(dest => dest.Nombre, opt => opt.MapFrom(prop => prop.Nombre))
            //    .ForMember(dest => dest.DNI, opt => opt.MapFrom(prop => prop.DNI))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));


            //CreateMap<RecursoOportunidad, RecursoOportunidadDtoResponse>()
            //    .ForMember(dest => dest.IdOportunidad, opt => opt.MapFrom(prop => prop.IdOportunidad))
            //    .ForMember(dest => dest.IdAsignacion, opt => opt.MapFrom(prop => prop.IdAsignacion))
            //    .ForMember(dest => dest.IdRecurso, opt => opt.MapFrom(prop => prop.IdRecurso))
            //    .ForMember(dest => dest.IdCaso, opt => opt.MapFrom(prop => prop.IdCaso))
            //    .ForMember(dest => dest.IdRol, opt => opt.MapFrom(prop => prop.IdRol))
            //    .ForMember(dest => dest.PorcentajeDedicacion, opt => opt.MapFrom(prop => prop.PorcentajeDedicacion))
            //    .ForMember(dest => dest.Observaciones, opt => opt.MapFrom(prop => prop.Observaciones))
            //    .ForMember(dest => dest.FInicioAsignacion, opt => opt.MapFrom(prop => prop.FInicioAsignacion))
            //    .ForMember(dest => dest.FFinAsignacion, opt => opt.MapFrom(prop => prop.FFinAsignacion))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<RolRecurso, RolRecursoDtoResponse>()
            //    .ForMember(dest => dest.IdRol, opt => opt.MapFrom(prop => prop.IdRol))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.Nivel, opt => opt.MapFrom(prop => prop.Nivel))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<Sector, SectorDtoResponse>()
            //    .ForMember(dest => dest.IdSector, opt => opt.MapFrom(prop => prop.IdSector))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));

            //CreateMap<TipoOportunidad, TipoOportunidadDtoResponse>()
            //    .ForMember(dest => dest.IdTipoOportunidad, opt => opt.MapFrom(prop => prop.IdTipoOportunidad))
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(prop => prop.Descripcion))
            //    .ForMember(dest => dest.IdEstado, opt => opt.MapFrom(prop => prop.IdEstado))
            //    .ForMember(dest => dest.IdUsuarioCreacion, opt => opt.MapFrom(prop => prop.IdUsuarioCreacion))
            //    .ForMember(dest => dest.FechaCreacion, opt => opt.MapFrom(prop => prop.FechaCreacion))
            //    .ForMember(dest => dest.IdUsuarioEdicion, opt => opt.MapFrom(prop => prop.IdUsuarioEdicion))
            //    .ForMember(dest => dest.FechaEdicion, opt => opt.MapFrom(prop => prop.FechaEdicion));


            #endregion
        }
    }
}

