﻿using SimpleInjector;
using SimpleInjector.Integration.Wcf;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Seguridad;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Implementacion.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Util.Adapter;
using Tgs.SDIO.Util.AutoMapper;
using Tgs.SDIO.Util.Error.Logging.Interfaces;
using Tgs.SDIO.Util.Error.Logging.NLog;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Funnel;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Funnel;
using Tgs.SDIO.AL.DataAccess.Interfaces.Funnel;
using Tgs.SDIO.AL.DataAccess.Implementacion.Funnel;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.CartaFianza;
using Tgs.SDIO.AL.DataAccess.Interfaces.PlantaExterna;
using Tgs.SDIO.AL.DataAccess.Implementacion.PlantaExterna;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.PlantaExterna;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.PlantaExterna;
using Tgs.SDIO.AL.BussinesLayer.Interfaces;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Negocio;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.AL.DataAccess.Implementacion.Negocios;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Proyecto;
using Tgs.SDIO.AL.DataAccess.Implementacion.Proyecto;   
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex;
using Tgs.SDIO.AL.DataAccess.Implementacion.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Implementacion.Compra;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra;


namespace Tgs.SDIO.AL.Host.Configuration
{
    public static class InjectorConfiguration
    {
        public static void ConfigurationContainer()
        {

            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WcfOperationLifestyle();

            container.Register<DioContext, DioContext>(Lifestyle.Scoped);
            container.Register<ITypeAdapterFactory, AutoMapperTypeAdapterFactory>(Lifestyle.Singleton);
            container.Register<ILoggerFactory, NLogLoggingFactory>(Lifestyle.Singleton);

            #region comun 
            container.Register<ISalesForceConsolidadoCabeceraDal, SalesForceConsolidadoCabeceraDal>(Lifestyle.Scoped);
            container.Register<ISalesForceConsolidadoCabeceraBl, SalesForceConsolidadoCabeceraBl>(Lifestyle.Scoped);
            container.Register<IMedioDal, MedioDal>(Lifestyle.Scoped);
            container.Register<ISubServicioDal, SubServicioDal>(Lifestyle.Scoped);
            container.Register<IServicioDal, ServicioDal>(Lifestyle.Scoped);
            container.Register<IServicioSubServicioDal, ServicioSubServicioDal>(Lifestyle.Scoped);
            container.Register<IArchivoDal, ArchivoDal>(Lifestyle.Scoped);
            container.Register<IClienteDal, ClienteDal>(Lifestyle.Scoped);
            container.Register<IBWDal, BWDal>(Lifestyle.Scoped);
            container.Register<ILineaNegocioDal, LineaNegocioDal>(Lifestyle.Scoped);
            container.Register<IDepreciacionDal, DepreciacionDal>(Lifestyle.Scoped);
            container.Register<IDGDal, DGDal>(Lifestyle.Scoped);
            container.Register<IDireccionComercialDal, DireccionComercialDal>(Lifestyle.Scoped);
            container.Register<IFentoceldaDal, FentoceldaDal>(Lifestyle.Scoped);
            container.Register<ILineaProductoDal, LineaProductoDal>(Lifestyle.Scoped);
            container.Register<IMaestraDal, MaestraDal>(Lifestyle.Scoped);
            container.Register<IProveedorDal, ProveedorDal>(Lifestyle.Scoped);
            container.Register<ISectorDal, SectorDal>(Lifestyle.Scoped);
            container.Register<IServicioCMIDal, ServicioCMIDal>(Lifestyle.Scoped);

            container.Register<ICasoNegocioDal, CasoNegocioDal>(Lifestyle.Scoped);
            container.Register<ICasoNegocioServicioDal, CasoNegocioServicioDal>(Lifestyle.Scoped);
            container.Register<ICostoDal, CostoDal>(Lifestyle.Scoped);
            container.Register<IMedioCostoDal, MedioCostoDal>(Lifestyle.Scoped);

            container.Register<IRecursoJefeDal, RecursoJefeDal>(Lifestyle.Scoped);
            container.Register<IRecursoLineaNegocioDal, RecursoLineaNegocioDal>(Lifestyle.Scoped);
            container.Register<ICargoDal, CargoDal>(Lifestyle.Scoped);
            container.Register<IAreaDal, AreaDal>(Lifestyle.Scoped);
            container.Register<IUbigeoDal, UbigeoDal>(Lifestyle.Scoped);
            container.Register<ISalesForceConsolidadoDetalleDal, SalesForceConsolidadoDetalleDal>(Lifestyle.Scoped);
            container.Register<IRiesgoProyectoDal, RiesgoProyectoDal>(Lifestyle.Scoped);
            container.Register<ICorreoDal, CorreoDal>(Lifestyle.Scoped);
            container.Register<IEmpresaDal, EmpresaDal>(Lifestyle.Scoped);
            container.Register<IServicioGrupoDal, ServicioGrupoDal>(Lifestyle.Scoped);
            container.Register<IEquipoTrabajoDal, EquipoTrabajoDal>(Lifestyle.Scoped);
            container.Register<ITipoCambioDal, TipoCambioDal>(Lifestyle.Scoped);
            container.Register<ITipoCambioDetalleDal, TipoCambioDetalleDal>(Lifestyle.Scoped);
            container.Register<ITipoSolucionDal, TipoSolucionDal>(Lifestyle.Scoped);
            container.Register<ITipoSolucionBl, TipoSolucionBl>(Lifestyle.Scoped);
            
			container.Register<IContratoMarcoDal, ContratoMarcoDal>(Lifestyle.Scoped);
			#endregion
            #region Capex 
            container.Register<ISolicitudDal, SolicitudDal>(Lifestyle.Scoped);
            container.Register<IAsignacionCapexTotalDal, AsignacionCapexTotalDal>(Lifestyle.Scoped);
            container.Register<IAsignacionCapexTotalDetalleDal, AsignacionCapexTotalDetalleDal>(Lifestyle.Scoped);
            container.Register<ICapexLineaNegocioDal, CapexLineaNegocioDal>(Lifestyle.Scoped);
            container.Register<IConceptosCapexDal, ConceptosCapexDal>(Lifestyle.Scoped);
            container.Register<ICronogramaDal, CronogramaDal>(Lifestyle.Scoped);
            container.Register<ICronogramaDetalleDal, CronogramaDetalleDal>(Lifestyle.Scoped);
            container.Register<IDiagramaDal, DiagramaDal>(Lifestyle.Scoped);
            container.Register<IDiagramaDetalleDal, DiagramaDetalleDal>(Lifestyle.Scoped);
            container.Register<IEstructuraCostoDal, EstructuraCostoDal>(Lifestyle.Scoped);


            container.Register<IEstructuraCostoGrupoDal, EstructuraCostoGrupoDal>(Lifestyle.Scoped);
            container.Register<IEstructuraCostoGrupoDetalleDal, EstructuraCostoGrupoDetalleDal>(Lifestyle.Scoped);
            container.Register<IGrupoDal, GrupoDal>(Lifestyle.Scoped);
            container.Register<IObservacionDal, ObservacionDal>(Lifestyle.Scoped);
            container.Register<ISeccionDal, SeccionDal>(Lifestyle.Scoped);
            container.Register<ISolicitudCapexDal, SolicitudCapexDal>(Lifestyle.Scoped);
            container.Register<ISolicitudCapexFlujoEstadoDal, SolicitudCapexFlujoEstadoDal>(Lifestyle.Scoped);
            container.Register<ITopologiaDal, TopologiaDal>(Lifestyle.Scoped);
            #endregion
            #region CartaFianza

            container.Register<ICartaFianzaDal, CartaFianzaDal>(Lifestyle.Scoped);
            container.Register<ICartaFianzaColaboradorDetalleDal, CartaFianzaColaboradorDetalleDal>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleAccionDal, CartaFianzaDetalleAccionesDal>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleEstadoDal, CartaFianzaDetalleEstadoDal>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleRecuperoDal, CartaFianzaDetalleRecuperoDal>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleRenovacionDal, CartaFianzaDetalleRenovacionDal>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleSeguimientoDal, CartaFianzaDetalleSeguimientoDal>(Lifestyle.Scoped);

            container.Register<ICartaFianzaBl, CartaFianzaBl>(Lifestyle.Scoped);
            container.Register<ICartaFianzaColaboradorDetalleBl, CartaFianzaColaboradorDetalleBl>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleAccionBl, CartaFianzaDetalleAccionBl>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleEstadoBl, CartaFianzaDetalleEstadoBl>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleRecuperoBl, CartaFianzaDetalleRecuperoBl>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleRenovacionBl, CartaFianzaDetalleRenovacionBl>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleSeguimientoBl, CartaFianzaDetalleSeguimientoBl>(Lifestyle.Scoped);

            #endregion

            #region Oportunidad
            container.Register<IOportunidadFlujoEstadoDal, OportunidadFlujoEstadoDal>(Lifestyle.Scoped);
            container.Register<IOportunidadLineaNegocioDal, OportunidadLineaNegocioDal>(Lifestyle.Scoped);
            container.Register<IOportunidadDal, OportunidadDal>(Lifestyle.Scoped);
            container.Register<IOportunidadParametroDal, OportunidadParametroDal>(Lifestyle.Scoped);
            container.Register<IConceptoDal, ConceptoDal>(Lifestyle.Scoped);
            container.Register<IConceptoDatosCapexDal, ConceptoDatosCapexDal>(Lifestyle.Scoped);
            container.Register<ISubServicioDatosCaratulaDal, SubServicioDatosCaratulaDal>(Lifestyle.Scoped);
            container.Register<ILineaConceptoCMIDal, LineaConceptoCMIDal>(Lifestyle.Scoped);
            container.Register<ILineaConceptoGrupoDal, LineaConceptoGrupoDal>(Lifestyle.Scoped);
            container.Register<ILineaPestanaDal, LineaPestanaDal>(Lifestyle.Scoped);
            container.Register<ILineaProductoCMIDal, LineaProductoCMIDal>(Lifestyle.Scoped);
            container.Register<IPestanaGrupoDal, PestanaGrupoDal>(Lifestyle.Scoped);
            container.Register<IProyectoLineaConceptoProyectadoDal, ProyectoLineaConceptoProyectadoDal>(Lifestyle.Scoped);
            container.Register<IProyectoLineaProductoDal, ProyectoLineaProductoDal>(Lifestyle.Scoped);
            container.Register<IProyectoServicioCMIDal, ProyectoServicioCMIDal>(Lifestyle.Scoped);
            container.Register<IProyectoServicioConceptoDal, ProyectoServicioConceptoDal>(Lifestyle.Scoped);
            container.Register<IServicioConceptoDocumentoDal, ServicioConceptoDocumentoDal>(Lifestyle.Scoped);
            container.Register<IServicioConceptoProyectadoDal, ServicioConceptoProyectadoDal>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoCajaDetalleDal, OportunidadFlujoCajaDetalleDal>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoCajaDal, OportunidadFlujoCajaDal>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoCajaConfiguracionDal, OportunidadFlujoCajaConfiguracionDal>(Lifestyle.Scoped);
            container.Register<IOportunidadServicioCMIDal, OportunidadServicioCMIDal>(Lifestyle.Scoped);
            container.Register<ISubServicioDatosCapexDal, SubServicioDatosCapexDal>(Lifestyle.Scoped);
            container.Register<IOportunidadDocumentoDal, OportunidadDocumentoDal>(Lifestyle.Scoped);
            container.Register<IOportunidadTipoCambioDal, OportunidadTipoCambioDal>(Lifestyle.Scoped);
            container.Register<IOportunidadTipoCambioDetalleDal, OportunidadTipoCambioDetalleDal>(Lifestyle.Scoped);
            container.Register<IOportunidadCostoDal, OportunidadCostoDal>(Lifestyle.Scoped);
            container.Register<ISituacionActualDal, SituacionActualDal>(Lifestyle.Scoped);
            container.Register<IVisitaDal, VisitaDal>(Lifestyle.Scoped);
            container.Register<IVisitaDetalleDal, VisitaDetalleDal>(Lifestyle.Scoped);
            container.Register<IPestanaGrupoTipoDal, PestanaGrupoTipoDal>(Lifestyle.Scoped);
            container.Register<ISedeDal, SedeDal>(Lifestyle.Scoped);
            container.Register<ISedeInstalacionDal, SedeInstalacionDal>(Lifestyle.Scoped);
            container.Register<ILogEstadoSisegoDal, LogEstadoSisegoDal>(Lifestyle.Scoped);
            container.Register<IOportunidadCalculadoDal, OportunidadCalculadoDal>(Lifestyle.Scoped);
            container.Register<IOportunidadIndicadorFinancieroDal, OportunidadIndicadorFinancieroDal>(Lifestyle.Scoped);

            container.Register<IPestanaGrupoTipoBl, PestanaGrupoTipoBl>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoEstadoBl, OportunidadFlujoEstadoBl>(Lifestyle.Scoped);
            container.Register<IOportunidadLineaNegocioBl, OportunidadLineaNegocioBl>(Lifestyle.Scoped);
            container.Register<IOportunidadBl, OportunidadBl>(Lifestyle.Scoped);
            container.Register<IOportunidadParametroBl, OportunidadParametroBl>(Lifestyle.Scoped);
            container.Register<IConceptoBl, ConceptoBl>(Lifestyle.Scoped);
            container.Register<IConceptoDatosCapexBl, ConceptoDatosCapexBl>(Lifestyle.Scoped);
            container.Register<ISubServicioDatosCaratulaBl, SubServicioDatosCaratulaBl>(Lifestyle.Scoped);
            container.Register<ILineaConceptoCMIBl, LineaConceptoCMIBl>(Lifestyle.Scoped);
            container.Register<ILineaConceptoGrupoBl, LineaConceptoGrupoBl>(Lifestyle.Scoped);
            container.Register<ILineaPestanaBl, LineaPestanaBl>(Lifestyle.Scoped);
            container.Register<ILineaProductoCMIBl, LineaProductoCMIBl>(Lifestyle.Scoped);
            container.Register<IPestanaGrupoBl, PestanaGrupoBl>(Lifestyle.Scoped);
            container.Register<IProyectoLineaConceptoProyectadoBl, ProyectoLineaConceptoProyectadoBl>(Lifestyle.Scoped);
            container.Register<IProyectoLineaProductoBl, ProyectoLineaProductoBl>(Lifestyle.Scoped);
            container.Register<IProyectoServicioCMIBl, ProyectoServicioCMIBl>(Lifestyle.Scoped);
            container.Register<IProyectoServicioConceptoBl, ProyectoServicioConceptoBl>(Lifestyle.Scoped);
            container.Register<IServicioConceptoDocumentoBl, ServicioConceptoDocumentoBl>(Lifestyle.Scoped);
            container.Register<IServicioConceptoProyectadoBl, ServicioConceptoProyectadoBl>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoCajaBl, OportunidadFlujoCajaBl>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoCajaConfiguracionBl, OportunidadFlujoCajaConfiguracionBl>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoCajaDetalleBl, OportunidadFlujoCajaDetalleBl>(Lifestyle.Scoped);
            container.Register<IOportunidadServicioCMIBl, OportunidadServicioCMIBl>(Lifestyle.Scoped);                                                 
            container.Register<ISubServicioDatosCapexBl, SubServicioDatosCapexBl>(Lifestyle.Scoped);
            container.Register<IOportunidadDocumentoBl, OportunidadDocumentoBl>(Lifestyle.Scoped);
            container.Register<IOportunidadTipoCambioBl, OportunidadTipoCambioBl>(Lifestyle.Scoped);
            container.Register<IOportunidadTipoCambioDetalleBl, OportunidadTipoCambioDetalleBl>(Lifestyle.Scoped);
            container.Register<IOportunidadCostoBl, OportunidadCostoBl>(Lifestyle.Scoped);
            container.Register<ISituacionActualBl, SituacionActualBl>(Lifestyle.Scoped);
            container.Register<IVisitaBl, VisitaBl>(Lifestyle.Scoped);
            container.Register<IVisitaDetalleBl, VisitaDetalleBl>(Lifestyle.Scoped);
            container.Register<ISedeBl, SedeBl>(Lifestyle.Scoped);
            container.Register<IContactoDal, ContactoDal>(Lifestyle.Scoped);
            container.Register<IContactoBl, ContactoBl>(Lifestyle.Scoped);
            container.Register<IEstudiosDal, EstudiosDal>(Lifestyle.Scoped);
            container.Register<IEstudiosBl, EstudiosBl>(Lifestyle.Scoped);
            container.Register<IServicioSedeInstalacionDal, ServicioSedeInstalacionDal>(Lifestyle.Scoped);
            container.Register<IServicioSedeInstalacionBl, ServicioSedeInstalacionBl>(Lifestyle.Scoped);
            container.Register<ICotizacionDal, CotizacionDal>(Lifestyle.Scoped);
            container.Register<ICotizacionBl, CotizacionBl>(Lifestyle.Scoped);
            container.Register<IOportunidadIndicadorFinancieroBl, OportunidadIndicadorFinancieroBl>(Lifestyle.Scoped);
            container.Register<IPlantillaImplantacionDal, PlantillaImplantacionDal>(Lifestyle.Scoped);
            container.Register<IPlantillaImplantacionBl, PlantillaImplantacionBl>(Lifestyle.Scoped);

            #endregion

            #region Proyecto

            container.Register<IProyectoDal, ProyectoDal>(Lifestyle.Scoped);
            container.Register<IDetalleSolucionDal, DetalleSolucionDal>(Lifestyle.Scoped);
            container.Register<IDetalleFacturarDal, DetalleFacturarDal>(Lifestyle.Scoped);

            container.Register<IProyectoBl, ProyectoBl>(Lifestyle.Scoped);
            container.Register<IDetalleSolucionBl, DetalleSolucionBl>(Lifestyle.Scoped);
            container.Register<IDetalleFacturarBl, DetalleFacturarBl>(Lifestyle.Scoped);

            container.Register<IDetalleFinancieroDal, DetalleFinancieroDal>(Lifestyle.Scoped);
            container.Register<IDetalleFinancieroBl, DetalleFinancieroBl>(Lifestyle.Scoped);

            container.Register<IDetallePlazoDal, DetallePlazoDal>(Lifestyle.Scoped);
            container.Register<IDetallePlazoBl, DetallePlazoBl>(Lifestyle.Scoped);

            container.Register<IRecursoProyectoDal, RecursoProyectoDal>(Lifestyle.Scoped);
            container.Register<IRecursoProyectoBl, RecursoProyectoBl>(Lifestyle.Scoped);

            container.Register<IDocumentacionArchivoDal, DocumentacionArchivoDal>(Lifestyle.Scoped);
            container.Register<IDocumentacionArchivoBl, DocumentacionArchivoBl>(Lifestyle.Scoped);

            container.Register<IDocumentacionProyectoDal, DocumentacionProyectoDal>(Lifestyle.Scoped);
            container.Register<IDocumentacionProyectoBl, DocumentacionProyectoBl>(Lifestyle.Scoped);

            container.Register<IServicioCMIFinancieroDal, ServicioCMIFinancieroDal>(Lifestyle.Scoped);
            container.Register<IServicioCMIFinancieroBl, ServicioCMIFinancieroBl>(Lifestyle.Scoped);
            #endregion  

			#region Compra

            container.Register<IDetalleClienteProveedorDal, DetalleClienteProveedorDal>(Lifestyle.Scoped);
            container.Register<IDetalleClienteProveedorBl, DetalleClienteProveedorBl>(Lifestyle.Scoped);
            container.Register<ICentroCostoRecursoDal, CentroCostoRecursoDal>(Lifestyle.Scoped);
            container.Register<ICentroCostoRecursoBl, CentroCostoRecursoBl>(Lifestyle.Scoped);
            container.Register<IPeticionCompraDal, PeticionCompraDal>(Lifestyle.Scoped);
            container.Register<IPeticionCompraBl, PeticionCompraBl>(Lifestyle.Scoped);
            container.Register<ICuentaDal, CuentaDal>(Lifestyle.Scoped);
            container.Register<ICuentaBl, CuentaBl>(Lifestyle.Scoped);
            container.Register<IDetalleCompraDal, DetalleCompraDal>(Lifestyle.Scoped);
            container.Register<IDetalleCompraBl, DetalleCompraBl>(Lifestyle.Scoped);
            container.Register<IGestionDal, GestionDal>(Lifestyle.Scoped);
            container.Register<IGestionBl, GestionBl>(Lifestyle.Scoped);
            container.Register<IAnexoDal, AnexoDal>(Lifestyle.Scoped);
            container.Register<IAnexoBl, AnexoBl>(Lifestyle.Scoped);
            container.Register<IAnexoArchivoDal, AnexoArchivoDal>(Lifestyle.Scoped);
            container.Register<IAnexoArchivoBl, AnexoArchivoBl>(Lifestyle.Scoped);

            container.Register<IEtapaPeticionCompraDal, EtapaPeticionCompraDal>(Lifestyle.Scoped);
            container.Register<IEtapaPeticionCompraBl, EtapaPeticionCompraBl>(Lifestyle.Scoped);

            container.Register<IConfiguracionEtapaDal, ConfiguracionEtapaDal>(Lifestyle.Scoped);
            container.Register<IConfiguracionEtapaBl, ConfiguracionEtapaBl>(Lifestyle.Scoped);

            container.Register<IPrePeticionCabeceraDal, PrePeticionCabeceraDal>(Lifestyle.Scoped);
            container.Register<IPrePeticionCabeceraBl, PrePeticionCabeceraBl>(Lifestyle.Scoped);

            container.Register<IPrePeticionDCMDetalleDal, PrePeticionDCMDetalleDal>(Lifestyle.Scoped);
            container.Register<IPrePeticionDCMDetalleBl, PrePeticionDCMDetalleBl>(Lifestyle.Scoped);

            container.Register<IPrePeticionOrdinariaDetalleDal, PrePeticionOrdinariaDetalleDal>(Lifestyle.Scoped);
            container.Register<IPrePeticionOrdinariaDetalleBl, PrePeticionOrdinariaDetalleBl>(Lifestyle.Scoped);
            #endregion
            #region trazabilidad conteiners

            container.Register<IAreaSeguimientoDal, AreaSeguimientoDal>(Lifestyle.Scoped);
            container.Register<IAreaSeguimientoBl, AreaSeguimientoBl>(Lifestyle.Scoped);
            container.Register<ISegmentoNegocioDal, SegmentoNegocioDal>(Lifestyle.Scoped);
            container.Register<ISegmentoNegocioBl, SegmentoNegocioBl>(Lifestyle.Scoped);
            container.Register<IAreaSegmentoNegocioDal, AreaSegmentoNegocioDal>(Lifestyle.Scoped);
            container.Register<IAreaSegmentoNegocioBl, AreaSegmentoNegocioBl>(Lifestyle.Scoped);
            container.Register<ITiempoAtencionEquipoDal, TiempoAtencionEquipoDal>(Lifestyle.Scoped);
            container.Register<ITiempoAtencionEquipoBl, TiempoAtencionEquipoBl>(Lifestyle.Scoped);

            container.Register<IRecursoDal, RecursoDal>(Lifestyle.Scoped);
            container.Register<IRecursoBl, RecursoBl>(Lifestyle.Scoped);
            container.Register<IRolRecursoDal, RolRecursoDal>(Lifestyle.Scoped);
            container.Register<IRolRecursoBl, RolRecursoBl>(Lifestyle.Scoped);
            container.Register<IConceptoSeguimientoDal, ConceptoSeguimientoDal>(Lifestyle.Scoped);
            container.Register<IConceptoSeguimientoBl, ConceptoSeguimientoBl>(Lifestyle.Scoped);
            container.Register<IActividadDal, ActividadDal>(Lifestyle.Scoped);
            container.Register<IActividadBl, ActividadBl>(Lifestyle.Scoped);
            container.Register<IEtapaDal, EtapaDal>(Lifestyle.Scoped);
            container.Register<IEtapaBl, EtapaBl>(Lifestyle.Scoped);
            container.Register<IFaseDal, FaseDal>(Lifestyle.Scoped);
            container.Register<IFaseBl, FaseBl>(Lifestyle.Scoped);
            container.Register<IActividadSegmentoNegocioDal, ActividadSegmentoNegocioDal>(Lifestyle.Scoped);


            container.Register<IRecursoOportunidadDal, RecursoOportunidadDal>(Lifestyle.Scoped);
            container.Register<IRecursoOportunidadBl, RecursoOportunidadBl>(Lifestyle.Scoped);
            container.Register<IObservacionOportunidadDal, ObservacionOportunidadDal>(Lifestyle.Scoped);
            container.Register<IObservacionOportunidadBl, ObservacionOportunidadBl>(Lifestyle.Scoped);
            container.Register<IRecursoEquipoTrabajoDal, RecursoEquipoTrabajoDal>(Lifestyle.Scoped);
            container.Register<IRecursoEquipoTrabajoBl, RecursoEquipoTrabajoBl>(Lifestyle.Scoped);
            container.Register<IOportunidadSTDal, OportunidadSTDal>(Lifestyle.Scoped);
            container.Register<IOportunidadSTBl, OportunidadSTBl>(Lifestyle.Scoped);
            container.Register<ICasoOportunidadDal, CasoOportunidadDal>(Lifestyle.Scoped);
            container.Register<ICasoOportunidadBl, CasoOportunidadBl>(Lifestyle.Scoped);
            container.Register<IDatosPreventaMovilDal, DatosPreventaMovilDal>(Lifestyle.Scoped);
            container.Register<IDatosPreventaMovilBl, DatosPreventaMovilBl>(Lifestyle.Scoped);
            container.Register<IEstadoCasoDal, EstadoCasoDal>(Lifestyle.Scoped);
            container.Register<IEstadoCasoBl, EstadoCasoBl>(Lifestyle.Scoped);
            container.Register<ITipoOportunidadDal, TipoOportunidadDal>(Lifestyle.Scoped);
            container.Register<ITipoOportunidadBl, TipoOportunidadBl>(Lifestyle.Scoped);
            container.Register<IMotivoOportunidadDal, MotivoOportunidadDal>(Lifestyle.Scoped);
            container.Register<IMotivoOportunidadBl, MotivoOportunidadBl>(Lifestyle.Scoped);
            container.Register<IFuncionPropietarioDal, FuncionPropietarioDal>(Lifestyle.Scoped);
            container.Register<IFuncionPropietarioBl, FuncionPropietarioBl>(Lifestyle.Scoped);

            container.Register<IEtapaOportunidadDal, EtapaOportunidadDal>(Lifestyle.Scoped);
            container.Register<IEtapaOportunidadBl, EtapaOportunidadBl>(Lifestyle.Scoped);
            container.Register<ITipoSolicitudDal, TipoSolicitudDal>(Lifestyle.Scoped);
            container.Register<ITipoSolicitudBl, TipoSolicitudBl>(Lifestyle.Scoped);
            #endregion


            container.Register<IOpcionMenuBl, OpcionMenuBl>(Lifestyle.Scoped);
            container.Register<IPerfilBl, PerfilBl>(Lifestyle.Scoped);
            container.Register<IUsuarioBl, UsuarioBl>(Lifestyle.Scoped);
            container.Register<IUsuarioPerfilBl, UsuarioPerfilBl>(Lifestyle.Scoped);


            #region comun BL
            container.Register<ITipoCambioBl, TipoCambioBl>(Lifestyle.Scoped);
            container.Register<ITipoCambioDetalleBl, TipoCambioDetalleBl>(Lifestyle.Scoped);
            container.Register<IMedioBl, MedioBl>(Lifestyle.Scoped);
            container.Register<IServicioBl, ServicioBl>(Lifestyle.Scoped);

            container.Register<ISubServicioBl, SubServicioBl>(Lifestyle.Scoped);
            container.Register<IServicioSubServicioBl, ServicioSubServicioBl>(Lifestyle.Scoped);

            container.Register<IClienteBl, ClienteBl>(Lifestyle.Scoped);
            container.Register<IArchivoBl, ArchivoBl>(Lifestyle.Scoped);
            container.Register<IBWBl, BWBl>(Lifestyle.Scoped);
            container.Register<ILineaNegocioBl, LineaNegocioBl>(Lifestyle.Scoped);
            container.Register<IDepreciacionBl, DepreciacionBl>(Lifestyle.Scoped);
            container.Register<IDGBl, DGBl>(Lifestyle.Scoped);
            container.Register<IDireccionComercialBl, DireccionComercialBl>(Lifestyle.Scoped);
            container.Register<IFentoceldaBl, FentoceldaBl>(Lifestyle.Scoped);
            container.Register<ILineaProductoBl, LineaProductoBl>(Lifestyle.Scoped);
            container.Register<IMaestraBl, MaestraBl>(Lifestyle.Scoped);
            container.Register<IProveedorBl, ProveedorBl>(Lifestyle.Scoped);
            container.Register<ISectorBl, SectorBl>(Lifestyle.Scoped);
            container.Register<IServicioCMIBl, ServicioCMIBl>(Lifestyle.Scoped);

            container.Register<ICasoNegocioBl, CasoNegocioBl>(Lifestyle.Scoped);
            container.Register<ICasoNegocioServicioBl, CasoNegocioServicioBl>(Lifestyle.Scoped);
            container.Register<ICostoBl, CostoBl>(Lifestyle.Scoped);
            container.Register<IMedioCostoBl, MedioCostoBl>(Lifestyle.Scoped);

            container.Register<IRecursoJefeBl, RecursoJefeBl>(Lifestyle.Scoped);
            container.Register<IRecursoLineaNegocioBl, RecursoLineaNegocioBl>(Lifestyle.Scoped);
            container.Register<ICargoBl, CargoBl>(Lifestyle.Scoped);
            container.Register<IAreaBl, AreaBl>(Lifestyle.Scoped);
            container.Register<IUbigeoBl, UbigeoBl>(Lifestyle.Scoped);
            container.Register<ISalesForceConsolidadoDetalleBl, SalesForceConsolidadoDetalleBl>(Lifestyle.Scoped);
            container.Register<IRiesgoProyectoBl, RiesgoProyectoBl>(Lifestyle.Scoped);
            container.Register<ICorreoBl, CorreoBl>(Lifestyle.Scoped);
            container.Register<IEmpresaBl, EmpresaBl>(Lifestyle.Scoped);
            container.Register<IServicioGrupoBl, ServicioGrupoBl>(Lifestyle.Scoped);
            container.Register<IEquipoTrabajoBl, EquipoTrabajoBl>(Lifestyle.Scoped);

			container.Register<IContratoMarcoBl, ContratoMarcoBl>(Lifestyle.Scoped);

            #endregion
            #region Capex 
            container.Register<ISolicitudBl, SolicitudBl>(Lifestyle.Scoped);
            container.Register<IAsignacionCapexTotalBl, AsignacionCapexTotalBl>(Lifestyle.Scoped);
            container.Register<IAsignacionCapexTotalDetalleBl, AsignacionCapexTotalDetalleBl>(Lifestyle.Scoped);
            container.Register<ICapexLineaNegocioBl, CapexLineaNegocioBl>(Lifestyle.Scoped);
            container.Register<IConceptosCapexBl, ConceptosCapexBl>(Lifestyle.Scoped);
            container.Register<ICronogramaBl, CronogramaBl>(Lifestyle.Scoped);
            container.Register<ICronogramaDetalleBl, CronogramaDetalleBl>(Lifestyle.Scoped);
            container.Register<IDiagramaBl, DiagramaBl>(Lifestyle.Scoped);
            container.Register<IDiagramaDetalleBl, DiagramaDetalleBl>(Lifestyle.Scoped);
            container.Register<IEstructuraCostoBl, EstructuraCostoBl>(Lifestyle.Scoped);

            container.Register<IEstructuraCostoGrupoBl, EstructuraCostoGrupoBl>(Lifestyle.Scoped);
            container.Register<IEstructuraCostoGrupoDetalleBl, EstructuraCostoGrupoDetalleBl>(Lifestyle.Scoped);
            container.Register<IGrupoBl, GrupoBl>(Lifestyle.Scoped);
            container.Register<IObservacionBl, ObservacionBl>(Lifestyle.Scoped);
            container.Register<ISeccionBl, SeccionBl>(Lifestyle.Scoped);
            container.Register<ISolicitudCapexBl, SolicitudCapexBl>(Lifestyle.Scoped);
            container.Register<ISolicitudCapexFlujoEstadoBl, SolicitudCapexFlujoEstadoBl>(Lifestyle.Scoped);
            container.Register<ITopologiaBl, TopologiaBl>(Lifestyle.Scoped);
            #endregion
            #region Funnel BL

            container.Register<IOportunidadFinancieraOrigenBl, OportunidadFinancieraOrigenBl>(Lifestyle.Scoped);
            container.Register<IOportunidadFinancieraOrigenLineaNegocioBl, OportunidadFinancieraOrigenLineaNegocioBl>(Lifestyle.Scoped);

            #endregion Funnel BL

            #region Funnel DA

            container.Register<IOportunidadFinancieraOrigenDal, OportunidadFinancieraOrigenDal>(Lifestyle.Scoped);
            container.Register<IOportunidadFinancieraOrigenLineaNegocioDal, OportunidadFinancieraOrigenLineaNegocioDal>(Lifestyle.Scoped);

            #endregion Funnel DA

            #region Negocio BL
            container.Register<IIsisNroOfertaBl, IsisNroOfertaBl>(Lifestyle.Scoped);
            container.Register<ISisegoCotizadoBl, SisegoCotizadoBl>(Lifestyle.Scoped);
            container.Register<IOportunidadGanadoraBl, OportunidadGanadoraBl>(Lifestyle.Scoped);
            container.Register<IAsociarNroOfertaSisegosBl, AsociarNroOfertaSisegosBl>(Lifestyle.Scoped);
            container.Register<IAsociarServicioSisegosBl, AsociarServicioSisegosBl>(Lifestyle.Scoped);
            container.Register<IRPAServiciosxNroOfertaBl, RPAServiciosxNroOfertaBl>(Lifestyle.Scoped);
            container.Register<IRPAEquiposServicioBl, RPAEquiposServicioBl>(Lifestyle.Scoped);
            container.Register<IRPASisegoDetalleBl, RPASisegoDetalleBl>(Lifestyle.Scoped);
            container.Register<IRPASisegoDatosContactoBl, RPASisegoDatosContactoBl>(Lifestyle.Scoped);
            #endregion

            #region Negocio DA
            container.Register<IIsisNroOfertaDal, IsisNroOfertaDal>(Lifestyle.Scoped);
            container.Register<ISisegoCotizadoDal, SisegoCotizadoDal>(Lifestyle.Scoped);
            container.Register<IOportunidadGanadoraDal, OportunidadGanadoraDal>(Lifestyle.Scoped);
            container.Register<IAsociarNroOfertaSisegosDal, AsociarNroOfertaSisegosDal>(Lifestyle.Scoped);
            container.Register<IAsociarServicioSisegosDal, AsociarServicioSisegosDal> (Lifestyle.Scoped);
            container.Register<IRPAServiciosxNroOfertaDal, RPAServiciosxNroOfertaDal>(Lifestyle.Scoped);
            container.Register<IRPAEquiposServicioDal, RPAEquiposServicioDal>(Lifestyle.Scoped);
            container.Register<IRPASisegoDetalleDal, RPASisegoDetalleDal>(Lifestyle.Scoped);
            container.Register<IRPASisegoDatosContactoDal, RPASisegoDatosContactoDal>(Lifestyle.Scoped);

            #endregion

            #region PlantaExterna

            container.Register<INoPepsDal, NoPepsDal>(Lifestyle.Scoped);

            container.Register<INoPepsBl, NoPepsBl>(Lifestyle.Scoped);

            container.Register<ITipoCambioSisegoDal, TipoCambioSisegoDal>(Lifestyle.Scoped);

            container.Register<IAccionEstrategicaDal, AccionEstrategicaDal>(Lifestyle.Scoped);

            container.Register<ITipoCambioSisegoBl, TipoCambioSisegoBl>(Lifestyle.Scoped);

            container.Register<IAccionEstrategicaBl, AccionEstrategicaBl>(Lifestyle.Scoped);

            #endregion

            container.Verify();
            SimpleInjectorServiceHostFactory.SetContainer(container);

            var automapperTypeAdapterFactory = container.GetInstance<ITypeAdapterFactory>();
            TypeAdapterFactory.SetCurrent(automapperTypeAdapterFactory);

            var nlogLoggingFactory = container.GetInstance<ILoggerFactory>();
            LoggerFactory.SetCurrent(nlogLoggingFactory);
        }
    }
}