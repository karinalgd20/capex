﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Web;

namespace Tgs.SDIO.AL.Host.Utils
{
    public class ErrorBehavior : BehaviorExtensionElement
    {
        private const string NombreProperty = "nombre";
        private const string HabilitadoProperty = "habilitado";
        private const string RutaProperty = "ruta";

        [ConfigurationProperty(NombreProperty, DefaultValue = "Errores", IsRequired = true)]
        public string Nombre
        {
            get
            {
                return (string)base[NombreProperty];
            }
            set
            {
                base[NombreProperty] = value;
            }
        }

        [ConfigurationProperty(HabilitadoProperty, DefaultValue = true, IsRequired = true)]
        public bool Habilitado
        {
            get
            {
                return (bool)base[HabilitadoProperty];
            }
            set
            {
                base[HabilitadoProperty] = value;
            }
        }

        [ConfigurationProperty(RutaProperty, DefaultValue = "", IsRequired = true)]
        public string Ruta
        {
            get
            {
                return (string)base[RutaProperty];
            }
            set
            {
                base[RutaProperty] = value;
            }
        }

        #region BehaviorExtensionElement
        public override Type BehaviorType
        {
            get
            {
                return typeof(ErrorServiceBehavior);
            }
        }

        protected override object CreateBehavior()
        {
            return new ErrorServiceBehavior(Nombre, Habilitado);
        }
        #endregion
    }
}