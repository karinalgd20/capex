﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Web;
using Tgs.SDIO.Util.Error.Logging;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Error.Logging.Interfaces;

namespace Tgs.SDIO.AL.Host.Utils
{
    public class ErrorHandler : IErrorHandler
    {
        public bool Habilitado { get; set; }
        public string Nombre { get; set; }

        public ErrorHandler(string nombre, bool habilitado)
        {
            this.Habilitado = habilitado;
            this.Nombre = nombre;
        }

        public bool HandleError(Exception error)
        {
            if (Habilitado && !(error is FaultException))
            {
                var logger = LoggerFactory.CreateLog(Nombre);
                if (error is AppException)
                    logger.LogError(((AppException)error).MensajeDefault, error);
                else
                    logger.LogError(error.Message, error);
            }
            return true;
        }

        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            if (error is FaultException)
                return;
            var errorDto = FactoryError.ConstruirError(error);
            if (errorDto != null)
            {
                Type faultType = typeof(FaultException<>).MakeGenericType(errorDto.GetType());
                var faultReason = new FaultReason(errorDto.Mensaje);
                var faultCode = FaultCode.CreateReceiverFaultCode(new FaultCode(errorDto.IdError.ToString()));
                var faultException = (FaultException)Activator.CreateInstance(faultType, errorDto, faultReason, faultCode);

                var messageFault = faultException.CreateMessageFault();
                fault = Message.CreateMessage(version, messageFault, faultException.Action);
            }
        }
    }
}