﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Seguridad;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Implementacion.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.UnitTest.Seguridad
{
    [TestClass]
    public class UsuarioBlTest
    {

        DioContext dbContext;
        IPerfilBl iPerfilBl;
        IRecursoBl iRecursoBl;         
        IRecursoDal iRecursoDal;
        IUsuarioBl IUsuarioBl;

        [TestInitialize()]
        public void IniciarTest()
        {
            dbContext = new DioContext();

            iRecursoDal = new RecursoDal(dbContext);
            iPerfilBl = new PerfilBl();
            iRecursoBl = new RecursoBl(iRecursoDal);
            IUsuarioBl = new UsuarioBl(iPerfilBl, iRecursoBl);
            iRecursoBl = new RecursoBl(iRecursoDal);
        }

        [TestMethod]
        public void ActualizarUsuario_ActualizarUsuarioPersona_RespuestaOk()
        {
            UsuarioDtoRequest plantilla = new UsuarioDtoRequest
            {
                Nombres= "LUIS",
                Apellidos="IBAZETA",
                CorreoElectronico="",
                IntentosFallidos=1,
                FechaCaducidad= DateTime.Now,
                SolicitaCambioClave=true, 
                IdUsuarioEdicion =13,
                IdArea=1,
                IdCargo=1,
                IdUsuario=13, 
                IdEstado=Estados.Activo
            };

            var resultado = IUsuarioBl.ActualizarUsuario(plantilla);

            Assert.IsTrue(resultado.TipoRespuesta==0);
        }


    }
}
