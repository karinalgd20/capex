﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Util.Web.Proyecto.Conversiones
{
    public class ExcelToDataTable
    {

        public static DataTable ReadExcelXLSXToDataTable(string ArchivoLectura, string separdor, int tipoArchivo = -1, bool bTieneNombreHoja = false)
        {
            var dtMetadata = new DataTable();

            var package = new ExcelPackage(new FileInfo(ArchivoLectura));
            package.File.IsReadOnly = true;

            string sLibroExcel = ConfigurationManager.AppSettings["LibroExcel"];
            ExcelWorksheet workSheet = null;

            if (bTieneNombreHoja)
            {
                workSheet = package.Workbook.Worksheets[sLibroExcel];
            }
            else
            {
                workSheet = package.Workbook.Worksheets[1];
            }

            var maxColumnNumber = 0;
            var inicioFila = 1;

            if (tipoArchivo == 1)
            {
                inicioFila = 4;
                maxColumnNumber = 132;
            }
            else if (tipoArchivo == 2)
            {
                inicioFila = 4;
                maxColumnNumber = 139;
            }
            else
            {
                inicioFila = 1;
                maxColumnNumber = workSheet.Dimension.End.Column;
            }

            var totalRowCount = workSheet.Dimension.End.Row;

            int iColumnasExistentes = 1;
            for (var j = 0; j < maxColumnNumber; j++)
            {
                if (workSheet.Cells[inicioFila, j + 1].Value != null)
                {
                    iColumnasExistentes++;
                }
            }
            maxColumnNumber = iColumnasExistentes;

            var sColumna = ObtenerColumnaExcel(maxColumnNumber);
            var sFilaCabeceraFrom = @"A" + inicioFila.ToString();
            var sFilaCabeceraTo = sColumna + inicioFila.ToString();

            var oCabecera =
                (from c in workSheet.Cells[string.Format("{0}:{1}", sFilaCabeceraFrom, sFilaCabeceraTo)].ToList()
                 select new DataColumn { Caption = Convert.ToString(c.Value), ColumnName = Convert.ToString(c.Value) })
                    .ToArray();
            dtMetadata.Columns.AddRange(oCabecera);

            var valorCelda = "";
            for (var i = (inicioFila + 1); i <= totalRowCount; i++)
            {
                var oRowValue = new List<string>();
                for (var j = 1; j <= maxColumnNumber; j++)
                {
                    valorCelda = Convert.ToString(workSheet.Cells[i, j].Value);
                    valorCelda = valorCelda.Replace("\n", " ");
                    valorCelda = valorCelda.Replace("\r", " ");
                    valorCelda = valorCelda.Replace("'", "''");
                    oRowValue.Add(valorCelda);
                }
                dtMetadata.LoadDataRow(oRowValue.ToArray(), false);
            }

            return dtMetadata;
        }

        //public static DataTable ReadExcelXLSToDataTable(string ArchivoLectura, string separdor)
        //{
        //    var dtMetadata = new DataTable();

        //    HSSFWorkbook hssfwb;
        //    using (var file = new FileStream(ArchivoLectura, FileMode.Open, FileAccess.Read))
        //    {
        //        hssfwb = new HSSFWorkbook(file);
        //    }

        //    var sheet = hssfwb.GetSheetAt(0);
        //    var headerRow = sheet.GetRow(0);

        //    int maxColumnNumber = headerRow.LastCellNum;
        //    var totalRowCount = sheet.LastRowNum + 1;

        //    var sColumna = ObtenerColumnaExcel(maxColumnNumber);
        //    var sFilaCabeceraFrom = @"A1";
        //    var sFilaCabeceraTo = sColumna + "1";

        //    var oCabecera = (from c in sheet.GetRow(0).Cells.ToList()
        //                     select
        //                         new DataColumn
        //                         {
        //                             Caption = Convert.ToString(c.StringCellValue),
        //                             ColumnName = Convert.ToString(c.StringCellValue)
        //                         }).ToArray();

        //    dtMetadata.Columns.AddRange(oCabecera);

        //    var text = "";
        //    var valorCelda = "";

        //    for (var row = 1; row <= sheet.LastRowNum; row++)
        //    {
        //        if (sheet.GetRow(row) != null)
        //        {
        //            var oRowValue = new List<string>();
        //            for (var colm = 0; colm <= maxColumnNumber - 1; colm++)
        //            {
        //                valorCelda = "";
        //                if (sheet.GetRow(row).GetCell(colm) != null)
        //                {
        //                    valorCelda = sheet.GetRow(row).GetCell(colm).ToString();
        //                    valorCelda = valorCelda.Replace("'", "''");
        //                }
        //                oRowValue.Add(valorCelda);
        //            }

        //            dtMetadata.LoadDataRow(oRowValue.ToArray(), false);
        //        }
        //    }

        //    return dtMetadata;
        //}

        private static string ObtenerColumnaExcel(long i)
        {
            if (i == 0) return "";
            i--;
            return ObtenerColumnaExcel(i / 26) + (char)('A' + i % 26);
        }


    }
}
