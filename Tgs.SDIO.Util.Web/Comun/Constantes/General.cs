﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Util.Web.Comun.Constantes
{
    public class General
    {
        public class Estados
        {
            public const int Inactivo = 0;
            public const int Activo = 1;
            public const int EnProceso = 1;
            public const int Ganado = 2;
            public const int Desestimado = 3;
            public const int Temporal = 0;
        }

    }
}
