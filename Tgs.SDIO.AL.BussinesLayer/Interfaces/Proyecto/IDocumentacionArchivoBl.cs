﻿using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto
{
    public interface IDocumentacionArchivoBl
    {
        bool InsertarDocumentacionArchivo(DocumentacionProyectoDtoRequest request);
        DocumentacionArchivoDtoResponse ObtenerDocumentacionArchivo(int iIdDocumentoArchivo);
    }
}

