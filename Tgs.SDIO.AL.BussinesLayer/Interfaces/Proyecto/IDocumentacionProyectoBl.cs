﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto
{
    public interface IDocumentacionProyectoBl
    {
        bool InsertarDocumentacionProyecto(DocumentacionProyectoDtoRequest request);
        List<DocumentacionProyectoDtoResponse> ListarDocumentacionProyecto(int iIdProyecto);
        bool EliminarDocumentacionProyecto(DocumentacionProyectoDtoRequest request);
    }
}

