﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto
{
    public interface IDetalleFacturarBl
    {
        DetalleFacturarDtoResponse ObtenerDetalleFacturarPorIdProyecto(DetalleFacturarDtoRequest request);

        DetalleFacturarDtoResponse ObtenerDetalleFacturarPorId(DetalleFacturarDtoRequest request);

        ProcesoResponse RegistrarDetalleFacturar(DetalleFacturarDtoRequest request);

        ProcesoResponse ActualizarDetalleFacturar(DetalleFacturarDtoRequest request);

    }
}
