﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto
{
    public interface IDetalleSolucionBl
    {
        List<DetalleSolucionDtoResponse> ObtenerDetallePorIdProyecto(DetalleSolucionDtoRequest request);

        DetalleSolucionDtoResponse ObtenerDetalleSolucionById(DetalleSolucionDtoRequest request);

        ProcesoResponse RegistrarDetalleSolucion(DetalleSolucionDtoRequest request);

        ProcesoResponse EliminarDetalleSolucion(DetalleSolucionDtoRequest request);

    }
}
