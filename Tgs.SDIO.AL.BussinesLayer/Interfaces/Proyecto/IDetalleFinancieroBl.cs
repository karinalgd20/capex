﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto
{
    public interface IDetalleFinancieroBl
    {
        List<DetalleFinancieroDtoResponse> ListarDetalleFinanciero(int iIdProyecto);
    }
}

