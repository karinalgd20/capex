﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto
{
    public interface IProyectoBl
    {
        List<ProyectoDtoResponse> ListarProyectoCabecera(ProyectoDtoRequest oportunidad);
        OportunidadGanadaDtoResponse ObtenerOportunidadGanada(OportunidadGanadaDtoRequest request);
        ProcesoResponse RegistrarProyecto(ProyectoDtoRequest proyecto);
        ProcesoResponse ActualizarProyecto(ProyectoDtoRequest proyecto);
        ProyectoDtoResponse ObtenerProyectoByID(ProyectoDtoRequest proyecto);
        ProcesoResponse ActualizarProyectoDescripcion(ProyectoDtoRequest request);
        ProcesoResponse RegistrarStageFinanciero(DetalleFinancieroDtoRequest request);
        ProcesoResponse ProcesarArchivosFinancieros(ProyectoDtoRequest request);
        List<PagoFinancieroDtoResponse> ListarPagos(ProyectoDtoRequest request);
        List<IndicadorFinancieroDtoResponse> ListarIndicadores(ProyectoDtoRequest request);
        List<CostoFinancieroDtoResponse> ListarCostos(ProyectoDtoRequest request);
        List<CostoConceptoDtoResponse> ListarCostosOpex(ProyectoDtoRequest request);
        List<CostoConceptoDtoResponse> ListarCostosCapex(ProyectoDtoRequest request);
        List<DetalleFinancieroDtoResponse> ListarServiciosCMI(ProyectoDtoRequest request);
        ProcesoResponse RegistrarEtapaEstado(EtapaProyectoDtoRequest request);
        List<DetalleFinancieroDtoResponse> BuscarDetalleFinancieroPorProyecto(ProyectoDtoRequest request);
        List<ProyectoDtoResponse> ListarProyecto(ProyectoDtoRequest request);
    }
}

