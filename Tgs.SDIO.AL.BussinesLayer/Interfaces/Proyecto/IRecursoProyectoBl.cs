﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto
{
    public interface IRecursoProyectoBl
    {
        bool InsertarRecursoProyecto(RecursoProyectoDtoRequest request);
        List<RecursoProyectoDtoResponse> ListarRecursoProyecto(int iIdProyecto);
        RecursoProyectoDtoResponse ObtenerRecursoPorProyectoRol(RecursoProyectoDtoRequest request);
    }
}

