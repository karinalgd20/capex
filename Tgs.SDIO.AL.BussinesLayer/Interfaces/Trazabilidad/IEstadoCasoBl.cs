﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
    public interface IEstadoCasoBl
    {
        List<ListaDtoResponse> ListarComboEstadoCaso();
    }
}
