﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
   public  interface IOportunidadSTBl
    {
        OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalObservacionPaginado(OportunidadSTDtoRequest request);

        OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalRecursoPaginado(OportunidadSTDtoRequest request);

        OportunidadSTDtoResponse ObtenerOportunidadSTPorId(OportunidadSTDtoRequest request);

        OportunidadSTPaginadoDtoResponse ListaOportunidadSTMasivoPaginado(OportunidadSTDtoRequest request);

        OportunidadSTSeguimientoPreventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPreventaPaginado(OportunidadSTSeguimientoPreventaDtoRequest request);

        OportunidadSTSeguimientoPostventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPostventaPaginado(OportunidadSTSeguimientoPostventaDtoRequest request);

        ProcesoResponse ActualizarOportunidadSTModalPreventa(OportunidadSTDtoRequest request);

    }
}
