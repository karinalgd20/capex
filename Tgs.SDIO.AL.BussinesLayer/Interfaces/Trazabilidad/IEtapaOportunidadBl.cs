﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
   public  interface IEtapaOportunidadBl
    {
        List<ListaDtoResponse> ListarComboEtapaOportunidad();
        List<ListaDtoResponse> ListarComboEtapaOportunidadPorIdFase(FaseDtoRequest request);
    }
}
