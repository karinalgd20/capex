﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

//EAAR: PF12
namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
    public interface ITableroOportunidadBl
    {
        TableroOportunidadMatrizDtoResponse ListarTableroOportunidadMatriz(TableroOportunidadListarDtoRequest request);
    }
}
