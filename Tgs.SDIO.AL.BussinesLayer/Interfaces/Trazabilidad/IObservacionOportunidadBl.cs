﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
   public interface IObservacionOportunidadBl
    {
        ObservacionOportunidadPaginadoDtoResponse ListadoObservacionesOportunidadPaginado(ObservacionOportunidadDtoRequest request);

        ObservacionOportunidadDtoResponse ObtenerObservacionOportunidadPorId(ObservacionOportunidadDtoRequest request);

        ProcesoResponse ActualizarObservacionOportunidad(ObservacionOportunidadDtoRequest request);

        ProcesoResponse RegistrarObservacionOportunidad(ObservacionOportunidadDtoRequest request);

        ProcesoResponse EliminarObservacionOportunidad(ObservacionOportunidadDtoRequest request);
    }
}
