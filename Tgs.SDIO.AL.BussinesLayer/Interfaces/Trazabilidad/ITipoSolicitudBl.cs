﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;


namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
   public interface ITipoSolicitudBl
    {
        List<ListaDtoResponse> ListarComboTipoSolicitud();
    }
}
