﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
    public interface ITiempoAtencionEquipoBl
    {
        TiempoAtencionEquipoPaginadoDtoResponse ListadoTiempoAtencionEquipoPaginado(TiempoAtencionEquipoDtoRequest request);

        TiempoAtencionEquipoDtoResponse ObtenerTiempoAtencionEquipoPorId(TiempoAtencionEquipoDtoRequest request);

        ProcesoResponse ActualizarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request);

        ProcesoResponse RegistrarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request);

        ProcesoResponse EliminarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request);
    }
}
