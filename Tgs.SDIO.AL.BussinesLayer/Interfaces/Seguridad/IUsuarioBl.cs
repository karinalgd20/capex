﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad
{
    public interface IUsuarioBl
    {
        UsuarioDtoResponse ObtenerUsuarioPorLogin(string login);
        UsuarioDtoResponse ValidarUsuario(UsuarioDtoRequest usuarioDtoRequest); 
        string EnvioClaveUsuario(UsuarioDtoRequest usuarioDtoRequest);
        List<EntidadDtoResponse> ObtenerAmbitoUsuario(UsuarioDtoRequest usuarioDtoRequest);
        ProcesoResponse RegistrarUsuario(UsuarioDtoRequest usuarioDtoRequest);
        UsuarioDtoResponse CambiarPassword(UsuarioDtoRequest usuarioDtoRequest);
        UsuarioDtoResponse ObtenerUsuarioPerfil(UsuarioDtoRequest usuarioDtoRequest);
        List<UsuarioDtoResponse> ObtenerUsuariosPorPerfil(UsuarioDtoRequest usuarioDtoRequest); 
        UsuarioDtoResponse ObtenerUsuarioPorId(int idUsuario);

        List<UsuarioDtoResponse> ListarUsuariosPaginado(UsuarioDtoRequest usuarioDtoRequest);

        ProcesoResponse ActualizarEstadoUsuario(UsuarioDtoRequest usuarioDtoRequest);

        List<ListaDtoResponse> ListarUsuariosFiltro(UsuarioDtoRequest usuarioDtoRequest);

        ProcesoResponse ActualizarUsuario(UsuarioDtoRequest usuarioDtoRequest);
    }
}
