﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad
{
    public interface IUsuarioPerfilBl
    {
        ProcesoResponse ActualizarEstadoUsuarioPerfil(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest);

        List<UsuarioPerfilDtoResponse> ListarPerfilesAsignados(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest);

        List<ListaDtoResponse> ListarPerfilesPorSistema();

        ProcesoResponse RegistrarUsuarioPerfil(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest);
    }
}
