﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza
{
    public interface ICartaFianzaBl
    {
        ProcesoResponse ActualizaARenovacionCartaFianza(CartaFianzaDetalleRenovacionRequest CartaFianzaRequestActualiza);
        ProcesoResponse InsertaCartaFianza(CartaFianzaRequest CartaFianzaRequestInserta);
        ProcesoResponse ActualizaCartaFianza(CartaFianzaRequest CartaFianzaRequestActualiza);
        MailResponse ObtenerDatosCorreo(MailRequest mailRequest);
        CartaFianzaListaResponsePaginado ListaCartaFianza(CartaFianzaRequest CartaFianzaRequestLista);
        List<CartaFianzaListaResponse> ListaCartaFianzaId(CartaFianzaRequest CartaFianzaRequestLista);
        ProcesoResponse ActualizarCartaFianzaRequerida(CartaFianzaRequest CartaFianzaRequestLista);
        List<CartaFianzaListaResponse> ListaCartaFianzaCombo(CartaFianzaRequest cartaFianzaRequestLista);

        DashBoardCartaFianzaResponse ListaDatos_DashoBoard_CartaFianza(DashBoardCartaFianzaRequest dashBoardCartaFianza);
        List<DashBoardCartaFianzaListGResponse> ListaDatos_DashoBoard_CartaFianza_Pie(DashBoardCartaFianzaRequest dashBoardCartaFianza);
        List<CartaFianzaListaResponse> CartasFianzasSinRespuesta(CartaFianzaRequest cartaFianzaRequest);
        ProcesoResponse GuardarRespuestaGerenteCuenta(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacion);
        ProcesoResponse GuardarConfirmacionGerenteCuenta(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacion);
    }
}
