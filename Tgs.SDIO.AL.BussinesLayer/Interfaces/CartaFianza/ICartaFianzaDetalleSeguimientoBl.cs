﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces
{
    public interface ICartaFianzaDetalleSeguimientoBl
    {
        CartaFianzaDetalleSeguimientoResponsePaginado ListaCartaFianzaDetalleSeguimiento(CartaFianzaDetalleSeguimientoRequest cartaFianzaDetalleRecuperoLista);
        ProcesoResponse InsertaCartaFianzaSeguimiento(CartaFianzaDetalleSeguimientoRequest cartaFianzaDetalleSeguimiento);
    }
}
