﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza
{
    public interface ICartaFianzaColaboradorDetalleBl
    {
        List<CartaFianzaColaboradorDetalleResponse> ListarColaborador(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestList);
        ProcesoResponse AsignarSupervisorAGerenteCuenta(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest);
        ProcesoResponse AsignarGerenteCuentaACartaFianza(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest);
        ProcesoResponse ActualizarTesorera(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest);
    }
}
