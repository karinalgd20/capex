﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza
{
    public interface ICartaFianzaDetalleEstadoBl
    {
        List<CartaFianzaDetalleEstadoResponse> InsertaCartaFianzaDetalleEstado(List<CartaFianzaDetalleEstadoRequest> CartaFianzaDetalleEstadoRequestInserta);
        CartaFianzaDetalleEstadoResponse ActualizaCartaFianzaDetalleEstado(List<CartaFianzaDetalleEstadoRequest> CartaFianzaDetalleEstadoequestActualiza);
        CartaFianzaDetalleEstadoResponse ListaCartaFianzaDetalleEstado(List<CartaFianzaDetalleEstadoRequest> CartaFianzaDetalleEstadoequestLista);

    }
}
