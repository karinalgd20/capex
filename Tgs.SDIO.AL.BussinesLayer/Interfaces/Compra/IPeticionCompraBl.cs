﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra
{
    public interface IPeticionCompraBl
    {
        PeticionCompraPaginadoDtoResponse ListarPeticionCompraPaginado(PeticionCompraDtoRequest request);
        ProcesoResponse RegistrarPeticionCompra(PeticionCompraDtoRequest request);
        ProcesoResponse ActualizarPeticionCompra(PeticionCompraDtoRequest request);
        PeticionCompraDtoResponse ObtenerPeticionCompraPorId(PeticionCompraDtoRequest request);
        ProcesoResponse ActualizarPeticionCompraDetalleCompra(PeticionCompraDtoRequest request);
        List<AprobacionDtoResponse> ListaAprobaciones(PeticionCompraDtoRequest request);
    }
}