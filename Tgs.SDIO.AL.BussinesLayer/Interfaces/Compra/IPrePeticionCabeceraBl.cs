﻿
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra
{
    public interface IPrePeticionCabeceraBl
    {
        List<PrePeticionCabeceraDtoResponse> ListarPrePeticion(PrePeticionCabeceraDtoRequest request);
        List<PrePeticionCabeceraDtoResponse> ListarPrePeticionCabecera(PrePeticionCabeceraDtoRequest request);
        ProcesoResponse RegistrarPrePeticionCompraCabecera(PrePeticionCabeceraDtoRequest request);
        PrePeticionCabeceraDtoResponse ObtenerPrePeticionCabecera(PrePeticionCabeceraDtoRequest request);
        int ValidacionMontoLimite(PrePeticionCabeceraDtoRequest request);
        string ObtenerSaldo(PrePeticionCabeceraDtoRequest request);
    }
}
