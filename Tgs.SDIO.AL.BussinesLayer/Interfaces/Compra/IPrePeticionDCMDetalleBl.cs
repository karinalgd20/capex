﻿
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra
{
    public interface IPrePeticionDCMDetalleBl
    {
        ProcesoResponse RegistrarPrePeticionDCMDetalle(PrePeticionDetalleDCMDetalleDtoRequest request);
        List<PrePeticionDCMDetalleDtoResponse> ListarPrePeticionDCM(PrePeticionDetalleDCMDetalleDtoRequest request);
        PrePeticionDCMDetalleDtoResponse ObtenerDetalleDCM(PrePeticionDetalleDCMDetalleDtoRequest request);
    }
}
