﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra
{
    public interface ICentroCostoRecursoBl
    {
        CentroCostoRecursoPaginadoDtoResponse ListarCentroCostoRecursoPaginado(CentroCostoRecursoDtoRequest request);
    }
}
