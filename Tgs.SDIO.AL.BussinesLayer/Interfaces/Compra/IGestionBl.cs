﻿
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra
{
    public interface IGestionBl
    {
        GestionDtoResponse ObtenerGestion(GestionDtoRequest request);
        ProcesoResponse RegistrarGestion(GestionDtoRequest request);
    }
}
