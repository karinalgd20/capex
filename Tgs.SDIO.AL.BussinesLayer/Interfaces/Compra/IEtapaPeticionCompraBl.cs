﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra
{
    public interface IEtapaPeticionCompraBl
    {
        ProcesoResponse RegistrarEtapaPeticionCompra(EtapaPeticionCompraDtoRequest request);
        EtapaPeticionCompraDtoResponse ObtenerEtapaPeticionCompraActual(EtapaPeticionCompraDtoRequest request);
        EtapaPeticionCompraPaginadoDtoResponse ListarEtapaPeticionCompraPaginado(EtapaPeticionCompraDtoRequest request);
    }
}