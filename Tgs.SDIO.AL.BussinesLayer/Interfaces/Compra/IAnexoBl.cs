﻿
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra
{
    public interface IAnexoBl
    {
        ProcesoResponse RegistrarAnexo(AnexoDtoRequest request);
        List<AnexoDtoResponse> ListarAnexos(AnexoDtoRequest request);
        bool EliminarAnexo(AnexoDtoRequest request);
    }
}
