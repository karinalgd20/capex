﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IProveedorBl
    {
        ProcesoResponse RegistrarProveedor(ProveedorDtoRequest objProveedor);
        ProcesoResponse ActualizarProveedor(ProveedorDtoRequest objProveedor);
        ProcesoResponse InactivarProveedor(ProveedorDtoRequest objProveedor);
        ProveedorDtoResponse ObtenerProveedor(ProveedorDtoRequest objProveedor);
        List<ListaDtoResponse> ListarProveedor(ProveedorDtoRequest objProveedor);
        List<ProveedorDtoResponse> ListarProveedores(ProveedorDtoRequest objProveedor);
        ProveedorPaginadoDtoResponse ListarProveedorPaginado(ProveedorDtoRequest objProveedor);
    }
}
