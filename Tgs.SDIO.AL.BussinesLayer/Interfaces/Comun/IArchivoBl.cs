﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IArchivoBl
    {
        ProcesoResponse RegistrarArchivo(ArchivoDtoRequest archivo, byte[] bytes);
        ProcesoResponse InactivarArchivo(ArchivoDtoRequest archivo);
        ArchivoPaginadoDtoResponse ListarArchivoPaginado(ArchivoDtoRequest request);
        ArchivoDtoResponse ObtenerArchivo(ArchivoDtoRequest request);
    }
}