﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IRiesgoProyectoBl
    {
        ProcesoResponse RegistrarRiesgoProyecto(RiesgoProyectoDtoRequest riesgoProyecto);
        ProcesoResponse ActualizarRiesgoProyecto(RiesgoProyectoDtoRequest riesgoProyecto);
        ProcesoResponse EliminarRiesgoProyecto(RiesgoProyectoDtoRequest riesgoProyecto);
        RiesgoProyectoDtoResponse ObtenerRiesgoProyectoPorId(RiesgoProyectoDtoRequest riesgoProyecto);
        RiesgoProyectoPaginadoDtoResponse ListadoRiesgoProyectosPaginado(RiesgoProyectoDtoRequest riesgoProyecto);
    }
}