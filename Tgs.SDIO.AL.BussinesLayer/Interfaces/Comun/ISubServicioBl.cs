﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface ISubServicioBl
    {
        List<ListaDtoResponse> ListarSubServicios(SubServicioDtoRequest subServicio);

        SubServicioDtoResponse ObtenerSubServicio(SubServicioDtoRequest subServicio);

        ProcesoResponse RegistrarSubServicio(SubServicioDtoRequest subServicio);

        ProcesoResponse ActualizarSubServicio(SubServicioDtoRequest subServicio);

        SubServicioPaginadoDtoResponse ListaSubServicioPaginado(SubServicioDtoRequest subServicio);
        ProcesoResponse InhabilitarSubServicio(SubServicioDtoRequest subServicio);

        List<ListaDtoResponse> ListarSubServiciosPorIdGrupo(SubServicioDtoRequest subServicio);

    }
}
