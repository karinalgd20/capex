﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface ICasoNegocioBl
    {
        CasoNegocioPaginadoDtoResponse ListarCasoNegocio(CasoNegocioDtoRequest casonegocio);

        CasoNegocioDtoResponse ObtenerCasoNegocio(CasoNegocioDtoRequest casonegocio);

        ProcesoResponse RegistrarCasoNegocio(CasoNegocioDtoRequest casonegocio);

        ProcesoResponse ActualizarCasoNegocio(CasoNegocioDtoRequest casonegocio);

        List<ListaDtoResponse> ListarCasoNegocioDefecto(CasoNegocioDtoRequest casonegocio);

    }
}
