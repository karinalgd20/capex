﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface ICasoNegocioServicioBl
    {
        List<CasoNegocioServicioDtoResponse> ListarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio);

        CasoNegocioServicioDtoResponse ObtenerCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio);

        ProcesoResponse RegistrarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio);

        ProcesoResponse ActualizarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio);
        CasoNegocioServicioPaginadoDtoResponse ListPaginadoCasoNegocioServicio(CasoNegocioServicioDtoRequest casoNegocio);
    }
}
