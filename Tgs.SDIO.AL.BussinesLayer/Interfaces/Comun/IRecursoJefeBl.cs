﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IRecursoJefeBl
    {
        RecursoJefePaginadoDtoResponse ListarRecursoPorJefePaginado(RecursoJefeDtoRequest recursoJefeDtoRequest);

        ProcesoResponse ActualizarEstadoRecursoJefe(RecursoJefeDtoRequest recursoJefeDtoRequest);

        ProcesoResponse RegistrarRecursoJefe(RecursoJefeDtoRequest recursoJefeDtoRequest);
    }
}
