﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IFaseBl
    {
        List<ListaDtoResponse> ListarComboFase();
    }
}
