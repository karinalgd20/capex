﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;


namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface ICorreoBl
    {
        ProcesoResponse EnviarCorreo(MailDto mailDto);
    }
}
