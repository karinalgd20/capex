﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IRecursoLineaNegocioBl
    {
        RecursoLineaNegocioPaginadoDtoResponse ListarRecursoLineaNegocioPaginado(RecursoLineaNegocioDtoRequest recursoLineaRequest);

        ProcesoResponse ActualizarEstadoRecursoLineaNegocio(RecursoLineaNegocioDtoRequest recursoLineaRequest);

        ProcesoResponse RegistrarRecursoLineaNegocio(RecursoLineaNegocioDtoRequest recursoLineaRequest);
    }
}
