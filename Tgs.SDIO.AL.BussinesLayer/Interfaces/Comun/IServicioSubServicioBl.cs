﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IServicioSubServicioBl
    {
        List<ServicioSubServicioDtoResponse> ListarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        ServicioSubServicioDtoResponse ObtenerServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        ProcesoResponse RegistrarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        ProcesoResponse ActualizarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        ServicioSubServicioPaginadoDtoResponse ListaServicioSubServicioPaginado(ServicioSubServicioDtoRequest servicioSubServicio);

        ProcesoResponse InhabilitarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        ProcesoResponse EliminarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        ServicioSubServicioPaginadoDtoResponse ListarServicioSubServicioGrupos(ServicioSubServicioDtoRequest ServiciosubServicio);
    }
}
