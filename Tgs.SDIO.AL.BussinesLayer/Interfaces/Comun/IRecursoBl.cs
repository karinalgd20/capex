﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
   public interface IRecursoBl
    {
        RecursoPaginadoDtoResponse ListadoRecursosPaginado(RecursoDtoRequest request);

        RecursoDtoResponse ObtenerRecursoPorId(RecursoDtoRequest request);

        ProcesoResponse ActualizarRecurso(RecursoDtoRequest request);

        ProcesoResponse RegistrarRecurso(RecursoDtoRequest request);

        ProcesoResponse EliminarRecurso(RecursoDtoRequest request);

        RecursoPaginadoDtoResponse ListaRecursoModalRecursoPaginado(RecursoDtoRequest request);

        ProcesoResponse ActualizarRecursoUsuario(RecursoDtoRequest recursoDtoRequest);

        RecursoDtoResponse ObtenerRecursoPorIdUsuarioRais(int idUsuarioRais);

        RecursoDtoResponse ObtenerRecursoPorCorreo(string correo);
        List<RecursoDtoResponse> ListarRecursoPorCargo(RecursoDtoRequest request);

        RecursoDtoResponse ListarRecursoDeUsuario(RecursoDtoRequest request);

        RecursoPaginadoDtoResponse ListadoRecursosByCargoPaginado(RecursoDtoRequest request);
    }
}
