﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface ICargoBl
    {
        List<ListaDtoResponse> ListarCargos(CargoDtoRequest cargoRequest);

        List<ListaDtoResponse> ListarComboCargo();
    }
}
