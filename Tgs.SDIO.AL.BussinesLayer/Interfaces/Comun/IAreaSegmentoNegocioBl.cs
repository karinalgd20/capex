﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IAreaSegmentoNegocioBl
    {
        ProcesoResponse RegistrarAreaSegmentoNegocio(AreaSegmentoNegocioDtoRequest request);

        ProcesoResponse EliminarAreaSegmentoNegocio(AreaSegmentoNegocioDtoRequest request);

        AreaSegmentoNegocioDtoResponse ObtenerAreaSegmentoNegocioPorId(AreaSegmentoNegocioDtoRequest request);
    }
}
