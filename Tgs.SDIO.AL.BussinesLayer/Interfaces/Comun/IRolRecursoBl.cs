﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
   public interface IRolRecursoBl
    {
        RolRecursoPaginadoDtoResponse ListadoRolRecursosPaginado(RolRecursoDtoRequest request);

        RolRecursoDtoResponse ObtenerRolRecursoPorId(RolRecursoDtoRequest request);

        ProcesoResponse ActualizarRolRecurso(RolRecursoDtoRequest request);

        ProcesoResponse RegistrarRolRecurso(RolRecursoDtoRequest request);

        ProcesoResponse EliminarRolRecurso(RolRecursoDtoRequest request);

        List<ListaDtoResponse> ListarComboRolRecurso();
    }
}
