﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IConceptoBl
    {
        List<ConceptoDtoResponse> ListarBandejaConceptos(ConceptoDtoRequest concepto);

        List<ConceptoDtoResponse> ListarConceptos(ConceptoDtoRequest concepto);

        ConceptoDtoResponse ObtenerConcepto(ConceptoDtoRequest concepto);

        ProcesoResponse RegistrarConcepto(ConceptoDtoRequest concepto);

        ProcesoResponse ActualizarConcepto(ConceptoDtoRequest concepto);
        

    }
}
