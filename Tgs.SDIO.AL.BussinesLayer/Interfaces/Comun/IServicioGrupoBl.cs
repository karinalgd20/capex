﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IServicioGrupoBl
    {
        List<ListaDtoResponse> ListarServicioGrupo(ServicioGrupoDtoRequest serviciogrupo);
    }
}
