﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
   public  interface IEtapaBl
    {
        List<ListaDtoResponse> ListarComboEtapaOportunidad();
        List<ListaDtoResponse> ListarComboEtapaOportunidadPorIdFase(FaseDtoRequest request);
    }
}
