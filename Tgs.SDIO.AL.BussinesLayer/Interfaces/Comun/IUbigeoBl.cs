﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IUbigeoBl
    {
        List<ListaDtoResponse> ListarComboUbigeoDepartamento();

        List<ListaDtoResponse> ListarComboUbigeoProvincia(UbigeoDtoRequest request);

        List<ListaDtoResponse> ListarComboUbigeoDistrito(UbigeoDtoRequest request);
        UbigeoDtoResponse ObtenerPorCodigoDistrito(UbigeoDtoRequest UbigeoDtoRequest);

    }
}
