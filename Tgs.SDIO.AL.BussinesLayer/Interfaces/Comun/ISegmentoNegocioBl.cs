﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface ISegmentoNegocioBl
    {
        List<ComboDtoResponse> ListarComboSegmentoNegocio();

        List<ListaDtoResponse> ListarComboSegmentoNegocioSimple();
    }
}
