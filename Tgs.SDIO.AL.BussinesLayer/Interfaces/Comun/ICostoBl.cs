﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface ICostoBl
    {
         CostoDtoResponse ObtenerCostoPorCodigo(CostoDtoRequest medioCosto);

        List<ListaDtoResponse> ListarCosto(CostoDtoRequest medioCosto);
     
        List<CostoDtoResponse> ListarComboCosto(CostoDtoRequest medioCosto);
        List<CostoDtoResponse> ListarCostoPorMedioServicioGrupo(CostoDtoRequest costo);
        CostoDtoResponse ObtenerCosto(CostoDtoRequest medioCosto);
    }
}
