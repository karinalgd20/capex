﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IConceptoSeguimientoBl
    {
        ConceptoSeguimientoPaginadoDtoResponse ListadoConceptosSeguimientoPaginado(ConceptoSeguimientoDtoRequest request);

        ConceptoSeguimientoDtoResponse ObtenerConceptoSeguimientoPorId(ConceptoSeguimientoDtoRequest request);

        ProcesoResponse ActualizarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request);

        ProcesoResponse RegistrarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request);

        ProcesoResponse EliminarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request);

        List<ListaDtoResponse> ListarComboConceptoSeguimiento();

        List<ListaDtoResponse> ListarComboConceptoSeguimientoAgrupador();

        List<ListaDtoResponse> ListarComboConceptoSeguimientoNiveles();
    }
}
