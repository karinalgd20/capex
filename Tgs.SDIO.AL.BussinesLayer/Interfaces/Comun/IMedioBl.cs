﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IMedioBl
    {


        List<ListaDtoResponse> ListarMedio(MedioDtoRequest Medio);

        MedioDtoResponse ObtenerMedio(MedioDtoRequest Medio);

        ProcesoResponse RegistrarMedio(MedioDtoRequest Medio);

        ProcesoResponse ActualizarMedio(MedioDtoRequest Medio);


    }
}
