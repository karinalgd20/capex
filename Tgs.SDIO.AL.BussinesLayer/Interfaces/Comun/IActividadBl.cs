﻿using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IActividadBl
    {
        ActividadPaginadoDtoResponse ListatActividadPaginado(ActividadDtoRequest request);

        ActividadDtoResponse ObtenerActividadPorId(ActividadDtoRequest request);

        ProcesoResponse ActualizarActividad(ActividadDtoRequest request);

        ProcesoResponse RegistrarActividad(ActividadDtoRequest request);

        ProcesoResponse EliminarActividad(ActividadDtoRequest request);

        List<ListaDtoResponse> ListarComboActividadPadres();

    }
}
