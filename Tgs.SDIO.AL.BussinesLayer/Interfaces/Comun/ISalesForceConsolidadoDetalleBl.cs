﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface ISalesForceConsolidadoDetalleBl
    {
        SalesForceConsolidadoDetalleDtoResponse ObtenerSalesForceConsolidadoDetalle(SalesForceConsolidadoDetalleDtoRequest salesForceConsolidadoDetalle);
        List<ListaDtoResponse> ListaNumerodeCasoPorNumeroSalesForceDetalle(SalesForceConsolidadoDetalleDtoRequest salesForceConsolidadoDetalle);
    }
}
