﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface IAsignacionCapexTotalBl
    {
        List<ListaDtoResponse> ListarAsignacionCapexTotal(AsignacionCapexTotalDtoRequest AsignacionCapexTotal);

        AsignacionCapexTotalDtoResponse ObtenerAsignacionCapexTotal(AsignacionCapexTotalDtoRequest AsignacionCapexTotal);

        ProcesoResponse RegistrarAsignacionCapexTotal(AsignacionCapexTotalDtoRequest AsignacionCapexTotal);

        ProcesoResponse ActualizarAsignacionCapexTotal(AsignacionCapexTotalDtoRequest AsignacionCapexTotal);
        ProcesoResponse InhabilitarAsignacionCapexTotal(AsignacionCapexTotalDtoRequest AsignacionCapexTotal);

    }
}
