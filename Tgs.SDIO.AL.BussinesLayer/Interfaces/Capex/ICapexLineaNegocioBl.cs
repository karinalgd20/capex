﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface ICapexLineaNegocioBl
    {
        List<ListaDtoResponse> ListarCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio);

        CapexLineaNegocioDtoResponse ObtenerCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio);

        ProcesoResponse RegistrarCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio);

        ProcesoResponse ActualizarCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio);
        ProcesoResponse InhabilitarCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio);
        CapexLineaNegocioPaginadoDtoResponse ListarLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocio);
        CapexLineaNegocioDtoResponse ObtenerCapexLineaNegocioPorIdSolicitud(CapexLineaNegocioDtoRequest CapexLineaNegocio);

    }
}
