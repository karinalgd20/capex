﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface ICronogramaDetalleBl
    {
        List<ListaDtoResponse> ListarCronogramaDetalle(CronogramaDetalleDtoRequest CronogramaDetalle);

        CronogramaDetalleDtoResponse ObtenerCronogramaDetalle(CronogramaDetalleDtoRequest CronogramaDetalle);

        ProcesoResponse RegistrarCronogramaDetalle(CronogramaDetalleDtoRequest CronogramaDetalle);

        ProcesoResponse ActualizarCronogramaDetalle(CronogramaDetalleDtoRequest CronogramaDetalle);
        ProcesoResponse InhabilitarCronogramaDetalle(CronogramaDetalleDtoRequest CronogramaDetalle);

    }
}
