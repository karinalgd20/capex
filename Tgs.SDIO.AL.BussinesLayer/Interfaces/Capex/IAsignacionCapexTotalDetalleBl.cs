﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface IAsignacionCapexTotalDetalleBl
    {
        List<ListaDtoResponse> ListarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle);

        AsignacionCapexTotalDetalleDtoResponse ObtenerAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle);

        ProcesoResponse RegistrarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle);

        ProcesoResponse ActualizarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle);
        ProcesoResponse InhabilitarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest AsignacionCapexTotalDetalle);

    }
}
