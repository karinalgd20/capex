﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface ISolicitudBl
    {
        List<ListaDtoResponse> ListarSolicitud(SolicitudDtoRequest solicitud);

        SolicitudDtoResponse ObtenerSolicitud(SolicitudDtoRequest solicitud);

        ProcesoResponse RegistrarSolicitud(SolicitudDtoRequest solicitud);

        ProcesoResponse ActualizarSolicitud(SolicitudDtoRequest solicitud);
        ProcesoResponse InhabilitarSolicitud(SolicitudDtoRequest solicitud);

    }
}
