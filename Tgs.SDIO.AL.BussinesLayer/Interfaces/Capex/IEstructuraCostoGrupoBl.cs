﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface IEstructuraCostoGrupoBl
    {
      
        List<EstructuraCostoGrupoDtoResponse> ListarEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest EstructuraCostoGrupo);

        ProcesoResponse RegistrarEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest EstructuraCostoGrupo);

        ProcesoResponse ActualizarEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest estructuraCostoGrupo);

        EstructuraCostoGrupoPaginadoDtoResponse ListaEstructuraCostoGrupoPaginado(EstructuraCostoGrupoDtoRequest estructuraCostoGrupo);
        EstructuraCostoGrupoDetalleDtoResponse ObtenerEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest estructuraCostoGrupo);

    }
}
