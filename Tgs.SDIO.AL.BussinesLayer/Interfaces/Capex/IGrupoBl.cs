﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface IGrupoBl
    {
        List<GrupoDtoResponse> ListarBandejaGrupo(GrupoDtoRequest Grupo);

        List<ListaDtoResponse> ListarGrupo(GrupoDtoRequest Grupo);

        GrupoDtoResponse ObtenerGrupo(GrupoDtoRequest Grupo);

        ProcesoResponse RegistrarGrupo(GrupoDtoRequest Grupo);

        ProcesoResponse ActualizarGrupo(GrupoDtoRequest Grupo);
    }
}
