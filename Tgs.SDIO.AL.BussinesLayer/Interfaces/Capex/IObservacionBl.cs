﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface IObservacionBl
    {
        List<ObservacionDtoResponse> ListarBandejaObservacion(ObservacionDtoRequest Observacion);

        List<ObservacionDtoResponse> ListarObservacion(ObservacionDtoRequest Observacion);

        ObservacionDtoResponse ObtenerObservacion(ObservacionDtoRequest Observacion);

        ProcesoResponse RegistrarObservacion(ObservacionDtoRequest Observacion);

        ProcesoResponse ActualizarObservacion(ObservacionDtoRequest Observacion);
        
    }
}
