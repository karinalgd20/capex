﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface ITopologiaBl
    {
        List<TopologiaDtoResponse> ListarBandejaTopologia(TopologiaDtoRequest Topologia);

        List<TopologiaDtoResponse> ListarTopologia(TopologiaDtoRequest Topologia);

        TopologiaDtoResponse ObtenerTopologia(TopologiaDtoRequest Topologia);

        ProcesoResponse RegistrarTopologia(TopologiaDtoRequest Topologia);

        ProcesoResponse ActualizarTopologia(TopologiaDtoRequest Topologia);
        
    }
}
