﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface IConceptosCapexBl
    {
        List<ListaDtoResponse> ListarConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex);

        ConceptosCapexDtoResponse ObtenerConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex);

        ProcesoResponse RegistrarConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex);

        ProcesoResponse ActualizarConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex);
        ProcesoResponse InhabilitarConceptosCapex(ConceptosCapexDtoRequest ConceptosCapex);

    }
}
