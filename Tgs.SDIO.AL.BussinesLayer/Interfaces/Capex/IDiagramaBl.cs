﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface IDiagramaBl
    {
        List<ListaDtoResponse> ListarDiagrama(DiagramaDtoRequest Diagrama);

        DiagramaDtoResponse ObtenerDiagrama(DiagramaDtoRequest Diagrama);

        ProcesoResponse RegistrarDiagrama(DiagramaDtoRequest Diagrama);

        ProcesoResponse ActualizarDiagrama(DiagramaDtoRequest Diagrama);
        ProcesoResponse InhabilitarDiagrama(DiagramaDtoRequest Diagrama);

    }
}
