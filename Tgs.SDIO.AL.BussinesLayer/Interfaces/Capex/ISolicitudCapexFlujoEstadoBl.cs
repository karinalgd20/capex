﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface ISolicitudCapexFlujoEstadoBl
    {
        List<SolicitudCapexFlujoEstadoDtoResponse> ListarBandejaSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado);
        List<SolicitudCapexFlujoEstadoDtoResponse> ListarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado);
        SolicitudCapexFlujoEstadoDtoResponse ObtenerSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado);
        ProcesoResponse RegistrarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado);
        ProcesoResponse ActualizarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado);
        SolicitudCapexFlujoEstadoDtoResponse ObtenerSolicitudCapexFlujoEstadoId(SolicitudCapexFlujoEstadoDtoRequest solicitudCapexFlujoEstado);
    }
}
