﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface ISeccionBl
    {
        List<SeccionDtoResponse> ListarBandejaSeccion(SeccionDtoRequest Seccion);

        List<SeccionDtoResponse> ListarSeccion(SeccionDtoRequest Seccion);

        SeccionDtoResponse ObtenerSeccion(SeccionDtoRequest Seccion);

        ProcesoResponse RegistrarSeccion(SeccionDtoRequest Seccion);

        ProcesoResponse ActualizarSeccion(SeccionDtoRequest Seccion);

        ProcesoResponse ActualizarSeccionObservacion(SeccionDtoRequest seccion);
    }
}
