﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface ISolicitudCapexBl
    {
        List<SolicitudCapexDtoResponse> ListarBandejaSolicitudCapex(SolicitudCapexDtoRequest SolicitudCapex);

        List<SolicitudCapexDtoResponse> ListarSolicitudCapex(SolicitudCapexDtoRequest SolicitudCapex);

        SolicitudCapexDtoResponse ObtenerSolicitudCapex(SolicitudCapexDtoRequest SolicitudCapex);

        ProcesoResponse RegistrarSolicitudCapex(SolicitudCapexDtoRequest SolicitudCapex);

        ProcesoResponse ActualizarSolicitudCapex(SolicitudCapexDtoRequest SolicitudCapex);

        SolicitudCapexPaginadoDtoResponse ListaSolicitudCapexPaginado(SolicitudCapexDtoRequest SolicitudCapex);

        ProcesoResponse ActualizarSolicitudCapexEstado(SolicitudCapexDtoRequest SolicitudCapex);

    }
}
