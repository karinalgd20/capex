﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using static Tgs.SDIO.DataContracts.Dto.Response.Capex.EstructuraCostoDtoResponse;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface IEstructuraCostoBl
    {
        List<ListaDtoResponse> ListarEstructuraCosto(EstructuraCostoDtoRequest EstructuraCosto);

        ProcesoResponse RegistrarEstructuraCosto(EstructuraCostoDtoRequest EstructuraCosto);

        ProcesoResponse ActualizarEstructuraCosto(EstructuraCostoDtoRequest EstructuraCosto);
        ProcesoResponse InhabilitarEstructuraCosto(EstructuraCostoDtoRequest EstructuraCosto);

    }
}
