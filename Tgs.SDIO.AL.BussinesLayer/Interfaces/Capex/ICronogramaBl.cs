﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface ICronogramaBl
    {
        List<ListaDtoResponse> ListarCronograma(CronogramaDtoRequest Cronograma);

        CronogramaDtoResponse ObtenerCronograma(CronogramaDtoRequest Cronograma);

        ProcesoResponse RegistrarCronograma(CronogramaDtoRequest Cronograma);

        ProcesoResponse ActualizarCronograma(CronogramaDtoRequest Cronograma);
        ProcesoResponse InhabilitarCronograma(CronogramaDtoRequest Cronograma);

    }
}
