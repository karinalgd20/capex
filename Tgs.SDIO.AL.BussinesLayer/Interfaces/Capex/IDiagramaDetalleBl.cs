﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex
{
    public interface IDiagramaDetalleBl
    {
        List<ListaDtoResponse> ListarDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle);

        DiagramaDetalleDtoResponse ObtenerDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle);

        ProcesoResponse RegistrarDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle);

        ProcesoResponse ActualizarDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle);
        ProcesoResponse InhabilitarDiagramaDetalle(DiagramaDetalleDtoRequest DiagramaDetalle);

    }
}
