﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio
{
    public interface IRPASisegoDatosContactoBl
    {
        
        ProcesoResponse ActualizarContactoServicio(RPASisegoDatosContactoDtoRequest resquest);
        ProcesoResponse RegistrarContactoServicio(RPASisegoDatosContactoDtoRequest resquest);
        List<RPASisegoDatosContactoDtoResponse> ListaDetallesContactoRPA(RPASisegoDatosContactoDtoRequest resquest);
        RPASisegoDatosContactoPaginadoDtoResponse ListaDetallesContactoRPAPaginadoDtoResponse(RPASisegoDatosContactoDtoRequest resquest);
    }
}
