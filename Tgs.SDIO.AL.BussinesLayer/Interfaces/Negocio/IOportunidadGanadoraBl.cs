﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio
{
    public interface IOportunidadGanadoraBl
    {
        ProcesoResponse AgregarOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        ProcesoResponse ValidarOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        ProcesoResponse EliminarOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        OportunidadGanadoraDtoResponse ObtenerOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        ClienteDtoResponse ObtenerRucClientePorIdOportunidad(OportunidadGanadoraDtoRequest request);
        SalesForceConsolidadoCabeceraDtoResponse ObtenerCasoOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        List<OportunidadGanadoraDtoResponse> ListarOportunidadGanadora(OportunidadGanadoraDtoRequest request);
        BandejaOportunidadPaginadoDtoResponse ListarBandejaOportunidadPaginado(OportunidadGanadoraDtoRequest request);
        OportunidadGanadoraPaginadoDtoResponse ObtenerListaCodigo();
        OportunidadGanadoraPaginadoDtoResponse ObtenerListaOportunidad();
        ClienteDtoResponse ObtenerClientePorRuc(ClienteDtoRequest request);
        ProcesoResponse GenerarCodificacionRPA(CodificacionRPA request);
        ProcesoResponse GenerarCierreOfertaRPA(CierreDeOfertaRPA request);
        ProcesoResponse GenerarEmisionCircuitoRPA(EmisionCircuitoRPA request);
        ProcesoResponse ActualizarEstadoRPA(string Numero_oportunidad, string EstadoOportunidad);
        SisegoCotizadoDtoResponse ObtenerSisegoCotizadoPorIdOporNroSisego(SisegoCotizadoDtoRequest request);
        IsisNroOfertaDtoResponse ObtenerIsisNroOfertaPorNroPer_NroOferta(IsisNroOfertaDtoRequest request);
        OportunidadGanadoraDtoResponse ObtenerOportunidadxPer(OportunidadGanadoraDtoRequest request);
        ProcesoResponse ActualizarEtapaCierreOportunidad(OportunidadGanadoraDtoRequest request);
        OportunidadGanadoraDtoResponse ObtenerOportunidadById(OportunidadGanadoraDtoRequest request);
        RPAEquiposServicioDtoResponse ObtenerRPAEquiposServicioPorIdOferta_IdRPASerNroOfer(RPAEquiposServicioDtoRequest request);
        RPASisegoDetalleDtoResponse ObtenerRPASisegoDetallePorIdSisego(RPASisegoDetalleDtoRequest request);
        RPASisegoDatosContactoDtoResponse ObtenerRPASisegoDatosContactoPorId_RPASisegoDetalle(RPASisegoDatosContactoDtoRequest request);
        RPAServiciosxNroOfertaDtoResponse ObtenerRPAServiciosxNroOfertaPorId_Oferta(RPAServiciosxNroOfertaDtoRequest request);



    }
}
