﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio
{
    public interface IAsociarServicioSisegosBl
    {
        ProcesoResponse AsociarServicioSisegos(AsociarServicioSisegosDtoRequest request);
        List<AsociarServicioSisegosDtoResponse> ListarServicioSisegos(AsociarServicioSisegosDtoRequest request);
        AsociarServicioSisegosPaginadoDtoResponse ListarServicioSisegosPaginado(AsociarServicioSisegosDtoRequest request);
        RPAServiciosxNroOfertaDtoResponse ObtenerServicioById(AsociarServicioSisegosDtoRequest request);
        RPAEquiposServicioDtoResponse ObtenerEquipoById(AsociarServicioSisegosDtoRequest request);
        RPASisegoDetalleDtoResponse ObtenerCodSisegoById(AsociarServicioSisegosDtoRequest request);
    }
}
