﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio
{
    public interface ISisegoCotizadoBl
    {
        ProcesoResponse AgregarSisegoCotizado(SisegoCotizadoDtoRequest resquest);

        ProcesoResponse EliminarSisegoCotizado(SisegoCotizadoDtoRequest resquest);
        List<SisegoCotizadoDtoResponse> ListarSisegosCotizados(SisegoCotizadoDtoRequest resquest);

        SisegoCotizadoPaginadoDtoResponse ListarSisegoCotizadoPaginado(SisegoCotizadoDtoRequest resquest);
    }
}
