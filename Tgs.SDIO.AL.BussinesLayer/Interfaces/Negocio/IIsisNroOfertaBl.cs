﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio
{
    public interface IIsisNroOfertaBl
    {
        ProcesoResponse AgregarIsisNroOferta(IsisNroOfertaDtoRequest resquest);
        ProcesoResponse EliminarIsisNroOferta(IsisNroOfertaDtoRequest resquest);
        List<IsisNroOfertaDtoResponse> ListarNroOferta(IsisNroOfertaDtoRequest resquest);
        IsisNroOfertaPaginadoDtoResponse ListarNroOfertaPaginado(IsisNroOfertaDtoRequest resquest);
    }
}
