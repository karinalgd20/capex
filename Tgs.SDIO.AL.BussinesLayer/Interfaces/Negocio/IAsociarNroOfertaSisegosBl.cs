﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio
{
    public interface IAsociarNroOfertaSisegosBl
    {
        
        ProcesoResponse AgregarOfertaSisegos(AsociarNroOfertaSisegosDtoRequest resquest);
        ProcesoResponse EliminarOfertaSisego(AsociarNroOfertaSisegosDtoRequest resquest);
        List<AsociarNroOfertaSisegosDtoResponse> ListarOfertaSisegos(AsociarNroOfertaSisegosDtoRequest resquest);
        AsociarOfertaSisegosPaginadoDtoResponse ListarOfertaSisegosPaginado(AsociarNroOfertaSisegosDtoRequest resquest);

        IsisNroOfertaDtoResponse ObtenerNroOfertaById(AsociarNroOfertaSisegosDtoRequest request);
        SisegoCotizadoDtoResponse ObtenerCodSisegoById(AsociarNroOfertaSisegosDtoRequest resquest);
    }
}
