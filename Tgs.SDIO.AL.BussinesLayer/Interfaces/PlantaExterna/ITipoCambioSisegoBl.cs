﻿using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.PlantaExterna
{
    public interface ITipoCambioSisegoBl
    {
        TipoCambioSisegoPaginadoDtoResponse ListarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest);
        ProcesoResponse ActualizarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest);
        TipoCambioSisegoDtoResponse ObtenerTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest);
        ProcesoResponse RegistrarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest);
    }
}
