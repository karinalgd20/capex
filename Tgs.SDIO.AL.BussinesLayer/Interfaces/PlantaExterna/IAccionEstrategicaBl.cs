﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.PlantaExterna
{
    public interface IAccionEstrategicaBl
    {
        List<ComboDtoResponse> ListarAccionEstrategica(AccionEstrategicaDtoRequest accionRequest);
    }
}
