﻿using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.PlantaExterna
{
    public interface INoPepsBl
    {
        NoPepsDtoResponse ObtenerNoPeps(NoPepsDtoRequest noPepsRequest);

        ProcesoResponse ActualizarNoPeps(NoPepsDtoRequest noPepsRequest);

        ProcesoResponse EliminarNoPeps(NoPepsDtoRequest noPepsRequest);

        ProcesoResponse RegistrarNoPeps(NoPepsDtoRequest noPepsRequest);

        NoPepsPaginadoDtoResponse ListarNoPeps(NoPepsDtoRequest noPepsRequest);
    }
}
