﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface ISedeBl
    {
        ProcesoResponse RegistrarSede(SedeDtoRequest request);
        ProcesoResponse ActualizarSede(SedeDtoRequest request);
        ProcesoResponse EliminarSede(SedeDtoRequest request);
        ProcesoResponse AgregarSedeInstalacion(SedeDtoRequest request);
        ProcesoResponse EliminarServicios(ServicioSedeInstalacionDtoRequest request);
        SedePaginadoDtoResponse ListarSedeInstalacion(OportunidadDtoRequest oportunidad);
        SedePaginadoDtoResponse ListarSedeCliente(OportunidadDtoRequest request);
        ClienteDtoResponse ObtenerClientePorIdOportunidad(OportunidadDtoRequest request);
        SedeDtoResponse ObtenerSedePorId(SedeDtoRequest request);
        ServicioSedeInstalacionDtoResponse ObtenerServicioSedeInstalacionPorId(ServicioSedeInstalacionDtoRequest request);
        SedeInstalacionDtoResponse obtenerSedeInstalada(int IdSede, int IdOportunidad);
        UbigeoDtoResponse ObtenerCodDetalleUbigeo(int Id);
        SedePaginadoDtoResponse ListarSedeServiciosPaginado(SedeDtoRequest request);
        SedeDtoResponse DireccionBuscar(SedeDtoRequest request);
        SedeDtoResponse DetalleSedePorId(SedeDtoRequest request);
    }
}