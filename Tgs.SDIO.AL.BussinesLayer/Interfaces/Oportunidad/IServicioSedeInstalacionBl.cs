﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IServicioSedeInstalacionBl
    {
        ProcesoResponse RegistrarServicioSedeInstalacion(ServicioSedeInstalacionDtoRequest servicioSedeInstalacion);
    }
}
