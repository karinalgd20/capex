﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IVisitaBl
    {
        ProcesoResponse RegistrarVisita(VisitaDtoRequest visita);
        ProcesoResponse ActualizarVisita(VisitaDtoRequest visita);
        VisitaDtoResponse ObtenerVisitaPorIdOportunidad(VisitaDtoRequest visita);
    }
}