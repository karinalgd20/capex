﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IOportunidadServicioCMIBl
    {
        ProcesoResponse RegistrarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidad);
        ProcesoResponse ActualizarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidad);
        OportunidadServicioCMIPaginadoDtoResponse ListarOportunidadServicioCMIPaginado(OportunidadServicioCMIDtoRequest oportunidad);
        List<OportunidadServicioCMIDtoResponse> ObtenerTodosOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidad);
        OportunidadServicioCMIDtoResponse ObtenerOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidad);
    }
}
