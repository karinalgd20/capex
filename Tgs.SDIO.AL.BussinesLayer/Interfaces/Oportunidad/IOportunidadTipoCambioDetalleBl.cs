﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IOportunidadTipoCambioDetalleBl
    {
        ProcesoResponse RegistrarOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest oportunidadTipoCambioDetalle);
        OportunidadTipoCambioDetalleDtoResponse ObtenerOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest oportunidadTipoCambioDetalle);
        ProcesoResponse ActualizarOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest oportunidadTipoCambioDetalle);

        List<OportunidadTipoCambioDetalleDtoResponse> ObtenerTodasOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest OportunidadTipoCambioDetalle);
    }
}
