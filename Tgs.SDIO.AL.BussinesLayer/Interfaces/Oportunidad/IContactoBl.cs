﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IContactoBl
    {
        ProcesoResponse RegistrarContacto(ContactoDtoRequest contacto);
        ProcesoResponse ActualizarContacto(ContactoDtoRequest contacto);
        ProcesoResponse InactivarContacto(ContactoDtoRequest contacto);
        ContactoPaginadoDtoResponse ListarContactoPaginado(ContactoDtoRequest contacto);
        ContactoDtoResponse ObtenerContactoPorId(ContactoDtoRequest contacto);
    }
}