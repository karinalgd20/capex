﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IOportunidadParametroBl
    {
        ProcesoResponse RegistrarOportunidadParametro(OportunidadParametroDtoRequest oportunidadParametro);
        List<OportunidadParametroDtoResponse> ObtenerParametroPorOportunidadLineaNegocio(OportunidadParametroDtoRequest oportunidadParametro);
        OportunidadParametroPaginadoDtoResponse ListaOportunidadParametroPaginado(OportunidadParametroDtoRequest oportunidadParametro);
        ProcesoResponse ActualizarOportunidadParametro(OportunidadParametroDtoRequest oportunidadParametro);
        OportunidadParametroDtoResponse ObtenerOportunidadParametro(OportunidadParametroDtoRequest oportunidadParametro);
    }
}
