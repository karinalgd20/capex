﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IServicioConceptoProyectadoBl
    {
        List<ServicioConceptoProyectadoDtoResponse> ListarConceptos(ServicioConceptoProyectadoDtoRequest concepto);
        ProcesoResponse RegistrarConceptoPersonalizado(ServicioConceptoProyectadoDtoRequest concepto);
        ProcesoResponse ActualizarConceptoPersonalizado(ServicioConceptoProyectadoDtoRequest concepto);
        ServicioConceptoProyectadoDtoResponse ObtenerConceptoPersonalizado(ServicioConceptoProyectadoDtoRequest concepto);
        List<ServicioConceptoProyectadoDtoResponse> DatosPorVersion(ServicioConceptoProyectadoDtoRequest concepto);

        decimal? IndicadorCostoDirecto(ServicioConceptoProyectadoDtoRequest concepto);
        decimal? IndicadorIngresoTotal(ServicioConceptoProyectadoDtoRequest concepto);
        decimal? IndicadorCapex(ServicioConceptoProyectadoDtoRequest concepto);
    }
}
