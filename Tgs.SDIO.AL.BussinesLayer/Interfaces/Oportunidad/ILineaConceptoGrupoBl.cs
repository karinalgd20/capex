﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface ILineaConceptoGrupoBl
    {
        List<LineaConceptoGrupoDtoResponse> ListarLineaConceptoGrupo(LineaConceptoGrupoDtoRequest linea);

    }
}
