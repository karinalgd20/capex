﻿using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface ISituacionActualBl
    {
        /// <summary>
        /// Obtiene un objeto situacion actual por el id de la oportunidad y el tipo.
        /// </summary>
        /// <param name="request">Parametro para el filtro</param>
        /// <returns>Retorna un objeto de tipo situación actual.</returns>
        SituacionActualDtoResponse ObtenerPorIdOportunidad(SituacionActualDtoRequest request);

        /// <summary>
        /// Registra o actualiza la situacion actual de la oportunidad.
        /// </summary>
        /// <param name="">Recepciona la información de la situacion actual</param>
        /// <returns>Retorma un mensaje de la operación realizada.</returns>
        ProcesoResponse RegistrarSituacionActual(SituacionActualDtoRequest situacionActual);
    }
}