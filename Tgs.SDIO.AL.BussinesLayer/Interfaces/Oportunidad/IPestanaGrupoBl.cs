﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IPestanaGrupoBl
    {
        List<ListaDtoResponse> ListarPestanaGrupo(PestanaGrupoDtoRequest grupo);
        PestanaGrupoDtoResponse ObtenerPestanaGrupo(PestanaGrupoDtoRequest pestanaGrupo);
    }
}
