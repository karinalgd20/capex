﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IVisitaDetalleBl
    {
        ProcesoResponse RegistrarVisitaDetalle(VisitaDetalleDtoRequest visitaDetalle);
        ProcesoResponse InhabilitarVisitaDetalle(VisitaDetalleDtoRequest visitaDetalle);
        VisitaDetallePaginadoDtoResponse ListarVisitaDetallePaginado(VisitaDetalleDtoRequest visitaDetalle);
    }
}