﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IPestanaGrupoTipoBl
    {
        List<ListaDtoResponse> ListarPestanaGrupoTipo(PestanaGrupoTipoDtoRequest pestanaGrupoTipo);
    }
}
