﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface ISubServicioDatosCapexBl
    {
        ProcesoResponse RegistrarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio);
        ProcesoResponse ActualizarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio);
        SubServicioDatosCapexDtoResponse ObtenerSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio);
        SubServicioDatosCapexDtoResponse ListarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio);
        SubServicioDatosCapexDtoResponse CantidadEstudiosEsp(SubServicioDatosCapexDtoRequest dto);
        SubServicioDatosCapexDtoResponse CantidadEquiposEsp(SubServicioDatosCapexDtoRequest dto);
        SubServicioDatosCapexDtoResponse CantidadRouters(SubServicioDatosCapexDtoRequest dto);
        SubServicioDatosCapexDtoResponse CantidadModems(SubServicioDatosCapexDtoRequest dto);
        SubServicioDatosCapexDtoResponse CantidadEquiposSeguridad(SubServicioDatosCapexDtoRequest dto);
        SubServicioDatosCapexDtoResponse CantidadCapex(SubServicioDatosCapexDtoRequest dto);
        SubServicioDatosCapexDtoResponse CantidadSolarWindVPN(SubServicioDatosCapexDtoRequest dto);
        SubServicioDatosCapexDtoResponse CantidadSatelitales(SubServicioDatosCapexDtoRequest dto);

    }
}
