﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IOportunidadFlujoCajaDetalleBl
    {
        ProcesoResponse RegistrarOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalle);
        OportunidadFlujoCajaDetalleDtoResponse ObtenerOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest flujocaja);
        OportunidadFlujoCajaDetalleDtoResponse GeneraProyectadoOportunidadFlujoCaja(OportunidadFlujoCajaDetalleDtoRequest flujocaja);
        List<OportunidadFlujoCajaDetalleDtoResponse> ListaOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaDetalleDtoRequest flujocaja);
        List<OportunidadFlujoCajaDetalleDtoResponse> ListarOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest flujocaja);
        ProcesoResponse ActualizarOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalle);
    }
}
