﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IOportunidadFlujoCajaConfiguracionBl
    {
        ProcesoResponse RegistrarOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);
        ProcesoResponse ActualizarOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);
        OportunidadFlujoCajaConfiguracionDtoResponse ObtenerOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);
        List<OportunidadFlujoCajaConfiguracionDtoResponse> ListaOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);
        List<OportunidadFlujoCajaConfiguracionDtoResponse> ListarOportunidadFlujoCajaConfiguracionBandeja(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);
        OportunidadFlujoCajaConfiguracionDtoResponse ObtenerOportunidadFlujoCajaConfiguracionPorId(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja);
    }
}
