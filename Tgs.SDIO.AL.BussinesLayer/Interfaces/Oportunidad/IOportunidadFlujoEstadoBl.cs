﻿using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IOportunidadFlujoEstadoBl
    {
        OportunidadFlujoEstadoDtoResponse ObtenerOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado);

        ProcesoResponse RegistrarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado);

        ProcesoResponse ActualizarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado);

        OportunidadFlujoEstadoDtoResponse ObtenerOportunidadFlujoEstadoId(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado);

        ProcesoResponse InsertarConfiguracionFlujo(OportunidadLineaNegocioDtoRequest oportunidadFlujoEstado);
    
    }
}

