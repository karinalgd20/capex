﻿using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface ICotizacionBl
    {
        CotizacionPaginadoDtoResponse ListarCotizacionPaginado(CotizacionDtoRequest Cotizacion);
    }
}
