﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface ISubServicioDatosCaratulaBl
    {
        ProcesoResponse RegistrarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio);
        ProcesoResponse ActualizarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio);
        ProcesoResponse EliminarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio);
        SubServicioDatosCaratulaDtoResponse ObtenerSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio);
        List<SubServicioDatosCaratulaDtoResponse> ListarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio);

    }
}
