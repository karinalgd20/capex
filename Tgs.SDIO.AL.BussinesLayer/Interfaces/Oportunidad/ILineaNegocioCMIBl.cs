﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface ILineaNegocioCMIBl
    {
         ProcesoResponse ActualizarLineaNegocioCMI(LineaNegocioCMIDtoRequest lineaNegocioCMI);
         List<ListaDtoResponse> ListarLineaNegocioCMI(LineaNegocioCMIDtoRequest lineaNegocioCMI);
         LineaNegocioCMIDtoResponse ObtenerLineaNegocioCMI(LineaNegocioCMIDtoRequest lineaNegocioCMI);
         ProcesoResponse RegistrarLineaNegocioCMI(LineaNegocioCMIDtoRequest lineaNegocioCMI);
    }
}

