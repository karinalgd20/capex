﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IOportunidadFlujoCajaBl
    {
        ProcesoResponse RegistrarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        ProcesoResponse ActualizarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        ProcesoResponse EliminarCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio);
        OportunidadFlujoCajaDtoResponse ObtenerOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        List<OportunidadFlujoCajaDtoResponse> ListaOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        List<OportunidadFlujoCajaDtoResponse> ListarOportunidadFlujoCajaBandeja(OportunidadFlujoCajaDtoRequest flujocaja);
        List<ListaDtoResponse> ListaAnoMesProyecto(OportunidadFlujoCajaDtoRequest flujocaja);
        List<OportunidadFlujoCajaDtoResponse> GeneraCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio);
        List<OportunidadFlujoCajaDtoResponse> GeneraServicio(OportunidadFlujoCajaDtoRequest casonegocio);
        OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio);
        OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio);
       
        OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio);
        OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio);
        ProcesoResponse ActualizarPeriodoOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
		List<OportunidadFlujoCajaDtoResponse> ObtenerTodosOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        OportunidadFlujoCajaPaginadoDtoResponse ListarSubservicioPaginado(OportunidadFlujoCajaDtoRequest oportunidad);
        OportunidadFlujoCajaDtoResponse ObtenerSubServicio(OportunidadFlujoCajaDtoRequest solicitud);
        ProcesoResponse InhabilitarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        ProcesoResponse InhabilitarOportunidadEcapex(OportunidadFlujoCajaDtoRequest flujocaja);
        OportunidadFlujoCajaPaginadoDtoResponse ListarServiciosCircuitosPaginado(OportunidadFlujoCajaDtoRequest flujocaja);
        List<ListaDtoResponse> ListaServicioPorOportunidadLineaNegocio(OportunidadFlujoCajaDtoRequest flujocaja);
        ProcesoResponse RegistrarComponente(OportunidadFlujoCajaDtoRequest oportunidad);
        List<ListaDtoResponse> ListarPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        ProcesoResponse RegistrarServicioComponente(ServicioSubServicioDtoRequest servicioSubServicio);
        ProcesoResponse RegistrarCasoNegocioServicio(ServicioSubServicioDtoRequest servicioSubServicio);
        OportunidadFlujoCajaDtoResponse ObtenerServicioPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);

        OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadCaratulaTotal(OportunidadFlujoCajaDtoRequest casonegocio);

        OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadEcapexTotal(OportunidadFlujoCajaDtoRequest casonegocio);
    }
}