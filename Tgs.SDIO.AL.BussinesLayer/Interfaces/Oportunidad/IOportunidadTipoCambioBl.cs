﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IOportunidadTipoCambioBl
    {
        ProcesoResponse RegistrarOportunidadTipoCambio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio);
        OportunidadTipoCambioDtoResponse ObtenerOportunidadTipoCambio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio);
        ProcesoResponse ActualizarOportunidadTipoCambio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio);
        OportunidadTipoCambioDtoResponse ObtenerTipoCambioPorOportunidadLineaNegocio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio);
        OportunidadTipoCambioPaginadoDtoResponse ListaOportunidadTipoCambioPaginado(OportunidadTipoCambioDtoRequest oportunidadTipoCambio);

        OportunidadTipoCambioDtoResponse ObtenerOportunidadTipoCambioPorLineaNegocio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio);
    }
}
