﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IOportunidadBl
    {
        OportunidadDtoResponse ObtenerOportunidad(OportunidadDtoRequest oportunidad);

        ProcesoResponse RegistrarVersionOportunidad(OportunidadDtoRequest oportunidad);

        ProcesoResponse OportunidadGanador(OportunidadDtoRequest oportunidad);

        OportunidadPaginadoDtoResponse ListarOportunidadCabecera(OportunidadDtoRequest oportunidad);

        List<OportunidadDtoResponse> ListarProyectadoOIBDA(OportunidadDtoRequest oportunidad);

        ProcesoResponse OportunidadInactivar(OportunidadDtoRequest oportunidad);

        ProcesoResponse RegistrarOportunidad(OportunidadDtoRequest oportunidad);

        ProcesoResponse ActualizarOportunidad(OportunidadDtoRequest oportunidad);

        List<OportunidadDtoResponse> ObtenerVersiones(OportunidadDtoRequest oportunidad);

        ProcesoResponse NuevaVersion(OportunidadDtoRequest oportunidad);
    }
}

