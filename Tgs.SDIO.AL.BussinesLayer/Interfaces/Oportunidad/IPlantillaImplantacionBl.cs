﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IPlantillaImplantacionBl
    {
        ProcesoResponse RegistrarPlantillaImplantacion(PlantillaImplantacionDtoRequest plantillaImplantacion);
        ProcesoResponse InactivarPlantillaImplantacion(PlantillaImplantacionDtoRequest plantillaImplantacion);
        PlantillaImplantacionPaginadoDtoResponse ListarPlantillaImplantacionPaginado(PlantillaImplantacionDtoRequest filtro);
    }
}