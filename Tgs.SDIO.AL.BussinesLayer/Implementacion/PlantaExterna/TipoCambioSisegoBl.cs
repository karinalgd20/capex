﻿using System;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.PlantaExterna;
using Tgs.SDIO.AL.DataAccess.Interfaces.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;
using Tgs.SDIO.Entities.Entities.PlantaExterna;
using Tgs.SDIO.Util.Error.PlantaExterna;
using Tgs.SDIO.Util.Mensajes.PlantaExterna;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.PlantaExterna
{
    public  class TipoCambioSisegoBl : ITipoCambioSisegoBl
    {
        private readonly ITipoCambioSisegoDal iTipoCambioSisegoDal; 

        ProcesoResponse respuesta = new ProcesoResponse();

        public TipoCambioSisegoBl(ITipoCambioSisegoDal tipoCambioSisegoDal)
        {
            iTipoCambioSisegoDal = tipoCambioSisegoDal; 
        }

        public TipoCambioSisegoPaginadoDtoResponse ListarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest)
        { 
            return iTipoCambioSisegoDal.ListarTipoCambioSisego(tipoCambioDtoRequest);
        }

        public ProcesoResponse ActualizarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest)
        {
            var tipoCambio = new TipoCambioSisego
            {
                Id = tipoCambioDtoRequest.Id,
                IdEstado = tipoCambioDtoRequest.IdEstado,
                FechaFin= Convert.ToDateTime(tipoCambioDtoRequest.FechaFin),
                FechaInicio= Convert.ToDateTime(tipoCambioDtoRequest.FechaInicio),
                IdUsuarioEdicion = tipoCambioDtoRequest.IdUsuarioEdicion,
                FechaEdicion = tipoCambioDtoRequest.FechaEdicion,
                Monto =tipoCambioDtoRequest.Monto
            };

            iTipoCambioSisegoDal.ActualizarPorCampos(tipoCambio,
                                                        x => x.IdEstado,
                                                        x => x.IdUsuarioEdicion,
                                                        x => x.Monto,
                                                        x => x.FechaInicio,
                                                        x => x.FechaFin,
                                                        x => x.FechaEdicion);

            iTipoCambioSisegoDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralPlantaExterna.DatosRegistrados;

            return respuesta;
        }

        public TipoCambioSisegoDtoResponse ObtenerTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest)
        {
            var consultaTipoCambio = iTipoCambioSisegoDal.GetFilteredAsNoTracking(
             x => x.Id == tipoCambioDtoRequest.Id
             ).FirstOrDefault();

            return new TipoCambioSisegoDtoResponse
            {
                Id = consultaTipoCambio.Id,
                IdEstado = consultaTipoCambio.IdEstado,
                Monto = consultaTipoCambio.Monto,
                FechaInicio= consultaTipoCambio.FechaInicio,
                FechaFin= consultaTipoCambio.FechaFin
            };
        }

        public ProcesoResponse RegistrarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest)
        {
            var validarFechas = ValidarTipoCambioSisego(tipoCambioDtoRequest);

            if (validarFechas.TipoRespuesta.Equals(Proceso.Invalido))
            {
                return validarFechas;
            }

            var tipoCambio = new TipoCambioSisego
            { 
                IdEstado = tipoCambioDtoRequest.IdEstado,
                FechaFin = Convert.ToDateTime(tipoCambioDtoRequest.FechaFin),
                FechaInicio = Convert.ToDateTime(tipoCambioDtoRequest.FechaInicio),
                IdUsuarioCreacion = tipoCambioDtoRequest.IdUsuarioCreacion,
                FechaCreacion = tipoCambioDtoRequest.FechaCreacion,
                Monto = tipoCambioDtoRequest.Monto
            };

            iTipoCambioSisegoDal.Add(tipoCambio);
            iTipoCambioSisegoDal.UnitOfWork.Commit();

            respuesta.Id = tipoCambio.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;
        }

        private ProcesoResponse ValidarTipoCambioSisego(TipoCambioSisegoDtoRequest tipoCambioDtoRequest)
        {
            var listarFechas = iTipoCambioSisegoDal.GetFilteredAsNoTracking(
               x => x.FechaInicio <= tipoCambioDtoRequest.FechaInicio &&
               x.FechaFin >= tipoCambioDtoRequest.FechaInicio  
               ).FirstOrDefault();

            if (listarFechas!=null)
            {
                return new ProcesoResponse
                {
                    TipoRespuesta = Proceso.Invalido,
                    Mensaje = ErrorValidacionPlantaExterna.ErrorFechaExiste
                };
            }

            return new ProcesoResponse
            {
                TipoRespuesta = Proceso.Valido
            }; 
        }

    }
}
