﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.PlantaExterna;
using Tgs.SDIO.AL.DataAccess.Interfaces.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.PlantaExterna
{
    public class AccionEstrategicaBl : IAccionEstrategicaBl
    {
        private readonly IAccionEstrategicaDal iAccionEstrategicaDal;

        ProcesoResponse respuesta = new ProcesoResponse();

        public AccionEstrategicaBl(IAccionEstrategicaDal accionEstrategicaDal)
        {
            iAccionEstrategicaDal = accionEstrategicaDal;
        }

        public List<ComboDtoResponse> ListarAccionEstrategica(AccionEstrategicaDtoRequest accionRequest)
        {
            var listaAccionEstrategica = iAccionEstrategicaDal.GetFilteredAsNoTracking(x => x.IdEstado == accionRequest.IdEstado).ToList();

            return listaAccionEstrategica.Select(x => new ComboDtoResponse
            {
                id = x.Nombre,
                label = x.Nombre
            }).ToList();
        }


    }
}
