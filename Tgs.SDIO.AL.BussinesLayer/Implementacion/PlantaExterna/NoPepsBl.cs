﻿using System;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Request.PlantaExterna;
using Tgs.SDIO.DataContracts.Dto.Response.PlantaExterna;
using Tgs.SDIO.Entities.Entities.PlantaExterna;
using Tgs.SDIO.AL.DataAccess.Interfaces.PlantaExterna;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.PlantaExterna;
using Tgs.SDIO.Util.Error.PlantaExterna;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.PlantaExterna
{
    public class NoPepsBl : INoPepsBl
    {
        readonly INoPepsDal iNoPepsDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public NoPepsBl(INoPepsDal noPepsDal)
        {
            iNoPepsDal = noPepsDal;
        }
                
        public NoPepsDtoResponse ObtenerNoPeps(NoPepsDtoRequest noPepsRequest)
        {
            var consultaPorNoPep = iNoPepsDal.GetFilteredAsNoTracking(
             x => x.Id == noPepsRequest.Id
             ).FirstOrDefault();

            return new NoPepsDtoResponse
            {
                Id= consultaPorNoPep.Id,
                IdEstado= consultaPorNoPep.IdEstado,
                CodigoPep= consultaPorNoPep.CodigoPep
            };
        }

        public ProcesoResponse ActualizarNoPeps(NoPepsDtoRequest noPepsRequest)
        {
            var consultaPorNoPep = iNoPepsDal.GetFilteredAsNoTracking(
             x => x.CodigoPep == noPepsRequest.CodigoPep && x.Id != noPepsRequest.Id
             ).FirstOrDefault();

            if (consultaPorNoPep != null)
            {
                return new ProcesoResponse
                {
                    Id = 0,
                    TipoRespuesta = Proceso.Invalido,
                    Mensaje = ErrorValidacionPlantaExterna.ErrorPepRepetida
                };
            }

            var noPeps = new NoPeps()
            {
                Id = noPepsRequest.Id,
                IdEstado = noPepsRequest.IdEstado,
                CodigoPep= noPepsRequest.CodigoPep,
                IdUsuarioEdicion = noPepsRequest.IdUsuarioEdicion,
                FechaEdicion = noPepsRequest.FechaEdicion
            };

            iNoPepsDal.ActualizarPorCampos(noPeps,
                                        x => x.IdEstado,
                                        x=>x.CodigoPep,
                                        x => x.IdUsuarioEdicion,
                                        x => x.FechaEdicion);

            iNoPepsDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralPlantaExterna.DatosRegistrados; 
           
            return respuesta;
        }

        public ProcesoResponse EliminarNoPeps(NoPepsDtoRequest noPepsRequest)
        {
            var noPeps = new NoPeps()
            {
                Id = noPepsRequest.Id
            };
                   
            iNoPepsDal.Remove(noPeps);

            iNoPepsDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralPlantaExterna.DatosRegistrados;

            return respuesta;
        }

        public ProcesoResponse RegistrarNoPeps(NoPepsDtoRequest noPepsRequest)
        {
            var consultaPorNoPep = iNoPepsDal.GetFilteredAsNoTracking(
             x => x.CodigoPep == noPepsRequest.CodigoPep
             ).FirstOrDefault();

            if (consultaPorNoPep != null)
            {
                return new ProcesoResponse {
                    Id = 0,
                    TipoRespuesta = Proceso.Invalido,
                    Mensaje = ErrorValidacionPlantaExterna.ErrorPepRepetida
                };
            }

            var noPeps = new NoPeps()
            {
                Id = noPepsRequest.Id,
                CodigoPep = noPepsRequest.CodigoPep,
                IdEstado = noPepsRequest.IdEstado,
                IdUsuarioCreacion = noPepsRequest.IdUsuarioCreacion,
                FechaCreacion = DateTime.Now
            };

            iNoPepsDal.Add(noPeps);
            iNoPepsDal.UnitOfWork.Commit();

            respuesta.Id = noPeps.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk; 

            return respuesta; 
        }

        public NoPepsPaginadoDtoResponse ListarNoPeps(NoPepsDtoRequest noPepsRequest)
        {
            return iNoPepsDal.ListarNoPeps(noPepsRequest);
        } 
    }
}
