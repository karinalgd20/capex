﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class ConfiguracionEtapaBl : IConfiguracionEtapaBl
    {
        readonly IConfiguracionEtapaDal iConfiguracionEtapaDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public ConfiguracionEtapaBl(IConfiguracionEtapaDal IConfiguracionEtapaDal)
        {
            iConfiguracionEtapaDal = IConfiguracionEtapaDal;
        }

        public List<ConfiguracionEtapaDtoResponse> ListarEtapas(ConfiguracionEtapaDtoRequest request)
        {
            List<ConfiguracionEtapaDtoResponse> ListaEtapa = 
                        iConfiguracionEtapaDal.GetFiltered(e => e.IdEstado == Generales.Estados.Activo 
                        && e.IdTipoCompra == request.IdTipoCompra 
                        && e.IdTipoCosto == request.IdTipoCosto
                        && e.Orden < request.Orden
                    ).Select(e =>
                    new ConfiguracionEtapaDtoResponse
                    {
                        Descripcion = e.Descripcion,
                        Orden = e.Orden
                    }).ToList();

            return ListaEtapa;
        }
    }
}
