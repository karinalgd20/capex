﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Compra;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class AnexoArchivoBl : IAnexoArchivoBl
    {
        readonly IAnexoArchivoDal iAnexoArchivoDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public AnexoArchivoBl(IAnexoArchivoDal IAnexoArchivoDal)
        {
            iAnexoArchivoDal = IAnexoArchivoDal;
        }

        //public GestionDtoResponse ObtenerGestion(GestionDtoRequest request)
        //{
        //    GestionDtoResponse oGestion = iGestionDal.GetFiltered(g => g.IdPeticionCompra == request.IdPeticionCompra).Select(g => new GestionDtoResponse
        //    {
        //        IdGestionPeticionCompra = g.IdGestionPeticionCompra,
        //        IdPeticionCompra = g.IdPeticionCompra,
        //        Cesta = g.Cesta,
        //        CestaLiberada = g.CestaLiberada,
        //        EnvioPedido = g.EnvioPedido,
        //        FechaActaRecibida = g.FechaActaRecibida,
        //        FechaAtencionGestor = g.FechaAtencionGestor,
        //        FechaCesta = g.FechaCesta,
        //        FechaConfirmacion = g.FechaConfirmacion,
        //        FechaPedido = g.FechaPedido,
        //        MontoPedido = g.MontoPedido,
        //        NumeroPedido = g.NumeroPedido,
        //        PosicionCesta = g.PosicionCesta,
        //        ContratoMarco = g.ContratoMarco,
        //        PosicionPedido = g.PosicionPedido,
        //        SaldoPedido = g.SaldoPedido
        //    }).FirstOrDefault();

        //    if (oGestion == null) oGestion = new GestionDtoResponse();

        //    return oGestion;
        //}

        public ProcesoResponse RegistrarAnexoArchivo(AnexoArchivoDtoRequest request)
        {
            ProcesoResponse oProcesoResponse = new ProcesoResponse();
            try
            {
                AnexosArchivo oAnexoArchivo = new AnexosArchivo
                {
                    IdAnexoPeticionCompras = request.IdAnexoPeticionCompras,
                    ArchivoAdjunto = request.ArchivoAdjunto
                };

                oAnexoArchivo.IdEstado = Generales.Estados.Activo;
                oAnexoArchivo.IdUsuarioCreacion = request.IdUsuario;
                oAnexoArchivo.FechaCreacion = DateTime.Now;

                iAnexoArchivoDal.Add(oAnexoArchivo);

                iAnexoArchivoDal.UnitOfWork.Commit();

                oProcesoResponse.Id = oAnexoArchivo.IdAnexoArchivo;
                oProcesoResponse.TipoRespuesta = Proceso.Valido;
                oProcesoResponse.Mensaje = MensajesGeneralCompra.RegistrarAnexos;
            }
            catch (Exception ex)
            {
                oProcesoResponse.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioCompra.ErrorAnexo, null, Generales.Sistemas.Compra);
            }

            return oProcesoResponse;
        }

        public AnexoArchivoDtoResponse ObtenerAnexoArchivo(int iIdAnexoPeticionCompra) {
            return iAnexoArchivoDal.ObtenerAnexoArchivo(iIdAnexoPeticionCompra);
        }
    }
}
