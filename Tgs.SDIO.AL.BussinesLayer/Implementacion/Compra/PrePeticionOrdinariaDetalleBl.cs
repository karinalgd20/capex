﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Compra;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class PrePeticionOrdinariaDetalleBl : IPrePeticionOrdinariaDetalleBl
    {
        readonly IPrePeticionOrdinariaDetalleDal iPrePeticionOrdinariaDetalleDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public PrePeticionOrdinariaDetalleBl(IPrePeticionOrdinariaDetalleDal IPrePeticionOrdinariaDetalleDal)
        {
            iPrePeticionOrdinariaDetalleDal = IPrePeticionOrdinariaDetalleDal;
        }

        public PrePeticionOrdinariaDetalleDtoResponse ObtenerDetalleOrdinaria(PrePeticionOrdinariaDetalleDtoRequest request)
        {
            return iPrePeticionOrdinariaDetalleDal.ObtenerDetalleOrdinaria(request);
        }

        public ProcesoResponse RegistrarPrePeticionOrdinariaDetalle(PrePeticionOrdinariaDetalleDtoRequest request)
        {
            ProcesoResponse oProcesoResponse = new ProcesoResponse();
            try
            {
                bool oExisteRegistro = iPrePeticionOrdinariaDetalleDal.GetFiltered(g => g.Id == request.Id).Any();
                bool oEsNuevo = (request.Id == 0);

                PrePeticionOrdinariaDetalle oPrePeticionOrdinariaDetalle = new PrePeticionOrdinariaDetalle
                {
                    Id = request.Id,
                    IdCabecera = request.IdCabecera,
                    TipoCosto = request.TipoCosto,
                    ProductManager = request.ProductManager,
                    Descripcion = request.Descripcion,
                    IdProveedor = request.IdProveedor,
                    Moneda = request.Moneda,
                    Monto = request.Monto,
                    CoordinadorCompras = request.CoordinadorCompras,
                    CompradorAsignado = request.CompradorAsignado,
                    GrupoCompra = request.GrupoCompra,
                    PQAdjudicado = request.PQAdjudicado,
                    PliegoTecnico = request.PliegoTecnico,
                    CotizacionAdjunta = request.CotizacionAdjunta,
                    PosicionesOC = request.PosicionesOC,
                    FechaEntregaOC = request.FechaEntregaOC,
                    Observaciones = request.Observaciones,
                    IdLineaProducto = request.IdLineaProducto,
                    GrupoCompraConfirmado = request.GrupoCompraConfirmado
                };

                if (oEsNuevo)
                {
                    oPrePeticionOrdinariaDetalle.IdEstado = Generales.Estados.Activo;
                    oPrePeticionOrdinariaDetalle.IdUsuarioCreacion = request.IdUsuario;
                    oPrePeticionOrdinariaDetalle.FechaCreacion = DateTime.Now;

                    iPrePeticionOrdinariaDetalleDal.Add(oPrePeticionOrdinariaDetalle);
                }
                else
                {
                    oPrePeticionOrdinariaDetalle.FechaEdicion = DateTime.Now;
                    oPrePeticionOrdinariaDetalle.IdUsuarioEdicion = request.IdUsuario;

                    iPrePeticionOrdinariaDetalleDal.ActualizarPorCampos(oPrePeticionOrdinariaDetalle,
                                                            r => r.TipoCosto,
                                                            r => r.ProductManager,
                                                            r => r.Descripcion,
                                                            r => r.IdProveedor,
                                                            r => r.Moneda,
                                                            r => r.Monto,
                                                            r => r.CoordinadorCompras,
                                                            r => r.CompradorAsignado,
                                                            r => r.GrupoCompra,
                                                            r => r.PQAdjudicado,
                                                            r => r.PliegoTecnico,
                                                            r => r.CotizacionAdjunta,
                                                            r => r.PosicionesOC,
                                                            r => r.FechaEntregaOC,
                                                            r => r.Observaciones,
                                                            r => r.IdUsuarioEdicion,
                                                            r => r.GrupoCompraConfirmado
                                                        );
                }

                iPrePeticionOrdinariaDetalleDal.UnitOfWork.Commit();

                oProcesoResponse.Id = oPrePeticionOrdinariaDetalle.Id;
                oProcesoResponse.TipoRespuesta = Proceso.Valido;
                oProcesoResponse.Mensaje = oEsNuevo ? MensajesGeneralCompra.RegistrarDetalleClienteProveedor : MensajesGeneralCompra.ActualizarDetalleClienteProveedor;
            }
            catch (Exception ex)
            {
                oProcesoResponse.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioCompra.ErrorDetalleClienteProveedor, null, Generales.Sistemas.Compra);
            }

            return oProcesoResponse;
        }

        public List<PrePeticionOrdinariaDetalleDtoResponse> ListarPrePeticionOrdinaria(PrePeticionOrdinariaDetalleDtoRequest request) {
            return iPrePeticionOrdinariaDetalleDal.ListarPrePeticionOrdinaria(request);
        }
    }
}
