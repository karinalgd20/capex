﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class CentroCostoRecursoBl : ICentroCostoRecursoBl
    {
        readonly ICentroCostoRecursoDal iCentroCostoRecursoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public CentroCostoRecursoBl(ICentroCostoRecursoDal ICentroCostoRecursoDal)
        {
            iCentroCostoRecursoDal = ICentroCostoRecursoDal;
        }

        public CentroCostoRecursoPaginadoDtoResponse ListarCentroCostoRecursoPaginado(CentroCostoRecursoDtoRequest request)
        {
            var centroCostoRecursopaginadoDtoResponse = new CentroCostoRecursoPaginadoDtoResponse();

            centroCostoRecursopaginadoDtoResponse.ListCeCoDtoResponse = iCentroCostoRecursoDal.ListarCentroCostoRecurso(request);

            if (centroCostoRecursopaginadoDtoResponse.ListCeCoDtoResponse.Count > Numeric.Cero)
            {
                centroCostoRecursopaginadoDtoResponse.TotalItemCount = centroCostoRecursopaginadoDtoResponse.ListCeCoDtoResponse[0].TotalRow;
            }

            return centroCostoRecursopaginadoDtoResponse;
        }

    }
}
