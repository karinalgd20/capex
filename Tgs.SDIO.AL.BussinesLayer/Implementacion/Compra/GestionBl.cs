﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Compra;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class GestionBl : IGestionBl
    {
        readonly IGestionDal iGestionDal;
        readonly IEtapaPeticionCompraBl iEtapaPeticionCompraBl;
        readonly IPeticionCompraDal iPeticionCompraDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public GestionBl(IGestionDal IGestionDal, IEtapaPeticionCompraBl IEtapaPeticionCompraBl, IPeticionCompraDal IPeticionCompraDal)
        {
            iGestionDal = IGestionDal;
            iEtapaPeticionCompraBl = IEtapaPeticionCompraBl;
            iPeticionCompraDal = IPeticionCompraDal;
        }

        public GestionDtoResponse ObtenerGestion(GestionDtoRequest request)
        {
            GestionDtoResponse oGestion = iGestionDal.GetFiltered(g => g.IdPeticionCompra == request.IdPeticionCompra).Select(g => new GestionDtoResponse
            {
                IdGestionPeticionCompra = g.IdGestionPeticionCompra,
                IdPeticionCompra = g.IdPeticionCompra,
                Cesta = g.Cesta,
                CestaLiberada = g.CestaLiberada,
                EnvioPedido = g.EnvioPedido,
                FechaActaRecibida = g.FechaActaRecibida,
                FechaAtencionGestor = g.FechaAtencionGestor,
                FechaCesta = g.FechaCesta,
                FechaConfirmacion = g.FechaConfirmacion,
                FechaPedido = g.FechaPedido,
                MontoPedido = g.MontoPedido,
                NumeroPedido = g.NumeroPedido,
                PosicionCesta = g.PosicionCesta,
                ContratoMarco = g.ContratoMarco,
                PosicionPedido = g.PosicionPedido,
                SaldoPedido = g.SaldoPedido,
                IdTipoMoneda = g.IdTipoMoneda,
                CodigoConfirmacion = g.CodigoConfirmacion
            }).FirstOrDefault();

            if (oGestion == null) oGestion = new GestionDtoResponse();

            return oGestion;
        }

        public ProcesoResponse RegistrarGestion(GestionDtoRequest request)
        {
            ProcesoResponse oProcesoResponse = new ProcesoResponse();
            try
            {
                bool oEsNuevo = (request.IdGestionPeticionCompra == 0);

                GestionPeticionCompras oGestion = new GestionPeticionCompras
                {
                    IdGestionPeticionCompra = request.IdGestionPeticionCompra,
                    IdPeticionCompra = request.IdPeticionCompra,
                    Cesta = request.Cesta,
                    CestaLiberada = request.CestaLiberada,
                    EnvioPedido = request.EnvioPedido,
                    FechaActaRecibida = request.FechaActaRecibida,
                    FechaAtencionGestor = request.FechaAtencionGestor,
                    FechaCesta = request.FechaCesta,
                    FechaConfirmacion = request.FechaConfirmacion,
                    FechaPedido = request.FechaPedido,
                    MontoPedido = request.MontoPedido,
                    NumeroPedido = request.NumeroPedido,
                    PosicionCesta = request.PosicionCesta,
                    ContratoMarco = request.ContratoMarco,
                    PosicionPedido = request.PosicionPedido,
                    SaldoPedido = request.SaldoPedido,
                    IdTipoMoneda = request.IdTipoMoneda,
                    CodigoConfirmacion = request.CodigoConfirmacion
                };

                if (oEsNuevo)
                {
                    oGestion.IdUsuarioCreacion = request.IdUsuario;
                    oGestion.FechaCreacion = DateTime.Now;

                    iGestionDal.Add(oGestion);
                }
                else
                {
                    oGestion.FechaEdicion = DateTime.Now;
                    oGestion.IdUsuarioEdicion = request.IdUsuario;

                    iGestionDal.ActualizarPorCampos(oGestion,
                                                            g => g.IdGestionPeticionCompra,
                                                            g => g.IdPeticionCompra,
                                                            g => g.Cesta,
                                                            g => g.CestaLiberada,
                                                            g => g.EnvioPedido,
                                                            g => g.FechaActaRecibida,
                                                            g => g.FechaAtencionGestor,
                                                            g => g.FechaCesta,
                                                            g => g.FechaConfirmacion,
                                                            g => g.FechaPedido,
                                                            g => g.MontoPedido,
                                                            g => g.NumeroPedido,
                                                            g => g.PosicionCesta,
                                                            g => g.ContratoMarco,
                                                            g => g.PosicionPedido,
                                                            g => g.SaldoPedido,
                                                            g => g.FechaEdicion,
                                                            g => g.IdUsuarioEdicion,
                                                            g => g.IdTipoMoneda,
                                                            g => g.CodigoConfirmacion
                                                        );
                }

                iGestionDal.UnitOfWork.Commit();

                if (oEsNuevo || (request.IdOrden == 1 && request.IdEstadoEtapa == Util.Constantes.Compra.EstadosPeticionCompra.Subsanado))
                {

                    PeticionCompras oPeticionCompra = iPeticionCompraDal
                                                      .GetFilteredAsNoTracking(p => p.IdPeticionCompra == request.IdPeticionCompra)
                                                      .SingleOrDefault();

                    EtapaPeticionCompraDtoRequest EtapaPeticionCompraRequest = null;
                    if (oPeticionCompra.IdTipoCompra == Util.Constantes.Compra.TipoCompra.Ordinaria 
                        && oPeticionCompra.IdTipoCosto == Util.Constantes.Compra.TipoCosto.GastoOpex)    
                    {
                        EtapaPeticionCompraRequest = new EtapaPeticionCompraDtoRequest
                        {
                            IdPeticionCompra = request.IdPeticionCompra,
                            IdEstadoEtapa = Util.Constantes.Compra.EstadosPeticionCompra.Registrado,
                            IdUsuario = request.IdUsuario,
                            IdOrden = 3,
                            Comentario = "",
                            IrSiguienteEtapa = 1,
                            IdEtapaPeticionCompra = request.IdEtapaPeticionCompra
                        };
                    }
                    else
                    {
                        EtapaPeticionCompraRequest = new EtapaPeticionCompraDtoRequest
                        {
                            IdPeticionCompra = request.IdPeticionCompra,
                            IdEstadoEtapa = Util.Constantes.Compra.EstadosPeticionCompra.Registrado,
                            IdUsuario = request.IdUsuario,
                            IdOrden = 2,
                            Comentario = "",
                            IrSiguienteEtapa = 1,
                            IdEtapaPeticionCompra = request.IdEtapaPeticionCompra
                        };
                    }

                    iEtapaPeticionCompraBl.RegistrarEtapaPeticionCompra(EtapaPeticionCompraRequest);
                }

                oProcesoResponse.Id = oGestion.IdGestionPeticionCompra;
                oProcesoResponse.TipoRespuesta = Proceso.Valido;
                oProcesoResponse.Mensaje = oEsNuevo ? MensajesGeneralCompra.RegistrarDetalleClienteProveedor : MensajesGeneralCompra.ActualizarDetalleClienteProveedor;
            }
            catch (Exception ex)
            {
                oProcesoResponse.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioCompra.ErrorDetalleClienteProveedor, null, Generales.Sistemas.Compra);
            }

            return oProcesoResponse;
        }

    }
}
