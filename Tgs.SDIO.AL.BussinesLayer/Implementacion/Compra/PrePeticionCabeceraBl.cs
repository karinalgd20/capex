﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Compra;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class PrePeticionCabeceraBl : IPrePeticionCabeceraBl
    {
        readonly IPrePeticionCabeceraDal iPrePeticionCabeceraDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public PrePeticionCabeceraBl(IPrePeticionCabeceraDal IPrePeticionCabeceraDal)
        {
            iPrePeticionCabeceraDal = IPrePeticionCabeceraDal;
        }

        public PrePeticionCabeceraDtoResponse ObtenerPrePeticionCabecera(PrePeticionCabeceraDtoRequest request)
        {
            return iPrePeticionCabeceraDal.ObtenerPrePeticionCabecera(request);
        }

        public List<PrePeticionCabeceraDtoResponse> ListarPrePeticion(PrePeticionCabeceraDtoRequest request) {
            return iPrePeticionCabeceraDal.ListarPrePeticion(request);
        }

        public List<PrePeticionCabeceraDtoResponse> ListarPrePeticionCabecera(PrePeticionCabeceraDtoRequest request)
        {
            return iPrePeticionCabeceraDal.ListarPrePeticionCabecera(request);
        }

        public int ValidacionMontoLimite(PrePeticionCabeceraDtoRequest request) {
            return iPrePeticionCabeceraDal.ValidacionMontoLimite(request);
        }

        public string ObtenerSaldo(PrePeticionCabeceraDtoRequest request) {
            return iPrePeticionCabeceraDal.ObtenerSaldo(request);
        }
        public ProcesoResponse RegistrarPrePeticionCompraCabecera(PrePeticionCabeceraDtoRequest request)
        {
            ProcesoResponse oProcesoResponse = new ProcesoResponse();
            try
            {
                bool oExisteRegistro = iPrePeticionCabeceraDal.GetFiltered(g => g.Id == request.Id).Any();
                bool oEsNuevo = (request.Id == 0);

                PrePeticionCabecera oPrePeticionCabecera = new PrePeticionCabecera
                {
                    Id = request.Id,
                    IdCliente = request.IdCliente,
                    IdProyecto = request.IdProyecto,
                    IdLineaCMI = request.IdLineaCMI,
                    Tipo = request.Tipo,
                    FechaInicioProyecto = request.FechaInicioProyecto
                };

                if (oEsNuevo)
                {
                    oPrePeticionCabecera.IdEstado = Generales.Estados.Activo;
                    oPrePeticionCabecera.IdUsuarioCreacion = request.IdUsuario;
                    oPrePeticionCabecera.FechaCreacion = DateTime.Now;

                    iPrePeticionCabeceraDal.Add(oPrePeticionCabecera);
                }
                else
                {
                    oPrePeticionCabecera.FechaEdicion = DateTime.Now;
                    oPrePeticionCabecera.IdUsuarioEdicion = request.IdUsuario;

                    iPrePeticionCabeceraDal.ActualizarPorCampos(oPrePeticionCabecera,
                                                            r => r.IdCliente,
                                                            r => r.Tipo,
                                                            r => r.FechaEdicion,
                                                            r => r.IdUsuarioEdicion,
                                                            r => r.FechaInicioProyecto
                                                        );
                }

                iPrePeticionCabeceraDal.UnitOfWork.Commit();

                oProcesoResponse.Id = oPrePeticionCabecera.Id;
                oProcesoResponse.TipoRespuesta = Proceso.Valido;
                oProcesoResponse.Mensaje = oEsNuevo ? MensajesGeneralCompra.RegistrarDetalleClienteProveedor : MensajesGeneralCompra.ActualizarDetalleClienteProveedor;
            }
            catch (Exception ex)
            {
                oProcesoResponse.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioCompra.ErrorDetalleClienteProveedor, null, Generales.Sistemas.Compra);
            }

            return oProcesoResponse;
        }

    }
}
