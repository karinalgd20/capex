﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class PeticionCompraBl : IPeticionCompraBl
    {
        readonly IPeticionCompraDal iPeticionCompraDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public PeticionCompraBl(IPeticionCompraDal IPeticionCompraDal)
        {
            iPeticionCompraDal = IPeticionCompraDal;
        }

        public PeticionCompraPaginadoDtoResponse ListarPeticionCompraPaginado(PeticionCompraDtoRequest request)
        {
            var peticionCompraPaginadoDtoResponse = new PeticionCompraPaginadoDtoResponse();

            peticionCompraPaginadoDtoResponse.ListaPeticionDtoResponse= iPeticionCompraDal.ListarPeticionCompraCabecera(request);

            if (peticionCompraPaginadoDtoResponse.ListaPeticionDtoResponse.Count > Numeric.Cero)
            {
                peticionCompraPaginadoDtoResponse.TotalItemCount = peticionCompraPaginadoDtoResponse.ListaPeticionDtoResponse[0].TotalRegistros;
            }

            return peticionCompraPaginadoDtoResponse;
        }

        public ProcesoResponse ActualizarPeticionCompra(PeticionCompraDtoRequest request)
        {
            try
            {
                var peticionCompraEL = new PeticionCompras()
                {
                    Descripcion = request.Descripcion,
                    AsociadoAProyecto = request.AsociadoAProyecto,
                    IdTipoCompra = request.IdTipoCompra,
                    IdTipoCosto = request.IdTipoCosto,
                    IdLineaNegocio = request.IdLineaNegocio,
                    IdComprador = request.IdComprador,
                    AdjuntaCotizacion = request.AdjuntaCotizacion,
                    MontoCotizacion = request.MontoCotizacion,
                    SubGrupoCompras = request.SubGrupoCompras,
                    IdAreaSolicitante = request.IdAreaSolicitante,
                    IdCentroCosto = request.IdCentroCosto,
                    IdResponsablePostventa = request.IdResponsablePostventa,
                    ElementoPEP = request.ElementoPEP,
                    Grafo = request.Grafo,
                    Cuenta = request.Cuenta,
                    DescripcionCuenta = request.DescripcionCuenta,
                    AreaFuncional = request.AreaFuncional,
                    PeticionPenalidad = request.PeticionPenalidad,
                    ArrendamientoRenting = request.ArrendamientoRenting,
                    FechaEdicion = request.FechaEdicion,
                    IdUsuarioEdicion = (int)request.IdUsuarioEdicion,
                    IdContratoMarco = request.IdContratoMarco,
                    IdPrePeticionDetalle = request.IdPrePeticionDetalle,

                    IdPeticionCompra = request.IdPeticionCompra
                };

                iPeticionCompraDal.ActualizarPorCampos(peticionCompraEL,
                                       x => x.DescripcionCuenta,
                                       x => x.AsociadoAProyecto,
                                       x => x.IdTipoCompra,
                                       x => x.IdTipoCosto,
                                       x => x.IdLineaNegocio,
                                       x => x.IdComprador,
                                       x => x.AdjuntaCotizacion,
                                       x => x.MontoCotizacion,
                                       x => x.SubGrupoCompras,
                                       x => x.IdAreaSolicitante,
                                       x => x.IdCentroCosto,
                                       x => x.IdResponsablePostventa,
                                       x => x.ElementoPEP,
                                       x => x.Grafo,
                                       x => x.Cuenta,
                                       x => x.DescripcionCuenta,
                                       x => x.AreaFuncional,
                                       x => x.PeticionPenalidad,
                                       x => x.ArrendamientoRenting,
                                       x => x.FechaEdicion,
                                       x => x.IdUsuarioEdicion,
                                       x => x.IdContratoMarco,
                                       x => x.IdPrePeticionDetalle
                                        );
                iPeticionCompraDal.UnitOfWork.Commit();

                respuesta.Id = peticionCompraEL.IdPeticionCompra;
                respuesta.Mensaje = MensajesGeneralCompra.ActualizarPeticionCompra;
                respuesta.TipoRespuesta = Proceso.Valido;
            }
            catch (Exception ex)
            {
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.Compra);
            }
            return respuesta;
        }

        public PeticionCompraDtoResponse ObtenerPeticionCompraPorId(PeticionCompraDtoRequest request)
        {
            return iPeticionCompraDal.ObtenerPeticionCompraPorID(request);
        }

        public ProcesoResponse RegistrarPeticionCompra(PeticionCompraDtoRequest request)
        {
            return iPeticionCompraDal.RegistrarPeticionCompra(request);
        }

        public ProcesoResponse ActualizarPeticionCompraDetalleCompra(PeticionCompraDtoRequest request)
        {
            try
            {
                var peticionCompraEL = new PeticionCompras()
                {
                    IdTipoMoneda = request.IdTipoMoneda,
                    SustentoCualitativo = request.SustentoCualitativo,
                    DetalleSolpe = request.DetalleSolpe,
                    CostoTotal = request.CostoTotal,
                    FechaEdicion = request.FechaEdicion,
                    IdUsuarioEdicion = (int)request.IdUsuarioEdicion,
                    
                    IdPeticionCompra = request.IdPeticionCompra
                };

                iPeticionCompraDal.ActualizarPorCampos(peticionCompraEL,
                                       x => x.IdTipoMoneda,
                                       x => x.SustentoCualitativo,
                                       x => x.DetalleSolpe,
                                       x => x.CostoTotal,
                                       x => x.FechaEdicion,
                                       x => x.IdUsuarioEdicion);
                iPeticionCompraDal.UnitOfWork.Commit();

                respuesta.Id = peticionCompraEL.IdPeticionCompra;
                respuesta.Mensaje = MensajesGeneralCompra.ActualizarPeticionCompra;
                respuesta.TipoRespuesta = Proceso.Valido;
            }
            catch (Exception ex)
            {
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.Compra);
            }
            return respuesta;
        }

        public List<AprobacionDtoResponse> ListaAprobaciones(PeticionCompraDtoRequest request)
        {
            return iPeticionCompraDal.ListaAprobaciones(request);
        }
    }
}
