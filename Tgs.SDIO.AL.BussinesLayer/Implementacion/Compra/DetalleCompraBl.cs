﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class DetalleCompraBl : IDetalleCompraBl
    {
        readonly IDetalleCompraDal iDetalleCompraDal;
        readonly IPeticionCompraBl iPeticionCompraBl;

        ProcesoResponse respuesta = new ProcesoResponse();

        public DetalleCompraBl(IDetalleCompraDal IDetalleCompraDal,
                                IPeticionCompraBl IPeticionCompraBl)
        {
            iDetalleCompraDal = IDetalleCompraDal;
            iPeticionCompraBl = IPeticionCompraBl;
        }

        public List<DetalleCompraDtoResponse> ListarDetalleCompra(DetalleCompraDtoRequest request)
        {
            List<DetalleCompraDtoResponse> lista = iDetalleCompraDal.ListarDetalleCompra(request);

            var oPeticionCompraReq = new PeticionCompraDtoRequest() { IdPeticionCompra = request.IdPeticionCompra };
            var oPeticionCompraRes = iPeticionCompraBl.ObtenerPeticionCompraPorId(oPeticionCompraReq);

            foreach (var item in lista)
            {
                item.IdTipoMoneda = oPeticionCompraRes.IdTipoMoneda;
                item.DetalleSolpe = oPeticionCompraRes.DetalleSolpe;
                item.SustentoCualitativo = oPeticionCompraRes.SustentoCualitativo;
                item.TotalCosto = oPeticionCompraRes.CostoTotal;
            }

            return lista;
        }

        public DetalleCompraDtoResponse ObtenerDetalleCompraPorId(DetalleCompraDtoRequest request)
        {
            return iDetalleCompraDal.ObtenerDetalleCompraPorId(request);
        }

        public ProcesoResponse RegistrarDetalleCompra(DetalleCompraDtoRequest request)
        {
            return iDetalleCompraDal.RegistrarDetalleCompra(request);
        }

        public bool EliminarDetalleCompra(DetalleCompraDtoRequest request)
        {
            DetalleCompra objDetalleCompra = iDetalleCompraDal
                            .GetFilteredAsNoTracking(dp => dp.IdDetalleCompra == request.IdDetalleCompra)
                            .FirstOrDefault();

            iDetalleCompraDal.Remove(objDetalleCompra);
            iDetalleCompraDal.UnitOfWork.Commit();

            return true;
        }
    }
}
