﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Compra;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Compra;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class AnexoBl : IAnexoBl
    {
        readonly IAnexoDal iAnexoDal;
        readonly IAnexoArchivoBl iAnexoArchivoBl;
        ProcesoResponse respuesta = new ProcesoResponse();
        public AnexoBl(IAnexoDal IAnexoDal, IAnexoArchivoBl IAnexoArchivoBl)
        {
            iAnexoDal = IAnexoDal;
            iAnexoArchivoBl = IAnexoArchivoBl;
        }

        //public GestionDtoResponse ObtenerGestion(GestionDtoRequest request)
        //{
        //    GestionDtoResponse oGestion = iGestionDal.GetFiltered(g => g.IdPeticionCompra == request.IdPeticionCompra).Select(g => new GestionDtoResponse
        //    {
        //        IdGestionPeticionCompra = g.IdGestionPeticionCompra,
        //        IdPeticionCompra = g.IdPeticionCompra,
        //        Cesta = g.Cesta,
        //        CestaLiberada = g.CestaLiberada,
        //        EnvioPedido = g.EnvioPedido,
        //        FechaActaRecibida = g.FechaActaRecibida,
        //        FechaAtencionGestor = g.FechaAtencionGestor,
        //        FechaCesta = g.FechaCesta,
        //        FechaConfirmacion = g.FechaConfirmacion,
        //        FechaPedido = g.FechaPedido,
        //        MontoPedido = g.MontoPedido,
        //        NumeroPedido = g.NumeroPedido,
        //        PosicionCesta = g.PosicionCesta,
        //        ContratoMarco = g.ContratoMarco,
        //        PosicionPedido = g.PosicionPedido,
        //        SaldoPedido = g.SaldoPedido
        //    }).FirstOrDefault();

        //    if (oGestion == null) oGestion = new GestionDtoResponse();

        //    return oGestion;
        //}

        public ProcesoResponse RegistrarAnexo(AnexoDtoRequest request)
        {
            ProcesoResponse oProcesoResponse = new ProcesoResponse();
            try
            {
                if (request.IdTipoDocumento == TipoDocumento.ActaAceptacion)
                {
                    EliminarActaExistente(request);
                }

                AnexosPeticionCompras oAnexo = new AnexosPeticionCompras
                {
                    IdPeticionCompra = request.IdPeticionCompra,
                    IdTipoDocumento = request.IdTipoDocumento,
                    NombreArchivo = request.NombreArchivo
                };

                oAnexo.IdEstado = Generales.Estados.Activo;
                oAnexo.IdUsuarioCreacion = request.IdUsuario;
                oAnexo.FechaCreacion = DateTime.Now;

                iAnexoDal.Add(oAnexo);
                
                iAnexoDal.UnitOfWork.Commit();

                AnexoArchivoDtoRequest oAnexoArchivoDtoRequest = new AnexoArchivoDtoRequest
                {
                    ArchivoAdjunto = request.ArchivoAdjunto,
                    IdAnexoPeticionCompras = oAnexo.IdAnexoPeticionCompra,
                    IdUsuario = request.IdUsuario
            };

                request.IdAnexoPeticionCompra = oAnexo.IdAnexoPeticionCompra;
                iAnexoArchivoBl.RegistrarAnexoArchivo(oAnexoArchivoDtoRequest);

                oProcesoResponse.Id = oAnexo.IdAnexoPeticionCompra;
            }
            catch (Exception ex)
            {
                oProcesoResponse.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioCompra.ErrorAnexo, null, Generales.Sistemas.Compra);
            }

            return oProcesoResponse;
        }

        public List<AnexoDtoResponse> ListarAnexos(AnexoDtoRequest request) {
            return iAnexoDal.ListarAnexos(request);
        }

        public bool EliminarAnexo(AnexoDtoRequest request)
        {
            AnexosPeticionCompras objAnexo = iAnexoDal.GetFilteredAsNoTracking(dp => dp.IdAnexoPeticionCompra == request.IdAnexoPeticionCompra).FirstOrDefault();
            iAnexoDal.Remove(objAnexo);
            iAnexoDal.UnitOfWork.Commit();

            return true;
        }

        private void EliminarActaExistente(AnexoDtoRequest request)
        {
            AnexosPeticionCompras objAnexo = iAnexoDal.GetFilteredAsNoTracking(dp => dp.IdPeticionCompra == request.IdPeticionCompra
                                               && dp.IdTipoDocumento == TipoDocumento.ActaAceptacion
                                            ).FirstOrDefault();

            if (objAnexo != null)
            {
                var anexoAA = new AnexoDtoRequest() { IdAnexoPeticionCompra = objAnexo.IdAnexoPeticionCompra };

                EliminarAnexo(anexoAA);
            }
        }
    }
}
