﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Compra;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class PrePeticionDCMDetalleBl : IPrePeticionDCMDetalleBl
    {
        readonly IPrePeticionDCMDetalleDal iPrePeticionDCMDetalleDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public PrePeticionDCMDetalleBl(IPrePeticionDCMDetalleDal IPrePeticionDCMDetalleDal)
        {
            iPrePeticionDCMDetalleDal = IPrePeticionDCMDetalleDal;
        }

        public PrePeticionDCMDetalleDtoResponse ObtenerDetalleDCM(PrePeticionDetalleDCMDetalleDtoRequest request)
        {
            return iPrePeticionDCMDetalleDal.ObtenerDetalleDCM(request);
        }

        public ProcesoResponse RegistrarPrePeticionDCMDetalle(PrePeticionDetalleDCMDetalleDtoRequest request)
        {
            ProcesoResponse oProcesoResponse = new ProcesoResponse();
            try
            {
                bool oExisteRegistro = iPrePeticionDCMDetalleDal.GetFiltered(g => g.Id == request.Id).Any();
                bool oEsNuevo = (request.Id == 0);

                PrePeticionDCMDetalle oPrePeticionDCMDetalle = new PrePeticionDCMDetalle
                {
                    Id = request.Id,
                    IdCabecera = request.IdCabecera,
                    TipoCosto = request.TipoCosto,
                    ProductManager = request.ProductManager,
                    Descripcion = request.Descripcion,
                    IdProveedor = request.IdProveedor,
                    IdContratoMarco = request.IdContratoMarco,
                    AutorizadoPor = request.AutorizadoPor,
                    TipoEntrega = request.TipoEntrega,
                    PlazoEntrega = request.PlazoEntrega,
                    PQAdjudicado = request.PQAdjudicado,
                    CotizacionAdjunta = request.CotizacionAdjunta,
                    FechaEntregaOC = request.FechaEntregaOC,
                    Observaciones = request.Observaciones,
                    Direccion = request.Direccion,
                    PosicionOC = request.PosicionOC,
                    MonedaCompra = request.MonedaCompra,
                    Monto = request.Monto,
                    IdLineaProducto = request.IdLineaProducto
                };

                if (oEsNuevo)
                {
                    oPrePeticionDCMDetalle.IdEstado = Generales.Estados.Activo;
                    oPrePeticionDCMDetalle.IdUsuarioCreacion = request.IdUsuario;
                    oPrePeticionDCMDetalle.FechaCreacion = DateTime.Now;

                    iPrePeticionDCMDetalleDal.Add(oPrePeticionDCMDetalle);
                }
                else
                {
                    oPrePeticionDCMDetalle.FechaEdicion = DateTime.Now;
                    oPrePeticionDCMDetalle.IdUsuarioEdicion = request.IdUsuario;

                    iPrePeticionDCMDetalleDal.ActualizarPorCampos(oPrePeticionDCMDetalle,
                                                            r => r.TipoCosto,
                                                            r => r.ProductManager,
                                                            r => r.Descripcion,
                                                            r => r.IdProveedor,
                                                            r => r.IdContratoMarco,
                                                            r => r.AutorizadoPor,
                                                            r => r.TipoEntrega,
                                                            r => r.PlazoEntrega,
                                                            r => r.PQAdjudicado,
                                                            r => r.CotizacionAdjunta,
                                                            r => r.FechaEntregaOC,
                                                            r => r.Observaciones,
                                                            r => r.Direccion,
                                                            r => r.PosicionOC,
                                                            r => r.MonedaCompra,
                                                            r => r.PosicionOC,
                                                            r => r.Monto,
                                                            r => r.IdUsuarioEdicion
                                                        );
                }

                iPrePeticionDCMDetalleDal.UnitOfWork.Commit();

                oProcesoResponse.Id = oPrePeticionDCMDetalle.Id;
                oProcesoResponse.TipoRespuesta = Proceso.Valido;
                oProcesoResponse.Mensaje = oEsNuevo ? MensajesGeneralCompra.RegistrarDetalleClienteProveedor : MensajesGeneralCompra.ActualizarDetalleClienteProveedor;
            }
            catch (Exception ex)
            {
                oProcesoResponse.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioCompra.ErrorDetalleClienteProveedor, null, Generales.Sistemas.Compra);
            }

            return oProcesoResponse;
        }

        public List<PrePeticionDCMDetalleDtoResponse> ListarPrePeticionDCM(PrePeticionDetalleDCMDetalleDtoRequest request) {
            return iPrePeticionDCMDetalleDal.ListarPrePeticionDCM(request);
        }
    }
}
