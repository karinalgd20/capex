﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class CuentaBl : ICuentaBl
    {
        readonly ICuentaDal iCuentaDal;

        public CuentaBl(ICuentaDal ICuentaDal)
        {
            iCuentaDal = ICuentaDal;
        }

        public List<CuentaDtoResponse> ListarCuentas(CuentaDtoRequest request)
        {
            return iCuentaDal.ListarCuentas(request);
        }

        public CuentaPaginadoDtoResponse ListarCuentaPaginado(CuentaDtoRequest request)
        {
            var cuentaPaginadoDtoResponse = new CuentaPaginadoDtoResponse();

            cuentaPaginadoDtoResponse.ListaCuentaDtoResponse = iCuentaDal.ListarCuentas(request);

            if (cuentaPaginadoDtoResponse.ListaCuentaDtoResponse.Count > Numeric.Cero)
            {
                cuentaPaginadoDtoResponse.TotalItemCount = cuentaPaginadoDtoResponse.ListaCuentaDtoResponse[0].TotalRow;
            }

            return cuentaPaginadoDtoResponse;
        }

    }
}
