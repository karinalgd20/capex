﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Compra;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class DetalleClienteProveedorBl : IDetalleClienteProveedorBl
    {
        readonly IDetalleClienteProveedorDal iDetalleClienteProveedorDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public DetalleClienteProveedorBl(IDetalleClienteProveedorDal IDetalleClienteProveedorDal)
        {
            iDetalleClienteProveedorDal = IDetalleClienteProveedorDal;
        }

        public DetalleClienteProveedorDtoResponse ObtenerDetalleClienteProveedor(DetalleClienteProveedorDtoRequest request)
        {
            return iDetalleClienteProveedorDal.ObtenerDetalleClienteProveedor(request);
        }

        public ProcesoResponse RegistrarDetalleClienteProveedor(DetalleClienteProveedorDtoRequest request)
        {
            ProcesoResponse oProcesoResponse = new ProcesoResponse();
            try
            {
                bool oExisteRegistro = iDetalleClienteProveedorDal.GetFiltered(cp => cp.IdDetalleClientePro == request.IdDetalleClientePro).Any();
                bool oEsNuevo = (request.IdDetalleClientePro == 0);

                DetalleClienteProveedor oDetalleClienteProveedor = new DetalleClienteProveedor
                {
                    IdDetalleClientePro = request.IdDetalleClientePro,
                    IdPeticionCompra = request.IdPeticionCompra,
                    IdCliente = request.IdCliente,
                    IdProveedor = request.IdProveedor,
                    CodigoSrm = request.CodigoSrm,
                    CodigoSap = request.CodigoSap,
                    EmailContacto = request.EmailContacto,
                    NombreContacto = request.NombreContacto,
                    TelefonoContacto = request.TelefonoContacto
                };

                if (oEsNuevo)
                {
                    oDetalleClienteProveedor.IdUsuarioCreacion = request.IdUsuario;
                    oDetalleClienteProveedor.FechaCreacion = DateTime.Now;

                    iDetalleClienteProveedorDal.Add(oDetalleClienteProveedor);
                }
                else
                {
                    oDetalleClienteProveedor.FechaEdicion = DateTime.Now;
                    oDetalleClienteProveedor.IdUsuarioEdicion = request.IdUsuario;

                    iDetalleClienteProveedorDal.ActualizarPorCampos(oDetalleClienteProveedor,
                                                                       x => x.IdDetalleClientePro,
                                                                       x => x.IdCliente,
                                                                       x => x.IdProveedor,
                                                                       x => x.CodigoSrm,
                                                                       x => x.CodigoSap,
                                                                       x => x.EmailContacto,
                                                                       x => x.NombreContacto,
                                                                       x => x.TelefonoContacto,
                                                                       x => x.FechaEdicion,
                                                                       x => x.IdUsuarioEdicion
                                                                    );
                }

                iDetalleClienteProveedorDal.UnitOfWork.Commit();

                oProcesoResponse.Id = oDetalleClienteProveedor.IdDetalleClientePro;
                oProcesoResponse.TipoRespuesta = Proceso.Valido;
                oProcesoResponse.Mensaje = oEsNuevo ? MensajesGeneralCompra.RegistrarDetalleClienteProveedor : MensajesGeneralCompra.ActualizarDetalleClienteProveedor;
            }
            catch (Exception ex)
            {
                oProcesoResponse.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioCompra.ErrorDetalleClienteProveedor, null, Generales.Sistemas.Compra);
            }

            return oProcesoResponse;
        }
    }
}
