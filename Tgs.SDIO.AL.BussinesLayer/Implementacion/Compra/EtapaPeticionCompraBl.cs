﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Compra;
using Tgs.SDIO.AL.DataAccess.Interfaces.Compra;
using Tgs.SDIO.DataContracts.Dto.Request.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Compra;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Compra;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Compra;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Compra
{
    public class EtapaPeticionCompraBl : IEtapaPeticionCompraBl
    {
        readonly IEtapaPeticionCompraDal iEtapaPeticionCompraDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public EtapaPeticionCompraBl(IEtapaPeticionCompraDal IEtapaPeticionCompraDal)
        {
            iEtapaPeticionCompraDal = IEtapaPeticionCompraDal;
        }

        public ProcesoResponse RegistrarEtapaPeticionCompra(EtapaPeticionCompraDtoRequest request) {
            return iEtapaPeticionCompraDal.RegistrarEtapaPeticionCompra(request);
        }

        public EtapaPeticionCompraDtoResponse ObtenerEtapaPeticionCompraActual(EtapaPeticionCompraDtoRequest request) {
            return iEtapaPeticionCompraDal.ObtenerEtapaPeticionCompraActual(request);
        }

        public EtapaPeticionCompraPaginadoDtoResponse ListarEtapaPeticionCompraPaginado(EtapaPeticionCompraDtoRequest request){
            var etapaPaginadoDtoResponse = new EtapaPeticionCompraPaginadoDtoResponse();

            etapaPaginadoDtoResponse.ListaEtapaDtoResponse = iEtapaPeticionCompraDal.ListarEtapaPeticionCompra(request);

            if (etapaPaginadoDtoResponse.ListaEtapaDtoResponse.Count > Numeric.Cero)
            {
                etapaPaginadoDtoResponse.TotalItemCount = etapaPaginadoDtoResponse.ListaEtapaDtoResponse[0].TotalRow;
            }

            return etapaPaginadoDtoResponse;
        }

    }
}
