﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Funciones;
using Tgs.SDIO.Util.Mensajes.CartaFianza;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.CartaFianza
{
    public class CartaFianzaDetalleRenovacionBl : ICartaFianzaDetalleRenovacionBl 
    {
        readonly ICartaFianzaDetalleRenovacionDal iCartaFianzaDetalleRenovacionDal;
        readonly ICartaFianzaDal iCartaFianzaDal;

        ProcesoResponse respuesta = new ProcesoResponse();

        public CartaFianzaDetalleRenovacionBl(ICartaFianzaDetalleRenovacionDal vICartaFianzaDetalleRenovacionDal
            , ICartaFianzaDal viCartaFianzaDal)
        {
            this.iCartaFianzaDetalleRenovacionDal = vICartaFianzaDetalleRenovacionDal;
            this.iCartaFianzaDal = viCartaFianzaDal;
        }
        public ProcesoResponse ActualizaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest cartaFianzaDetalleRenovacionActualiza)
        {
            try
            {
                var objCartaFianzaRenovacion = new CartaFianzaDetalleRenovacion()
                {
                    IdCartaFianzaDetalleRenovacion = cartaFianzaDetalleRenovacionActualiza.IdCartaFianzaDetalleRenovacion,
                    IdRenovarTm = cartaFianzaDetalleRenovacionActualiza.IdRenovarTm,
                    //FechaVencimientoRenovacion = cartaFianzaDetalleRenovacionActualiza.FechaVencimientoRenovacion,
                    //PeriodoMesRenovacion = cartaFianzaDetalleRenovacionActualiza.PeriodoMesRenovacion,
                    SustentoRenovacion = cartaFianzaDetalleRenovacionActualiza.SustentoRenovacion,
                    //Observacion = cartaFianzaDetalleRenovacionActualiza.Observacion,
                    IdUsuarioEdicion = cartaFianzaDetalleRenovacionActualiza.IdUsuarioEdicion,
                    FechaEdicion = cartaFianzaDetalleRenovacionActualiza.FechaEdicion,
                    CartaFianzaReemplazo = cartaFianzaDetalleRenovacionActualiza.CartaFianzaReemplazo,
                    FechaSolitudTesoria = cartaFianzaDetalleRenovacionActualiza.FechaSolitudTesoria,

                    NumeroGarantiaReemplazo = cartaFianzaDetalleRenovacionActualiza.NumeroGarantiaReemplazo,
                    IdTipoBancoReemplazo = cartaFianzaDetalleRenovacionActualiza.IdTipoBancoReemplazo,
                    //ValidacionContratoGcSm = cartaFianzaDetalleRenovacionActualiza.ValidacionContratoGcSm,
                    //FechaRespuestaFinalGcSm = cartaFianzaDetalleRenovacionActualiza.FechaRespuestaFinalGcSm,
                    FechaEntregaRenovacion = cartaFianzaDetalleRenovacionActualiza.FechaEntregaRenovacion,
                    FechaRecepcionCartaFianzaRenovacion = cartaFianzaDetalleRenovacionActualiza.FechaRecepcionCartaFianzaRenovacion,
                    UsuarioEdicion = cartaFianzaDetalleRenovacionActualiza.UsuarioEdicion,
                    FechaEmisionBancoRnv = cartaFianzaDetalleRenovacionActualiza.FechaEmisionBancoRnv,
                    FechaVencimientoBancoRnv = cartaFianzaDetalleRenovacionActualiza.FechaVencimientoBancoRnv,
                    FechaVigenciaBancoRnv  = cartaFianzaDetalleRenovacionActualiza.FechaVigenciaBancoRnv
                };

                iCartaFianzaDetalleRenovacionDal.ActualizarPorCampos(objCartaFianzaRenovacion,
                                                                    x => x.IdRenovarTm,
                                                                    // x => x.FechaVencimientoRenovacion,
                                                                    //x => x.PeriodoMesRenovacion,
                                                                    x => x.SustentoRenovacion,
                                                                    // x => x.Observacion,
                                                                    x => x.IdUsuarioEdicion,
                                                                    x => x.FechaEdicion,
                                                                    x => x.CartaFianzaReemplazo,
                                                                    x => x.FechaSolitudTesoria,
                                                                    x => x.NumeroGarantiaReemplazo,
                                                                    x => x.IdTipoBancoReemplazo,
                                                                   // x => x.ValidacionContratoGcSm,
                                                                    //x => x.FechaRespuestaFinalGcSm,
                                                                    x => x.FechaEntregaRenovacion,
                                                                    x => x.FechaRecepcionCartaFianzaRenovacion,
                                                                    x => x.UsuarioEdicion,
                                                                    x => x.FechaEmisionBancoRnv,
                                                                    x => x.FechaVencimientoBancoRnv,
                                                                    x => x.FechaVigenciaBancoRnv

                                                                    );

                iCartaFianzaDetalleRenovacionDal.UnitOfWork.Commit();

                respuesta.Id = objCartaFianzaRenovacion.IdCartaFianzaDetalleRenovacion;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianzaRenovacion;

            }
            catch (Exception ex)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                respuesta.Mensaje = ex.Message;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.CartaFianza);
            }

            return respuesta;
        }
        public CartaFianzaDetalleRenovacionResponsePaginado ListaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest cartaFianzaDetalleRenovacionLista)
        {
            return iCartaFianzaDetalleRenovacionDal.ListaCartaFianzaDetalleRenovacion(cartaFianzaDetalleRenovacionLista);
        }
        public MailResponse ObtenerDatosCorreoRenovacion(MailRequest mailRequest)
        {
            return iCartaFianzaDetalleRenovacionDal.ObtenerDatosCorreoRenovacion(mailRequest);
        }
        public ProcesoResponse EnviarCorreoCartaFianza(MailRequest mailRequest)
        {
            try
            {
                DatosCorreo datosCorreo = new DatosCorreo()
                {
                    CorreoEnvia = mailRequest.From,
                    Destinatarios = mailRequest.To,
                    Copia = mailRequest.Copy,
                    Asunto = mailRequest.Subject,
                    Cuerpo = mailRequest.Body,
                    Adjuntos = mailRequest.Attachment
                };
                respuesta.Mensaje = Funciones.EnviarCorreo(datosCorreo);
                respuesta.TipoRespuesta = (respuesta.Mensaje == ErrorGeneralComun.Ok) ? Proceso.Valido : Proceso.Invalido;
                if (respuesta.Mensaje == ErrorGeneralComun.Ok) { InsertaAccionesEnvioCorreo(mailRequest); }
                //InsertaAccionesEnvioCorreo(mailRequest);

            }
            catch (Exception ex)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                respuesta.Mensaje = ex.Message;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.CartaFianza);
            }

            return respuesta;
        }
        public ProcesoResponse InsertaAccionesEnvioCorreo(MailRequest mailRequest)
        {
            CartaFianzaDetalleRenovacionRequest CartaFianzaDetalleAccionRequestInserta =new CartaFianzaDetalleRenovacionRequest() ;
            CartaFianzaDetalleAccionRequestInserta.IdCartaFianza = mailRequest.IdCartaFianza;
            CartaFianzaDetalleAccionRequestInserta.IdColaboradorACargo = mailRequest.IdUsuarioCreacion;
            CartaFianzaDetalleAccionRequestInserta.IdUsuarioCreacion = mailRequest.IdUsuarioCreacion;
            CartaFianzaDetalleAccionRequestInserta.UsuarioEdicion = mailRequest.UsuarioEdicion;

            CartaFianzaDetalleAccionRequestInserta.IdTipoAccionTm = mailRequest.IdTipoAccionTm;
            CartaFianzaDetalleAccionRequestInserta.IdEstadoCartaFianzaTm = mailRequest.IdEstadoCartaFianzaTm;
            CartaFianzaDetalleAccionRequestInserta.IdRenovarTm = mailRequest.IdRenovarTm;
            CartaFianzaDetalleAccionRequestInserta.IdTipoSubEstadoCartaFianzaTm = mailRequest.IdTipoSubEstadoCartaFianzaTm;


            return iCartaFianzaDal.InsertaAccionMailCartaFianza(CartaFianzaDetalleAccionRequestInserta);
        }

    }
}
