﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.CartaFianza
{
    public class CartaFianzaColaboradorDetalleBl : ICartaFianzaColaboradorDetalleBl
    {
        readonly ICartaFianzaColaboradorDetalleDal iCartaFianzaColaboradorDetalleDal;

        public CartaFianzaColaboradorDetalleBl(ICartaFianzaColaboradorDetalleDal vICartaFianzaColaboradorDetalleDal)
        {
            this.iCartaFianzaColaboradorDetalleDal = vICartaFianzaColaboradorDetalleDal;
        }

        public List<CartaFianzaColaboradorDetalleResponse> ListarColaborador(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            return iCartaFianzaColaboradorDetalleDal.ListarColaborador(cartaFianzaColaboradorDetalleRequest);
        }

        public ProcesoResponse AsignarSupervisorAGerenteCuenta(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            return iCartaFianzaColaboradorDetalleDal.AsignarSupervisorAGerenteCuenta(cartaFianzaColaboradorDetalleRequest);
        }

        public ProcesoResponse AsignarGerenteCuentaACartaFianza(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            return iCartaFianzaColaboradorDetalleDal.AsignarGerenteCuentaACartaFianza(cartaFianzaColaboradorDetalleRequest);
        }

        public ProcesoResponse ActualizarTesorera(CartaFianzaColaboradorDetalleRequest cartaFianzaColaboradorDetalleRequest)
        {
            return iCartaFianzaColaboradorDetalleDal.ActualizarTesorera(cartaFianzaColaboradorDetalleRequest);
        }
    }
}
