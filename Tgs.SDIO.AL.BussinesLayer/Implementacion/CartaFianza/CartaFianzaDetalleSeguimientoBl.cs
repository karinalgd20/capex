﻿using System;
using System.Collections.Generic;
using Tgs.SDIO.AL.BussinesLayer.Interfaces;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.Util.Mensajes.CartaFianza;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.CartaFianza
{
    public class CartaFianzaDetalleSeguimientoBl : ICartaFianzaDetalleSeguimientoBl
    {
        readonly ICartaFianzaDetalleSeguimientoDal iCartaFianzaDetalleSeguimientoDal;

        public CartaFianzaDetalleSeguimientoBl(ICartaFianzaDetalleSeguimientoDal vICartaFianzaDetalleSeguimientoDal)
        {
            this.iCartaFianzaDetalleSeguimientoDal = vICartaFianzaDetalleSeguimientoDal;
        }

        ProcesoResponse respuesta = new ProcesoResponse();

        public CartaFianzaDetalleSeguimientoResponsePaginado ListaCartaFianzaDetalleSeguimiento(CartaFianzaDetalleSeguimientoRequest cartaFianzaDetalleRecuperoLista)
        {
            return iCartaFianzaDetalleSeguimientoDal.ListaCartaFianzaDetalleSeguimiento(cartaFianzaDetalleRecuperoLista);
        }

        public ProcesoResponse InsertaCartaFianzaSeguimiento(CartaFianzaDetalleSeguimientoRequest cartaFianzaDetalleSeguimiento)
        {
            var objCartaFianzaSeguimiento = new CartaFianzaDetalleSeguimiento()
            {
                IdCartaFianza = cartaFianzaDetalleSeguimiento.IdCartaFianza,
                IdColaboradorACargo = cartaFianzaDetalleSeguimiento.IdColaboradorACargo,
                IdEstadoRecuperacionCartaFianzaTm = cartaFianzaDetalleSeguimiento.IdEstadoRecuperacionCartaFianzaTm,
                Comentario = cartaFianzaDetalleSeguimiento.Comentario,
                IdUsuarioCreacion = cartaFianzaDetalleSeguimiento.IdUsuarioCreacion,
                FechaCreacion = cartaFianzaDetalleSeguimiento.FechaCreacion,
                UltimoSeguimiento = true,
                Eliminado = false
            };

            iCartaFianzaDetalleSeguimientoDal.Add(objCartaFianzaSeguimiento);
            iCartaFianzaDetalleSeguimientoDal.UnitOfWork.Commit();

            respuesta.Id = objCartaFianzaSeguimiento.IdCartaFianzaDetalleSeguimiento.Value;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;

            return respuesta;
        }
    }
}
