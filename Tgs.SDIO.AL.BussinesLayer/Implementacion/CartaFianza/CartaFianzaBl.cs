﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Mensajes.CartaFianza;
using Tgs.SDIO.Util.Funciones;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.CartaFianza
{
    public class CartaFianzaBl : ICartaFianzaBl
    {
        readonly ICartaFianzaDal iCartaFianzaDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public CartaFianzaBl(ICartaFianzaDal vICartaFianzaDal)
        {
            iCartaFianzaDal = vICartaFianzaDal;
        }
        public ProcesoResponse ActualizaARenovacionCartaFianza(CartaFianzaDetalleRenovacionRequest CartaFianzaRequestActualiza)
        {
            return iCartaFianzaDal.ActualizaARenovacionCartaFianza(CartaFianzaRequestActualiza);
        }

        public ProcesoResponse ActualizaCartaFianza(CartaFianzaRequest CartaFianzaRequestActualiza)
        {
            var objCartaFianza = new CartaFianzaMaestro()
            {
                IdCartaFianza = CartaFianzaRequestActualiza.IdCartaFianza,
                IdCliente = CartaFianzaRequestActualiza.IdCliente,
                NumeroOportunidad = CartaFianzaRequestActualiza.NumeroOportunidad,
                IdTipoContratoTm = CartaFianzaRequestActualiza.IdTipoContratoTm,
                NumeroContrato = CartaFianzaRequestActualiza.NumeroContrato,
                IdProcesoTm = CartaFianzaRequestActualiza.IdProcesoTm,
                NumeroProceso = CartaFianzaRequestActualiza.NumeroProceso,

                Servicio = CartaFianzaRequestActualiza.Servicio,
                DescripcionServicioCartaFianza = CartaFianzaRequestActualiza.DescripcionServicioCartaFianza,

                FechaFirmaServicioContrato = CartaFianzaRequestActualiza.FechaFirmaServicioContrato,
                FechaFinServicioContrato = CartaFianzaRequestActualiza.FechaFinServicioContrato,

                IdEmpresaAdjudicadaTm = CartaFianzaRequestActualiza.IdEmpresaAdjudicadaTm,
                IdTipoGarantiaTm = CartaFianzaRequestActualiza.IdTipoGarantiaTm,
                NumeroGarantia = CartaFianzaRequestActualiza.NumeroGarantia,
                IdTipoMonedaTm = CartaFianzaRequestActualiza.IdTipoMonedaTm,
                ImporteCartaFianzaSoles = CartaFianzaRequestActualiza.ImporteCartaFianzaSoles,
                ImporteCartaFianzaDolares = CartaFianzaRequestActualiza.ImporteCartaFianzaDolares,
                IdBanco = CartaFianzaRequestActualiza.IdBanco,

                FechaEmisionBanco = CartaFianzaRequestActualiza.FechaEmisionBanco,
                FechaVencimientoBanco = CartaFianzaRequestActualiza.FechaVencimientoBanco,
                FechaVigenciaBanco = CartaFianzaRequestActualiza.FechaVigenciaBanco,

                IdTipoAccionTm = CartaFianzaRequestActualiza.IdTipoAccionTm,
                //IdEstadoVencimientoTm = CartaFianzaRequestActualiza.IdEstadoVencimientoTm,

                IdEstadoCartaFianzaTm = CartaFianzaRequestActualiza.IdEstadoCartaFianzaTm,
                IdTipoSubEstadoCartaFianzaTm = CartaFianzaRequestActualiza.IdTipoSubEstadoCartaFianzaTm,

                IdRenovarTm = CartaFianzaRequestActualiza.IdRenovarTm,
                //IdEstadoRecuperacionCartaFianzaTm = CartaFianzaRequestActualiza.IdEstadoRecuperacionCartaFianzaTm,
                ClienteEspecial = CartaFianzaRequestActualiza.ClienteEspecial,
                SeguimientoImportante = CartaFianzaRequestActualiza.SeguimientoImportante,
                Observacion = CartaFianzaRequestActualiza.Observacion,
                Incidencia = CartaFianzaRequestActualiza.Incidencia,
                IdEstado = CartaFianzaRequestActualiza.IdEstado,
                IdUsuarioEdicion = CartaFianzaRequestActualiza.IdUsuarioEdicion,
                FechaEdicion = DateTime.Now,

                FechaCorrecionTesoreria = CartaFianzaRequestActualiza.FechaCorrecionTesoreria,
                FechaRecepcionTesoreria = CartaFianzaRequestActualiza.FechaRecepcionTesoreria,
                MotivoCartaFianzaErrada = CartaFianzaRequestActualiza.MotivoCartaFianzaErrada,
                UsuarioEdicion = CartaFianzaRequestActualiza.UsuarioEdicion
            };


            iCartaFianzaDal.ActualizarPorCampos(objCartaFianza,
                                                //x=> x.IdCliente,
                                                x => x.NumeroOportunidad,
                                                x => x.IdTipoContratoTm,
                                                x => x.NumeroContrato,
                                                x => x.IdProcesoTm,
                                                x => x.NumeroProceso,
                                                x => x.Servicio,
                                                x => x.DescripcionServicioCartaFianza,

                                                x => x.FechaFirmaServicioContrato,
                                                x => x.FechaFinServicioContrato,

                                                x => x.IdEmpresaAdjudicadaTm,
                                                x => x.IdTipoAccionTm,
                                                x => x.IdEstadoCartaFianzaTm,


                                                x => x.IdTipoGarantiaTm,
                                                x => x.NumeroGarantia,
                                                x => x.IdTipoMonedaTm,
                                                x => x.ImporteCartaFianzaSoles,
                                                x => x.ImporteCartaFianzaDolares,
                                                x => x.IdBanco,

                                                x => x.FechaEmisionBanco,
                                                x => x.FechaVencimientoBanco,
                                                x => x.FechaVigenciaBanco,

                                                x => x.ClienteEspecial,
                                                x => x.SeguimientoImportante,
                                                x => x.Observacion,
                                                x => x.Incidencia,
                                                x => x.IdEstado,
                                                x => x.IdUsuarioEdicion,
                                                x => x.FechaEdicion,
                                                x => x.IdTipoSubEstadoCartaFianzaTm,

                                                x => x.FechaCorrecionTesoreria,
                                                x => x.FechaRecepcionTesoreria,
                                                x => x.MotivoCartaFianzaErrada,
                                                x => x.UsuarioEdicion
                                                );

            iCartaFianzaDal.UnitOfWork.Commit();

            respuesta.Id = objCartaFianza.IdCartaFianza;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;

            return respuesta;

        }

        public ProcesoResponse InsertaCartaFianza(CartaFianzaRequest CartaFianzaRequestInserta)
        {
            var objCartaFianza = new CartaFianzaMaestro()
            {
                // IdCartaFianza = CartaFianzaRequestInserta.IdCartaFianza,
                IdCliente = CartaFianzaRequestInserta.IdCliente,
                NumeroOportunidad = CartaFianzaRequestInserta.NumeroOportunidad,
                IdTipoContratoTm = CartaFianzaRequestInserta.IdTipoContratoTm,
                NumeroContrato = CartaFianzaRequestInserta.NumeroContrato,
                IdProcesoTm = CartaFianzaRequestInserta.IdProcesoTm,
                NumeroProceso = CartaFianzaRequestInserta.NumeroProceso,


                DescripcionServicioCartaFianza = CartaFianzaRequestInserta.DescripcionServicioCartaFianza,
                FechaFirmaServicioContrato = CartaFianzaRequestInserta.FechaFirmaServicioContrato,
                FechaFinServicioContrato = CartaFianzaRequestInserta.FechaFinServicioContrato,

                IdEmpresaAdjudicadaTm = CartaFianzaRequestInserta.IdEmpresaAdjudicadaTm,
                IdTipoGarantiaTm = CartaFianzaRequestInserta.IdTipoGarantiaTm,
                NumeroGarantia = CartaFianzaRequestInserta.NumeroGarantia,
                IdTipoMonedaTm = CartaFianzaRequestInserta.IdTipoMonedaTm,
                ImporteCartaFianzaSoles = CartaFianzaRequestInserta.ImporteCartaFianzaSoles,
                ImporteCartaFianzaDolares = CartaFianzaRequestInserta.ImporteCartaFianzaDolares,
                IdBanco = CartaFianzaRequestInserta.IdBanco,

                FechaEmisionBanco = CartaFianzaRequestInserta.FechaEmisionBanco,
                FechaVencimientoBanco = CartaFianzaRequestInserta.FechaVencimientoBanco,
                FechaVigenciaBanco = CartaFianzaRequestInserta.FechaVigenciaBanco,

                IdTipoAccionTm = CartaFianzaRequestInserta.IdTipoAccionTm,
                //IdEstadoVencimientoTm = CartaFianzaRequestInserta.IdEstadoVencimientoTm,
                IdEstadoCartaFianzaTm = CartaFianzaRequestInserta.IdEstadoCartaFianzaTm,
                IdRenovarTm = CartaFianzaRequestInserta.IdRenovarTm,
                IdTipoSubEstadoCartaFianzaTm = CartaFianzaRequestInserta.IdTipoSubEstadoCartaFianzaTm,


                //IdEstadoRecuperacionCartaFianzaTm = CartaFianzaRequestInserta.IdEstadoRecuperacionCartaFianzaTm,
                ClienteEspecial = CartaFianzaRequestInserta.ClienteEspecial,
                SeguimientoImportante = CartaFianzaRequestInserta.SeguimientoImportante,
                Observacion = CartaFianzaRequestInserta.Observacion,
                Incidencia = CartaFianzaRequestInserta.Incidencia,
                IdEstado = CartaFianzaRequestInserta.IdEstado,
                IdUsuarioCreacion = CartaFianzaRequestInserta.IdUsuarioCreacion,
                FechaCreacion = DateTime.Now,
                UsuarioEdicion = CartaFianzaRequestInserta.UsuarioEdicion,

                Servicio = CartaFianzaRequestInserta.Servicio,

            };
            iCartaFianzaDal.Add(objCartaFianza);
            iCartaFianzaDal.UnitOfWork.Commit();

            respuesta.Id = objCartaFianza.IdCartaFianza;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;

            return respuesta;
        }

        public ProcesoResponse ActualizarCartaFianzaRequerida(CartaFianzaRequest CartaFianzaRequestActualiza)
        {
            var objCartaFianza = new CartaFianzaMaestro()
            {
                IdCartaFianza = CartaFianzaRequestActualiza.IdCartaFianza,
                IdCliente = CartaFianzaRequestActualiza.IdCliente,

                IdTipoAccionTm = CartaFianzaRequestActualiza.IdTipoAccionTm,
                IdEstadoCartaFianzaTm = CartaFianzaRequestActualiza.IdEstadoCartaFianzaTm,
                IdRenovarTm = CartaFianzaRequestActualiza.IdRenovarTm,
                IdTipoRequeridaTm = CartaFianzaRequestActualiza.IdTipoRequeridaTm,

                FechaRenovacionEjecucion = CartaFianzaRequestActualiza.FechaRenovacionEjecucion,
                FechaDesestimarRequerimiento = CartaFianzaRequestActualiza.FechaDesestimarRequerimiento,
                FechaEjecucion = CartaFianzaRequestActualiza.FechaEjecucion,
                UsuarioEdicion = CartaFianzaRequestActualiza.UsuarioEdicion,
                MotivoCartaFianzaRequerida = CartaFianzaRequestActualiza.MotivoCartaFianzaRequerida
            };

            iCartaFianzaDal.ActualizarPorCampos(objCartaFianza,

                                                x => x.IdEstadoCartaFianzaTm,
                                               // x => x.IdRenovarTm,
                                                x => x.IdTipoRequeridaTm,
                                               // x => x.IdTipoAccionTm,
                                                x => x.MotivoCartaFianzaRequerida,
                                                x => x.FechaEjecucion,
                                                x => x.FechaRenovacionEjecucion,
                                                x => x.FechaDesestimarRequerimiento,
                                                x => x.FechaEjecucion,
                                                x => x.UsuarioEdicion
                                                );

            iCartaFianzaDal.UnitOfWork.Commit();

            respuesta.Id = objCartaFianza.IdCartaFianza;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;

            return respuesta;

        }

        public CartaFianzaListaResponsePaginado ListaCartaFianza(CartaFianzaRequest CartaFianzaRequestLista)
        {
            return iCartaFianzaDal.ListaCartaFianza(CartaFianzaRequestLista);
        }

        public List<CartaFianzaListaResponse> ListaCartaFianzaId(CartaFianzaRequest CartaFianzaRequestLista)
        {
            return iCartaFianzaDal.ListaCartaFianzaId(CartaFianzaRequestLista);
        }

        public MailResponse ObtenerDatosCorreo(MailRequest mailResponse)
        {
            return iCartaFianzaDal.ObtenerDatosCorreo(mailResponse);
        }

        public DashBoardCartaFianzaResponse ListaDatos_DashoBoard_CartaFianza(DashBoardCartaFianzaRequest dashBoardCartaFianza)
        {

            return iCartaFianzaDal.ListaDatos_DashoBoard_CartaFianza(dashBoardCartaFianza);
        }

        public List<CartaFianzaListaResponse> ListaCartaFianzaCombo(CartaFianzaRequest cartaFianzaRequestLista)
        {
            return iCartaFianzaDal.ListaCartaFianzaCombo(cartaFianzaRequestLista);
        }

        public List<DashBoardCartaFianzaListGResponse> ListaDatos_DashoBoard_CartaFianza_Pie(DashBoardCartaFianzaRequest dashBoardCartaFianza)
        {

            return iCartaFianzaDal.ListaDatos_DashoBoard_CartaFianza_Pie(dashBoardCartaFianza);
        }

        public List<CartaFianzaListaResponse> CartasFianzasSinRespuesta(CartaFianzaRequest cartaFianzaRequest)
        {
            return iCartaFianzaDal.CartasFianzasSinRespuesta(cartaFianzaRequest);
        }

        public ProcesoResponse GuardarRespuestaGerenteCuenta(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacion)
        {
            return iCartaFianzaDal.GuardarRespuestaGerenteCuenta(cartaFianzaRenovacion);
        }

        public ProcesoResponse GuardarConfirmacionGerenteCuenta(CartaFianzaDetalleRenovacionRequest cartaFianzaRenovacion)
        {
            return iCartaFianzaDal.GuardarConfirmacionGerenteCuenta(cartaFianzaRenovacion);
        }
    }
}
