﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.CartaFianza;
using static Tgs.SDIO.DataContracts.Dto.Response.CartaFianza.CartaFianzaDetalleSeguimientoResponse;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.CartaFianza
{
    public class CartaFianzaDetalleRecuperoBl : ICartaFianzaDetalleRecuperoBl
    {
        readonly ICartaFianzaDetalleRecuperoDal iCartaFianzaDetalleRecuperoDal;

        public CartaFianzaDetalleRecuperoBl(ICartaFianzaDetalleRecuperoDal vICartaFianzaDetalleRecuperoDal)
        {
            this.iCartaFianzaDetalleRecuperoDal = vICartaFianzaDetalleRecuperoDal;
        }

        public ProcesoResponse ActualizaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest CartaFianzaDetalleRecuperoActualiza)
        {
            return iCartaFianzaDetalleRecuperoDal.ActualizaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoActualiza);
        }

        public CartaFianzaDetalleRecuperoResponsePaginado ListaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest CartaFianzaDetalleRecuperoLista)
        {
            return iCartaFianzaDetalleRecuperoDal.ListaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoLista);
        }

    }
}
