﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Funciones;
using Tgs.SDIO.Util.Mensajes.CartaFianza;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.CartaFianza
{
    public class CartaFianzaDetalleAccionBl : ICartaFianzaDetalleAccionBl
    {

        readonly ICartaFianzaDetalleAccionDal iCartaFianzaDetalleAccionesDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public CartaFianzaDetalleAccionBl(ICartaFianzaDetalleAccionDal viCartaFianzaDetalleAccionesDal)
        {
            iCartaFianzaDetalleAccionesDal = viCartaFianzaDetalleAccionesDal;
        }

        public ProcesoResponse ActualizaCartaFianzaDetalleAccion(CartaFianzaDetalleAccionRequest CartaFianzaDetalleAccionRequestActualiza)
        {
            try
            {
                var objCartaFianzaAccion = new CartaFianzaDetalleAccion()
                {
                    IdCartaFianza = CartaFianzaDetalleAccionRequestActualiza.IdCartaFianza,
                    IdColaboradorACargo = CartaFianzaDetalleAccionRequestActualiza.IdColaboradorACargo,
                    IdTipoAccionTm = CartaFianzaDetalleAccionRequestActualiza.IdTipoAccionTm,
                    //IdUsuarioCreacion = CartaFianzaDetalleAccionRequestActualiza.IdUsuarioCreacion,
                    IdUsuarioEdicion = CartaFianzaDetalleAccionRequestActualiza.IdUsuarioEdicion,
                    FechaRegistro = DateTime.Now,
                    FechaEdicion = DateTime.Now
                };
                iCartaFianzaDetalleAccionesDal.ActualizarPorCampos(objCartaFianzaAccion,
                                                    //x=> x.IdCliente,
                                                    x => x.IdColaboradorACargo,
                                                    x => x.IdTipoAccionTm,
                                                    x => x.IdUsuarioEdicion,
                                                    x => x.FechaEdicion,
                                                    x => x.FechaRegistro
                                                    );

                iCartaFianzaDetalleAccionesDal.UnitOfWork.Commit();

                respuesta.Id = objCartaFianzaAccion.IdCartaFianzaDetalleAccion;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;

                return respuesta;
            }
            catch (Exception ex)
            {
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.CartaFianza);
            }
            return respuesta;
        }

        public ProcesoResponse InsertaCartaFianzaDetalleAccion(CartaFianzaDetalleAccionRequest cartaFianzaDetalleAccionRequestInserta)
        {
            try
            {
                var objCartaFianzaAccion = new CartaFianzaDetalleAccion()
                {
                    IdCartaFianza = cartaFianzaDetalleAccionRequestInserta.IdCartaFianza,
                    IdColaboradorACargo = cartaFianzaDetalleAccionRequestInserta.IdColaboradorACargo,
                    IdTipoAccionTm = cartaFianzaDetalleAccionRequestInserta.IdTipoAccionTm,



                    IdUsuarioCreacion = cartaFianzaDetalleAccionRequestInserta.IdUsuarioCreacion,
                    //IdUsuarioEdicion = cartaFianzaDetalleAccionRequestInserta.IdUsuarioEdicion,
                    Observacion = cartaFianzaDetalleAccionRequestInserta.Observacion,
                    FechaRegistro = cartaFianzaDetalleAccionRequestInserta.FechaRegistro,
                    FechaCreacion = DateTime.Parse(Funciones.FormatoFechaHorams(DateTime.Now))


                };

                iCartaFianzaDetalleAccionesDal.Add(objCartaFianzaAccion);
                iCartaFianzaDetalleAccionesDal.UnitOfWork.Commit();

                respuesta.Id = objCartaFianzaAccion.IdCartaFianzaDetalleAccion;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;
            }
            catch (Exception ex)
            {
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.CartaFianza);
            }

            return respuesta;
        }

        public List<CartaFianzaDetalleAccionResponse> ListaCartaFianzaDetalleAccionId(CartaFianzaDetalleAccionRequest CartaFianzaDetalleAccionesRequestLista)
        {
               return iCartaFianzaDetalleAccionesDal.ListaCartaFianzaDetalleAccionId(CartaFianzaDetalleAccionesRequestLista);
        }
    }
}
