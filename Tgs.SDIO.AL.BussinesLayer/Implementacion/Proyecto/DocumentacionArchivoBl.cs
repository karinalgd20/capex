﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Proyecto
{
    public class DocumentacionArchivoBl : IDocumentacionArchivoBl
    {
        readonly IDocumentacionArchivoDal iDocumentacionArchivoDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public DocumentacionArchivoBl(IDocumentacionArchivoDal IDocumentacionArchivoDal)
        {
            iDocumentacionArchivoDal = IDocumentacionArchivoDal;
        }

        public DocumentacionArchivoDtoResponse ObtenerDocumentacionArchivo(int iIdDocumentoArchivo) {
            //DocumentacionArchivoDtoResponse oDocumentacionArchivoDtoResponse = iDocumentacionArchivoDal.GetFilteredAsNoTracking(da => da.IdDocumentoArchivo == iIdDocumentoArchivo)
            //                                                                 .Select(da => new DocumentacionArchivoDtoResponse {
            //                                                                     IdDocumentoArchivo = da.IdDocumentoArchivo,
            //                                                                     IdDocumentoProyecto = da.IdDocumentoProyecto.Value,
            //                                                                     ArchivoAdjunto = da.ArchivoAdjunto,

            //                                                                 }).FirstOrDefault();

            //return oDocumentacionArchivoDtoResponse;
            return iDocumentacionArchivoDal.ObtenerDocumentoArchivo(iIdDocumentoArchivo);
        }

        public bool InsertarDocumentacionArchivo(DocumentacionProyectoDtoRequest request)
        {
            DocumentacionArchivo oDocumentacionArchivo = new DocumentacionArchivo
            {
                IdDocumentoProyecto = request.IdDocumentoProyecto,
                ArchivoAdjunto = request.ArchivoAdjunto,
                IdEstado = Generales.Estados.Activo,
                FechaCreacion = DateTime.Now,
                IdUsuarioCreacion = 1
            };

            iDocumentacionArchivoDal.Add(oDocumentacionArchivo);
            iDocumentacionArchivoDal.UnitOfWork.Commit();

            return true;
        }
        
    }
}
