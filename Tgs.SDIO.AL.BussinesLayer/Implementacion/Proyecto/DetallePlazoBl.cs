﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Proyecto;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Proyecto
{
    public class DetallePlazoBl : IDetallePlazoBl
    {
        readonly IDetallePlazoDal iDetallePlazoDal;
        
        ProcesoResponse respuesta = new ProcesoResponse();
        public DetallePlazoBl(IDetallePlazoDal IDetallePlazoDal)
        {
            iDetallePlazoDal = IDetallePlazoDal;
        }

        public List<DetallePlazoDtoResponse> ListarDetallePlazo(int iIdProyecto) {

            List<DetallePlazoDtoResponse> ListaDetallePlazoDefault = new List<DetallePlazoDtoResponse>();
            ListaDetallePlazoDefault.Add(new DetallePlazoDtoResponse {
                DesActividad = "Implantación / Proyecto",
                IdActividad = 68,
                IdUnidadMedida = -1
            });
            ListaDetallePlazoDefault.Add(new DetallePlazoDtoResponse
            {
                DesActividad = "Contratación / Servicio",
                IdActividad = 69,
                IdUnidadMedida = -1
            });

            List<DetallePlazoDtoResponse> ListaDetallePlazo = iDetallePlazoDal.GetFilteredAsNoTracking(dp => dp.IdProyecto == iIdProyecto).Select(dp => new DetallePlazoDtoResponse {
                                                                IdDetallePlazo = dp.IdDetallePlazo,
                                                                IdProyecto = dp.IdProyecto,
                                                                Cantidad = dp.Cantidad,
                                                                Anios = dp.Anios,
                                                                Meses = dp.Meses,
                                                                Dias = dp.Dias,
                                                                FechaInicio = dp.FechaInicio,
                                                                FechaFin = dp.FechaFin,
                                                                IdActividad = dp.IdActividad,
                                                                IdUnidadMedida = dp.IdUnidadMedida,
                                                                VentaDirecta = dp.VentaDirecta
                                                              }).ToList();

            ListaDetallePlazoDefault = ListaDetallePlazoDefault.GroupJoin(ListaDetallePlazo,
                                                                    dpdefault => dpdefault.IdActividad,
                                                                    dp => dp.IdActividad,
                                                                    (dpdefault, dp) => new { dpdefault, dp }).Select(dp => new DetallePlazoDtoResponse
                                                                    {
                                                                        IdDetallePlazo = dp.dp.FirstOrDefault() == null ? 0 : dp.dp.FirstOrDefault().IdDetallePlazo,
                                                                        IdProyecto = dp.dp.FirstOrDefault() == null ? 0 : dp.dp.FirstOrDefault().IdProyecto,
                                                                        Cantidad = dp.dp.FirstOrDefault() == null ? 0 : dp.dp.FirstOrDefault().Cantidad,
                                                                        FechaInicio = dp.dp.FirstOrDefault() == null ? null : dp.dp.FirstOrDefault().FechaInicio,
                                                                        FechaFin = dp.dp.FirstOrDefault() == null ? null : dp.dp.FirstOrDefault().FechaFin,
                                                                        IdActividad = dp.dpdefault.IdActividad,
                                                                        IdUnidadMedida = dp.dp.FirstOrDefault() == null ? -1 : dp.dp.FirstOrDefault().IdUnidadMedida,
                                                                        VentaDirecta = dp.dp.FirstOrDefault() == null ? null : dp.dp.FirstOrDefault().VentaDirecta,
                                                                        DesActividad = dp.dpdefault.DesActividad,
                                                                        Anios = dp.dp.FirstOrDefault() == null ? 0 : dp.dp.FirstOrDefault().Anios,
                                                                        Meses = dp.dp.FirstOrDefault() == null ? 0 : dp.dp.FirstOrDefault().Meses,
                                                                        Dias = dp.dp.FirstOrDefault() == null ? 0 : dp.dp.FirstOrDefault().Dias
                                                                    }).ToList();

            return ListaDetallePlazoDefault;
        }

        public ProcesoResponse InsertarDetallePlazo(DetallePlazoDtoRequest request)
        {
            try
            {
                bool bEsNuevo = false;

                if (request.IdDetallePlazo == 0)
                {
                    bEsNuevo = true;
                }

                DetallePlazo objDetallePlazo = iDetallePlazoDal.GetFilteredAsNoTracking(dp => dp.IdDetallePlazo == request.IdDetallePlazo).FirstOrDefault();
                DetallePlazo oDetallePlazo = new DetallePlazo
                {
                    IdDetallePlazo = request.IdDetallePlazo,
                    IdProyecto = request.IdProyecto,
                    IdActividad = request.IdActividad,
                    Cantidad = request.Cantidad,
                    FechaInicio = request.FechaInicio,
                    FechaFin = request.FechaFin,
                    IdUnidadMedida = request.IdUnidadMedida,
                    VentaDirecta = request.VentaDirecta,
                    Anios = request.Anios,
                    Meses = request.Meses,
                    Dias = request.Dias
                };

                if (bEsNuevo)
                {
                    oDetallePlazo.IdEstado = Generales.Estados.Activo;
                    oDetallePlazo.FechaCreacion = DateTime.Now;
                    oDetallePlazo.IdUsuarioCreacion = request.IdUsuarioCreacion;
                    iDetallePlazoDal.Add(oDetallePlazo);
                }
                else
                {
                    oDetallePlazo.IdEstado = objDetallePlazo.IdEstado;
                    oDetallePlazo.FechaCreacion = objDetallePlazo.FechaCreacion;
                    oDetallePlazo.IdUsuarioCreacion = objDetallePlazo.IdUsuarioCreacion;

                    if (request.Modificado)
                    {
                        oDetallePlazo.FechaEdicion = DateTime.Now;
                        oDetallePlazo.IdUsuarioEdicion = request.IdUsuarioEdicion;
                        iDetallePlazoDal.Modify(oDetallePlazo);
                    }
                }

                iDetallePlazoDal.UnitOfWork.Commit();

                respuesta.Id = oDetallePlazo.IdDetallePlazo;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralProyecto.ActualizarPlazoProyecto;

                return respuesta;
            }
            catch (Exception ex)
            {
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.Proyecto);
            }

            return respuesta;
        }
    }
}
