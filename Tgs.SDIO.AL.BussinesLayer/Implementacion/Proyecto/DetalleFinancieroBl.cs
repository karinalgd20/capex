﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Proyecto;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Proyecto
{
    public class DetalleFinancieroBl : IDetalleFinancieroBl
    {
        readonly IDetalleFinancieroDal iDetalleFinancieroDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public DetalleFinancieroBl(IDetalleFinancieroDal IDetalleFinancieroDal)
        {
            iDetalleFinancieroDal = IDetalleFinancieroDal;
        }

        public List<DetalleFinancieroDtoResponse> ListarDetalleFinanciero(int iIdProyecto)
        {

            List<DetalleFinancieroDtoResponse> ListaDetalleFinanciero = iDetalleFinancieroDal.GetFilteredAsNoTracking(dp => dp.IdProyecto == iIdProyecto && dp.IdTipoDetalle == 2).Select(dp => new DetalleFinancieroDtoResponse
            {
                Linea = dp.Linea,
                SubLinea = dp.SubLinea,
                Servicio = dp.Servicio,
                Producto = dp.Producto,
                AnalistaCGUsuarioLotus = dp.AnalistaCGUsuarioLotus,
                Porcentaje = dp.Porcentaje,
                PorcentajeCalculado = Convert.ToString(Math.Round(Convert.ToDecimal(dp.Porcentaje) * 100, 0))
            }).ToList();


            return ListaDetalleFinanciero;
        }

    }
}
