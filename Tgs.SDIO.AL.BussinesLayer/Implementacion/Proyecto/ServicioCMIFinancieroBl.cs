﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;
using Tgs.SDIO.Util.Constantes;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Proyecto
{
    public class ServicioCMIFinancieroBl : IServicioCMIFinancieroBl
    {
        readonly IServicioCMIFinancieroDal iServicioCMIFinancieroDal;
        readonly IProyectoDal iProyectoDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public ServicioCMIFinancieroBl(IServicioCMIFinancieroDal IServicioCMIFinancieroDal, IProyectoDal IProyectoDal)
        {
            iServicioCMIFinancieroDal = IServicioCMIFinancieroDal;
            iProyectoDal = IProyectoDal;
        }

        public ServicioCMIFinancieroDtoResponse ObtenerServicioCMIFinanciero(ServicioCMIFinancieroDtoRequest request)
        {
            
            ServicioCMIFinanciero objRecursoProyecto = iServicioCMIFinancieroDal.GetFilteredAsNoTracking(e => e.IdDetalleFinanciero == request.IdDetalleFinanciero).FirstOrDefault();
            ServicioCMIFinancieroDtoResponse oServicioCMIFinanciero = new ServicioCMIFinancieroDtoResponse();
            if (objRecursoProyecto != null) {
                oServicioCMIFinanciero = new ServicioCMIFinancieroDtoResponse
                {
                    IdServicioFinanciero = objRecursoProyecto.IdServicioFinanciero,
                    IdDetalleFinanciero = objRecursoProyecto.IdDetalleFinanciero,
                    CeCo = objRecursoProyecto.CeCo,
                    CentroGestor = objRecursoProyecto.CentroGestor,
                    CodigoCMI = objRecursoProyecto.CodigoCMI,
                    ElementoPEP = objRecursoProyecto.ElementoPEP
                };
            }
            

            return oServicioCMIFinanciero;
        }

        public ProcesoResponse InsertarServicioCMIFinanciero(ServicioCMIFinancieroDtoRequest request)
        {
            try
            {
                bool bEsNuevo = false;

                if (request.IdServicioFinanciero == 0)
                {
                    bEsNuevo = true;
                }

                ServicioCMIFinanciero objRecursoProyecto = iServicioCMIFinancieroDal.GetFilteredAsNoTracking(e => e.IdDetalleFinanciero == request.IdDetalleFinanciero).FirstOrDefault();
                ServicioCMIFinanciero oServicioCMIFinanciero = new ServicioCMIFinanciero
                {
                    IdServicioFinanciero = request.IdServicioFinanciero,
                    IdDetalleFinanciero = request.IdDetalleFinanciero,
                    CeCo = request.CeCo,
                    CentroGestor = request.CentroGestor,
                    CodigoCMI = request.CodigoCMI,
                    ElementoPEP = request.ElementoPEP,
                    FechaCreacion = objRecursoProyecto == null ? DateTime.Now : objRecursoProyecto.FechaCreacion,
                    IdUsuarioCreacion = objRecursoProyecto == null ? request.IdUsuarioCreacion : objRecursoProyecto.IdUsuarioCreacion
                };

                if (bEsNuevo)
                {
                    oServicioCMIFinanciero.IdEstado = Generales.Estados.Activo;
                    oServicioCMIFinanciero.FechaCreacion = DateTime.Now;
                    oServicioCMIFinanciero.IdUsuarioCreacion = request.IdUsuarioCreacion;
                    iServicioCMIFinancieroDal.Add(oServicioCMIFinanciero);
                }
                else
                {
                    oServicioCMIFinanciero.FechaEdicion = DateTime.Now;
                    oServicioCMIFinanciero.IdUsuarioEdicion = request.IdUsuarioEdicion;
                    iServicioCMIFinancieroDal.Modify(oServicioCMIFinanciero);
                }

                iServicioCMIFinancieroDal.UnitOfWork.Commit();

                if (bEsNuevo) RegistrarEtapaEstado(request);

                respuesta.Mensaje = "Grabado con éxito.";
                respuesta.TipoRespuesta = Proceso.Valido;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }

        private void RegistrarEtapaEstado(ServicioCMIFinancieroDtoRequest request) {
            EtapaProyectoDtoRequest oEtapaProyectoRequest = new EtapaProyectoDtoRequest
            {
                IdProyecto = request.IdProyecto,
                IdFase = Configuracion.IdFasePreImplantacion,
                IdEtapa = Configuracion.IdEtapaJP,
                IdEstadoEtapa = Generales.ProyectoControlEstado.Registrado,
                Observaciones = "",
                IdUsuarioCreacion = request.IdUsuarioCreacion
            };

            iProyectoDal.RegistrarEtapaEstado(oEtapaProyectoRequest);
        }
    }
}
