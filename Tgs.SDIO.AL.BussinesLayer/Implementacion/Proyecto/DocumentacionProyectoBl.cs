﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Proyecto
{
    public class DocumentacionProyectoBl : IDocumentacionProyectoBl
    {
        readonly IDocumentacionProyectoDal iDocumentacionProyectoDal;
        readonly IDocumentacionArchivoBl iDocumentacionArchivoBl;

        ProcesoResponse respuesta = new ProcesoResponse();
        public DocumentacionProyectoBl(IDocumentacionProyectoDal IDocumentacionProyectoDal, IDocumentacionArchivoBl IDocumentacionArchivoBl)
        {
            iDocumentacionProyectoDal = IDocumentacionProyectoDal;
            iDocumentacionArchivoBl = IDocumentacionArchivoBl;
        }

        public List<DocumentacionProyectoDtoResponse> ListarDocumentacionProyecto(int iIdProyecto) {
            return iDocumentacionProyectoDal.ListarDocumentacionProyecto(iIdProyecto);
        }

        public bool InsertarDocumentacionProyecto(DocumentacionProyectoDtoRequest request)
        {
            DocumentacionProyecto objDocumentacionProyecto = iDocumentacionProyectoDal.GetFilteredAsNoTracking(dp => dp.IdDocumentoProyecto == request.IdDocumentoProyecto).FirstOrDefault();
            DocumentacionProyecto oDocumentacionProyecto = new DocumentacionProyecto
            {
                IdDocumentoProyecto = request.IdDocumentoProyecto,
                IdDocumento = request.IdDocumento,
                IdProyecto = request.IdProyecto,
                NombreArchivo = request.NombreArchivo,
                IdEstado = Generales.Estados.Activo,
                FechaCreacion = DateTime.Now,
                IdUsuarioCreacion = request.IdUsuarioCreacion
            };

            iDocumentacionProyectoDal.Add(oDocumentacionProyecto);
            iDocumentacionProyectoDal.UnitOfWork.Commit();

            request.IdDocumentoProyecto = oDocumentacionProyecto.IdDocumentoProyecto;
            iDocumentacionArchivoBl.InsertarDocumentacionArchivo(request);

            return true;
        }

        public bool EliminarDocumentacionProyecto(DocumentacionProyectoDtoRequest request) {
            DocumentacionProyecto objDocumentacionProyecto = iDocumentacionProyectoDal.GetFilteredAsNoTracking(dp => dp.IdDocumentoProyecto == request.IdDocumentoProyecto).FirstOrDefault();
            iDocumentacionProyectoDal.Remove(objDocumentacionProyecto);
            iDocumentacionProyectoDal.UnitOfWork.Commit();

            return true;
        }
    }
}
