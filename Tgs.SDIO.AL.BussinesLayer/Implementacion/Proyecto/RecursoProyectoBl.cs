﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Proyecto
{
    public class RecursoProyectoBl : IRecursoProyectoBl
    {
        readonly IRecursoProyectoDal iRecursoProyectoDal;
        
        ProcesoResponse respuesta = new ProcesoResponse();
        public RecursoProyectoBl(IRecursoProyectoDal IRecursoProyectoDal)
        {
            iRecursoProyectoDal = IRecursoProyectoDal;
        }

        public List<RecursoProyectoDtoResponse> ListarRecursoProyecto(int iIdProyecto) {
            return iRecursoProyectoDal.ListarRecursoProyecto(iIdProyecto);
        }

        public bool InsertarRecursoProyecto(RecursoProyectoDtoRequest request)
        {
            RecursoProyecto objRecursoProyecto = iRecursoProyectoDal
                                                .GetFilteredAsNoTracking(dp => dp.IdRol == request.IdRol &&
                                                                        dp.IdProyecto == request.IdProyecto)
                                                .FirstOrDefault();
            var idAsignacion = (objRecursoProyecto == null ? request.IdAsignacion : objRecursoProyecto.IdAsignacion);

            RecursoProyecto oRecursoProyecto = new RecursoProyecto
            {
                IdAsignacion = idAsignacion,
                IdProyecto = request.IdProyecto,
                IdRecurso = request.IdRecurso,
                IdRol = request.IdRol,
                FechaCreacion = objRecursoProyecto == null ? DateTime.Now : objRecursoProyecto.FechaCreacion,
                IdUsuarioCreacion = objRecursoProyecto == null ? request.IdUsuarioCreacion : objRecursoProyecto.IdUsuarioCreacion
            };

            if (objRecursoProyecto == null)
            {
                oRecursoProyecto.IdEstado = Generales.Estados.Activo;
                oRecursoProyecto.FechaCreacion = DateTime.Now;
                oRecursoProyecto.IdUsuarioCreacion = request.IdUsuarioCreacion;
                iRecursoProyectoDal.Add(oRecursoProyecto);
            }
            else {
                if (request.Modificado) {
                    oRecursoProyecto.FechaEdicion = DateTime.Now;
                    oRecursoProyecto.IdUsuarioEdicion = request.IdUsuarioEdicion;
                    iRecursoProyectoDal.ActualizarPorCampos(oRecursoProyecto,
                                        x => x.IdAsignacion,
                                        x => x.IdRecurso,
                                        x => x.FechaEdicion,
                                        x => x.IdUsuarioEdicion);
                }
            }

            iRecursoProyectoDal.UnitOfWork.Commit();

            return true;
        }

        public RecursoProyectoDtoResponse ObtenerRecursoPorProyectoRol(RecursoProyectoDtoRequest request)
        {
           return iRecursoProyectoDal.ObtenerRecursoPorProyectoRol(request);
        }

    }
}
