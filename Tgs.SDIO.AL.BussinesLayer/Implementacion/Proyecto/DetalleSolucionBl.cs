﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Proyecto
{
    public class DetalleSolucionBl : IDetalleSolucionBl
    {
        readonly IDetalleSolucionDal iDetalleSolucionDal;

        ProcesoResponse respuesta = new ProcesoResponse();

        public DetalleSolucionBl(IDetalleSolucionDal IDetalleSolucionDal)
        {
            iDetalleSolucionDal = IDetalleSolucionDal;
        }

        public List<DetalleSolucionDtoResponse> ObtenerDetallePorIdProyecto(DetalleSolucionDtoRequest request)
        {
            return iDetalleSolucionDal.ObtenerDetallePorIdProyecto(request);
        }

        public DetalleSolucionDtoResponse ObtenerDetalleSolucionById(DetalleSolucionDtoRequest request)
        {
            return iDetalleSolucionDal.ObtenerDetalleSolucionById(request);
        }

        public ProcesoResponse RegistrarDetalleSolucion(DetalleSolucionDtoRequest request)
        {
            var entidad = iDetalleSolucionDal
                            .GetFiltered(x => x.IdProyecto == request.IdProyecto && 
                                              x.IdTipoSolucion == request.IdTipoSolucion
                            ).FirstOrDefault();

            if (entidad == null)
            {
                entidad = new DetalleSolucion
                {
                    IdProyecto = request.IdProyecto,
                    IdTipoSolucion = request.IdTipoSolucion,
                };
                iDetalleSolucionDal.Add(entidad);
            }

            iDetalleSolucionDal.UnitOfWork.Commit();

            respuesta.Id = entidad.IdDetalleSolucion;
            respuesta.TipoRespuesta = Proceso.Valido;

            return respuesta;
        }

        public ProcesoResponse EliminarDetalleSolucion(DetalleSolucionDtoRequest request)
        {
            var entidad = iDetalleSolucionDal
                            .GetFiltered(x => x.IdProyecto == request.IdProyecto &&
                                              x.IdTipoSolucion == request.IdTipoSolucion
                            ).FirstOrDefault();

            if (entidad != null)
            {
                iDetalleSolucionDal.Remove(entidad);
                iDetalleSolucionDal.UnitOfWork.Commit();
            }

            respuesta.TipoRespuesta = Proceso.Valido;

            return respuesta;
        }


    }
}
