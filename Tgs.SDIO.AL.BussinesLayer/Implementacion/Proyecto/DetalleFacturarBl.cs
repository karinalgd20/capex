﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Entities.Entities.Proyecto;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Proyecto
{
    public class DetalleFacturarBl : IDetalleFacturarBl
    {
        readonly IDetalleFacturarDal iDetalleFacturarDal;

        ProcesoResponse respuesta = new ProcesoResponse();

        public DetalleFacturarBl(IDetalleFacturarDal IDetalleFacturarDal)
        {
            iDetalleFacturarDal = IDetalleFacturarDal;
        }

        public DetalleFacturarDtoResponse ObtenerDetalleFacturarPorIdProyecto(DetalleFacturarDtoRequest request)
        {
            return iDetalleFacturarDal.ObtenerDetalleFacturarPorIdProyecto(request);
        }

        public DetalleFacturarDtoResponse ObtenerDetalleFacturarPorId(DetalleFacturarDtoRequest request)
        {
            return iDetalleFacturarDal.ObtenerDetalleFacturarPorId(request);
        }

        public ProcesoResponse RegistrarDetalleFacturar(DetalleFacturarDtoRequest request)
        {
            try
            {
                var entidad = new DetalleFacturar()
                {
                    IdProyecto = request.IdProyecto,
                    IdConceptoIngresoEgreso = request.IdConceptoIngresoEgreso,
                    IdFormaPago = request.IdFormaPago,
                    IdMonedaPagoUnico = request.IdMonedaPagoUnico,
                    MontoPagoUnico = request.MontoPagoUnico,
                    IdMonedaRecurrenteMensual = request.IdMonedaRecurrenteMensual,
                    MontoRecurrenteMensual = request.MontoRecurrenteMensual
                };

                iDetalleFacturarDal.Add(entidad);
                iDetalleFacturarDal.UnitOfWork.Commit();

                respuesta.Id = entidad.IdDetalleFacturar;
                respuesta.TipoRespuesta = Proceso.Valido;
            }
            catch (Exception ex)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.Proyecto);

            }

            return respuesta;
        }

        public ProcesoResponse ActualizarDetalleFacturar(DetalleFacturarDtoRequest request)
        {
            try
            {
                var detalleFactEL = new DetalleFacturar()
                {
                    IdConceptoIngresoEgreso = request.IdConceptoIngresoEgreso,
                    IdFormaPago = request.IdFormaPago,
                    IdMonedaPagoUnico = request.IdMonedaPagoUnico,
                    MontoPagoUnico = request.MontoPagoUnico,
                    IdMonedaRecurrenteMensual = request.IdMonedaRecurrenteMensual,
                    MontoRecurrenteMensual = request.MontoRecurrenteMensual,

                    IdDetalleFacturar = request.IdDetalleFacturar,
                };

                iDetalleFacturarDal.ActualizarPorCampos(detalleFactEL,
                                       x => x.IdConceptoIngresoEgreso,
                                       x => x.IdFormaPago,
                                       x => x.IdMonedaPagoUnico,
                                       x => x.MontoPagoUnico,
                                       x => x.IdMonedaRecurrenteMensual,
                                       x => x.MontoRecurrenteMensual
                                    );

                iDetalleFacturarDal.UnitOfWork.Commit();

                respuesta.Id = detalleFactEL.IdDetalleFacturar;
                respuesta.TipoRespuesta = Proceso.Valido;

                return respuesta;
            }
            catch (Exception ex)
            {
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.Proyecto);
            }
            return respuesta;
        }
    }
}
