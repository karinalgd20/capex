﻿using System;
using System.Collections.Generic;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Proyecto;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Proyecto;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Proyecto;
using static Tgs.SDIO.Util.Constantes.Generales;
using Entidad = Tgs.SDIO.Entities.Entities.Proyecto;
namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Proyecto
{
    public class ProyectoBl : IProyectoBl
    {
        readonly IProyectoDal iProyectoDal;
        

        ProcesoResponse respuesta = new ProcesoResponse();
        public ProyectoBl(IProyectoDal IProyectoDal)
        {
            iProyectoDal = IProyectoDal;
        }

        public List<ProyectoDtoResponse> ListarProyectoCabecera(ProyectoDtoRequest proyecto)
        {
            return iProyectoDal.ListarProyectoCabecera(proyecto);
        }

        public OportunidadGanadaDtoResponse ObtenerOportunidadGanada(OportunidadGanadaDtoRequest request)
        {
            return iProyectoDal.ObtenerOportunidadGanada(request);
        }

        public ProcesoResponse RegistrarProyecto(ProyectoDtoRequest proyecto)
        {
            return iProyectoDal.RegistrarProyecto(proyecto);
        }

        public ProcesoResponse ActualizarProyecto(ProyectoDtoRequest proyecto)
        {

            try
            {
                var proyectoEL = new Entidad.Proyecto()
                {
                    IdProyectoSF = proyecto.IdProyectoSF,
                    IdCliente = proyecto.IdCliente,
                    FechaOportunidadGanada = proyecto.FechaOportunidadGanada,
                    IdEstado = proyecto.IdEstado,
                    FechaEdicion = proyecto.FechaModifica,
                    IdUsuarioEdicion = proyecto.IdUsuarioModifica,

                    IdProyecto = proyecto.IdProyecto
                };

                iProyectoDal.ActualizarPorCampos(proyectoEL,
                                       x => x.IdProyectoSF,
                                       x => x.IdCliente,
                                       x => x.FechaOportunidadGanada,
                                       x => x.IdEstado,
                                       x => x.FechaEdicion,
                                       x => x.IdUsuarioEdicion
                                        );
                iProyectoDal.UnitOfWork.Commit();

                respuesta.Id = proyectoEL.IdProyecto;
                respuesta.Mensaje = MensajesGeneralProyecto.ActualizarProyecto;
                respuesta.TipoRespuesta = Proceso.Valido;
            }
            catch (Exception ex)
            {
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.Proyecto);
            }
            return respuesta;
        }

        public ProyectoDtoResponse ObtenerProyectoByID(ProyectoDtoRequest proyecto)
        {
            return iProyectoDal.ObtenerProyectoByID(proyecto);
        }

        public ProcesoResponse ActualizarProyectoDescripcion(ProyectoDtoRequest request)
        {
            try
            {
                var proyectoEL = new Entidad.Proyecto()
                {
                    IdComplejidad = request.IdComplejidad,
                    IdOficinaOportunidad = request.IdOficinaOportunidad,
                    FechaEdicion = request.FechaModifica,
                    Descripcion = request.Descripcion,
                    IdUsuarioEdicion = request.IdUsuarioModifica,

                    IdProyecto = request.IdProyecto,
                };

                iProyectoDal.ActualizarPorCampos(proyectoEL,
                                       x => x.IdComplejidad,
                                       x => x.IdOficinaOportunidad,
                                       x => x.Descripcion,
                                       x => x.IdUsuarioEdicion,
                                       x => x.FechaEdicion                        
                                        );

                iProyectoDal.UnitOfWork.Commit();

                respuesta.Id = proyectoEL.IdProyecto;
                respuesta.TipoRespuesta = Proceso.Valido;

                return respuesta;
            }
            catch (Exception ex)
            {
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.Proyecto);
            }
            return respuesta;
        }

        public ProcesoResponse RegistrarStageFinanciero(DetalleFinancieroDtoRequest request)
        {
            return iProyectoDal.RegistrarStageFinanciero(request);
        }

        public ProcesoResponse ProcesarArchivosFinancieros(ProyectoDtoRequest request)
        {
            return iProyectoDal.ProcesarArchivosFinancieros(request);
        }

        public List<PagoFinancieroDtoResponse> ListarPagos(ProyectoDtoRequest request)
        {
            return iProyectoDal.ListarPagos(request);
        }

        public List<IndicadorFinancieroDtoResponse> ListarIndicadores(ProyectoDtoRequest request)
        {
            return iProyectoDal.ListarIndicadores(request);
        }

        public List<CostoFinancieroDtoResponse> ListarCostos(ProyectoDtoRequest request)
        {
            return iProyectoDal.ListarCostos(request);
        }

        public List<CostoConceptoDtoResponse> ListarCostosOpex(ProyectoDtoRequest request)
        {
            return iProyectoDal.ListarCostosOpex(request);
        }

        public List<CostoConceptoDtoResponse> ListarCostosCapex(ProyectoDtoRequest request)
        {
            return iProyectoDal.ListarCostosCapex(request);
        }

        public List<DetalleFinancieroDtoResponse> ListarServiciosCMI(ProyectoDtoRequest request)
        {
            return iProyectoDal.ListarServiciosCMI(request);
        }

        public ProcesoResponse RegistrarEtapaEstado(EtapaProyectoDtoRequest request)
        {
            return iProyectoDal.RegistrarEtapaEstado(request);
        }

        public List<DetalleFinancieroDtoResponse> BuscarDetalleFinancieroPorProyecto(ProyectoDtoRequest request)
        {
            return iProyectoDal.BuscarDetalleFinancieroPorProyecto(request);
        }
        public List<ProyectoDtoResponse> ListarProyecto(ProyectoDtoRequest request)
        {
            return iProyectoDal.ListarProyecto(request);
        }
    }
}
