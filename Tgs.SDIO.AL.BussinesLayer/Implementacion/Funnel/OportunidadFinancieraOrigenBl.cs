﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Funnel;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Funnel;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;
using TitulosDetalleOportunidades = Tgs.SDIO.Util.Constantes.Funnel.TitulosDetalleOportunidades;
using NivelesDashboardReportes = Tgs.SDIO.Util.Constantes.Funnel.NivelesDashboardReportes;
using System;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Funnel
{
    public class OportunidadFinancieraOrigenBl : IOportunidadFinancieraOrigenBl
    {
        readonly IOportunidadFinancieraOrigenDal iOportunidadFinancieraOrigenDal;
        readonly ISalesForceConsolidadoCabeceraDal iSalesForceConsolidadoCabeceraDal;
        readonly IRecursoDal iRecursoDal;

        public OportunidadFinancieraOrigenBl(IOportunidadFinancieraOrigenDal IOportunidadFinancieraOrigenDal,
            ISalesForceConsolidadoCabeceraDal ISalesForceConsolidadoCabeceraDal,
            IRecursoDal IRecursoDal)
        {
            iOportunidadFinancieraOrigenDal = IOportunidadFinancieraOrigenDal;
            iSalesForceConsolidadoCabeceraDal = ISalesForceConsolidadoCabeceraDal;
            iRecursoDal = IRecursoDal;
        }

        public IndicadorMadurezDtoResponse ListarIndicadorMadurez(IndicadorMadurezDtoRequest request)
        {
            request.Mes = request.Mes.Equals(0) ? null : request.Mes;
            request.IdLineaNegocio = request.IdLineaNegocio.Equals(0) ? null : request.IdLineaNegocio;
            request.IdSector = request.IdSector.Equals(0) ? null : request.IdSector;
            request.ProbabilidadExito = request.ProbabilidadExito.Equals(-1) ? null : request.ProbabilidadExito;
            request.Etapa = request.Etapa.Equals("0") ? null : request.Etapa;

            var indicadorMadurez = iOportunidadFinancieraOrigenDal.ListarIndicadorMadurez(request);
            var indicadorMadurezCostos = iSalesForceConsolidadoCabeceraDal.ListarIndicadorCostoOportunidad(request);

            return new IndicadorMadurezDtoResponse { ListaMadurez = indicadorMadurez, ListaMadurezCostos = indicadorMadurezCostos };       
        }

        public DashoardDtoResponse MostrarDashboard(DashoardDtoRequest request)
        {
            var requestListaMadurez = new IndicadorMadurezDtoRequest { Anio = request.Anio };
            var indicadorMadurez = iOportunidadFinancieraOrigenDal.ListarIndicadorMadurez(requestListaMadurez);
            var indicadorOfertasLineaNegocio = iOportunidadFinancieraOrigenDal.ListarIndicadorOfertasLineaNegocio(request);
            var indicadorOfertasSector = iOportunidadFinancieraOrigenDal.ListarIndicadorOfertasSector(request);
            var indicadorIngresosLineaNegocio = iOportunidadFinancieraOrigenDal.ListarIndicadorIngresosLineaNegocio(request);
            var indicadorIngresosSector = iOportunidadFinancieraOrigenDal.ListarIndicadorIngresosSector(request);
            var indicadorOfertasEstado = iOportunidadFinancieraOrigenDal.ListarIndicadorOfertasEstado(request);
            var indicadorTotales = iOportunidadFinancieraOrigenDal.ListarIndicadoresTotales(request).FirstOrDefault();

            return new DashoardDtoResponse
            {
                ListaMadurez = indicadorMadurez.Select(x => new DashoardMadurezDtoResponse
                {
                    Madurez = x.Madurez,
                    Mes = x.Mes,
                    Anio = request.Anio,
                    IdOportunidad = x.IdOportunidad,
                    Capex = x.Capex
                }).ToList(),
                OfertasLineaNegocio = indicadorOfertasLineaNegocio.ToList(),
                OfertasSector = indicadorOfertasSector.ToList(),
                IngresosLineaNegocio = indicadorIngresosLineaNegocio.ToList(),
                IngresosSector = indicadorIngresosSector.ToList(),
                OfertasEstado = indicadorOfertasEstado.ToList(),
                OportunidadesTrabajadas = indicadorTotales.OportTrabajadas,
                IngresoTotal = indicadorTotales.SumIngresoAnual,
                CapexTotal = indicadorTotales.SumCapex,
                OpexTotal = 0,
                OibdaTotal = indicadorTotales.SumOibdaAnual
            };
        }

        public IndicadorDashboardPreventaConsolidadoDtoResponse ListarSeguimientoOportunidadesPreventaGerente(IndicadorDashboardPreventaDtoRequest request)
        {
            var ordenMostraDetalle = Util.Constantes.Generales.Numeric.Cero;

            var resultadoDashoard = new List<IndicadorDashboardPreventaDtoResponse>();
            var listaOportunidades = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesPreventaGerenteLider(request);

            foreach (var elemento in listaOportunidades)
            {
                resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                {
                    IdLider = elemento.IdLider,
                    Lider = elemento.Lider,
                    IdPreventa = Util.Constantes.Generales.Numeric.Cero,
                    Preventa = string.Empty,
                    Nivel = Util.Constantes.Generales.Numeric.Uno,
                    Desplegado = Util.Constantes.Generales.Numeric.Cero,
                    EquipoPreventaLiderCadena = elemento.EquipoPreventaLider.ToString(),
                    CarteraClientesLiderCadena = elemento.CarteraClientesLider.ToString(),
                    NroOportunidadesLiderCadena = elemento.NroOportunidadesLider.ToString(),
                    WinRateLiderCadena = elemento.WinRateLider.ToString(),
                    TiempoEntregaPromedioLiderCadena = elemento.TiempoEntregaPromedioLider.ToString(),
                    PorcentajeCumplimientoLiderCadena = elemento.PorcentajeCumplimientoLider.ToString(),
                    WinRateClienteCadena = string.Empty,
                    TiempoEntregaPromedioClienteCadena = string.Empty,
                    PorcentajeCumplimientoClienteCadena = string.Empty,
                    Orden = elemento.Orden,
                    OrdenMostrar = Util.Constantes.Generales.Numeric.Cero,
                    FechaCierreEstimadaCadena = string.Empty,
                    Payback = string.Empty,
                    PagoUnicoDolaresCadena = string.Empty,
                    RecurrenteDolaresCadena = string.Empty,
                    PeriodoMesesCadena = string.Empty,
                    TipoCambioCadena = string.Empty
                });
            }

            if (request.Nivel == Util.Constantes.Generales.Numeric.Uno)
            {
                return new IndicadorDashboardPreventaConsolidadoDtoResponse
                {
                    ListaOportunidades = resultadoDashoard.OrderBy(w => w.Orden).ToList(),
                    Total = listaOportunidades.Count() > Util.Constantes.Generales.Numeric.Cero ?
                    listaOportunidades.ToList()[0].TotalRow.Value : Util.Constantes.Generales.Numeric.Cero
                };
            }


            #region Nivel0


            if (request.Nivel == Util.Constantes.Generales.Numeric.Cero)
            {
                var listaLiderPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaLiderIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaLiderIncluir);
                var grupoLiderIncluir = (from g in listaLiderIncluir
                                         group g by new
                                         {
                                             idLider = g.idLider,
                                             Level = g.Level
                                         }
                                   into g
                                         select new
                                         {
                                             Id = g.Key.idLider,
                                             Name = g.Key.Level,
                                             FechaHora = g.Max(d => d.FechaHora)
                                         }).Select(x => new PersonalSeleccionadoDashboard
                                         {
                                             idLider = x.Id,
                                             Level = x.Name,
                                             FechaHora = x.FechaHora
                                         });


                var listaLiderExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaLiderExcluir);
                var grupoLiderExcluir = (from g in listaLiderExcluir
                                         group g by new
                                         {
                                             idLider = g.idLider,
                                             Level = g.Level
                                         }
                                  into g
                                         select new
                                         {
                                             Id = g.Key.idLider,
                                             Name = g.Key.Level,
                                             FechaHora = g.Max(d => d.FechaHora)
                                         }).Select(x => new PersonalSeleccionadoDashboard
                                         {
                                             idLider = x.Id,
                                             Level = x.Name,
                                             FechaHora = x.FechaHora
                                         });

                foreach (var itemIncluir in grupoLiderIncluir)
                {
                    var itemAgregar = grupoLiderExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaLiderPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoLiderExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaLiderPersonal.Add(itemIncluir);
                        }
                    }
                }


                var listaPreventaPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaPreventaIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaPreventaIncluir);
                var grupoPreventaIncluir = (from g in listaPreventaIncluir
                                            group g by new
                                            {
                                                idLider = g.idLider,
                                                idPreventa = g.idPreventa,
                                                Level = g.Level
                                            }
                                   into g
                                            select new
                                            {
                                                IdLider = g.Key.idLider,
                                                Level = g.Key.Level,
                                                IdPreventa = g.Key.idPreventa,
                                                FechaHora = g.Max(d => d.FechaHora)
                                            }).Select(x => new PersonalSeleccionadoDashboard
                                            {
                                                idLider = x.IdLider,
                                                Level = x.Level,
                                                idPreventa = x.IdPreventa,
                                                FechaHora = x.FechaHora
                                            });


                var listaPreventaExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaPreventaExcluir);
                var grupoPreventaExcluir = (from g in listaPreventaExcluir
                                            group g by new
                                            {
                                                idLider = g.idLider,
                                                idPreventa = g.idPreventa,
                                                Level = g.Level
                                            }
                                   into g
                                            select new
                                            {
                                                IdLider = g.Key.idLider,
                                                Level = g.Key.Level,
                                                IdPreventa = g.Key.idPreventa,
                                                FechaHora = g.Max(d => d.FechaHora)
                                            }).Select(x => new PersonalSeleccionadoDashboard
                                            {
                                                idLider = x.IdLider,
                                                Level = x.Level,
                                                idPreventa = x.IdPreventa,
                                                FechaHora = x.FechaHora
                                            });

                foreach (var itemIncluir in grupoPreventaIncluir)
                {
                    var itemAgregar = grupoPreventaExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.idPreventa == itemIncluir.idPreventa &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaPreventaPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoPreventaExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.idPreventa == itemIncluir.idPreventa &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaPreventaPersonal.Add(itemIncluir);
                        }
                    }
                }

                var listaClientePersonal = new List<PersonalSeleccionadoDashboard>();

                var listaClienteIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaClientesIncluir);
                var grupoClienteIncluir = (from g in listaClienteIncluir
                                           group g by new
                                           {
                                               idLider = g.idLider,
                                               idPreventa = g.idPreventa,
                                               idCliente = g.idCliente,
                                               Level = g.Level
                                           }
                                   into g
                                           select new
                                           {
                                               IdLider = g.Key.idLider,
                                               Level = g.Key.Level,
                                               IdPreventa = g.Key.idPreventa,
                                               IdCliente = g.Key.idCliente,
                                               FechaHora = g.Max(d => d.FechaHora)
                                           }).Select(x => new PersonalSeleccionadoDashboard
                                           {
                                               idLider = x.IdLider,
                                               Level = x.Level,
                                               idPreventa = x.IdPreventa,
                                               idCliente = x.IdCliente,
                                               FechaHora = x.FechaHora
                                           });



                var listaClienteExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaClientesExcluir);
                var grupoClienteExcluir = (from g in listaClienteExcluir
                                           group g by new
                                           {
                                               idLider = g.idLider,
                                               idPreventa = g.idPreventa,
                                               idCliente = g.idCliente,
                                               Level = g.Level
                                           }
                                   into g
                                           select new
                                           {
                                               IdLider = g.Key.idLider,
                                               Level = g.Key.Level,
                                               IdPreventa = g.Key.idPreventa,
                                               IdCliente = g.Key.idCliente,
                                               FechaHora = g.Max(d => d.FechaHora)
                                           }).Select(x => new PersonalSeleccionadoDashboard
                                           {
                                               idLider = x.IdLider,
                                               Level = x.Level,
                                               idPreventa = x.IdPreventa,
                                               idCliente = x.IdCliente,
                                               FechaHora = x.FechaHora
                                           });

                foreach (var itemIncluir in grupoClienteIncluir)
                {
                    var itemAgregar = grupoClienteExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.idPreventa == itemIncluir.idPreventa &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaClientePersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoClienteExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.idPreventa == itemIncluir.idPreventa &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaClientePersonal.Add(itemIncluir);
                        }
                    }
                }

                foreach (var lider in listaLiderPersonal.Where(x => x.Level == "2"))
                {
                    ordenMostraDetalle = Util.Constantes.Generales.Numeric.Uno;

                    var requestLider = new IndicadorDashboardSectorDtoRequest
                    {
                        Anio = request.Anio,
                        IdJefe = lider.idLider
                    };

                    var preventas = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorPreventas(requestLider);

                    if (preventas.Any())
                    {
                        var cabeceraDesplegada = resultadoDashoard.Where(x =>
                                                 x.IdLider == lider.idLider && x.Nivel == 1).FirstOrDefault();
                        if (cabeceraDesplegada != null)
                        {
                            cabeceraDesplegada.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                        }

                        resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                        {
                            IdLider = lider.idLider,
                            Lider = string.Empty,
                            EquipoPreventaLiderCadena = TitulosDetalleOportunidades.Preventa,
                            CarteraClientesLiderCadena = TitulosDetalleOportunidades.CantidadClientes,
                            NroOportunidadesLiderCadena = TitulosDetalleOportunidades.CantidadOportunidades,
                            WinRateLiderCadena = TitulosDetalleOportunidades.WinRateLider,
                            TiempoEntregaPromedioLiderCadena = TitulosDetalleOportunidades.TiempoEntregaPromedioLider,
                            PorcentajeCumplimientoLiderCadena = TitulosDetalleOportunidades.PorcentajeCumplimientoLider,
                            Nivel = 2,
                            Desplegado = Util.Constantes.Generales.Numeric.Cero,
                            WinRateClienteCadena = string.Empty,
                            TiempoEntregaPromedioClienteCadena = string.Empty,
                            PorcentajeCumplimientoClienteCadena = string.Empty,
                            Orden = cabeceraDesplegada.Orden,
                            FechaCierreEstimadaCadena = string.Empty,
                            Payback = string.Empty,
                            PagoUnicoDolaresCadena = string.Empty,
                            RecurrenteDolaresCadena = string.Empty,
                            PeriodoMesesCadena = string.Empty,
                            TipoCambioCadena = string.Empty,
                            OrdenMostrar = ordenMostraDetalle
                        });

                        ordenMostraDetalle++;

                        foreach (var item in preventas)
                        { 
                            resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                            {
                                IdLider = lider.idLider,
                                Lider = string.Empty,
                                IdPreventa = item.IdPreventa,
                                Preventa = string.Empty,
                                Nivel = 2,
                                Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                EquipoPreventaLiderCadena = item.Preventa,
                                CarteraClientesLiderCadena = item.CantidadClientesPreventa.ToString(),
                                NroOportunidadesLiderCadena = item.OportunidadesPreventa.ToString(),
                                WinRateLiderCadena = item.WinRatePreventa.ToString(),
                                TiempoEntregaPromedioLiderCadena = item.TiempoEntregaPromedioPreventa.ToString(),
                                PorcentajeCumplimientoLiderCadena = item.PorcentajeCumplimientoPreventa.ToString(),
                                WinRateClienteCadena = string.Empty,
                                TiempoEntregaPromedioClienteCadena = string.Empty,
                                PorcentajeCumplimientoClienteCadena = string.Empty,
                                Orden = cabeceraDesplegada.Orden,
                                FechaCierreEstimadaCadena = string.Empty,
                                Payback = string.Empty,
                                PagoUnicoDolaresCadena = string.Empty,
                                RecurrenteDolaresCadena = string.Empty,
                                PeriodoMesesCadena = string.Empty,
                                TipoCambioCadena = string.Empty,
                                OrdenMostrar = ordenMostraDetalle
                            });

                            ordenMostraDetalle++;

                            var preventaRestriccion = listaPreventaPersonal.Where(x => x.Level == "3" && x.idLider == lider.idLider &&
                                                                                                x.idPreventa == item.IdPreventa).FirstOrDefault();

                            if (preventaRestriccion != null)
                            {
                                var requestPreventa = new IndicadorDashboardSectorDtoRequest
                                {
                                    Anio = request.Anio,
                                    IdJefe = preventaRestriccion.idPreventa
                                };

                                var clientes = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorClientes(requestPreventa);

                                if (clientes.Any())
                                {
                                    var cabeceraDesplegada2 = resultadoDashoard.Where(x => x.IdLider == lider.idLider && x.IdPreventa == item.IdPreventa
                                                        && x.Nivel == 2).FirstOrDefault();

                                    if (cabeceraDesplegada2 != null)
                                    {
                                        cabeceraDesplegada2.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                                    }

                                    resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                                    {
                                        IdLider = lider.idLider,
                                        Lider = string.Empty,
                                        EquipoPreventaLiderCadena = string.Empty,
                                        CarteraClientesLiderCadena = TitulosDetalleOportunidades.CarteraClientesLider,
                                        NroOportunidadesLiderCadena = TitulosDetalleOportunidades.NroOportunidadesLider,
                                        WinRateLiderCadena = TitulosDetalleOportunidades.TiempoEntregaPromedioLiderCadena,
                                        TiempoEntregaPromedioLiderCadena = TitulosDetalleOportunidades.PorcentajeCumplimientoLiderCadena,
                                        PorcentajeCumplimientoLiderCadena = TitulosDetalleOportunidades.WinRateLider,
                                        Nivel = 3,
                                        Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                        WinRateClienteCadena = TitulosDetalleOportunidades.TiempoEntregaPromedioLider,
                                        TiempoEntregaPromedioClienteCadena = TitulosDetalleOportunidades.PorcentajeCumplimientoLider,
                                        PorcentajeCumplimientoClienteCadena = string.Empty,
                                        Orden = cabeceraDesplegada.Orden,
                                        FechaCierreEstimadaCadena = string.Empty,
                                        Payback = string.Empty,
                                        PagoUnicoDolaresCadena = string.Empty,
                                        RecurrenteDolaresCadena = string.Empty,
                                        PeriodoMesesCadena = string.Empty,
                                        TipoCambioCadena = string.Empty,
                                        OrdenMostrar = ordenMostraDetalle
                                    });

                                    ordenMostraDetalle++;

                                    foreach(var item2 in clientes)
                                    {
                                        resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                                        {
                                            IdLider = lider.idLider,
                                            Lider = string.Empty,
                                            IdPreventa = item.IdPreventa,
                                            Preventa = string.Empty,
                                            Nivel = 3,
                                            Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                            EquipoPreventaLiderCadena = string.Empty,
                                            IdCliente = item2.IdCliente,
                                            CarteraClientesLiderCadena = item2.Cliente,
                                            NroOportunidadesLiderCadena = item2.OportunidadesAbiertasCliente.ToString(),
                                            WinRateLiderCadena = item2.OfertasTrabajadasCliente.ToString(),
                                            TiempoEntregaPromedioLiderCadena = item2.OfertasGanadasCliente.ToString(),
                                            PorcentajeCumplimientoLiderCadena = item2.WinRateCliente.ToString(),
                                            WinRateClienteCadena = item2.TiempoEntregaPromedioCliente.ToString(),
                                            TiempoEntregaPromedioClienteCadena = item2.PorcentajeCumplimientoCliente.ToString(),
                                            PorcentajeCumplimientoClienteCadena = string.Empty,
                                            Orden = cabeceraDesplegada.Orden,
                                            FechaCierreEstimadaCadena = string.Empty,
                                            Payback = string.Empty,
                                            PagoUnicoDolaresCadena = string.Empty,
                                            RecurrenteDolaresCadena = string.Empty,
                                            PeriodoMesesCadena = string.Empty,
                                            TipoCambioCadena = string.Empty,
                                            OrdenMostrar = ordenMostraDetalle
                                        });


                                        ordenMostraDetalle++;

                                        var clientesRestriccion = listaClientePersonal.Where(x => x.Level == "4" && x.idLider == lider.idLider &&
                                                                                                x.idPreventa == item.IdPreventa && x.idCliente == item2.IdCliente).FirstOrDefault();

                                        if (clientesRestriccion != null)
                                        {
                                            var requestOportunidad = new IndicadorDashboardSectorDtoRequest
                                            {
                                                Anio = request.Anio,
                                                IdJefe = item2.IdCliente.Value
                                            };

                                            var oportunidadesLista =
                                                iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorOportunidades(requestOportunidad);

                                            if (oportunidadesLista.Any())
                                            {
                                                var cabeceraDesplegada3 = resultadoDashoard.Where(x =>
                                                                            x.IdLider == lider.idLider && x.IdPreventa == item.IdPreventa &&
                                                                            x.IdCliente == item2.IdCliente
                                                                            && x.Nivel == 3).FirstOrDefault();
                                                if (cabeceraDesplegada3 != null)
                                                {
                                                    cabeceraDesplegada3.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                                                }


                                                resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                                                {
                                                    IdLider = lider.idLider,
                                                    Lider = string.Empty,
                                                    EquipoPreventaLiderCadena = string.Empty,
                                                    CarteraClientesLiderCadena = string.Empty,
                                                    NroOportunidadesLiderCadena = TitulosDetalleOportunidades.NroOportunidades,
                                                    WinRateLiderCadena = TitulosDetalleOportunidades.GerenteCuenta,
                                                    TiempoEntregaPromedioLiderCadena = TitulosDetalleOportunidades.LineaNegocio,
                                                    PorcentajeCumplimientoLiderCadena = TitulosDetalleOportunidades.DescripcionOportunidad,
                                                    Nivel = 4,
                                                    Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                                    WinRateClienteCadena = TitulosDetalleOportunidades.TipoOportunidad,
                                                    TiempoEntregaPromedioClienteCadena = TitulosDetalleOportunidades.CantidadOfertas,
                                                    PorcentajeCumplimientoClienteCadena = TitulosDetalleOportunidades.Fase,
                                                    FechaCierreEstimadaCadena = TitulosDetalleOportunidades.Exito,
                                                    Orden = cabeceraDesplegada.Orden,
                                                    Payback = TitulosDetalleOportunidades.CierreEstimado,
                                                    PagoUnicoDolaresCadena = string.Empty,
                                                    RecurrenteDolaresCadena = string.Empty,
                                                    PeriodoMesesCadena = string.Empty,
                                                    TipoCambioCadena = string.Empty,
                                                    OrdenMostrar = ordenMostraDetalle
                                                });


                                                ordenMostraDetalle++;

                                                foreach (var item3 in oportunidadesLista)
                                                {
                                                    resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                                                    {
                                                        IdLider = item2.IdLider,
                                                        Lider = string.Empty,
                                                        IdPreventa = item2.IdPreventa,
                                                        Preventa = string.Empty,
                                                        Nivel = 4,
                                                        Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                                        EquipoPreventaLiderCadena = string.Empty,
                                                        IdCliente = item2.IdCliente,
                                                        CarteraClientesLiderCadena = string.Empty,
                                                        IdOportunidad = item3.IdOportunidad.ToString(),
                                                        NroOportunidadesLiderCadena = item3.IdOportunidad.ToString(),
                                                        WinRateLiderCadena = item3.PropietarioOportunidad,
                                                        TiempoEntregaPromedioLiderCadena = item3.LineaNegocio,
                                                        PorcentajeCumplimientoLiderCadena = item3.NombreOportunidad,
                                                        WinRateClienteCadena = item3.TipologiaOportunidad,
                                                        TiempoEntregaPromedioClienteCadena = item3.CantidadOfertas.ToString(),
                                                        PorcentajeCumplimientoClienteCadena = item3.FaseOportunidad,
                                                        FechaCierreEstimadaCadena = item3.ProbabilidadExito,
                                                        Orden = cabeceraDesplegada.Orden,
                                                        Payback = Util.Funciones.Funciones.FormatoFechaIndicadores(item3.FechaCierreEstimada),
                                                        PagoUnicoDolaresCadena = string.Empty,
                                                        RecurrenteDolaresCadena = string.Empty,
                                                        PeriodoMesesCadena = string.Empty,
                                                        TipoCambioCadena = string.Empty,
                                                        OrdenMostrar = ordenMostraDetalle
                                                    });

                                                    ordenMostraDetalle++;

                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }


                return new IndicadorDashboardPreventaConsolidadoDtoResponse
                {
                    ListaOportunidades = resultadoDashoard.OrderBy(x => x.Orden).ThenBy(x => x.OrdenMostrar).ToList(),
                    Total = listaOportunidades.Count() > Util.Constantes.Generales.Numeric.Cero ?
                    listaOportunidades.ToList()[0].TotalRow.Value : Util.Constantes.Generales.Numeric.Cero
                };
            }

            #endregion Nivel0

            return null;
        }

        public IndicadorDashboardPreventaConsolidadoDtoResponse ListarSeguimientoOportunidadesPreventaLider(IndicadorDashboardPreventaDtoRequest request)
        {
            var ordenMostraDetalle = Util.Constantes.Generales.Numeric.Cero;

            var resultadoDashoard = new List<IndicadorDashboardPreventaDtoResponse>();
            var listaOportunidades = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesPreventaLider(request);

            foreach (var elemento in listaOportunidades)
            {
                resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                {
                    IdPreventa = elemento.IdPreventa,
                    Preventa = elemento.Preventa,
                    Nivel = Util.Constantes.Generales.Numeric.Uno,
                    Desplegado = Util.Constantes.Generales.Numeric.Cero,
                    CantidadClientesPreventaCadena = elemento.CantidadClientesPreventa.ToString(),
                    OportunidadesPreventaCadena = elemento.OportunidadesPreventa.ToString(),
                    WinRatePreventaCadena = elemento.WinRatePreventa.ToString(),
                    TiempoEntregaPromedioPreventaCadena = elemento.TiempoEntregaPromedioPreventa.ToString(),
                    PorcentajeCumplimientoPreventaCadena = elemento.PorcentajeCumplimientoPreventa.ToString(),
                    IspPreventaCadena = string.Empty,
                    Orden = elemento.Orden,
                    OrdenMostrar = Util.Constantes.Generales.Numeric.Cero,
                    TiempoEntregaPromedioClienteCadena = string.Empty,
                    PorcentajeCumplimientoClienteCadena = string.Empty,
                    IspClienteCadena = string.Empty,
                    FechaCierreEstimadaCadena = string.Empty
                });
            }


            if (request.Nivel == 1)
            {
                return new IndicadorDashboardPreventaConsolidadoDtoResponse
                {
                    ListaOportunidades = resultadoDashoard.OrderBy(w => w.Orden).ToList(),
                    Total = listaOportunidades.Count() > Util.Constantes.Generales.Numeric.Cero ? 
                    listaOportunidades.ToList()[0].TotalRow.Value : Util.Constantes.Generales.Numeric.Cero
                };
            }

            #region Nivel0

            if (request.Nivel == Util.Constantes.Generales.Numeric.Cero)
            {
                var listaPreventaPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaPreventaIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaPreventaIncluir);
                var grupoPreventaIncluir = (from g in listaPreventaIncluir
                                            group g by new
                                            {
                                                idLider = g.idLider,
                                                idPreventa = g.idPreventa,
                                                Level = g.Level
                                            }
                                   into g
                                            select new
                                            {
                                                IdLider = g.Key.idLider,
                                                Level = g.Key.Level,
                                                IdPreventa = g.Key.idPreventa,
                                                FechaHora = g.Max(d => d.FechaHora)
                                            }).Select(x => new PersonalSeleccionadoDashboard
                                            {
                                                idLider = x.IdLider,
                                                Level = x.Level,
                                                idPreventa = x.IdPreventa,
                                                FechaHora = x.FechaHora
                                            });


                var listaPreventaExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaPreventaExcluir);
                var grupoPreventaExcluir = (from g in listaPreventaExcluir
                                            group g by new
                                            {
                                                idLider = g.idLider,
                                                idPreventa = g.idPreventa,
                                                Level = g.Level
                                            }
                                   into g
                                            select new
                                            {
                                                IdLider = g.Key.idLider,
                                                Level = g.Key.Level,
                                                IdPreventa = g.Key.idPreventa,
                                                FechaHora = g.Max(d => d.FechaHora)
                                            }).Select(x => new PersonalSeleccionadoDashboard
                                            {
                                                idLider = x.IdLider,
                                                Level = x.Level,
                                                idPreventa = x.IdPreventa,
                                                FechaHora = x.FechaHora
                                            });

                foreach (var itemIncluir in grupoPreventaIncluir)
                {
                    var itemAgregar = grupoPreventaExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.idPreventa == itemIncluir.idPreventa &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaPreventaPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoPreventaExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.idPreventa == itemIncluir.idPreventa &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaPreventaPersonal.Add(itemIncluir);
                        }
                    }
                }

                var listaClientePersonal = new List<PersonalSeleccionadoDashboard>();

                var listaClienteIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaClientesIncluir);
                var grupoClienteIncluir = (from g in listaClienteIncluir
                                           group g by new
                                           {
                                               idLider = g.idLider,
                                               idPreventa = g.idPreventa,
                                               idCliente = g.idCliente,
                                               Level = g.Level
                                           }
                                   into g
                                           select new
                                           {
                                               IdLider = g.Key.idLider,
                                               Level = g.Key.Level,
                                               IdPreventa = g.Key.idPreventa,
                                               IdCliente = g.Key.idCliente,
                                               FechaHora = g.Max(d => d.FechaHora)
                                           }).Select(x => new PersonalSeleccionadoDashboard
                                           {
                                               idLider = x.IdLider,
                                               Level = x.Level,
                                               idPreventa = x.IdPreventa,
                                               idCliente = x.IdCliente,
                                               FechaHora = x.FechaHora
                                           });



                var listaClienteExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaClientesExcluir);
                var grupoClienteExcluir = (from g in listaClienteExcluir
                                           group g by new
                                           {
                                               idLider = g.idLider,
                                               idPreventa = g.idPreventa,
                                               idCliente = g.idCliente,
                                               Level = g.Level
                                           }
                                   into g
                                           select new
                                           {
                                               IdLider = g.Key.idLider,
                                               Level = g.Key.Level,
                                               IdPreventa = g.Key.idPreventa,
                                               IdCliente = g.Key.idCliente,
                                               FechaHora = g.Max(d => d.FechaHora)
                                           }).Select(x => new PersonalSeleccionadoDashboard
                                           {
                                               idLider = x.IdLider,
                                               Level = x.Level,
                                               idPreventa = x.IdPreventa,
                                               idCliente = x.IdCliente,
                                               FechaHora = x.FechaHora
                                           });

                foreach (var itemIncluir in grupoClienteIncluir)
                {
                    var itemAgregar = grupoClienteExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.idPreventa == itemIncluir.idPreventa &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaClientePersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoClienteExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.idPreventa == itemIncluir.idPreventa &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaClientePersonal.Add(itemIncluir);
                        }
                    }
                }


                foreach (var preventa in listaPreventaPersonal.Where(x => x.Level == "2"))
                {
                    ordenMostraDetalle = Util.Constantes.Generales.Numeric.Uno;

                    var requestPreventa = new IndicadorDashboardSectorDtoRequest
                    {
                        Anio = request.Anio,
                        IdJefe = preventa.idPreventa
                    };

                    var clientes = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorClientes(requestPreventa);

                    if (clientes.Any())
                    {
                        var cabeceraDesplegada = resultadoDashoard.Where(x => x.IdPreventa == preventa.idPreventa
                                                        && x.Nivel == 1).FirstOrDefault();


                        if (cabeceraDesplegada != null)
                        {
                            cabeceraDesplegada.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                        }


                        resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                        {
                            IdPreventa = preventa.idPreventa,
                            Preventa = string.Empty,
                            Nivel = 2,
                            Desplegado = Util.Constantes.Generales.Numeric.Cero,
                            CantidadClientesPreventaCadena = TitulosDetalleOportunidades.CarteraClientesLider,
                            OportunidadesPreventaCadena = TitulosDetalleOportunidades.NroOportunidadesLider,
                            WinRatePreventaCadena = TitulosDetalleOportunidades.TiempoEntregaPromedioLiderCadena,
                            TiempoEntregaPromedioPreventaCadena = TitulosDetalleOportunidades.PorcentajeCumplimientoLiderCadena,
                            PorcentajeCumplimientoPreventaCadena = TitulosDetalleOportunidades.WinRateLider,
                            IspPreventaCadena = TitulosDetalleOportunidades.TiempoEntregaPromedioLider,
                            TiempoEntregaPromedioClienteCadena = TitulosDetalleOportunidades.PorcentajeCumplimientoLider,
                            PorcentajeCumplimientoClienteCadena = string.Empty,
                            IspClienteCadena = string.Empty,
                            Orden = cabeceraDesplegada.Orden,
                            OrdenMostrar = ordenMostraDetalle,
                            FechaCierreEstimadaCadena = string.Empty
                        });


                        ordenMostraDetalle++;

                        foreach (var item in clientes)
                        {
                            resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                            {
                                IdPreventa = preventa.idPreventa,
                                Preventa = string.Empty,
                                IdCliente = item.IdCliente,
                                Cliente = string.Empty,
                                Nivel = 2,
                                Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                CantidadClientesPreventaCadena = item.Cliente,
                                OportunidadesPreventaCadena = item.OportunidadesAbiertasCliente.ToString(),
                                WinRatePreventaCadena = item.OfertasTrabajadasCliente.ToString(),
                                TiempoEntregaPromedioPreventaCadena = item.OfertasGanadasCliente.ToString(),
                                PorcentajeCumplimientoPreventaCadena = item.WinRateCliente.ToString(),
                                IspPreventaCadena = item.TiempoEntregaPromedioCliente.ToString(),
                                TiempoEntregaPromedioClienteCadena = item.PorcentajeCumplimientoCliente.ToString(),
                                PorcentajeCumplimientoClienteCadena = string.Empty,
                                IspClienteCadena = string.Empty,
                                Orden = cabeceraDesplegada.Orden,
                                OrdenMostrar = ordenMostraDetalle,
                                FechaCierreEstimadaCadena = string.Empty
                            });

                            ordenMostraDetalle++;


                            var clientesRestriccion = listaClientePersonal.Where(x => x.Level == "3" &&
                                                      x.idPreventa == preventa.idPreventa && x.idCliente == item.IdCliente).FirstOrDefault();

                            if (clientesRestriccion != null)
                            {
                                var requestOportunidad = new IndicadorDashboardSectorDtoRequest
                                {
                                    Anio = request.Anio,
                                    IdJefe = item.IdCliente.Value
                                };

                                var oportunidadesLista =
                                               iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorOportunidades(requestOportunidad);

                                if (oportunidadesLista.Any())
                                {
                                    var cabeceraDesplegada2 = resultadoDashoard.Where(x => x.IdPreventa == preventa.idPreventa &&
                                                                            x.IdCliente == item.IdCliente
                                                                            && x.Nivel == 2).FirstOrDefault();
                                    if (cabeceraDesplegada2 != null)
                                    {
                                        cabeceraDesplegada2.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                                    }


                                    resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                                    {
                                        IdPreventa = item.IdPreventa,
                                        Preventa = string.Empty,
                                        IdCliente = item.IdCliente,
                                        Nivel = 3,
                                        Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                        CantidadClientesPreventaCadena = string.Empty,
                                        OportunidadesPreventaCadena = TitulosDetalleOportunidades.NroOportunidades,
                                        WinRatePreventaCadena = TitulosDetalleOportunidades.GerenteCuenta,
                                        TiempoEntregaPromedioPreventaCadena =TitulosDetalleOportunidades.LineaNegocio,
                                        PorcentajeCumplimientoPreventaCadena = TitulosDetalleOportunidades.DescripcionOportunidad,
                                        IspPreventaCadena = TitulosDetalleOportunidades.TipoOportunidad,
                                        TiempoEntregaPromedioClienteCadena = TitulosDetalleOportunidades.CantidadOfertas,
                                        PorcentajeCumplimientoClienteCadena = TitulosDetalleOportunidades.Fase,
                                        IspClienteCadena = TitulosDetalleOportunidades.Exito,
                                        FechaCierreEstimadaCadena = TitulosDetalleOportunidades.CierreEstimado,
                                        Orden = cabeceraDesplegada.Orden,
                                        OrdenMostrar = ordenMostraDetalle
                                    });


                                    ordenMostraDetalle++;

                                    foreach (var item2 in oportunidadesLista)
                                    {
                                       
                                        resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                                        {
                                            IdPreventa = item2.IdPreventa,
                                            Preventa = string.Empty,
                                            Nivel = 3,
                                            Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                            CantidadClientesPreventaCadena = string.Empty,
                                            OportunidadesPreventaCadena = item2.IdOportunidad,
                                            WinRatePreventaCadena = item2.PropietarioOportunidad,
                                            TiempoEntregaPromedioPreventaCadena = item2.LineaNegocio,
                                            PorcentajeCumplimientoPreventaCadena = item2.NombreOportunidad,
                                            IspPreventaCadena = item2.TipologiaOportunidad,
                                            TiempoEntregaPromedioClienteCadena = item2.CantidadOfertas.ToString(),
                                            PorcentajeCumplimientoClienteCadena = item2.FaseOportunidad,
                                            IspClienteCadena = item2.ProbabilidadExito,
                                            FechaCierreEstimadaCadena =
                                            Util.Funciones.Funciones.FormatoFechaIndicadores(item2.FechaCierreEstimada),
                                            Orden = cabeceraDesplegada.Orden,
                                            OrdenMostrar = ordenMostraDetalle
                                        });

                                        ordenMostraDetalle++;
                                    }
                                }

                            }
                        }
                    }

                }

                return new IndicadorDashboardPreventaConsolidadoDtoResponse
                {
                    ListaOportunidades = resultadoDashoard.OrderBy(x => x.Orden).ThenBy(x => x.OrdenMostrar).ToList(),
                    Total = listaOportunidades.Count() > Util.Constantes.Generales.Numeric.Cero ? 
                    listaOportunidades.ToList()[0].TotalRow.Value : Util.Constantes.Generales.Numeric.Cero
                };

            }

            #endregion Nivel0

            return null;
        }

        public IndicadorDashboardPreventaConsolidadoDtoResponse ListarSeguimientoOportunidadesPreventaPreventa(IndicadorDashboardPreventaDtoRequest request)
        {
            var ordenMostraDetalle = Util.Constantes.Generales.Numeric.Cero;

            var resultadoDashoard = new List<IndicadorDashboardPreventaDtoResponse>();
            var listaOportunidades = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesPreventaPreventa(request);

            foreach (var elemento in listaOportunidades)
            {
                resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                {
                    IdCliente = elemento.IdCliente,
                    Cliente = elemento.Cliente,
                    Nivel = Util.Constantes.Generales.Numeric.Uno,
                    Desplegado = Util.Constantes.Generales.Numeric.Cero,
                    Orden = elemento.Orden,
                    OrdenMostrar = Util.Constantes.Generales.Numeric.Cero,
                    OportunidadesAbiertasClienteCadena = elemento.OportunidadesAbiertasCliente.ToString(),
                    OfertasTrabajadasClienteCadena = elemento.OfertasTrabajadasCliente.ToString(),
                    OfertasGanadasClienteCadena = elemento.OfertasGanadasCliente.ToString(),
                    WinRateClienteCadena = elemento.WinRateCliente.ToString(),
                    TiempoEntregaPromedioClienteCadena = elemento.TiempoEntregaPromedioCliente.ToString(),
                    PorcentajeCumplimientoClienteCadena = elemento.PorcentajeCumplimientoCliente.ToString(),
                    IspClienteCadena = elemento.IspCliente.ToString(),
                    FechaCierreEstimadaCadena = string.Empty,
                    NumeroDelCaso = string.Empty
                });
            }


            if (request.Nivel == Util.Constantes.Generales.Numeric.Uno)
            {
                return new IndicadorDashboardPreventaConsolidadoDtoResponse
                {
                    ListaOportunidades = resultadoDashoard.OrderBy(w => w.Orden).ToList(),
                    Total = listaOportunidades.Count() > Util.Constantes.Generales.Numeric.Cero ? 
                    listaOportunidades.ToList()[0].TotalRow.Value : Util.Constantes.Generales.Numeric.Cero
                };
            }

            #region Nivel0

            if (request.Nivel == Util.Constantes.Generales.Numeric.Cero)
            {

                var listaClientePersonal = new List<PersonalSeleccionadoDashboard>();

                var listaClienteIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaClientesIncluir);
                var grupoClienteIncluir = (from g in listaClienteIncluir
                                           group g by new
                                           {
                                               idCliente = g.idCliente,
                                               Level = g.Level
                                           }
                                   into g
                                           select new
                                           {
                                               Level = g.Key.Level,
                                               IdCliente = g.Key.idCliente,
                                               FechaHora = g.Max(d => d.FechaHora)
                                           }).Select(x => new PersonalSeleccionadoDashboard
                                           {
                                               Level = x.Level,
                                               idCliente = x.IdCliente,
                                               FechaHora = x.FechaHora
                                           });


                var listaClienteExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaClientesExcluir);
                var grupoClienteExcluir = (from g in listaClienteExcluir
                                           group g by new
                                           {
                                               idCliente = g.idCliente,
                                               Level = g.Level
                                           }
                                   into g
                                           select new
                                           {
                                               Level = g.Key.Level,
                                               IdCliente = g.Key.idCliente,
                                               FechaHora = g.Max(d => d.FechaHora)
                                           }).Select(x => new PersonalSeleccionadoDashboard
                                           {
                                               idCliente = x.IdCliente,
                                               FechaHora = x.FechaHora,
                                               Level = x.Level
                                           });

                foreach (var itemIncluir in grupoClienteIncluir)
                {
                    var itemAgregar = grupoClienteExcluir.Where(x =>  x.idCliente == itemIncluir.idCliente &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaClientePersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoClienteExcluir.Where(x => x.Level == itemIncluir.Level &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaClientePersonal.Add(itemIncluir);
                        }
                    }
                }

                foreach (var cliente in listaClientePersonal.Where(x => x.Level == "2"))
                {
                    ordenMostraDetalle = Util.Constantes.Generales.Numeric.Uno;

                    var requestOportunidad = new IndicadorDashboardSectorDtoRequest
                    {
                        Anio = request.Anio,
                        IdJefe = cliente.idCliente
                    };

                    var oportunidadesLista = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorOportunidades(requestOportunidad);

                    if (oportunidadesLista.Any())
                    {
                        var cabeceraDesplegada = resultadoDashoard.Where(x =>
                                                x.IdCliente == cliente.idCliente && x.Nivel == 1).FirstOrDefault();

                        if (cabeceraDesplegada != null)
                        {
                            cabeceraDesplegada.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                        }

                        resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                        {
                            IdCliente = cliente.idCliente,
                            Nivel = 2,
                            Desplegado = Util.Constantes.Generales.Numeric.Cero,
                            Orden = cabeceraDesplegada.Orden,
                            OrdenMostrar = ordenMostraDetalle,
                            Cliente = string.Empty,
                            OportunidadesAbiertasClienteCadena = TitulosDetalleOportunidades.NroOportunidades,
                            OfertasTrabajadasClienteCadena = TitulosDetalleOportunidades.GerenteCuenta,
                            OfertasGanadasClienteCadena = TitulosDetalleOportunidades.LineaNegocio,
                            WinRateClienteCadena = TitulosDetalleOportunidades.DescripcionOportunidad,
                            TiempoEntregaPromedioClienteCadena = TitulosDetalleOportunidades.TipoOportunidad,
                            PorcentajeCumplimientoClienteCadena = TitulosDetalleOportunidades.CantidadOfertas,
                            IspClienteCadena = TitulosDetalleOportunidades.Fase,
                            FechaCierreEstimadaCadena = TitulosDetalleOportunidades.Exito,
                            NumeroDelCaso = TitulosDetalleOportunidades.CierreEstimado
                        });

                        ordenMostraDetalle++;


                        foreach (var item2 in oportunidadesLista)
                        {
                            resultadoDashoard.Add(new IndicadorDashboardPreventaDtoResponse
                            {
                                IdCliente = cliente.idCliente,
                                Cliente = string.Empty,
                                Nivel = 2,
                                Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                Orden = cabeceraDesplegada.Orden,
                                OrdenMostrar = ordenMostraDetalle,
                                OportunidadesAbiertasClienteCadena = item2.IdOportunidad.ToString(),
                                OfertasTrabajadasClienteCadena = item2.PropietarioOportunidad,
                                OfertasGanadasClienteCadena = item2.LineaNegocio,
                                WinRateClienteCadena = item2.NombreOportunidad,
                                TiempoEntregaPromedioClienteCadena = item2.TipologiaOportunidad.ToString(),
                                PorcentajeCumplimientoClienteCadena = item2.CantidadOfertas.ToString(),
                                IspClienteCadena = item2.FaseOportunidad,
                                FechaCierreEstimadaCadena = item2.ProbabilidadExito,
                                NumeroDelCaso = Util.Funciones.Funciones.FormatoFechaIndicadores(item2.FechaCierreEstimada)
                            });

                            ordenMostraDetalle++;
                        }
                    }
                }

                return new IndicadorDashboardPreventaConsolidadoDtoResponse
                {
                    ListaOportunidades = resultadoDashoard.OrderBy(x => x.Orden).ThenBy(x => x.OrdenMostrar).ToList(),
                    Total = listaOportunidades.Count() > Util.Constantes.Generales.Numeric.Cero ? 
                    listaOportunidades.ToList()[0].TotalRow.Value : Util.Constantes.Generales.Numeric.Cero
                };

            }

            #endregion Nivel0

            return null;
        }

        public IndicadorDashboardPreventaConsolidadoDtoResponse ListarSeguimientoOportunidadesSectorOportunidad(IndicadorDashboardSectorDtoRequest request)
        {
            var ordenMostraDetalle = Util.Constantes.Generales.Numeric.Cero;

            var listaOportunidadesResultado = new List<IndicadorDashboardPreventaDtoResponse>();

            var oportunidades = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorOportunidad(request);

            foreach (var item in oportunidades)
            {
                listaOportunidadesResultado.Add(new IndicadorDashboardPreventaDtoResponse
                {
                    IdSector = item.IdSector,
                    Sector = item.Sector,
                    IdLider = Util.Constantes.Generales.Numeric.Cero,
                    IdPreventa = Util.Constantes.Generales.Numeric.Cero,
                    IdCliente = Util.Constantes.Generales.Numeric.Cero,
                    OportunidadesCadena = item.NroOportunidadesLider.ToString(),
                    Orden = item.Orden,
                    OrdenMostrar = Util.Constantes.Generales.Numeric.Cero,
                    Nivel = Util.Constantes.Generales.Numeric.Uno
                });
            }

            if (request.Nivel == Util.Constantes.Generales.Numeric.Uno)
            {
                return new IndicadorDashboardPreventaConsolidadoDtoResponse
                {
                    ListaOportunidades = listaOportunidadesResultado.OrderBy(x => x.Orden),
                    Total = oportunidades.Count() > Util.Constantes.Generales.Numeric.Cero ?
                    oportunidades.ToList()[0].TotalRow.Value : Util.Constantes.Generales.Numeric.Cero
                };
            }

            if (request.Nivel == Util.Constantes.Generales.Numeric.Cero)
            {
                var listaSectorPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaSectorIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaSectorIncluir);
                var grupoSectorIncluir = (from g in listaSectorIncluir
                                          group g by new
                                          {
                                              idSector = g.idSector,
                                              Level = g.Level
                                          }
                                   into g
                                          select new
                                          {
                                              Id = g.Key.idSector,
                                              Name = g.Key.Level,
                                              FechaHora = g.Max(d => d.FechaHora)
                                          }).Select(x => new PersonalSeleccionadoDashboard
                                          {
                                              idSector = x.Id,
                                              Level = x.Name,
                                              FechaHora = x.FechaHora
                                          });


                var listaSectorExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaSectorExcluir);
                var grupoSectorExcluir = (from g in listaSectorExcluir
                                          group g by new
                                          {
                                              idSector = g.idSector,
                                              Level = g.Level
                                          }
                                  into g
                                          select new
                                          {
                                              Id = g.Key.idSector,
                                              Name = g.Key.Level,
                                              FechaHora = g.Max(d => d.FechaHora)
                                          }).Select(x => new PersonalSeleccionadoDashboard
                                          {
                                              idSector = x.Id,
                                              Level = x.Name,
                                              FechaHora = x.FechaHora
                                          });

                foreach (var itemIncluir in grupoSectorIncluir)
                {
                    var itemAgregar = grupoSectorExcluir.Where(x => x.idSector == itemIncluir.idSector &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaSectorPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoSectorExcluir.Where(x => x.idSector == itemIncluir.idSector &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaSectorPersonal.Add(itemIncluir);
                        }
                    }
                }

                var listaLiderPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaLiderIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaLiderIncluir);
                var grupoLiderIncluir = (from g in listaLiderIncluir
                                         group g by new
                                         {
                                             idSector = g.idSector,
                                             idLider = g.idLider,
                                             Level = g.Level
                                         }
                                   into g
                                         select new
                                         {
                                             idLider = g.Key.idLider,
                                             idSector = g.Key.idSector,
                                             Nivel = g.Key.Level,
                                             FechaHora = g.Max(d => d.FechaHora)
                                         }).Select(x => new PersonalSeleccionadoDashboard
                                         {
                                             idSector = x.idSector,
                                             idLider = x.idLider,
                                             Level = x.Nivel,
                                             FechaHora = x.FechaHora
                                         });


                var listaLiderExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaLiderExcluir);
                var grupoLiderExcluir = (from g in listaLiderExcluir
                                         group g by new
                                         {
                                             idSector = g.idSector,
                                             idLider = g.idLider,
                                             Level = g.Level
                                         }
                                  into g
                                         select new
                                         {
                                             idLider = g.Key.idLider,
                                             idSector = g.Key.idSector,
                                             Nivel = g.Key.Level,
                                             FechaHora = g.Max(d => d.FechaHora)
                                         }).Select(x => new PersonalSeleccionadoDashboard
                                         {
                                             idSector = x.idSector,
                                             idLider = x.idLider,
                                             Level = x.Nivel,
                                             FechaHora = x.FechaHora
                                         });

                foreach (var itemIncluir in grupoLiderIncluir)
                {
                    var itemAgregar = grupoLiderExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.idSector == itemIncluir.idSector &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaLiderPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoLiderExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.idSector == itemIncluir.idSector &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaLiderPersonal.Add(itemIncluir);
                        }
                    }
                }

                var listaPreventaPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaPreventaIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaPreventaIncluir);
                var grupoPreventaIncluir = (from g in listaPreventaIncluir
                                            group g by new
                                            {
                                                idSector = g.idSector,
                                                idLider = g.idLider,
                                                idPreventa = g.idPreventa,
                                                Level = g.Level
                                            }
                                   into g
                                            select new
                                            {
                                                IdSector = g.Key.idSector,
                                                IdLider = g.Key.idLider,
                                                Level = g.Key.Level,
                                                IdPreventa = g.Key.idPreventa,
                                                FechaHora = g.Max(d => d.FechaHora)
                                            }).Select(x => new PersonalSeleccionadoDashboard
                                            {
                                                idSector = x.IdSector,
                                                idLider = x.IdLider,
                                                Level = x.Level,
                                                idPreventa = x.IdPreventa,
                                                FechaHora = x.FechaHora
                                            });


                var listaPreventaExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaPreventaExcluir);
                var grupoPreventaExcluir = (from g in listaPreventaExcluir
                                            group g by new
                                            {
                                                idSector = g.idSector,
                                                idLider = g.idLider,
                                                idPreventa = g.idPreventa,
                                                Level = g.Level
                                            }
                                   into g
                                            select new
                                            {
                                                IdSector = g.Key.idSector,
                                                IdLider = g.Key.idLider,
                                                Level = g.Key.Level,
                                                IdPreventa = g.Key.idPreventa,
                                                FechaHora = g.Max(d => d.FechaHora)
                                            }).Select(x => new PersonalSeleccionadoDashboard
                                            {
                                                idSector = x.IdSector,
                                                idLider = x.IdLider,
                                                Level = x.Level,
                                                idPreventa = x.IdPreventa,
                                                FechaHora = x.FechaHora
                                            });

                foreach (var itemIncluir in grupoPreventaIncluir)
                {
                    var itemAgregar = grupoPreventaExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.idSector == itemIncluir.idSector &&
                                                                      x.idPreventa == itemIncluir.idPreventa &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaPreventaPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoPreventaExcluir.Where(x => x.idLider == itemIncluir.idLider &&
                                                                      x.idSector == itemIncluir.idSector &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.idPreventa == itemIncluir.idPreventa &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaPreventaPersonal.Add(itemIncluir);
                        }
                    }
                }

                var listaClientePersonal = new List<PersonalSeleccionadoDashboard>();

                var listaClienteIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaClientesIncluir);
                var grupoClienteIncluir = (from g in listaClienteIncluir
                                           group g by new
                                           {
                                               idSector = g.idSector,
                                               idLider = g.idLider,
                                               idPreventa = g.idPreventa,
                                               idCliente = g.idCliente,
                                               Level = g.Level
                                           }
                                   into g
                                           select new
                                           {
                                               idSector = g.Key.idSector,
                                               idLider = g.Key.idLider,
                                               idPreventa = g.Key.idPreventa,
                                               IdCliente = g.Key.idCliente,
                                               Level = g.Key.Level,
                                               FechaHora = g.Max(d => d.FechaHora)
                                           }).Select(x => new PersonalSeleccionadoDashboard
                                           {
                                               idSector = x.idSector,
                                               idLider = x.idLider,
                                               idPreventa = x.idPreventa,
                                               idCliente = x.IdCliente,
                                               Level = x.Level,
                                               FechaHora = x.FechaHora
                                           });


                var listaClienteExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(request.ListaClientesExcluir);
                var grupoClienteExcluir = (from g in listaClienteExcluir
                                           group g by new
                                           {
                                               idSector = g.idSector,
                                               idLider = g.idLider,
                                               idPreventa = g.idPreventa,
                                               idCliente = g.idCliente,
                                               Level = g.Level
                                           }
                                   into g
                                           select new
                                           {
                                               idSector = g.Key.idSector,
                                               idLider = g.Key.idLider,
                                               idPreventa = g.Key.idPreventa,
                                               IdCliente = g.Key.idCliente,
                                               Level = g.Key.Level,
                                               FechaHora = g.Max(d => d.FechaHora)
                                           }).Select(x => new PersonalSeleccionadoDashboard
                                           {
                                               idSector = x.idSector,
                                               idLider = x.idLider,
                                               idPreventa = x.idPreventa,
                                               idCliente = x.IdCliente,
                                               FechaHora = x.FechaHora,
                                               Level = x.Level
                                           });

                foreach (var itemIncluir in grupoClienteIncluir)
                {
                    var itemAgregar = grupoClienteExcluir.Where(x => x.idSector == itemIncluir.idSector &&
                                                                      x.idLider == itemIncluir.idLider &&
                                                                      x.idPreventa == itemIncluir.idPreventa &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaClientePersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoClienteExcluir.Where(x => x.Level == itemIncluir.Level &&
                                                                      x.idLider == itemIncluir.idLider &&
                                                                      x.idPreventa == itemIncluir.idPreventa &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaClientePersonal.Add(itemIncluir);
                        }
                    }
                }

                foreach (var sector in listaSectorPersonal.Where(x => x.Level == "2"))
                {
                    ordenMostraDetalle = Util.Constantes.Generales.Numeric.Uno;

                    var requestLider = new IndicadorDashboardSectorDtoRequest
                    {
                        Anio = request.Anio,
                        IdJefe = request.IdJefe,
                        IdSector = sector.idSector
                    };

                    var lideres = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorLideres(requestLider).ToList();


                    if (lideres.Any())
                    {
                        var cabeceraDesplegada = listaOportunidadesResultado.Where(x =>
                                                 x.IdSector == sector.idSector && x.Nivel == 1).FirstOrDefault();
                        if (cabeceraDesplegada != null)
                        {
                            cabeceraDesplegada.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                        }

                        listaOportunidadesResultado.Add(new IndicadorDashboardPreventaDtoResponse
                        {
                            IdSector = sector.idSector,
                            Sector = string.Empty,
                            IdLider = Util.Constantes.Generales.Numeric.Cero,
                            IdPreventa = Util.Constantes.Generales.Numeric.Cero,
                            IdCliente = Util.Constantes.Generales.Numeric.Cero,
                            OportunidadesCadena = TitulosDetalleOportunidades.Lider,
                            EquipoPreventaCadena = TitulosDetalleOportunidades.EquipoPreventa,
                            CarteraClientesCadena = TitulosDetalleOportunidades.CarteraClientes,
                            NroOportunidadesLiderCadena = TitulosDetalleOportunidades.Oportunidades,
                            WinRateLiderCadena = TitulosDetalleOportunidades.WinRateLider,
                            TiempoEntregaOfertaCadena = TitulosDetalleOportunidades.TiempoEntregaPromedioLider,
                            PorcentajeCumplimientoEntregaOfertaCadena = TitulosDetalleOportunidades.PorcentajeCumplimientoLider,
                            Nivel = NivelesDashboardReportes.Nivel2,
                            Desplegado = Util.Constantes.Generales.Numeric.Cero,
                            Orden = cabeceraDesplegada.Orden,
                            OrdenMostrar = ordenMostraDetalle
                        });

                        ordenMostraDetalle++;

                        foreach (var lider in lideres)
                        {
                            listaOportunidadesResultado.Add(new IndicadorDashboardPreventaDtoResponse
                            {
                                IdSector = sector.idSector,
                                IdLider = lider.IdLider,
                                IdPreventa = Util.Constantes.Generales.Numeric.Cero,
                                IdCliente = Util.Constantes.Generales.Numeric.Cero,
                                Nivel = NivelesDashboardReportes.Nivel2,
                                Sector = string.Empty,
                                Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                OportunidadesCadena = lider.Lider,
                                EquipoPreventaCadena = lider.EquipoPreventaLider.ToString(),
                                CarteraClientesCadena = lider.CarteraClientesLider.ToString(),
                                NroOportunidadesLiderCadena = lider.NroOportunidadesLider.ToString(),
                                WinRateLiderCadena = lider.WinRateLider.ToString(),
                                TiempoEntregaOfertaCadena = lider.TiempoEntregaPromedioLider.ToString(),
                                PorcentajeCumplimientoEntregaOfertaCadena = lider.PorcentajeCumplimientoLider.ToString(),
                                Orden = cabeceraDesplegada.Orden,
                                OrdenMostrar = ordenMostraDetalle
                            });

                            ordenMostraDetalle++;

                            var lideresRestriccion = listaLiderPersonal.Where(x => x.Level == "3" && x.idSector == sector.idSector && 
                                                                                                     x.idLider == lider.IdLider).FirstOrDefault();

                            if (lideresRestriccion != null)
                            {
                                var requestPreventa = new IndicadorDashboardSectorDtoRequest
                                {
                                    Anio = request.Anio,
                                    IdJefe = lideresRestriccion.idLider
                                };

                                var preventas = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorPreventas(requestPreventa);

                                if (preventas.Any())
                                {
                                    var cabeceraDesplegada1 = listaOportunidadesResultado.Where(x =>
                                                x.IdSector == sector.idSector && x.IdLider == lider.IdLider && x.Nivel == 2).FirstOrDefault();
                                    if (cabeceraDesplegada1 != null)
                                    {
                                        cabeceraDesplegada1.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                                    }

                                    listaOportunidadesResultado.Add(new IndicadorDashboardPreventaDtoResponse
                                    {
                                        IdSector = sector.idSector,
                                        IdLider = lider.IdLider,
                                        IdPreventa = Util.Constantes.Generales.Numeric.Cero,
                                        Nivel = NivelesDashboardReportes.Nivel3,
                                        Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                        Sector = string.Empty,
                                        OportunidadesCadena = string.Empty,
                                        EquipoPreventaCadena = TitulosDetalleOportunidades.Preventa,
                                        CarteraClientesCadena = TitulosDetalleOportunidades.CantidadClientes,
                                        NroOportunidadesLiderCadena = TitulosDetalleOportunidades.CantidadOportunidades,
                                        WinRateLiderCadena = TitulosDetalleOportunidades.WinRateLider,
                                        TiempoEntregaOfertaCadena = TitulosDetalleOportunidades.TiempoEntregaPromedioLider,
                                        PorcentajeCumplimientoEntregaOfertaCadena = TitulosDetalleOportunidades.PorcentajeCumplimientoLider,
                                        Orden = cabeceraDesplegada.Orden,
                                        OrdenMostrar = ordenMostraDetalle
                                    });

                                    ordenMostraDetalle++;

                                    foreach (var item in preventas)
                                    {
                                        listaOportunidadesResultado.Add(new IndicadorDashboardPreventaDtoResponse
                                        {
                                            IdSector = sector.idSector,
                                            IdLider = lider.IdLider,
                                            IdPreventa = item.IdPreventa,
                                            IdCliente = Util.Constantes.Generales.Numeric.Cero,
                                            Nivel = NivelesDashboardReportes.Nivel3,
                                            Sector = string.Empty,
                                            OportunidadesCadena = string.Empty,
                                            Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                            Orden = cabeceraDesplegada.Orden,
                                            OrdenMostrar = ordenMostraDetalle,
                                            EquipoPreventaCadena = item.Preventa,
                                            CarteraClientesCadena = item.CantidadClientesPreventa.ToString(),
                                            NroOportunidadesLiderCadena = item.OportunidadesPreventa.ToString(),
                                            WinRateLiderCadena = item.WinRatePreventa.ToString(),
                                            TiempoEntregaOfertaCadena = item.TiempoEntregaPromedioPreventa.ToString(),
                                            PorcentajeCumplimientoEntregaOfertaCadena = item.PorcentajeCumplimientoPreventa.ToString()
                                        });

                                        ordenMostraDetalle++;

                                        var preventasRestriccion = listaPreventaPersonal.Where(x => x.Level == "4" && x.idLider == lider.IdLider &&
                                                                                                                      x.idSector == sector.idSector &&
                                                                                               x.idPreventa == item.IdPreventa).FirstOrDefault();

                                        if (preventasRestriccion != null)
                                        {
                                            var requestClientes = new IndicadorDashboardSectorDtoRequest
                                            {
                                                Anio = request.Anio,
                                                IdJefe = item.IdPreventa.Value
                                            };


                                            var clientes = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorClientes(requestClientes);

                                            if (clientes.Any())
                                            {
                                                var cabeceraDesplegada2 = listaOportunidadesResultado.Where(x =>
                                                            x.IdSector == sector.idSector && x.IdLider == lider.IdLider && x.IdPreventa == item.IdPreventa
                                                            && x.Nivel == 3).FirstOrDefault();
                                                if (cabeceraDesplegada2 != null)
                                                {
                                                    cabeceraDesplegada2.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                                                }

                                                listaOportunidadesResultado.Add(new IndicadorDashboardPreventaDtoResponse
                                                {
                                                    IdSector = sector.idSector,
                                                    IdLider = lider.IdLider,
                                                    IdPreventa = item.IdPreventa,
                                                    Nivel = NivelesDashboardReportes.Nivel4,
                                                    Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                                    Orden = cabeceraDesplegada.Orden,
                                                    OrdenMostrar = ordenMostraDetalle,
                                                    Sector = string.Empty,
                                                    OportunidadesCadena = string.Empty,
                                                    EquipoPreventaCadena = string.Empty,
                                                    CarteraClientesCadena = TitulosDetalleOportunidades.CarteraClientesLider,
                                                    NroOportunidadesLiderCadena = TitulosDetalleOportunidades.NroOportunidadesLider,
                                                    WinRateLiderCadena = TitulosDetalleOportunidades.TiempoEntregaPromedioLiderCadena,
                                                    TiempoEntregaOfertaCadena = TitulosDetalleOportunidades.PorcentajeCumplimientoLiderCadena,
                                                    PorcentajeCumplimientoEntregaOfertaCadena = TitulosDetalleOportunidades.WinRateLider,
                                                    WinRateClienteCadena = TitulosDetalleOportunidades.TiempoEntregaPromedioLider,
                                                    TiempoEntregaPromedioClienteCadena = TitulosDetalleOportunidades.PorcentajeCumplimientoLider,
                                                    PorcentajeCumplimientoClienteCadena = string.Empty
                                                });

                                                ordenMostraDetalle++;

                                                foreach (var item2 in clientes)
                                                {
                                                    listaOportunidadesResultado.Add(new IndicadorDashboardPreventaDtoResponse
                                                    {
                                                        IdSector = sector.idSector,
                                                        IdLider = lider.IdLider,
                                                        IdPreventa = item.IdPreventa,
                                                        IdCliente = item2.IdCliente,
                                                        Nivel = NivelesDashboardReportes.Nivel4,
                                                        Sector = string.Empty,
                                                        OportunidadesCadena = string.Empty,
                                                        Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                                        Orden = cabeceraDesplegada.Orden,
                                                        OrdenMostrar = ordenMostraDetalle,
                                                        EquipoPreventaCadena = string.Empty,
                                                        CarteraClientesCadena = item2.Cliente.ToString(),
                                                        NroOportunidadesLiderCadena = item2.OportunidadesAbiertasCliente.ToString(),
                                                        WinRateLiderCadena = item2.OfertasTrabajadasCliente.ToString(),
                                                        TiempoEntregaOfertaCadena = item2.OfertasGanadasCliente.ToString(),
                                                        PorcentajeCumplimientoEntregaOfertaCadena = item2.WinRateCliente.ToString(),
                                                        WinRateClienteCadena = item2.TiempoEntregaPromedioCliente.ToString(),
                                                        TiempoEntregaPromedioClienteCadena = item2.PorcentajeCumplimientoCliente.ToString(),
                                                        PorcentajeCumplimientoClienteCadena = string.Empty
                                                    });

                                                    ordenMostraDetalle++;

                                                    var clientesRestriccion = listaClientePersonal.Where(x => x.Level == "5" &&
                                                                x.idSector == sector.idSector &&
                                                                x.idLider == lider.IdLider &&
                                                                x.idPreventa == item.IdPreventa && x.idCliente == item2.IdCliente).FirstOrDefault();

                                                    if (clientesRestriccion != null)
                                                    {
                                                        var requestOportunidad = new IndicadorDashboardSectorDtoRequest
                                                        {
                                                            Anio = request.Anio,
                                                            IdJefe = item2.IdCliente.Value
                                                        };

                                                        var oportunidadesLista =
                                                        iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorOportunidades(requestOportunidad);

                                                        if (oportunidadesLista.Any())
                                                        {
                                                            var cabeceraDesplegada3 = listaOportunidadesResultado.Where(x =>
                                                            x.IdSector == sector.idSector && x.IdLider == lider.IdLider && x.IdPreventa == item.IdPreventa &&
                                                            x.IdCliente == item2.IdCliente
                                                            && x.Nivel == 4).FirstOrDefault();
                                                            if (cabeceraDesplegada3 != null)
                                                            {
                                                                cabeceraDesplegada3.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                                                            }

                                                            listaOportunidadesResultado.Add(new IndicadorDashboardPreventaDtoResponse
                                                            {
                                                                IdSector = sector.idSector,
                                                                IdLider = lider.IdLider,
                                                                IdPreventa = item.IdPreventa,
                                                                IdCliente = item2.IdCliente,
                                                                Nivel = NivelesDashboardReportes.Nivel5,
                                                                Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                                                Orden = cabeceraDesplegada.Orden,
                                                                OrdenMostrar = ordenMostraDetalle,
                                                                Sector = string.Empty,
                                                                OportunidadesCadena = string.Empty,
                                                                EquipoPreventaCadena = string.Empty,
                                                                CarteraClientesCadena = string.Empty,
                                                                NroOportunidadesLiderCadena = TitulosDetalleOportunidades.NroOportunidades,
                                                                WinRateLiderCadena = TitulosDetalleOportunidades.GerenteCuenta,
                                                                TiempoEntregaOfertaCadena = TitulosDetalleOportunidades.LineaNegocio,
                                                                PorcentajeCumplimientoEntregaOfertaCadena = TitulosDetalleOportunidades.DescripcionOportunidad,
                                                                WinRateClienteCadena = TitulosDetalleOportunidades.TipoOportunidad,
                                                                TiempoEntregaPromedioClienteCadena = TitulosDetalleOportunidades.CantidadOfertas,
                                                                PorcentajeCumplimientoClienteCadena = TitulosDetalleOportunidades.Fase,
                                                                FaseOportunidad = TitulosDetalleOportunidades.Exito,
                                                                ProbabilidadExito = TitulosDetalleOportunidades.CierreEstimado
                                                            });

                                                            ordenMostraDetalle++;

                                                            foreach (var item3 in oportunidadesLista)
                                                            {
                                                                listaOportunidadesResultado.Add(new IndicadorDashboardPreventaDtoResponse
                                                                {
                                                                    IdSector = sector.idSector,
                                                                    IdLider = lider.IdLider,
                                                                    IdPreventa = item.IdPreventa,
                                                                    IdCliente = item2.IdCliente,
                                                                    IdOportunidad = item3.IdOportunidad,
                                                                    Nivel = NivelesDashboardReportes.Nivel5,
                                                                    Sector = string.Empty,
                                                                    OportunidadesCadena = string.Empty,
                                                                    Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                                                    Orden = cabeceraDesplegada.Orden,
                                                                    OrdenMostrar = ordenMostraDetalle,
                                                                    EquipoPreventaCadena = string.Empty,
                                                                    CarteraClientesCadena = string.Empty,
                                                                    NroOportunidadesLiderCadena = item3.IdOportunidad.ToString(),
                                                                    WinRateLiderCadena = item3.PropietarioOportunidad,
                                                                    TiempoEntregaOfertaCadena = item3.LineaNegocio,
                                                                    PorcentajeCumplimientoEntregaOfertaCadena = item3.NombreOportunidad.ToString(),
                                                                    WinRateClienteCadena = item3.TipologiaOportunidad.ToString(),
                                                                    TiempoEntregaPromedioClienteCadena = item3.CantidadOfertas.ToString(),
                                                                    PorcentajeCumplimientoClienteCadena = item3.FaseOportunidad.ToString(),
                                                                    FaseOportunidad = item3.ProbabilidadExito,
                                                                    ProbabilidadExito = Util.Funciones.Funciones.FormatoFechaIndicadores(item3.FechaCierreEstimada)
                                                                });

                                                                ordenMostraDetalle++;

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return new IndicadorDashboardPreventaConsolidadoDtoResponse
                {
                    ListaOportunidades = listaOportunidadesResultado.OrderBy(x => x.Orden).ThenBy(x => x.OrdenMostrar),
                    Total = oportunidades.Count() > Util.Constantes.Generales.Numeric.Cero ?
                   oportunidades.ToList()[0].TotalRow.Value : Util.Constantes.Generales.Numeric.Cero
                };

            }
            return null;

        }

        public IndicadorRentabilidadConsolidadoDtoResponse ListarRentabilidadAnalistasFinancieros(IndicadorRentabilidadDtoRequest filtro)
        {
            var recurso = iRecursoDal.ListarRecursoDeUsuario(new DataContracts.Dto.Request.Comun.RecursoDtoRequest {
                IdUsuarioRais = filtro.IdGerente.Value
            });

            if (recurso.IdRecurso != 0)
            {
                filtro.IdGerente = recurso.IdRecurso;
            }

            var analistas = iOportunidadFinancieraOrigenDal.ListarRentabilidadAnalistasFinancieros(filtro).ToList();

            var resultado = new List<IndicadorRentabilidadDtoResponse>();

            foreach (var item in analistas)
            {
                resultado.Add(new IndicadorRentabilidadDtoResponse
                {
                    IdAnalistaFinanciero = item.IdAnalistaFinanciero,
                    AnalistaFinanciero = item.AnalistaFinanciero,
                    CarteraClientesLiderCadena = item.CarteraClientesLider.ToString(),
                    OportunidadesCadena = item.OportunidadesTrabajadas.ToString(),
                    TiempoEntregaPromedioCadena = item.TiempoEntregaPromedio.ToString(),
                    PorcentajeCumplimientoCadena = item.PorcentajeCumplimiento.ToString(),
                    TiempoEntregaPromedioClienteCadena = string.Empty,
                    CumplimientoEntregaClienteCadena = string.Empty,
                    CantidadOfertasClientes = string.Empty,
                    FaseOportunidad = string.Empty,
                    ProbabilidadExitoCadena = string.Empty,
                    FechaCierreEstimadaCadena = string.Empty,
                    OibdaCadena = string.Empty,
                    FlujoCajaCadena = string.Empty,
                    FullContractValueNetoCadena = string.Empty,
                    PayBack = string.Empty,
                    PagoUnicoDolares = string.Empty,
                    PagoRecurrenteDolares = string.Empty,
                    MesesPagoRecurrenteCadena = string.Empty,
                    TipoCambioCadena = string.Empty,
                    Desplegado = Util.Constantes.Generales.Numeric.Cero,
                    Orden = item.Orden,
                    OrdenMostrar = Util.Constantes.Generales.Numeric.Cero,
                    Nivel = Util.Constantes.Generales.Numeric.Uno
                });
            }


            if (filtro.Nivel.Equals(Util.Constantes.Generales.Numeric.Uno))
            {
                return new IndicadorRentabilidadConsolidadoDtoResponse
                {
                    ListadoOportunidades = resultado.OrderBy(x => x.Orden).ToList(),
                    Total = analistas.Any() ? analistas[0].TotalRow.Value : Util.Constantes.Generales.Numeric.Cero

                };
            }
            else
            {
                var listaAnalistaPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaAnalistaFinancierosIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaAnalistaFinancierosIncluir);
                var grupoAnalistaFinancierosIncluir = (from g in listaAnalistaFinancierosIncluir
                                          group g by new
                                          {
                                              idAnalista = g.idAnalista,
                                              Level = g.Level
                                          }
                                   into g
                                          select new
                                          {
                                              Id = g.Key.idAnalista,
                                              Name = g.Key.Level,
                                              FechaHora = g.Max(d => d.FechaHora)
                                          }).Select(x => new PersonalSeleccionadoDashboard
                                          {
                                              idAnalista = x.Id,
                                              Level = x.Name,
                                              FechaHora = x.FechaHora
                                          });


                var listaAnalistaFinancierosExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaAnalistaFinancierosExcluir);
                var grupoAnalistaFinancierosExcluir = (from g in listaAnalistaFinancierosExcluir
                                          group g by new
                                          {
                                              idAnalista = g.idAnalista,
                                              Level = g.Level
                                          }
                                  into g
                                          select new
                                          {
                                              Id = g.Key.idAnalista,
                                              Name = g.Key.Level,
                                              FechaHora = g.Max(d => d.FechaHora)
                                          }).Select(x => new PersonalSeleccionadoDashboard
                                          {
                                              idAnalista = x.Id,
                                              Level = x.Name,
                                              FechaHora = x.FechaHora
                                          });

                foreach (var itemIncluir in grupoAnalistaFinancierosIncluir)
                {
                    var itemAgregar = grupoAnalistaFinancierosExcluir.Where(x => x.idAnalista == itemIncluir.idAnalista &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaAnalistaPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoAnalistaFinancierosExcluir.Where(x => x.idAnalista == itemIncluir.idAnalista &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaAnalistaPersonal.Add(itemIncluir);
                        }
                    }
                }


                var listaClientesPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaClientesIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaClientesIncluir);
                var grupoClientesIncluir = (from g in listaClientesIncluir
                                                       group g by new
                                                       {
                                                           idAnalista = g.idAnalista,
                                                           idCliente = g.idCliente,
                                                           Level = g.Level
                                                       }
                                           into g
                                                       select new
                                                       {
                                                           IdAnalista = g.Key.idAnalista,
                                                           IdCliente = g.Key.idCliente,
                                                           Name = g.Key.Level,
                                                           FechaHora = g.Max(d => d.FechaHora)
                                                       }).Select(x => new PersonalSeleccionadoDashboard
                                                       {
                                                           idAnalista = x.IdAnalista,
                                                           idCliente = x.IdCliente,
                                                           Level = x.Name,
                                                           FechaHora = x.FechaHora
                                                       });

                var listaClientesExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaClientesExcluir);
                var grupoClientesExcluir = (from g in listaClientesExcluir
                                            group g by new
                                                       {
                                                            idAnalista = g.idAnalista,
                                                            idCliente = g.idCliente,
                                                            Level = g.Level
                                                       }
                                            into g
                                                       select new
                                                       {
                                                           IdAnalista = g.Key.idAnalista,
                                                           IdCliente = g.Key.idCliente,
                                                           Name = g.Key.Level,
                                                           FechaHora = g.Max(d => d.FechaHora)
                                                       }).Select(x => new PersonalSeleccionadoDashboard
                                                       {
                                                           idAnalista = x.IdAnalista,
                                                           idCliente = x.IdCliente,
                                                           Level = x.Name,
                                                           FechaHora = x.FechaHora
                                                       });


                foreach (var itemIncluir in grupoClientesIncluir)
                {
                    var itemAgregar = grupoClientesExcluir.Where(x => x.idAnalista == itemIncluir.idAnalista &&
                                                                      x.idCliente == itemIncluir.idCliente && 
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaClientesPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoClientesExcluir.Where(x => x.idAnalista == itemIncluir.idAnalista &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaClientesPersonal.Add(itemIncluir);
                        }
                    }
                }


                var listaOportunidadPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaOportunidadIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaOportunidadesIncluir);
                var grupoOportunidadIncluir = (from g in listaOportunidadIncluir
                                               group g by new
                                            {
                                                idAnalista = g.idAnalista,
                                                idCliente = g.idCliente,
                                                idOportunidad = g.IdOportunidad,
                                                Level = g.Level
                                            }
                                           into g
                                            select new
                                            {
                                                IdAnalista = g.Key.idAnalista,
                                                IdCliente = g.Key.idCliente,
                                                IdOportunidad = g.Key.idOportunidad,
                                                Name = g.Key.Level,
                                                FechaHora = g.Max(d => d.FechaHora)
                                            }).Select(x => new PersonalSeleccionadoDashboard
                                            {
                                                idAnalista = x.IdAnalista,
                                                idCliente = x.IdCliente,
                                                IdOportunidad = x.IdOportunidad,
                                                Level = x.Name,
                                                FechaHora = x.FechaHora
                                            });

                var listaOportunidadExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaOportunidadesExcluir);
                var grupoOportunidadExcluir = (from g in listaOportunidadExcluir
                                               group g by new
                                            {
                                                idAnalista = g.idAnalista,
                                                idCliente = g.idCliente,
                                                idOportunidad = g.IdOportunidad,
                                                Level = g.Level
                                            }
                                            into g
                                            select new
                                            {
                                                IdAnalista = g.Key.idAnalista,
                                                IdCliente = g.Key.idCliente,
                                                IdOportunidad = g.Key.idOportunidad,
                                                Name = g.Key.Level,
                                                FechaHora = g.Max(d => d.FechaHora)
                                            }).Select(x => new PersonalSeleccionadoDashboard
                                            {
                                                idAnalista = x.IdAnalista,
                                                idCliente = x.IdCliente,
                                                IdOportunidad = x.IdOportunidad,
                                                Level = x.Name,
                                                FechaHora = x.FechaHora
                                            });


                foreach (var itemIncluir in grupoOportunidadIncluir)
                {
                    var itemAgregar = grupoOportunidadExcluir.Where(x => x.idAnalista == itemIncluir.idAnalista &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.IdOportunidad == itemIncluir.IdOportunidad && 
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaOportunidadPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoOportunidadExcluir.Where(x => x.idAnalista == itemIncluir.idAnalista &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.IdOportunidad == itemIncluir.IdOportunidad &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaOportunidadPersonal.Add(itemIncluir);
                        }
                    }
                }


                var listaCasosPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaCasosIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaCasosIncluir);
                var grupoCasosIncluir = (from g in listaCasosIncluir
                                         group g by new
                                               {
                                                   idAnalista = g.idAnalista,
                                                   idCliente = g.idCliente,
                                                   idOportunidad = g.IdOportunidad,
                                                   NumeroCaso = g.NumeroCaso,
                                                   Level = g.Level
                                               }
                                           into g
                                               select new
                                               {
                                                   IdAnalista = g.Key.idAnalista,
                                                   IdCliente = g.Key.idCliente,
                                                   IdOportunidad = g.Key.idOportunidad,
                                                   NumeroCaso = g.Key.NumeroCaso,
                                                   Name = g.Key.Level,
                                                   FechaHora = g.Max(d => d.FechaHora)
                                               }).Select(x => new PersonalSeleccionadoDashboard
                                               {
                                                   idAnalista = x.IdAnalista,
                                                   idCliente = x.IdCliente,
                                                   IdOportunidad = x.IdOportunidad,
                                                   NumeroCaso = x.NumeroCaso,
                                                   Level = x.Name,
                                                   FechaHora = x.FechaHora
                                               });


                var listaCasosExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaCasosExcluir);
                var grupoCasosExcluir = (from g in listaCasosExcluir
                                         group g by new
                                               {
                                                   idAnalista = g.idAnalista,
                                                   idCliente = g.idCliente,
                                                   idOportunidad = g.IdOportunidad,
                                                   NumeroCaso = g.NumeroCaso,
                                                   Level = g.Level
                                               }
                                            into g
                                               select new
                                               {
                                                   IdAnalista = g.Key.idAnalista,
                                                   IdCliente = g.Key.idCliente,
                                                   IdOportunidad = g.Key.idOportunidad,
                                                   NumeroCaso = g.Key.NumeroCaso,
                                                   Name = g.Key.Level,
                                                   FechaHora = g.Max(d => d.FechaHora)
                                               }).Select(x => new PersonalSeleccionadoDashboard
                                               {
                                                   idAnalista = x.IdAnalista,
                                                   idCliente = x.IdCliente,
                                                   IdOportunidad = x.IdOportunidad,
                                                   NumeroCaso = x.NumeroCaso,
                                                   Level = x.Name,
                                                   FechaHora = x.FechaHora
                                               });

                foreach (var itemIncluir in grupoCasosIncluir)
                {
                    var itemAgregar = grupoCasosExcluir.Where(x => x.idAnalista == itemIncluir.idAnalista &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.IdOportunidad == itemIncluir.IdOportunidad &&
                                                                      x.NumeroCaso == itemIncluir.NumeroCaso &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaCasosPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoCasosExcluir.Where(x => x.idAnalista == itemIncluir.idAnalista &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.IdOportunidad == itemIncluir.IdOportunidad &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.NumeroCaso == itemIncluir.NumeroCaso &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaCasosPersonal.Add(itemIncluir);
                        }
                    }
                }


               var detalleOportuidades = 
                    ListarRentabilidadAnalistasFinancierosDetalle(listaAnalistaPersonal, listaClientesPersonal, listaOportunidadPersonal, listaCasosPersonal,
                    filtro, resultado);

                return new IndicadorRentabilidadConsolidadoDtoResponse
                {
                    ListadoOportunidades = detalleOportuidades.OrderBy(x => x.Orden).ThenBy(x => x.OrdenMostrar).ToList(),
                    Total = analistas.Any() ? analistas[0].TotalRow.Value : Util.Constantes.Generales.Numeric.Cero
                };

            }

            
        }

        private List<IndicadorRentabilidadDtoResponse> ListarRentabilidadAnalistasFinancierosDetalle(
                List<PersonalSeleccionadoDashboard> listaAnalistaPersonal,
                List<PersonalSeleccionadoDashboard> listaClientesPersonal,
                List<PersonalSeleccionadoDashboard> listaOportunidadPersonal,
                List<PersonalSeleccionadoDashboard> listaCasosPersonal,
                IndicadorRentabilidadDtoRequest filtro,
                List<IndicadorRentabilidadDtoResponse> resultado)
                {

                 foreach (var item in listaAnalistaPersonal.Where(x => x.Level == "2"))
                    {

                        var ordenMostraDetalle = Util.Constantes.Generales.Numeric.Uno;

                        var clientes = iOportunidadFinancieraOrigenDal.ListarRentabilidadClientes(new IndicadorRentabilidadDtoRequest
                        {
                            Anio = filtro.Anio,
                            IdAnalistaFinanciero = item.idAnalista
                        });

                        
                        if (clientes.Any())
                        {

                            var cabeceraDesplegada = resultado.Where(x =>
                                                         x.IdAnalistaFinanciero == item.idAnalista && x.Nivel == 1).FirstOrDefault();
                            if (cabeceraDesplegada != null)
                            {
                                cabeceraDesplegada.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                            }

                            resultado.Add(new IndicadorRentabilidadDtoResponse
                            {
                                IdAnalistaFinanciero = item.idAnalista,
                                AnalistaFinanciero = string.Empty,
                                IdCliente = 0,
                                CarteraClientesLiderCadena = TitulosDetalleOportunidades.CarteraClientesLider,
                                OportunidadesCadena = TitulosDetalleOportunidades.NroOportunidadesLider,
                                TiempoEntregaPromedioCadena = TitulosDetalleOportunidades.TiempoEntregaPromedioLiderCadena,
                                PorcentajeCumplimientoCadena = TitulosDetalleOportunidades.PorcentajeCumplimientoLiderCadena,
                                TiempoEntregaPromedioClienteCadena = TitulosDetalleOportunidades.TiempoEntregaPromedioLider,
                                CumplimientoEntregaClienteCadena = TitulosDetalleOportunidades.PorcentajeCumplimientoLider,
                                Orden = cabeceraDesplegada.Orden,
                                OrdenMostrar = ordenMostraDetalle,
                                Nivel = Util.Constantes.Generales.Numeric.Dos,
                                Desplegado = Util.Constantes.Generales.Numeric.Cero
                            });

                            ordenMostraDetalle++;

                            foreach (var cliente in clientes)
                            {
                                resultado.Add(new IndicadorRentabilidadDtoResponse
                                {
                                    IdAnalistaFinanciero = item.idAnalista,
                                    AnalistaFinanciero = string.Empty,
                                    IdCliente = cliente.IdCliente,
                                    CarteraClientesLiderCadena = cliente.Cliente,
                                    OportunidadesCadena = cliente.OportunidadesAbiertasCliente.ToString(),
                                    TiempoEntregaPromedioCadena = cliente.OfertasTrabajadasCliente.ToString(),
                                    PorcentajeCumplimientoCadena = cliente.OfertasGanadasCliente.ToString(),
                                    TiempoEntregaPromedioClienteCadena = cliente.TiempoEntregaPromedioCliente.ToString(),
                                    CumplimientoEntregaClienteCadena = cliente.PorcentajeCumplimientoCliente.ToString(),
                                    Orden = cabeceraDesplegada.Orden,
                                    OrdenMostrar = ordenMostraDetalle,
                                    Nivel = Util.Constantes.Generales.Numeric.Dos,
                                    Desplegado = Util.Constantes.Generales.Numeric.Cero
                                });

                                ordenMostraDetalle++;

                                var clientesRestriccion = listaClientesPersonal.Where(x => x.Level == "3" && x.idAnalista == item.idAnalista &&
                                                                                                           x.idCliente == cliente.IdCliente).FirstOrDefault();

                                
                                if (clientesRestriccion != null)
                                {

                                    var oportunidades = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorOportunidades(new IndicadorDashboardSectorDtoRequest
                                    {
                                        Anio = filtro.Anio.Value,
                                        IdJefe = cliente.IdCliente.Value
                                    });

                                    
                                    if (oportunidades.Any())
                                    {

                                        var cabeceraDesplegada2 = resultado.Where(x =>
                                                        x.IdAnalistaFinanciero == item.idAnalista && x.IdCliente == cliente.IdCliente 
                                                        && x.Nivel == 2).FirstOrDefault();

                                        if (cabeceraDesplegada2 != null)
                                        {
                                            cabeceraDesplegada2.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                                        }

                                        resultado.Add(new IndicadorRentabilidadDtoResponse
                                        {
                                            IdAnalistaFinanciero = item.idAnalista,
                                            AnalistaFinanciero = string.Empty,
                                            IdCliente = cliente.IdCliente,
                                            CarteraClientesLiderCadena = string.Empty,
                                            OportunidadesCadena = TitulosDetalleOportunidades.NroOportunidades,
                                            TiempoEntregaPromedioCadena = TitulosDetalleOportunidades.GerenteCuenta,
                                            PorcentajeCumplimientoCadena = TitulosDetalleOportunidades.LineaNegocio,
                                            TiempoEntregaPromedioClienteCadena = TitulosDetalleOportunidades.DescripcionOportunidad,
                                            CumplimientoEntregaClienteCadena = TitulosDetalleOportunidades.TipoOportunidad,
                                            CantidadOfertasClientes = TitulosDetalleOportunidades.CantidadOfertas,
                                            FaseOportunidad = TitulosDetalleOportunidades.Fase,
                                            ProbabilidadExitoCadena = TitulosDetalleOportunidades.Exito,
                                            FechaCierreEstimadaCadena = TitulosDetalleOportunidades.CierreEstimado,
                                            Orden = cabeceraDesplegada.Orden,
                                            OrdenMostrar = ordenMostraDetalle,
                                            Nivel = Util.Constantes.Generales.Numeric.Tres,
                                            Desplegado = Util.Constantes.Generales.Numeric.Cero
                                        });

                                        ordenMostraDetalle++;

                                        foreach (var oportunidad in oportunidades)
                                        {
                                            resultado.Add(new IndicadorRentabilidadDtoResponse
                                            {
                                                IdAnalistaFinanciero = item.idAnalista,
                                                AnalistaFinanciero = string.Empty,
                                                IdCliente = cliente.IdCliente,
                                                CarteraClientesLiderCadena = string.Empty,
                                                IdOportunidad = oportunidad.IdOportunidad,
                                                OportunidadesCadena = oportunidad.IdOportunidad,
                                                TiempoEntregaPromedioCadena = oportunidad.PropietarioOportunidad,
                                                PorcentajeCumplimientoCadena = oportunidad.LineaNegocio,
                                                TiempoEntregaPromedioClienteCadena = oportunidad.NombreOportunidad,
                                                CumplimientoEntregaClienteCadena = oportunidad.TipologiaOportunidad,
                                                CantidadOfertasClientes = oportunidad.CantidadOfertas.Value.ToString(),
                                                FaseOportunidad = oportunidad.FaseOportunidad,
                                                ProbabilidadExitoCadena = oportunidad.ProbabilidadExito,
                                                FechaCierreEstimadaCadena = Util.Funciones.Funciones.FormatoFechaIndicadores(oportunidad.FechaCierreEstimada),
                                                Orden = cabeceraDesplegada.Orden,
                                                OrdenMostrar = ordenMostraDetalle,
                                                Nivel = Util.Constantes.Generales.Numeric.Tres,
                                                Desplegado = Util.Constantes.Generales.Numeric.Cero
                                            });

                                            ordenMostraDetalle++;

                                            var oportunidadesRestriccion = 
                                            listaOportunidadPersonal.Where(x => x.Level == "4" && x.idAnalista == item.idAnalista &&
                                                                                                x.idCliente == cliente.IdCliente &&
                                                                                                x.IdOportunidad == oportunidad.IdOportunidad).FirstOrDefault();

                                            if (oportunidadesRestriccion != null)
                                            {

                                                var casos = iOportunidadFinancieraOrigenDal.ListarRentabilidadCasos(new IndicadorRentabilidadDtoRequest
                                                {
                                                    Anio = filtro.Anio.Value,
                                                    IdOportunidad = oportunidad.IdOportunidad
                                                });

                                                
                                                if (casos.Any())
                                                {

                                                    var cabeceraDesplegada3 = resultado.Where(x =>
                                                               x.IdAnalistaFinanciero == item.idAnalista && x.IdCliente == cliente.IdCliente &&
                                                               x.IdOportunidad == oportunidad.IdOportunidad 
                                                               && x.Nivel == 3).FirstOrDefault();

                                                    if (cabeceraDesplegada3 != null)
                                                    {
                                                        cabeceraDesplegada3.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                                                    }

                                                    resultado.Add(new IndicadorRentabilidadDtoResponse
                                                    {
                                                        IdAnalistaFinanciero = item.idAnalista,
                                                        AnalistaFinanciero = string.Empty,
                                                        IdCliente = cliente.IdCliente,
                                                        CarteraClientesLiderCadena = string.Empty,
                                                        IdOportunidad = oportunidad.IdOportunidad,
                                                        OportunidadesCadena = string.Empty,
                                                        NumeroCaso = string.Empty,
                                                        TiempoEntregaPromedioCadena = TitulosDetalleOportunidades.Oferta,
                                                        PorcentajeCumplimientoCadena = TitulosDetalleOportunidades.Preventa,
                                                        TiempoEntregaPromedioClienteCadena = TitulosDetalleOportunidades.DiasAtencionPreventa,
                                                        CumplimientoEntregaClienteCadena = TitulosDetalleOportunidades.AnalistaFinanciero,
                                                        CantidadOfertasClientes = TitulosDetalleOportunidades.DiasAtencionFinanzas,
                                                        FaseOportunidad = TitulosDetalleOportunidades.Capex,
                                                        ProbabilidadExitoCadena = TitulosDetalleOportunidades.Opex,
                                                        FechaCierreEstimadaCadena = TitulosDetalleOportunidades.Ingresos,
                                                        OibdaCadena = TitulosDetalleOportunidades.Oidba,
                                                        FlujoCajaCadena = TitulosDetalleOportunidades.Fc,
                                                        FullContractValueNetoCadena = TitulosDetalleOportunidades.Fcv,
                                                        PayBack = TitulosDetalleOportunidades.Payback,
                                                        PagoUnicoDolares = TitulosDetalleOportunidades.PagoUnico,
                                                        PagoRecurrenteDolares = TitulosDetalleOportunidades.PagoRecurrente,
                                                        MesesPagoRecurrenteCadena = TitulosDetalleOportunidades.Meses,
                                                        TipoCambioCadena = TitulosDetalleOportunidades.TipoCambio,
                                                        Orden = cabeceraDesplegada.Orden,
                                                        OrdenMostrar = ordenMostraDetalle,
                                                        Nivel = Util.Constantes.Generales.Numeric.Cuatro,
                                                        Desplegado = Util.Constantes.Generales.Numeric.Cero
                                                    });

                                                    ordenMostraDetalle++;

                                                    foreach (var caso in casos)
                                                    {
                                                        resultado.Add(new IndicadorRentabilidadDtoResponse
                                                        {
                                                            IdAnalistaFinanciero = item.idAnalista,
                                                            AnalistaFinanciero = string.Empty,
                                                            IdCliente = cliente.IdCliente,
                                                            CarteraClientesLiderCadena = string.Empty,
                                                            IdOportunidad = oportunidad.IdOportunidad,
                                                            OportunidadesCadena = string.Empty,
                                                            NumeroCaso = caso.NumeroCaso,
                                                            TiempoEntregaPromedioCadena = caso.NumeroCaso,
                                                            PorcentajeCumplimientoCadena = caso.Preventa,
                                                            TiempoEntregaPromedioClienteCadena = caso.DiasAtencionPreventa.ToString(),
                                                            CumplimientoEntregaClienteCadena = caso.AnalistaFinanciero,
                                                            CantidadOfertasClientes = caso.DiasAtencionFinanzas.ToString(),
                                                            FaseOportunidad = caso.Capex.ToString(),
                                                            ProbabilidadExitoCadena = caso.CostosOpex.ToString(),
                                                            FechaCierreEstimadaCadena = caso.ImporteVentas.ToString(),
                                                            OibdaCadena = caso.Oibda.ToString(),
                                                            FlujoCajaCadena = caso.FlujoCaja.ToString(),
                                                            FullContractValueNetoCadena = caso.FullContractValueNeto.ToString(),
                                                            PayBack = caso.PayBack,
                                                            PagoUnicoDolares = caso.PagoUnicoDolares,
                                                            PagoRecurrenteDolares = caso.PagoRecurrenteDolares,
                                                            MesesPagoRecurrenteCadena = caso.MesesPagoRecurrente.ToString(),
                                                            TipoCambioCadena = caso.TipoCambio.ToString(),
                                                            Orden = cabeceraDesplegada.Orden,
                                                            OrdenMostrar = ordenMostraDetalle,
                                                            Nivel = Util.Constantes.Generales.Numeric.Cuatro,
                                                            Desplegado = Util.Constantes.Generales.Numeric.Cero
                                                        });

                                                        ordenMostraDetalle++;

                                                        var casosRestriccion =
                                                        listaCasosPersonal.Where(x => x.Level == "5" && x.idAnalista == item.idAnalista &&
                                                                                                       x.idCliente == cliente.IdCliente &&
                                                                                                       x.IdOportunidad == oportunidad.IdOportunidad &&
                                                                                                       x.NumeroCaso == Convert.ToInt32(caso.NumeroCaso)).FirstOrDefault();
                                                        
                                                        if (casosRestriccion != null)
                                                        {

                                                            var cabeceraDesplegada4 = resultado.Where(x =>
                                                               x.IdAnalistaFinanciero == item.idAnalista && 
                                                               x.IdCliente == cliente.IdCliente &&
                                                               x.IdOportunidad == oportunidad.IdOportunidad &&
                                                               int.Parse(string.IsNullOrWhiteSpace(x.NumeroCaso)  ? "0" : x.NumeroCaso) 
                                                               == Convert.ToInt32(caso.NumeroCaso) &&
                                                               x.Nivel == 4).FirstOrDefault();

                                                            if (cabeceraDesplegada4 != null)
                                                            {
                                                                cabeceraDesplegada4.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                                                            }


                                                            resultado.Add(new IndicadorRentabilidadDtoResponse
                                                            {
                                                                Orden = cabeceraDesplegada.Orden,
                                                                OrdenMostrar = ordenMostraDetalle,
                                                                Nivel = Util.Constantes.Generales.Numeric.Cinco,
                                                                IdAnalistaFinanciero = item.idAnalista,
                                                                AnalistaFinanciero = string.Empty,
                                                                IdCliente = cliente.IdCliente,
                                                                CarteraClientesLiderCadena = string.Empty,
                                                                IdOportunidad = oportunidad.IdOportunidad,
                                                                OportunidadesCadena = string.Empty,
                                                                NumeroCaso = caso.NumeroCaso,
                                                                Desplegado = Util.Constantes.Generales.Numeric.Cero
                                                            });

                                                            ordenMostraDetalle++;

                                                        }
                                                       
                                                    }

                                                    resultado.Add(new IndicadorRentabilidadDtoResponse
                                                    {
                                                        Orden = cabeceraDesplegada.Orden,
                                                        OrdenMostrar = ordenMostraDetalle,
                                                        Nivel = Util.Constantes.Generales.Numeric.Cuatro,
                                                        IdAnalistaFinanciero = item.idAnalista,
                                                        AnalistaFinanciero = string.Empty,
                                                        IdCliente = cliente.IdCliente,
                                                        CarteraClientesLiderCadena = string.Empty,
                                                        IdOportunidad = oportunidad.IdOportunidad,
                                                        OportunidadesCadena = string.Empty,
                                                        NumeroCaso = string.Empty,
                                                        TiempoEntregaPromedioCadena = TitulosDetalleOportunidades.EvolutivoOfertas,
                                                        Desplegado = Util.Constantes.Generales.Numeric.Cero
                                                    });

                                                    ordenMostraDetalle++;

                                                    resultado.Add(new IndicadorRentabilidadDtoResponse
                                                    {
                                                        Orden = cabeceraDesplegada.Orden,
                                                        OrdenMostrar = ordenMostraDetalle,
                                                        Nivel = Util.Constantes.Generales.Numeric.Cuatro,
                                                        IdAnalistaFinanciero = item.idAnalista,
                                                        AnalistaFinanciero = string.Empty,
                                                        IdCliente = cliente.IdCliente,
                                                        CarteraClientesLiderCadena = string.Empty,
                                                        IdOportunidad = oportunidad.IdOportunidad,
                                                        OportunidadesCadena = string.Empty,
                                                        NumeroCaso = string.Empty,
                                                        TiempoEntregaPromedioCadena = string.Empty,
                                                        Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                                        GraficoEvolutivo = 1
                                                    });

                                                    ordenMostraDetalle++;

                                                }
                                            }
                                        }
                                   }

                                }        
                            }
                        }    
                    }
                    return resultado;
                }

    }
}
