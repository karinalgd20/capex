﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Funnel;
using Tgs.SDIO.AL.DataAccess.Interfaces.Funnel;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;
using TitulosDetalleOportunidades = Tgs.SDIO.Util.Constantes.Funnel.TitulosDetalleOportunidades;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Funnel
{
    public class OportunidadFinancieraOrigenLineaNegocioBl : IOportunidadFinancieraOrigenLineaNegocioBl
    {
        readonly IOportunidadFinancieraOrigenDal iOportunidadFinancieraOrigenDal;
        readonly IOportunidadFinancieraOrigenLineaNegocioDal iOportunidadFinancieraOrigenLineaNegocioDal;

        public OportunidadFinancieraOrigenLineaNegocioBl(
            IOportunidadFinancieraOrigenLineaNegocioDal IOportunidadFinancieraOrigenLineaNegocioDal,
            IOportunidadFinancieraOrigenDal IOportunidadFinancieraOrigenDal)
            {
                iOportunidadFinancieraOrigenLineaNegocioDal = IOportunidadFinancieraOrigenLineaNegocioDal;
                iOportunidadFinancieraOrigenDal = IOportunidadFinancieraOrigenDal;
            }

        public IndicadorLineasNegocioDtoResponse ListarLineasNegocio(IndicadorLineasNegocioDtoRequest filtro)
        {
            var respuesta = new List<IndicadorLineasNegocioListadoDtoResponse>();

            var lineas = iOportunidadFinancieraOrigenLineaNegocioDal.ListarLineasNegocio(filtro).ToList();

            foreach (var item in lineas)
            {
                respuesta.Add(new IndicadorLineasNegocioListadoDtoResponse
                {
                    IdLineaNegocio = item.IdLineaNegocio,
                    LineaNegocio = item.LineaNegocio,
                    CantidadClientesCadena = item.CantidadClientes.ToString(),
                    OportunidadesAbiertasClienteCadena = item.OportunidadesAbiertasCliente.ToString(),
                    IngresoTotalCadena = item.IngresoTotal.ToString(),
                    IngresoCapexCadena = item.IngresoCapex.ToString(),
                    CostosDirectosCadena = item.CostosDirectos.ToString(),
                    Orden = item.Orden,
                    OrdenMostrar = Util.Constantes.Generales.Numeric.Cero,
                    Nivel = Util.Constantes.Generales.Numeric.Uno,
                    Desplegado = Util.Constantes.Generales.Numeric.Cero
                });
            }


            if (filtro.Nivel.Equals(Util.Constantes.Generales.Numeric.Uno))
            {
                return new IndicadorLineasNegocioDtoResponse
                {
                    Lineas = respuesta.OrderBy(x => x.Orden).ToList(),
                    Total = lineas.Any() ? lineas[0].TotalRow.Value : Util.Constantes.Generales.Numeric.Cero

                };
            }
            else
            {
                var listaLNPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaLineaNegocioIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaLineaNegocioIncluir);
                var grupoLineaNegocioIncluir = (from g in listaLineaNegocioIncluir
                                                       group g by new
                                                       {
                                                           idLineaNegocio = g.idLineaNegocio,
                                                           Level = g.Level
                                                       }
                                                        into g
                                                       select new
                                                       {
                                                           Id = g.Key.idLineaNegocio,
                                                           Name = g.Key.Level,
                                                           FechaHora = g.Max(d => d.FechaHora)
                                                       }).Select(x => new PersonalSeleccionadoDashboard
                                                       {
                                                           idLineaNegocio = x.Id,
                                                           Level = x.Name,
                                                           FechaHora = x.FechaHora
                                                       });


                var listaLineaNegocioExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaLineaNegocioExcluir);
                var grupoLineaNegocioExcluir = (from g in listaLineaNegocioExcluir
                                                       group g by new
                                                       {
                                                           idLineaNegocio = g.idLineaNegocio,
                                                           Level = g.Level
                                                       }
                                                        into g
                                                       select new
                                                       {
                                                           Id = g.Key.idLineaNegocio,
                                                           Name = g.Key.Level,
                                                           FechaHora = g.Max(d => d.FechaHora)
                                                       }).Select(x => new PersonalSeleccionadoDashboard
                                                       {
                                                           idLineaNegocio = x.Id,
                                                           Level = x.Name,
                                                           FechaHora = x.FechaHora
                                                       });

                foreach (var itemIncluir in grupoLineaNegocioIncluir)
                {
                    var itemAgregar = grupoLineaNegocioExcluir.Where(x => x.idLineaNegocio == itemIncluir.idLineaNegocio &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaLNPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoLineaNegocioExcluir.Where(x => x.idLineaNegocio == itemIncluir.idLineaNegocio &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaLNPersonal.Add(itemIncluir);
                        }
                    }
                }



                var listaClientesPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaClientesIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaClientesIncluir);
                var grupoClientesIncluir = (from g in listaClientesIncluir
                                            group g by new
                                            {
                                                idLineaNegocio = g.idLineaNegocio,
                                                idCliente = g.idCliente,
                                                Level = g.Level
                                            }
                                           into g
                                            select new
                                            {
                                                idLineaNegocio = g.Key.idLineaNegocio,
                                                IdCliente = g.Key.idCliente,
                                                Name = g.Key.Level,
                                                FechaHora = g.Max(d => d.FechaHora)
                                            }).Select(x => new PersonalSeleccionadoDashboard
                                            {
                                                idLineaNegocio = x.idLineaNegocio,
                                                idCliente = x.IdCliente,
                                                Level = x.Name,
                                                FechaHora = x.FechaHora
                                            });

                var listaClientesExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaClientesExcluir);
                var grupoClientesExcluir = (from g in listaClientesExcluir
                                            group g by new
                                            {
                                                idLineaNegocio = g.idLineaNegocio,
                                                idCliente = g.idCliente,
                                                Level = g.Level
                                            }
                                            into g
                                            select new
                                            {
                                                idLineaNegocio = g.Key.idLineaNegocio,
                                                IdCliente = g.Key.idCliente,
                                                Name = g.Key.Level,
                                                FechaHora = g.Max(d => d.FechaHora)
                                            }).Select(x => new PersonalSeleccionadoDashboard
                                            {
                                                idLineaNegocio = x.idLineaNegocio,
                                                idCliente = x.IdCliente,
                                                Level = x.Name,
                                                FechaHora = x.FechaHora
                                            });


                foreach (var itemIncluir in grupoClientesIncluir)
                {
                    var itemAgregar = grupoClientesExcluir.Where(x => x.idLineaNegocio == itemIncluir.idLineaNegocio &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaClientesPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoClientesExcluir.Where(x => x.idLineaNegocio == itemIncluir.idLineaNegocio &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaClientesPersonal.Add(itemIncluir);
                        }
                    }
                }


                var listaOportunidadPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaOportunidadIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaOportunidadesIncluir);
                var grupoOportunidadIncluir = (from g in listaOportunidadIncluir
                                               group g by new
                                               {
                                                   idLineaNegocio = g.idLineaNegocio,
                                                   idCliente = g.idCliente,
                                                   idOportunidad = g.IdOportunidad,
                                                   Level = g.Level
                                               }
                                           into g
                                               select new
                                               {
                                                   idLineaNegocio = g.Key.idLineaNegocio,
                                                   IdCliente = g.Key.idCliente,
                                                   IdOportunidad = g.Key.idOportunidad,
                                                   Name = g.Key.Level,
                                                   FechaHora = g.Max(d => d.FechaHora)
                                               }).Select(x => new PersonalSeleccionadoDashboard
                                               {
                                                   idLineaNegocio = x.idLineaNegocio,
                                                   idCliente = x.IdCliente,
                                                   IdOportunidad = x.IdOportunidad,
                                                   Level = x.Name,
                                                   FechaHora = x.FechaHora
                                               });

                var listaOportunidadExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaOportunidadesExcluir);
                var grupoOportunidadExcluir = (from g in listaOportunidadExcluir
                                               group g by new
                                               {
                                                   idLineaNegocio = g.idLineaNegocio,
                                                   idCliente = g.idCliente,
                                                   idOportunidad = g.IdOportunidad,
                                                   Level = g.Level
                                               }
                                            into g
                                               select new
                                               {
                                                   idLineaNegocio = g.Key.idLineaNegocio,
                                                   IdCliente = g.Key.idCliente,
                                                   IdOportunidad = g.Key.idOportunidad,
                                                   Name = g.Key.Level,
                                                   FechaHora = g.Max(d => d.FechaHora)
                                               }).Select(x => new PersonalSeleccionadoDashboard
                                               {
                                                   idLineaNegocio = x.idLineaNegocio,
                                                   idCliente = x.IdCliente,
                                                   IdOportunidad = x.IdOportunidad,
                                                   Level = x.Name,
                                                   FechaHora = x.FechaHora
                                               });


                foreach (var itemIncluir in grupoOportunidadIncluir)
                {
                    var itemAgregar = grupoOportunidadExcluir.Where(x => x.idLineaNegocio == itemIncluir.idLineaNegocio &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.IdOportunidad == itemIncluir.IdOportunidad &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaOportunidadPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoOportunidadExcluir.Where(x => x.idLineaNegocio == itemIncluir.idLineaNegocio &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.IdOportunidad == itemIncluir.IdOportunidad &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaOportunidadPersonal.Add(itemIncluir);
                        }
                    }
                }

                var listaCasosPersonal = new List<PersonalSeleccionadoDashboard>();

                var listaCasosIncluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaCasosIncluir);
                var grupoCasosIncluir = (from g in listaCasosIncluir
                                         group g by new
                                         {
                                             idLineaNegocio = g.idLineaNegocio,
                                             idCliente = g.idCliente,
                                             idOportunidad = g.IdOportunidad,
                                             NumeroCaso = g.NumeroCaso,
                                             Level = g.Level
                                         }
                                           into g
                                         select new
                                         {
                                             idLineaNegocio = g.Key.idLineaNegocio,
                                             IdCliente = g.Key.idCliente,
                                             IdOportunidad = g.Key.idOportunidad,
                                             NumeroCaso = g.Key.NumeroCaso,
                                             Name = g.Key.Level,
                                             FechaHora = g.Max(d => d.FechaHora)
                                         }).Select(x => new PersonalSeleccionadoDashboard
                                         {
                                             idLineaNegocio = x.idLineaNegocio,
                                             idCliente = x.IdCliente,
                                             IdOportunidad = x.IdOportunidad,
                                             NumeroCaso = x.NumeroCaso,
                                             Level = x.Name,
                                             FechaHora = x.FechaHora
                                         });


                var listaCasosExcluir = JsonConvert.DeserializeObject<List<PersonalSeleccionadoDashboard>>(filtro.ListaCasosExcluir);
                var grupoCasosExcluir = (from g in listaCasosExcluir
                                         group g by new
                                         {
                                             idLineaNegocio = g.idLineaNegocio,
                                             idCliente = g.idCliente,
                                             idOportunidad = g.IdOportunidad,
                                             NumeroCaso = g.NumeroCaso,
                                             Level = g.Level
                                         }
                                            into g
                                         select new
                                         {
                                             idLineaNegocio = g.Key.idLineaNegocio,
                                             IdCliente = g.Key.idCliente,
                                             IdOportunidad = g.Key.idOportunidad,
                                             NumeroCaso = g.Key.NumeroCaso,
                                             Name = g.Key.Level,
                                             FechaHora = g.Max(d => d.FechaHora)
                                         }).Select(x => new PersonalSeleccionadoDashboard
                                         {
                                             idLineaNegocio = x.idLineaNegocio,
                                             idCliente = x.IdCliente,
                                             IdOportunidad = x.IdOportunidad,
                                             NumeroCaso = x.NumeroCaso,
                                             Level = x.Name,
                                             FechaHora = x.FechaHora
                                         });

                foreach (var itemIncluir in grupoCasosIncluir)
                {
                    var itemAgregar = grupoCasosExcluir.Where(x => x.idLineaNegocio == itemIncluir.idLineaNegocio &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.IdOportunidad == itemIncluir.IdOportunidad &&
                                                                      x.NumeroCaso == itemIncluir.NumeroCaso &&
                                                                      x.Level == itemIncluir.Level);
                    if (!itemAgregar.Any())
                    {
                        listaCasosPersonal.Add(itemIncluir);
                    }
                    else
                    {
                        var itemAgregarHora = grupoCasosExcluir.Where(x => x.idLineaNegocio == itemIncluir.idLineaNegocio &&
                                                                      x.idCliente == itemIncluir.idCliente &&
                                                                      x.IdOportunidad == itemIncluir.IdOportunidad &&
                                                                      x.Level == itemIncluir.Level &&
                                                                      x.NumeroCaso == itemIncluir.NumeroCaso &&
                                                                      x.FechaHora >= itemIncluir.FechaHora);
                        if (!itemAgregarHora.Any())
                        {
                            listaCasosPersonal.Add(itemIncluir);
                        }
                    }
                }

                var detalleOportunidades =
                    ListarOportunidadesLineaNegocioDetalle(listaLNPersonal, listaClientesPersonal, 
                    listaOportunidadPersonal, listaCasosPersonal,
                    filtro, respuesta);

                return new IndicadorLineasNegocioDtoResponse
                {
                    Lineas = detalleOportunidades.OrderBy(x => x.Orden).ThenBy(x => x.OrdenMostrar).ToList(),
                    Total = lineas.Any() ? lineas[0].TotalRow.Value : Util.Constantes.Generales.Numeric.Cero
                };

            }
        }

        private List<IndicadorLineasNegocioListadoDtoResponse> ListarOportunidadesLineaNegocioDetalle(
              List<PersonalSeleccionadoDashboard> listaLNPersonal,
              List<PersonalSeleccionadoDashboard> listaClientesPersonal,
              List<PersonalSeleccionadoDashboard> listaOportunidadPersonal,
              List<PersonalSeleccionadoDashboard> listaCasosPersonal,
              IndicadorLineasNegocioDtoRequest filtro,
              List<IndicadorLineasNegocioListadoDtoResponse> resultado)
             {

                foreach (var item in listaLNPersonal.Where(x => x.Level == "2"))
                {
                    var ordenMostraDetalle = Util.Constantes.Generales.Numeric.Uno;

                    var clientes = iOportunidadFinancieraOrigenLineaNegocioDal.ListarClientesLineasNegocio(new IndicadorLineasNegocioDtoRequest
                    {
                       Anio = filtro.Anio,
                       IdLineaNegocio = item.idLineaNegocio
                    });

                    if (clientes.Any())
                    {
                        var cabeceraDesplegada = resultado.Where(x =>
                                                        x.IdLineaNegocio == item.idLineaNegocio && x.Nivel == 1).FirstOrDefault();
                        if (cabeceraDesplegada != null)
                        {
                            cabeceraDesplegada.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                        }

                        resultado.Add(new IndicadorLineasNegocioListadoDtoResponse
                        {
                            IdLineaNegocio = item.idLineaNegocio,
                            LineaNegocio = string.Empty,
                            IdCliente = 0,
                            CantidadClientesCadena = TitulosDetalleOportunidades.CarteraClientesLider,
                            OportunidadesAbiertasClienteCadena = TitulosDetalleOportunidades.NroOportunidadesLider,
                            IngresoTotalCadena = TitulosDetalleOportunidades.TiempoEntregaPromedioLiderCadena,
                            IngresoCapexCadena = TitulosDetalleOportunidades.PorcentajeCumplimientoLiderCadena,
                            CostosDirectosCadena = TitulosDetalleOportunidades.TotalIngresos,
                            OfertasTrabajadasClienteCadena = TitulosDetalleOportunidades.TotalCapex,
                            OfertasGanadasClienteCadena = TitulosDetalleOportunidades.TotalOpex,
                            Orden = cabeceraDesplegada.Orden,
                            OrdenMostrar = ordenMostraDetalle,
                            Nivel = Util.Constantes.Generales.Numeric.Dos,
                            Desplegado = Util.Constantes.Generales.Numeric.Cero
                        });

                        ordenMostraDetalle++;

                        foreach (var cliente in clientes)
                        {
                            resultado.Add(new IndicadorLineasNegocioListadoDtoResponse
                            {
                                IdLineaNegocio = item.idLineaNegocio,
                                LineaNegocio = string.Empty,
                                IdCliente = cliente.IdCliente,
                                CantidadClientesCadena = cliente.Cliente,
                                OportunidadesAbiertasClienteCadena = cliente.OportunidadesAbiertasCliente.ToString(),
                                IngresoTotalCadena = cliente.OfertasTrabajadasCliente.ToString(),
                                IngresoCapexCadena = cliente.OfertasGanadasCliente.ToString(),
                                CostosDirectosCadena = cliente.TotalIngresos.ToString(),
                                OfertasTrabajadasClienteCadena = cliente.TotalCapex.ToString(),
                                OfertasGanadasClienteCadena = cliente.TotalOpex.ToString(),
                                Orden = cabeceraDesplegada.Orden,
                                OrdenMostrar = ordenMostraDetalle,
                                Nivel = Util.Constantes.Generales.Numeric.Dos,
                                Desplegado = Util.Constantes.Generales.Numeric.Cero
                            });

                            ordenMostraDetalle++;

                            var clientesRestriccion = listaClientesPersonal.Where(x => x.Level == "3" && 
                                    x.idLineaNegocio == item.idLineaNegocio &&
                                    x.idCliente == cliente.IdCliente).FirstOrDefault();


                            if (clientesRestriccion != null)
                            {

                                var oportunidades = iOportunidadFinancieraOrigenDal.ListarSeguimientoOportunidadesSectorOportunidades(
                                    new IndicadorDashboardSectorDtoRequest
                                    {
                                        Anio = filtro.Anio.Value,
                                        IdJefe = cliente.IdCliente.Value
                                    });

                                if (oportunidades.Any())
                                {

                                    var cabeceraDesplegada2 = resultado.Where(x =>
                                                      x.IdLineaNegocio == item.idLineaNegocio 
                                                      && x.IdCliente == cliente.IdCliente
                                                      && x.Nivel == 2).FirstOrDefault();

                                    if (cabeceraDesplegada2 != null)
                                    {
                                        cabeceraDesplegada2.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                                    }

                                    resultado.Add(new IndicadorLineasNegocioListadoDtoResponse
                                    {
                                        IdLineaNegocio = item.idLineaNegocio,
                                        LineaNegocio = string.Empty,
                                        IdCliente = cliente.IdCliente,
                                        CantidadClientesCadena = string.Empty,
                                        IdOportunidad = string.Empty,
                                        OportunidadesAbiertasClienteCadena = TitulosDetalleOportunidades.Oportunidades,
                                        IngresoTotalCadena = TitulosDetalleOportunidades.GerenteCuenta,
                                        IngresoCapexCadena = TitulosDetalleOportunidades.LineaNegocio,
                                        CostosDirectosCadena = TitulosDetalleOportunidades.DescripcionOportunidad,
                                        OfertasTrabajadasClienteCadena = TitulosDetalleOportunidades.TipoOportunidad,
                                        OfertasGanadasClienteCadena = TitulosDetalleOportunidades.CantidadOfertas,
                                        FaseOportunidad = TitulosDetalleOportunidades.Fase,
                                        ProbabilidadExito = TitulosDetalleOportunidades.Exito,
                                        FechaCierreEstimadaCadena = TitulosDetalleOportunidades.CierreEstimado,
                                        Orden = cabeceraDesplegada.Orden,
                                        OrdenMostrar = ordenMostraDetalle,
                                        Nivel = Util.Constantes.Generales.Numeric.Tres,
                                        Desplegado = Util.Constantes.Generales.Numeric.Cero
                                    });

                                    ordenMostraDetalle++;

                                    foreach (var oportunidad in oportunidades)
                                    {

                                        resultado.Add(new IndicadorLineasNegocioListadoDtoResponse
                                        {
                                            IdLineaNegocio = item.idLineaNegocio,
                                            LineaNegocio = string.Empty,
                                            IdCliente = cliente.IdCliente,
                                            CantidadClientesCadena = string.Empty,
                                            IdOportunidad = oportunidad.IdOportunidad,
                                            OportunidadesAbiertasClienteCadena = oportunidad.IdOportunidad,
                                            IngresoTotalCadena = oportunidad.PropietarioOportunidad,
                                            IngresoCapexCadena = oportunidad.LineaNegocio,
                                            CostosDirectosCadena = oportunidad.NombreOportunidad,
                                            OfertasTrabajadasClienteCadena = oportunidad.TipologiaOportunidad,
                                            OfertasGanadasClienteCadena = oportunidad.CantidadOfertas.ToString(),
                                            FaseOportunidad = oportunidad.FaseOportunidad,
                                            ProbabilidadExito = oportunidad.ProbabilidadExito,
                                            FechaCierreEstimadaCadena =
                                                  Util.Funciones.Funciones.FormatoFechaIndicadores(oportunidad.FechaCierreEstimada),
                                            Orden = cabeceraDesplegada.Orden,
                                            OrdenMostrar = ordenMostraDetalle,
                                            Nivel = Util.Constantes.Generales.Numeric.Tres,
                                            Desplegado = Util.Constantes.Generales.Numeric.Cero
                                        });

                                        ordenMostraDetalle++;

                                        var oportunidadesRestriccion = listaOportunidadPersonal.Where(x => x.Level == "4" &&
                                            x.idLineaNegocio == item.idLineaNegocio &&
                                            x.idCliente == cliente.IdCliente &&
                                            x.IdOportunidad == oportunidad.IdOportunidad).FirstOrDefault();
                                        
                                        if (oportunidadesRestriccion != null)
                                        {

                                            var casos = iOportunidadFinancieraOrigenDal.ListarRentabilidadCasos(new IndicadorRentabilidadDtoRequest
                                            {
                                                Anio = filtro.Anio.Value,
                                                IdOportunidad = oportunidad.IdOportunidad
                                            });
                                            
                                            if (casos.Any())
                                            {

                                                var cabeceraDesplegada3 = resultado.Where(x =>
                                                      x.IdLineaNegocio == item.idLineaNegocio
                                                      && x.IdCliente == cliente.IdCliente
                                                      && x.IdOportunidad == oportunidad.IdOportunidad
                                                      && x.Nivel == 3).FirstOrDefault();

                                                if (cabeceraDesplegada3 != null)
                                                {
                                                    cabeceraDesplegada3.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                                                }


                                                resultado.Add(new IndicadorLineasNegocioListadoDtoResponse
                                                {
                                                    IdLineaNegocio = item.idLineaNegocio,
                                                    LineaNegocio = string.Empty,
                                                    IdCliente = cliente.IdCliente,
                                                    CantidadClientesCadena = string.Empty,
                                                    IdOportunidad = oportunidad.IdOportunidad,
                                                    OportunidadesAbiertasClienteCadena = string.Empty,
                                                    NumeroCaso = string.Empty,
                                                    IngresoTotalCadena = TitulosDetalleOportunidades.Oferta,
                                                    IngresoCapexCadena = TitulosDetalleOportunidades.Preventa,
                                                    CostosDirectosCadena = TitulosDetalleOportunidades.DiasAtencionPreventa,
                                                    OfertasTrabajadasClienteCadena = TitulosDetalleOportunidades.AnalistaFinanciero,
                                                    OfertasGanadasClienteCadena = TitulosDetalleOportunidades.DiasAtencionFinanzas,
                                                    FaseOportunidad = TitulosDetalleOportunidades.Capex,
                                                    ProbabilidadExito = TitulosDetalleOportunidades.Opex,
                                                    FechaCierreEstimadaCadena = TitulosDetalleOportunidades.Ingresos,
                                                    OidbaCadena = TitulosDetalleOportunidades.Oidba,
                                                    FlujoCajaCadena = TitulosDetalleOportunidades.Fc,
                                                    FullContractValueNetoCadena = TitulosDetalleOportunidades.Fcv,
                                                    PayBack = TitulosDetalleOportunidades.Payback,
                                                    PagoUnicoDolares = TitulosDetalleOportunidades.PagoUnico,
                                                    PagoRecurrenteDolares = TitulosDetalleOportunidades.PagoRecurrente,
                                                    MesesPagoRecurrenteCadena = TitulosDetalleOportunidades.Meses,
                                                    TipoCambioCadena = TitulosDetalleOportunidades.TipoCambio,
                                                    Orden = cabeceraDesplegada.Orden,
                                                    OrdenMostrar = ordenMostraDetalle,
                                                    Nivel = Util.Constantes.Generales.Numeric.Cuatro,
                                                    Desplegado = Util.Constantes.Generales.Numeric.Cero
                                                });

                                                ordenMostraDetalle++;

                                                foreach (var caso in casos)
                                                {
                                                    resultado.Add(new IndicadorLineasNegocioListadoDtoResponse
                                                    {
                                                        IdLineaNegocio = item.idLineaNegocio,
                                                        LineaNegocio = string.Empty,
                                                        IdCliente = cliente.IdCliente,
                                                        CantidadClientesCadena = string.Empty,
                                                        IdOportunidad = oportunidad.IdOportunidad,
                                                        OportunidadesAbiertasClienteCadena = string.Empty,
                                                        NumeroCaso = caso.NumeroCaso,
                                                        IngresoTotalCadena = caso.NumeroCaso,
                                                        IngresoCapexCadena = caso.Preventa,
                                                        CostosDirectosCadena = caso.DiasAtencionPreventa.ToString(),
                                                        OfertasTrabajadasClienteCadena = caso.AnalistaFinanciero,
                                                        OfertasGanadasClienteCadena = caso.DiasAtencionFinanzas.ToString(),
                                                        FaseOportunidad = caso.Capex.ToString(),
                                                        ProbabilidadExito = caso.CostosOpex.ToString(),
                                                        FechaCierreEstimadaCadena = caso.ImporteVentas.ToString(),
                                                        OidbaCadena = caso.Oibda.ToString(),
                                                        FlujoCajaCadena = caso.FlujoCaja.ToString(),
                                                        FullContractValueNetoCadena = caso.FullContractValueNeto.ToString(),
                                                        PayBack = caso.PayBack,
                                                        PagoUnicoDolares = caso.PagoUnicoDolares,
                                                        PagoRecurrenteDolares = caso.PagoRecurrenteDolares,
                                                        MesesPagoRecurrenteCadena = caso.MesesPagoRecurrente.ToString(),
                                                        TipoCambioCadena = caso.TipoCambio.ToString(),
                                                        Orden = cabeceraDesplegada.Orden,
                                                        OrdenMostrar = ordenMostraDetalle,
                                                        Nivel = Util.Constantes.Generales.Numeric.Cuatro,
                                                        Desplegado = Util.Constantes.Generales.Numeric.Cero
                                                    });


                                                    ordenMostraDetalle++;

                                                   var casosRestriccion = listaCasosPersonal.Where(x => x.Level == "5" &&
                                                   x.idLineaNegocio == item.idLineaNegocio &&
                                                   x.idCliente == cliente.IdCliente &&
                                                   x.IdOportunidad == oportunidad.IdOportunidad &&
                                                   x.NumeroCaso == Convert.ToInt32(string.IsNullOrWhiteSpace(caso.NumeroCaso) ? "0" : 
                                                                   caso.NumeroCaso) 
                                                   ).FirstOrDefault();
                                                   
                                                   
                                                   if (casosRestriccion != null)
                                                   {
                                                        var cabeceraDesplegada4 = resultado.Where(x =>
                                                        x.IdLineaNegocio == item.idLineaNegocio
                                                        && x.IdCliente == cliente.IdCliente
                                                        && x.IdOportunidad == oportunidad.IdOportunidad
                                                        && int.Parse(string.IsNullOrWhiteSpace(x.NumeroCaso) ? "0" : x.NumeroCaso) == 
                                                        Convert.ToInt32(string.IsNullOrWhiteSpace(caso.NumeroCaso) ? "0" : caso.NumeroCaso)
                                                        && x.Nivel == 4).FirstOrDefault();

                                                        if (cabeceraDesplegada4 != null)
                                                        {
                                                            cabeceraDesplegada4.Desplegado = Util.Constantes.Generales.Numeric.Uno;
                                                        }

                                                        resultado.Add(new IndicadorLineasNegocioListadoDtoResponse
                                                        {
                                                            IdLineaNegocio = item.idLineaNegocio,
                                                            LineaNegocio = string.Empty,
                                                            IdCliente = cliente.IdCliente,
                                                            CantidadClientesCadena = string.Empty,
                                                            IdOportunidad = oportunidad.IdOportunidad,
                                                            OportunidadesAbiertasClienteCadena = string.Empty,
                                                            NumeroCaso = caso.NumeroCaso,
                                                            IngresoTotalCadena = string.Empty,
                                                            IngresoCapexCadena = string.Empty,
                                                            CostosDirectosCadena = string.Empty,
                                                            OfertasTrabajadasClienteCadena = string.Empty,
                                                            OfertasGanadasClienteCadena = string.Empty,
                                                            FaseOportunidad = string.Empty,
                                                            ProbabilidadExito = string.Empty,
                                                            FechaCierreEstimadaCadena = string.Empty,
                                                            OidbaCadena = string.Empty,
                                                            FlujoCajaCadena = string.Empty,
                                                            FullContractValueNetoCadena = string.Empty,
                                                            PayBack = string.Empty,
                                                            PagoUnicoDolares = string.Empty,
                                                            PagoRecurrenteDolares = string.Empty,
                                                            MesesPagoRecurrenteCadena = string.Empty,
                                                            TipoCambioCadena = string.Empty,
                                                            Orden = cabeceraDesplegada.Orden,
                                                            OrdenMostrar = ordenMostraDetalle,
                                                            Nivel = Util.Constantes.Generales.Numeric.Cinco,
                                                            Desplegado = Util.Constantes.Generales.Numeric.Cero
                                                        });


                                                        ordenMostraDetalle++;

                                                    }
                                                }


                                                resultado.Add(new IndicadorLineasNegocioListadoDtoResponse
                                                {
                                                    IdLineaNegocio = item.idLineaNegocio,
                                                    LineaNegocio = string.Empty,
                                                    IdCliente = cliente.IdCliente,
                                                    CantidadClientesCadena = string.Empty,
                                                    IdOportunidad = oportunidad.IdOportunidad,
                                                    OportunidadesAbiertasClienteCadena = string.Empty,
                                                    NumeroCaso = string.Empty,
                                                    IngresoTotalCadena = TitulosDetalleOportunidades.EvolutivoOfertas,
                                                    IngresoCapexCadena = string.Empty,
                                                    CostosDirectosCadena = string.Empty,
                                                    OfertasTrabajadasClienteCadena = string.Empty,
                                                    OfertasGanadasClienteCadena = string.Empty,
                                                    FaseOportunidad = string.Empty,
                                                    ProbabilidadExito = string.Empty,
                                                    FechaCierreEstimadaCadena = string.Empty,
                                                    OidbaCadena = string.Empty,
                                                    FlujoCajaCadena = string.Empty,
                                                    FullContractValueNetoCadena = string.Empty,
                                                    PayBack = string.Empty,
                                                    PagoUnicoDolares = string.Empty,
                                                    PagoRecurrenteDolares = string.Empty,
                                                    MesesPagoRecurrenteCadena = string.Empty,
                                                    TipoCambioCadena = string.Empty,
                                                    Orden = cabeceraDesplegada.Orden,
                                                    OrdenMostrar = ordenMostraDetalle,
                                                    Nivel = Util.Constantes.Generales.Numeric.Cuatro,
                                                    Desplegado = Util.Constantes.Generales.Numeric.Cero
                                                });

                                                ordenMostraDetalle++;

                                                resultado.Add(new IndicadorLineasNegocioListadoDtoResponse
                                                {
                                                    IdLineaNegocio = item.idLineaNegocio,
                                                    LineaNegocio = string.Empty,
                                                    IdCliente = cliente.IdCliente,
                                                    CantidadClientesCadena = string.Empty,
                                                    IdOportunidad = oportunidad.IdOportunidad,
                                                    OportunidadesAbiertasClienteCadena = string.Empty,
                                                    NumeroCaso = string.Empty,
                                                    IngresoTotalCadena = string.Empty,
                                                    IngresoCapexCadena = string.Empty,
                                                    CostosDirectosCadena = string.Empty,
                                                    OfertasTrabajadasClienteCadena = string.Empty,
                                                    OfertasGanadasClienteCadena = string.Empty,
                                                    FaseOportunidad = string.Empty,
                                                    ProbabilidadExito = string.Empty,
                                                    FechaCierreEstimadaCadena = string.Empty,
                                                    OidbaCadena = string.Empty,
                                                    FlujoCajaCadena = string.Empty,
                                                    FullContractValueNetoCadena = string.Empty,
                                                    PayBack = string.Empty,
                                                    PagoUnicoDolares = string.Empty,
                                                    PagoRecurrenteDolares = string.Empty,
                                                    MesesPagoRecurrenteCadena = string.Empty,
                                                    TipoCambioCadena = string.Empty,
                                                    Orden = cabeceraDesplegada.Orden,
                                                    OrdenMostrar = ordenMostraDetalle,
                                                    Nivel = Util.Constantes.Generales.Numeric.Cuatro,
                                                    Desplegado = Util.Constantes.Generales.Numeric.Cero,
                                                    GraficoEvolutivo = Util.Constantes.Generales.Numeric.Uno
                                                });

                                                ordenMostraDetalle++;


                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return resultado;  
          }
    }
}

