﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.AgenteServicio;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Seguridad
{
    public class UsuarioPerfilBl : IUsuarioPerfilBl
    {
        public ProcesoResponse ActualizarEstadoUsuarioPerfil(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest)
        {  
            var resultado = AgenteServicioUsuarioPerfil.ActualizarEstadoUsuarioPerfil(usuarioPerfilDtoRequest);

            return new ProcesoResponse
            {
                TipoRespuesta = resultado.TipoRespuesta, 
                Mensaje = resultado.Mensaje
            };
        }


        public List<UsuarioPerfilDtoResponse> ListarPerfilesAsignados(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest)
        {
            var consultaPerfiles= AgenteServicioUsuarioPerfil.ListarPerfilesAsignados(usuarioPerfilDtoRequest);

            var resultado = (from data in consultaPerfiles
                             select new UsuarioPerfilDtoResponse
                             {
                                 IdPerfil=data.IdPerfil,
                                 Perfil= data.Perfil,
                                 Estado=data.Estado,
                                 IdUsuarioSistemaEmpresa=data.IdUsuarioSistemaEmpresa,
                                 TotalRegistros =data.Pagina.Total,
                                 IdEstadoRegistro = data.IdEstadoRegistro
                             }).ToList();

            return resultado;
        }

        public List<ListaDtoResponse> ListarPerfilesPorSistema()
        {
            var consultaPerfiles = AgenteServicioUsuarioPerfil.ListarPerfilesPorSistema();

            var resultado = (from data in consultaPerfiles
                             select new ListaDtoResponse
                             {
                                 Codigo = Convert.ToString(data.IdPerfil),
                                 Descripcion = data.Nombre_Perfil
                             }).ToList();

            return resultado;
        }

        public ProcesoResponse RegistrarUsuarioPerfil(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest)
        {  
            var resultado = AgenteServicioUsuarioPerfil.RegistrarUsuarioPerfil(usuarioPerfilDtoRequest);

            return new ProcesoResponse
            {
                TipoRespuesta = resultado.TipoRespuesta,
                Mensaje = resultado.Mensaje
            };
        }
    }
}
