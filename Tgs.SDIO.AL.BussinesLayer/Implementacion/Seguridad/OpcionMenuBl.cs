﻿using System.Collections.Generic;
using System.Data;
using System.Linq; 
using Tgs.SDIO.AL.AgenteServicio;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Seguridad
{
    public class OpcionMenuBl : IOpcionMenuBl
    {
        public List<OpcionMenuResponse> ListarOpcionesUsuario(int idUsuarioEmpresa)
        {
            var dataSet = AgenteServicioOpcionMenu.ListarOpcionesUsuario(idUsuarioEmpresa);

            if (dataSet.Tables[0] == null || dataSet.Tables[0].Rows.Count <= 0)
            {
                return null;
            }

            var menuRais = (from DataRow dataRow in dataSet.Tables[0].Rows
                select new OpcionMenuDto()
                {
                    IdOpcion = dataRow["IdOpcion"].ToString(),
                    CodigoOpcion = dataRow["Codigo_Opcion"].ToString(),
                    NombreOpcion = dataRow["Nombre_Opcion"].ToString(),
                    DescripcionOpcion = dataRow["Descripcion_Opcion"].ToString(),
                    UrlPagina = dataRow["URL_Pagina"].ToString(),
                    TipoOpcion = dataRow["Tipo_Opcion"].ToString(),
                    FlagOpcion = dataRow["Flag_Opcion_Critica"].ToString(),
                    NumeroOrden = dataRow["Numero_Orden"].ToString(),
                    IdOpcionPadre = dataRow["IdOpcion_Padre"].ToString(),
                    NumeroNivel = dataRow["Numero_Nivel"].ToString(),
                    OpcionCodigo = dataRow["opcic_Codigo_Control"].ToString(),
                    CodigoPerfil = dataRow["perfc_Codigo_Perfil"].ToString()
                }).ToList();

            var menuPadre = menuRais.Where(x => x.TipoOpcion.Equals("1")).ToList();

            var menuPadreUnicos = (from m in menuPadre
                group m by m.NombreOpcion into g
                select new
                {
               
                    NombreOpcion = g.Key,
                    menu = g.FirstOrDefault(),
                 
                }).Select(w => new OpcionMenuDto
                {   DescripcionOpcion=w.menu.DescripcionOpcion,
                    NombreOpcion = w.NombreOpcion,
                    IdOpcion = w.menu.IdOpcion,
                    UrlPagina = w.menu.UrlPagina,
                    CodigoPerfil = w.menu.CodigoPerfil,
                    NumeroOrden = w.menu.NumeroOrden
                }).OrderBy(y => int.Parse(y.NumeroOrden)).ToList();


            var resultado = new List<OpcionMenuResponse>();

            foreach (var item in menuPadreUnicos)
            {
                var menu = new OpcionMenuResponse
                {
                    OpcionMenuDto = new OpcionMenuDto
                    {
                        DescripcionOpcion=item.DescripcionOpcion,
                        NombreOpcion = item.NombreOpcion,
                        IdOpcion = item.IdOpcion,
                        UrlPagina = item.UrlPagina,
                        CodigoPerfil = item.CodigoPerfil
                    }
                };


                var menuHijo = menuRais.Where(x => (x.TipoOpcion.Equals("2") || x.TipoOpcion.Equals("3"))
                                                   && x.IdOpcionPadre.Equals(item.IdOpcion))
                    .Select(x => new { x.NombreOpcion, x.IdOpcion, x.UrlPagina, x.TipoOpcion,x.NumeroOrden ,x.DescripcionOpcion}).Distinct().OrderBy(y => int.Parse(y.NumeroOrden));

                menu.OpcionMenuDtoLista = new List<OpcionMenuDto>();

                foreach (var itemHijo in menuHijo)
                {
                    if (itemHijo.TipoOpcion.Equals("3"))
                    {
                        var opcionSubMenuDto = new OpcionMenuDto
                        { 
                            NombreOpcion = itemHijo.NombreOpcion,
                            DescripcionOpcion = itemHijo.DescripcionOpcion,
                            IdOpcion = itemHijo.IdOpcion,
                            UrlPagina = itemHijo.UrlPagina,
                            OpcionSubMenuDto = new List<OpcionSubMenuDto>()
                        };

                        var subMenuHijo = menuRais.Where(x => x.TipoOpcion.Equals("2")
                                                              && x.IdOpcionPadre.Equals(itemHijo.IdOpcion))
                            .Select(y => new { y.NombreOpcion, y.IdOpcion, y.UrlPagina, y.TipoOpcion,y.NumeroOrden }).Distinct().OrderBy(w => int.Parse(w.NumeroOrden)).ToList();

                        foreach (var subItemHijo in subMenuHijo)
                        {
                            opcionSubMenuDto.OpcionSubMenuDto.Add(
                                new OpcionSubMenuDto
                                {   
                                    NombreOpcion = subItemHijo.NombreOpcion,
                                    DescripcionOpcion = itemHijo.DescripcionOpcion,
                                    IdOpcion = subItemHijo.IdOpcion,
                                    UrlPagina = subItemHijo.UrlPagina
                                }
                            );
                        }

                        menu.OpcionMenuDtoLista.Add(opcionSubMenuDto);
                    }
                    else
                    {
                        if (itemHijo.TipoOpcion.Equals("2"))
                        {
                            menu.OpcionMenuDtoLista.Add(
                                new OpcionMenuDto
                                {
                                    NombreOpcion = itemHijo.NombreOpcion,
                                    DescripcionOpcion=itemHijo.DescripcionOpcion,
                                    IdOpcion = itemHijo.IdOpcion,
                                    UrlPagina = itemHijo.UrlPagina,
                                    OpcionSubMenuDto = new List<OpcionSubMenuDto>()
                                }
                            );
                        }
                    }
                }

                resultado.Add(menu);
            }

            return resultado;
        }
    }
}
