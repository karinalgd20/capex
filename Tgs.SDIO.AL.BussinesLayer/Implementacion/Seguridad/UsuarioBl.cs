﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using Tgs.SDIO.AL.AgenteServicio;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Mensajes.PlantaExterna;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Seguridad
{
    public class UsuarioBl : IUsuarioBl
    {
        private readonly IPerfilBl iPerfilBl;
        private readonly IRecursoBl iRecursoBl;
        public UsuarioBl(IPerfilBl perfilBl, IRecursoBl recursoBl)
        {
            iPerfilBl = perfilBl;
            iRecursoBl = recursoBl;
        }

        public UsuarioDtoResponse ObtenerUsuarioPorLogin(string login)
        {
            var usuarioProxy = AgenteServicioUsuario.ObtenerUsuarioPorLogin(login);

            return new UsuarioDtoResponse
            {
                Apellidos = usuarioProxy.Apellidos_Usuario,
                CodigoCip = usuarioProxy.CIP_Usuario,
                CorreoElectronico = usuarioProxy.Correo_Electronico,
                FechaCaducidad = usuarioProxy.Fecha_Caducidad,
                FechaCambioPassword = usuarioProxy.Fecha_Caducidad,
                FechaModoficacion = usuarioProxy.FH_Modifica,
                FechaRegistro = usuarioProxy.FH_Registro,
                FechaUltimoAcceso = usuarioProxy.FH_Ultimo_Acceso,
                FlagSolicitarCambioClave = usuarioProxy.Flag_Solicitar_Cambio_Clave,
                FlagUsuarioBloqueado = usuarioProxy.Flag_Usuario_Bloqueado,
                IdEmpresa = usuarioProxy.IdEmpresa,
                IdEstadoRegistro = usuarioProxy.IdEstadoRegistro,
                IdUsuario = usuarioProxy.IdUsuario,
                IdUsuarioModificacion = usuarioProxy.IdUsuario_Modifica,
                IdUsuarioRegistro = usuarioProxy.IdUsuario_Registro,
                IdUsuarioSuperior = usuarioProxy.IdUsuario_Superior,
                IntentosFallidos = usuarioProxy.Numero_Intentos_Fallidos,
                Login = usuarioProxy.Login,
                LoginWindows = usuarioProxy.Login_Windows,
                Nombres = usuarioProxy.Nombre_Usuario,
                Password = usuarioProxy.Password,
                TipoUsuario = usuarioProxy.Tipo_Usuario
            };
        }

        public UsuarioDtoResponse ObtenerUsuarioPorId(int idUsuario)
        {
            var usuarioProxy = AgenteServicioUsuario.ObtenerUsuarioPorId(idUsuario);

            var datosRecurso = iRecursoBl.ObtenerRecursoPorIdUsuarioRais(idUsuario);

            return new UsuarioDtoResponse
            {
                Apellidos = usuarioProxy.Apellidos_Usuario,
                CodigoCip = usuarioProxy.CIP_Usuario,
                CorreoElectronico = usuarioProxy.Correo_Electronico,
                FechaCaducidad = usuarioProxy.Fecha_Caducidad,
                FechaCambioPassword = usuarioProxy.Fecha_Caducidad,
                FechaModoficacion = usuarioProxy.FH_Modifica,
                FechaRegistro = usuarioProxy.FH_Registro,
                FechaUltimoAcceso = usuarioProxy.FH_Ultimo_Acceso,
                FlagSolicitarCambioClave = usuarioProxy.Flag_Solicitar_Cambio_Clave,
                FlagUsuarioBloqueado = usuarioProxy.Flag_Usuario_Bloqueado,
                IdEmpresa = usuarioProxy.IdEmpresa,
                IdEstadoRegistro = usuarioProxy.IdEstadoRegistro,
                IdUsuario = usuarioProxy.IdUsuario,
                IdUsuarioModificacion = usuarioProxy.IdUsuario_Modifica,
                IdUsuarioRegistro = usuarioProxy.IdUsuario_Registro,
                IdUsuarioSuperior = usuarioProxy.IdUsuario_Superior,
                IntentosFallidos = usuarioProxy.Numero_Intentos_Fallidos,
                Login = usuarioProxy.Login,
                LoginWindows = usuarioProxy.Login_Windows,
                Nombres = usuarioProxy.Nombre_Usuario,
                Password = usuarioProxy.Password,
                NombresCompletos = usuarioProxy.Nombre_Usuario + " " + usuarioProxy.Apellidos_Usuario,
                TipoUsuario = usuarioProxy.Tipo_Usuario,
                IdArea = datosRecurso == null ? 0 : datosRecurso.IdArea,
                IdCargo = datosRecurso == null ? 0 : datosRecurso.IdCargo
            };
        }

        public UsuarioDtoResponse ValidarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            var dataSet = AgenteServicioUsuario.ValidarUsuario(usuarioDtoRequest);

            UsuarioDtoResponse seguridad = new UsuarioDtoResponse();

            if (dataSet.Tables[0].Rows.Count >= 0)
            {
                if (dataSet.Tables[0].Rows[0]["CodigoResultado"].ToString().Equals("0"))
                {
                    seguridad = new UsuarioDtoResponse
                    {
                        Login = usuarioDtoRequest.Login.Trim().ToUpper(),
                        Usuariosistema =
                            !string.IsNullOrEmpty(dataSet.Tables[0].Rows[0][5].ToString())
                                ? Convert.ToInt16(dataSet.Tables[0].Rows[0][5].ToString())
                                : Convert.ToInt16(dataSet.Tables[0].Rows[0][3].ToString()),
                        CodigoError = 0
                    };
                }
                else
                {
                    seguridad = new UsuarioDtoResponse
                    {
                        MensajeError = dataSet.Tables[0].Rows[0][1].ToString(),
                        CodigoError = -1
                    };
                }
            }

            return seguridad;
        }

        public string EnvioClaveUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            usuarioDtoRequest.CorreoElectronico = Configuracion.CorreoSalida;
            usuarioDtoRequest.UsuarioEnvia = Configuracion.NombreSistemaCorreoSalida;
            return AgenteServicioUsuario.EnvioClaveUsuario(usuarioDtoRequest);
        }

        public List<EntidadDtoResponse> ObtenerAmbitoUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            var entidadProxy = AgenteServicioUsuario.ObtenerAmbitoUsuario(usuarioDtoRequest);

            var listado = entidadProxy.Select(item => new EntidadDtoResponse
            {
                CodigoEntidad = item.CodigoEntidad,
                DescripcionEntidad = item.DescripcionEntidad,
                IdEntidad = item.IdEntidad,
                NombreEntidad = item.NombreEntidad,
                TipoEntidad = item.TipoEntidad
            }).ToList();

            return listado;
        }

        public ProcesoResponse RegistrarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            ProcesoResponse registrarRecurso;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                var resultadoRais = AgenteServicioUsuario.RegistrarUsuario(usuarioDtoRequest);

                if (resultadoRais.TipoRespuesta == 1)
                {
                    var datosRecurso = new RecursoDtoRequest
                    {
                        Nombre = string.Concat(usuarioDtoRequest.Nombres, ' ', usuarioDtoRequest.Apellidos),
                        IdEstado = usuarioDtoRequest.IdEstado,
                        IdUsuarioEdicion = usuarioDtoRequest.IdUsuarioEdicion,
                        FechaEdicion = usuarioDtoRequest.FechaEdicion,
                        IdArea = usuarioDtoRequest.IdArea,
                        IdCargo = usuarioDtoRequest.IdCargo,
                        IdUsuarioRais = resultadoRais.Id,
                        IdUsuario = usuarioDtoRequest.IdUsuario,
                        TipoRecurso = usuarioDtoRequest.TipoRecurso,
                        Email = usuarioDtoRequest.CorreoElectronico,
                        NombreInterno = string.Concat(usuarioDtoRequest.Nombres, ' ', usuarioDtoRequest.Apellidos)
                    };

                    registrarRecurso = iRecursoBl.RegistrarRecurso(datosRecurso);
                }
                else
                {
                    registrarRecurso = new ProcesoResponse
                    {
                        TipoRespuesta = Proceso.Invalido,
                        Id = resultadoRais.Id,
                        Mensaje = resultadoRais.Mensaje
                    };
                }

                scope.Complete();
            }

            return registrarRecurso;
        }

        public UsuarioDtoResponse CambiarPassword(UsuarioDtoRequest usuarioDtoRequest)
        {
            var dataSet = AgenteServicioUsuario.CambiarPassword(usuarioDtoRequest);

            UsuarioDtoResponse seguridad = new UsuarioDtoResponse();

            if (dataSet.Tables[0].Rows.Count >= 0)
            {
                if (dataSet.Tables[0].Rows[0]["CodigoResultado"].ToString().Equals("0"))
                {
                    seguridad = new UsuarioDtoResponse
                    {
                        MensajeError = dataSet.Tables[0].Rows[0][1].ToString(),
                        CodigoError = 0
                    };
                }
                else
                {
                    seguridad = new UsuarioDtoResponse
                    {
                        MensajeError = dataSet.Tables[0].Rows[0][1].ToString(),
                        CodigoError = -1
                    };
                }
            }

            return seguridad;
        }

        public UsuarioDtoResponse ObtenerUsuarioPerfil(UsuarioDtoRequest usuarioDtoRequest)
        {
            var perfilesRais = iPerfilBl.ObtenerPerfiles(usuarioDtoRequest.IdUsuarioSistema);

            var usuarioRais = ObtenerUsuarioPorLogin(usuarioDtoRequest.Login);

            usuarioRais.Login = usuarioDtoRequest.Login;

            usuarioRais.IdUsuarioSistema = usuarioDtoRequest.IdUsuarioSistema;

            usuarioRais.PerfilDtoResponseLista = perfilesRais;

            var perfilPlantaExterna = perfilesRais.Where(x => x.CodigoPerfil.Equals(TipoPerfil.Preventa) ||
            x.CodigoPerfil.Equals(TipoPerfil.JefeProyecto) ||
            x.CodigoPerfil.Equals(TipoPerfil.LiderJefeProyecto) ||
            x.CodigoPerfil.Equals(TipoPerfil.GestorPlantaExterna));

            if (perfilPlantaExterna != null)
            {
                var obtenerUsuario = iRecursoBl.ObtenerRecursoPorIdUsuarioRais(usuarioRais.IdUsuario);

                if (obtenerUsuario != null)
                {
                    usuarioRais.JefeProyecto = perfilPlantaExterna.Where(z => z.CodigoPerfil.Equals(TipoPerfil.JefeProyecto)).Any() ? obtenerUsuario.NombreInterno : MensajesGeneralPlantaExterna.NoAsignado;
                    usuarioRais.LiderJefeProyecto = perfilPlantaExterna.Where(p => p.CodigoPerfil.Equals(TipoPerfil.LiderJefeProyecto)).Any() ? obtenerUsuario.NombreInterno : MensajesGeneralPlantaExterna.NoAsignado;
                    usuarioRais.NombrePreventa = perfilPlantaExterna.Where(r => r.CodigoPerfil.Equals(TipoPerfil.Preventa)).Any() ? obtenerUsuario.NombreInterno : MensajesGeneralPlantaExterna.NoAsignado;
                }
            }
            else
            {
                usuarioRais.JefeProyecto = string.Empty;
                usuarioRais.LiderJefeProyecto = string.Empty;
                usuarioRais.NombrePreventa = string.Empty;
            }

            return usuarioRais;
        }

        public List<UsuarioDtoResponse> ObtenerUsuariosPorPerfil(UsuarioDtoRequest usuarioDtoRequest)
        {
            var dataset = AgenteServicioUsuario.ObtenerUsuariosPorPerfil(usuarioDtoRequest);


            var listado = (from DataRow dataRow in dataset.Tables[0].Rows
                           select new UsuarioDtoResponse
                           {
                               Apellidos = dataRow["Apellidos_Usuario"].ToString(),
                               Nombres = dataRow["Nombre_Usuario"].ToString(),
                               CodigoCip = dataRow["CIP_Usuario"].ToString(),
                               IdUsuario = Convert.ToInt32(dataRow["IdUsuario"]),
                               Login = dataRow["Login"].ToString(),
                               TipoUsuario = Convert.ToInt32(dataRow["Tipo_Usuario"]),
                               NombresCompletos = dataRow["Nombre_Usuario"] + " " + dataRow["Apellidos_Usuario"],
                               CorreoElectronico = dataRow["Correo_Electronico"].ToString(),
                           }).ToList();
            return listado;
        }


        public ProcesoResponse ActualizarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            ProcesoResponse registrarRecurso;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                var resultadoRais = AgenteServicioUsuario.ActualizarUsuario(usuarioDtoRequest);

                if (resultadoRais.TipoRespuesta == 1)
                {
                    var obtenerUsuario = iRecursoBl.ObtenerRecursoPorIdUsuarioRais(usuarioDtoRequest.IdUsuario);

                    var recurso = new RecursoDtoRequest
                    {
                        IdRecurso = obtenerUsuario.IdRecurso,
                        Nombre = string.Concat(usuarioDtoRequest.Nombres, ' ', usuarioDtoRequest.Apellidos),
                        IdEstado = usuarioDtoRequest.IdEstado,
                        IdUsuarioEdicion = usuarioDtoRequest.IdUsuarioEdicion,
                        FechaEdicion = usuarioDtoRequest.FechaEdicion,
                        IdArea = usuarioDtoRequest.IdArea,
                        IdCargo = usuarioDtoRequest.IdCargo
                    };

                    registrarRecurso = iRecursoBl.ActualizarRecursoUsuario(recurso);
                }
                else
                {
                    registrarRecurso = new ProcesoResponse
                    {
                        TipoRespuesta = Proceso.Invalido,
                        Mensaje = resultadoRais.Mensaje
                    };
                }

                scope.Complete();
            }

            return registrarRecurso;
        }

        public ProcesoResponse ActualizarEstadoUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            var resultado = AgenteServicioUsuario.ActualizarEstadoUsuario(usuarioDtoRequest);

            return new ProcesoResponse
            {
                TipoRespuesta = resultado.TipoRespuesta,
                Id = resultado.Id,
                Mensaje = resultado.Mensaje,
                Detalle = resultado.Detalle
            };
        }

        public List<UsuarioDtoResponse> ListarUsuariosPaginado(UsuarioDtoRequest usuarioDtoRequest)
        {
            var consultaUsuarios = AgenteServicioUsuario.ListarUsuariosPaginado(usuarioDtoRequest);

            var resultado = (from data in consultaUsuarios
                             select new UsuarioDtoResponse
                             {
                                 IdUsuario = data.IdUsuario,
                                 Apellidos = data.Apellidos_Usuario,
                                 Nombres = data.Nombre_Usuario,
                                 Login = data.Login,
                                 FlagUsuarioBloqueado = data.Flag_Usuario_Bloqueado,
                                 IdEstadoRegistro = data.IdEstadoRegistro,
                                 EstadoRegistro = data.NombreEstadoRegistro,
                                 TotalRegistros = data.Pagina.Total,
                                 IdUsuarioSistemaEmpresa = data.IdUsuarioSistemaEmpresa,
                             }).ToList();

            return resultado;
        }

        public List<ListaDtoResponse> ListarUsuariosFiltro(UsuarioDtoRequest usuarioDtoRequest)
        {
            usuarioDtoRequest.CodigoSistema = Configuracion.CodigoAplicacion;
            var consultaUsuarios = AgenteServicioUsuario.ListarUsuariosFiltro(usuarioDtoRequest);

            var resultado = (from data in consultaUsuarios
                             select new ListaDtoResponse
                             {
                                 Codigo = Convert.ToString(data.IdUsuario),
                                 Descripcion = data.Nombre_Usuario
                             }).ToList();

            return resultado;
        }

    }
}
