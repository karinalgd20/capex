﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Transactions;
using System.Linq;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using System.Collections.Generic;
using Tgs.SDIO.Util.Funciones;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
    public class TiempoAtencionEquipoBl : ITiempoAtencionEquipoBl
    {
        readonly ITiempoAtencionEquipoDal itiempoAtencionEquipoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public TiempoAtencionEquipoBl(ITiempoAtencionEquipoDal rolRecursoDal)
        {
            itiempoAtencionEquipoDal = rolRecursoDal;
        }

        public TiempoAtencionEquipoPaginadoDtoResponse ListadoTiempoAtencionEquipoPaginado(TiempoAtencionEquipoDtoRequest request)
        {
            return itiempoAtencionEquipoDal.ListadoTiempoAtencionEquipoPaginado(request);
        }

        public TiempoAtencionEquipoDtoResponse ObtenerTiempoAtencionEquipoPorId(TiempoAtencionEquipoDtoRequest request)
        {
            var query = itiempoAtencionEquipoDal.GetFilteredAsNoTracking(x => x.IdTiempoAtencion == request.IdTiempoAtencion && (x.IdEstado == Activo || x.IdEstado == Inactivo)).ToList();
            return query.Select(x => new TiempoAtencionEquipoDtoResponse
            {
                IdTiempoAtencion = x.IdTiempoAtencion,
                IdOportunidad = x.IdOportunidad,
                IdCaso = x.IdCaso,
                IdOportunidadSF = x.IdOportunidadSF,
                IdCasoSF = x.IdCasoSF,
                IdSisego = x.IdSisego,
                IdSisegoSW = x.IdSisegoSW,                
                IdEquipoTrabajo = x.IdEquipoTrabajo,
                FechaInicio = x.FechaInicio,
                FechaFin = x.FechaFin,
                strFechaInicio = x.FechaInicio != null ? Funciones.FormatoFecha(x.FechaInicio.Value) : "",
                strFechaFin = x.FechaFin != null ? Funciones.FormatoFecha(x.FechaFin.Value) : "",
                DiasGestion = x.DiasGestion,
                Observaciones = x.Observaciones,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion
            }).FirstOrDefault();
        }

        public ProcesoResponse ActualizarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request)
        {
            var tiempoAtencionAntigua = ObtenerTiempoAtencionEquipoPorId(request);
            var tiempoAtencion = new TiempoAtencionEquipo()
            {
                IdTiempoAtencion = request.IdTiempoAtencion,
                IdOportunidad = request.IdOportunidad,
                IdCaso = request.IdCaso,
                IdOportunidadSF = request.IdOportunidadSF,
                IdCasoSF = request.IdCasoSF,
                IdSisego = request.IdSisego,
                IdSisegoSW = request.IdSisegoSW,
                IdEquipoTrabajo = request.IdEquipoTrabajo,
                FechaInicio = request.FechaInicio,
                FechaFin = request.FechaFin,
                DiasGestion = request.DiasGestion,
                Observaciones = request.Observaciones,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = tiempoAtencionAntigua.IdUsuarioCreacion,
                FechaCreacion = tiempoAtencionAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now,
            };
            itiempoAtencionEquipoDal.Modify(tiempoAtencion);
            itiempoAtencionEquipoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }

        public ProcesoResponse RegistrarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request)
        {
            var tiempoAtencion = new TiempoAtencionEquipo()
            {
                IdOportunidad = request.IdOportunidad,
                IdCaso = request.IdCaso,
                IdOportunidadSF = request.IdOportunidadSF,
                IdCasoSF = request.IdCasoSF,
                IdSisego = request.IdSisego,
                IdSisegoSW = request.IdSisegoSW,
                IdEquipoTrabajo = request.IdEquipoTrabajo,
                FechaInicio = request.FechaInicio,
                FechaFin = request.FechaFin,
                DiasGestion = request.DiasGestion,
                Observaciones = request.Observaciones,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = request.IdUsuario,
                FechaCreacion = DateTime.Now
            };
            itiempoAtencionEquipoDal.Add(tiempoAtencion);
            itiempoAtencionEquipoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;
        }

        public ProcesoResponse EliminarTiempoAtencionEquipo(TiempoAtencionEquipoDtoRequest request)
        {
            var areaAntigua = ObtenerTiempoAtencionEquipoPorId(request);
            var tiempoAtencion = new TiempoAtencionEquipo()
            {
                IdTiempoAtencion = request.IdTiempoAtencion,
                IdOportunidad = areaAntigua.IdOportunidad,
                IdCaso = areaAntigua.IdCaso,
                IdOportunidadSF = areaAntigua.IdOportunidadSF,
                IdCasoSF = areaAntigua.IdCasoSF,
                IdSisego = areaAntigua.IdSisego,
                IdSisegoSW = areaAntigua.IdSisegoSW,
                IdEquipoTrabajo = areaAntigua.IdEquipoTrabajo,
                FechaInicio = areaAntigua.FechaInicio,
                FechaFin = areaAntigua.FechaFin,
                DiasGestion = areaAntigua.DiasGestion,
                Observaciones = areaAntigua.Observaciones,
                IdEstado = Eliminado,
                IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                FechaCreacion = areaAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now
            };
            itiempoAtencionEquipoDal.Modify(tiempoAtencion);
            itiempoAtencionEquipoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;

            return respuesta;
        }
    }
}
