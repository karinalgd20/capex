﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System.Collections.Generic;
using System;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
    public class OportunidadSTBl : IOportunidadSTBl
    {
        readonly IOportunidadSTDal ioportunidadSTDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadSTBl(IOportunidadSTDal oportunidadSTDal)
        {
            ioportunidadSTDal = oportunidadSTDal;
        }

        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalObservacionPaginado(OportunidadSTDtoRequest request)
        {
            return ioportunidadSTDal.ListaOportunidadSTModalObservacionPaginado(request);
        }

        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalRecursoPaginado(OportunidadSTDtoRequest request)
        {
            return ioportunidadSTDal.ListaOportunidadSTModalRecursoPaginado(request);
        }

        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTMasivoPaginado(OportunidadSTDtoRequest request)
        {
            return ioportunidadSTDal.ListaOportunidadSTMasivoPaginado(request);
        }

        public OportunidadSTSeguimientoPreventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPreventaPaginado(OportunidadSTSeguimientoPreventaDtoRequest request)
        {
            return ioportunidadSTDal.ListaOportunidadSTSeguimientoPreventaPaginado(request);
        }

        public OportunidadSTSeguimientoPostventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPostventaPaginado(OportunidadSTSeguimientoPostventaDtoRequest request)
        {
            return ioportunidadSTDal.ListaOportunidadSTSeguimientoPostventaPaginado(request);
        }


        public OportunidadSTDtoResponse ObtenerOportunidadSTPorId(OportunidadSTDtoRequest request)
        {
            var query = ioportunidadSTDal.GetFilteredAsNoTracking(x => x.IdOportunidad == request.IdOportunidad && (x.IdEstado == Activo || x.IdEstado == Inactivo)).ToList();
            return query.Select(x => new OportunidadSTDtoResponse
            {
                IdOportunidad = x.IdOportunidad,
                IdOportunidadPadre = x.IdOportunidadPadre,
                IdOportunidadSF = x.IdOportunidadSF,
                IdOportunidadAux = x.IdOportunidadAux,
                Descripcion = x.Descripcion,
                IdTipoOportunidad = x.IdTipoOportunidad,
                IdMotivoOportunidad = x.IdMotivoOportunidad,
                IdTipoEntidadCliente = x.IdTipoEntidadCliente,
                IdCliente = x.IdCliente,
                IdSector = x.IdSector,
                IdSegmentoNegocio = x.IdSegmentoNegocio,
                IdTipoCicloVenta = x.IdTipoCicloVenta,
                IdTipoCicloImplementacion = x.IdTipoCicloImplementacion,
                Prioridad = x.Prioridad,
                ProbalidadExito = x.ProbalidadExito,
                PorcentajeCierre = x.PorcentajeCierre,
                PorcentajeCheckList = x.PorcentajeCheckList,
                IdFase = x.IdFase,
                IdEtapa = x.IdEtapa,
                IdLineaNegocio = x.IdLineaNegocio,                
                FlgCapexMayor = x.FlgCapexMayor,
                ImporteCapex = x.ImporteCapex,
                IdMoneda = x.IdMoneda,
                ImporteFCV = x.ImporteFCV,
                FechaApertura = x.FechaApertura,
                FechaCierre = x.FechaCierre,
                IdEstadoOportunidad = x.IdEstadoOportunidad,
                IdRecursoComercial = x.IdRecursoComercial,
                IdRecursoPreventa = x.IdRecursoPreventa,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion,
                IdFuncionPropietario = x.IdFuncionPropietario
            }).FirstOrDefault();
        }

        public ProcesoResponse ActualizarOportunidadSTModalPreventa(OportunidadSTDtoRequest request)
        {
            var dataAntigua = ObtenerOportunidadSTPorId(request);
            var oportunidad = new OportunidadST()
            {
                IdOportunidad = request.IdOportunidad,
                IdOportunidadPadre = dataAntigua.IdOportunidadPadre,
                IdOportunidadSF = request.IdOportunidadSF,
                IdOportunidadAux = dataAntigua.IdOportunidadAux,
                Descripcion = request.DescripcionOportunidad,
                IdTipoOportunidad = request.IdTipoOportunidad,
                IdFuncionPropietario = dataAntigua.IdFuncionPropietario,
                IdMotivoOportunidad = request.IdMotivoOportunidad,
                IdTipoEntidadCliente = dataAntigua.IdTipoEntidadCliente,
                IdCliente = dataAntigua.IdCliente,
                IdSector = request.IdSector,
                IdSegmentoNegocio = request.IdSegmentoNegocio,
                IdTipoCicloVenta = dataAntigua.IdTipoCicloVenta,
                IdTipoCicloImplementacion = dataAntigua.IdTipoCicloImplementacion,
                Prioridad = dataAntigua.Prioridad,
                ProbalidadExito = dataAntigua.ProbalidadExito,
                PorcentajeCierre = dataAntigua.PorcentajeCierre,
                PorcentajeCheckList = dataAntigua.PorcentajeCheckList,
                IdFase = dataAntigua.IdFase,
                IdEtapa = request.IdEtapa,
                FlgCapexMayor = dataAntigua.FlgCapexMayor,
                ImporteCapex = dataAntigua.ImporteCapex,
                IdMoneda = dataAntigua.IdMoneda,
                ImporteFCV = dataAntigua.ImporteFCV,
                FechaApertura = dataAntigua.FechaApertura,
                FechaCierre = dataAntigua.FechaCierre,
                IdEstadoOportunidad = dataAntigua.IdEstadoOportunidad,
                IdRecursoComercial = dataAntigua.IdRecursoComercial,
                IdRecursoPreventa = dataAntigua.IdRecursoPreventa,
                IdLineaNegocio = request.IdLineaNegocio,               
                IdEstado = dataAntigua.IdEstado,
                IdUsuarioCreacion = dataAntigua.IdUsuarioCreacion,
                FechaCreacion = dataAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now,
            };
            ioportunidadSTDal.Modify(oportunidad);
            ioportunidadSTDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }

    }
}
