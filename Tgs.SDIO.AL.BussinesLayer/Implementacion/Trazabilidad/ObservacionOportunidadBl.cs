﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System.Linq;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using Tgs.SDIO.Util.Funciones;
namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
    public class ObservacionOportunidadBl : IObservacionOportunidadBl
    {
        readonly IObservacionOportunidadDal iobservacionOportunidadDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ObservacionOportunidadBl(IObservacionOportunidadDal observacionOportunidadDal)
        {
            iobservacionOportunidadDal = observacionOportunidadDal;
        }

        public ObservacionOportunidadPaginadoDtoResponse ListadoObservacionesOportunidadPaginado(ObservacionOportunidadDtoRequest request)
        {
            return iobservacionOportunidadDal.ListadoObservacionesOportunidadPaginado(request);
        }

        public ObservacionOportunidadDtoResponse ObtenerObservacionOportunidadPorId(ObservacionOportunidadDtoRequest request)
        {
            var query = iobservacionOportunidadDal.GetFilteredAsNoTracking(x => x.IdObservacion == request.IdObservacion && (x.IdEstado == Activo || x.IdEstado == Inactivo)).ToList();
            return query.Select(x => new ObservacionOportunidadDtoResponse
            {
                IdObservacion = x.IdObservacion,
                IdOportunidad = x.IdOportunidad,
                IdCaso = x.IdCaso,
                IdSeguimiento = x.IdSeguimiento,
                FechaSeguimiento = x.FechaSeguimiento,
                strFechaSeguimiento = x.FechaSeguimiento != null ? Funciones.FormatoFecha(x.FechaSeguimiento.Value) : "",
                IdConcepto = x.IdConcepto,
                Observaciones = x.Observaciones,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion
            }).FirstOrDefault();
        }

        public ProcesoResponse ActualizarObservacionOportunidad(ObservacionOportunidadDtoRequest request)
        {
            var areaAntigua = ObtenerObservacionOportunidadPorId(request);
            var observacion = new ObservacionOportunidad()
            {
                IdObservacion    = request.IdObservacion,
                IdOportunidad = request.IdOportunidad,
                IdCaso = request.IdCaso,
                FechaSeguimiento = request.FechaSeguimiento,
                IdConcepto = request.IdConcepto,
                Observaciones = request.Observaciones,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                FechaCreacion = areaAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now,
            };
            iobservacionOportunidadDal.Modify(observacion);
            iobservacionOportunidadDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }

        public ProcesoResponse RegistrarObservacionOportunidad(ObservacionOportunidadDtoRequest request)
        {
            var observacion = new ObservacionOportunidad()
            {
                IdObservacion = request.IdObservacion,
                IdOportunidad = request.IdOportunidad,
                IdCaso = request.IdCaso,
                IdSeguimiento = request.IdSeguimiento,
                FechaSeguimiento = request.FechaSeguimiento,
                IdConcepto = request.IdConcepto,
                Observaciones = request.Observaciones,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = request.IdUsuario,
                FechaCreacion = DateTime.Now
            };
            iobservacionOportunidadDal.Add(observacion);
            iobservacionOportunidadDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;
        }

        public ProcesoResponse EliminarObservacionOportunidad(ObservacionOportunidadDtoRequest request)
        {
            var areaAntigua = ObtenerObservacionOportunidadPorId(request);
            var observacion = new ObservacionOportunidad()
            {
                IdObservacion = request.IdObservacion,
                IdOportunidad = areaAntigua.IdOportunidad,
                IdCaso = areaAntigua.IdCaso,
                IdSeguimiento = areaAntigua.IdSeguimiento,
                FechaSeguimiento = areaAntigua.FechaSeguimiento,
                IdConcepto = areaAntigua.IdConcepto,
                Observaciones = areaAntigua.Observaciones,
                IdEstado = Eliminado,
                IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                FechaCreacion = areaAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now
            };
            iobservacionOportunidadDal.Modify(observacion);
            iobservacionOportunidadDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;

            return respuesta;
        }
    }
}
