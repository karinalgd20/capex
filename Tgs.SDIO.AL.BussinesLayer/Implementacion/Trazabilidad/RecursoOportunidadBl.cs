﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System.Linq;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using Tgs.SDIO.Util.Funciones;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
    public class RecursoOportunidadBl : IRecursoOportunidadBl
    {
        readonly IRecursoOportunidadDal irecursoOportunidadDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public RecursoOportunidadBl(IRecursoOportunidadDal recursoOportunidadDal)
        {
            irecursoOportunidadDal = recursoOportunidadDal;
        }

        public RecursoOportunidadPaginadoDtoResponse ListadoRecursosOportunidadPaginado(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadDal.ListadoRecursosOportunidadPaginado(request);
        }

        public RecursoOportunidadDtoResponse ObtenerRecursoOportunidadPorId(RecursoOportunidadDtoRequest request)
        {
            var query = irecursoOportunidadDal.GetFilteredAsNoTracking(x => x.IdAsignacion == request.IdAsignacion && (x.IdEstado == Activo || x.IdEstado == Inactivo)).ToList();
            return query.Select(x => new RecursoOportunidadDtoResponse
            {
                IdAsignacion = x.IdAsignacion,
                IdOportunidad = x.IdOportunidad,
                IdRecurso = x.IdRecurso,
                IdCaso = x.IdCaso,
                IdRol = x.IdRol,
                PorcentajeDedicacion = x.PorcentajeDedicacion,
                Observaciones = x.Observaciones,
                FInicioAsignacion = x.FInicioAsignacion,
                FFinAsignacion = x.FFinAsignacion,
                strFInicioAsignacion = x.FInicioAsignacion != null ? Funciones.FormatoFecha(x.FInicioAsignacion.Value) : "",
                strFFinAsignacion = x.FFinAsignacion != null ? Funciones.FormatoFecha(x.FFinAsignacion.Value) : "",
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion
            }).FirstOrDefault();
        }

        public ProcesoResponse ActualizarRecursoOportunidad(RecursoOportunidadDtoRequest request)
        {
            var recursoOportAntigua = ObtenerRecursoOportunidadPorId(request);
            var recursoOportunidad = new RecursoOportunidad()
            {
                IdAsignacion = request.IdAsignacion,
                IdOportunidad = request.IdOportunidad,
                IdRecurso = request.IdRecurso,
                IdCaso = request.IdCaso,
                IdRol = request.IdRol,
                PorcentajeDedicacion = request.PorcentajeDedicacion,
                Observaciones = request.Observaciones,
                FInicioAsignacion = request.FInicioAsignacion,
                FFinAsignacion = request.FFinAsignacion,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = recursoOportAntigua.IdUsuarioCreacion,
                FechaCreacion = recursoOportAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now,
            };
            irecursoOportunidadDal.Modify(recursoOportunidad);
            irecursoOportunidadDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }

        public ProcesoResponse RegistrarRecursoOportunidad(RecursoOportunidadDtoRequest request)
        {
            try
            {
                var recursoOportunidad = new RecursoOportunidad()
                {
                    IdOportunidad = request.IdOportunidad,
                    IdRecurso = request.IdRecurso,
                    IdCaso = request.IdCaso,
                    IdRol = request.IdRol,
                    PorcentajeDedicacion = request.PorcentajeDedicacion,
                    Observaciones = request.Observaciones,
                    FInicioAsignacion = request.FInicioAsignacion,
                    FFinAsignacion = request.FFinAsignacion,
                    IdEstado = request.IdEstado,
                    IdUsuarioCreacion = request.IdUsuario,
                    FechaCreacion = DateTime.Now
                };
                irecursoOportunidadDal.Add(recursoOportunidad);
                irecursoOportunidadDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = General.RegistraOk;

                return respuesta;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public ProcesoResponse EliminarRecursoOportunidad(RecursoOportunidadDtoRequest request)
        {
            var recursoOportAntigua = ObtenerRecursoOportunidadPorId(request);
            var observacion = new RecursoOportunidad()
            {
                IdAsignacion = request.IdAsignacion,
                IdOportunidad = recursoOportAntigua.IdOportunidad,
                IdRecurso = recursoOportAntigua.IdRecurso,
                IdCaso = recursoOportAntigua.IdCaso,
                IdRol = recursoOportAntigua.IdRol,
                PorcentajeDedicacion = recursoOportAntigua.PorcentajeDedicacion,
                Observaciones = recursoOportAntigua.Observaciones,
                FInicioAsignacion = recursoOportAntigua.FInicioAsignacion,
                FFinAsignacion = recursoOportAntigua.FFinAsignacion,
                IdEstado = Eliminado,
                IdUsuarioCreacion = recursoOportAntigua.IdUsuarioCreacion,
                FechaCreacion = recursoOportAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now
            };
            irecursoOportunidadDal.Modify(observacion);
            irecursoOportunidadDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;

            return respuesta;
        }

        public ProcesoResponse RegistrarRecursoOportunidadMasivo(RecursoOportunidadDtoRequest request)
        {
            try
            {

                foreach (ComboDtoResponse oportunidad in request.ListOportunidadSelect)
                {
                    foreach (ComboDtoResponse recurso in request.ListRecursoSelect)
                    {
                        var recursoOportunidad = new RecursoOportunidad()
                        {
                            IdOportunidad = Convert.ToInt32(oportunidad.id),
                            IdRecurso = Convert.ToInt32(recurso.id),
                            IdCaso = 1,
                            IdRol = request.IdRol,
                            PorcentajeDedicacion = null,
                            Observaciones = null,
                            FInicioAsignacion = request.FInicioAsignacion,
                            FFinAsignacion = request.FFinAsignacion,
                            IdEstado = 1,
                            IdUsuarioCreacion = request.IdUsuario,
                            FechaCreacion = DateTime.Now
                        };
                        irecursoOportunidadDal.Add(recursoOportunidad);
                        irecursoOportunidadDal.UnitOfWork.Commit();
                    }
                }

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = General.RegistraOk;
                return respuesta;
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
