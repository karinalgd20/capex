﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using Tgs.SDIO.Util.Funciones;


namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
  public  class DatosPreventaMovilBl : IDatosPreventaMovilBl
    {
        readonly IDatosPreventaMovilDal idatosPreventaMovilDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public DatosPreventaMovilBl(IDatosPreventaMovilDal datosPreventaMovilDal)
        {
            idatosPreventaMovilDal = datosPreventaMovilDal;
        }

        public ProcesoResponse CargaExcelDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            return idatosPreventaMovilDal.CargaExcelDatosPreventaMovil(request);
        }

        public DatosPreventaMovilDtoResponse ObtenerDatosPreventaMovilPorId(DatosPreventaMovilDtoRequest request)
        {
                var query = idatosPreventaMovilDal.GetFilteredAsNoTracking(x => x.IdDatosPreventaMovil == request.IdDatosPreventaMovil && (x.IdEstado == Activo || x.IdEstado == Inactivo)).ToList();
                return query.Select(x => new DatosPreventaMovilDtoResponse
                {
                    IdDatosPreventaMovil = x.IdDatosPreventaMovil,
                    IdOportunidad = x.IdOportunidad,
                    IdCaso = x.IdCaso,
                    IdOperadorActual = x.IdOperadorActual,
                    LineasActuales = x.LineasActuales,
                    MesesContratoActual = x.MesesContratoActual,
                    RecurrenteMensualActual = x.RecurrenteMensualActual,
                    FCVActual = x.FCVActual,
                    ArpuServicioActual = x.ArpuServicioActual,
                    Lineas = x.Lineas,
                    MesesContrato = x.MesesContrato,
                    RecurrenteMensualVR = x.RecurrenteMensualVR,
                    FCVVR = x.FCVVR,
                    ArpuServicioVR = x.ArpuServicioVR,
                    FechaPresentacion = x.FechaPresentacion,
                    FechaBuenaPro = x.FechaBuenaPro,
                    strFechaPresentacion = x.FechaPresentacion != null ? Funciones.FormatoFecha(x.FechaPresentacion.Value) : "",
                    strFechaBuenaPro = x.FechaBuenaPro != null ? Funciones.FormatoFecha(x.FechaBuenaPro.Value) : "",
                    Observaciones = x.Observaciones,
                    MontoCapex = x.MontoCapex,
                    IdEstadoOportunidad = x.IdEstadoOportunidad,
                    FCVMovistar = x.FCVMovistar,
                    ArpuTotalMovistar = x.ArpuTotalMovistar,
                    VanVai = x.VanVai,
                    MesesRecupero = x.MesesRecupero,
                    FCVClaro = x.FCVClaro,
                    ArpuTotalClaro = x.ArpuTotalClaro,
                    FCVEntel = x.FCVEntel,
                    ArpuTotalEntel = x.ArpuTotalEntel,
                    FCVViettel = x.FCVViettel,
                    ArpuTotalViettel = x.ArpuTotalViettel,
                    ArpuServicio = x.ArpuServicio,
                    ArpuEquipos = x.ArpuEquipos,
                    ModalidadEquipo = x.ModalidadEquipo,
                    CostoPromedioEquipo = x.CostoPromedioEquipo,
                    PorcentajeSubsidio = x.PorcentajeSubsidio,
                    IdEstado = x.IdEstado,
                    IdUsuarioCreacion = x.IdUsuarioCreacion,
                    FechaCreacion = x.FechaCreacion,
                    IdUsuarioEdicion = x.IdUsuarioEdicion,
                    FechaEdicion = x.FechaEdicion
                }).FirstOrDefault();

        }

        public ProcesoResponse ActualizarDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            var dataAntigua = ObtenerDatosPreventaMovilPorId(request);
            var datosPreventaMovil = new DatosPreventaMovil()
            {
                IdDatosPreventaMovil = request.IdDatosPreventaMovil,
                IdOportunidad = request.IdOportunidad,
                IdCaso = request.IdCaso,
                IdOperadorActual = request.IdOperadorActual,
                LineasActuales = request.LineasActuales,
                MesesContratoActual = request.MesesContratoActual,
                RecurrenteMensualActual = request.RecurrenteMensualActual,
                FCVActual = request.FCVActual,
                ArpuServicioActual = request.ArpuServicioActual,
                Lineas = request.Lineas,
                MesesContrato = request.MesesContrato,
                RecurrenteMensualVR = request.RecurrenteMensualVR,
                FCVVR = request.FCVVR,
                ArpuServicioVR = request.ArpuServicioVR,
                FechaPresentacion = request.FechaPresentacion,
                FechaBuenaPro = request.FechaBuenaPro,
                Observaciones = request.Observaciones,
                MontoCapex = request.MontoCapex,
                IdEstadoOportunidad = request.IdEstadoOportunidad,
                FCVMovistar = request.FCVMovistar,
                ArpuTotalMovistar = request.ArpuTotalMovistar,
                VanVai = request.VanVai,
                MesesRecupero = request.MesesRecupero,
                FCVClaro = request.FCVClaro,
                ArpuTotalClaro = request.ArpuTotalClaro,
                FCVEntel = request.FCVEntel,
                ArpuTotalEntel = request.ArpuTotalEntel,
                FCVViettel = request.FCVViettel,
                ArpuTotalViettel = request.ArpuTotalViettel,
                ArpuServicio = request.ArpuServicio,
                ArpuEquipos = request.ArpuEquipos,
                ModalidadEquipo = request.ModalidadEquipo,
                CostoPromedioEquipo = request.CostoPromedioEquipo,
                PorcentajeSubsidio = request.PorcentajeSubsidio,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = dataAntigua.IdUsuarioCreacion,
                FechaCreacion = dataAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now,
            };
            idatosPreventaMovilDal.Modify(datosPreventaMovil);
            idatosPreventaMovilDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }

        public ProcesoResponse ActualizarDatosPreventaMovilModalPreventa(DatosPreventaMovilDtoRequest request)
        {
            var dataAntigua = ObtenerDatosPreventaMovilPorId(request);
            var datosPreventaMovil = new DatosPreventaMovil()
            {
                IdDatosPreventaMovil = request.IdDatosPreventaMovil,
                IdOportunidad = dataAntigua.IdOportunidad,
                IdCaso = dataAntigua.IdCaso,
                IdOperadorActual = request.IdOperadorActual,
                LineasActuales = request.LineasActuales,
                MesesContratoActual = request.MesesContratoActual,
                RecurrenteMensualActual = request.RecurrenteMensualActual,
                FCVActual = request.FCVActual,
                ArpuServicioActual = request.ArpuServicioActual,
                Lineas = request.Lineas,
                MesesContrato = request.MesesContrato,
                RecurrenteMensualVR = request.RecurrenteMensualVR,
                FCVVR = request.FCVVR,
                ArpuServicioVR = request.ArpuServicioVR,
                FechaPresentacion = request.FechaPresentacion,
                FechaBuenaPro = request.FechaBuenaPro,
                Observaciones = request.Observaciones,
                MontoCapex = request.MontoCapex,
                IdEstadoOportunidad = request.IdEstadoOportunidad,
                FCVMovistar = request.FCVMovistar,
                ArpuTotalMovistar = request.ArpuTotalMovistar,
                VanVai = request.VanVai,
                MesesRecupero = request.MesesRecupero,
                FCVClaro = request.FCVClaro,
                ArpuTotalClaro = request.ArpuTotalClaro,
                FCVEntel = request.FCVEntel,
                ArpuTotalEntel = request.ArpuTotalEntel,
                FCVViettel = request.FCVViettel,
                ArpuTotalViettel = request.ArpuTotalViettel,
                ArpuServicio = request.ArpuServicio,
                ArpuEquipos = request.ArpuEquipos,
                ModalidadEquipo = request.ModalidadEquipo,
                CostoPromedioEquipo = request.CostoPromedioEquipo,
                PorcentajeSubsidio = request.PorcentajeSubsidio,
                IdEstado = dataAntigua.IdEstado,
                IdUsuarioCreacion = dataAntigua.IdUsuarioCreacion,
                FechaCreacion = dataAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now,
            };
            idatosPreventaMovilDal.Modify(datosPreventaMovil);
            idatosPreventaMovilDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }
        public ProcesoResponse RegistrarDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            var datosPreventaMovil = new DatosPreventaMovil()
            {
                IdOportunidad = request.IdOportunidad,
                IdCaso = request.IdCaso,
                IdOperadorActual = request.IdOperadorActual,
                LineasActuales = request.LineasActuales,
                MesesContratoActual = request.MesesContratoActual,
                RecurrenteMensualActual = request.RecurrenteMensualActual,
                FCVActual = request.FCVActual,
                ArpuServicioActual = request.ArpuServicioActual,
                Lineas = request.Lineas,
                MesesContrato = request.MesesContrato,
                RecurrenteMensualVR = request.RecurrenteMensualVR,
                FCVVR = request.FCVVR,
                ArpuServicioVR = request.ArpuServicioVR,
                FechaPresentacion = request.FechaPresentacion,
                FechaBuenaPro = request.FechaBuenaPro,
                Observaciones = request.Observaciones,
                MontoCapex = request.MontoCapex,
                IdEstadoOportunidad = request.IdEstadoOportunidad,
                FCVMovistar = request.FCVMovistar,
                ArpuTotalMovistar = request.ArpuTotalMovistar,
                VanVai = request.VanVai,
                MesesRecupero = request.MesesRecupero,
                FCVClaro = request.FCVClaro,
                ArpuTotalClaro = request.ArpuTotalClaro,
                FCVEntel = request.FCVEntel,
                ArpuTotalEntel = request.ArpuTotalEntel,
                FCVViettel = request.FCVViettel,
                ArpuTotalViettel = request.ArpuTotalViettel,
                ArpuServicio = request.ArpuServicio,
                ArpuEquipos = request.ArpuEquipos,
                ModalidadEquipo = request.ModalidadEquipo,
                CostoPromedioEquipo = request.CostoPromedioEquipo,
                PorcentajeSubsidio = request.PorcentajeSubsidio,
                IdEstado = Activo,
                IdUsuarioCreacion = request.IdUsuario,
                FechaCreacion = DateTime.Now
            };
            idatosPreventaMovilDal.Add(datosPreventaMovil);
            idatosPreventaMovilDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;
        }

        public ProcesoResponse EliminarDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            var dataAntigua = ObtenerDatosPreventaMovilPorId(request);
            var datosPreventaMovil = new DatosPreventaMovil()
            {
                IdDatosPreventaMovil = request.IdDatosPreventaMovil,
                IdOportunidad = dataAntigua.IdOportunidad,
                IdCaso = dataAntigua.IdCaso,
                IdOperadorActual = dataAntigua.IdOperadorActual,
                LineasActuales = dataAntigua.LineasActuales,
                MesesContratoActual = dataAntigua.MesesContratoActual,
                RecurrenteMensualActual = dataAntigua.RecurrenteMensualActual,
                FCVActual = dataAntigua.FCVActual,
                ArpuServicioActual = dataAntigua.ArpuServicioActual,
                Lineas = dataAntigua.Lineas,
                MesesContrato = dataAntigua.MesesContrato,
                RecurrenteMensualVR = dataAntigua.RecurrenteMensualVR,
                FCVVR = dataAntigua.FCVVR,
                ArpuServicioVR = dataAntigua.ArpuServicioVR,
                FechaPresentacion = dataAntigua.FechaPresentacion,
                FechaBuenaPro = dataAntigua.FechaBuenaPro,
                Observaciones = dataAntigua.Observaciones,
                MontoCapex = dataAntigua.MontoCapex,
                IdEstadoOportunidad = dataAntigua.IdEstadoOportunidad,
                FCVMovistar = dataAntigua.FCVMovistar,
                ArpuTotalMovistar = dataAntigua.ArpuTotalMovistar,
                VanVai = dataAntigua.VanVai,
                MesesRecupero = dataAntigua.MesesRecupero,
                FCVClaro = dataAntigua.FCVClaro,
                ArpuTotalClaro = dataAntigua.ArpuTotalClaro,
                FCVEntel = dataAntigua.FCVEntel,
                ArpuTotalEntel = dataAntigua.ArpuTotalEntel,
                FCVViettel = dataAntigua.FCVViettel,
                ArpuTotalViettel = dataAntigua.ArpuTotalViettel,
                ArpuServicio = dataAntigua.ArpuServicio,
                ArpuEquipos = dataAntigua.ArpuEquipos,
                ModalidadEquipo = dataAntigua.ModalidadEquipo,
                CostoPromedioEquipo = dataAntigua.CostoPromedioEquipo,
                PorcentajeSubsidio = dataAntigua.PorcentajeSubsidio,
                IdEstado = Eliminado,
                IdUsuarioCreacion = dataAntigua.IdUsuarioCreacion,
                FechaCreacion = dataAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now
            };
            idatosPreventaMovilDal.Modify(datosPreventaMovil);
            idatosPreventaMovilDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;

            return respuesta;
        }
    }
}
