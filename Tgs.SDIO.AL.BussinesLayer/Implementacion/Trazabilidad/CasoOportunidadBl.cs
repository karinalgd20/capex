﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using Tgs.SDIO.Util.Funciones;


namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
   public class CasoOportunidadBl : ICasoOportunidadBl
    {
        readonly ICasoOportunidadDal icasoOportunidadDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public CasoOportunidadBl(ICasoOportunidadDal casoOportunidadDal)
        {
            icasoOportunidadDal = casoOportunidadDal;
        }

        public CasoOportunidadDtoResponse ObtenerCasoOportunidadPorId(CasoOportunidadDtoRequest request)
        {
            var query = icasoOportunidadDal.GetFilteredAsNoTracking(x => x.IdCaso == request.IdCaso && (x.IdEstado == Activo || x.IdEstado == Inactivo)).ToList();
            return query.Select(x => new CasoOportunidadDtoResponse
            {
                IdCaso = x.IdCaso,
                IdOportunidad = x.IdOportunidad,
                Descripcion = x.Descripcion,
                IdCasoSF = x.IdCasoSF,
                IdCasoPadre = x.IdCasoPadre,
                IdTipoSolicitud = x.IdTipoSolicitud,
                Asunto = x.Asunto,
                IdFase = x.IdFase,
                IdEtapa = x.IdEtapa,
                Complejidad = x.Complejidad,
                Prioridad = x.Prioridad,
                IdEstadoCaso = x.IdEstadoCaso,
                SolucionCaso = x.SolucionCaso,
                FechaApertura = x.FechaApertura,
                FechaCierre = x.FechaCierre,
                strFechaApertura = x.FechaApertura != null ? Funciones.FormatoFecha(x.FechaApertura.Value) : "",
                strFechaCierre = x.FechaCierre != null ? Funciones.FormatoFecha(x.FechaCierre.Value) : "",
                FechaCompromisoAtencion = x.FechaCompromisoAtencion,               
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion
            }).FirstOrDefault();

        }

        public ProcesoResponse ActualizarCasoOportunidadModalPreventa(CasoOportunidadDtoRequest request)
        {
            var dataAntigua = ObtenerCasoOportunidadPorId(request);
            var caso = new CasoOportunidad()
            {
                IdCaso = request.IdCaso,
                IdOportunidad = dataAntigua.IdOportunidad,
                Descripcion = dataAntigua.Descripcion,
                IdCasoSF = request.IdCasoSF,
                IdCasoPadre = dataAntigua.IdCasoPadre,
                IdTipoSolicitud = request.IdTipoSolicitud,
                Asunto = request.Asunto,
                IdFase = dataAntigua.IdFase,
                IdEtapa = dataAntigua.IdEtapa,
                Complejidad = dataAntigua.Complejidad,
                Prioridad = dataAntigua.Prioridad,
                IdEstadoCaso = request.IdEstadoCaso,
                SolucionCaso = dataAntigua.SolucionCaso,
                FechaApertura = request.FechaApertura,
                FechaCierre = request.FechaCierre,
                FechaCompromisoAtencion = dataAntigua.FechaCompromisoAtencion,
                IdEstado = dataAntigua.IdEstado,
                IdUsuarioCreacion = dataAntigua.IdUsuarioCreacion,
                FechaCreacion = dataAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now,
            };
            icasoOportunidadDal.Modify(caso);
            icasoOportunidadDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }
    }
}
