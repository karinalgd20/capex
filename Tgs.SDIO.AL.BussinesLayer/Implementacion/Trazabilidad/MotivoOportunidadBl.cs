﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Transactions;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
    public class MotivoOportunidadBl : IMotivoOportunidadBl
    {
        readonly IMotivoOportunidadDal imotivoOportunidadDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public MotivoOportunidadBl(IMotivoOportunidadDal motivoOportunidadDal)
        {
            imotivoOportunidadDal = motivoOportunidadDal;
        }

        public List<ListaDtoResponse> ListarComboMotivoOportunidad()
        {
            return imotivoOportunidadDal.ListarComboMotivoOportunidad();
        }
    }
}
