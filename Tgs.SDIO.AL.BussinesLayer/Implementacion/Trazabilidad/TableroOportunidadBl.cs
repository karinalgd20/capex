﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Transactions;
using System.Linq;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using System.Collections.Generic;

//EAAR: PF12
namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
    public class TableroOportunidadBl : ITableroOportunidadBl
    {
        readonly ITableroOportunidadDal itableroOportunidadDal;
      

        public TableroOportunidadBl(ITableroOportunidadDal tableroOportunidadDal)
        {
            itableroOportunidadDal = tableroOportunidadDal; 
        }

        public TableroOportunidadMatrizDtoResponse ListarTableroOportunidadMatriz(TableroOportunidadListarDtoRequest request)
        {
            return itableroOportunidadDal.ListarTableroOportunidadMatriz(request);
        }
    }
}
