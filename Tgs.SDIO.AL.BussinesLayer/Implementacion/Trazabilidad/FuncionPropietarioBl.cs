﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
    public class FuncionPropietarioBl : IFuncionPropietarioBl
    {
        readonly IFuncionPropietarioDal ifuncionPropietarioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public FuncionPropietarioBl(IFuncionPropietarioDal funcionPropietarioDal)
        {
            ifuncionPropietarioDal = funcionPropietarioDal;
        }

        public List<ListaDtoResponse> ListarComboFuncionPropietario()
        {
            return ifuncionPropietarioDal.ListarComboFuncionPropietario();
        }
    }
}
