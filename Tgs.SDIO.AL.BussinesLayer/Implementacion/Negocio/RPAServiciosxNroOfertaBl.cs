﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Negocio
{
    public class RPAServiciosxNroOfertaBl : IRPAServiciosxNroOfertaBl
    {
        private readonly IRPAServiciosxNroOfertaDal iRPAServiciosxNroOfertaDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public RPAServiciosxNroOfertaBl(IRPAServiciosxNroOfertaDal IRPAServiciosxNroOfertaDal)
        {
            iRPAServiciosxNroOfertaDal = IRPAServiciosxNroOfertaDal;
        }
        public ProcesoResponse AgregarServiciosNroOferta(RPAServiciosxNroOfertaDtoRequest request)
        {
            return iRPAServiciosxNroOfertaDal.AgregarServiciosNroOferta(request);
        }
        public List<RPAServiciosxNroOfertaDtoResponse> ListarServiciosNroOferta(RPAServiciosxNroOfertaDtoRequest request)
        {
            return iRPAServiciosxNroOfertaDal.ListarServiciosNroOferta(request);
        }
        public RPAServiciosxNroOfertaPaginadoDtoResponse ListarServiciosNroOfertaPaginado(RPAServiciosxNroOfertaDtoRequest request)
        {
            return iRPAServiciosxNroOfertaDal.ListarServiciosNroOfertaPaginado(request);
        }
    }
}
