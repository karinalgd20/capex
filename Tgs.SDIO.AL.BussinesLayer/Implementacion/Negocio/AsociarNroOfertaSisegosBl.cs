﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.Util.Mensajes.Negocio;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Negocio
{
    public class AsociarNroOfertaSisegosBl : IAsociarNroOfertaSisegosBl
    {
        private readonly IAsociarNroOfertaSisegosDal iAsociarNroOfertaSisegosDal;

        ProcesoResponse respuesta = new ProcesoResponse();

        public AsociarNroOfertaSisegosBl(IAsociarNroOfertaSisegosDal IAsociarNroOfertaSisegosDal)
        {
            iAsociarNroOfertaSisegosDal = IAsociarNroOfertaSisegosDal;
        }

        public IsisNroOfertaDtoResponse ObtenerNroOfertaById(AsociarNroOfertaSisegosDtoRequest request)
        {
            return iAsociarNroOfertaSisegosDal.ObtenerNroOfertaById(request);
        }

        public SisegoCotizadoDtoResponse ObtenerCodSisegoById(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            return iAsociarNroOfertaSisegosDal.ObtenerCodSisegoById(resquest);
        }

        public ProcesoResponse AgregarOfertaSisegos(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            return iAsociarNroOfertaSisegosDal.AgregarOfertaSisegos(resquest);
        }
        public ProcesoResponse EliminarOfertaSisego(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            var validaOfertaSisego = iAsociarNroOfertaSisegosDal.GetFilteredAsNoTracking(x => x.Id == resquest.Id).Any();


            if (validaOfertaSisego)
            {
                var ofertaSisego = new AsociarNroOfertaSisegos
                {
                    Id = resquest.Id
                };

                iAsociarNroOfertaSisegosDal.Remove(ofertaSisego);
                iAsociarNroOfertaSisegosDal.UnitOfWork.Commit();

                respuesta.Id = resquest.Id;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralNegocio.EliminarOfertaSisego;
            }

            return respuesta;
        }
        public List<AsociarNroOfertaSisegosDtoResponse> ListarOfertaSisegos(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            return iAsociarNroOfertaSisegosDal.ListarOfertaSisegos(resquest);
        }

        public AsociarOfertaSisegosPaginadoDtoResponse ListarOfertaSisegosPaginado(AsociarNroOfertaSisegosDtoRequest resquest)
        {
            return iAsociarNroOfertaSisegosDal.ListarOfertaSisegosPaginado(resquest);
        }
    }
}
