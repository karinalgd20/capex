﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Util.Mensajes.Negocio;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Negocio
{
    public class RPASisegoDatosContactoBl : IRPASisegoDatosContactoBl
    {
        private readonly IRPASisegoDatosContactoDal iRPASisegoDatosContactoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public RPASisegoDatosContactoBl(IRPASisegoDatosContactoDal IRPASisegoDatosContactoDal)
        {
            iRPASisegoDatosContactoDal = IRPASisegoDatosContactoDal;
        }

        #region Transaccionales
        public ProcesoResponse ActualizarContactoServicio(RPASisegoDatosContactoDtoRequest resquest)
        {
            var contacto = iRPASisegoDatosContactoDal.Get(resquest.Id);
            contacto.Contacto = resquest.Contacto;
            contacto.Telefono_1 = resquest.Telefono_1;
            contacto.Telefono_2 = resquest.Telefono_2;
            contacto.Telefono_3 = resquest.Telefono_3;

            contacto.Contacto_2 = resquest.Contacto_2;
            contacto.Telefono1_Contacto2 = resquest.Telefono1_Contacto2;
            contacto.Telefono2_Contacto2 = resquest.Telefono2_Contacto2;
            contacto.Telefono3_Contacto2 = resquest.Telefono3_Contacto2;

            contacto.IdUsuarioEdicion = resquest.IdUsuarioEdicion;
            contacto.FechaEdicion = resquest.FechaEdicion;

            iRPASisegoDatosContactoDal.Modify(contacto);
            iRPASisegoDatosContactoDal.UnitOfWork.Commit();

            respuesta.Id = contacto.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralNegocio.ActualizarContactoSisego;
            return respuesta;
        }
        public ProcesoResponse RegistrarContactoServicio(RPASisegoDatosContactoDtoRequest resquest)
        {
            return null;
        }

        #endregion

        #region ---- No Transaccional -----
        public List<RPASisegoDatosContactoDtoResponse> ListaDetallesContactoRPA(RPASisegoDatosContactoDtoRequest resquest)
        {
            return iRPASisegoDatosContactoDal.ListaDetallesContactoRPA(resquest);
        }
        public RPASisegoDatosContactoPaginadoDtoResponse ListaDetallesContactoRPAPaginadoDtoResponse(RPASisegoDatosContactoDtoRequest resquest)
        {
            return iRPASisegoDatosContactoDal.ListaDetallesContactoRPAPaginadoDtoResponse(resquest);
        }
        #endregion

    }
}
