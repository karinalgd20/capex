﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Mensajes.Negocio;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Negocio
{
    public class SisegoCotizadoBl : ISisegoCotizadoBl
    {
        readonly ISisegoCotizadoDal iSisegoCotizadoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SisegoCotizadoBl(ISisegoCotizadoDal ISisegoCotizadoDal)
        {
            iSisegoCotizadoDal = ISisegoCotizadoDal;
        }

        public ProcesoResponse AgregarSisegoCotizado(SisegoCotizadoDtoRequest resquest)
        {
            var validaSisego = iSisegoCotizadoDal.GetFilteredAsNoTracking(x => x.codigo_sisego == resquest.codigo_sisego).Any();

            if (validaSisego)
            {
                respuesta.Id = 0;
                respuesta.TipoRespuesta = Proceso.Invalido;
                respuesta.Mensaje = MensajesGeneralNegocio.SisegoDuplicado;
                respuesta.Detalle = "Error";
            }
            else
            {
                var CodigoSisego = new SisegoCotizado
                {
                    Id = resquest.Id,
                    Id_OportunidadGanadora = resquest.Id_OportunidadGanadora,
                    codigo_sisego = resquest.codigo_sisego,
                    IdEstado = Estados.Activo,
                    IdUsuarioCreacion = resquest.IdUsuarioCreacion,
                    FechaCreacion = resquest.FechaCreacion
                };
                iSisegoCotizadoDal.Add(CodigoSisego);
                iSisegoCotizadoDal.UnitOfWork.Commit();
                respuesta.Id = resquest.Id;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralNegocio.AgregarSisego;
            }
            
            return respuesta;
            
        }

        public ProcesoResponse EliminarSisegoCotizado(SisegoCotizadoDtoRequest resquest)
        {
            var sisego = iSisegoCotizadoDal.Get(resquest.Id);
            sisego.IdEstado = Generales.Estados.Inactivo;
            sisego.IdUsuarioEdicion = resquest.IdUsuarioEdicion;
            sisego.FechaEdicion = resquest.FechaEdicion;

            iSisegoCotizadoDal.Modify(sisego);
            iSisegoCotizadoDal.UnitOfWork.Commit();

            respuesta.Id = sisego.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralNegocio.EliminarSisego;
            return respuesta;
        }

        public SisegoCotizadoPaginadoDtoResponse ListarSisegoCotizadoPaginado(SisegoCotizadoDtoRequest resquest)
        {
            return iSisegoCotizadoDal.ListarSisegoCotizadoPaginado(resquest);
        }

        public List<SisegoCotizadoDtoResponse> ListarSisegosCotizados(SisegoCotizadoDtoRequest resquest)
        {
            return iSisegoCotizadoDal.ListarSisegosCotizados(resquest);
        }
    }
}
