﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Negocio
{
    public class RPASisegoDetalleBl : IRPASisegoDetalleBl
    {
        private readonly IRPASisegoDetalleDal iRPASisegoDetalleDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public RPASisegoDetalleBl(IRPASisegoDetalleDal IRPASisegoDetalleDal)
        {
            iRPASisegoDetalleDal = IRPASisegoDetalleDal;
        }
        public ProcesoResponse AgregarSisegoDetalle(RPASisegoDetalleDtoRequest request)
        {
            return iRPASisegoDetalleDal.AgregarSisegoDetalle(request);
        }
        public List<RPASisegoDetalleDtoResponse> ListarSisegoDetalle(RPASisegoDetalleDtoRequest request)
        {
            return iRPASisegoDetalleDal.ListarSisegoDetalle(request);
        }
        public RPASisegoDetallePaginadoDtoResponse ListarSisegoDetallePaginado(RPASisegoDetalleDtoRequest request)
        {
            return iRPASisegoDetalleDal.ListarSisegoDetallePaginado(request);
        }
    }
}
