﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Util.Mensajes.Negocio;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Negocio
{
    public class IsisNroOfertaBl : IIsisNroOfertaBl
    {
        readonly IIsisNroOfertaDal isisNroOfertaDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public IsisNroOfertaBl(IIsisNroOfertaDal IsisNroOfertaDal)
        {
            isisNroOfertaDal = IsisNroOfertaDal;
        }

        public ProcesoResponse AgregarIsisNroOferta(IsisNroOfertaDtoRequest resquest)
        {
            var validaOferta = isisNroOfertaDal.GetFilteredAsNoTracking(x => x.Nro_oferta == resquest.Nro_oferta).Any();

            if (validaOferta)
            {
                var validaVersion = isisNroOfertaDal.GetFilteredAsNoTracking(x => x.Nro_version_Oferta == resquest.Nro_version_Oferta).Any();

                if (validaVersion)
                {
                    respuesta.Id = 0;
                    respuesta.TipoRespuesta = Proceso.Invalido;
                    respuesta.Mensaje = MensajesGeneralNegocio.OfertaVersionDuplicada;
                    respuesta.Detalle = "Error";
                }
                else
                {
                    var NroOferta = new IsisNroOferta()
                    {
                        Id = resquest.Id,
                        Nro_oferta = resquest.Nro_oferta,
                        Nro_version_Oferta = resquest.Nro_version_Oferta,
                        Id_OportunidadGanadora = resquest.Id_OportunidadGanadora,
                        IdEstado = Estados.Activo,
                        IdUsuarioCreacion = resquest.IdUsuarioCreacion,
                        FechaCreacion = DateTime.Now
                    };

                    isisNroOfertaDal.Add(NroOferta);
                    isisNroOfertaDal.UnitOfWork.Commit();

                    respuesta.Id = resquest.Id;
                    respuesta.TipoRespuesta = Proceso.Valido;
                    respuesta.Mensaje = MensajesGeneralNegocio.AgregarOferta;
                }
            }
            else
            {
                var NroOferta = new IsisNroOferta()
                {
                    Id = resquest.Id,
                    Nro_oferta = resquest.Nro_oferta,
                    Nro_version_Oferta = resquest.Nro_version_Oferta,
                    Id_OportunidadGanadora = resquest.Id_OportunidadGanadora,
                    IdEstado = Estados.Activo,
                    IdUsuarioCreacion = resquest.IdUsuarioCreacion,
                    FechaCreacion = DateTime.Now
                };

                isisNroOfertaDal.Add(NroOferta);
                isisNroOfertaDal.UnitOfWork.Commit();

                respuesta.Id = resquest.Id;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralNegocio.AgregarOferta;
            }
           
            return respuesta;
        }
        public ProcesoResponse EliminarIsisNroOferta(IsisNroOfertaDtoRequest resquest)
        {
            var oferta = isisNroOfertaDal.Get(resquest.Id);
            oferta.IdEstado = Generales.Estados.Inactivo;
            oferta.IdUsuarioEdicion = resquest.IdUsuarioEdicion;
            oferta.FechaEdicion = resquest.FechaEdicion;

            isisNroOfertaDal.Modify(oferta);
            isisNroOfertaDal.UnitOfWork.Commit();

            respuesta.Id = oferta.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralNegocio.EliminarOferta;
            return respuesta;
        }

        public List<IsisNroOfertaDtoResponse> ListarNroOferta(IsisNroOfertaDtoRequest resquest)
        {
            return isisNroOfertaDal.ListarNroOferta(resquest);
        }

        public IsisNroOfertaPaginadoDtoResponse ListarNroOfertaPaginado(IsisNroOfertaDtoRequest resquest)
        {
            return isisNroOfertaDal.ListarNroOfertaPaginado(resquest);
        }
    }
}
