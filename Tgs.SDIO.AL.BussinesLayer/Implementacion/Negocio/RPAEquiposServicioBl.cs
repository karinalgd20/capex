﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Negocio
{
    public class RPAEquiposServicioBl : IRPAEquiposServicioBl
    {
        private readonly IRPAEquiposServicioDal iRPAEquiposServicioDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public RPAEquiposServicioBl(IRPAEquiposServicioDal IRPAEquiposServicioDal)
        {
            iRPAEquiposServicioDal = IRPAEquiposServicioDal;
        }
        public ProcesoResponse AgregarEquipoServicio(RPAEquiposServicioDtoRequest request)
        {
            return iRPAEquiposServicioDal.AgregarEquipoServicio(request);
        }
        public List<RPAEquiposServicioDtoResponse> ListarEquipoServicio(RPAEquiposServicioDtoRequest request)
        {
            return iRPAEquiposServicioDal.ListarEquipoServicio(request);
        }
        public RPAEquiposServicioPaginadoDtoResponse ListarEquipoServicioPaginado(RPAEquiposServicioDtoRequest request)
        {
            return iRPAEquiposServicioDal.ListarEquipoServicioPaginado(request);
        }
    }
}
