﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.Util.Mensajes.Negocio;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Negocio
{
    public class AsociarServicioSisegosBl : IAsociarServicioSisegosBl
    {
        private readonly IAsociarServicioSisegosDal iAsociarServicioSisegosDal;
     
        ProcesoResponse respuesta = new ProcesoResponse();
        public AsociarServicioSisegosBl(IAsociarServicioSisegosDal IAsociarServicioSisegosDal)
        {
            iAsociarServicioSisegosDal = IAsociarServicioSisegosDal;
        }
        public ProcesoResponse AsociarServicioSisegos(AsociarServicioSisegosDtoRequest request)
        {
            return iAsociarServicioSisegosDal.AsociarServicioSisegos(request);
        }
        public List<AsociarServicioSisegosDtoResponse> ListarServicioSisegos(AsociarServicioSisegosDtoRequest request)
        {
            return iAsociarServicioSisegosDal.ListarServicioSisegos(request);
        }
        public AsociarServicioSisegosPaginadoDtoResponse ListarServicioSisegosPaginado(AsociarServicioSisegosDtoRequest request)
        {
            return iAsociarServicioSisegosDal.ListarServicioSisegosPaginado(request);
        }
        public RPAServiciosxNroOfertaDtoResponse ObtenerServicioById(AsociarServicioSisegosDtoRequest request)
        {
            return iAsociarServicioSisegosDal.ObtenerServicioById(request);
        }
        public RPAEquiposServicioDtoResponse ObtenerEquipoById(AsociarServicioSisegosDtoRequest request)
        {
            return iAsociarServicioSisegosDal.ObtenerEquipoById(request);
        }
        public RPASisegoDetalleDtoResponse ObtenerCodSisegoById(AsociarServicioSisegosDtoRequest request)
        {
            return iAsociarServicioSisegosDal.ObtenerCodSisegoById(request);
        }
    }
}
