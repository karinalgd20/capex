﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.Negocios;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Negocio;
using Tgs.SDIO.AL.DataAccess.Interfaces.Negocios;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Negocios;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Negocios;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Mensajes.Negocio;
using static Tgs.SDIO.Util.Constantes.Negocio;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using static Tgs.SDIO.Util.Constantes.Generales;
using System.Transactions;




namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Negocio
{
    public class OportunidadGanadoraBl : IOportunidadGanadoraBl
    {
        readonly IOportunidadGanadoraDal iOportunidadGanadoraDal;
        readonly IClienteDal iClienteDal;
        readonly IIsisNroOfertaDal iIsisNroOfertaDal;
        readonly IRPAServiciosxNroOfertaDal iRPAServiciosxNroOferta;
        readonly IRPASisegoDetalleDal iRPASisegoDetalle;
        readonly IRPASisegoDatosContactoDal iRPASisegoDatosContacto;
        readonly IRPAEquiposServicioDal iRPAEquiposServicio;

        public OportunidadGanadoraBl(IOportunidadGanadoraDal IOportunidadGanadoraDal, IClienteDal IClienteDal, IIsisNroOfertaDal IIsisNroOfertaDal, IRPAServiciosxNroOfertaDal IRPAServiciosxNroOfertaDal, IRPASisegoDetalleDal IRPASisegoDetalleDal, IRPASisegoDatosContactoDal IRPASisegoDatosContactoDal, IRPAEquiposServicioDal IRPAEquiposServicioDal)
        {
            iOportunidadGanadoraDal = IOportunidadGanadoraDal;
            iClienteDal = IClienteDal;
            iIsisNroOfertaDal = IIsisNroOfertaDal;
            iRPAServiciosxNroOferta = IRPAServiciosxNroOfertaDal;
            iRPASisegoDetalle = IRPASisegoDetalleDal;
            iRPASisegoDatosContacto = IRPASisegoDatosContactoDal;
            iRPAEquiposServicio = IRPAEquiposServicioDal;
        }

        ProcesoResponse respuesta = new ProcesoResponse();

        public SalesForceConsolidadoCabeceraDtoResponse ObtenerCasoOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraDal.ObtenerCasoOportunidadGanadora(request);
        }
        public ClienteDtoResponse ObtenerRucClientePorIdOportunidad(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraDal.ObtenerRucClientePorIdOportunidad(request);
        }

        public OportunidadGanadoraDtoResponse ObtenerOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraDal.ObtenerOportunidadGanadora(request);
        }

        public ProcesoResponse AgregarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            var validarOportunidadGanadora = iOportunidadGanadoraDal.GetFilteredAsNoTracking(x => x.PER == request.PER).Any();

            if (validarOportunidadGanadora)
            {
                respuesta.Id = 0;
                respuesta.TipoRespuesta = Proceso.Invalido;
                respuesta.Mensaje = MensajesGeneralNegocio.OportunidadDuplicada;
                respuesta.Detalle = "Error";
            }
            else
            {
                DateTime fecha = DateTime.Now;
                var OportunidadGanadora = new OportunidadGanadora()
                {
                    Id = request.Id,
                    PER = request.PER,
                    Ruc = request.Ruc,
                    IdEtapa = request.IdEtapa,
                    IdEstado = request.IdEstado,
                    IdUsuarioCreacion = request.IdUsuarioCreacion,
                    FechaCreacion = fecha
                };
                iOportunidadGanadoraDal.Add(OportunidadGanadora);
                iOportunidadGanadoraDal.UnitOfWork.Commit();

                respuesta.Id = OportunidadGanadora.Id;
                respuesta.Mensaje = MensajesGeneralNegocio.AgregarOportunidad;
                respuesta.TipoRespuesta = Proceso.Valido;
            }
            return respuesta;
        }

        public ProcesoResponse ValidarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            var oportunidad = iOportunidadGanadoraDal.Get(request.Id);
            oportunidad.IdEstado = request.IdEstado;
            oportunidad.IdUsuarioEdicion = request.IdUsuarioEdicion;
            oportunidad.FechaEdicion = request.FechaEdicion;

            iOportunidadGanadoraDal.Modify(oportunidad);
            iOportunidadGanadoraDal.UnitOfWork.Commit();

            respuesta.Id = oportunidad.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralNegocio.ValidarOportunidadGanadora;
            return respuesta;
        }

        public ProcesoResponse EliminarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            var oportunidad = iOportunidadGanadoraDal.Get(request.Id);
            oportunidad.IdEstado = Generales.Estados.Inactivo;
            oportunidad.IdUsuarioEdicion = request.IdUsuarioEdicion;
            oportunidad.FechaEdicion = request.FechaEdicion;

            iOportunidadGanadoraDal.Modify(oportunidad);
            iOportunidadGanadoraDal.UnitOfWork.Commit();

            respuesta.Id = oportunidad.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralNegocio.EliminarOportunidadGanadora;
            return respuesta;
        }

        public List<OportunidadGanadoraDtoResponse> ListarOportunidadGanadora(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraDal.ListarOportunidadGanadora(request);
        }

        public BandejaOportunidadPaginadoDtoResponse ListarBandejaOportunidadPaginado(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraDal.ListarBandejaOportunidadPaginado(request);
        }

        public RPAEquiposServicioDtoResponse ObtenerRPAEquiposServicioPorIdOferta_IdRPASerNroOfer(RPAEquiposServicioDtoRequest request)
        {
            return iOportunidadGanadoraDal.ObtenerRPAEquiposServicioPorIdOferta_IdRPASerNroOfer(request);
        }

        public OportunidadGanadoraPaginadoDtoResponse ObtenerListaCodigo()
        {
            var Lista = iOportunidadGanadoraDal.ObtenerListaCodigo();

            foreach (var item in Lista.ListaOportunidadGanadoraPaginadoDtoResponse)
            {
                int Id = item.Id;
                int IdEstado = Util.Constantes.Negocio.EstadosRPA.EnProceso;
                int IdUsuarioEdicion = 0;
                var valor = ActualizarEstadoOportunidad(Id, IdEstado, IdUsuarioEdicion);
            }
            return Lista;
        }

        public OportunidadGanadoraPaginadoDtoResponse ObtenerListaOportunidad()
        {
            var Lista = iOportunidadGanadoraDal.ObtenerListaOportunidad();

            foreach (var item in Lista.ListaOportunidadGanadoraPaginadoDtoResponse)
            {
                int Id = item.Id;
                int IdEstado = Util.Constantes.Negocio.EstadosRPA.EnProceso;
                int IdUsuarioEdicion = 0;
                var valor = ActualizarEstadoOportunidad(Id, IdEstado, IdUsuarioEdicion);
            }
            return Lista;
        }

        public ProcesoResponse GenerarCodificacionRPA(CodificacionRPA request)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                #region Registro de cliente
                    ClienteDtoRequest ob = new ClienteDtoRequest();
                    ob.NumeroIdentificadorFiscal = request.NumeroIdentificadorFiscal;

                    var clientes = ObtenerClientePorRuc(ob);

                    if (clientes == null)
                    {
                        var oClientes = new Cliente()
                        {
                            CodigoCliente = request.CodigoCliente,
                            Descripcion = request.Descripcion,
                            IdSector = 3,
                            GerenteComercial = "",
                            IdDireccionComercial = 3,
                            IdEstado = Estados.Activo,
                            IdUsuarioCreacion = request.IdUsuarioCreacion,
                            FechaCreacion = request.FechaCreacion,
                            IdUsuarioEdicion = request.IdUsuarioEdicion,
                            IdTipoIdentificadorFiscalTm = 1,
                            NumeroIdentificadorFiscal = request.NumeroIdentificadorFiscal,
                            Email = "",
                            Direccion = request.Direccion,
                            IdTipoEntidad = 1,
                            DireccionLegal = request.DireccionLegal,
                        };
                        iClienteDal.Add(oClientes);
                        iClienteDal.UnitOfWork.Commit();
                    }
                    else
                    {
                        var oClientes = new Cliente()
                        {
                            IdCliente = clientes.IdCliente,
                            CodigoCliente = request.CodigoCliente,
                            Descripcion = request.Descripcion,
                            IdSector = clientes.IdSector,
                            GerenteComercial = "",
                            IdDireccionComercial = clientes.IdDireccionComercial,
                            IdEstado = Estados.Activo,
                            IdUsuarioCreacion = clientes.IdUsuarioCreacion,
                            FechaCreacion = clientes.FechaCreacion,
                            IdUsuarioEdicion = 0,
                            FechaEdicion = DateTime.Now,
                            IdTipoIdentificadorFiscalTm = 1,
                            NumeroIdentificadorFiscal = request.NumeroIdentificadorFiscal,
                            Email = "",
                            Direccion = request.Direccion,
                            IdTipoEntidad = 1,
                            DireccionLegal = request.DireccionLegal,
                        };
                        iClienteDal.Modify(oClientes);
                        iClienteDal.UnitOfWork.Commit();
                    }
                #endregion

                #region Registro IsisNroOferta
                    IsisNroOfertaDtoRequest oIsis = new IsisNroOfertaDtoRequest();
                    oIsis.Per = request.PER;
                    oIsis.Nro_oferta = request.Nro_oferta;

                    var oIsisNro = ObtenerIsisNroOfertaPorNroPer_NroOferta(oIsis);

                    var oIsisNroOfertas = new IsisNroOferta()
                    {
                        Id = oIsisNro.Id,
                        Nro_oferta = request.Nro_oferta,
                        Nro_version_Oferta = request.Nro_version_Oferta,
                        Id_OportunidadGanadora = oIsisNro.Id_OportunidadGanadora,
                        IdEstado = Estados.Activo,
                        IdUsuarioCreacion = oIsisNro.IdUsuarioCreacion,
                        FechaCreacion = oIsisNro.FechaCreacion,
                        IdUsuarioEdicion = 0,
                        FechaEdicion = DateTime.Now,
                    };
                    iIsisNroOfertaDal.Modify(oIsisNroOfertas);
                    iIsisNroOfertaDal.UnitOfWork.Commit();
                #endregion
                    
                #region Registro OportunidadGanadora

                    OportunidadGanadoraDtoRequest oGanadora = new OportunidadGanadoraDtoRequest();
                    oGanadora.PER = request.PER;

                    var oOportunidad = ObtenerOportunidadGanadora(oGanadora);

                    var oOportunidadGanadora = new OportunidadGanadora()
                    {
                        Id = oOportunidad.Id,
                        PER = request.PER,
                        Ruc = request.NumeroIdentificadorFiscal,
                        CasoDerivado = "",
                        IdEtapa = 0,
                        TipoOportunidad = request.TipoOportunidad,
                        SegmentoCliente = request.SegmentoCliente,
                        FechaCreacionOportunidad = request.FechaCreacionOportunidad,
                        PlazoContrato = request.PlazoContrato,
                        TipoPlazoContrato = request.TipoPlazoContrato,
                        CreadorOferta = request.CreadorOferta,
                        FechaCreacionOferta = request.FechaCreacionOferta,
                        ComercialOferta = request.ComercialOferta,
                        IdEstado = Estados.Activo,
                        FechaCreacion = oOportunidad.FechaCreacion,
                        IdUsuarioCreacion = oOportunidad.IdUsuarioCreacion,
                        IdUsuarioEdicion = 0,
                        FechaEdicion = DateTime.Now,
                        EstadoOportunidad = request.EstadoOportunidad,
                        EtapaOportunidad = request.EtapaOportunidad,
                    };
                    iOportunidadGanadoraDal.Modify(oOportunidadGanadora);
                    iOportunidadGanadoraDal.UnitOfWork.Commit();
                #endregion
                    
                #region Registro RPAServiciosxNroOferta

                    RPAServiciosxNroOfertaDtoRequest oNroOferta = new RPAServiciosxNroOfertaDtoRequest();
                    oNroOferta.Id_Oferta = oIsisNro.Id;

                    var lista = ObtenerRPAServiciosxNroOfertaPorId_Oferta(oNroOferta);

                    int Id_RPAServOferta = 0;
                    if (lista == null)
                    {
                        var oRPAServiciosxNroOferta = new RPAServiciosxNroOferta()
                        {
                            NombreServicio = request.NombreServicio,
                            Id_Oferta = oIsisNro.Id,
                            Contacto_Id = 0,
                            TipoServicio = 0,
                            Accion = request.Accion,
                            MedioTx = "",
                            TipoCircuito = "",
                            LDN = "",
                            Realtime = "",
                            Video = "",
                            Caudal_oro = "",
                            Caudal_plata = "",
                            Caudal_platino = "",
                            Caudal_Bronce = "",
                            Caudal_VRF = "",
                            VA_Servicio = "",
                            VA_Velocidad = "",
                            VA_Acción = "",
                            VA_MedioTX = "",
                            VA_TipoCircuito = "",
                            IdEstado = Estados.Activo,
                            IdUsuarioCreacion = 0,
                            FechaCreacion = DateTime.Now,
                            Cantidad = request.Cantidad,
                            Velocidad = request.Velocidad,
                            PagoUnico = request.PagoUnico,
                            PagoRecurrente = request.PagoRecurrente,
                        };
                        iRPAServiciosxNroOferta.Add(oRPAServiciosxNroOferta);
                        iRPAServiciosxNroOferta.UnitOfWork.Commit();
                        Id_RPAServOferta = oRPAServiciosxNroOferta.Id;
                    }
                    else
                    {
                        var oRPAServiciosxNroOferta = new RPAServiciosxNroOferta()
                        {
                            Id = lista.Id,
                            NombreServicio = request.NombreServicio,
                            Id_Oferta = oIsisNro.Id,
                            Contacto_Id = 0,
                            TipoServicio = 0,
                            Accion = request.Accion,
                            MedioTx = "",
                            TipoCircuito = "",
                            LDN = "",
                            Realtime = "",
                            Video = "",
                            Caudal_oro = "",
                            Caudal_plata = "",
                            Caudal_platino = "",
                            Caudal_Bronce = "",
                            Caudal_VRF = "",
                            VA_Servicio = "",
                            VA_Velocidad = "",
                            VA_Acción = "",
                            VA_MedioTX = "",
                            VA_TipoCircuito = "",
                            IdEstado = Estados.Activo,
                            IdUsuarioCreacion = 0,
                            FechaCreacion = DateTime.Now,
                            Cantidad = request.Cantidad,
                            Velocidad = request.Velocidad,
                            PagoUnico = request.PagoUnico,
                            PagoRecurrente = request.PagoRecurrente,
                        };
                        iRPAServiciosxNroOferta.Modify(oRPAServiciosxNroOferta);
                        iRPAServiciosxNroOferta.UnitOfWork.Commit();
                        Id_RPAServOferta = oRPAServiciosxNroOferta.Id;
                    }

                #endregion

                #region Registro RPAEquiposServicio

                    RPAEquiposServicioDtoRequest objRPA = new RPAEquiposServicioDtoRequest();

                    objRPA.Id_RPAServOferta = Id_RPAServOferta;
                    objRPA.Id_Oferta = oIsisNro.Id;

                    var objasas = ObtenerRPAEquiposServicioPorIdOferta_IdRPASerNroOfer(objRPA);


                    if (objasas == null)
                    {
                        var oRPAEquiposServicio = new RPAEquiposServicio()
                        {
                            Id_Oferta = oIsisNro.Id,
                            Id_RPAServOferta = Id_RPAServOferta,
                            Cod_Equipo = "",
                            Tipo = "",
                            Marca = "",
                            Modelo = "",
                            Componente_1 = "",
                            Tipo_de_acceso = "",
                            Meses = "",
                            Con_Valor_Agregado = "",
                            IdEstado = Estados.Activo,
                            IdUsuarioCreacion = 0,
                            FechaCreacion = DateTime.Now,
                            ValorAgregado = request.ValorAgregado,
                            Equipos1 = request.Equipos1,
                            Equipos2 = request.Equipos2,
                        };
                        iRPAEquiposServicio.Add(oRPAEquiposServicio);
                        iRPAEquiposServicio.UnitOfWork.Commit();
                    }
                    else
                    {
                        var oRPAEquiposServicio = new RPAEquiposServicio()
                        {
                            Id = objasas.Id,
                            Id_Oferta = oIsisNro.Id,
                            Id_RPAServOferta = Id_RPAServOferta,
                            Cod_Equipo = "",
                            Tipo = "",
                            Marca = "",
                            Modelo = "",
                            Componente_1 = "",
                            Tipo_de_acceso = "",
                            Meses = "",
                            Con_Valor_Agregado = "",
                            IdEstado = Estados.Activo,
                            IdUsuarioCreacion = 0,
                            FechaCreacion = DateTime.Now,
                            ValorAgregado = request.ValorAgregado,
                            Equipos1 = request.Equipos1,
                            Equipos2 = request.Equipos2,
                        };
                        iRPAEquiposServicio.Modify(oRPAEquiposServicio);
                        iRPAEquiposServicio.UnitOfWork.Commit();
                    }
                #endregion

                #region Registro RPASisegoDetalle

                    SisegoCotizadoDtoRequest obj = new SisegoCotizadoDtoRequest();

                    obj.PER = request.PER;
                    obj.codigo_sisego = request.codigo_sisego;

                    var sisego = ObtenerSisegoCotizadoPorIdOporNroSisego(obj);

                    RPASisegoDetalleDtoRequest oSigDet = new RPASisegoDetalleDtoRequest();
                    oSigDet.Id_sisego = sisego.Id;

                    var sds = ObtenerRPASisegoDetallePorIdSisego(oSigDet);
                    int IdRPASisegoDetalle;

                    if (sds==null)
                    {
                        var RPASisegoDetalle = new RPASisegoDetalle()
                        {
                            Id_sisego = sisego.Id,
                            Latitud = request.Latitud,
                            Longitud = request.Longitud,
                            Departamento = request.Departamento,
                            Provincia = request.Provincia,
                            Distrito = request.Distrito,
                            Ciudad = request.Ciudad,
                            TipoSede = "",
                            Direccion = request.Direccion,
                            Via = request.Via,
                            Numero = request.Numero,
                            Nro_Piso = request.Nro_Piso,
                            Interior = request.Interior,
                            Lote = request.Lote,
                            Manzana = request.Manzana,
                            IdEstado = Estados.Activo,
                            IdUsuarioCreacion = 0,
                            FechaCreacion = DateTime.Now,
                            MedioTransmision = request.MedioTransmision,
                            CreadorSisego = request.CreadorSisego,
                        };
                        iRPASisegoDetalle.Add(RPASisegoDetalle);
                        iRPASisegoDetalle.UnitOfWork.Commit();
                        IdRPASisegoDetalle = RPASisegoDetalle.Id;
                    }
                    else
                    {
                        var RPASisegoDetalle = new RPASisegoDetalle()
                        {
                            Id = sds.Id,
                            Id_sisego = sisego.Id,
                            Latitud = request.Latitud,
                            Longitud = request.Longitud,
                            Departamento = request.Departamento,
                            Provincia = request.Provincia,
                            Distrito = request.Distrito,
                            Ciudad = request.Ciudad,
                            TipoSede = "",
                            Direccion = request.Direccion,
                            Via = request.Via,
                            Numero = request.Numero,
                            Nro_Piso = request.Nro_Piso,
                            Interior = request.Interior,
                            Lote = request.Lote,
                            Manzana = request.Manzana,
                            IdEstado = Estados.Activo,
                            IdUsuarioCreacion = 0,
                            FechaCreacion = DateTime.Now,
                            MedioTransmision = request.MedioTransmision,
                            CreadorSisego = request.CreadorSisego,
                        };
                        iRPASisegoDetalle.Modify(RPASisegoDetalle);
                        iRPASisegoDetalle.UnitOfWork.Commit();
                        IdRPASisegoDetalle = RPASisegoDetalle.Id;
                    }
                #endregion

                #region Registro RPASisegoDatosContacto

                    RPASisegoDatosContactoDtoRequest oCont = new RPASisegoDatosContactoDtoRequest();
                    oCont.Id_RPASisegoDetalle = IdRPASisegoDetalle;
                    oCont.Contacto = request.Contacto;

                    var oContact = ObtenerRPASisegoDatosContactoPorId_RPASisegoDetalle(oCont);

                    if (oContact==null)
                    {
                        var oRPASisegoDatosContacto = new RPASisegoDatosContacto()
                        {
                            Id_RPASisegoDetalle = IdRPASisegoDetalle,
                            Contacto = request.Contacto,
                            Telefono_1 = request.Telefono_1,
                            Telefono_2 = request.Telefono_2,
                            Telefono_3 = request.Telefono_3,
                            IdEstado = Estados.Activo,
                            IdUsuarioCreacion = 0,
                            FechaCreacion = DateTime.Now,
                            Contacto_2 = request.Contacto_2,
                            Telefono1_Contacto2 = request.Telefono1_Contacto2,
                            Telefono2_Contacto2 = request.Telefono2_Contacto2,
                            Telefono3_Contacto2 = request.Telefono3_Contacto2,
                        };
                        iRPASisegoDatosContacto.Add(oRPASisegoDatosContacto);
                        iRPASisegoDatosContacto.UnitOfWork.Commit();
                    }
                    else
                    {
                        var oRPASisegoDatosContacto = new RPASisegoDatosContacto()
                        {
                            Id= oContact.Id,
                            Id_RPASisegoDetalle = IdRPASisegoDetalle,
                            Contacto = request.Contacto,
                            Telefono_1 = request.Telefono_1,
                            Telefono_2 = request.Telefono_2,
                            Telefono_3 = request.Telefono_3,
                            IdEstado = Estados.Activo,
                            IdUsuarioCreacion = 0,
                            FechaCreacion = DateTime.Now,
                            Contacto_2 = request.Contacto_2,
                            Telefono1_Contacto2 = request.Telefono1_Contacto2,
                            Telefono2_Contacto2 = request.Telefono2_Contacto2,
                            Telefono3_Contacto2 = request.Telefono3_Contacto2,
                        };
                        iRPASisegoDatosContacto.Modify(oRPASisegoDatosContacto);
                        iRPASisegoDatosContacto.UnitOfWork.Commit();
                    }
                #endregion

                respuesta.Id = oOportunidadGanadora.Id;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = General.RegistraOk;
                scope.Complete();
            }
            return respuesta;
        }

        public ProcesoResponse GenerarCierreOfertaRPA(CierreDeOfertaRPA request)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {

                #region --- OportunidadGanadora ---

                OportunidadGanadoraDtoRequest oGanadora = new OportunidadGanadoraDtoRequest();
                oGanadora.PER = request.PER;

                var oOportunidad = ObtenerOportunidadGanadora(oGanadora);
                

                var oOportunidadGanadora = new OportunidadGanadora()
                {
                    ///Etapa
                    //Estado
                    //Numero_oferta
                    //Version_oferta
                    //Razon_social
                    //Dirección_legal
                    TipoOportunidad =  request.Tipo_oportunidad,
                    //Plazo_contrato
                    //Tipo_plazo_contrato
                    //Creador_oferta
                    Ruc = request.Numero_ruc,
                    ComercialOferta = request.Comercial_oferta,
                    FechaCreacionOferta = request.Fecha_creacion_oferta,
                    //Cantidad_servicio
                    //Servicio
                    //Velocidad_servicio
                    PER= request.Numero_oportunidad
                    //Codigo_equipo
                    //Departamento_instalación
                    //Provincia_instalación
                    //Distrito_instalación
                    //Tipo_Via_instalación
                    //Dirección_instalación
                    //Latitud_instalación
                    //Longitud_instalación
                    //Numero_instalación
                    //Piso_instalación
                    //Interior_instalación
                    //Manzana_instalación
                    //Lote_instalación
                    //Direccción de cobranza

                };

                iOportunidadGanadoraDal.Modify(oOportunidadGanadora);
                iOportunidadGanadoraDal.UnitOfWork.Commit();
                #endregion

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = General.RegistraOk;
                scope.Complete();
            }
            return respuesta;
        }

        public ProcesoResponse GenerarEmisionCircuitoRPA(EmisionCircuitoRPA request)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = General.RegistraOk;
                scope.Complete();
            }
            return respuesta;
        }

        public ClienteDtoResponse ObtenerClientePorRuc(ClienteDtoRequest request)
        {
            return iOportunidadGanadoraDal.ObtenerClientePorRuc(request);
        }

        public SisegoCotizadoDtoResponse ObtenerSisegoCotizadoPorIdOporNroSisego(SisegoCotizadoDtoRequest request)
        {
            return iOportunidadGanadoraDal.ObtenerSisegoCotizadoPorIdOporNroSisego(request);
        }

        public IsisNroOfertaDtoResponse ObtenerIsisNroOfertaPorNroPer_NroOferta(IsisNroOfertaDtoRequest request)
        {
            return iOportunidadGanadoraDal.ObtenerIsisNroOfertaPorNroPer_NroOferta(request);
        }

        public ProcesoResponse ActualizarEstadoOportunidad(int Id, int? IdEstado,int? IdUsuarioEdicion)
        {
            var objOportunidadGanadora = new OportunidadGanadora()
            {
                Id = Id,
                IdEstado = IdEstado,
                FechaEdicion = DateTime.Now,
                IdUsuarioEdicion = IdUsuarioEdicion
            };

            iOportunidadGanadoraDal.ActualizarPorCampos(objOportunidadGanadora, x => x.Id, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
            iOportunidadGanadoraDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;

            return respuesta;
        }

        public ProcesoResponse ActualizarEtapaCierreOportunidad(OportunidadGanadoraDtoRequest request)
        {
            var objOportunidadGanadora = new OportunidadGanadora()
            {
                Id = request.Id,
                PER = request.PER,
                IdEtapa = request.IdEtapa,
                IdEstado = Util.Constantes.Negocio.EstadosRPA.Pendiente,
                FechaEdicion = DateTime.Now,
                IdUsuarioEdicion = request.IdUsuarioEdicion
            };

            iOportunidadGanadoraDal.ActualizarPorCampos(objOportunidadGanadora, x => x.Id, x => x.IdEtapa, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
            iOportunidadGanadoraDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = "Se Cerro Correctamente Oportunidad Ganadora";
            return respuesta;
        }
        
        public OportunidadGanadoraDtoResponse ObtenerOportunidadxPer(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraDal.ObtenerOportunidadxPer(request);
        }

        public ProcesoResponse ActualizarEstadoRPA(string Numero_oportunidad, string EstadoOportunidad)
        {


            int IdEstadoRpa = 1;
            switch (EstadoOportunidad)
            {
                case "Inactivo":
                    IdEstadoRpa = Tgs.SDIO.Util.Constantes.Negocio.EstadosRPA.Inactivo;
                    break;
                case "Por Enviar":
                    IdEstadoRpa = Tgs.SDIO.Util.Constantes.Negocio.EstadosRPA.PorEnviar;
                    break;
                case "Pendiente":
                    IdEstadoRpa = Tgs.SDIO.Util.Constantes.Negocio.EstadosRPA.Pendiente;
                    break;
                case "En Proceso":
                    IdEstadoRpa = Tgs.SDIO.Util.Constantes.Negocio.EstadosRPA.EnProceso;
                    break;
                case "Expirado":
                    IdEstadoRpa = Tgs.SDIO.Util.Constantes.Negocio.EstadosRPA.Expirado;
                    break;
                case "Observado":
                    IdEstadoRpa = Tgs.SDIO.Util.Constantes.Negocio.EstadosRPA.Observado;
                    break;
                case "Procesado":
                    IdEstadoRpa = Tgs.SDIO.Util.Constantes.Negocio.EstadosRPA.Procesado;
                    break;
                case "Cancelado":
                    IdEstadoRpa = Tgs.SDIO.Util.Constantes.Negocio.EstadosRPA.Cancelado;
                    break;
            }

            OportunidadGanadoraDtoRequest obj = new OportunidadGanadoraDtoRequest();
            obj.PER = Numero_oportunidad;

            var oportunidad = ObtenerOportunidadxPer(obj);

            var objOportunidadGanadora = new OportunidadGanadora()
            {
                IdEstado = IdEstadoRpa,
                Id = oportunidad.Id,
                EstadoOportunidad = EstadoOportunidad,
                FechaEdicion = DateTime.Now,
                IdUsuarioEdicion = 0
            };

            iOportunidadGanadoraDal.ActualizarPorCampos(objOportunidadGanadora, x => x.Id, x => x.EstadoOportunidad, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
            iOportunidadGanadoraDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }

        public OportunidadGanadoraDtoResponse ObtenerOportunidadById(OportunidadGanadoraDtoRequest request)
        {
            return iOportunidadGanadoraDal.ObtenerOportunidadById(request);
        }

        public RPASisegoDetalleDtoResponse ObtenerRPASisegoDetallePorIdSisego(RPASisegoDetalleDtoRequest request)
        {
            return iOportunidadGanadoraDal.ObtenerRPASisegoDetallePorIdSisego(request);
        }

        public RPASisegoDatosContactoDtoResponse ObtenerRPASisegoDatosContactoPorId_RPASisegoDetalle(RPASisegoDatosContactoDtoRequest request)
        {
            return iOportunidadGanadoraDal.ObtenerRPASisegoDatosContactoPorId_RPASisegoDetalle(request);
        }

        public RPAServiciosxNroOfertaDtoResponse ObtenerRPAServiciosxNroOfertaPorId_Oferta(RPAServiciosxNroOfertaDtoRequest request)
        {
            return iOportunidadGanadoraDal.ObtenerRPAServiciosxNroOfertaPorId_Oferta(request);
        }

    }
}
