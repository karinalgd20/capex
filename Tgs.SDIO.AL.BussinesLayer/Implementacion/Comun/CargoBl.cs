﻿using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class CargoBl : ICargoBl
    {
        private readonly ICargoDal iCargoDal;

        public CargoBl(ICargoDal cargoDal)
        {
            iCargoDal = cargoDal;
        }

        public List<ListaDtoResponse> ListarCargos(CargoDtoRequest cargoRequest)
        {
            var query = iCargoDal.GetFilteredAsNoTracking(x => x.IdEstado == cargoRequest.IdEstado).ToList();

            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.Id.ToString(),
                Descripcion = x.Descripcion
            }).OrderBy(x=>x.Descripcion).ToList();
        }

        public List<ListaDtoResponse> ListarComboCargo()
        {
            return iCargoDal.ListarComboCargo();
        }
    }
}
