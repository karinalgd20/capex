﻿using System;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Mensajes.Comun;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class RecursoLineaNegocioBl : IRecursoLineaNegocioBl
    {
        private readonly IRecursoLineaNegocioDal iRecursoLineaNegocioDal;
        private readonly IRecursoDal iRecursoDal;

        ProcesoResponse respuesta = new ProcesoResponse();

        public RecursoLineaNegocioBl(IRecursoLineaNegocioDal recursoLineaNegocioDal , IRecursoDal recursoDal)
        {
            iRecursoLineaNegocioDal = recursoLineaNegocioDal;
            iRecursoDal = recursoDal;
        }

        public RecursoLineaNegocioPaginadoDtoResponse ListarRecursoLineaNegocioPaginado(RecursoLineaNegocioDtoRequest recursoLineaRequest)
        { 
            return iRecursoLineaNegocioDal.ListarRecursoLineaNegocioPaginado(recursoLineaRequest);
        }

        public ProcesoResponse ActualizarEstadoRecursoLineaNegocio(RecursoLineaNegocioDtoRequest recursoLineaRequest)
        {
            var recursoLineaNegocio = new RecursoLineaNegocio()
            {
                Id = recursoLineaRequest.Id,
                IdEstado = recursoLineaRequest.IdEstado,
                IdUsuarioEdicion = recursoLineaRequest.IdUsuarioEdicion,
                FechaEdicion = recursoLineaRequest.FechaEdicion
            };

            iRecursoLineaNegocioDal.ActualizarPorCampos(recursoLineaNegocio,
                                                        x => x.IdEstado,
                                                        x => x.IdUsuarioEdicion, 
                                                        x => x.FechaEdicion);

            iRecursoLineaNegocioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.DatosRegistrados; 

            return respuesta;
        }

        public ProcesoResponse RegistrarRecursoLineaNegocio(RecursoLineaNegocioDtoRequest recursoLineaRequest)
        {
            var consultaRecurso = iRecursoDal.GetFilteredAsNoTracking(
                x => x.IdUsuarioRais == recursoLineaRequest.IdUsuarioRais
                ).FirstOrDefault();

            var consultaLineaNegocio = iRecursoLineaNegocioDal.GetFilteredAsNoTracking(
                x => x.IdLineaNegocio == recursoLineaRequest.IdLineaNegocio &&
                x.IdRecurso == consultaRecurso.IdRecurso
                ).FirstOrDefault();

            if (consultaLineaNegocio != null)
            {
                return new ProcesoResponse
                {
                    TipoRespuesta = Proceso.Invalido,
                    Mensaje = ErrorValidacionComun.RLN0001
                };
            } 

            var recursoLineaNegocio = new RecursoLineaNegocio()
            {
                IdRecurso= consultaRecurso.IdRecurso,
                IdLineaNegocio = recursoLineaRequest.IdLineaNegocio,
                IdEstado = Estados.Activo,
                IdUsuarioCreacion = recursoLineaRequest.IdUsuarioCreacion,
                FechaCreacion = DateTime.Now                
            };

            iRecursoLineaNegocioDal.Add(recursoLineaNegocio);
            iRecursoLineaNegocioDal.UnitOfWork.Commit();

            respuesta.Id = recursoLineaNegocio.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;
        }

    }
}
