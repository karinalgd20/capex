﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Mensajes.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class EmpresaBl : IEmpresaBl
    {
        readonly IEmpresaDal iempresaDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public EmpresaBl(IEmpresaDal empresaDal)
        {
            iempresaDal = empresaDal;
        }
        public List<ListaDtoResponse> ListarComboEmpresa()
        {
            return iempresaDal.ListarComboEmpresa();
        }
    }
}
