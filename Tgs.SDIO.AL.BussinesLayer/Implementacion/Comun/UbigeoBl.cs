﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using System.Linq;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class UbigeoBl : IUbigeoBl
    {
        readonly IUbigeoDal iubigeoDal;

        public UbigeoBl(IUbigeoDal ubigeoDal)
        {
            iubigeoDal = ubigeoDal;
        }

        public List<ListaDtoResponse> ListarComboUbigeoDepartamento()
        {
            return iubigeoDal.ListarComboUbigeoDepartamento();
        }

        public List<ListaDtoResponse> ListarComboUbigeoProvincia(UbigeoDtoRequest request)
        {
            return iubigeoDal.ListarComboUbigeoProvincia(request);
        }

        public List<ListaDtoResponse> ListarComboUbigeoDistrito(UbigeoDtoRequest request)
        {
            return iubigeoDal.ListarComboUbigeoDistrito(request);
        }
        public UbigeoDtoResponse ObtenerPorCodigoDistrito(UbigeoDtoRequest UbigeoDtoRequest)
        {
            if (UbigeoDtoRequest.CodigoDistrito != null)
            {
                var objGrupo = iubigeoDal.GetFiltered(x => x.CodigoDistrito == UbigeoDtoRequest.CodigoDistrito);
                return objGrupo.Select(x => new UbigeoDtoResponse
                {
                    CodigoDepartamento = x.CodigoDepartamento,
                    CodigoProvincia = x.CodigoProvincia,
                    CodigoDistrito = x.CodigoDistrito
                }).Single();


            }

            else
            {

                return null;
            }




        }


    }
}
