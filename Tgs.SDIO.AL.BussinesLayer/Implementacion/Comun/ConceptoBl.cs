﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class ConceptoBl : IConceptoBl
    {
        readonly IConceptoDal iConceptoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ConceptoBl(IConceptoDal IConceptoDal)
        {
            iConceptoDal = IConceptoDal;

        }

        public ProcesoResponse ActualizarConcepto(ConceptoDtoRequest concepto)
        {
         
                var objConcepto = new Concepto()
                {
                    IdConcepto = concepto.IdConcepto,
                    Descripcion = concepto.Descripcion,
                    IdTipoConcepto = concepto.IdTipoConcepto,
                    IdEstado = concepto.IdEstado,
                    IdUsuarioEdicion = concepto.IdUsuarioEdicion,
                    FechaEdicion = concepto.FechaEdicion
                };

                iConceptoDal.Modify(objConcepto);
                iConceptoDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (concepto.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarConcepto : MensajesGeneralComun.ActualizarConcepto;
        
          
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, null, Generales.Sistemas.Comun);
      

            return respuesta;
        }

        public List<ConceptoDtoResponse> ListarBandejaConceptos(ConceptoDtoRequest concepto)
        {
            return iConceptoDal.ListarBandejaConceptos(concepto);
        }

        public List<ConceptoDtoResponse> ListarConceptos(ConceptoDtoRequest concepto)
        {
            var query = iConceptoDal.GetFilteredAsNoTracking(x => x.IdConcepto == concepto.IdConcepto
            && concepto.IdEstado == concepto.IdEstado).ToList();
            return query.Select(x => new ConceptoDtoResponse
            {
                IdConcepto = x.IdConcepto,
                Depreciacion = x.Descripcion,
                IdTipoConcepto = Convert.ToInt32(x.IdTipoConcepto),
                IdEstado = x.IdEstado
            }).ToList();
        }

        public ConceptoDtoResponse ObtenerConcepto(ConceptoDtoRequest concepto)
        {
            var objConcepto = iConceptoDal.GetFiltered(x => x.IdConcepto == concepto.IdConcepto &&
                                                       x.IdEstado == concepto.IdEstado);
            return objConcepto.Select(x => new ConceptoDtoResponse
            {
                IdConcepto = x.IdConcepto,
                Depreciacion = x.Descripcion,
                IdTipoConcepto = Convert.ToInt32(x.IdTipoConcepto),
                IdEstado = x.IdEstado
            }).Single();

        }

        public ProcesoResponse RegistrarConcepto(ConceptoDtoRequest concepto)
        {
          
                var objConcepto = new Concepto()
                {
                    IdConcepto = concepto.IdConcepto,
                    Descripcion = concepto.Descripcion,
                    IdTipoConcepto = concepto.IdTipoConcepto,
                    IdEstado = concepto.IdEstado,
                    IdUsuarioCreacion = concepto.IdUsuarioCreacion,
                    FechaCreacion = Convert.ToDateTime(concepto.FechaCreacion) 
                };

                iConceptoDal.Add(objConcepto);
                iConceptoDal.UnitOfWork.Commit();

                respuesta.Id = objConcepto.IdConcepto;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarConcepto;
            
      
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, null, Generales.Sistemas.Comun);
           

            return respuesta;
        }
    }
}
