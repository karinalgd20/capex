﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ContratoMarcoBl : IContratoMarcoBl
    {
        readonly IContratoMarcoDal iContratoMarcoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ContratoMarcoBl(IContratoMarcoDal IContratoMarcoDal)
        {
            iContratoMarcoDal = IContratoMarcoDal;
        }

        public ContratoMarcoDtoResponsePaginado ListarContratoMarcoPaginado(ContratoMarcoDtoRequest contratomarco)
        {
            return iContratoMarcoDal.ListarContratoMarcoPaginado(contratomarco);
        }

        public ContratoMarcoDtoResponse ObtenerContratoMarcoPorId(ContratoMarcoDtoRequest request)
        {
            return iContratoMarcoDal.ObtenerContratoMarcoPorId(request);
        }

    }
}
