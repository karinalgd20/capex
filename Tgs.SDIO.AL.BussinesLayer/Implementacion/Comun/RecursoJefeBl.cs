﻿using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Mensajes.Comun;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class RecursoJefeBl: IRecursoJefeBl
    {
        private readonly IRecursoJefeDal iRecursoJefeDal;
        private readonly IRecursoDal iRecursoDal;

        ProcesoResponse respuesta = new ProcesoResponse();

        public RecursoJefeBl(IRecursoJefeDal recursoJefeDal, IRecursoDal recursoDal)
        {
            iRecursoJefeDal = recursoJefeDal;
            iRecursoDal = recursoDal;
        }

        public RecursoJefePaginadoDtoResponse ListarRecursoPorJefePaginado(RecursoJefeDtoRequest recursoJefeDtoRequest)
        {
            var consultaRecurso = iRecursoDal.GetFilteredAsNoTracking(
             x => x.IdUsuarioRais == recursoJefeDtoRequest.IdUsuarioRais
             ).FirstOrDefault();

            recursoJefeDtoRequest.IdJefe = consultaRecurso?.IdRecurso ?? 0;

            return iRecursoJefeDal.ListarRecursoPorJefePaginado(recursoJefeDtoRequest);
        }

        public ProcesoResponse ActualizarEstadoRecursoJefe(RecursoJefeDtoRequest recursoJefeDtoRequest)
        {
            var recursoJefe = new RecursoJefe()
            {
                Id= recursoJefeDtoRequest.Id,
                IdEstado = recursoJefeDtoRequest.IdEstado,
                IdUsuarioEdicion = recursoJefeDtoRequest.IdUsuarioEdicion,
                FechaEdicion = recursoJefeDtoRequest.FechaEdicion
            };

            iRecursoJefeDal.ActualizarPorCampos(recursoJefe,
                                                        x => x.IdEstado,
                                                        x => x.IdUsuarioEdicion,
                                                        x => x.FechaEdicion);

            iRecursoJefeDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.DatosRegistrados;

            return respuesta;
        }

        public ProcesoResponse RegistrarRecursoJefe(RecursoJefeDtoRequest recursoJefeDtoRequest)
        {
            var consultaRecurso = iRecursoDal.GetFilteredAsNoTracking(
               x => x.IdUsuarioRais == recursoJefeDtoRequest.IdUsuarioRais
               ).FirstOrDefault();
             
            var consultaJefe = iRecursoDal.GetFilteredAsNoTracking(
             x => x.IdUsuarioRais == recursoJefeDtoRequest.IdJefe
             ).FirstOrDefault();

            var idRecurso = consultaRecurso?.IdRecurso ?? 0;
            var idJefe = consultaJefe?.IdRecurso ?? 0;

            var consultaRecursoJefe = iRecursoJefeDal.GetFilteredAsNoTracking(
                x => x.IdRecurso == idRecurso &&
                x.IdJefe == idJefe
                ).FirstOrDefault();

            if (consultaRecursoJefe != null)
            {
                return new ProcesoResponse
                {
                    TipoRespuesta = Proceso.Invalido,
                    Mensaje = ErrorValidacionComun.RJ0001
                };
            }

            if (consultaRecurso==null)
            {
                return new ProcesoResponse
                {
                    TipoRespuesta = Proceso.Invalido,
                    Mensaje = ErrorValidacionComun.RJ0002
                };
            } 

            var recursoJefe = new RecursoJefe()
            {
                IdRecurso = consultaRecurso.IdRecurso,
                IdJefe = idJefe,
                IdEstado = recursoJefeDtoRequest.IdEstado,
                IdUsuarioCreacion = recursoJefeDtoRequest.IdUsuarioCreacion,
                FechaCreacion = recursoJefeDtoRequest.FechaCreacion
            };

            iRecursoJefeDal.Add(recursoJefe);
            iRecursoJefeDal.UnitOfWork.Commit();

            respuesta.Id = recursoJefe.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;
        }

    }
}
