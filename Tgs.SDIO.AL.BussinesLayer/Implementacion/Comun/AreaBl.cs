﻿using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class AreaBl : IAreaBl
    {
        private readonly IAreaDal iAreaDal;

        public AreaBl(IAreaDal areaDal)
        {
            iAreaDal = areaDal;
        }

        public List<ListaDtoResponse> ListarAreas(AreaDtoRequest areaRequest)
        {
            var query = iAreaDal.GetFilteredAsNoTracking(x => x.IdEstado == areaRequest.IdEstado).ToList();

            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdArea.ToString(),
                Descripcion = x.Descripcion
            }).OrderBy(x=>x.Descripcion).ToList();
        }

        public List<ListaDtoResponse> ListarComboArea()
        {
            return iAreaDal.ListarComboArea();
        }

    }
}
