﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class ServicioGrupoBl : IServicioGrupoBl
    {
        readonly IServicioGrupoDal iServicioGrupoDal;

        public ServicioGrupoBl(IServicioGrupoDal IServicioGrupoDal)
        {
            iServicioGrupoDal = IServicioGrupoDal;
        }

        public List<ListaDtoResponse> ListarServicioGrupo(ServicioGrupoDtoRequest serviciogrupo)
        {
            var objServicioGrupo = iServicioGrupoDal.GetFiltered(x => x.IdEstado == serviciogrupo.IdEstado);
            return objServicioGrupo.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdServicioGrupo.ToString(),
                Descripcion = x.Descripcion
            }).OrderBy(t => t.Descripcion).ToList();
        }
    }
}
