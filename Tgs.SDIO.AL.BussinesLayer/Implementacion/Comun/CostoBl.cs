﻿using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class CostoBl : ICostoBl
    {
        readonly ICostoDal iCostoDal;

        public CostoBl(ICostoDal ICostoDal)
        {
            iCostoDal = ICostoDal;
        }
        public List<CostoDtoResponse> ListarComboCosto(CostoDtoRequest medioCosto)
        {
            return iCostoDal.ListarComboCosto(medioCosto);
        }
        public List<CostoDtoResponse> ListarCostoPorMedioServicioGrupo(CostoDtoRequest costo)
        {
            return iCostoDal.ListarCostoPorMedioServicioGrupo(costo);
        }

        public CostoDtoResponse ObtenerCostoPorCodigo(CostoDtoRequest medioCosto)
        {
            return iCostoDal.ObtenerCostoPorCodigo(medioCosto);
        }
        public List<ListaDtoResponse> ListarCosto(CostoDtoRequest medioCosto)
        {
            return iCostoDal.ListarCosto(medioCosto);
        }

        public CostoDtoResponse ObtenerCosto(CostoDtoRequest medioCosto)
        {
            var objCosto = iCostoDal.GetFiltered(x => x.IdCosto == medioCosto.IdCosto);
            return objCosto.Select(x => new CostoDtoResponse
            {               
                IdCosto = x.IdCosto,
                IdTipoCosto = x.IdTipoCosto,
                Descripcion = x.Descripcion,
                Monto = x.Monto,
                VelocidadSubidaKBPS = x.VelocidadSubidaKBPS,
                PorcentajeGarantizado = x.PorcentajeGarantizado,
                PorcentajeSobresuscripcion = x.PorcentajeSobresuscripcion,
                CostoSegmentoSatelital = x.CostoSegmentoSatelital,
                InvAntenaHubUSD = x.InvAntenaHubUSD,
                AntenaCasaClienteUSD = x.AntenaCasaClienteUSD,
                Instalacion = x.Instalacion,
                IdUnidadConsumo = x.IdUnidadConsumo,
                IdTipificacion = x.IdTipificacion,
                CodigoModelo = x.CodigoModelo,
                Modelo = x.Modelo
            }).Single();

        }
    
    }
}
