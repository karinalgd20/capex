﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class MaestraBl : IMaestraBl
    {
        readonly IMaestraDal iMaestraDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public MaestraBl(IMaestraDal IMaestraDal)
        {
            iMaestraDal = IMaestraDal;
        }


        public ProcesoResponse ActualizarMaestra(MaestraDtoRequest maestra)
        {
            var objMaestra = new Maestra()
            {
                IdMaestra = maestra.IdMaestra,
                Descripcion = maestra.Descripcion,
                Valor = maestra.Valor,
                Comentario = maestra.Comentario,
                IdEstado = maestra.IdEstado,
                IdOpcion = maestra.IdOpcion,
                IdUsuarioEdicion = maestra.IdUsuarioEdicion,
                FechaEdicion = DateTime.Now
            };

            iMaestraDal.ActualizarPorCampos(objMaestra,
                                            x => x.Descripcion,
                                            x => x.Valor,
                                            x => x.Comentario,
                                            x => x.IdEstado,
                                            x => x.IdOpcion,
                                            x => x.IdUsuarioEdicion,
                                            x => x.FechaEdicion);

            iMaestraDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (maestra.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarMaestra : MensajesGeneralComun.ActualizarMaestra;

            return respuesta;
        }


        public List<ListaDtoResponse> ListarMaestraPorIdRelacion(MaestraDtoRequest maestra)
        {
            var query = iMaestraDal.GetFilteredAsNoTracking(x => x.IdRelacion == maestra.IdRelacion && x.IdEstado == maestra.IdEstado).OrderBy(t => t.Valor).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.Valor,
                Descripcion = x.Descripcion

            }).ToList();
        }

        public List<MaestraDtoResponse> ListarMaestraPorIdRelacion2(MaestraDtoRequest maestra)
        {
            var query = iMaestraDal.GetFilteredAsNoTracking(x => x.IdRelacion == maestra.IdRelacion && x.IdEstado == maestra.IdEstado).OrderBy(t => t.Valor).ToList();
            return query.Select(x => new MaestraDtoResponse
            {
                IdMaestra = x.IdMaestra,
                Descripcion = x.Descripcion,
                Valor = x.Valor,
                Valor2 = x.Valor2,
                Comentario = x.Comentario,
                IdEstado = x.IdEstado,

            }).ToList();
        }

        public List<MaestraDtoResponse> ObtenerMaestra(MaestraDtoRequest maestra)
        {
            var objMaestra = iMaestraDal.GetFiltered(x =>
                                                     x.IdRelacion == maestra.IdRelacion &&
                                                     x.IdEstado == maestra.IdEstado);

            return objMaestra.Select(x => new MaestraDtoResponse
            {
                IdMaestra = x.IdMaestra,
                Descripcion = x.Descripcion,
                Valor = x.Valor,
                Valor2 = x.Valor2,
                Comentario = x.Comentario,
                IdEstado = x.IdEstado,
                IdOpcion = x.IdOpcion

            }).ToList();

        }
        public List<ListaDtoResponse> ListarMaestraPorValor(MaestraDtoRequest maestra)
        {
            var objMaestra = iMaestraDal.GetFilteredAsNoTracking(x => x.IdRelacion == maestra.IdRelacion &&
                                                       x.IdEstado == maestra.IdEstado && x.Valor == maestra.Valor).OrderBy(t => t.Valor).ToList();
            return objMaestra.Select(x => new ListaDtoResponse
            {
                Codigo = x.Valor,
                Descripcion = x.Descripcion

            }).ToList();
        }
        public List<ListaDtoResponse> ListarMaestra(MaestraDtoRequest maestra)
        {
            var objMaestra = iMaestraDal.GetFilteredAsNoTracking(x => x.IdRelacion == maestra.IdRelacion &&
                                                       x.IdEstado == maestra.IdEstado && x.Valor2 == maestra.Valor2).OrderBy(t => t.Valor).ToList();
            return objMaestra.Select(x => new ListaDtoResponse
            {
                Codigo = x.Valor,
                Descripcion = x.Descripcion

            }).ToList();
        }


        public ProcesoResponse RegistrarMaestra(MaestraDtoRequest maestra)
        {
            var objMaestra = new Maestra()
            {
                IdRelacion = maestra.IdRelacion,
                Descripcion = maestra.Descripcion,
                Valor = maestra.Valor,
                Comentario = maestra.Comentario,
                IdEstado = maestra.IdEstado,
                IdOpcion = maestra.IdOpcion,
                IdUsuarioCreacion = maestra.IdUsuarioCreacion,
                FechaCreacion = DateTime.Now
            };

            iMaestraDal.Add(objMaestra);
            iMaestraDal.UnitOfWork.Commit();

            respuesta.Id = objMaestra.IdMaestra;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.RegistrarMaestra;
            return respuesta;
        }

        public List<ComboDtoResponse> ListarComboTiposRecursos()
        {
            return iMaestraDal.ListarComboTiposRecursos();
        }

    }
}
