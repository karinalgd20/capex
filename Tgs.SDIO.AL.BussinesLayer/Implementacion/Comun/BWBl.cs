﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class BWBl : IBWBl
    {
        readonly IBWDal iBWDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public BWBl(IBWDal IBWDal)



        {
            iBWDal = IBWDal;
        }

        public ProcesoResponse ActualizarBW(BWDtoRequest bw)
        {
            try
            {
                var objBW = new BW()
                {

                    IdBW = bw.IdBW,
                    Descripcion = bw.Descripcion,
                    BW_NUM = bw.BW_NUM,
                    Precio = bw.Precio,
                    IdLineaProducto = bw.IdLineaProducto,
                    IdPestana = bw.IdPestana,
                    IdGrupo = bw.IdGrupo,
                    IdEstado = bw.IdEstado,
                    IdUsuarioEdicion = bw.IdUsuarioEdicion,
                    FechaEdicion = bw.FechaEdicion
                };
                iBWDal.Modify(objBW);
                iBWDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (bw.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarBW : MensajesGeneralComun.ActualizarBW;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorBW, null, Generales.Sistemas.Comun);
            }


            return respuesta;
        }


        public List<BWDtoResponse> ListarBW(BWDtoRequest bw)
        {
            var query = iBWDal.GetFilteredAsNoTracking(x => x.IdBW == bw.IdBW && x.IdEstado == bw.IdEstado
            && x.IdPestana== bw.IdPestana && x.IdLineaProducto == bw.IdLineaProducto).ToList();
            return query.Select(x => new BWDtoResponse
            {
                IdBW = x.IdBW,
                Descripcion = x.Descripcion,
                BW_NUM = x.BW_NUM,
                IdEstado = x.IdEstado,
                Precio=x.Precio,
                IdLineaProducto=x.IdLineaProducto,
                IdGrupo=x.IdGrupo


            }).ToList();
        }

        public BWDtoResponse ObtenerBW(BWDtoRequest bw)
        {
            var objBW = iBWDal.GetFiltered(x => x.IdBW == bw.IdBW &&
                                                       x.IdEstado == bw.IdEstado);
            return objBW.Select(x => new BWDtoResponse
            {
                IdBW = x.IdBW,
                Descripcion = x.Descripcion,
                BW_NUM = x.BW_NUM,
                IdEstado = x.IdEstado,
                Precio = x.Precio,
                IdLineaProducto = x.IdLineaProducto,
                IdGrupo = x.IdGrupo
            }).Single();

        }

        public ProcesoResponse RegistrarBW(BWDtoRequest bw)
        {
            try
            {
                var objBW = new BW()
                {
                    IdBW = bw.IdBW,
                    Descripcion = bw.Descripcion,
                    BW_NUM = bw.BW_NUM,
                    IdEstado = bw.IdEstado,
                    Precio = bw.Precio,
                    IdLineaProducto = bw.IdLineaProducto,
                    IdGrupo = bw.IdGrupo,
                    IdPestana = bw.IdPestana,
                    IdUsuarioCreacion = bw.IdUsuarioCreacion,
                    FechaCreacion = bw.FechaCreacion
                };

                iBWDal.Add(objBW);
                iBWDal.UnitOfWork.Commit();

                respuesta.Id = objBW.IdBW;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarBW;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorBW, null, Generales.Sistemas.Comun);
            }

            return respuesta;
        }
    }
}
