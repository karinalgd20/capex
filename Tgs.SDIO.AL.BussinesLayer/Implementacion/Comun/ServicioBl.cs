﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class ServicioBl : IServicioBl 
    {
        readonly IServicioDal iServicioDal;
        readonly IServicioSubServicioDal iServicioSubServicioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ServicioBl(IServicioDal IServicioDal, IServicioSubServicioDal IServicioSubServicioDal)
        {
            iServicioDal = IServicioDal;
            iServicioSubServicioDal = IServicioSubServicioDal;
        }

        public ProcesoResponse ActualizarServicio(ServicioDtoRequest servicio)
        {

            var objServicio = new Servicio()
            {
                IdServicio = servicio.IdServicio,
                IdLineaNegocio = servicio.IdLineaNegocio,
                Descripcion = servicio.Descripcion,
                IdMedio = servicio.IdMedio,
                IdGrupo = servicio.IdGrupo,
                IdTipoEnlace = servicio.IdTipoEnlace,
                IdServicioCMI = servicio.IdServicioCMI,
                IdServicioGrupo = servicio.IdServicioGrupo,
                IdEstado =servicio.IdEstado,
                IdUsuarioEdicion = servicio.IdUsuarioEdicion,
                FechaEdicion = Convert.ToDateTime(servicio.FechaEdicion)


            };

            iServicioDal.ActualizarPorCampos(objServicio, x => x.IdServicio, x => x.IdLineaNegocio, x => x.Descripcion, x => x.IdMedio, x => x.IdGrupo, x => x.IdTipoEnlace, x => x.IdServicioCMI,
            x => x.IdEstado ,   x => x.IdUsuarioEdicion, x => x.FechaEdicion);
            iServicioDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.ActualizarServicio;
            respuesta.Id = objServicio.IdServicio;


            return respuesta;
        }

        public ProcesoResponse InhabilitarServicio(ServicioDtoRequest servicio)
        {

            var objServicio = new Servicio()
            {
                IdServicio = servicio.IdServicio,
                IdEstado = servicio.IdEstado,
                IdUsuarioEdicion = servicio.IdUsuarioEdicion,
                FechaEdicion = servicio.FechaEdicion

            };

            iServicioDal.ActualizarPorCampos(objServicio, x => x.IdServicio, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
            iServicioDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.EliminarServicio ;
            respuesta.Id = objServicio.IdServicio;


            return respuesta;
        }
        public ServicioPaginadoDtoResponse ListaServicioPaginado(ServicioDtoRequest servicio)
        {
            return iServicioDal.ListaServicioPaginado(servicio);

        }

        public List<ListaDtoResponse> ListarServicios(ServicioDtoRequest servicio)
        {
            var query = iServicioDal.GetFilteredAsNoTracking(x => x.IdEstado == servicio.IdEstado
          ).ToList();
            return query.Select(x => new ListaDtoResponse
            {

                Codigo = x.IdServicio.ToString(),
                Descripcion = x.Descripcion

            }).OrderBy(x => x.Descripcion).ToList();
        }

        public ServicioDtoResponse ObtenerServicio(ServicioDtoRequest servicio)
        {
            var idServicioSubServicio = iServicioSubServicioDal.ObtenerIdServicioSubServicio(servicio.IdServicio);
            var objServicio = iServicioDal.GetFiltered(x => x.IdServicio == servicio.IdServicio);
            return objServicio.Select(x => new ServicioDtoResponse
            {
                IdServicio = x.IdServicio,
                Descripcion = x.Descripcion,
                IdMedio = x.IdMedio,
                IdGrupo=x.IdGrupo,
                IdEstado=x.IdEstado,
                IdLineaNegocio = x.IdLineaNegocio,
                IdTipoEnlace = x.IdTipoEnlace,
                IdServicioCMI = x.IdServicioCMI,
                IdServicioGrupo = x.IdServicioGrupo,
                IdServicioSubServicio = idServicioSubServicio

            }).Single();

        }

        public ProcesoResponse RegistrarServicio(ServicioDtoRequest servicio)
        {
            DateTime fecha = DateTime.Now;
            var objServicio = new Servicio()
            {

                IdLineaNegocio = servicio.IdLineaNegocio,
                Descripcion = servicio.Descripcion,
                IdMedio = servicio.IdMedio,
                IdGrupo = servicio.IdGrupo,
                IdTipoEnlace = servicio.IdTipoEnlace,
                IdServicioCMI = servicio.IdServicioCMI,
                IdEstado = Generales.Estados.Activo,
                IdServicioGrupo = servicio.IdServicioGrupo,
                IdUsuarioCreacion = servicio.IdUsuarioCreacion,
                FechaCreacion = fecha
            };

            iServicioDal.Add(objServicio);
            iServicioDal.UnitOfWork.Commit();

            respuesta.Id = objServicio.IdServicio;
            respuesta.Mensaje = MensajesGeneralComun.RegistrarServicio;
            respuesta.TipoRespuesta = Proceso.Valido;

            return respuesta;
        }
    }
}
