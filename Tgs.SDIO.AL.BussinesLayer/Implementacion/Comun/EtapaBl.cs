﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class EtapaBl : IEtapaBl
    {
        readonly IEtapaDal ietapaOportunidadDal;

        public EtapaBl(IEtapaDal etapaOportunidadDal)
        {
            ietapaOportunidadDal = etapaOportunidadDal;
        }

        public List<ListaDtoResponse> ListarComboEtapaOportunidad()
        {
            return ietapaOportunidadDal.ListarComboEtapaOportunidad();
        }

        public List<ListaDtoResponse> ListarComboEtapaOportunidadPorIdFase(FaseDtoRequest request)
        {
            return ietapaOportunidadDal.ListarComboEtapaOportunidadPorIdFase(request);
        }
    }
}
