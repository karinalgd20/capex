﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Transactions;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class EquipoTrabajoBl : IEquipoTrabajoBl
    {
        readonly IEquipoTrabajoDal iequipoTrabajoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public EquipoTrabajoBl(IEquipoTrabajoDal equipoTrabajoDal)
        {
            iequipoTrabajoDal = equipoTrabajoDal;
        }

        public List<ListaDtoResponse> ListarComboEquipoTrabajoAreas()
        {
            return iequipoTrabajoDal.ListarComboEquipoTrabajoAreas();
        }
    }
}
