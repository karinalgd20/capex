﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class LineaNegocioBl : ILineaNegocioBl
    {
        readonly ILineaNegocioDal iLineaNegocioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public LineaNegocioBl(ILineaNegocioDal ILineaNegocioDal)
        {
            iLineaNegocioDal = ILineaNegocioDal;
        }

        public ProcesoResponse ActualizarLineaNegocio(LineaNegocioDtoRequest lineaNegocio)
        {

            var objLineaNegocio = new LineaNegocio()
            {

                IdLineaNegocio = lineaNegocio.IdLineaNegocio.Value,
                Descripcion = lineaNegocio.Descripcion,
                IdEstado = lineaNegocio.IdEstado,
                IdUsuarioEdicion = lineaNegocio.IdUsuarioEdicion,
                FechaEdicion = lineaNegocio.FechaEdicion
            };
            iLineaNegocioDal.Modify(objLineaNegocio);
            iLineaNegocioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (lineaNegocio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarLineaNegocio : MensajesGeneralComun.ActualizarLineaNegocio;

            ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorLineaNegocio, null, Generales.Sistemas.Comun);
            return respuesta;
        }


        public List<ListaDtoResponse> ListarLineaNegocio(LineaNegocioDtoRequest lineaNegocio)
        {
            var listaLineaNegocio = iLineaNegocioDal.GetFilteredAsNoTracking(x => x.IdEstado == lineaNegocio.IdEstado).ToList();

            return listaLineaNegocio.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdLineaNegocio.ToString(),
                Descripcion = x.Descripcion
            }).ToList();
        }

        public LineaNegocioDtoResponse ObtenerLineaNegocio(LineaNegocioDtoRequest lineaNegocio)
        {
            var objLineaNegocio = iLineaNegocioDal.GetFiltered(x => x.IdLineaNegocio == lineaNegocio.IdLineaNegocio &&
                                                       x.IdEstado == lineaNegocio.IdEstado);
            return objLineaNegocio.Select(x => new LineaNegocioDtoResponse
            {
                IdLineaNegocio = x.IdLineaNegocio,
                Descripcion = x.Descripcion,
                IdEstado = x.IdEstado
            }).Single();

        }

        public ProcesoResponse RegistrarLineaNegocio(LineaNegocioDtoRequest lineaNegocio)
        {

            var objLineaNegocio = new LineaNegocio()
            {
                IdLineaNegocio = lineaNegocio.IdLineaNegocio.Value,
                Descripcion = lineaNegocio.Descripcion,
                IdEstado = lineaNegocio.IdEstado,
                IdUsuarioCreacion = lineaNegocio.IdUsuarioCreacion,
                FechaCreacion = lineaNegocio.FechaCreacion
            };

            iLineaNegocioDal.Add(objLineaNegocio);
            iLineaNegocioDal.UnitOfWork.Commit();

            respuesta.Id = objLineaNegocio.IdLineaNegocio;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.RegistrarLineaNegocio;

            ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorLineaNegocio, null, Generales.Sistemas.Comun);

            return respuesta;
        }
    }
}
