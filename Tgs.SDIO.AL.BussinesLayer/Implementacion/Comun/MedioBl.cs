﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class MedioBl : IMedioBl
    {
        readonly IMedioDal iMedioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public MedioBl(IMedioDal IMedioDal)
        {
            iMedioDal = IMedioDal;

        }

        public ProcesoResponse ActualizarMedio(MedioDtoRequest Medio)
        {

            var objMedio = new Medio()
            {
                IdMedio = Medio.IdMedio,
                Descripcion = Medio.Descripcion,
                IdEstado = Medio.IdEstado,
                IdUsuarioEdicion = Medio.IdUsuarioEdicion,
                FechaEdicion = Medio.FechaEdicion
            };

            iMedioDal.Modify(objMedio);
            iMedioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (Medio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarMedio : MensajesGeneralComun.ActualizarMedio;





            return respuesta;
        }


        public List<ListaDtoResponse> ListarMedio(MedioDtoRequest Medio)
        {
            var query = iMedioDal.GetFilteredAsNoTracking(x => Medio.IdEstado == Medio.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdMedio.ToString(),
                Descripcion=x.Descripcion
             
            }).ToList();
        }

        public MedioDtoResponse ObtenerMedio(MedioDtoRequest Medio)
        {
            var objMedio = iMedioDal.GetFiltered(x => x.IdMedio == Medio.IdMedio &&
                                                       x.IdEstado == Medio.IdEstado);
            return objMedio.Select(x => new MedioDtoResponse
            {
                IdMedio = x.IdMedio
           
            }).Single();

        }

        public ProcesoResponse RegistrarMedio(MedioDtoRequest Medio)
        {

            var objMedio = new Medio()
            {
                IdMedio = Medio.IdMedio,
                Descripcion = Medio.Descripcion,
                IdEstado = Medio.IdEstado,
                IdUsuarioCreacion = Medio.IdUsuarioCreacion,
                FechaCreacion = Convert.ToDateTime(Medio.FechaCreacion)
            };

            iMedioDal.Add(objMedio);
            iMedioDal.UnitOfWork.Commit();

            respuesta.Id = objMedio.IdMedio;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.RegistrarMedio;


            ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorMedio, null, Generales.Sistemas.Comun);


            return respuesta;
        }
    }
}
