﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class TipoSolucionBl : ITipoSolucionBl
    {
        readonly ITipoSolucionDal iTipoSolucionDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public TipoSolucionBl(ITipoSolucionDal ITipoSolucionDal)
        {
            iTipoSolucionDal = ITipoSolucionDal;
        }

        public List<TipoSolucionDtoResponse> ListarTipoSolucionByProyecto(DetalleSolucionDtoRequest request)
        {
            return iTipoSolucionDal.ListarTipoSolucionByProyecto(request);
        }
    }
}
