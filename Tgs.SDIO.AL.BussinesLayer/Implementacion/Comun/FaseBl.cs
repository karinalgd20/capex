﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class FaseBl : IFaseBl
    {
        readonly IFaseDal ifaseDal;

        public FaseBl(IFaseDal faseDal)
        {
            ifaseDal = faseDal;
        }

        public List<ListaDtoResponse> ListarComboFase()
        {
            return ifaseDal.ListarComboFase();
        }
    }
}
