﻿using System;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Util.Mensajes.Comun;
using Tgs.SDIO.Util.Funciones;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class RiesgoProyectoBl : IRiesgoProyectoBl
    {
        readonly IRiesgoProyectoDal iriesgoproyectoDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public RiesgoProyectoBl(IRiesgoProyectoDal riesgoproyectoDal)
        {
            iriesgoproyectoDal = riesgoproyectoDal;
        }

        #region - Transaccionales -
        public ProcesoResponse RegistrarRiesgoProyecto(RiesgoProyectoDtoRequest riesgoProyecto)
        {
            var entidad = new RiesgoProyecto
            {
                IdOportunidad = riesgoProyecto.IdOportunidad,
                IdTipo = riesgoProyecto.IdTipo,
                FechaDeteccion = riesgoProyecto.FechaDeteccion,
                Descripcion = riesgoProyecto.Descripcion,
                Consecuencias = riesgoProyecto.Consecuencias,
                PlanAccion = riesgoProyecto.PlanAccion,
                Responsable = riesgoProyecto.Responsable,
                IdProbabilidad = riesgoProyecto.IdProbabilidad,
                IdImpacto = riesgoProyecto.IdImpacto,
                IdNivel = riesgoProyecto.IdNivel,
                FechaCierre = riesgoProyecto.FechaCierre,
                IdEstado = riesgoProyecto.IdEstado,
                IdUsuarioCreacion = riesgoProyecto.IdUsuarioCreacion,
                FechaCreacion = DateTime.Now
            };

            iriesgoproyectoDal.Add(entidad);
            iriesgoproyectoDal.UnitOfWork.Commit();
            respuesta.Id = entidad.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.RegistrarRiesgoProyecto;
            return respuesta;
        }
        public ProcesoResponse ActualizarRiesgoProyecto(RiesgoProyectoDtoRequest riesgoProyecto)
        {
            var entidad = iriesgoproyectoDal.Get(riesgoProyecto.Id);
            entidad.IdTipo = riesgoProyecto.IdTipo;
            entidad.FechaDeteccion = riesgoProyecto.FechaDeteccion;
            entidad.Descripcion = riesgoProyecto.Descripcion;
            entidad.Consecuencias = riesgoProyecto.Consecuencias;
            entidad.PlanAccion = riesgoProyecto.PlanAccion;
            entidad.Responsable = riesgoProyecto.Responsable;
            entidad.IdProbabilidad = riesgoProyecto.IdProbabilidad;
            entidad.IdImpacto = riesgoProyecto.IdImpacto;
            entidad.IdNivel = riesgoProyecto.IdNivel;
            entidad.FechaCierre = riesgoProyecto.FechaCierre;
            entidad.IdEstado = riesgoProyecto.IdEstado;
            entidad.IdUsuarioEdicion = riesgoProyecto.IdUsuarioEdicion;
            entidad.FechaEdicion = riesgoProyecto.FechaEdicion;

            iriesgoproyectoDal.Modify(entidad);
            iriesgoproyectoDal.UnitOfWork.Commit();
            respuesta.Id = entidad.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }
        public ProcesoResponse EliminarRiesgoProyecto(RiesgoProyectoDtoRequest riesgoProyecto)
        {
            var entidad = iriesgoproyectoDal.Get(riesgoProyecto.Id);
            entidad.IdEstado = 303;
            entidad.IdUsuarioEdicion = riesgoProyecto.IdUsuarioEdicion;
            entidad.FechaEdicion = riesgoProyecto.FechaEdicion;
            iriesgoproyectoDal.Modify(entidad);
            iriesgoproyectoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;

            return respuesta;
        }
        #endregion

        #region - No Transaccionales -
        public RiesgoProyectoDtoResponse ObtenerRiesgoProyectoPorId(RiesgoProyectoDtoRequest riesgoProyecto)
        {
            var entidad = iriesgoproyectoDal.Get(riesgoProyecto.Id);
            return new RiesgoProyectoDtoResponse
            {
                Id = entidad.Id,
                IdOportunidad = entidad.IdOportunidad,
                IdTipo = entidad.IdTipo,
                FechaDeteccion = Funciones.FormatoFecha(entidad.FechaDeteccion),
                Descripcion = entidad.Descripcion,
                Consecuencias = entidad.Consecuencias,
                PlanAccion = entidad.PlanAccion,
                Responsable = entidad.Responsable,
                IdProbabilidad = entidad.IdProbabilidad,
                IdImpacto = entidad.IdImpacto,
                IdNivel = entidad.IdNivel,
                FechaCierre = entidad.FechaCierre.HasValue ? Funciones.FormatoFecha(entidad.FechaCierre.Value) : "",
                IdEstado = entidad.IdEstado
            };
        }
        public RiesgoProyectoPaginadoDtoResponse ListadoRiesgoProyectosPaginado(RiesgoProyectoDtoRequest riesgoProyecto)
        {
            return iriesgoproyectoDal.ListadoRiesgoProyectosPaginado(riesgoProyecto);
        }
        #endregion
    }
}