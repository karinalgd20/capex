﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class ServicioSubServicioBl : IServicioSubServicioBl
    {
        readonly IServicioSubServicioDal iServicioSubServicioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ServicioSubServicioBl(IServicioSubServicioDal IServicioSubServicioDal)
        {
            iServicioSubServicioDal = IServicioSubServicioDal;

        }
      
         public ProcesoResponse EliminarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {

            var objSubServicio = new ServicioSubServicio()
            {

                IdServicioSubServicio = servicioSubServicio.IdServicioSubServicio,
     
            };

            //iServicioSubServicioDal.ActualizarPorCampos(objSubServicio, x => x.IdServicioSubServicio);
            iServicioSubServicioDal.Remove(objSubServicio);
            iServicioSubServicioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.EliminarServicioSubServicio;




            return respuesta;
        }
        public ProcesoResponse ActualizarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {

            var objSubServicio = new ServicioSubServicio()
            {
                IdServicioSubServicio=servicioSubServicio.IdServicioSubServicio,
                IdSubServicio = servicioSubServicio.IdSubServicio,
                IdServicio = servicioSubServicio.IdServicio,
                IdTipoCosto = servicioSubServicio.IdTipoCosto,
               // IdProveedor = servicioSubServicio.IdProveedor,
              //  ContratoMarco = servicioSubServicio.ContratoMarco,
              //  IdPeriodos = servicioSubServicio.IdPeriodos,
               // Inicio =Numeric.Uno,
             //   Ponderacion = 100,
                IdMoneda = servicioSubServicio.IdMoneda,
                IdGrupo = servicioSubServicio.IdGrupo,
                // FlagSISEGO = servicioSubServicio.FlagSISEGO, x =>x.FlagSISEGO


                IdUsuarioEdicion = servicioSubServicio.IdUsuarioEdicion,
                FechaEdicion =  Convert.ToDateTime(servicioSubServicio.FechaEdicion)
            };
           
              iServicioSubServicioDal.ActualizarPorCampos(objSubServicio, x => x.IdServicioSubServicio, x => x.IdServicio, x => x.IdTipoCosto,
               //   x => x.IdProveedor, x => x.ContratoMarco, x => x.IdPeriodos, x => x.Inicio, x => x.Ponderacion, 
                x =>x.IdMoneda, x =>x.IdGrupo, x=>x.IdSubServicio,x=>x.IdUsuarioEdicion,x=> x.FechaEdicion);
            iServicioSubServicioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.ActualizarSubServicio;


         

            return respuesta;
        }
        public ProcesoResponse InhabilitarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {
         
            var objSubServicio = new ServicioSubServicio()
            {
                IdServicioSubServicio = servicioSubServicio.IdServicioSubServicio,
                IdEstado = servicioSubServicio.IdEstado,
                IdUsuarioEdicion = servicioSubServicio.IdUsuarioEdicion,
                FechaEdicion = Convert.ToDateTime(servicioSubServicio.FechaEdicion)
            };

            iServicioSubServicioDal.ActualizarPorCampos(objSubServicio,x=> x.IdServicioSubServicio, x => x.IdSubServicio, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
            iServicioSubServicioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (servicioSubServicio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarSubServicio : MensajesGeneralComun.ActualizarSubServicio;




            return respuesta;
        }

  
        public ServicioSubServicioPaginadoDtoResponse ListaServicioSubServicioPaginado(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            return iServicioSubServicioDal.ListaServicioSubServicioPaginado(servicioSubServicio);
            
        }
        public List<ServicioSubServicioDtoResponse> ListarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            var query = iServicioSubServicioDal.GetFilteredAsNoTracking(x => x.IdEstado == servicioSubServicio.IdEstado
            && servicioSubServicio.IdEstado == servicioSubServicio.IdEstado).ToList();
            return query.Select(x => new ServicioSubServicioDtoResponse
            {
               
                IdEstado = x.IdEstado
            }).ToList();
        }

        public ServicioSubServicioDtoResponse ObtenerServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            var objServicioSubServicio = iServicioSubServicioDal.GetFiltered(x =>
                                                       x.IdServicioSubServicio == servicioSubServicio.IdServicioSubServicio && x.IdEstado==servicioSubServicio.IdEstado);
            return objServicioSubServicio.Select(x => new ServicioSubServicioDtoResponse
            {

                IdSubServicio = x.IdSubServicio,
                IdServicio = x.IdServicio,
                IdTipoCosto = x.IdTipoCosto,
                IdProveedor = x.IdProveedor,
                ContratoMarco = x.ContratoMarco,
                IdPeriodos = x.IdPeriodos,
                Inicio = x.Inicio,
                Ponderacion = x.Ponderacion,
                IdMoneda = x.IdMoneda,
                IdGrupo = x.IdGrupo,
                FlagSISEGO = x.FlagSISEGO
            }).Single();

        }

        public ProcesoResponse RegistrarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            DateTime fecha = DateTime.Now;
            //DateTime Mes = DateTime.Now.Month();
           

            var objSubServicio = new ServicioSubServicio()
            {
               IdSubServicio = servicioSubServicio.IdSubServicio,
               IdServicio =servicioSubServicio.IdServicio,
               IdTipoCosto=servicioSubServicio.IdTipoCosto,
              // IdProveedor=servicioSubServicio.IdProveedor,
              // ContratoMarco=servicioSubServicio.ContratoMarco,

             //  IdPeriodos=Numeric.Dos,
              // Inicio=Numeric.Uno,
             //  Ponderacion=100,

               IdMoneda=servicioSubServicio.IdMoneda,
               IdGrupo= servicioSubServicio.IdGrupo,
              // FlagSISEGO=servicioSubServicio.FlagSISEGO,


                 IdEstado = Generales.Estados.Activo,
                IdUsuarioCreacion = servicioSubServicio.IdUsuarioCreacion,
                FechaCreacion = fecha         
            };

            iServicioSubServicioDal.Add(objSubServicio);
            iServicioSubServicioDal.UnitOfWork.Commit();

            respuesta.Id = objSubServicio.IdServicioSubServicio;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.RegistrarSubServicio;


       

            return respuesta;
        }

        public ServicioSubServicioPaginadoDtoResponse ListarServicioSubServicioGrupos(ServicioSubServicioDtoRequest ServiciosubServicio)
        {
            return iServicioSubServicioDal.ListarServicioSubServicioGrupos(ServiciosubServicio);
        }
    }
}
