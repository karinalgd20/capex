﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{

    public class SalesForceConsolidadoCabeceraBl : ISalesForceConsolidadoCabeceraBl
    {
        readonly ISalesForceConsolidadoCabeceraDal _salesForceConsolidadoCabeceraDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public SalesForceConsolidadoCabeceraBl(ISalesForceConsolidadoCabeceraDal salesForceConsolidadoCabeceraDal)
        {
            _salesForceConsolidadoCabeceraDal = salesForceConsolidadoCabeceraDal;
        }
        public List<SalesForceConsolidadoCabecera> SalesForceConsolidadoCabecera(SalesForceConsolidadoCabecera request)
        {
            if (request.IdOportunidad == (Generales.Numeric.Cero).ToString())
            {
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorValidacionComun.EV00001, null, Generales.Sistemas.Comun);
            }

            if (request.IdOportunidad == (Generales.Numeric.Uno).ToString())
            {
                ExceptionManager.GenerarAppExcepcionReglaNegocio(ErrorNegocioComun.ERN00001, Generales.Sistemas.Comun);
            }

            return _salesForceConsolidadoCabeceraDal.SalesForceConsolidadoCabecera(request);
        }

        public List<SalesForceConsolidadoCabeceraResponse> ListarProbabilidades()
        {
            return _salesForceConsolidadoCabeceraDal.ListarProbabilidad();
        }

        public ProcesoResponse ActualizarSalesForceConsolidadoCabecera(SalesForceConsolidadoCabeceraDtoRequest salesForceConsolidadoCabecera)
        {

            DateTime fecha = DateTime.Now;

            var objSalesForceConsolidadoCabecera = new SalesForceConsolidadoCabecera()
            {
                NumeroDelCaso = salesForceConsolidadoCabecera.NumeroDelCaso,
                IdEstado = salesForceConsolidadoCabecera.IdEstado,
                IdUsuarioEdicion = salesForceConsolidadoCabecera.IdUsuarioEdicion,
                FechaEdicion = fecha,
           
            };


            _salesForceConsolidadoCabeceraDal.ActualizarPorCampos(objSalesForceConsolidadoCabecera);
            _salesForceConsolidadoCabeceraDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            //respuesta.Mensaje = MensajesGeneralComun.ActualizarSalesForceConsolidadoCabecera;



            return respuesta;
        }

        public ProcesoResponse InhabilitarSalesForceConsolidadoCabecera(SalesForceConsolidadoCabeceraDtoRequest salesForceConsolidadoCabecera)
        {

            DateTime fecha = DateTime.Now;

            var objSalesForceConsolidadoCabecera = new SalesForceConsolidadoCabecera()
            {
                Id = salesForceConsolidadoCabecera.Id,
                IdEstado = salesForceConsolidadoCabecera.IdEstado,
                IdUsuarioEdicion = salesForceConsolidadoCabecera.IdUsuarioEdicion,
                FechaEdicion = fecha
            };


            _salesForceConsolidadoCabeceraDal.ActualizarPorCampos(objSalesForceConsolidadoCabecera, x => x.Id, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
            _salesForceConsolidadoCabeceraDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
        //    respuesta.Mensaje = MensajesGeneralComun.EliminarSalesForceConsolidadoCabecera;



            return respuesta;
        }

        public List<ListaDtoResponse> ListarSalesForceConsolidadoCabeceras(SalesForceConsolidadoCabeceraDtoRequest salesForceConsolidadoCabecera)
        {
            var query = _salesForceConsolidadoCabeceraDal.GetFilteredAsNoTracking(x => x.IdEstado == salesForceConsolidadoCabecera.IdEstado
            && salesForceConsolidadoCabecera.Id == salesForceConsolidadoCabecera.Id).ToList();
            return query.Select(x => new ListaDtoResponse
            {
               
                Descripcion = x.Descripcion
            }).OrderBy(x => x.Descripcion).ToList();
        }

        public SalesForceConsolidadoCabeceraDtoResponse ObtenerSalesForceConsolidadoCabecera(SalesForceConsolidadoCabeceraDtoRequest salesForceConsolidadoCabecera)
        {
            var objSalesForceConsolidadoCabecera = _salesForceConsolidadoCabeceraDal.GetFiltered(x => x.Id == salesForceConsolidadoCabecera.Id);
            return objSalesForceConsolidadoCabecera.Select(x => new SalesForceConsolidadoCabeceraDtoResponse
            {
    
                IdEstado = x.IdEstado
          
            }).Single();

        }

        public List<SalesForceConsolidadoCabeceraDtoResponse> ListarNumeroSalesForcePorIdOportunidad(SalesForceConsolidadoCabeceraDtoRequest salesForceConsolidadoCabecera)
        {
            var ListSalesForceConsolidadoCabecera = _salesForceConsolidadoCabeceraDal.ListarNumeroSalesForcePorIdOportunidad(salesForceConsolidadoCabecera);

            return ListSalesForceConsolidadoCabecera;

        }
        

        public ProcesoResponse RegistrarSalesForceConsolidadoCabecera(SalesForceConsolidadoCabeceraDtoRequest SalesForceConsolidadoCabecera)
        {
            DateTime fecha = DateTime.Now;
            SalesForceConsolidadoCabecera.IdEstado = Generales.Estados.Activo;
            var objSalesForceConsolidadoCabecera = new SalesForceConsolidadoCabecera()
            {

  
                IdUsuarioCreacion = SalesForceConsolidadoCabecera.IdUsuarioCreacion,
                FechaCreacion = fecha
            };

            _salesForceConsolidadoCabeceraDal.Add(objSalesForceConsolidadoCabecera);
            _salesForceConsolidadoCabeceraDal.UnitOfWork.Commit();


            respuesta.TipoRespuesta = Proceso.Valido;
            //respuesta.Mensaje = MensajesGeneralComun.RegistrarSalesForceConsolidadoCabecera;
            respuesta.Id = objSalesForceConsolidadoCabecera.Id;


            return respuesta;
        }

    }
}
