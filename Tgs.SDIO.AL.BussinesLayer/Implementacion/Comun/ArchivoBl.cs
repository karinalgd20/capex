﻿using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Configuracion;
using System;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ArchivoBl : IArchivoBl
    {
        readonly IArchivoDal iArchivoDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public ArchivoBl(IArchivoDal IArchivoDal)
        {
            iArchivoDal = IArchivoDal;
        }

        #region - Transaccionales -
        public ProcesoResponse RegistrarArchivo(ArchivoDtoRequest archivo, byte[] bytes)
        {
            try {
                if (ValidarCarga(bytes).TipoRespuesta.Equals(0))
                {
                    string path = System.IO.Path.Combine(RutaServidorArchivos, archivo.NombreInterno);
                    if (bytes != null)
                    {
                        System.IO.File.WriteAllBytes(path, bytes);
                    }

                    var item = new Archivo
                    {
                        CodigoTabla = archivo.CodigoTabla,
                        IdEntidad = archivo.IdEntidad,
                        IdCategoria = archivo.IdCategoria,
                        Nombre = archivo.Nombre,
                        NombreInterno = archivo.NombreInterno,
                        Descripcion = archivo.Descripcion,
                        Ruta = RutaServidorArchivos,
                        IdEstado = archivo.IdEstado,
                        IdUsuarioCreacion = archivo.IdUsuarioCreacion,
                        FechaCreacion = archivo.FechaCreacion
                    };

                    iArchivoDal.Add(item);
                    iArchivoDal.UnitOfWork.Commit();

                    respuesta.Id = item.Id;
                    respuesta.TipoRespuesta = Proceso.Valido;
                    respuesta.Mensaje = MensajesGeneralComun.RegistrarArchivo;
                    respuesta.Detalle = path;
                }
            }
            catch (Exception ex) {
                respuesta.Id = 0;
                respuesta.TipoRespuesta = Proceso.Invalido;
                respuesta.Mensaje = ex.Message.ToString();
                respuesta.Detalle = "Error";
            }

            return respuesta;
        }
        public ProcesoResponse InactivarArchivo(ArchivoDtoRequest archivo)
        {
            var entidad = iArchivoDal.Get(archivo.Id);
            entidad.IdEstado = 0;
            entidad.IdUsuarioEdicion = archivo.IdUsuarioEdicion;
            entidad.FechaEdicion = archivo.FechaEdicion;
            iArchivoDal.Modify(entidad);
            iArchivoDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.InactivarArchivo;
            return respuesta;
        }
        #endregion

        #region - No Transaccionales -
        private ProcesoResponse ValidarCarga(byte[] bytes)
        {
            if (bytes.Length == 0)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                respuesta.Mensaje = "Seleccione un archivo";
                return respuesta;
            }

            if (bytes.Length > 4194304)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                respuesta.Mensaje = "El archivo no puede ser mayor a 4 MB";
                return respuesta;
            }

            return respuesta;
        }
        public ArchivoPaginadoDtoResponse ListarArchivoPaginado(ArchivoDtoRequest request)
        {
            return iArchivoDal.ListarPaginado(request);
        }
        public ArchivoDtoResponse ObtenerArchivo(ArchivoDtoRequest request)
        {
            var entidad = iArchivoDal.Get(request.Id);

            ArchivoDtoResponse archivo = new ArchivoDtoResponse
            {
                Id = entidad.Id,
                CodigoTabla = entidad.CodigoTabla,
                IdEntidad = entidad.IdEntidad,
                IdCategoria = entidad.IdCategoria,
                Nombre = entidad.Nombre,
                NombreInterno = entidad.NombreInterno,
                Ruta = System.IO.Path.Combine(entidad.Ruta, entidad.NombreInterno)
            };

            return archivo;
        }
        #endregion
    }
}