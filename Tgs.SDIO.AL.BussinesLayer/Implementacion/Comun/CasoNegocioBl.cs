﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class CasoNegocioBl : ICasoNegocioBl
    {
        readonly ICasoNegocioDal iCasoNegocioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public CasoNegocioBl(ICasoNegocioDal ICasoNegocioDal)
        {
            iCasoNegocioDal = ICasoNegocioDal;
        }

        public ProcesoResponse ActualizarCasoNegocio(CasoNegocioDtoRequest casonegocio)
        {
            if (casonegocio.FlagDefecto == Numeric.Uno.ToString())
            {
                casonegocio.Indice = Numeric.Uno;
                casonegocio.Tamanio = Numeric.Tamanio;

                var ListCasonegocioResponse = iCasoNegocioDal.ListarCasoNegocio(casonegocio);
                if (ListCasonegocioResponse.ListCasoNegocioDtoResponse.Count > 0)
                {
                    var caso = ListCasonegocioResponse.ListCasoNegocioDtoResponse.Where(c => c.FlagDefecto == Numeric.Uno.ToString()).Single();
                    var casodefecto = new CasoNegocio()
                    {
                        FlagDefecto = Numeric.Cero.ToString(),
                        IdCasoNegocio = caso.IdCasoNegocio,
                    };
                    iCasoNegocioDal.ActualizarPorCampos(casodefecto, x => x.IdLineaNegocio, x => x.IdCasoNegocio);
                    iCasoNegocioDal.UnitOfWork.Commit();
                }
            }
            
            var casoNegocio = new CasoNegocio()
            {
                IdLineaNegocio = casonegocio.IdLineaNegocio,
                IdCasoNegocio = casonegocio.IdCasoNegocio,
                Descripcion = casonegocio.Descripcion,
                FlagDefecto = casonegocio.FlagDefecto,
                IdEstado = casonegocio.IdEstado,
                FechaEdicion = casonegocio.FechaEdicion,
                IdUsuarioEdicion = casonegocio.IdUsuarioEdicion
            };

            iCasoNegocioDal.ActualizarPorCampos(casoNegocio, x => x.IdLineaNegocio, x => x.IdCasoNegocio, x => x.Descripcion, x => x.FlagDefecto, x => x.IdEstado, x => x.FechaEdicion, x => x.IdUsuarioEdicion);
            iCasoNegocioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (casonegocio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarCasoNegocio : MensajesGeneralComun.ActualizarCasoNegocio;

            return respuesta;
        }

        public CasoNegocioPaginadoDtoResponse ListarCasoNegocio(CasoNegocioDtoRequest casonegocio)
        {
            return iCasoNegocioDal.ListarCasoNegocio(casonegocio);
        }

        public List<ListaDtoResponse> ListarCasoNegocioDefecto(CasoNegocioDtoRequest casonegocio)
        {
            var objCasoNegocio = iCasoNegocioDal.GetFiltered(x => x.IdLineaNegocio == casonegocio.IdLineaNegocio &&
                                                               x.IdEstado == casonegocio.IdEstado).OrderByDescending(z=>z.FlagDefecto);

            return objCasoNegocio.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdCasoNegocio.ToString(),
                Descripcion = x.Descripcion
            }).ToList();
        }

        public CasoNegocioDtoResponse ObtenerCasoNegocio(CasoNegocioDtoRequest casonegocio)
        {
            return iCasoNegocioDal.ObtenerCasoNegocio(casonegocio);
        }

        public ProcesoResponse RegistrarCasoNegocio(CasoNegocioDtoRequest casonegocio)
        {
            var casoNegocio = new CasoNegocio()
            {
                IdLineaNegocio = casonegocio.IdLineaNegocio,
                Descripcion = casonegocio.Descripcion,
                FlagDefecto = casonegocio.FlagDefecto,
                IdEstado = casonegocio.IdEstado,
                IdUsuarioCreacion = casonegocio.IdUsuarioCreacion,
                FechaCreacion = casonegocio.FechaCreacion
            };

            iCasoNegocioDal.Add(casoNegocio);
            iCasoNegocioDal.UnitOfWork.Commit();

            respuesta.Id = casoNegocio.IdCasoNegocio;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.RegistrarCasoNegocio;

            return respuesta;
        }
    }
}
