﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Transactions;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using System.Collections.Generic;
namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class RolRecursoBl : IRolRecursoBl
    {
        readonly IRolRecursoDal irolRecursoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public RolRecursoBl(IRolRecursoDal rolRecursoDal)
        {
            irolRecursoDal = rolRecursoDal;
        }

        public RolRecursoPaginadoDtoResponse ListadoRolRecursosPaginado(RolRecursoDtoRequest request)
        {
            return irolRecursoDal.ListadoRolRecursosPaginado(request);
        }

        public List<ListaDtoResponse> ListarComboRolRecurso()
        {
            return irolRecursoDal.ListarComboRolRecurso();
        }

        public RolRecursoDtoResponse ObtenerRolRecursoPorId(RolRecursoDtoRequest request)
        {
            var query = irolRecursoDal.GetFilteredAsNoTracking(x => x.IdRol == request.IdRol && (x.IdEstado == Activo || x.IdEstado == Inactivo)).ToList();
            return query.Select(x => new RolRecursoDtoResponse
            {
                IdRol = x.IdRol,
                Descripcion = x.Descripcion,
                Nivel = x.Nivel,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion
            }).FirstOrDefault();
        }

        public ProcesoResponse ActualizarRolRecurso(RolRecursoDtoRequest request)
        {
                        var areaAntigua = ObtenerRolRecursoPorId(request);
                        var areaSeguimiento = new RolRecurso()
                        {
                            IdRol = request.IdRol,
                            Descripcion = request.Descripcion,
                            Nivel = request.Nivel,
                            IdEstado = request.IdEstado,
                            IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                            FechaCreacion = areaAntigua.FechaCreacion,
                            IdUsuarioEdicion = request.IdUsuario,
                            FechaEdicion = DateTime.Now,
                        };
                            irolRecursoDal.Modify(areaSeguimiento);
                            irolRecursoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }

        public ProcesoResponse RegistrarRolRecurso(RolRecursoDtoRequest request)
        {
                        var areaSeguimiento = new RolRecurso()
                        {
                            Descripcion = request.Descripcion,
                            Nivel = request.Nivel,
                            IdEstado = request.IdEstado,
                            IdUsuarioCreacion = request.IdUsuario,
                            FechaCreacion = DateTime.Now
                        };
                            irolRecursoDal.Add(areaSeguimiento);
                            irolRecursoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;
        }

        public ProcesoResponse EliminarRolRecurso(RolRecursoDtoRequest request)
        {
                        var areaAntigua = ObtenerRolRecursoPorId(request);
                        var areaSeguimiento = new RolRecurso()
                        {
                            IdRol = request.IdRol,
                            Descripcion = areaAntigua.Descripcion,
                            Nivel = areaAntigua.Nivel,
                            IdEstado = Eliminado,
                            IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                            FechaCreacion = areaAntigua.FechaCreacion,
                            IdUsuarioEdicion = request.IdUsuario,
                            FechaEdicion = DateTime.Now
                        };
                            irolRecursoDal.Modify(areaSeguimiento);
                            irolRecursoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;

            return respuesta;
        }

    }
}
