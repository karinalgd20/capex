﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Funciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class CasoNegocioServicioBl : ICasoNegocioServicioBl
    {
        readonly ICasoNegocioServicioDal iCasoNegocioServicioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public CasoNegocioServicioBl(ICasoNegocioServicioDal ICasoNegocioServicioDal)
        {
            iCasoNegocioServicioDal = ICasoNegocioServicioDal;
        }

        public ProcesoResponse ActualizarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio)
        {
            var casoNegocioServicio = new CasoNegocioServicio()
            {
                IdCasoNegocioServicio = casoservicio.IdCasoNegocioServicio,
                IdCasoNegocio = casoservicio.IdCasoNegocio,
                IdServicio = casoservicio.IdServicio,
                IdEstado = casoservicio.IdEstado,
                IdUsuarioEdicion = casoservicio.IdUsuarioEdicion,
                FechaEdicion = casoservicio.FechaEdicion
            };
            iCasoNegocioServicioDal.ActualizarPorCampos(casoNegocioServicio, x=> x.IdCasoNegocioServicio, x => x.IdCasoNegocio, x => x.IdServicio, x => x.IdEstado, x => x.FechaEdicion, x => x.IdUsuarioEdicion);
            iCasoNegocioServicioDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (casoservicio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarCasoNegocioServicio : MensajesGeneralComun.ActualizarCasoNegocioServicio;
            
            return respuesta;
        }

        public List<CasoNegocioServicioDtoResponse> ListarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio)
        {
            return iCasoNegocioServicioDal.ListarCasoNegocioServicio(casoservicio);
        }

        public CasoNegocioServicioDtoResponse ObtenerCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio)
        {
            return iCasoNegocioServicioDal.ObtenerCasoNegocioServicio(casoservicio);
        }

        public ProcesoResponse RegistrarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio)
        {
            var casoNegocioServicio = new CasoNegocioServicio()
            {
                IdCasoNegocio = casoservicio.IdCasoNegocio,
                IdServicio = casoservicio.IdServicio,
                IdEstado = casoservicio.IdEstado,
                IdUsuarioCreacion = casoservicio.IdUsuarioCreacion,
                FechaCreacion = casoservicio.FechaCreacion
            };

            iCasoNegocioServicioDal.Add(casoNegocioServicio);
            iCasoNegocioServicioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.RegistrarCasoNegocioServicio;
            return respuesta;
        }


        public CasoNegocioServicioPaginadoDtoResponse ListPaginadoCasoNegocioServicio(CasoNegocioServicioDtoRequest casoNegocio)
        {
            return iCasoNegocioServicioDal.ListPaginadoCasoNegocioServicio(casoNegocio);
        }
    }
}
