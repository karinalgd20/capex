﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;

using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class ActividadBl : IActividadBl
    {
        readonly IActividadDal iactividadDal;
        readonly IActividadSegmentoNegocioDal iactividadSegmentoNegocioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ActividadBl(IActividadDal actividadDal, IActividadSegmentoNegocioDal actividadSegmentoNegocioDal)
        {
            iactividadDal = actividadDal;
            iactividadSegmentoNegocioDal = actividadSegmentoNegocioDal;
        }

        public ActividadPaginadoDtoResponse ListatActividadPaginado(ActividadDtoRequest request)
        {
            return iactividadDal.ListatActividadPaginado(request);
        }

        public ActividadDtoResponse ObtenerActividadPorId(ActividadDtoRequest request)
        {
            return iactividadDal.ObtenerActividadPorId(request);
        }

        public List<ListaDtoResponse> ListarComboActividadPadres()
        {
            return iactividadDal.ListarComboActividadPadres();
        }

        public ProcesoResponse ActualizarActividad(ActividadDtoRequest request)
        {

            var actividadAntigua = ObtenerActividadPorId(request);
            var actividadSeguimiento = new Actividad()
            {
                IdActividad = request.IdActividad,
                IdActividadPadre = request.IdActividadPadre,
                Descripcion = request.Descripcion,
                Predecesoras = request.Predecesoras,
                IdFase = request.IdFase,
                IdEtapa = request.IdEtapa,
                FlgCapexMayor = request.FlgCapexMayor,
                FlgCapexMenor = request.FlgCapexMenor,
                IdAreaSeguimiento = request.IdAreaSeguimiento,
                NumeroDiaCapexMenor = request.NumeroDiaCapexMenor,
                CantidadDiasCapexMenor = request.CantidadDiasCapexMenor,
                NumeroDiaCapexMayor = request.NumeroDiaCapexMayor,
                CantidadDiasCapexMayor = request.CantidadDiasCapexMayor,
                IdTipoActividad = request.IdTipoActividad,
                AsegurarOferta = request.AsegurarOferta,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = actividadAntigua.IdUsuarioCreacion,
                FechaCreacion = actividadAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now,
            };
            iactividadDal.Modify(actividadSeguimiento);
            iactividadDal.UnitOfWork.Commit();
            var segmentos = iactividadSegmentoNegocioDal.GetFilteredAsNoTracking(x => x.IdActividad == request.IdActividad && x.IdEstado == Activo).ToList();
            iactividadSegmentoNegocioDal.Remove(segmentos);
            foreach (ComboDtoResponse segmento in request.ListSegmentoNegocio)
            {
                var actividadSegmentoNegocio = new ActividadSegmentoNegocio()
                {
                    IdActividad = actividadSeguimiento.IdActividad,
                    IdSegmentoNegocio = Convert.ToInt32(segmento.id),
                    IdEstado = Activo,
                    IdUsuarioCreacion = request.IdUsuario,
                    FechaCreacion = DateTime.Now
                };
                iactividadSegmentoNegocioDal.Add(actividadSegmentoNegocio);
            }
            iactividadSegmentoNegocioDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.ActivoRegistro;

            return respuesta;
        }

        public ProcesoResponse RegistrarActividad(ActividadDtoRequest request)
        {
            var actividadSeguimiento = new Actividad()
            {
                IdActividadPadre = request.IdActividadPadre,
                Descripcion = request.Descripcion,
                Predecesoras = request.Predecesoras,
                IdFase = request.IdFase,
                IdEtapa = request.IdEtapa,
                FlgCapexMayor = request.FlgCapexMayor,
                FlgCapexMenor = request.FlgCapexMenor,
                IdAreaSeguimiento = request.IdAreaSeguimiento,
                NumeroDiaCapexMenor = request.NumeroDiaCapexMenor,
                CantidadDiasCapexMenor = request.CantidadDiasCapexMenor,
                NumeroDiaCapexMayor = request.NumeroDiaCapexMayor,
                CantidadDiasCapexMayor = request.CantidadDiasCapexMayor,
                IdTipoActividad = request.IdTipoActividad,
                AsegurarOferta = request.AsegurarOferta,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = request.IdUsuario,
                FechaCreacion = DateTime.Now
            };
            iactividadDal.Add(actividadSeguimiento);
            iactividadDal.UnitOfWork.Commit();
            foreach (ComboDtoResponse segmento in request.ListSegmentoNegocio)
            {
                var actividadSegmentoNegocio = new ActividadSegmentoNegocio()
                {
                    IdActividad = actividadSeguimiento.IdActividad,
                    IdSegmentoNegocio = Convert.ToInt32(segmento.id),
                    IdEstado = Activo,
                    IdUsuarioCreacion = request.IdUsuario,
                    FechaCreacion = DateTime.Now
                };
                iactividadSegmentoNegocioDal.Add(actividadSegmentoNegocio);

            }
            iactividadSegmentoNegocioDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.RegistraOk;

            return respuesta;
        }

        public ProcesoResponse EliminarActividad(ActividadDtoRequest request)
        {
            var areaAntigua = ObtenerActividadPorId(request);
            var actividad = new Actividad()
            {
                IdActividad = request.IdActividad,
                IdActividadPadre = areaAntigua.IdActividadPadre,
                Descripcion = areaAntigua.Descripcion,
                Predecesoras = areaAntigua.Predecesoras,
                IdFase = areaAntigua.IdFase,
                IdEtapa = areaAntigua.IdEtapa,
                FlgCapexMayor = areaAntigua.FlgCapexMayor,
                FlgCapexMenor = areaAntigua.FlgCapexMenor,
                IdAreaSeguimiento = areaAntigua.IdAreaSeguimiento,
                NumeroDiaCapexMenor = areaAntigua.NumeroDiaCapexMenor,
                CantidadDiasCapexMenor = areaAntigua.CantidadDiasCapexMenor,
                NumeroDiaCapexMayor = areaAntigua.NumeroDiaCapexMayor,
                CantidadDiasCapexMayor = areaAntigua.CantidadDiasCapexMayor,
                IdTipoActividad = areaAntigua.IdTipoActividad,
                AsegurarOferta = areaAntigua.AsegurarOferta,
                IdEstado = Eliminado,
                IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                FechaCreacion = areaAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now

            };
            iactividadDal.Modify(actividad);
            iactividadDal.UnitOfWork.Commit();
            var segmentos = iactividadSegmentoNegocioDal.GetFilteredAsNoTracking(x => x.IdActividad == request.IdActividad && x.IdEstado == Activo).ToList();
            foreach (ActividadSegmentoNegocio segmento in segmentos)
            {
                var actividadSegmento = new ActividadSegmentoNegocio()
                {
                    IdActividadSegmentoNegocio = segmento.IdActividadSegmentoNegocio,
                    IdActividad = segmento.IdActividad,
                    IdSegmentoNegocio = segmento.IdSegmentoNegocio,
                    IdEstado = Eliminado,
                    IdUsuarioCreacion = segmento.IdUsuarioCreacion,
                    FechaCreacion = segmento.FechaCreacion,
                    IdUsuarioEdicion = request.IdUsuario,
                    FechaEdicion = DateTime.Now
                };
                iactividadSegmentoNegocioDal.Add(actividadSegmento);
            }
            iactividadSegmentoNegocioDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.EliminaRegistro;

            return respuesta;
        }
    }
}
