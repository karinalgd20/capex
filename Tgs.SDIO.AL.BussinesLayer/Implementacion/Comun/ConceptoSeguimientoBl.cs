﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Mensajes.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class ConceptoSeguimientoBl : IConceptoSeguimientoBl
    {
        readonly IConceptoSeguimientoDal iconceptoSeguimientoDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public ConceptoSeguimientoBl(IConceptoSeguimientoDal conceptoSeguimientoDal)
        {
            iconceptoSeguimientoDal = conceptoSeguimientoDal;
        }

        public List<ListaDtoResponse> ListarComboConceptoSeguimiento()
        {
            return iconceptoSeguimientoDal.ListarComboConceptoSeguimiento();
        }
        public List<ListaDtoResponse> ListarComboConceptoSeguimientoAgrupador()
        {
            return iconceptoSeguimientoDal.ListarComboConceptoSeguimientoAgrupador();
        }

        public List<ListaDtoResponse> ListarComboConceptoSeguimientoNiveles()
        {
            return iconceptoSeguimientoDal.ListarComboConceptoSeguimientoNiveles();
        }
        public ConceptoSeguimientoPaginadoDtoResponse ListadoConceptosSeguimientoPaginado(ConceptoSeguimientoDtoRequest request)
        {
            return iconceptoSeguimientoDal.ListadoConceptosSeguimientoPaginado(request);
        }

        public ConceptoSeguimientoDtoResponse ObtenerConceptoSeguimientoPorId(ConceptoSeguimientoDtoRequest request)
        {
            var query = iconceptoSeguimientoDal.GetFilteredAsNoTracking(x => x.IdConcepto == request.IdConcepto && (x.IdEstado == Activo || x.IdEstado == Inactivo)).ToList();
            return query.Select(x => new ConceptoSeguimientoDtoResponse
            {
                IdConcepto = x.IdConcepto,
                IdConceptoPadre = x.IdConceptoPadre,
                Descripcion = x.Descripcion,
                Nivel = x.Nivel,
                OrdenVisual = x.OrdenVisual,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion
            }).FirstOrDefault();
        }

        public ProcesoResponse ActualizarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            var areaAntigua = ObtenerConceptoSeguimientoPorId(request);
            var areaSeguimiento = new ConceptoSeguimiento()
            {
                IdConcepto = request.IdConcepto,
                IdConceptoPadre = request.IdConceptoPadre,
                Descripcion = request.Descripcion,
                Nivel = request.IdConceptoPadre == null? 1 : iconceptoSeguimientoDal.GetFilteredAsNoTracking(x => x.IdConcepto == request.IdConceptoPadre && x.IdEstado == Activo).FirstOrDefault().Nivel+1,
                OrdenVisual = request.OrdenVisual,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                FechaCreacion = areaAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now,
            };
            iconceptoSeguimientoDal.Modify(areaSeguimiento);
            iconceptoSeguimientoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.ActivoRegistro;

            return respuesta;
        }

        public ProcesoResponse RegistrarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            var areaSeguimiento = new ConceptoSeguimiento()
            {
                IdConceptoPadre = request.IdConceptoPadre,
                Descripcion = request.Descripcion,
                Nivel = request.IdConceptoPadre == null ? 1 : iconceptoSeguimientoDal.GetFilteredAsNoTracking(x => x.IdConcepto == request.IdConceptoPadre && x.IdEstado == Activo).FirstOrDefault().Nivel + 1,
                OrdenVisual = request.OrdenVisual,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = request.IdUsuario,
                FechaCreacion = DateTime.Now
            };
            iconceptoSeguimientoDal.Add(areaSeguimiento);
            iconceptoSeguimientoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.RegistraOk;

            return respuesta;
        }

        public ProcesoResponse EliminarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            var areaAntigua = ObtenerConceptoSeguimientoPorId(request);
            var areaSeguimiento = new ConceptoSeguimiento()
            {
                IdConcepto = request.IdConcepto,
                IdConceptoPadre = request.IdConceptoPadre,
                Descripcion = areaAntigua.Descripcion,
                Nivel = areaAntigua.Nivel,
                OrdenVisual = areaAntigua.OrdenVisual,
                IdEstado = Eliminado,
                IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                FechaCreacion = areaAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now
            };
            iconceptoSeguimientoDal.Modify(areaSeguimiento);
            iconceptoSeguimientoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.EliminaRegistro;

            return respuesta;
        }



    }
}
