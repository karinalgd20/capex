﻿using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class AreaSegmentoNegocioBl : IAreaSegmentoNegocioBl
    {

        readonly IAreaSegmentoNegocioDal iareaSegmentoNegocioDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public AreaSegmentoNegocioBl(IAreaSegmentoNegocioDal areaSegmentoNegocioDal)
        {
            iareaSegmentoNegocioDal = areaSegmentoNegocioDal;
        }


        public AreaSegmentoNegocioDtoResponse ObtenerAreaSegmentoNegocioPorId(AreaSegmentoNegocioDtoRequest areaseg)
        {
            var query = iareaSegmentoNegocioDal.GetFilteredAsNoTracking(x => x.IdAreaSegmentoNegocio == areaseg.IdAreaSegmentoNegocio && x.IdEstado == Activo).ToList();
            return query.Select(x => new AreaSegmentoNegocioDtoResponse
            {
                IdAreaSegmentoNegocio = x.IdAreaSegmentoNegocio,
                IdAreaSeguimiento = x.IdAreaSeguimiento,
                IdSegmentoNegocio = x.IdSegmentoNegocio,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion
            }).FirstOrDefault();
        }

        public ProcesoResponse RegistrarAreaSegmentoNegocio(AreaSegmentoNegocioDtoRequest request)
        {

                    var areaSegmento = new AreaSegmentoNegocio()
                    {
                        IdAreaSeguimiento = request.IdAreaSeguimiento,
                        IdSegmentoNegocio = request.IdSegmentoNegocio,
                        IdEstado = request.IdEstado,
                        IdUsuarioCreacion = request.IdUsuario,
                        FechaCreacion = DateTime.Now
                    };
                        iareaSegmentoNegocioDal.Add(areaSegmento);
                        iareaSegmentoNegocioDal.UnitOfWork.Commit();
                
          respuesta.TipoRespuesta = Proceso.Valido;
          respuesta.Mensaje = General.RegistraOk;


            return respuesta;
        }

        public ProcesoResponse EliminarAreaSegmentoNegocio(AreaSegmentoNegocioDtoRequest request)
        {
                    var areaAntigua = this.ObtenerAreaSegmentoNegocioPorId(request);
                    var areaSegmento = new AreaSegmentoNegocio()
                    {
                        IdAreaSegmentoNegocio = request.IdAreaSegmentoNegocio,
                        IdAreaSeguimiento = areaAntigua.IdAreaSeguimiento,
                        IdSegmentoNegocio = areaAntigua.IdSegmentoNegocio,
                        IdEstado = Eliminado,
                        IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                        FechaCreacion = areaAntigua.FechaCreacion,
                        IdUsuarioEdicion = request.IdUsuario,
                        FechaEdicion = DateTime.Now

                    };
                        iareaSegmentoNegocioDal.Modify(areaSegmento);
                        iareaSegmentoNegocioDal.UnitOfWork.Commit();
          respuesta.TipoRespuesta = Proceso.Valido;
          respuesta.Mensaje = General.EliminaRegistro;

            return respuesta;
        }


    }
}
