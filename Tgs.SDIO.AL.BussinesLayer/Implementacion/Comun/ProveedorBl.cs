﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ProveedorBl : IProveedorBl
    {
        readonly IProveedorDal iProveedorDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ProveedorBl(IProveedorDal IProveedorDal)
        {
            iProveedorDal = IProveedorDal;
        }

        public ProcesoResponse RegistrarProveedor(ProveedorDtoRequest objProveedor)
        {
            var validarProveedor = iProveedorDal.GetFilteredAsNoTracking(x => x.Descripcion == objProveedor.Descripcion).Any();

            if (!validarProveedor)
            {
                var Proveedor = new Proveedor()
                {
                    Descripcion = objProveedor.Descripcion,
                    TipoProveedor = objProveedor.TipoProveedor,
                    RazonSocial = objProveedor.RazonSocial,
                    CodigoProveedor = objProveedor.CodigoProveedor,
                    RUC = objProveedor.RUC,
                    Pais = objProveedor.Pais,
                    IdSecuencia = objProveedor.IdSecuencia,
                    NombrePersonaContacto = objProveedor.NombrePersonaContacto,
                    TelefonoContactoPrincipal = objProveedor.TelefonoContactoPrincipal,
                    CorreoContactoPrincipal = objProveedor.CorreoContactoPrincipal,
                    TelefonoContactoSecundario = objProveedor.TelefonoContactoSecundario,
                    CorreoContactoSecundario = objProveedor.CorreoContactoSecundario,
                    CodigoSRM = objProveedor.CodigoSRM,

                    IdEstado = objProveedor.IdEstado,
                    IdUsuarioCreacion = objProveedor.IdUsuarioCreacion,
                    FechaCreacion = objProveedor.FechaCreacion
                };

                iProveedorDal.Add(Proveedor);
                iProveedorDal.UnitOfWork.Commit();

                respuesta.Id = Proveedor.IdProveedor;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarProveedor;
            }
            else
            {
                respuesta.Id = 0;
                respuesta.TipoRespuesta = Proceso.Invalido;
                respuesta.Mensaje = MensajesGeneralComun.ProveedorDuplicado;
            }

            return respuesta;
        }

        public ProcesoResponse ActualizarProveedor(ProveedorDtoRequest objProveedor)
        {
            var validarProveedor = iProveedorDal.GetFilteredAsNoTracking(x => x.IdProveedor == objProveedor.IdProveedor).Any();
            var Proveedor = new Proveedor()
                {

                Descripcion = objProveedor.Descripcion,
                TipoProveedor = objProveedor.TipoProveedor,
                RazonSocial = objProveedor.RazonSocial,
                CodigoProveedor = objProveedor.CodigoProveedor,
                RUC = objProveedor.RUC,
                Pais = objProveedor.Pais,
                IdSecuencia = objProveedor.IdSecuencia,
                NombrePersonaContacto = objProveedor.NombrePersonaContacto,
                TelefonoContactoPrincipal = objProveedor.TelefonoContactoPrincipal,
                CorreoContactoPrincipal = objProveedor.CorreoContactoPrincipal,
                TelefonoContactoSecundario = objProveedor.TelefonoContactoSecundario,
                CorreoContactoSecundario = objProveedor.CorreoContactoSecundario,
                CodigoSRM = objProveedor.CodigoSRM,

                IdEstado = objProveedor.IdEstado,
                IdUsuarioCreacion = objProveedor.IdUsuarioCreacion,
                FechaCreacion = objProveedor.FechaCreacion


            };

                iProveedorDal.Modify(Proveedor);
                iProveedorDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.ActualizarProveedor;
        
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorProveedor, null, Generales.Sistemas.Comun);
            


            return respuesta;
        }

        public ProcesoResponse InactivarProveedor(ProveedorDtoRequest objProveedor)
        {
            var proveedor = iProveedorDal.Get(objProveedor.IdProveedor);
            proveedor.IdEstado = Generales.Estados.Inactivo;
            proveedor.IdUsuarioEdicion = objProveedor.IdUsuarioEdicion;
            proveedor.FechaEdicion = objProveedor.FechaEdicion;

            iProveedorDal.Modify(proveedor);
            iProveedorDal.UnitOfWork.Commit();

            respuesta.Id = proveedor.IdProveedor;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.EliminarProveedor;
            return respuesta;
        }

        public List<ListaDtoResponse> ListarProveedor(ProveedorDtoRequest proveedor)
        {
            var query = iProveedorDal.GetFilteredAsNoTracking(x =>x.IdEstado == proveedor.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
              
                Codigo = x.IdProveedor.ToString(),
                Descripcion = x.Descripcion,




            }).ToList();
        }

        public ProveedorDtoResponse ObtenerProveedor(ProveedorDtoRequest proveedor)
        {
            var objProveedor = iProveedorDal.GetFiltered(x => x.IdProveedor == proveedor.IdProveedor &&
                                                       x.IdEstado == proveedor.IdEstado);
            return objProveedor.Select(x => new ProveedorDtoResponse
            {
                IdProveedor = x.IdProveedor,
                Descripcion = x.Descripcion,
                TipoProveedor = x.TipoProveedor,
                RazonSocial = x.RazonSocial,
                CodigoProveedor = x.CodigoProveedor,
                RUC = x.RUC,
                Pais = x.Pais,
                IdSecuencia = x.IdSecuencia,
                NombrePersonaContacto = x.NombrePersonaContacto,
                TelefonoContactoPrincipal = x.TelefonoContactoPrincipal,
                CorreoContactoPrincipal = x.CorreoContactoPrincipal,
                TelefonoContactoSecundario = x.TelefonoContactoSecundario,
                CorreoContactoSecundario = x.CorreoContactoSecundario,
                CodigoSRM = x.CodigoSRM

            }).Single();

        }

        public List<ProveedorDtoResponse> ListarProveedores(ProveedorDtoRequest objProveedor)
        {
            return iProveedorDal.ListarProveedores(objProveedor);
        }
        public ProveedorPaginadoDtoResponse ListarProveedorPaginado(ProveedorDtoRequest objProveedor)
        {
            return iProveedorDal.ListarProveedorPaginado(objProveedor);
        }
    }
}
