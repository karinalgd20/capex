﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class DireccionComercialBl : IDireccionComercialBl
    {
        readonly IDireccionComercialDal iDireccionComercialDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public DireccionComercialBl(IDireccionComercialDal IDireccionComercialDal)
        {

            iDireccionComercialDal = IDireccionComercialDal;
        }


        public ProcesoResponse ActualizarDireccionComercial(DireccionComercialDtoRequest direccioncomercial)
        {
            
                var objDireccionComercial = new DireccionComercial()
                {

                    IdDireccion = direccioncomercial.IdDireccion,
                    Descripcion = direccioncomercial.Descripcion,
                    IdEstado = direccioncomercial.IdEstado,
                    IdUsuarioEdicion = direccioncomercial.IdUsuarioEdicion,
                    FechaEdicion = direccioncomercial.FechaEdicion


                };
                iDireccionComercialDal.Modify(objDireccionComercial);
                iDireccionComercialDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (direccioncomercial.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarDireccionComercial : MensajesGeneralComun.ActualizarDireccionComercial;
            
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorDireccionComercial, null, Generales.Sistemas.Comun);
          

            return respuesta;
        }

 
        public List<DireccionComercialDtoResponse> ListarDireccionComercial(DireccionComercialDtoRequest direccioncomercial)
        {
            var query = iDireccionComercialDal.GetFiltered(x => x.IdEstado == direccioncomercial.IdEstado ).ToList();
            return query.Select(x => new DireccionComercialDtoResponse
            {
                IdDireccion = x.IdDireccion,
                Descripcion = x.Descripcion,
                Codigo = x.IdDireccion

            }).ToList();
        }

        public DireccionComercialDtoResponse ObtenerDireccionComercial(DireccionComercialDtoRequest direccionComercial)
        {
            var objDireccionComercial = iDireccionComercialDal.GetFiltered(x => x.IdDireccion == direccionComercial.IdDireccion && x.IdEstado == direccionComercial.IdEstado);
            return objDireccionComercial.Select(x => new DireccionComercialDtoResponse
            {
                IdDireccion = x.IdDireccion,
                Descripcion = x.Descripcion
            }).Single();

        }

        public ProcesoResponse RegistrarDireccionComercial(DireccionComercialDtoRequest direccionComercial)
        {
           
                var objDireccionComercial = new DireccionComercial()
                {
                    IdDireccion = direccionComercial.IdDireccion,
                    Descripcion = direccionComercial.Descripcion,
                    IdEstado = direccionComercial.IdEstado,
                    IdUsuarioCreacion = direccionComercial.IdUsuarioCreacion,
                    FechaCreacion = DateTime.Now
                };

                iDireccionComercialDal.Add(objDireccionComercial);
                iDireccionComercialDal.UnitOfWork.Commit();

                respuesta.Id = objDireccionComercial.IdDireccion;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarDireccionComercial;
      
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorDireccionComercial, null, Generales.Sistemas.Comun);
            

            return respuesta;
        }
    }
}
