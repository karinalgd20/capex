﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class SubServicioBl : ISubServicioBl
    {
        readonly ISubServicioDal iSubServicioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SubServicioBl(ISubServicioDal ISubServicioDal)
        {
            iSubServicioDal = ISubServicioDal;

        }

        public ProcesoResponse ActualizarSubServicio(SubServicioDtoRequest subServicio)
        {

            DateTime fecha = DateTime.Now;

            var objSubServicio = new SubServicio()
            {
                IdSubServicio = subServicio.IdSubServicio,
                Descripcion = subServicio.Descripcion,
                IdTipoSubServicio = subServicio.IdTipoSubServicio,
                IdEstado = subServicio.IdEstado,
              
                IdUsuarioEdicion = subServicio.IdUsuarioEdicion,
                FechaEdicion = fecha,
                DescripcionEquivalencia = subServicio.Descripcion,  
                IdDepreciacion = subServicio.IdDepreciacion,
                Orden = Generales.LineasProducto.Datos,
                Negrita = Generales.Numeric.Cero,             
                CostoInstalacion = subServicio.CostoInstalacion,
              
            };


            iSubServicioDal.ActualizarPorCampos(objSubServicio, x => x.IdSubServicio, x => x.Descripcion, x => x.Descripcion, x => x.IdTipoSubServicio,
             x=>x.IdEstado,   x => x.IdUsuarioEdicion, x => x.DescripcionEquivalencia, x => x.IdDepreciacion, x => x.Orden, x => x.Negrita,x=> x.CostoInstalacion);

            iSubServicioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.ActualizarSubServicio;


          
            return respuesta;
        }

        public ProcesoResponse InhabilitarSubServicio(SubServicioDtoRequest subServicio)
        {

            DateTime fecha = DateTime.Now;

            var objSubServicio = new SubServicio()
            {
                IdSubServicio = subServicio.IdSubServicio,
                IdEstado = subServicio.IdEstado,
                IdUsuarioEdicion = subServicio.IdUsuarioEdicion,
                FechaEdicion = fecha
            };


            iSubServicioDal.ActualizarPorCampos(objSubServicio,x=>x.IdSubServicio, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iSubServicioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.EliminarSubServicio;



            return respuesta;
        }


        public SubServicioPaginadoDtoResponse ListaSubServicioPaginado(SubServicioDtoRequest subServicio)
        {
            return iSubServicioDal.ListaSubServicioPaginado(subServicio);

        }

        public List<ListaDtoResponse> ListarSubServicios(SubServicioDtoRequest subServicio)
        {
            var query = iSubServicioDal.GetFilteredAsNoTracking(x => x.IdEstado == subServicio.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdSubServicio.ToString(),
                Descripcion = x.Descripcion
            }).OrderBy(x => x.Descripcion).ToList();
        }
        public List<ListaDtoResponse> ListarSubServiciosPorIdGrupo(SubServicioDtoRequest subServicio)
        {
            var query = iSubServicioDal.GetFilteredAsNoTracking(x => x.IdEstado == subServicio.IdEstado
            && x.IdGrupo == subServicio.IdGrupo).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdSubServicio.ToString(),
                Descripcion = x.Descripcion
            }).OrderBy(x => x.Descripcion).ToList();
        }
        public SubServicioDtoResponse ObtenerSubServicio(SubServicioDtoRequest subServicio)
        {
            var objSubServicio = iSubServicioDal.GetFiltered(x => x.IdSubServicio == subServicio.IdSubServicio);
            return objSubServicio.Select(x => new SubServicioDtoResponse
            {
                IdSubServicio = x.IdSubServicio,
                Descripcion = x.Descripcion,
                IdEstado = x.IdEstado,
                IdTipoSubServicio = x.IdTipoSubServicio,
                IdDepreciacion = x.IdDepreciacion,
                CostoInstalacion=x.CostoInstalacion,
            }).Single();

        }

        public ProcesoResponse RegistrarSubServicio(SubServicioDtoRequest subServicio)
        {
            DateTime fecha = DateTime.Now;
            var objSubServicio = new SubServicio()
            {
               
                Descripcion = subServicio.Descripcion,
                DescripcionEquivalencia = subServicio.Descripcion,
                IdTipoSubServicio = subServicio.IdTipoSubServicio,
                IdDepreciacion= subServicio.IdDepreciacion,
                Orden=LineasProducto.Datos,
                Negrita=Numeric.Cero,
                IdEstado =Estados.Activo,
                CostoInstalacion= subServicio.CostoInstalacion,
                IdUsuarioCreacion = subServicio.IdUsuarioCreacion,
                FechaCreacion =fecha
            };

            iSubServicioDal.Add(objSubServicio);
            iSubServicioDal.UnitOfWork.Commit();

         
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.RegistrarSubServicio;
            respuesta.Id = objSubServicio.IdSubServicio;


            return respuesta;
        }
    }
}
