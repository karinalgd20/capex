﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class TipoCambioDetalleBl : ITipoCambioDetalleBl
    {
        readonly ITipoCambioDetalleDal iTipoCambioDetalleDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public TipoCambioDetalleBl(ITipoCambioDetalleDal ITipoCambioDetalleDal)
        {
            iTipoCambioDetalleDal = ITipoCambioDetalleDal;
        }
        public ProcesoResponse ActualizarTipoCambioDetalle(TipoCambioDetalleDtoRequest TipoCambioDetalle)
        {

            DateTime fecha = DateTime.Now;

            var objTipoCambioDetalle = new TipoCambioDetalle()
            {


            };


            //iTipoCambioDetalleDal.ActualizarPorCampos(objTipoCambioDetalle, x => x.IdTipoCambioDetalle, x => x.Descripcion, x => x.Descripcion, x => x.IdTipoTipoCambioDetalle,
            // x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.DescripcionEquivalencia, x => x.IdDepreciacion, x => x.Orden, x => x.Negrita, x => x.CostoInstalacion);

            iTipoCambioDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
           


            return respuesta;
        }

        public ProcesoResponse InhabilitarTipoCambioDetalle(TipoCambioDetalleDtoRequest TipoCambioDetalle)
        {

            DateTime fecha = DateTime.Now;

            var objTipoCambioDetalle = new TipoCambioDetalle()
            {
                IdTipoCambioDetalle = TipoCambioDetalle.IdTipoCambioDetalle,
              
                FechaEdicion = fecha
            };


            iTipoCambioDetalleDal.ActualizarPorCampos(objTipoCambioDetalle, x => x.IdTipoCambioDetalle, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iTipoCambioDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;                        



            return respuesta;
        }




        public TipoCambioDetalleDtoResponse ObtenerTipoCambioDetalle(TipoCambioDetalleDtoRequest TipoCambioDetalle)
        {
            var objTipoCambioDetalle = iTipoCambioDetalleDal.GetFiltered(x => x.IdTipoCambioDetalle == TipoCambioDetalle.IdTipoCambioDetalle);
            return objTipoCambioDetalle.Select(x => new TipoCambioDetalleDtoResponse
            {
                IdTipoCambioDetalle = x.IdTipoCambioDetalle,
               
            }).Single();

        }

        public ProcesoResponse RegistrarTipoCambioDetalle(TipoCambioDetalleDtoRequest TipoCambioDetalle)
        {
            DateTime fecha = DateTime.Now;
            var objTipoCambioDetalle = new TipoCambioDetalle()
            {

             
                FechaCreacion = fecha
            };

            iTipoCambioDetalleDal.Add(objTipoCambioDetalle);
            iTipoCambioDetalleDal.UnitOfWork.Commit();


            respuesta.TipoRespuesta = Proceso.Valido;                     
            respuesta.Id = objTipoCambioDetalle.IdTipoCambioDetalle;


            return respuesta;
        }
    }
}
