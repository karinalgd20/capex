﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ClienteBl : IClienteBl
    {
        readonly IClienteDal iClienteDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ClienteBl(IClienteDal IClienteDal)
        {
            iClienteDal = IClienteDal;
        }

        public ProcesoResponse ActualizarCliente(ClienteDtoRequest cliente)
        {
            try
            {
                var objCliente = new Cliente()
                {
                    IdCliente = cliente.IdCliente,
                    CodigoCliente = cliente.CodigoCliente,
                    NumeroIdentificadorFiscal = cliente.NumeroIdentificadorFiscal,
                    Descripcion = cliente.Descripcion,
                    GerenteComercial = cliente.GerenteComercial,
                    IdDireccionComercial = cliente.IdDireccionComercial,
                    IdSector = cliente.IdSector,
                    IdTipoIdentificadorFiscalTm = cliente.IdTipoIdentificadorFiscalTm,
                    IdTipoEntidad = cliente.IdTipoEntidad,
                    Email = cliente.Email,
                    Direccion = cliente.Direccion,
                    IdEstado = cliente.IdEstado,
                    IdUsuarioEdicion = cliente.IdUsuarioEdicion,
                    FechaEdicion = DateTime.Now
                };

                iClienteDal.ActualizarPorCampos(objCliente,
                                                x => x.CodigoCliente,
                                                x => x.NumeroIdentificadorFiscal,
                                                x => x.Descripcion,
                                                x => x.GerenteComercial,
                                                x => x.IdDireccionComercial,
                                                x => x.IdSector,
                                                x => x.IdTipoIdentificadorFiscalTm,
                                                x => x.IdTipoEntidad,
                                                x => x.IdTipoEntidad,
                                                x => x.Email,
                                                x => x.Direccion,
                                                x => x.IdEstado,
                                                x => x.IdUsuarioEdicion,
                                                x => x.FechaEdicion);
                iClienteDal.UnitOfWork.Commit();


                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (cliente.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarCliente : MensajesGeneralComun.ActualizarCliente;

            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(null, null, Generales.Sistemas.Comun);
            }


            return respuesta;
        }

        public List<ClienteDtoResponse> ListarClientePorDescripcion(ClienteDtoRequest cliente)
        {
            return iClienteDal.ListarClientePorDescripcion(cliente);
        }
        public List<ClienteDtoResponse> ListarCliente(ClienteDtoRequest cliente)
        {
            var query = iClienteDal.GetFilteredAsNoTracking(x => x.IdEstado == cliente.IdEstado).ToList();
            return query.Select(x => new ClienteDtoResponse
            {
                IdCliente = x.IdCliente,
                CodigoCliente = x.CodigoCliente,
                Descripcion = x.Descripcion,
                IdEstado = x.IdEstado,
                IdSector = x.IdSector,
                GerenteComercial = x.GerenteComercial,
                IdDireccionComercial = x.IdDireccionComercial
            }).ToList();
        }

        public List<ListaDtoResponse> ListarClienteCartaFianza(ClienteDtoRequest cliente)
        {
            return iClienteDal.ListarClienteCartaFianza(cliente);
        }

        public ClienteDtoResponsePaginado ListarClientePaginado(ClienteDtoRequest cliente)
        {
            return iClienteDal.ListarClientePaginado(cliente);
        }


        public ClienteDtoResponse ObtenerCliente(ClienteDtoRequest cliente)
        {
            var objCliente = iClienteDal.GetFiltered(x => x.IdCliente == cliente.IdCliente &&
                                                       x.IdEstado == cliente.IdEstado);
            return objCliente.Select(x => new ClienteDtoResponse
            {
                IdCliente = x.IdCliente,
                CodigoCliente = x.CodigoCliente,
                Descripcion = x.Descripcion,
                IdEstado = x.IdEstado,
                IdSector = x.IdSector,
                GerenteComercial = x.GerenteComercial,
                IdDireccionComercial = x.IdDireccionComercial,
                IdTipoIdentificadorFiscalTm = x.IdTipoIdentificadorFiscalTm,
                Email = x.Email,
                NumeroIdentificadorFiscal = x.NumeroIdentificadorFiscal
            }).Single();

        }
        public ClienteDtoResponse ObtenerClientePorCodigo(ClienteDtoRequest cliente)
        {
            var objCliente = iClienteDal.GetFiltered(x => x.CodigoCliente == cliente.CodigoCliente &&
                                                       x.IdEstado == cliente.IdEstado);
            return objCliente.Select(x => new ClienteDtoResponse
            {
                IdCliente = x.IdCliente,
                CodigoCliente = x.CodigoCliente,
                Descripcion = x.Descripcion,
                IdEstado = x.IdEstado,
                IdSector = x.IdSector,
                GerenteComercial = x.GerenteComercial,
                IdDireccionComercial = x.IdDireccionComercial,
                IdTipoIdentificadorFiscalTm = x.IdTipoIdentificadorFiscalTm,
                Email = x.Email,
                NumeroIdentificadorFiscal = x.NumeroIdentificadorFiscal
            }).Single();

        }
        public ProcesoResponse RegistrarCliente(ClienteDtoRequest cliente)
        {
            try
            {
                var objCliente = new Cliente()
                {
                    IdCliente = cliente.IdCliente,
                    CodigoCliente = cliente.CodigoCliente,
                    NumeroIdentificadorFiscal = cliente.NumeroIdentificadorFiscal,
                    Descripcion = cliente.Descripcion,
                    GerenteComercial = cliente.GerenteComercial,
                    IdDireccionComercial = cliente.IdDireccionComercial,
                    IdSector = cliente.IdSector,
                    IdTipoIdentificadorFiscalTm = cliente.IdTipoIdentificadorFiscalTm,
                    IdTipoEntidad = cliente.IdTipoEntidad,
                    Email = cliente.Email,
                    Direccion = cliente.Direccion,
                    IdEstado = cliente.IdEstado,
                    IdUsuarioCreacion = cliente.IdUsuarioCreacion,
                    FechaCreacion = DateTime.Now
                };

                iClienteDal.Add(objCliente);
                iClienteDal.UnitOfWork.Commit();

                respuesta.Id = objCliente.IdCliente;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarCliente;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorCliente, null, Generales.Sistemas.Comun);
            }

            return respuesta;
        }

        public ProcesoResponse ActualizarClienteModalPreventa(ClienteDtoRequest request)
        {
            var dataAntigua = ObtenerCliente(request);
            var cliente = new Cliente()
            {
                IdCliente = request.IdCliente,
                CodigoCliente = dataAntigua.CodigoCliente,
                Descripcion = request.Descripcion,
                IdSector = dataAntigua.IdSector,
                GerenteComercial = dataAntigua.GerenteComercial,
                IdDireccionComercial = dataAntigua.IdDireccionComercial,
                NumeroIdentificadorFiscal = request.NumeroIdentificadorFiscal,
                IdTipoIdentificadorFiscalTm = dataAntigua.IdTipoIdentificadorFiscalTm,
                Email = dataAntigua.Email,
                IdEstado = dataAntigua.IdEstado,
                IdUsuarioCreacion = dataAntigua.IdUsuarioCreacion,
                FechaCreacion = dataAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now,
            };
            iClienteDal.Modify(cliente);
            iClienteDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }

    }
}