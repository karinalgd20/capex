﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class DGBl : IDGBl
    {
        readonly IDGDal iDGDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public DGBl(IDGDal IDGDal)
        {
           iDGDal = IDGDal;
        }

        public ProcesoResponse ActualizarDG(DGDtoRequest dg)
        {
            
                var objDG = new DG()
                {

                    IdDG = dg.IdDG,
                    IdConcepto = dg.IdConcepto,
                    Concatenado = dg.Concatenado,
                    AEReducido = dg.AEReducido,
                    IdEstado = dg.IdEstado,
                    IdUsuarioEdicion = dg.IdUsuarioEdicion,
                    FechaEdicion = dg.FechaEdicion,
                    Tipo = dg.Tipo

                };
                iDGDal.Modify(objDG);
                iDGDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (dg.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarDG : MensajesGeneralComun.ActualizarDG;
         
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorDG, null, Generales.Sistemas.Comun);
           

            return respuesta;
        }

        public List<DGDtoResponse> ListarDG(DGDtoRequest dg)
        {
            var query = iDGDal.GetFilteredAsNoTracking(x => x.IdDG == dg.IdDG && x.IdEstado == dg.IdEstado).ToList();
            return query.Select(x => new DGDtoResponse
            {
                IdDG = dg.IdDG,
                IdConcepto = dg.IdConcepto,
                Concatenado = dg.Concatenado,
                AEReducido = dg.AEReducido,
                IdEstado = dg.IdEstado,
                Tipo = dg.Tipo


            }).ToList();
        }





        public DGDtoResponse ObtenerDG(DGDtoRequest dg)
        {

            var objBW = iDGDal.GetFiltered(x => x.IdDG == dg.IdDG &&
                                                         x.IdEstado == dg.IdEstado);
            return objBW.Select(x => new DGDtoResponse
            {
                IdDG = dg.IdDG,
                IdConcepto = dg.IdConcepto,
                Concatenado = dg.Concatenado,
                AEReducido = dg.AEReducido,
                IdEstado = dg.IdEstado,
                Tipo = dg.Tipo
            }).Single();
        }


        public ProcesoResponse RegistrarDG(DGDtoRequest dg)
        {

           
                var objDG = new DG()
                {
                    IdDG = dg.IdDG,
                    IdConcepto = dg.IdConcepto,
                    Concatenado = dg.Concatenado,
                    AEReducido = dg.AEReducido,
                    IdEstado = dg.IdEstado,
                    Tipo = dg.Tipo,
                    IdUsuarioCreacion = dg.IdUsuarioCreacion,
                    FechaCreacion = dg.FechaCreacion
                };

                iDGDal.Add(objDG);
                iDGDal.UnitOfWork.Commit();

                respuesta.Id = objDG.IdDG;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarDG;
           
           
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorDG, null, Generales.Sistemas.Comun);
           

            return respuesta;
        }

      
    }
}
