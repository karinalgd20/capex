﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Linq;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class AreaSeguimientoBl : IAreaSeguimientoBl
    {
        readonly IAreaSeguimientoDal iareaSeguimientoDal;
        readonly IAreaSegmentoNegocioDal iareaSegmentoNegocioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public AreaSeguimientoBl(IAreaSeguimientoDal areaSeguimientoDal, IAreaSegmentoNegocioDal areaSegmentoNegocioDal)
        {
            iareaSeguimientoDal = areaSeguimientoDal;
            iareaSegmentoNegocioDal = areaSegmentoNegocioDal;
        }

        public AreaSeguimientoPaginadoDtoResponse ListadoAreasSeguimientoPaginado(AreaSeguimientoDtoRequest request)
        {
            return iareaSeguimientoDal.ListadoAreasSeguimientoPaginado(request);
        }

        public List<ListaDtoResponse> ListarComboAreaSeguimiento()
        {
            return iareaSeguimientoDal.ListarComboAreaSeguimiento();
        }

        public AreaSeguimientoDtoResponse ObtenerAreaSeguimientoPorId(AreaSeguimientoDtoRequest request)
        {
            return iareaSeguimientoDal.ObtenerAreaSeguimientoPorId(request);
        }

        public ProcesoResponse ActualizarAreaSeguimiento(AreaSeguimientoDtoRequest request)
        {
                        var areaAntigua = ObtenerAreaSeguimientoPorId(request);
                        var areaSeguimiento = new AreaSeguimiento()
                        {
                            IdAreaSeguimiento = request.IdAreaSeguimiento,
                            Descripcion = request.Descripcion,
                            OrdenVisual = request.OrdenVisual,
                            IdEstado = request.IdEstado,
                            IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                            FechaCreacion = areaAntigua.FechaCreacion,
                            IdUsuarioEdicion = request.IdUsuario,
                            FechaEdicion = DateTime.Now,
                        };
                        iareaSeguimientoDal.Modify(areaSeguimiento);
                        iareaSeguimientoDal.UnitOfWork.Commit();
                    var segmentos = iareaSegmentoNegocioDal.GetFilteredAsNoTracking(x => x.IdAreaSeguimiento == request.IdAreaSeguimiento && x.IdEstado == Activo).ToList();
                        iareaSegmentoNegocioDal.Remove(segmentos);
                        foreach (ComboDtoResponse segmento in request.ListSegmentoNegocio)
                        {
                            var areaSegmentoNegocio = new AreaSegmentoNegocio()
                            {
                                IdAreaSeguimiento = areaSeguimiento.IdAreaSeguimiento,
                                IdSegmentoNegocio = Convert.ToInt32(segmento.id),
                                IdEstado = Activo,
                                IdUsuarioCreacion = request.IdUsuario,
                                FechaCreacion = DateTime.Now
                            };
                            iareaSegmentoNegocioDal.Add(areaSegmentoNegocio);
                        }
                            iareaSegmentoNegocioDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActivoRegistro;

            return respuesta;
        }

        public ProcesoResponse RegistrarAreaSeguimiento(AreaSeguimientoDtoRequest request) {
                        var areaSeguimiento = new AreaSeguimiento()
                        {
                            Descripcion = request.Descripcion,
                            OrdenVisual = request.OrdenVisual,
                            IdEstado = request.IdEstado,
                            IdUsuarioCreacion = request.IdUsuario,
                            FechaCreacion = DateTime.Now
                        };
                            iareaSeguimientoDal.Add(areaSeguimiento);
                            iareaSeguimientoDal.UnitOfWork.Commit();
                    foreach (ComboDtoResponse segmento in request.ListSegmentoNegocio)
                        {
                            var areaSegmentoNegocio = new AreaSegmentoNegocio()
                            {
                                IdAreaSeguimiento = areaSeguimiento.IdAreaSeguimiento,
                                IdSegmentoNegocio = Convert.ToInt32(segmento.id),
                                IdEstado = Activo,
                                IdUsuarioCreacion = request.IdUsuario,
                                FechaCreacion = DateTime.Now
                            };
                            iareaSegmentoNegocioDal.Add(areaSegmentoNegocio);
                    
                        }
                            iareaSegmentoNegocioDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = General.RegistraOk;

            return respuesta;
        }

        public ProcesoResponse EliminarAreaSeguimiento(AreaSeguimientoDtoRequest request) {
                        var areaAntigua = ObtenerAreaSeguimientoPorId(request);
                        var areaSeguimiento = new AreaSeguimiento()
                        {
                            IdAreaSeguimiento = request.IdAreaSeguimiento,
                            Descripcion = areaAntigua.Descripcion,
                            OrdenVisual = areaAntigua.OrdenVisual,
                            IdEstado = Eliminado,
                            IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                            FechaCreacion = areaAntigua.FechaCreacion,
                            IdUsuarioEdicion = request.IdUsuario,
                            FechaEdicion = DateTime.Now
                        };
                        iareaSeguimientoDal.Modify(areaSeguimiento);
                        iareaSeguimientoDal.UnitOfWork.Commit();
                        var segmentos = iareaSegmentoNegocioDal.GetFilteredAsNoTracking(x => x.IdAreaSeguimiento == request.IdAreaSeguimiento && x.IdEstado == Activo).ToList();
                            foreach (AreaSegmentoNegocio segmento in segmentos)
                            {
                                var areaSegmentoNegocio = new AreaSegmentoNegocio()
                                {
                                    IdAreaSeguimiento = segmento.IdAreaSeguimiento,
                                    IdSegmentoNegocio = segmento.IdSegmentoNegocio,
                                    IdEstado = Eliminado,
                                    IdUsuarioCreacion = segmento.IdUsuarioCreacion,
                                    FechaCreacion = segmento.FechaCreacion,
                                    IdUsuarioEdicion = request.IdUsuario,
                                    FechaEdicion = DateTime.Now
                                };
                                iareaSegmentoNegocioDal.Add(areaSegmentoNegocio);
                            }
                                iareaSegmentoNegocioDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;

            return respuesta;
        }

    }
}
