﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Funciones;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class CorreoBl : ICorreoBl
    {
        readonly ICorreoDal iCorreoDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public CorreoBl(ICorreoDal ICorreoDal)
        {
            iCorreoDal = ICorreoDal;
        }

        public ProcesoResponse EnviarCorreo(MailDto mailDto)
        {
            try
            {
                DatosCorreo datosCorreo = new DatosCorreo()
                {
                    CorreoEnvia = mailDto.From,
                    Destinatarios = mailDto.To,
                    Copia = mailDto.Copy,
                    Asunto = mailDto.Subject,
                    Cuerpo = mailDto.Body,
                    Adjuntos = mailDto.Attachment
                };
                respuesta.Mensaje = Funciones.EnviarCorreo(datosCorreo);
            }
            catch (Exception ex)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                respuesta.Mensaje = ex.Message;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.CartaFianza);
            }

            return respuesta;
        }
    }
}
