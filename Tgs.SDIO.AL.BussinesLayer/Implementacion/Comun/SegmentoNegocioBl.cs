﻿using System.Collections.Generic;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class SegmentoNegocioBl : ISegmentoNegocioBl
    {
        readonly ISegmentoNegocioDal isegmentoNegocioDal;
        public SegmentoNegocioBl(ISegmentoNegocioDal segmentoNegocioDal)
        {
            isegmentoNegocioDal = segmentoNegocioDal;
        }

        public List<ComboDtoResponse> ListarComboSegmentoNegocio()
        {
            return isegmentoNegocioDal.ListarComboSegmentoNegocio();
        }

        public List<ListaDtoResponse> ListarComboSegmentoNegocioSimple()
        {
            return isegmentoNegocioDal.ListarComboSegmentoNegocioSimple();
        }
    }
}
