﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using System.Collections.Generic;
using static Tgs.SDIO.Util.Constantes.Comun;
using System.Diagnostics;
namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class RecursoBl : IRecursoBl
    {
        readonly IRecursoDal irecursoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public RecursoBl(IRecursoDal recursoDal)
        {
            irecursoDal = recursoDal;
        }

        public RecursoPaginadoDtoResponse ListadoRecursosPaginado(RecursoDtoRequest request)
        {
            return irecursoDal.ListadoRecursosPaginado(request);
        }

        public RecursoPaginadoDtoResponse ListaRecursoModalRecursoPaginado(RecursoDtoRequest request)
        {
            return irecursoDal.ListaRecursoModalRecursoPaginado(request);
        }

        public RecursoDtoResponse ObtenerRecursoPorId(RecursoDtoRequest request)
        {
            var query = irecursoDal.GetFilteredAsNoTracking(x => x.IdRecurso == request.IdRecurso && (x.IdEstado == Activo || x.IdEstado == Inactivo)).ToList();
            return query.Select(x => new RecursoDtoResponse
            {
                IdRecurso = x.IdRecurso,
                TipoRecurso = x.TipoRecurso,
                UserNameSF = x.UserNameSF,
                Nombre = x.Nombre,
                DNI = x.DNI,
                Email = x.Email,
                IdEstado = x.IdEstado,
                IdRolDefecto = x.IdRolDefecto,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion,
                IdCargo = x.IdCargo,
                IdEmpresa = x.IdEmpresa,
                IdArea = x.IdArea,
                IdUsuarioRais = x.IdUsuarioRais
            }).FirstOrDefault();
        }

        public ProcesoResponse ActualizarRecurso(RecursoDtoRequest request)
        {
            try
            {
                var areaAntigua = ObtenerRecursoPorId(request);
                var areaSeguimiento = new Recurso()
                {
                    IdRecurso = request.IdRecurso,
                    TipoRecurso = request.TipoRecurso,
                    UserNameSF = request.UserNameSF,
                    Nombre = request.Nombre,
                    DNI = request.DNI,
                    Email = request.Email,
                    IdRolDefecto = request.IdRolDefecto,
                    IdCargo = request.IdCargo,
                    IdEmpresa = request.IdEmpresa,
                    IdArea = request.IdArea,
                    IdEstado = request.IdEstado,
                    IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                    FechaCreacion = areaAntigua.FechaCreacion,
                    IdUsuarioEdicion = request.IdUsuario,
                    FechaEdicion = DateTime.Now,
                };
                irecursoDal.Modify(areaSeguimiento);
                irecursoDal.UnitOfWork.Commit();
            }
            catch (Exception)
            {

                throw;
            }
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }

        public ProcesoResponse RegistrarRecurso(RecursoDtoRequest request)
        {
            var recurso = new Recurso()
            {
                TipoRecurso = request.TipoRecurso,
                UserNameSF = request.UserNameSF,
                Nombre = request.Nombre,
                DNI = request.DNI,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = request.IdUsuario,
                FechaCreacion = DateTime.Now,
                Email = request.Email,
                IdUsuarioRais = request.IdUsuarioRais,
                IdArea = request.IdArea,
                IdRolDefecto = request.IdRolDefecto,
                IdEmpresa = request.IdEmpresa,
                IdCargo = request.IdCargo,
                NombreInterno = request.NombreInterno
            };

            irecursoDal.Add(recurso);
            irecursoDal.UnitOfWork.Commit();

            respuesta.Id = recurso.IdRecurso;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;
        }

        public ProcesoResponse EliminarRecurso(RecursoDtoRequest request)
        {
            var areaAntigua = ObtenerRecursoPorId(request);
            var areaSeguimiento = new Recurso()
            {
                IdRecurso = request.IdRecurso,
                TipoRecurso = areaAntigua.TipoRecurso,
                UserNameSF = areaAntigua.UserNameSF,
                Nombre = areaAntigua.Nombre,
                DNI = areaAntigua.DNI,
                Email = areaAntigua.Email,
                IdRolDefecto = areaAntigua.IdRolDefecto,
                IdCargo = areaAntigua.IdCargo,
                IdEmpresa = areaAntigua.IdEmpresa,
                IdArea = areaAntigua.IdArea,
                IdEstado = Eliminado,
                IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                FechaCreacion = areaAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuario,
                FechaEdicion = DateTime.Now
            };
            irecursoDal.Modify(areaSeguimiento);
            irecursoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;

            return respuesta;
        }

        public ProcesoResponse ActualizarRecursoUsuario(RecursoDtoRequest recursoDtoRequest)
        {
            var recurso = new Recurso()
            {
                IdRecurso = recursoDtoRequest.IdRecurso,
                Nombre = recursoDtoRequest.Nombre,
                IdEstado = recursoDtoRequest.IdEstado,
                IdUsuarioEdicion = recursoDtoRequest.IdUsuario,
                FechaEdicion = DateTime.Now,
                IdArea = recursoDtoRequest.IdArea,
                IdCargo = recursoDtoRequest.IdCargo
            };

            irecursoDal.ActualizarPorCampos(recurso,
                                    x => x.IdRecurso,
                                    x => x.Nombre,
                                    x => x.IdEstado,
                                    x => x.IdUsuarioEdicion,
                                    x => x.FechaEdicion,
                                     x => x.IdArea,
                                    x => x.IdCargo
                                    );


            irecursoDal.UnitOfWork.Commit();
            respuesta.Id = recursoDtoRequest.IdRecurso;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }


        public RecursoDtoResponse ObtenerRecursoPorIdUsuarioRais(int idUsuarioRais)
        {
            var query = irecursoDal.GetFilteredAsNoTracking(
                x => x.IdUsuarioRais == idUsuarioRais
                ).ToList();

            return query.Select(x => new RecursoDtoResponse
            {
                IdRecurso = x.IdRecurso,
                IdArea= x.IdArea,
                IdCargo=x.IdCargo,
                NombreInterno = x.NombreInterno
            }).FirstOrDefault();
        }

        public RecursoDtoResponse ObtenerRecursoPorCorreo(string correo)
        {
            var query = irecursoDal.GetFilteredAsNoTracking(
                x => x.Email == correo
                ).ToList();

            return query.Select(x => new RecursoDtoResponse
            {
                IdRecurso = x.IdRecurso,
                IdUsuarioRais=x.IdUsuarioRais
            }).FirstOrDefault();
        }

        public List<RecursoDtoResponse> ListarRecursoPorCargo(RecursoDtoRequest request)
        {
            var query = irecursoDal.GetFilteredAsNoTracking(x => x.IdCargo == request.IdCargo && x.IdEstado == EstadosEntidad.Activo).ToList();
            return query.Select(x => new RecursoDtoResponse
            {
                IdRecurso = x.IdRecurso,
                Nombre = x.Nombre,
                Email = x.Email,
                IdUsuarioRais = x.IdUsuarioRais
            }).ToList();
        }

        public RecursoDtoResponse ListarRecursoDeUsuario(RecursoDtoRequest request)
        {
            return irecursoDal.ListarRecursoDeUsuario(request);

        }

        public RecursoPaginadoDtoResponse ListadoRecursosByCargoPaginado(RecursoDtoRequest request)
        {
            return irecursoDal.ListadoRecursosByCargoPaginado(request);
        }
    }
}
