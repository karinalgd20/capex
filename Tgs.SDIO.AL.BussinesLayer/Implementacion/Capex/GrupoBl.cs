﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Mensajes.Capex;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class GrupoBl : IGrupoBl
    {
        readonly IGrupoDal iGrupoDal;

        ProcesoResponse respuesta = new ProcesoResponse();
        public GrupoBl(IGrupoDal IGrupoDal)
        {
            iGrupoDal = IGrupoDal;
        }

        public ProcesoResponse ActualizarGrupo(GrupoDtoRequest grupo)
        {
            try
            {
                var objGrupo = new Grupo()
                {
                    IdGrupo = grupo.IdGrupo,
                    Descripcion = grupo.Descripcion,
                    IdEstado = grupo.IdEstado,
                    IdUsuarioEdicion = grupo.IdUsuarioEdicion,
                    FechaEdicion = DateTime.Now
                };

                iGrupoDal.ActualizarPorCampos(objGrupo, x => x.IdGrupo, x => x.Descripcion, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
                iGrupoDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.ActualizarGrupo;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }

        public List<GrupoDtoResponse> ListarBandejaGrupo(GrupoDtoRequest Grupo)
        {
            throw new NotImplementedException();
        }

        public List<ListaDtoResponse> ListarGrupo(GrupoDtoRequest grupo)
        {
            var objGrupo = iGrupoDal.GetFiltered(x => x.IdEstado == grupo.IdEstado);
            return objGrupo.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdGrupo.ToString(),
                Descripcion = x.Descripcion
            }).ToList();
        }

        public GrupoDtoResponse ObtenerGrupo(GrupoDtoRequest grupo)
        {
            var objGrupo = iGrupoDal.GetFiltered(x => x.IdGrupo == grupo.IdGrupo &&
                                                         x.IdEstado == grupo.IdEstado);
            return objGrupo.Select(x => new GrupoDtoResponse
            {
                IdGrupo = x.IdGrupo,
                Descripcion = x.Descripcion
            }).Single();
        }

        public ProcesoResponse RegistrarGrupo(GrupoDtoRequest grupo)
        {
            try
            {
                var objGrupo = new Grupo()
                {
                    IdGrupo = grupo.IdGrupo,
                    Descripcion = grupo.Descripcion,
                    IdEstado = grupo.IdEstado,
                    IdUsuarioCreacion = grupo.IdUsuarioCreacion,
                    FechaCreacion = DateTime.Now
                };

                iGrupoDal.Add(objGrupo);
                iGrupoDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.RegistrarGrupo;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }
    }
}
