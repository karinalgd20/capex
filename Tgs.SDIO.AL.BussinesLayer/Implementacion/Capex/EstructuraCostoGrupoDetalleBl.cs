﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Mensajes.Capex;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class EstructuraCostoGrupoDetalleBl : IEstructuraCostoGrupoDetalleBl
    {
        readonly IEstructuraCostoGrupoDetalleDal iEstructuraCostoGrupoDetalleDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public EstructuraCostoGrupoDetalleBl(IEstructuraCostoGrupoDetalleDal IEstructuraCostoGrupoDetalleDal)
        {
            iEstructuraCostoGrupoDetalleDal = IEstructuraCostoGrupoDetalleDal;
        }

        public ProcesoResponse ActualizarEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalleDtoRequest estructuraCostoGrupoDetalle)
        {
           
                var objEstructuraCostoGrupo = new EstructuraCostoGrupoDetalle()
                {
                    IdEstructuraCostoGrupo = estructuraCostoGrupoDetalle.IdEstructuraCostoGrupo,
                    IdEstructuraCostoGrupoDetalle = estructuraCostoGrupoDetalle.IdEstructuraCostoGrupoDetalle,
                    Asignacion = estructuraCostoGrupoDetalle.Asignacion,
                    Modelo = estructuraCostoGrupoDetalle.Modelo,
                    Cantidad = estructuraCostoGrupoDetalle.Cantidad,
                    CapexDolares = estructuraCostoGrupoDetalle.CapexDolares,
                    CapexSoles = estructuraCostoGrupoDetalle.CapexSoles,
                    CapexTotalSoles = estructuraCostoGrupoDetalle.CapexTotalSoles,
                    Certificacion = estructuraCostoGrupoDetalle.Certificacion,
                    CuNuevoDolar = estructuraCostoGrupoDetalle.CuNuevoDolar,
                    IdConcepto = estructuraCostoGrupoDetalle.IdConcepto,
                    IdMedioCosto = estructuraCostoGrupoDetalle.IdMedioCosto,
                    IdServicio = estructuraCostoGrupoDetalle.IdServicio,
                    IdTipoGrupo = estructuraCostoGrupoDetalle.IdTipoGrupo,
                    IdUbigeo = estructuraCostoGrupoDetalle.IdUbigeo,
                    Descripcion = estructuraCostoGrupoDetalle.Descripcion,
                    IdEstado = estructuraCostoGrupoDetalle.IdEstado,
                    IdUsuarioEdicion = estructuraCostoGrupoDetalle.IdUsuarioEdicion,
                    FechaEdicion = DateTime.Now
                };

                iEstructuraCostoGrupoDetalleDal.ActualizarPorCampos(objEstructuraCostoGrupo, x=> x.IdEstructuraCostoGrupo, x=> x.IdEstructuraCostoGrupoDetalle, 
                    x=> x.Asignacion, x => x.Modelo, x => x.Cantidad, x => x.CapexDolares, x => x.CapexSoles, x => x.CapexTotalSoles, x => x.Certificacion,
                    x => x.CuNuevoDolar, x => x.IdConcepto, x => x.IdMedioCosto, x => x.IdServicio, x => x.IdTipoGrupo, x => x.IdUbigeo, x => x.Descripcion,
                    x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
                iEstructuraCostoGrupoDetalleDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.ActualizarEstructuraCostoGrupoDetalle;

       
            return respuesta;
        }

       

        public List<EstructuraCostoGrupoDetalleDtoResponse> ListarEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalleDtoRequest estructuraCostoGrupoDetalle)
        {
            var objServicio = iEstructuraCostoGrupoDetalleDal.GetFiltered(x => x.IdEstructuraCostoGrupo == estructuraCostoGrupoDetalle.IdEstructuraCostoGrupo &&
                                                                               x.IdEstado == estructuraCostoGrupoDetalle.IdEstado);

            return objServicio.Select(x => new EstructuraCostoGrupoDetalleDtoResponse
            {   

                IdEstructuraCostoGrupo = x.IdEstructuraCostoGrupo,
                IdConcepto = x.IdConcepto,
                Asignacion = x.Asignacion,
                Cantidad = x.Cantidad,
                CapexDolares  = x.CapexDolares,
                CapexSoles = x.CapexSoles,
                CapexTotalSoles = x.CapexTotalSoles,
                Certificacion = x.Certificacion,
                CuNuevoDolar = x.CuNuevoDolar,
                Descripcion = x.Descripcion,
                IdMedioCosto = x.IdMedioCosto,
                IdServicio = x.IdServicio,
                IdTipoGrupo = x.IdTipoGrupo,
                IdUbigeo = x.IdUbigeo,
                Modelo = x.Modelo 

            }).ToList();
        }

        public EstructuraCostoGrupoDetalleDtoResponse ObtenerEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalleDtoRequest estructuraCostoGrupoDetalle)
        {
            var objServicio = iEstructuraCostoGrupoDetalleDal.GetFiltered(x => x.IdEstructuraCostoGrupoDetalle == estructuraCostoGrupoDetalle.IdEstructuraCostoGrupoDetalle &&
                                                                               x.IdEstado == estructuraCostoGrupoDetalle.IdEstado);

            return objServicio.Select(x => new EstructuraCostoGrupoDetalleDtoResponse
            {

                IdEstructuraCostoGrupo = x.IdEstructuraCostoGrupo,
                IdConcepto = x.IdConcepto,
                Asignacion = x.Asignacion,
                Cantidad = x.Cantidad,
                CapexDolares = x.CapexDolares,
                CapexSoles = x.CapexSoles,
                CapexTotalSoles = x.CapexTotalSoles,
                Certificacion = x.Certificacion,
                CuNuevoDolar = x.CuNuevoDolar,
                Descripcion = x.Descripcion,
                IdMedioCosto = x.IdMedioCosto,
                IdServicio = x.IdServicio,
                IdTipoGrupo = x.IdTipoGrupo,
                IdUbigeo = x.IdUbigeo,
                Modelo = x.Modelo

            }).Single();
        }

        public ProcesoResponse RegistrarEstructuraCostoGrupoDetalle(EstructuraCostoGrupoDetalleDtoRequest estructuraCostoGrupoDetalle)
        {
           

                var objEstructuraCostoGrupo = new EstructuraCostoGrupoDetalle()
                {
                    IdEstructuraCostoGrupo = estructuraCostoGrupoDetalle.IdEstructuraCostoGrupo,
                   // IdEstructuraCostoGrupoDetalle = estructuraCostoGrupoDetalle.IdEstructuraCostoGrupoDetalle,
                    Asignacion = estructuraCostoGrupoDetalle.Asignacion,
                    Modelo = estructuraCostoGrupoDetalle.Modelo,
                    Cantidad = estructuraCostoGrupoDetalle.Cantidad,
                    CapexDolares = estructuraCostoGrupoDetalle.CapexDolares,
                    CapexSoles = estructuraCostoGrupoDetalle.CapexSoles,
                    CapexTotalSoles = estructuraCostoGrupoDetalle.CapexTotalSoles,
                    Certificacion = estructuraCostoGrupoDetalle.Certificacion,
                    CuNuevoDolar = estructuraCostoGrupoDetalle.CuNuevoDolar,
                    IdConcepto = estructuraCostoGrupoDetalle.IdConcepto,
                    IdMedioCosto = estructuraCostoGrupoDetalle.IdMedioCosto,
                    IdServicio = estructuraCostoGrupoDetalle.IdServicio,
                    IdTipoGrupo = estructuraCostoGrupoDetalle.IdTipoGrupo,
                    IdUbigeo = estructuraCostoGrupoDetalle.IdUbigeo,
                    Descripcion = estructuraCostoGrupoDetalle.Descripcion,
                    IdEstado = estructuraCostoGrupoDetalle.IdEstado,
                    IdUsuarioCreacion = estructuraCostoGrupoDetalle.IdUsuarioCreacion,
                    FechaCreacion = DateTime.Now
                };

                iEstructuraCostoGrupoDetalleDal.Add(objEstructuraCostoGrupo);
                iEstructuraCostoGrupoDetalleDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.RegistrarEstructuraCostoGrupoDetalle;

           

            return respuesta;
        }
    }
}
