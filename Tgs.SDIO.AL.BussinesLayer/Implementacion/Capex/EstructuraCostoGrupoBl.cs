﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Mensajes.Capex;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class EstructuraCostoGrupoBl : IEstructuraCostoGrupoBl
    {
       
        readonly IEstructuraCostoGrupoDal iEstructuraCostoGrupoDal;
        readonly IEstructuraCostoGrupoDetalleBl iEstructuraCostoGrupoDetalleBl;
        ProcesoResponse respuesta = new ProcesoResponse();

        public EstructuraCostoGrupoBl(IEstructuraCostoGrupoDal IEstructuraCostoGrupoDal,
            IEstructuraCostoGrupoDetalleBl IEstructuraCostoGrupoDetalleBl)
        {
            iEstructuraCostoGrupoDal = IEstructuraCostoGrupoDal;
            iEstructuraCostoGrupoDetalleBl = IEstructuraCostoGrupoDetalleBl;

        }
        public ProcesoResponse ActualizarEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest estructuraCostoGrupo)
        {
            DateTime fecha = DateTime.Now;
            var objEstructuraCostoGrupo = new EstructuraCostoGrupo()
                {
                    IdEstructuraCostoGrupo = estructuraCostoGrupo.IdEstructuraCostoGrupo,
                    IdCapexLineaNegocio = estructuraCostoGrupo.IdCapexLineaNegocio,
                    IdGrupo = estructuraCostoGrupo.IdGrupo,
                   // Descripcion = estructuraCostoGrupo.Descripcion,
                    IdEstado = estructuraCostoGrupo.IdEstado,
                    IdUsuarioEdicion = estructuraCostoGrupo.IdUsuarioEdicion,
                    FechaEdicion = fecha
            };

                iEstructuraCostoGrupoDal.ActualizarPorCampos(objEstructuraCostoGrupo, x => x.IdEstructuraCostoGrupo, x => x.IdCapexLineaNegocio, x => x.IdGrupo, 
                //    x => x.Descripcion, 
                    x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
                iEstructuraCostoGrupoDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.ActualizarEstructuraCosto;

            var objEstructuraCostoGrupoDetalle = new EstructuraCostoGrupoDetalleDtoRequest()
            {
                IdEstructuraCostoGrupo = estructuraCostoGrupo.IdEstructuraCostoGrupo,
                IdServicio = estructuraCostoGrupo.IdServicio,
                IdConcepto = estructuraCostoGrupo.IdConcepto,
                Modelo = estructuraCostoGrupo.Modelo,
                CapexSoles = estructuraCostoGrupo.CapexSoles,
                CapexTotalSoles = estructuraCostoGrupo.CapexTotalSoles,
                CapexDolares = estructuraCostoGrupo.CapexDolares,
                Cantidad = estructuraCostoGrupo.Cantidad,
                Certificacion = estructuraCostoGrupo.Certificacion,
                Descripcion = estructuraCostoGrupo.Descripcion,
                IdEstado = Estados.Activo,
                IdUsuarioCreacion = estructuraCostoGrupo.IdUsuarioCreacion,
                FechaCreacion = fecha,
                IdUbigeo = estructuraCostoGrupo.IdUbigeo,
                Asignacion = estructuraCostoGrupo.Asignacion,
                CuNuevoDolar = estructuraCostoGrupo.CuNuevoDolar,
                IdTipoGrupo = estructuraCostoGrupo.IdTipoGrupo,
                IdMedioCosto = estructuraCostoGrupo.IdMedioCosto


            };

            var result = iEstructuraCostoGrupoDetalleBl.ActualizarEstructuraCostoGrupoDetalle(objEstructuraCostoGrupoDetalle);

            return respuesta;
        }

       

        public List<EstructuraCostoGrupoDtoResponse> ListarEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest estructuraCostoGrupo)
        {
            var objServicio = iEstructuraCostoGrupoDal.GetFiltered(x => x.IdCapexLineaNegocio == estructuraCostoGrupo.IdCapexLineaNegocio &&
                                                                        x.IdGrupo == estructuraCostoGrupo.IdGrupo &&
                                                                        x.IdEstado == estructuraCostoGrupo.IdEstado);
                                
            return objServicio.Select(x => new EstructuraCostoGrupoDtoResponse
            {
                IdEstructuraCostoGrupo = x.IdEstructuraCostoGrupo,
                IdCapexLineaNegocio = x.IdCapexLineaNegocio,
                Descripcion = x.Descripcion,
                IdGrupo = x.IdGrupo
            }).ToList();
        }


        public EstructuraCostoGrupoDetalleDtoResponse ObtenerEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest estructuraCostoGrupo)
        {
            return iEstructuraCostoGrupoDal.ObtenerEstructuraCostoGrupo(estructuraCostoGrupo);


        }
        public EstructuraCostoGrupoPaginadoDtoResponse ListaEstructuraCostoGrupoPaginado(EstructuraCostoGrupoDtoRequest estructuraCostoGrupo)
        {
            return iEstructuraCostoGrupoDal.ListaEstructuraCostoGrupoPaginado(estructuraCostoGrupo);

        }


        public ProcesoResponse RegistrarEstructuraCostoGrupo(EstructuraCostoGrupoDtoRequest estructuraCostoGrupo)
        {
            DateTime fecha = DateTime.Now;
            var objEstructuraCostoGrupo = new EstructuraCostoGrupo()
                {
                    IdCapexLineaNegocio = estructuraCostoGrupo.IdCapexLineaNegocio,
                    IdGrupo = estructuraCostoGrupo.IdGrupo,
                    Descripcion = estructuraCostoGrupo.Descripcion,
                    IdEstado = estructuraCostoGrupo.IdEstado,
                    IdUsuarioCreacion = estructuraCostoGrupo.IdUsuarioEdicion,
                    FechaCreacion = DateTime.Now
                };

                iEstructuraCostoGrupoDal.Add(objEstructuraCostoGrupo);
                iEstructuraCostoGrupoDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.RegistrarEstructuraCosto;

     
            iEstructuraCostoGrupoDal.Add(objEstructuraCostoGrupo);
            iEstructuraCostoGrupoDal.UnitOfWork.Commit();

            var dato = objEstructuraCostoGrupo.IdEstructuraCostoGrupo;

            var objEstructuraCostoGrupoDetalle = new EstructuraCostoGrupoDetalleDtoRequest()
            { 
                IdEstructuraCostoGrupo = dato,
                IdServicio = estructuraCostoGrupo.IdServicio,
                IdConcepto = estructuraCostoGrupo.IdConcepto,
                Modelo = estructuraCostoGrupo.Modelo,
                CapexSoles = estructuraCostoGrupo.CapexSoles,
                CapexTotalSoles = estructuraCostoGrupo.CapexTotalSoles,
                CapexDolares = estructuraCostoGrupo.CapexDolares,
                Cantidad = estructuraCostoGrupo.Cantidad,
                Certificacion = estructuraCostoGrupo.Certificacion,
                Descripcion = estructuraCostoGrupo.Descripcion,
                IdEstado = Estados.Activo,
                IdUsuarioCreacion = estructuraCostoGrupo.IdUsuarioCreacion,
                FechaCreacion = fecha,
                IdUbigeo = estructuraCostoGrupo.IdUbigeo,
                Asignacion=estructuraCostoGrupo.Asignacion,
                CuNuevoDolar= estructuraCostoGrupo.CuNuevoDolar,
                IdTipoGrupo= estructuraCostoGrupo.IdTipoGrupo,
                IdMedioCosto= estructuraCostoGrupo.IdMedioCosto


            };

            var result=   iEstructuraCostoGrupoDetalleBl.RegistrarEstructuraCostoGrupoDetalle(objEstructuraCostoGrupoDetalle);
            //


            return respuesta;
        }
    }
}
