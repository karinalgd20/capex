﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Proyecto;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Mensajes.Capex;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class SolicitudCapexBl : ISolicitudCapexBl
    {
        readonly ISolicitudCapexDal iSolicitudCapexDal;
        readonly IDetalleSolucionDal iDetalleSolucionDal;
        readonly ICapexLineaNegocioBl iCapexLineaNegocioBl;
        readonly ISolicitudCapexFlujoEstadoBl iSolicitudCapexFlujoEstadoBl;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SolicitudCapexBl(ISolicitudCapexDal ISolicitudCapexDal,
                                IDetalleSolucionDal IDetalleSolucionDal,
                                ICapexLineaNegocioBl ICapexLineaNegocioBl,
                                ISolicitudCapexFlujoEstadoBl ISolicitudCapexFlujoEstadoBl)
        {
            iSolicitudCapexDal = ISolicitudCapexDal;
            iDetalleSolucionDal = IDetalleSolucionDal;
            iCapexLineaNegocioBl = ICapexLineaNegocioBl;
            iSolicitudCapexFlujoEstadoBl = ISolicitudCapexFlujoEstadoBl;
        }
        public ProcesoResponse ActualizarSolicitudCapex(SolicitudCapexDtoRequest solicitudCapex)
        {
            try
            {
                var objSolicitudCapex = new SolicitudCapex()
                {
                    Descripcion = solicitudCapex.Descripcion,
                    IdCliente = solicitudCapex.IdCliente,
                    IdSolicitudCapex = solicitudCapex.IdSolicitudCapex,
                    IdTipoEntidad = solicitudCapex.IdTipoEntidad,
                    IdTipoProyecto = solicitudCapex.IdTipoProyecto,
                    NumeroSalesForce = solicitudCapex.NumeroSalesForce,
                    IdEstado = solicitudCapex.IdEstado,
                    IdUsuarioEdicion = solicitudCapex.IdUsuarioEdicion,
                    FechaEdicion = DateTime.Now,
                    CodigoPmo = solicitudCapex.CodigoPmo
                };

                iSolicitudCapexDal.ActualizarPorCampos(objSolicitudCapex,
                x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
                iSolicitudCapexDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.ActualizarSolicitudCapex;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }

        public List<SolicitudCapexDtoResponse> ListarBandejaSolicitudCapex(SolicitudCapexDtoRequest SolicitudCapex)
        {
            throw new NotImplementedException();
        }
        public SolicitudCapexPaginadoDtoResponse ListaSolicitudCapexPaginado(SolicitudCapexDtoRequest SolicitudCapex)

        {
            return iSolicitudCapexDal.ListaSolicitudCapexPaginado(SolicitudCapex);


        }

        public List<SolicitudCapexDtoResponse> ListarSolicitudCapex(SolicitudCapexDtoRequest solicitudCapex)
        {
            var objServicio = iSolicitudCapexDal.GetFiltered(x => x.IdCliente == solicitudCapex.IdCliente &&
                                                                  x.IdTipoEntidad == solicitudCapex.IdTipoEntidad &&
                                                                  x.IdTipoProyecto == solicitudCapex.IdTipoProyecto &&
                                                                  x.IdEstado == solicitudCapex.IdEstado);
            return objServicio.Select(x => new SolicitudCapexDtoResponse
            {
                IdCliente = x.IdCliente,
                IdTipoEntidad = x.IdTipoEntidad,
                IdTipoProyecto = x.IdTipoProyecto,
                NumeroSalesForce = x.NumeroSalesForce,
                Descripcion = x.Descripcion,
                CodigoPmo = x.CodigoPmo
            }).ToList();
        }

        public SolicitudCapexDtoResponse ObtenerSolicitudCapex(SolicitudCapexDtoRequest solicitudCapex)
        {
            return iSolicitudCapexDal.ObtenerSolicitudCapex(solicitudCapex);
        }

        public ProcesoResponse RegistrarSolicitudCapex(SolicitudCapexDtoRequest solicitudCapex)
        {
            try
            {
                var objSolicitudCapex = new SolicitudCapex()
                {
                    Descripcion = solicitudCapex.Descripcion,
                    IdCliente = solicitudCapex.IdCliente,
                    IdSolicitudCapex = solicitudCapex.IdSolicitudCapex,
                    IdTipoEntidad = solicitudCapex.IdTipoEntidad,
                    IdTipoProyecto = solicitudCapex.IdTipoProyecto,
                    NumeroSalesForce = solicitudCapex.NumeroSalesForce,
                    IdEstado = solicitudCapex.IdEstado,
                    IdUsuarioCreacion = solicitudCapex.IdUsuarioCreacion,
                    IdPreVenta = solicitudCapex.IdUsuarioCreacion,
                    FechaCreacion = DateTime.Now,
                    CodigoPmo = solicitudCapex.CodigoPmo
                };

                iSolicitudCapexDal.Add(objSolicitudCapex);
                iSolicitudCapexDal.UnitOfWork.Commit();

                var detalle = new DetalleSolucionDtoRequest()
                {
                    IdProyecto = solicitudCapex.IdProyecto
                };

                var lineas = iDetalleSolucionDal.ObtenerDetallePorIdProyecto(detalle);

                foreach (var item in lineas)
                {
                    var capexLinea = new CapexLineaNegocioDtoRequest()
                    {
                        IdSolicitudCapex = objSolicitudCapex.IdSolicitudCapex,
                        IdLineaNegocio = item.IdTipoSolucion,
                        Fc = solicitudCapex.Fc,
                        Van = solicitudCapex.Van,
                        PayBack = solicitudCapex.PayBack,
                        IdEstado = Estados.Activo
                    };

                    iCapexLineaNegocioBl.RegistrarCapexLineaNegocio(capexLinea);
                }

                var estado = new SolicitudCapexFlujoEstadoDtoRequest()
                {
                    IdSolicitudCapex = objSolicitudCapex.IdSolicitudCapex,
                    IdEstado = Estados.Preventa,
                    IdUsuarioCreacion = objSolicitudCapex.IdUsuarioCreacion
                };

                iSolicitudCapexFlujoEstadoBl.RegistrarSolicitudCapexFlujoEstado(estado);
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.RegistrarSolicitudCapex;
                respuesta.Id = objSolicitudCapex.IdSolicitudCapex;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }

        public ProcesoResponse ActualizarSolicitudCapexEstado(SolicitudCapexDtoRequest solicitudCapex)
        {
            try
            {
                var objSolicitudCapex = new SolicitudCapex()
                {
                    IdSolicitudCapex = solicitudCapex.IdSolicitudCapex,
                    IdEstado = solicitudCapex.IdEstado,
                    IdUsuarioEdicion = solicitudCapex.IdUsuarioEdicion,
                    FechaEdicion = DateTime.Now
                };

                iSolicitudCapexDal.ActualizarPorCampos(objSolicitudCapex,
                x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
                iSolicitudCapexDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.ActualizarSolicitudCapex;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }
    }
}
