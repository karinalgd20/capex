﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Mensajes.Capex;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class SolicitudCapexFlujoEstadoBl : ISolicitudCapexFlujoEstadoBl
    {
        readonly ISolicitudCapexFlujoEstadoDal iSolicitudCapexFlujoEstadoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SolicitudCapexFlujoEstadoBl(ISolicitudCapexFlujoEstadoDal ISolicitudCapexFlujoEstadoDal)
        {
            iSolicitudCapexFlujoEstadoDal = ISolicitudCapexFlujoEstadoDal;
        }

        public ProcesoResponse ActualizarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest solicitudCapexFlujoEstado)
        {
            try
            {
                var objSolicitudCapexFlujoEstado = new SolicitudCapexFlujoEstado()
                {
                    IdSolicitudCapex = solicitudCapexFlujoEstado.IdSolicitudCapex,
                    IdSolicitudCapexFlujoEstado = solicitudCapexFlujoEstado.IdSolicitudCapexFlujoEstado,
                    IdEstado = solicitudCapexFlujoEstado.IdEstado
                };

                iSolicitudCapexFlujoEstadoDal.ActualizarPorCampos(objSolicitudCapexFlujoEstado, x => x.IdSolicitudCapex,
                x => x.IdSolicitudCapexFlujoEstado, x => x.IdEstado);
                iSolicitudCapexFlujoEstadoDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.ActualizarSolicitudCapexFlujoEstado;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }

        public List<SolicitudCapexFlujoEstadoDtoResponse> ListarBandejaSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest SolicitudCapexFlujoEstado)
        {
            throw new NotImplementedException();
        }

        public List<SolicitudCapexFlujoEstadoDtoResponse> ListarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest solicitudCapexFlujoEstado)
        {
            var objSolicitudCapexFlujoEstado = iSolicitudCapexFlujoEstadoDal.GetFiltered(x => x.IdSolicitudCapex == solicitudCapexFlujoEstado.IdSolicitudCapex);
            return objSolicitudCapexFlujoEstado.Select(x => new SolicitudCapexFlujoEstadoDtoResponse
            {
                IdSolicitudCapexFlujoEstado = x.IdSolicitudCapexFlujoEstado,
                IdEstado = x.IdEstado
            }).ToList();
        }

        public SolicitudCapexFlujoEstadoDtoResponse ObtenerSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest solicitudCapexFlujoEstado)
        {
            var objSolicitudCapexFlujoEstado = iSolicitudCapexFlujoEstadoDal.GetFiltered(x => x.IdSolicitudCapex == solicitudCapexFlujoEstado.IdSolicitudCapex &&
                                                                                                x.IdEstado == solicitudCapexFlujoEstado.IdEstado);
            return objSolicitudCapexFlujoEstado.Select(x => new SolicitudCapexFlujoEstadoDtoResponse
            {
                IdSolicitudCapexFlujoEstado = x.IdSolicitudCapexFlujoEstado,
                IdSolicitudCapex = x.IdSolicitudCapex,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion
            }).Single();
        }

        public ProcesoResponse RegistrarSolicitudCapexFlujoEstado(SolicitudCapexFlujoEstadoDtoRequest solicitudCapexFlujoEstado)
        {
            try
            {
                var objSolicitudCapexFlujoEstado = new SolicitudCapexFlujoEstado()
                {
                    IdSolicitudCapex = solicitudCapexFlujoEstado.IdSolicitudCapex,
                    IdSolicitudCapexFlujoEstado = solicitudCapexFlujoEstado.IdSolicitudCapexFlujoEstado,
                    IdEstado = solicitudCapexFlujoEstado.IdEstado,
                    IdUsuarioCreacion = solicitudCapexFlujoEstado.IdUsuarioCreacion,
                    FechaCreacion = DateTime.Now
                };

                iSolicitudCapexFlujoEstadoDal.Add(objSolicitudCapexFlujoEstado);
                iSolicitudCapexFlujoEstadoDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.RegistrarSolicitudCapexFlujoEstado;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }

        public SolicitudCapexFlujoEstadoDtoResponse ObtenerSolicitudCapexFlujoEstadoId(SolicitudCapexFlujoEstadoDtoRequest solicitudCapexFlujoEstado)
        {
            SolicitudCapexFlujoEstadoDtoResponse SolicitudCapexFlujoEstadoResponse = new SolicitudCapexFlujoEstadoDtoResponse();

            var objSolicitudCapexFlujoEstado = iSolicitudCapexFlujoEstadoDal.GetFiltered(p => p.IdSolicitudCapex == solicitudCapexFlujoEstado.IdSolicitudCapex).OrderByDescending(t => t.IdSolicitudCapexFlujoEstado).ToList().First();

            SolicitudCapexFlujoEstadoResponse.IdEstado = objSolicitudCapexFlujoEstado.IdEstado;



            return SolicitudCapexFlujoEstadoResponse;

        }
    }
}
