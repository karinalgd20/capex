﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Mensajes.Capex;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class TopologiaBl : ITopologiaBl
    {
        readonly ITopologiaDal iTopologiaDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public TopologiaBl(ITopologiaDal ITopologiaDal)
        {
            iTopologiaDal = ITopologiaDal;
        }

        public ProcesoResponse ActualizarTopologia(TopologiaDtoRequest topologia)
        {
            try
            {
                var objTopologia = new Topologia()
                {
                    IdEstructuraCosto = topologia.IdEstructuraCosto,
                    IdMedioCosto = topologia.IdMedioCosto,
                    IdTopologia = topologia.IdTopologia,
                    Observaciones = topologia.Observaciones,
                    IdEstado = topologia.IdEstado,
                    IdUsuarioEdicion = topologia.IdUsuarioEdicion,
                    FechaEdicion = DateTime.Now
                };

                iTopologiaDal.ActualizarPorCampos(objTopologia, x => x.IdEstructuraCosto,
                x => x.IdMedioCosto, x => x.IdTopologia, x => x.Observaciones,
                x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
                iTopologiaDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.ActualizarTopologia;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }

        public List<TopologiaDtoResponse> ListarBandejaTopologia(TopologiaDtoRequest Topologia)
        {
            throw new NotImplementedException();
        }

        public List<TopologiaDtoResponse> ListarTopologia(TopologiaDtoRequest topologia)
        {
            var objTopologia = iTopologiaDal.GetFiltered(x => x.IdEstructuraCosto == topologia.IdEstructuraCosto &
                                                                   x.IdEstado == topologia.IdEstado);
            return objTopologia.Select(x => new TopologiaDtoResponse
            {
                IdTopologia = x.IdTopologia,
                IdMedioCosto = x.IdMedioCosto,
                Observaciones = x.Observaciones
            }).ToList();
        }

        public TopologiaDtoResponse ObtenerTopologia(TopologiaDtoRequest topologia)
        {
            var objTopologia = iTopologiaDal.GetFiltered(x => x.IdTopologia == topologia.IdTopologia &
                                                                  x.IdEstado == topologia.IdEstado);
            return objTopologia.Select(x => new TopologiaDtoResponse
            {
                IdEstructuraCosto = x.IdEstructuraCosto,
                IdMedioCosto = x.IdMedioCosto,
                Observaciones = x.Observaciones
            }).Single();
        }

        public ProcesoResponse RegistrarTopologia(TopologiaDtoRequest topologia)
        {

            try
            {
                var objTopologia = new Topologia()
                {
                    IdEstructuraCosto = topologia.IdEstructuraCosto,
                    IdMedioCosto = topologia.IdMedioCosto,
                    IdTopologia = topologia.IdTopologia,
                    Observaciones = topologia.Observaciones,
                    IdEstado = topologia.IdEstado,
                    IdUsuarioCreacion = topologia.IdUsuarioCreacion,
                    FechaCreacion = DateTime.Now
                };

                iTopologiaDal.Add(objTopologia);
                iTopologiaDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.RegistrarTopologia;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }
    }
}
