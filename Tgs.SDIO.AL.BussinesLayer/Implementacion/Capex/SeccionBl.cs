﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Mensajes.Capex;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class SeccionBl : ISeccionBl
    {
        readonly ISeccionDal iSeccionDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SeccionBl(ISeccionDal ISeccionDal)
        {
            iSeccionDal = ISeccionDal;
        }

        public ProcesoResponse ActualizarSeccion(SeccionDtoRequest seccion)
        {
            try
            {
                var objSeccion = new Seccion()
                {
                    IdSeccion = seccion.IdSeccion,
                    Descripcion = seccion.Descripcion,
                    IdObservacion = seccion.IdObservacion,
                    IdSolicitudCapex = seccion.IdSolicitudCapex,
                    IdTipoDescripcion = seccion.IdTipoDescripcion,
                    IdEstado = seccion.IdEstado,
                    IdUsuarioEdicion = seccion.IdUsuarioEdicion,
                    FechaEdicion = DateTime.Now
                };

                iSeccionDal.ActualizarPorCampos(objSeccion, x => x.Descripcion,
                x => x.IdObservacion, x => x.IdSolicitudCapex, x => x.IdTipoDescripcion,
                x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
                iSeccionDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.ActualizarSeccion;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }

        public ProcesoResponse ActualizarSeccionObservacion(SeccionDtoRequest seccion)
        {
            try
            {
                var objSeccion = new Seccion()
                {
                    IdSeccion = seccion.IdSeccion,
                    IdObservacion = seccion.IdObservacion,
                    IdSolicitudCapex = seccion.IdSolicitudCapex
                };

                iSeccionDal.ActualizarPorCampos(objSeccion, x => x.IdObservacion);
                iSeccionDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.ActualizarSeccion;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }

        public List<SeccionDtoResponse> ListarBandejaSeccion(SeccionDtoRequest Seccion)
        {
            throw new NotImplementedException();
        }

        public List<SeccionDtoResponse> ListarSeccion(SeccionDtoRequest seccion)
        {
            var objSeccion = iSeccionDal.GetFiltered(x => x.IdEstado == seccion.IdEstado);
            return objSeccion.Select(x => new SeccionDtoResponse
            {
                IdSeccion = x.IdSeccion,
                IdObservacion = x.IdObservacion,
                IdTipoDescripcion = x.IdTipoDescripcion,
                IdSolicitudCapex = x.IdSolicitudCapex,
                Descripcion = x.Descripcion
            }).ToList();
        }

        public SeccionDtoResponse ObtenerSeccion(SeccionDtoRequest seccion)
        {
            var objSeccion = iSeccionDal.GetFiltered(x => x.IdSolicitudCapex == seccion.IdSolicitudCapex &&
                                                          x.IdTipoDescripcion == seccion.IdTipoDescripcion &&
                                                          x.IdEstado == seccion.IdEstado);
            var valida = objSeccion.Any();
            if (valida)
            {
                return objSeccion.Select(x => new SeccionDtoResponse
                {
                    IdSeccion = x.IdSeccion,
                    IdObservacion = x.IdObservacion,
                    IdTipoDescripcion = x.IdTipoDescripcion,
                    IdSolicitudCapex = x.IdSolicitudCapex,
                    Descripcion = x.Descripcion
                }).Single();
            }
            else
            {
                return new SeccionDtoResponse()
                {
                    IdTipoDescripcion = seccion.IdTipoDescripcion,
                    Descripcion = string.Empty
                };
            }

        }

        public ProcesoResponse RegistrarSeccion(SeccionDtoRequest seccion)
        {
            try
            {
                var objSeccion = new Seccion()
                {
                    IdSeccion = seccion.IdSeccion,
                    Descripcion = seccion.Descripcion,
                    IdObservacion = seccion.IdObservacion,
                    IdSolicitudCapex = seccion.IdSolicitudCapex,
                    IdTipoDescripcion = seccion.IdTipoDescripcion,
                    IdEstado = seccion.IdEstado,
                    IdUsuarioCreacion = seccion.IdUsuarioCreacion,
                    FechaCreacion = DateTime.Now
                };

                iSeccionDal.Add(objSeccion);
                iSeccionDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.ActualizarSeccion;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }
    }
}
