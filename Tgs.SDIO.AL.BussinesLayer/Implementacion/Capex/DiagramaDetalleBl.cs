﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class DiagramaDetalleBl : IDiagramaDetalleBl
    {
        readonly IDiagramaDetalleDal iDiagramaDetalleDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public DiagramaDetalleBl(IDiagramaDetalleDal IDiagramaDetalleDal)
        {
            iDiagramaDetalleDal = IDiagramaDetalleDal;

        }

        public ProcesoResponse ActualizarDiagramaDetalle(DiagramaDetalleDtoRequest diagramaDetalle)
        {

            DateTime fecha = DateTime.Now;

            var objDiagramaDetalle = new DiagramaDetalle()
            {
                IdDiagramaDetalle = diagramaDetalle.IdDiagramaDetalle,
                 IdDiagrama = diagramaDetalle.IdDiagrama,
                  IdConcepto = diagramaDetalle.IdConcepto,
                   Orden = diagramaDetalle.Orden,
                IdEstado = diagramaDetalle.IdEstado,
                IdUsuarioEdicion = diagramaDetalle.IdUsuarioEdicion,
                FechaEdicion = fecha

            };


            iDiagramaDetalleDal.ActualizarPorCampos(objDiagramaDetalle, x => x.IdDiagramaDetalle, x => x.IdDiagrama, x => x.IdConcepto
            , x => x.Orden, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iDiagramaDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }

        public ProcesoResponse InhabilitarDiagramaDetalle(DiagramaDetalleDtoRequest diagramaDetalle)
        {

            DateTime fecha = DateTime.Now;

            var objDiagramaDetalle = new DiagramaDetalle()
            {
                IdDiagramaDetalle = diagramaDetalle.IdDiagramaDetalle,
                IdEstado = diagramaDetalle.IdEstado,
                IdUsuarioEdicion = diagramaDetalle.IdUsuarioEdicion,
                FechaEdicion = fecha
            };


            iDiagramaDetalleDal.ActualizarPorCampos(objDiagramaDetalle, x => x.IdDiagramaDetalle, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iDiagramaDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }


        public List<ListaDtoResponse> ListarDiagramaDetalle(DiagramaDetalleDtoRequest diagramaDetalle)
        {
            var query = iDiagramaDetalleDal.GetFilteredAsNoTracking(x => x.IdEstado == diagramaDetalle.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdDiagramaDetalle.ToString(),
                Descripcion = ""
            }).OrderBy(x => x.Descripcion).ToList();
        }

        public DiagramaDetalleDtoResponse ObtenerDiagramaDetalle(DiagramaDetalleDtoRequest diagramaDetalle)
        {
            var objDiagramaDetalle = iDiagramaDetalleDal.GetFiltered(x => x.IdDiagramaDetalle == diagramaDetalle.IdDiagramaDetalle);
            return objDiagramaDetalle.Select(x => new DiagramaDetalleDtoResponse
            {
                IdDiagramaDetalle = x.IdDiagramaDetalle,
                IdDiagrama = x.IdDiagrama,
                IdConcepto = x.IdConcepto,
                Orden = x.Orden,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion= x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion


            }).Single();

        }

        public ProcesoResponse RegistrarDiagramaDetalle(DiagramaDetalleDtoRequest diagramaDetalle)
        {
            DateTime fecha = DateTime.Now;
            var objDiagramaDetalle = new DiagramaDetalle()
            {
                IdDiagramaDetalle = diagramaDetalle.IdDiagramaDetalle,
                IdDiagrama = diagramaDetalle.IdDiagrama,
                IdConcepto = diagramaDetalle.IdConcepto,
                Orden = diagramaDetalle.Orden,
                IdEstado = diagramaDetalle.IdEstado,
                IdUsuarioCreacion = diagramaDetalle.IdUsuarioCreacion,
                FechaCreacion = fecha
            };

            iDiagramaDetalleDal.Add(objDiagramaDetalle);
            iDiagramaDetalleDal.UnitOfWork.Commit();


            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Id = objDiagramaDetalle.IdDiagramaDetalle;


            return respuesta;
        }
    }
}
