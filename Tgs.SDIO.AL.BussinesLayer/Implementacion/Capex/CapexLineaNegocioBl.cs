﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class CapexLineaNegocioBl : ICapexLineaNegocioBl
    {
        readonly ICapexLineaNegocioDal iCapexLineaNegocioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public CapexLineaNegocioBl(ICapexLineaNegocioDal ICapexLineaNegocioDal)
        {
            iCapexLineaNegocioDal = ICapexLineaNegocioDal;

        }

        public ProcesoResponse ActualizarCapexLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocio)
        {
            var objCapexLineaNegocio = new CapexLineaNegocio()
            {
                IdCapexLineaNegocio = capexLineaNegocio.IdCapexLineaNegocio,
                IdSolicitudCapex = capexLineaNegocio.IdSolicitudCapex,
                IdEstado = capexLineaNegocio.IdEstado,
                IdUsuarioEdicion = capexLineaNegocio.IdUsuarioEdicion,
                FechaEdicion = DateTime.Now,
                Fc = capexLineaNegocio.Fc,
                PayBack = capexLineaNegocio.PayBack,
                Van = capexLineaNegocio.Van

            };

            iCapexLineaNegocioDal.ActualizarPorCampos(objCapexLineaNegocio, x => x.IdCapexLineaNegocio, x => x.IdSolicitudCapex
            , x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion,
            x => x.Fc, x => x.Van, x => x.PayBack);

            iCapexLineaNegocioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }

        public ProcesoResponse InhabilitarCapexLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocio)
        {
            var objCapexLineaNegocio = new CapexLineaNegocio()
            {
                IdCapexLineaNegocio = capexLineaNegocio.IdCapexLineaNegocio,
                IdEstado = capexLineaNegocio.IdEstado,
                IdUsuarioEdicion = capexLineaNegocio.IdUsuarioEdicion,
                FechaEdicion = DateTime.Now
            };


            iCapexLineaNegocioDal.ActualizarPorCampos(objCapexLineaNegocio, x => x.IdCapexLineaNegocio, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iCapexLineaNegocioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }


        public List<ListaDtoResponse> ListarCapexLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocio)
        {
            return iCapexLineaNegocioDal.ListarCapexLineaNegocio(capexLineaNegocio);
        }

        public CapexLineaNegocioPaginadoDtoResponse ListarLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocio)
        {
            return iCapexLineaNegocioDal.ListarLineaNegocio(capexLineaNegocio);
        }

        public CapexLineaNegocioDtoResponse ObtenerCapexLineaNegocio(CapexLineaNegocioDtoRequest CapexLineaNegocio)
        {
            var objCapexLineaNegocio = iCapexLineaNegocioDal.GetFiltered(x => x.IdCapexLineaNegocio == CapexLineaNegocio.IdCapexLineaNegocio);
            return objCapexLineaNegocio.Select(x => new CapexLineaNegocioDtoResponse
            {
                IdCapexLineaNegocio = x.IdCapexLineaNegocio,
                IdSolicitudCapex = x.IdSolicitudCapex,
                IdLineaNegocio = x.IdLineaNegocio,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaEdicion = x.FechaCreacion,
                Fc = x.Fc,
                PayBack = x.PayBack,
                Van = x.Van

            }).Single();

        }
        public CapexLineaNegocioDtoResponse ObtenerCapexLineaNegocioPorIdSolicitud(CapexLineaNegocioDtoRequest CapexLineaNegocio)
        {
            var objCapexLineaNegocio = iCapexLineaNegocioDal.GetFiltered(x => x.IdSolicitudCapex == CapexLineaNegocio.IdSolicitudCapex);
            return objCapexLineaNegocio.Select(x => new CapexLineaNegocioDtoResponse
            {
                IdCapexLineaNegocio = x.IdCapexLineaNegocio,
                IdSolicitudCapex = x.IdSolicitudCapex,
                IdLineaNegocio = x.IdLineaNegocio,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaEdicion = x.FechaCreacion,
                Fc = x.Fc,
                PayBack = x.PayBack,
                Van = x.Van

            }).Single();

        }
        public ProcesoResponse RegistrarCapexLineaNegocio(CapexLineaNegocioDtoRequest capexLineaNegocio)
        {
            var objCapexLineaNegocio = new CapexLineaNegocio()
            {

                IdCapexLineaNegocio = capexLineaNegocio.IdCapexLineaNegocio,
                IdSolicitudCapex = capexLineaNegocio.IdSolicitudCapex,
                IdLineaNegocio = capexLineaNegocio.IdLineaNegocio,
                IdEstado = capexLineaNegocio.IdEstado,
                IdUsuarioCreacion = capexLineaNegocio.IdUsuarioCreacion,
                FechaCreacion = DateTime.Now,
                Fc = capexLineaNegocio.Fc,
                PayBack = capexLineaNegocio.PayBack,
                Van = capexLineaNegocio.Van
            };

            iCapexLineaNegocioDal.Add(objCapexLineaNegocio);
            iCapexLineaNegocioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Id = objCapexLineaNegocio.IdCapexLineaNegocio;

            return respuesta;
        }
    }
}
