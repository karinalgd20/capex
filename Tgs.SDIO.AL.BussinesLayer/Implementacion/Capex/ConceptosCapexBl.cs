﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class ConceptosCapexBl : IConceptosCapexBl
    {
        readonly IConceptosCapexDal iConceptosCapexDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ConceptosCapexBl(IConceptosCapexDal IConceptosCapexDal)
        {
            iConceptosCapexDal = IConceptosCapexDal;

        }

        public ProcesoResponse ActualizarConceptosCapex(ConceptosCapexDtoRequest conceptosCapex)
        {

            DateTime fecha = DateTime.Now;

            var objConceptosCapex = new ConceptosCapex()
            {
                IdConceptosCapex = conceptosCapex.IdConceptosCapex,
                IdGrupo = conceptosCapex.IdGrupo,
                Descripcion = conceptosCapex.Descripcion


            };


            iConceptosCapexDal.ActualizarPorCampos(objConceptosCapex, x => x.IdConceptosCapex, x => x.IdGrupo, x => x.Descripcion);

            iConceptosCapexDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }

        public ProcesoResponse InhabilitarConceptosCapex(ConceptosCapexDtoRequest conceptosCapex)
        {

            DateTime fecha = DateTime.Now;




            return respuesta;
        }


        public List<ListaDtoResponse> ListarConceptosCapex(ConceptosCapexDtoRequest conceptosCapex)
        {
            var query = iConceptosCapexDal.GetFilteredAsNoTracking(x => x.IdGrupo == conceptosCapex.IdGrupo).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdConceptosCapex.ToString(),
                Descripcion = x.Descripcion
            }).OrderBy(x => x.Descripcion).ToList();
        }

        public ConceptosCapexDtoResponse ObtenerConceptosCapex(ConceptosCapexDtoRequest conceptosCapex)
        {
            var objConceptosCapex = iConceptosCapexDal.GetFiltered(x => x.IdConceptosCapex == conceptosCapex.IdConceptosCapex);
            return objConceptosCapex.Select(x => new ConceptosCapexDtoResponse
            {
                IdConceptosCapex = x.IdConceptosCapex,
                IdGrupo = x.IdGrupo,
                Descripcion = x.Descripcion

            }).Single();

        }

        public ProcesoResponse RegistrarConceptosCapex(ConceptosCapexDtoRequest conceptosCapex)
        {
            DateTime fecha = DateTime.Now;
            var objConceptosCapex = new ConceptosCapex()
            {

                IdConceptosCapex = conceptosCapex.IdConceptosCapex,
                IdGrupo = conceptosCapex.IdGrupo,
                Descripcion = conceptosCapex.Descripcion
            };

            iConceptosCapexDal.Add(objConceptosCapex);
            iConceptosCapexDal.UnitOfWork.Commit();


            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Id = objConceptosCapex.IdConceptosCapex;


            return respuesta;
        }
    }
}
