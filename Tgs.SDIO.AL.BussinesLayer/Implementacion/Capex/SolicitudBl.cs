﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class SolicitudBl : ISolicitudBl
    {
        readonly ISolicitudDal iSolicitudDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SolicitudBl(ISolicitudDal ISolicitudDal)
        {
            iSolicitudDal = ISolicitudDal;

        }

        public ProcesoResponse ActualizarSolicitud(SolicitudDtoRequest Solicitud)
        {

            DateTime fecha = DateTime.Now;

            var objSolicitud = new Solicitud()
            {
                IdSolicitud = Solicitud.IdSolicitud,
                IdEstado = Solicitud.IdEstado,
                IdUsuarioEdicion = Solicitud.IdUsuarioEdicion,
                FechaEdicion = fecha

            };


            iSolicitudDal.ActualizarPorCampos(objSolicitud, x => x.IdSolicitud);

            iSolicitudDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }

        public ProcesoResponse InhabilitarSolicitud(SolicitudDtoRequest Solicitud)
        {

            DateTime fecha = DateTime.Now;

            var objSolicitud = new Solicitud()
            {
                IdSolicitud = Solicitud.IdSolicitud,
                IdEstado = Solicitud.IdEstado,
                IdUsuarioEdicion = Solicitud.IdUsuarioEdicion,
                FechaEdicion = fecha
            };


            iSolicitudDal.ActualizarPorCampos(objSolicitud, x => x.IdSolicitud, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iSolicitudDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }


        public List<ListaDtoResponse> ListarSolicitud(SolicitudDtoRequest Solicitud)
        {
            var query = iSolicitudDal.GetFilteredAsNoTracking(x => x.IdEstado == Solicitud.IdEstado
            && Solicitud.IdEstado == Solicitud.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdSolicitud.ToString(),
                Descripcion = ""
            }).OrderBy(x => x.Descripcion).ToList();
        }

        public SolicitudDtoResponse ObtenerSolicitud(SolicitudDtoRequest Solicitud)
        {
            var objSolicitud = iSolicitudDal.GetFiltered(x => x.IdSolicitud == Solicitud.IdSolicitud);
            return objSolicitud.Select(x => new SolicitudDtoResponse
            {
                IdSolicitud = x.IdSolicitud,
              
            }).Single();

        }

        public ProcesoResponse RegistrarSolicitud(SolicitudDtoRequest Solicitud)
        {
            DateTime fecha = DateTime.Now;
            var objSolicitud = new Solicitud()
            {

                IdUsuarioCreacion = Solicitud.IdUsuarioCreacion,
                FechaCreacion = fecha
            };

            iSolicitudDal.Add(objSolicitud);
            iSolicitudDal.UnitOfWork.Commit();


            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Id = objSolicitud.IdSolicitud;


            return respuesta;
        }
    }
}
