﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class CronogramaDetalleBl : ICronogramaDetalleBl
    {
        readonly ICronogramaDetalleDal iCronogramaDetalleDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public CronogramaDetalleBl(ICronogramaDetalleDal ICronogramaDetalleDal)
        {
            iCronogramaDetalleDal = ICronogramaDetalleDal;

        }

        public ProcesoResponse ActualizarCronogramaDetalle(CronogramaDetalleDtoRequest cronogramaDetalle)
        {

            DateTime fecha = DateTime.Now;

            var objCronogramaDetalle = new CronogramaDetalle()
            {

                IdCronogramaDetalle= cronogramaDetalle.IdCronogramaDetalle,
                IdCronograma= cronogramaDetalle.IdCronograma,
                Descripcion = cronogramaDetalle.Descripcion,
                Duracion = cronogramaDetalle.Duracion,
                Inicio = cronogramaDetalle.Inicio,
                Final = cronogramaDetalle.Final,
                IdEstado = cronogramaDetalle.IdEstado,
                IdUsuarioEdicion = cronogramaDetalle.IdUsuarioEdicion,
                FechaEdicion = fecha

            };


            iCronogramaDetalleDal.ActualizarPorCampos(objCronogramaDetalle, x => x.IdCronogramaDetalle, x => x.IdCronograma, x => x.Descripcion
            , x => x.Duracion, x => x.Inicio, x => x.Final, x => x.IdEstado, x => x.IdUsuarioEdicion
            , x => x.FechaEdicion);

            iCronogramaDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }

        public ProcesoResponse InhabilitarCronogramaDetalle(CronogramaDetalleDtoRequest cronogramaDetalle)
        {

            DateTime fecha = DateTime.Now;

            var objCronogramaDetalle = new CronogramaDetalle()
            {
                IdCronogramaDetalle = cronogramaDetalle.IdCronogramaDetalle,
                IdEstado = cronogramaDetalle.IdEstado,
                IdUsuarioEdicion = cronogramaDetalle.IdUsuarioEdicion,
                FechaEdicion = fecha
            };


            iCronogramaDetalleDal.ActualizarPorCampos(objCronogramaDetalle, x => x.IdCronogramaDetalle, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iCronogramaDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }


        public List<ListaDtoResponse> ListarCronogramaDetalle(CronogramaDetalleDtoRequest cronogramaDetalle)
        {
            var query = iCronogramaDetalleDal.GetFilteredAsNoTracking(x => x.IdEstado == cronogramaDetalle.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdCronogramaDetalle.ToString(),
                Descripcion = x.Descripcion
            }).OrderBy(x => x.Descripcion).ToList();
        }

        public CronogramaDetalleDtoResponse ObtenerCronogramaDetalle(CronogramaDetalleDtoRequest cronogramaDetalle)
        {
            var objCronogramaDetalle = iCronogramaDetalleDal.GetFiltered(x => x.IdCronogramaDetalle == cronogramaDetalle.IdCronogramaDetalle);
            return objCronogramaDetalle.Select(x => new CronogramaDetalleDtoResponse
            {
                IdCronogramaDetalle = x.IdCronogramaDetalle,
                IdCronograma = x.IdCronograma,
                Descripcion = x.Descripcion,
                Duracion = x.Duracion,
                Inicio = x.Inicio,
                Final = x.Final,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion

            }).Single();

        }

        public ProcesoResponse RegistrarCronogramaDetalle(CronogramaDetalleDtoRequest cronogramaDetalle)
        {
            DateTime fecha = DateTime.Now;
            var objCronogramaDetalle = new CronogramaDetalle()
            {
                IdCronogramaDetalle = cronogramaDetalle.IdCronogramaDetalle,
                IdCronograma = cronogramaDetalle.IdCronograma,
                Descripcion = cronogramaDetalle.Descripcion,
                Duracion = cronogramaDetalle.Duracion,
                Inicio = cronogramaDetalle.Inicio,
                Final = cronogramaDetalle.Final,
                IdEstado = cronogramaDetalle.IdEstado,
                IdUsuarioCreacion = cronogramaDetalle.IdUsuarioCreacion,
                FechaCreacion = fecha
            };

            iCronogramaDetalleDal.Add(objCronogramaDetalle);
            iCronogramaDetalleDal.UnitOfWork.Commit();


            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Id = objCronogramaDetalle.IdCronogramaDetalle;


            return respuesta;
        }
    }
}
