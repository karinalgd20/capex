﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using static Tgs.SDIO.DataContracts.Dto.Response.Capex.EstructuraCostoDtoResponse;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class EstructuraCostoBl : IEstructuraCostoBl
    {
        readonly IEstructuraCostoDal iEstructuraCostoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public EstructuraCostoBl(IEstructuraCostoDal IEstructuraCostoDal)
        {
            iEstructuraCostoDal = IEstructuraCostoDal;;

        }

        public ProcesoResponse ActualizarEstructuraCosto(EstructuraCostoDtoRequest estructuraCosto)
        {

            DateTime fecha = DateTime.Now;

            var objEstructuraCosto = new EstructuraCosto()
            {
                IdEstructuraCosto = estructuraCosto.IdEstructuraCosto,
                IdEstado = estructuraCosto.IdEstado,
                IdUsuarioEdicion = estructuraCosto.IdUsuarioEdicion,
                FechaEdicion = fecha
            };


            iEstructuraCostoDal.ActualizarPorCampos(objEstructuraCosto, x => x.IdEstructuraCosto, x => x.IdSolicitudCapex, x => x.IdEstado
            , x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iEstructuraCostoDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }

        public ProcesoResponse InhabilitarEstructuraCosto(EstructuraCostoDtoRequest EstructuraCosto)
        {

            DateTime fecha = DateTime.Now;

            var objEstructuraCosto = new EstructuraCosto()
            {
                IdEstructuraCosto = EstructuraCosto.IdEstructuraCosto,
                IdEstado = EstructuraCosto.IdEstado,
                IdUsuarioEdicion = EstructuraCosto.IdUsuarioEdicion,
                FechaEdicion = fecha
            };


            iEstructuraCostoDal.ActualizarPorCampos(objEstructuraCosto, x => x.IdEstructuraCosto, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iEstructuraCostoDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }


        public List<ListaDtoResponse> ListarEstructuraCosto(EstructuraCostoDtoRequest EstructuraCosto)
        {
            var query = iEstructuraCostoDal.GetFilteredAsNoTracking(x => x.IdEstado == EstructuraCosto.IdEstado
            && EstructuraCosto.IdEstado == EstructuraCosto.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdEstructuraCosto.ToString(),
                Descripcion = ""
            }).OrderBy(x => x.Descripcion).ToList();
        }

        public ProcesoResponse RegistrarEstructuraCosto(EstructuraCostoDtoRequest estructuraCosto)
        {
            DateTime fecha = DateTime.Now;
            var objEstructuraCosto = new EstructuraCosto()
            {
                IdSolicitudCapex = estructuraCosto.IdSolicitudCapex,
                IdEstado = Estados.Activo,
                IdUsuarioCreacion = estructuraCosto.IdUsuarioCreacion,
                FechaCreacion = fecha
            };

            iEstructuraCostoDal.Add(objEstructuraCosto);
            iEstructuraCostoDal.UnitOfWork.Commit();

          


        respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Id = objEstructuraCosto.IdEstructuraCosto;


            return respuesta;
        }
    }
}
