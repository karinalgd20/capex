﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class AsignacionCapexTotalDetalleBl : IAsignacionCapexTotalDetalleBl
    {
        readonly IAsignacionCapexTotalDetalleDal iAsignacionCapexTotalDetalleDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public AsignacionCapexTotalDetalleBl(IAsignacionCapexTotalDetalleDal IAsignacionCapexTotalDetalleDal)
        {
            iAsignacionCapexTotalDetalleDal = IAsignacionCapexTotalDetalleDal;

        }

        public ProcesoResponse ActualizarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest asignacionCapexTotalDetalle)
        {

            DateTime fecha = DateTime.Now;

            var objAsignacionCapexTotalDetalle = new AsignacionCapexTotalDetalle()
            {
                IdAsignacionCapexTotalDetalle = asignacionCapexTotalDetalle.IdAsignacionCapexTotalDetalle,
                IdAsignacionCapexTotal=asignacionCapexTotalDetalle.IdAsignacionCapexTotal,
                Monto =asignacionCapexTotalDetalle.Monto,
                IdMes =asignacionCapexTotalDetalle.IdMes,
                IdTipo =asignacionCapexTotalDetalle.IdTipo,
                IdEstado = asignacionCapexTotalDetalle.IdEstado,
                IdUsuarioEdicion = asignacionCapexTotalDetalle.IdUsuarioEdicion,
                FechaEdicion = fecha
    };


            iAsignacionCapexTotalDetalleDal.ActualizarPorCampos(objAsignacionCapexTotalDetalle, x => x.IdAsignacionCapexTotalDetalle, x => x.IdAsignacionCapexTotal
            , x => x.Monto, x => x.IdMes, x => x.IdTipo, x => x.IdEstado
            , x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iAsignacionCapexTotalDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }

        public ProcesoResponse InhabilitarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest asignacionCapexTotalDetalle)
        {

            DateTime fecha = DateTime.Now;

            var objAsignacionCapexTotalDetalle = new AsignacionCapexTotalDetalle()
            {
                IdAsignacionCapexTotalDetalle = asignacionCapexTotalDetalle.IdAsignacionCapexTotalDetalle,
                IdEstado = asignacionCapexTotalDetalle.IdEstado,
                IdUsuarioEdicion = asignacionCapexTotalDetalle.IdUsuarioEdicion,
                FechaEdicion = fecha
            };


            iAsignacionCapexTotalDetalleDal.ActualizarPorCampos(objAsignacionCapexTotalDetalle, x => x.IdAsignacionCapexTotalDetalle, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iAsignacionCapexTotalDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }


        public List<ListaDtoResponse> ListarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest asignacionCapexTotalDetalle)
        {
            var query = iAsignacionCapexTotalDetalleDal.GetFilteredAsNoTracking(x => x.IdEstado == asignacionCapexTotalDetalle.IdEstado
            && asignacionCapexTotalDetalle.IdEstado == asignacionCapexTotalDetalle.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdAsignacionCapexTotalDetalle.ToString(),
                Descripcion = ""
            }).OrderBy(x => x.Descripcion).ToList();
        }

        public AsignacionCapexTotalDetalleDtoResponse ObtenerAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest asignacionCapexTotalDetalle)
        {
            var objAsignacionCapexTotalDetalle = iAsignacionCapexTotalDetalleDal.GetFiltered(x => x.IdAsignacionCapexTotalDetalle == asignacionCapexTotalDetalle.IdAsignacionCapexTotalDetalle);
            return objAsignacionCapexTotalDetalle.Select(x => new AsignacionCapexTotalDetalleDtoResponse
            {
                IdAsignacionCapexTotalDetalle = x.IdAsignacionCapexTotalDetalle,
                IdAsignacionCapexTotal = x.IdAsignacionCapexTotal,
                Monto = x.Monto,
                IdMes = x.IdMes,
                IdTipo = x.IdTipo,
                IdEstado = x.IdEstado,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaCreacion = x.FechaCreacion

            }).Single();

        }

        public ProcesoResponse RegistrarAsignacionCapexTotalDetalle(AsignacionCapexTotalDetalleDtoRequest asignacionCapexTotalDetalle)
        {
            DateTime fecha = DateTime.Now;
            var objAsignacionCapexTotalDetalle = new AsignacionCapexTotalDetalle()
            {

                IdAsignacionCapexTotalDetalle = asignacionCapexTotalDetalle.IdAsignacionCapexTotalDetalle,
                IdAsignacionCapexTotal = asignacionCapexTotalDetalle.IdAsignacionCapexTotal,
                Monto = asignacionCapexTotalDetalle.Monto,
                IdMes = asignacionCapexTotalDetalle.IdMes,
                IdTipo = asignacionCapexTotalDetalle.IdTipo,
                IdEstado = asignacionCapexTotalDetalle.IdEstado,
                IdUsuarioEdicion = asignacionCapexTotalDetalle.IdUsuarioEdicion,
                FechaCreacion = fecha
            };

            iAsignacionCapexTotalDetalleDal.Add(objAsignacionCapexTotalDetalle);
            iAsignacionCapexTotalDetalleDal.UnitOfWork.Commit();


            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Id = objAsignacionCapexTotalDetalle.IdAsignacionCapexTotalDetalle;


            return respuesta;
        }
    }
}
