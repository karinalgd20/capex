﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Mensajes.Capex;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class ObservacionBl : IObservacionBl
    {
        readonly IObservacionDal iObservacionDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ObservacionBl(IObservacionDal IObservacionDal)
        {
            iObservacionDal = IObservacionDal;
        }

        public ProcesoResponse ActualizarObservacion(ObservacionDtoRequest observacion)
        {
            try
            {
                var objObservacion = new Observacion()
                {
                    Descripcion = observacion.Descripcion,
                    IdAsignado = observacion.IdAsignado,
                    IdResponsable = observacion.IdResponsable,
                    IdSeccion = observacion.IdSeccion,
                    Respuesta = observacion.Respuesta,
                    IdEstado = observacion.IdEstado,
                    IdUsuarioEdicion = observacion.IdUsuarioEdicion,
                    FechaEdicion = DateTime.Now
                };

                iObservacionDal.ActualizarPorCampos(objObservacion, x => x.Descripcion, x => x.IdAsignado, x => x.IdResponsable, 
                x => x.IdSeccion, x => x.Respuesta, x=> x.IdEstado, x=> x.IdUsuarioEdicion, x=> x.FechaEdicion);
                iObservacionDal.UnitOfWork.Commit();
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.ActualizarObservacion;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }

        public List<ObservacionDtoResponse> ListarBandejaObservacion(ObservacionDtoRequest Observacion)
        {
            throw new NotImplementedException();
        }

        public List<ObservacionDtoResponse> ListarObservacion(ObservacionDtoRequest observacion)
        {
            var objObservacion = iObservacionDal.GetFiltered(x => (x.IdAsignado == observacion.IdAsignado ||
                                                                   x.IdResponsable == observacion.IdResponsable) &&
                                                                   x.IdEstado == observacion.IdEstado);
            return objObservacion.Select(x => new ObservacionDtoResponse
            {
                IdObservacion = x.IdObservacion,
                IdAsignado = x.IdAsignado,
                IdResponsable = x.IdResponsable,
                IdSeccion = x.IdSeccion,
                Respuesta = x.Respuesta,
                Descripcion = x.Descripcion
            }).ToList();
        }

        public ObservacionDtoResponse ObtenerObservacion(ObservacionDtoRequest observacion)
        {
            var objObservacion = iObservacionDal.GetFiltered(x => x.IdObservacion == observacion.IdObservacion &
                                                                  x.IdEstado == observacion.IdEstado);
            var dto = objObservacion.Select(x => new ObservacionDtoResponse
            {
                IdAsignado = x.IdAsignado,
                IdResponsable = x.IdResponsable,
                IdSeccion = x.IdSeccion,
                Respuesta = x.Respuesta,
                Descripcion = x.Descripcion
            }).Single();

            return dto;
        }

        public ProcesoResponse RegistrarObservacion(ObservacionDtoRequest observacion)
        {
            try
            {
                var objObservacion = new Observacion()
                {
                    Descripcion = observacion.Descripcion,
                    IdAsignado = observacion.IdAsignado,
                    IdResponsable = observacion.IdResponsable,
                    IdSeccion = observacion.IdSeccion,
                    Respuesta = observacion.Respuesta,
                    IdEstado = observacion.IdEstado,
                    IdUsuarioCreacion = observacion.IdUsuarioCreacion,
                    FechaCreacion = DateTime.Now
                };

                iObservacionDal.Add(objObservacion);
                iObservacionDal.UnitOfWork.Commit();
                respuesta.Id = objObservacion.IdObservacion;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCapex.ActualizarObservacion;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = ex.Message;
                respuesta.TipoRespuesta = Proceso.Invalido;
            }
            return respuesta;
        }
    }
}
