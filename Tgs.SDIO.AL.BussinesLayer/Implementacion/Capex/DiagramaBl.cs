﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Constantes;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class DiagramaBl : IDiagramaBl
    {
        readonly IDiagramaDal iDiagramaDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public DiagramaBl(IDiagramaDal IDiagramaDal)
        {
            iDiagramaDal = IDiagramaDal;

        }

        public ProcesoResponse ActualizarDiagrama(DiagramaDtoRequest diagrama)
        {

            DateTime fecha = DateTime.Now;

            var objDiagrama = new Diagrama()
            {
                IdDiagrama = diagrama.IdDiagrama,
                IdUbigeo= diagrama.IdUbigeo,
                IdGrupo= diagrama.IdUbigeo,
                Orden= diagrama.Orden,
                IdEstado = diagrama.IdEstado,
                IdUsuarioEdicion = diagrama.IdUsuarioEdicion,
                FechaEdicion = fecha

            };


            iDiagramaDal.ActualizarPorCampos(objDiagrama, x => x.IdDiagrama);

            iDiagramaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }

        public ProcesoResponse InhabilitarDiagrama(DiagramaDtoRequest diagrama)
        {

            DateTime fecha = DateTime.Now;

            var objDiagrama = new Diagrama()
            {
                IdDiagrama = diagrama.IdDiagrama,
                IdEstado = diagrama.IdEstado,
                IdUsuarioEdicion = diagrama.IdUsuarioEdicion,
                FechaEdicion = fecha
            };


            iDiagramaDal.ActualizarPorCampos(objDiagrama, x => x.IdDiagrama, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iDiagramaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }


        public List<ListaDtoResponse> ListarDiagrama(DiagramaDtoRequest diagrama)
        {
            var query = iDiagramaDal.GetFilteredAsNoTracking(x => x.IdEstado == diagrama.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdDiagrama.ToString(),
                Descripcion = ""
            }).OrderBy(x => x.Descripcion).ToList();
        }

        public DiagramaDtoResponse ObtenerDiagrama(DiagramaDtoRequest diagrama)
        {
            var objDiagrama = iDiagramaDal.GetFiltered(x => x.IdDiagrama == diagrama.IdDiagrama);
            return objDiagrama.Select(x => new DiagramaDtoResponse
            {
                IdDiagrama = x.IdDiagrama,
                IdUbigeo = x.IdUbigeo,
                IdGrupo = x.IdGrupo,
                Orden = x.Orden,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion


            }).Single();

        }

        public ProcesoResponse RegistrarDiagrama(DiagramaDtoRequest diagrama)
        {
            DateTime fecha = DateTime.Now;
            var objDiagrama = new Diagrama()
            {

                IdDiagrama = diagrama.IdDiagrama,
                IdUbigeo = diagrama.IdUbigeo,
                IdGrupo = diagrama.IdUbigeo,
                Orden = diagrama.Orden,
                IdEstado = diagrama.IdEstado,
                IdUsuarioCreacion = diagrama.IdUsuarioCreacion,
                FechaCreacion = fecha
            };

            iDiagramaDal.Add(objDiagrama);
            iDiagramaDal.UnitOfWork.Commit();


            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Id = objDiagrama.IdDiagrama;


            return respuesta;
        }
    }
}
