﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Capex;
using Tgs.SDIO.AL.DataAccess.Interfaces.Capex;
using Tgs.SDIO.DataContracts.Dto.Request.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Capex;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Capex;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Capex
{
    public class CronogramaBl : ICronogramaBl
    {
        readonly ICronogramaDal iCronogramaDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public CronogramaBl(ICronogramaDal ICronogramaDal)
        {
            iCronogramaDal = ICronogramaDal;

        }

        public ProcesoResponse ActualizarCronograma(CronogramaDtoRequest cronograma)
        {
             
        DateTime fecha = DateTime.Now;

            var objCronograma = new Cronograma()
            {
                IdCronograma = cronograma.IdCronograma,
                IdEstructuraCosto=cronograma.IdEstructuraCosto,
                Descripcion= cronograma.Descripcion,
                IdEstado = cronograma.IdEstado,
                IdUsuarioEdicion = cronograma.IdUsuarioEdicion,
                FechaEdicion = fecha

            };


            iCronogramaDal.ActualizarPorCampos(objCronograma, x => x.IdCronograma, x => x.IdEstructuraCosto, x => x.Descripcion
            , x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iCronogramaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }

        public ProcesoResponse InhabilitarCronograma(CronogramaDtoRequest cronograma)
        {

            DateTime fecha = DateTime.Now;

            var objCronograma = new Cronograma()
            {
                IdCronograma = cronograma.IdCronograma,
                IdEstado = cronograma.IdEstado,
                IdUsuarioEdicion = cronograma.IdUsuarioEdicion,
                FechaEdicion = fecha
            };


            iCronogramaDal.ActualizarPorCampos(objCronograma, x => x.IdCronograma, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iCronogramaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;



            return respuesta;
        }


        public List<ListaDtoResponse> ListarCronograma(CronogramaDtoRequest cronograma)
        {
            var query = iCronogramaDal.GetFilteredAsNoTracking(x => x.IdEstado == cronograma.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdCronograma.ToString(),
                Descripcion = x.Descripcion
            }).OrderBy(x => x.Descripcion).ToList();
        }

        public CronogramaDtoResponse ObtenerCronograma(CronogramaDtoRequest cronograma)
        {
            var objCronograma = iCronogramaDal.GetFiltered(x => x.IdCronograma == cronograma.IdCronograma);
            return objCronograma.Select(x => new CronogramaDtoResponse
            {
                IdCronograma = x.IdCronograma,
                IdEstructuraCosto = x.IdEstructuraCosto,
                Descripcion = x.Descripcion,
                IdEstado = x.IdEstado,
                IdUsuarioEdicion = x.IdUsuarioCreacion,
                FechaEdicion = x.FechaCreacion

            }).Single();

        }

        public ProcesoResponse RegistrarCronograma(CronogramaDtoRequest cronograma)
        {
            DateTime fecha = DateTime.Now;
            var objCronograma = new Cronograma()
            {
                IdCronograma = cronograma.IdCronograma,
                IdEstructuraCosto = cronograma.IdEstructuraCosto,
                Descripcion = cronograma.Descripcion,
                IdEstado = cronograma.IdEstado,
                IdUsuarioCreacion = cronograma.IdUsuarioCreacion,
                FechaCreacion = fecha
            };

            iCronogramaDal.Add(objCronograma);
            iCronogramaDal.UnitOfWork.Commit();


            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Id = objCronograma.IdCronograma;


            return respuesta;
        }
    }
}
