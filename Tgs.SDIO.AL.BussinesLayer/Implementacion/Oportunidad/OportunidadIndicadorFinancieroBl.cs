﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadIndicadorFinancieroBl : IOportunidadIndicadorFinancieroBl
    {
        readonly IOportunidadIndicadorFinancieroDal iOportunidadIndicadorFinancieroDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public OportunidadIndicadorFinancieroBl(IOportunidadIndicadorFinancieroDal IOportunidadIndicadorFinancieroDal)
        {
            iOportunidadIndicadorFinancieroDal = IOportunidadIndicadorFinancieroDal;
        }

        public ProcesoResponse InsertarOportunidadIndicadorFinanciero(OportunidadIndicadorFinancieroDtoRequest OportunidadIndicadorFinancieroDtoRequest)
        {
            var objOportunidadIndicadorFinanciero = new OportunidadIndicadorFinanciero()
            {
                IdIndicadorFinanciero = OportunidadIndicadorFinancieroDtoRequest.IdIndicadorFinanciero,
                IdOportunidadLineaNegocio = OportunidadIndicadorFinancieroDtoRequest.IdOportunidadLineaNegocio,
                Anio = OportunidadIndicadorFinancieroDtoRequest.Anio,
                Monto = OportunidadIndicadorFinancieroDtoRequest.Monto,
                IdEstado = OportunidadIndicadorFinancieroDtoRequest.IdEstado,
                FechaCreacion = OportunidadIndicadorFinancieroDtoRequest.FechaCreacion,
                IdUsuarioCreacion = OportunidadIndicadorFinancieroDtoRequest.IdUsuarioCreacion,
                TipoFicha = OportunidadIndicadorFinancieroDtoRequest.TipoFicha
            };

            iOportunidadIndicadorFinancieroDal.Add(objOportunidadIndicadorFinanciero);
            iOportunidadIndicadorFinancieroDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;

            return respuesta;
        }

        public OportunidadIndicadorFinancieroDtoResponse ObtenerOportunidadIndicadorFinanciero(OportunidadIndicadorFinancieroDtoRequest OportunidadIndicadorFinanciero)
        {
            var valida = iOportunidadIndicadorFinancieroDal.GetFilteredAsNoTracking(x => x.IdOportunidadLineaNegocio == OportunidadIndicadorFinanciero.IdOportunidadLineaNegocio &&
                                                                                                                    x.IdEstado == OportunidadIndicadorFinanciero.IdEstado &&
                                                                                                                    x.IdIndicadorFinanciero == OportunidadIndicadorFinanciero.IdIndicadorFinanciero &&
                                                                                                                    x.TipoFicha == OportunidadIndicadorFinanciero.TipoFicha).Any();
            if (valida)
            {
                var oportunidadIndicadorFinanciero = iOportunidadIndicadorFinancieroDal.GetFilteredAsNoTracking(x => x.IdOportunidadLineaNegocio == OportunidadIndicadorFinanciero.IdOportunidadLineaNegocio &&
                                                                                                                        x.IdEstado == OportunidadIndicadorFinanciero.IdEstado &&
                                                                                                                        x.IdIndicadorFinanciero == OportunidadIndicadorFinanciero.IdIndicadorFinanciero &&
                                                                                                                        x.TipoFicha == OportunidadIndicadorFinanciero.TipoFicha).ToList();
                return oportunidadIndicadorFinanciero.Select(x => new OportunidadIndicadorFinancieroDtoResponse
                {
                    IdOportunidadIndicador = x.IdOportunidadIndicador,
                    IdIndicadorFinanciero = x.IdIndicadorFinanciero,
                    IdOportunidadLineaNegocio = x.IdOportunidadLineaNegocio,
                    Anio = x.Anio,
                    Monto = x.Monto
                }).Single();
            }
            else { return null; }
            
        }
    }
}
