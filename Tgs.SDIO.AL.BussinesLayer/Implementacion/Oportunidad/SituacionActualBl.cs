﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using System.Linq;
using System.Linq.Expressions;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class SituacionActualBl: ISituacionActualBl
    {
        readonly ISituacionActualDal iSituacionActualDal;
        readonly IArchivoDal iArchivoDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public SituacionActualBl
            (
            ISituacionActualDal ISituacionActualDal,
            IArchivoDal IArchivoDal
            )
        {
            iSituacionActualDal = ISituacionActualDal;
            iArchivoDal = IArchivoDal;
        }

        #region - Transaccionales -
        public ProcesoResponse RegistrarSituacionActual(SituacionActualDtoRequest situacionActual)
        {
            var entidad = iSituacionActualDal.GetFiltered(x => x.IdOportunidad == situacionActual.IdOportunidad && x.IdTipo == situacionActual.IdTipo).FirstOrDefault();
            if (entidad == null)
            {
                entidad = new Entities.Entities.Oportunidad.SituacionActual
                {
                    IdOportunidad = situacionActual.IdOportunidad,
                    IdTipo = situacionActual.IdTipo,
                    Descripcion = situacionActual.Descripcion,
                    IdEstado = 1,
                    IdUsuarioCreacion = situacionActual.IdUsuarioCreacion,
                    FechaCreacion = situacionActual.FechaCreacion
                };
                iSituacionActualDal.Add(entidad);
            }
            else
            {
                entidad.Descripcion = situacionActual.Descripcion;
                entidad.IdUsuarioEdicion = situacionActual.IdUsuarioEdicion;
                entidad.FechaEdicion = situacionActual.FechaEdicion;
                iSituacionActualDal.Modify(entidad);
            }

            iSituacionActualDal.UnitOfWork.Commit();

            respuesta.Id = entidad.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje =
                !entidad.IdUsuarioEdicion.HasValue ?
                MensajesGeneralOportunidad.RegistrarSituacionActual :
                MensajesGeneralOportunidad.ActualizarSituacionActual;

            return respuesta;
        }
        #endregion

        #region - No Transaccionales -
        public SituacionActualDtoResponse ObtenerPorIdOportunidad(SituacionActualDtoRequest request)
        {
            var response = iSituacionActualDal.ObtenerPorIdOportunidad(request);
            //response.Archivos = iArchivoDal.ListarPaginado(new ArchivoDtoRequest
            //{
            //    CodigoTabla = Generales.Tablas.Oportunidad.SituacionActual.GetHashCode(),
            //    IdEntidad = response.Id,
            //    IdCategoria = response.IdTipo
            //});

            return response;
        }
        #endregion
    }
}
