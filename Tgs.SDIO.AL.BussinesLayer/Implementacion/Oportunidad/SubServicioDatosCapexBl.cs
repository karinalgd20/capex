﻿using System;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Implementacion.Comun;
using Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class SubServicioDatosCapexBl : ISubServicioDatosCapexBl
    {
        readonly ISubServicioDatosCapexDal iSubServicioDatosCapexDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SubServicioDatosCapexBl(ISubServicioDatosCapexDal ISubServicioDatosCapexDal)
        {
            iSubServicioDatosCapexDal = ISubServicioDatosCapexDal;
        }

        public ProcesoResponse ActualizarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio)
        {
            var objSubServicio = new SubServicioDatosCapex()
            {
                IdFlujoCaja = SubServicio.IdFlujoCaja,
                IdSubServicioDatosCapex = SubServicio.IdSubServicioDatosCapex,
                Circuito = SubServicio.Circuito,
                SISEGO = SubServicio.SISEGO,
                MesesAntiguedad = SubServicio.MesesAntiguedad,
                CostoUnitarioAntiguo = SubServicio.CostoUnitarioAntiguo,
                ValorResidualSoles = SubServicio.ValorResidualSoles,
                CostoUnitario = SubServicio.CostoUnitario,
                CapexDolares = SubServicio.CapexDolares,
                CapexSoles = SubServicio.CapexSoles,
                TotalCapex = SubServicio.TotalCapex,
                AnioRecupero = SubServicio.AnioRecupero,
                MesRecupero = SubServicio.MesRecupero,
                AnioComprometido = SubServicio.AnioComprometido,
                MesComprometido = SubServicio.MesComprometido,
                AnioCertificado = SubServicio.AnioCertificado,
                MesCertificado = SubServicio.MesCertificado,
                IdMedio = SubServicio.IdMedio,
                IdTipo = SubServicio.IdTipo,
                Garantizado = SubServicio.Garantizado,
                IdEstado = SubServicio.IdEstado,
                IdUsuarioEdicion = SubServicio.IdUsuarioEdicion,
                FechaEdicion = SubServicio.FechaEdicion,
                Cruce = SubServicio.Cruce,
                AEReducido = SubServicio.AEReducido,
                Combo = SubServicio.Combo,
                Antiguedad = SubServicio.Antiguedad,
                CapexInstalacion = SubServicio.CapexInstalacion,
                CapexReal = SubServicio.CapexReal,
                CapexTotalReal = SubServicio.CapexTotalReal,
                Marca = SubServicio.Marca,
                Modelo = SubServicio.Modelo,
                Tipo = SubServicio.Tipo
            };

            iSubServicioDatosCapexDal.ActualizarPorCampos(objSubServicio, x => x.IdFlujoCaja, x=> x.IdSubServicioDatosCapex, x => x.Circuito, x => x.SISEGO, x => x.MesesAntiguedad, x => x.CostoUnitarioAntiguo, x => x.ValorResidualSoles, x => x.CostoUnitario, x => x.CapexDolares, x => x.CapexSoles,
                                                x => x.TotalCapex, x => x.AnioRecupero, x=> x.MesRecupero, x=>x.AnioComprometido, x => x.MesComprometido, x => x.AnioCertificado, x => x.MesCertificado, x => x.IdMedio, x => x.IdTipo, x => x.Garantizado, x => x.IdEstado, x => x.IdUsuarioEdicion,
                                                x => x.FechaEdicion, x => x.Cruce, x => x.AEReducido, x => x.Combo, x => x.Antiguedad, x => x.CapexInstalacion, x => x.CapexReal, x => x.CapexTotalReal, x => x.Marca, x => x.Modelo, x => x.Tipo);
            iSubServicioDatosCapexDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (SubServicio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarSubServicio : MensajesGeneralComun.ActualizarSubServicio;

            return respuesta;
        }
        
        public SubServicioDatosCapexDtoResponse ListarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio)
        {
            var lista = iSubServicioDatosCapexDal.GetFilteredAsNoTracking(x => x.IdEstado == SubServicio.IdEstado &&
                                                                            x.IdSubServicioDatosCapex == SubServicio.IdSubServicioDatosCapex).ToList();
            return lista.Select(x => new SubServicioDatosCapexDtoResponse
            {
                AEReducido = x.AEReducido,
                AnioCertificado = x.AnioCertificado,
                AnioComprometido = x.AnioComprometido,
                AnioRecupero = x.AnioRecupero,
                Antiguedad = x.Antiguedad,
                CapexDolares = x.CapexDolares,
                CapexInstalacion = x.CapexInstalacion,
                CapexReal = x.CapexReal,
                CapexSoles = x.CapexSoles,
                CapexTotalReal = x.CapexTotalReal,
                Circuito = x.Circuito,
                Combo = x.Combo,
                CostoUnitario = x.CostoUnitario,
                CostoUnitarioAntiguo = x.CostoUnitarioAntiguo,
                Cruce = x.Cruce,
                Garantizado = x.Garantizado,
                IdFlujoCaja = x.IdFlujoCaja,
                IdSubServicioDatosCapex = x.IdSubServicioDatosCapex,
                IdTipo = x.IdTipo,
                Marca = x.Marca,
                IdMedio = x.IdMedio,
                MesCertificado = x.MesCertificado,
                MesComprometido = x.MesComprometido,
                MesesAntiguedad = x.MesesAntiguedad,
                MesRecupero = x.MesRecupero,
                Modelo = x.Modelo,
                SISEGO = x.SISEGO,
                Tipo = x.Tipo,
                TotalCapex = x.TotalCapex,
                ValorResidualSoles = x.ValorResidualSoles
            }).Single();
        }

        public SubServicioDatosCapexDtoResponse ObtenerSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio)
        {
            var lista = iSubServicioDatosCapexDal.GetFilteredAsNoTracking(x => x.IdEstado == SubServicio.IdEstado &&
                                                                            x.IdSubServicioDatosCapex == SubServicio.IdSubServicioDatosCapex).ToList();
            return lista.Select(x => new SubServicioDatosCapexDtoResponse
            {
                AEReducido = x.AEReducido,
                AnioCertificado = x.AnioCertificado,
                AnioComprometido = x.AnioComprometido,
                AnioRecupero = x.AnioRecupero,
                Antiguedad = x.Antiguedad,
                CapexDolares = x.CapexDolares,
                CapexInstalacion = x.CapexInstalacion,
                CapexReal = x.CapexReal,
                CapexSoles = x.CapexSoles,
                CapexTotalReal = x.CapexTotalReal,
                Circuito = x.Circuito,
                Combo = x.Combo,
                CostoUnitario = x.CostoUnitario,
                CostoUnitarioAntiguo = x.CostoUnitarioAntiguo,
                Cruce = x.Cruce,
                Garantizado = x.Garantizado,
                IdFlujoCaja = x.IdFlujoCaja,
                IdSubServicioDatosCapex = x.IdSubServicioDatosCapex,
                IdTipo = x.IdTipo,
                Marca = x.Marca,
                IdMedio = x.IdMedio,
                MesCertificado = x.MesCertificado,
                MesComprometido = x.MesComprometido,
                MesesAntiguedad = x.MesesAntiguedad,
                MesRecupero = x.MesRecupero,
                Modelo = x.Modelo,
                SISEGO = x.SISEGO,
                Tipo = x.Tipo,
                TotalCapex = x.TotalCapex,
                ValorResidualSoles = x.ValorResidualSoles
            }).Single();
        }

        public ProcesoResponse RegistrarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio)
        {
            var objSubServicio = new SubServicioDatosCapex()
            {
                IdFlujoCaja = SubServicio.IdFlujoCaja,
                Circuito = SubServicio.Circuito,
                SISEGO = SubServicio.SISEGO,
                MesesAntiguedad = SubServicio.MesesAntiguedad,
                CostoUnitarioAntiguo = SubServicio.CostoUnitarioAntiguo,
                ValorResidualSoles = SubServicio.ValorResidualSoles,
                CostoUnitario = SubServicio.CostoUnitario,
                CapexDolares = SubServicio.CapexDolares,
                CapexSoles = SubServicio.CapexSoles,
                TotalCapex = SubServicio.TotalCapex,
                AnioRecupero = SubServicio.AnioRecupero,
                MesRecupero = SubServicio.MesRecupero,
                AnioComprometido = SubServicio.AnioComprometido,
                MesComprometido = SubServicio.MesComprometido,
                AnioCertificado = SubServicio.AnioCertificado,
                MesCertificado = SubServicio.MesCertificado,
                IdMedio = SubServicio.IdMedio,
                IdTipo = SubServicio.IdTipo,
                Garantizado = SubServicio.Garantizado,
                Cruce = SubServicio.Cruce,
                AEReducido = SubServicio.AEReducido,
                Combo = SubServicio.Combo,
                Antiguedad = SubServicio.Antiguedad,
                CapexInstalacion = SubServicio.CapexInstalacion,
                CapexReal = SubServicio.CapexReal,
                CapexTotalReal = SubServicio.CapexTotalReal,
                Marca = SubServicio.Marca,
                Modelo = SubServicio.Modelo,
                Tipo = SubServicio.Tipo,
                IdEstado = SubServicio.IdEstado,
                IdUsuarioCreacion = SubServicio.IdUsuarioCreacion,
                FechaCreacion = SubServicio.FechaCreacion,
            };

            iSubServicioDatosCapexDal.Add(objSubServicio);
            iSubServicioDatosCapexDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (SubServicio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarSubServicio : MensajesGeneralComun.ActualizarSubServicio;



            return respuesta;
        }

        public SubServicioDatosCapexDtoResponse CantidadEstudiosEsp(SubServicioDatosCapexDtoRequest dto)
        {
            var context = new DioContext();   
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
                     int meses=0;

            if(subServicio.IdDepreciacion!=null)
            {
                var depreciacion = new DepreciacionDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                         && x.IdDepreciacion == subServicio.IdDepreciacion).Single();
                 meses = Convert.ToInt32(depreciacion.Meses);
            }
      

            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;
            //  auxTipoCambio.IdOportunidad = dto.IdOportunidad;            

          

            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);
          
            if (dto.IdTipoSubServicio == 4){

              
                decimal valorResidual = Decimal.Divide(dto.CuAntiguiedad, meses) * (meses - dto.MesesAntiguedad);
         
               // datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? Convert.ToDecimal(valorResidual.ToString("#.00")) : 0;
                datosCapex.ValorResidualSoles =  Convert.ToDecimal(valorResidual.ToString("#.00"));
                datosCapex.CapexInstalacion = Numeric.Cero;

                datosCapex.CostoUnitario = datosCapex.ValorResidualSoles;
                datosCapex.CapexSoles = datosCapex.ValorResidualSoles * dto.Cantidad; //Capex total (S/.)
            }
            else {
    
                datosCapex.ValorResidualSoles = Numeric.Cero;
                datosCapex.CapexSoles = dto.Cu * dto.Cantidad; //Capex total (S/.)
            }

            datosCapex.CapexInstalacion = dto.Instalacion;      //

        
            datosCapex.TotalCapex = datosCapex.CapexSoles+datosCapex.CapexInstalacion; //TOTAL CAPEX
            datosCapex.CapexDolares = Math.Round(Convert.ToDecimal(datosCapex.CapexSoles / tipoCambio.Monto), 2);
            
            //if (subServicio.Descripcion != "Estudios Residuales")


            return datosCapex;
        }

        public SubServicioDatosCapexDtoResponse CantidadEquiposEsp(SubServicioDatosCapexDtoRequest dto) {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
            int meses = 0;

            if (subServicio.IdDepreciacion != null)
            {
                var depreciacion = new DepreciacionDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                         && x.IdDepreciacion == subServicio.IdDepreciacion).Single();
                meses = Convert.ToInt32(depreciacion.Meses);
            }

            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;            

           
            decimal valorResidual = Decimal.Divide(dto.CuAntiguiedad, meses) * (meses - dto.MesesAntiguedad);
            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);


            if (dto.IdTipoSubServicio == 4)
            {

                datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? Convert.ToDecimal(valorResidual.ToString("#.00")) : 0; //Valor residual (USD)


                datosCapex.CostoUnitario = datosCapex.ValorResidualSoles; //Capex (USD)
                datosCapex.CapexDolares = datosCapex.ValorResidualSoles*dto.Cantidad; //Capex (USD)

                datosCapex.CapexSoles = (datosCapex.CapexDolares * tipoCambio.Monto); //Capex total (S/.)


                datosCapex.CostoUnitario = datosCapex.ValorResidualSoles;

            }
            else
            {
                datosCapex.CapexDolares =  Convert.ToDecimal(dto.Cu * dto.Cantidad); //Capex (USD)
                datosCapex.ValorResidualSoles = 0;
                datosCapex.CapexSoles = (datosCapex.CapexDolares * tipoCambio.Monto); //Capex total (S/.)
            }
          

            datosCapex.CapexInstalacion = dto.Instalacion;      //
            datosCapex.TotalCapex = datosCapex.CapexSoles+ dto.Instalacion;

            if ((subServicio.Descripcion == "Demarcadores Residuales" || subServicio.Descripcion == "Minilinks Residuales"))
            {
                    datosCapex.ValorResidualSoles = 0;
            }

                

            return datosCapex;
        }
        
        public SubServicioDatosCapexDtoResponse CantidadRouters(SubServicioDatosCapexDtoRequest dto)
        {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
            int meses = 0;

            if (subServicio.IdDepreciacion != null)
            {
                var depreciacion = new DepreciacionDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                         && x.IdDepreciacion == subServicio.IdDepreciacion).Single();
                meses = Convert.ToInt32(depreciacion.Meses);
            }

            var objSubServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                       && x.IdSubServicio == subServicio.IdSubServicio).Single(); 


            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;            

         
            
            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);
            decimal? Instalacion = objSubServicio.CostoInstalacion;
            decimal tc = Convert.ToDecimal(tipoCambio.Monto);

            if (dto.IdTipoSubServicio == 1)
            {

               
                datosCapex.ValorResidualSoles = Generales.Numeric.Cero;                                         
                datosCapex.CapexInstalacion = ((dto.Cantidad * Instalacion) * tc);                                                 
                datosCapex.CapexDolares = Convert.ToDecimal(dto.Cu * dto.Cantidad);
            }
            if (dto.IdTipoSubServicio == 2)
            {




                datosCapex.ValorResidualSoles = Generales.Numeric.Cero;
                datosCapex.CapexInstalacion = ((dto.Cantidad * Instalacion) * tc);          
                datosCapex.CapexDolares = Convert.ToDecimal(dto.Cu * dto.Cantidad);
           


               }
            if (dto.IdTipoSubServicio == 3)
            {




                datosCapex.ValorResidualSoles = Generales.Numeric.Cero;
                datosCapex.CapexInstalacion = ((dto.Cantidad * Instalacion) * tc);
                datosCapex.CapexDolares = Convert.ToDecimal(dto.Cu * dto.Cantidad);



            }

            if (dto.IdTipoSubServicio == 4)
            {





                decimal valorResidual = Decimal.Divide(dto.CuAntiguiedad, meses) * (meses - dto.MesesAntiguedad);

                datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? Convert.ToDecimal(valorResidual.ToString("#.00")) : 0; //Valor residual (USD)
              
                datosCapex.CapexInstalacion = Numeric.Cero;
 
                datosCapex.CostoUnitario = datosCapex.ValorResidualSoles;

                datosCapex.CapexDolares = Convert.ToDecimal(datosCapex.CostoUnitario * dto.Cantidad);

            }


            decimal capexSoles = Convert.ToDecimal(datosCapex.CapexDolares) * Convert.ToDecimal(tipoCambio.Monto);



            datosCapex.CapexSoles = Math.Round(capexSoles, 1, MidpointRounding.AwayFromZero); // Rounds "up"
            //  datosCapex.TotalCapex = (dto.Indice == 1) ? datosCapex.CapexSoles : ((dto.Cantidad * Instalacion) * tc) + datosCapex.CapexSoles;
            datosCapex.TotalCapex = ((datosCapex.CapexInstalacion)) + datosCapex.CapexSoles;






            //if (subServicio.Descripcion != "Routers Residuales")


            return datosCapex;
        }
        
        public SubServicioDatosCapexDtoResponse CantidadModems(SubServicioDatosCapexDtoRequest dto)
        {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
            int meses = 0;

            if (subServicio.IdDepreciacion != null)
            {
                var depreciacion = new DepreciacionDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                         && x.IdDepreciacion == subServicio.IdDepreciacion).Single();
                meses = Convert.ToInt32(depreciacion.Meses);
            }
            //var instalacion = new MaestraDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
            //                            && x.IdMaestra == 153).Single(); //153 = instalacion

            var objSubServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                       && x.IdSubServicio == subServicio.IdSubServicio).Single(); //153 = instalacion




            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;

     

            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);
            decimal? Instalacion = objSubServicio.CostoInstalacion;
            decimal tc = Convert.ToDecimal(tipoCambio.Monto);

            if (dto.IdTipoSubServicio == 4)
            {
               
               
                datosCapex.ValorResidualSoles = 0;


            }
            else
            {
    
               
                decimal valorResidual = Decimal.Divide(dto.CuAntiguiedad, meses) * (meses - dto.MesesAntiguedad);


                datosCapex.CapexInstalacion = ((dto.Cantidad * Instalacion) * tc);
                datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? decimal.Round(valorResidual, 2) : Numeric.Cero;

            }
            datosCapex.CapexDolares = decimal.Round((Convert.ToDecimal(dto.Cu) * dto.Cantidad), 2);
            datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);

            //  datosCapex.TotalCapex = (dto.Indice == 1) ? datosCapex.CapexSoles : ((dto.Cantidad * Instalacion) * tc) + datosCapex.CapexSoles;
            datosCapex.TotalCapex =  ((dto.Cantidad * Instalacion) * tc) + datosCapex.CapexSoles;

            //if (subServicio.Descripcion != "Modems Residuales")


            return datosCapex;
        }
        
        public SubServicioDatosCapexDtoResponse CantidadEquiposSeguridad(SubServicioDatosCapexDtoRequest dto)
        {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
            int meses = 0;

            if (subServicio.IdDepreciacion != null)
            {
                var depreciacion = new DepreciacionDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                         && x.IdDepreciacion == subServicio.IdDepreciacion).Single();
                meses = Convert.ToInt32(depreciacion.Meses);
            }
            //var instalacion = new MaestraDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
            //                            && x.IdMaestra == 153).Single();

            var objSubServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                   && x.IdSubServicio == subServicio.IdSubServicio).Single(); //153 = instalacion



            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;

            
            decimal valorResidual = Numeric.Cero;
            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);
           // int Instalacion = Convert.ToInt32(instalacion.Valor);
            decimal? Instalacion = objSubServicio.CostoInstalacion;
            decimal tc = Convert.ToDecimal(tipoCambio.Monto);
        
            if (dto.IdTipoSubServicio == 4)
            {

             
                valorResidual = Decimal.Divide(dto.CuAntiguiedad, meses) * (meses - dto.MesesAntiguedad);

                datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? decimal.Round(valorResidual, 2) : Numeric.Cero;
              


            }
            else
            {
                datosCapex.ValorResidualSoles = 0;

            }
            datosCapex.CapexInstalacion = 0;
            //datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? decimal.Round(valorResidual, 2) : Numeric.Cero;
            datosCapex.CapexDolares = dto.Cu * dto.Cantidad;
            datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            datosCapex.TotalCapex = datosCapex.CapexSoles;

            //if (subServicio.Descripcion != "Equipos de Seguridad Residuales")
            //    datosCapex.ValorResidualSoles = 0;

            return datosCapex;
        }
        
        public SubServicioDatosCapexDtoResponse CantidadCapex(SubServicioDatosCapexDtoRequest dto)
        {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
            int meses = 0;

            if (subServicio.IdDepreciacion != null)
            {
                var depreciacion = new DepreciacionDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                         && x.IdDepreciacion == subServicio.IdDepreciacion).Single();
                meses = Convert.ToInt32(depreciacion.Meses);
            }
            //var instalacion = new MaestraDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
            //                            && x.IdMaestra == 153).Single();

            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;

            var objSubServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                  && x.IdSubServicio == subServicio.IdSubServicio).Single(); //153 = instalacion


            decimal valorResidual = Numeric.Cero;
            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);
            decimal? Instalacion = objSubServicio.CostoInstalacion;
            decimal tc = Convert.ToDecimal(tipoCambio.Monto);


            if (dto.IdTipoSubServicio == 4 )
            {
        
                valorResidual = Decimal.Divide(dto.CuAntiguiedad, meses) * (meses - dto.MesesAntiguedad);

                datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? decimal.Round(valorResidual, 2) : Numeric.Cero;



            }

            //if (dto.IdTipoSubServicio == 4)
            //{

            //    dto.MesesAntiguedad = 60;
            //    valorResidual = Decimal.Divide(dto.CuAntiguiedad, meses) * (meses - dto.MesesAntiguedad);

            //    datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? decimal.Round(valorResidual, 2) : Numeric.Cero;



            //}
            //if (dto.IdTipoSubServicio == 4)
            //{

            //    dto.MesesAntiguedad = 60;
            //    valorResidual = Decimal.Divide(dto.CuAntiguiedad, meses) * (meses - dto.MesesAntiguedad);

            //    datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? decimal.Round(valorResidual, 2) : Numeric.Cero;



            //}
            //if (dto.IdTipoSubServicio == 4)
            //{

            //    dto.MesesAntiguedad = 60;
            //    valorResidual = Decimal.Divide(dto.CuAntiguiedad, meses) * (meses - dto.MesesAntiguedad);

            //    datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? decimal.Round(valorResidual, 2) : Numeric.Cero;



            //}
            else
            {
                datosCapex.ValorResidualSoles = 0;

            }
            datosCapex.CapexInstalacion = (dto.Cantidad * Instalacion) *tc;

            //datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? Convert.ToDecimal(valorResidual.ToString("#.00")) : Numeric.Cero;
            datosCapex.CapexDolares = dto.Cu * dto.Cantidad;
            datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            //datosCapex.TotalCapex = (dto.Indice == Numeric.Uno) ? datosCapex.CapexSoles : ((dto.Cantidad * Instalacion) * tc) + datosCapex.CapexSoles;
            datosCapex.TotalCapex = ((dto.Cantidad * Instalacion) * tc) + datosCapex.CapexSoles;

            return datosCapex;
        }
        
        public SubServicioDatosCapexDtoResponse CantidadSolarWindVPN(SubServicioDatosCapexDtoRequest dto)
        {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
           
            var solarw = new MaestraDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdMaestra == 108).Single(); //108 = solarwind

            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;

            int solarwind = Convert.ToInt32(solarw.Valor);
            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);
            decimal tc = Convert.ToDecimal(tipoCambio.Monto);
            
            datosCapex.CapexDolares = solarwind * dto.Cantidad;
            datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            datosCapex.TotalCapex = datosCapex.CapexSoles;

            return datosCapex;
        }
        public SubServicioDatosCapexDtoResponse CantidadSatelitales(SubServicioDatosCapexDtoRequest dto)
        {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var servicio = new ServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdServicio == flujoCaja.IdServicio).Single();
            
            var instalacion = new MaestraDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo

                                        && x.IdMaestra == 153).Single(); //153 = instalacion

            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;

          
 
            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);
            int Instalacion = Convert.ToInt32(instalacion.Valor);
            decimal tc = Convert.ToDecimal(tipoCambio.Monto);

            if (dto.IdTipoSubServicio == 2)
            {

                datosCapex.CapexDolares = dto.Cantidad * dto.Inversion;
                datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
                datosCapex.CapexInstalacion = (dto.Cantidad * dto.PorcentajeGarantizado) * tc;
                datosCapex.TotalCapex = datosCapex.CapexInstalacion + datosCapex.CapexSoles;
            }

            if (dto.IdTipoSubServicio == 4)
            {

                if (dto.MesesAntiguedad > 0)
                {
                    datosCapex.CapexDolares = (dto.Cantidad * dto.Inversion / 60) * (60 - dto.MesesAntiguedad);

                }
                else
                {
                    datosCapex.CapexDolares = dto.Cantidad * dto.Inversion;
                }
                datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
                datosCapex.CapexInstalacion = 0;
                datosCapex.TotalCapex = datosCapex.CapexInstalacion + datosCapex.CapexSoles;

            }


            //if (servicio.IdServicio == 47 || servicio.IdServicio == 48 || servicio.IdServicio == 49)
            //{

            //    datosCapex.CapexDolares =  dto.Cantidad* dto.Inversion;
            //    datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            //    datosCapex.CapexInstalacion = (dto.Cantidad * dto.PorcentajeGarantizado) * tc;
            //    datosCapex.TotalCapex = datosCapex.CapexInstalacion+ datosCapex.CapexSoles;   
            //}
            //if (servicio.IdServicio == 50 || servicio.IdServicio == 51 || servicio.IdServicio == 52)
            //{
            //    datosCapex.CapexDolares = dto.Cantidad * dto.Inversion;
            //    datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            //    datosCapex.CapexInstalacion = (dto.Cantidad * dto.PorcentajeGarantizado) * tc;
            //    datosCapex.TotalCapex = datosCapex.CapexInstalacion + datosCapex.CapexSoles;

            //}
            //if (servicio.IdServicio == 53 || servicio.IdServicio == 54 || servicio.IdServicio == 55)
            //{
            //    datosCapex.CapexDolares = dto.Cantidad * dto.Inversion;
            //    datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            //    datosCapex.CapexInstalacion = (dto.Cantidad * dto.PorcentajeGarantizado) * tc;
            //    datosCapex.TotalCapex = datosCapex.CapexInstalacion + datosCapex.CapexSoles;

            //}
            //if (servicio.IdServicio == 56 || servicio.IdServicio == 57)
            //{

            //    if (dto.MesesAntiguedad > 0)
            //    {
            //        datosCapex.CapexDolares = (dto.Cantidad * dto.Inversion / 60) * (60 - dto.MesesAntiguedad);

            //    }
            //    else
            //    {
            //        datosCapex.CapexDolares = dto.Cantidad * dto.Inversion;
            //    }
            //    datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            //    datosCapex.CapexInstalacion = 0;
            //    datosCapex.TotalCapex = datosCapex.CapexInstalacion + datosCapex.CapexSoles;

            //}
            //if (servicio.IdServicio == 58 || servicio.IdServicio == 59)
            //{

            //    if (dto.MesesAntiguedad > 0)
            //    {
            //        datosCapex.CapexDolares = (dto.Cantidad * dto.Inversion / 60) * (60 - dto.MesesAntiguedad);

            //    }
            //    else
            //    {
            //        datosCapex.CapexDolares = dto.Cantidad * dto.Inversion;
            //    }
            //    datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            //    datosCapex.CapexInstalacion = 0;
            //    datosCapex.TotalCapex = datosCapex.CapexInstalacion + datosCapex.CapexSoles;

            //}
            //if (servicio.IdServicio == 60 || servicio.IdServicio == 61)
            //{

            //    if (dto.MesesAntiguedad > 0)
            //    {
            //        datosCapex.CapexDolares = (dto.Cantidad * dto.Inversion / 60) * (60 - dto.MesesAntiguedad);

            //    }
            //    else
            //    {
            //        datosCapex.CapexDolares = dto.Cantidad * dto.Inversion;
            //    }
            //    datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            //    datosCapex.CapexInstalacion = 0;
            //    datosCapex.TotalCapex = datosCapex.CapexInstalacion + datosCapex.CapexSoles;
            //}
            //if (servicio.IdServicio == 62)
            //{


            //        datosCapex.CapexDolares = dto.Cantidad * dto.Inversion;

            //    datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            //    datosCapex.CapexInstalacion = 0;
            //    datosCapex.TotalCapex = datosCapex.CapexInstalacion + datosCapex.CapexSoles;
            //}
            //if (servicio.IdServicio == 63)
            //{
            //   if(dto.IdGarantizado ==3) { 
            //    if (dto.IdTipoSatelital == 3)
            //    {
            //        var costo = new CostoDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
            //                       && x.IdCosto == 301).Single();

            //        dto.Inversion = costo.CostoSegmentoSatelital;
            //    }
            //    if (dto.IdTipoSatelital == 4)
            //    {
            //        var costo = new CostoDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
            //         && x.IdCosto == 300).Single();
            //        dto.Inversion = costo.CostoSegmentoSatelital;
            //    }

            //    datosCapex.CapexDolares = Generales.Numeric.Uno* dto.Inversion;

            //    datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            //    datosCapex.CapexInstalacion = 0;
            //    datosCapex.TotalCapex = datosCapex.CapexInstalacion + datosCapex.CapexSoles;
            //    }
            //    if (dto.IdGarantizado ==4)
            //    {

            //        datosCapex.CapexDolares = 0;
            //        datosCapex.CapexSoles = 0;
            //        datosCapex.CapexInstalacion = 0;
            //        datosCapex.TotalCapex = 0;
            //    }



            //}
            //if (servicio.IdServicio == 64)
            //{
            //    if (dto.IdGarantizado == 3)
            //    {

            //        datosCapex.CapexDolares = Generales.Numeric.Uno * dto.Inversion;

            //    datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            //    datosCapex.CapexInstalacion = 0;
            //    datosCapex.TotalCapex = datosCapex.CapexInstalacion + datosCapex.CapexSoles;
            //    }
            //    if (dto.IdGarantizado == 4)
            //    {

            //        datosCapex.CapexDolares = 0;

            //        datosCapex.CapexSoles = 0;
            //        datosCapex.CapexInstalacion = 0;
            //        datosCapex.TotalCapex = 0;
            //    }


            //}
            //if (dto.IdTipoSatelital == 4)
            //{
            //    var costo = new CostoDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
            //     && x.IdCosto == 300).Single();
            //    dto.Inversion = costo.CostoSegmentoSatelital;
            //    datosCapex.CapexDolares = Generales.Numeric.Uno * dto.Inversion;

            //    datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            //    datosCapex.CapexInstalacion = 0;
            //    datosCapex.TotalCapex = datosCapex.CapexInstalacion + datosCapex.CapexSoles;
            //}



           if (dto.IdGarantizado == 3)
            {

                datosCapex.CapexDolares = Generales.Numeric.Uno * dto.Inversion;

                datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
                datosCapex.CapexInstalacion = 0;
                datosCapex.TotalCapex = datosCapex.CapexInstalacion + datosCapex.CapexSoles;


            }
            if (dto.IdGarantizado == 4)
            {

                datosCapex.CapexDolares = 0;

                datosCapex.CapexSoles = 0;
                datosCapex.CapexInstalacion = 0;
                datosCapex.TotalCapex = 0;
            }
            return datosCapex;
        }
    }
}
