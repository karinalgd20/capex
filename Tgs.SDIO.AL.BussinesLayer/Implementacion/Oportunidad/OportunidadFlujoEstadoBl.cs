﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadFlujoEstadoBl : IOportunidadFlujoEstadoBl
    {
        readonly IOportunidadFlujoEstadoDal iOportunidadFlujoEstadoDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadFlujoEstadoBl(IOportunidadFlujoEstadoDal IOportunidadFlujoEstadoDal)
        {
            iOportunidadFlujoEstadoDal = IOportunidadFlujoEstadoDal;
        }



        public ProcesoResponse ActualizarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            DateTime fecha = DateTime.Now;

            var objOportunidadFlujoEstado = new OportunidadFlujoEstado()
            {
                IdOportunidadFlujoEstado = oportunidadFlujoEstado.IdOportunidadFlujoEstado,

            };

            iOportunidadFlujoEstadoDal.ActualizarPorCampos(objOportunidadFlujoEstado);

            iOportunidadFlujoEstadoDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            //  respuesta.Mensaje = MensajesGeneralOportunidadFlujoEstado.ActualizarOportunidadFlujoEstado;




            return respuesta;
        }



        public List<OportunidadFlujoEstadoDtoResponse> ListarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            throw new NotImplementedException();
        }

        public OportunidadFlujoEstadoDtoResponse ObtenerOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            var objOportunidadFlujoEstado = iOportunidadFlujoEstadoDal.GetFiltered(p => p.IdOportunidadFlujoEstado == oportunidadFlujoEstado.IdOportunidadFlujoEstado);

            return objOportunidadFlujoEstado.Select(x => new OportunidadFlujoEstadoDtoResponse
            {

                IdOportunidadFlujoEstado = x.IdOportunidadFlujoEstado,

            }).Single();
        }

        public OportunidadFlujoEstadoDtoResponse ObtenerOportunidadFlujoEstadoId(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            OportunidadFlujoEstadoDtoResponse oportunidadFlujoEstadoResponse = new OportunidadFlujoEstadoDtoResponse();

            var objOportunidadFlujoEstado = iOportunidadFlujoEstadoDal.GetFiltered(p => p.IdOportunidad == oportunidadFlujoEstado.IdOportunidad).OrderByDescending(t => t.IdOportunidadFlujoEstado).ToList().First();

            oportunidadFlujoEstadoResponse.IdEstado = objOportunidadFlujoEstado.IdEstado;



            return oportunidadFlujoEstadoResponse;

        }

        public ProcesoResponse RegistrarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            DateTime fecha = DateTime.Now;

            var objFlujoCajaEstado= iOportunidadFlujoEstadoDal.GetFiltered(p => p.IdOportunidad == oportunidadFlujoEstado.IdOportunidad);


            var valida = objFlujoCajaEstado.Any();
            if (valida)
            {
             var objResponse  = objFlujoCajaEstado.Select(x => new OportunidadFlujoEstadoDtoResponse
                {                   
                    IdEstado = x.IdEstado
                }).OrderByDescending(t => t.IdOportunidadFlujoEstado).ToList().First(); ;


                if (objResponse.IdEstado != oportunidadFlujoEstado.IdEstado)
                {
                    var objOportunidadFlujoEstado = new OportunidadFlujoEstado()
                    {
                        FechaCreacion = fecha,
                        IdUsuarioCreacion = oportunidadFlujoEstado.IdUsuarioCreacion,
                        IdOportunidad = oportunidadFlujoEstado.IdOportunidad,
                        IdEstado = oportunidadFlujoEstado.IdEstado
                    };


                    iOportunidadFlujoEstadoDal.Add(objOportunidadFlujoEstado);
                    iOportunidadFlujoEstadoDal.UnitOfWork.Commit();

                    respuesta.Id = objOportunidadFlujoEstado.IdOportunidadFlujoEstado;
                }
             
            }
            else
            {
                var objOportunidadFlujoEstado = new OportunidadFlujoEstado()
                {
                    FechaCreacion = fecha,
                    IdUsuarioCreacion = oportunidadFlujoEstado.IdUsuarioCreacion,
                    IdOportunidad = oportunidadFlujoEstado.IdOportunidad,
                    IdEstado = oportunidadFlujoEstado.IdEstado
                };


                iOportunidadFlujoEstadoDal.Add(objOportunidadFlujoEstado);
                iOportunidadFlujoEstadoDal.UnitOfWork.Commit();

                respuesta.Id = objOportunidadFlujoEstado.IdOportunidadFlujoEstado;
            }

            respuesta.TipoRespuesta = Proceso.Valido;
            //  respuesta.Mensaje = MensajesGeneralOportunidadFlujoEstado.RegistrarOportunidadFlujoEstado;

            return respuesta;
        }


        public ProcesoResponse InsertarConfiguracionFlujo(OportunidadLineaNegocioDtoRequest oportunidadFlujoEstado)
        {
            return iOportunidadFlujoEstadoDal.InsertarConfiguracionFlujo(oportunidadFlujoEstado);
        }
    }
}
