﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Mensajes.Comun;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadCostoBl : IOportunidadCostoBl
    {
        readonly IOportunidadCostoDal iOportunidadCostoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public OportunidadCostoBl(IOportunidadCostoDal IOportunidadCostoDal)
        {
            iOportunidadCostoDal = IOportunidadCostoDal;
        }



      
        public OportunidadCostoPaginadoDtoResponse ListaOportunidadCostoPaginado(OportunidadCostoDtoRequest oportunidadCosto)
        {
         
              return iOportunidadCostoDal.ListaOportunidadCostoPaginado(oportunidadCosto);
        }

        public ProcesoResponse ActualizarOportunidadCosto(OportunidadCostoDtoRequest oportunidadCosto)
        {
            var OportunidadCosto = new OportunidadCosto()
            {
                IdOportunidadLineaNegocio = oportunidadCosto.IdOportunidadLineaNegocio,
                IdCosto = oportunidadCosto.IdCosto,
                IdOportunidadCosto = oportunidadCosto.IdOportunidadCosto,
                IdTipoCosto = oportunidadCosto.IdTipoCosto,
                Descripcion = oportunidadCosto.Descripcion,
                Monto = oportunidadCosto.Monto,
                VelocidadSubidaKBPS = oportunidadCosto.VelocidadSubidaKBPS,
                PorcentajeGarantizado = oportunidadCosto.PorcentajeGarantizado,
                PorcentajeSobresuscripcion = oportunidadCosto.PorcentajeSobresuscripcion,
                CostoSegmentoSatelital = oportunidadCosto.CostoSegmentoSatelital,
                InvAntenaHubUSD = oportunidadCosto.InvAntenaHubUSD,
                AntenaCasaClienteUSD = oportunidadCosto.AntenaCasaClienteUSD,
                Instalacion = oportunidadCosto.Instalacion,
                IdUnidadConsumo = oportunidadCosto.IdUnidadConsumo,
                IdTipificacion = oportunidadCosto.IdTipificacion,
                CodigoModelo = oportunidadCosto.CodigoModelo,
                Modelo = oportunidadCosto.Modelo,
                IdEstado = oportunidadCosto.IdEstado,
                FechaEdicion = oportunidadCosto.FechaEdicion,
                IdUsuarioEdicion = oportunidadCosto.IdUsuarioEdicion
            };
            iOportunidadCostoDal.ActualizarPorCampos(OportunidadCosto, x => x.IdOportunidadLineaNegocio, x => x.IdCosto, x => x.IdOportunidadCosto, x => x.IdTipoCosto, x => x.Descripcion, x => x.Monto, x => x.VelocidadSubidaKBPS, x => x.PorcentajeGarantizado, x => x.PorcentajeSobresuscripcion, x => x.CostoSegmentoSatelital,
                x => x.InvAntenaHubUSD, x => x.AntenaCasaClienteUSD, x => x.Instalacion, x => x.IdUnidadConsumo, x => x.IdTipificacion, x => x.CodigoModelo, x => x.Modelo, x => x.IdEstado, x => x.FechaEdicion, x => x.IdUsuarioEdicion);
            iOportunidadCostoDal.UnitOfWork.Commit();

            respuesta.Id = OportunidadCosto.IdOportunidadCosto;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (OportunidadCosto.IdEstado == Estados.Inactivo) ? MensajesGeneralOportunidad.EliminarOportunidadCosto : MensajesGeneralOportunidad.ActualizarOportunidadCosto;

            return respuesta;
        }

        public OportunidadCostoDtoResponse ObtenerOportunidadCosto(OportunidadCostoDtoRequest oportunidadCosto)
        {
            var lista = iOportunidadCostoDal.GetFilteredAsNoTracking(x => x.IdEstado == Generales.Estados.Activo   &&
                                                                                x.IdOportunidadCosto == oportunidadCosto.IdOportunidadCosto).ToList();
            return lista.Select(x => new OportunidadCostoDtoResponse
            {
                IdOportunidadLineaNegocio = x.IdOportunidadLineaNegocio,
                IdCosto = x.IdCosto,
                IdOportunidadCosto = x.IdOportunidadCosto,
                IdTipoCosto = x.IdTipoCosto,
                Descripcion = x.Descripcion,
                Monto = x.Monto,
                VelocidadSubidaKBPS = x.VelocidadSubidaKBPS,
                PorcentajeGarantizado = x.PorcentajeGarantizado,
                PorcentajeSobresuscripcion = x.PorcentajeSobresuscripcion,
                CostoSegmentoSatelital = x.CostoSegmentoSatelital,
                InvAntenaHubUSD = x.InvAntenaHubUSD,
                AntenaCasaClienteUSD = x.AntenaCasaClienteUSD,
                Instalacion = x.Instalacion,
                IdUnidadConsumo = x.IdUnidadConsumo,
                IdTipificacion = x.IdTipificacion,
                CodigoModelo = x.CodigoModelo,
                Modelo = x.Modelo
            }).Single();
        }
        public ProcesoResponse RegistrarOportunidadCostoPorLineaNegocio(OportunidadCostoDtoRequest oportunidadCosto)
        {

            DateTime fecha = DateTime.Now;

            var valida = iOportunidadCostoDal.GetFiltered(x => x.IdOportunidadLineaNegocio == oportunidadCosto.IdOportunidadLineaNegocio &&
                                                              x.IdCosto == oportunidadCosto.IdCosto &&
                                                              x.IdEstado == oportunidadCosto.IdEstado).Any();
            if (!valida) { 
                var OportunidadCosto = new OportunidadCosto()
                {
                    IdOportunidadLineaNegocio = oportunidadCosto.IdOportunidadLineaNegocio,
                    IdCosto = oportunidadCosto.IdCosto,
                    IdOportunidadCosto = oportunidadCosto.IdOportunidadCosto,
                    IdTipoCosto = oportunidadCosto.IdTipoCosto,
                    Descripcion = oportunidadCosto.Descripcion,
                    Monto = oportunidadCosto.Monto,
                    VelocidadSubidaKBPS = oportunidadCosto.VelocidadSubidaKBPS,
                    PorcentajeGarantizado = oportunidadCosto.PorcentajeGarantizado,
                    PorcentajeSobresuscripcion = oportunidadCosto.PorcentajeSobresuscripcion,
                    CostoSegmentoSatelital = oportunidadCosto.CostoSegmentoSatelital,
                    InvAntenaHubUSD = oportunidadCosto.InvAntenaHubUSD,
                    AntenaCasaClienteUSD = oportunidadCosto.AntenaCasaClienteUSD,
                    Instalacion = oportunidadCosto.Instalacion,
                    IdUnidadConsumo = oportunidadCosto.IdUnidadConsumo,
                    IdTipificacion = oportunidadCosto.IdTipificacion,
                    CodigoModelo = oportunidadCosto.CodigoModelo,
                    Modelo = oportunidadCosto.Modelo,
                    IdEstado = oportunidadCosto.IdEstado,
                    FechaCreacion = fecha,
                    IdUsuarioCreacion = oportunidadCosto.IdUsuarioCreacion
                };
                iOportunidadCostoDal.Add(OportunidadCosto);
                iOportunidadCostoDal.UnitOfWork.Commit();

                respuesta.Id = OportunidadCosto.IdOportunidadCosto;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarOportunidadCosto;
            }
            else
            {
                var Costo = iOportunidadCostoDal.GetFiltered(x => x.IdOportunidadLineaNegocio == oportunidadCosto.IdOportunidadLineaNegocio &&
                                                             x.IdCosto == oportunidadCosto.IdCosto &&
                                                             x.IdEstado == oportunidadCosto.IdEstado).Single();
                respuesta.Id = Costo.IdOportunidadCosto;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarOportunidadCosto;
            }
            return respuesta;
        }

        public ProcesoResponse ActualizarOportunidadCostoPorLineaNegocio(OportunidadCostoDtoRequest oportunidadCosto)
        {
            DateTime fecha = DateTime.Now;

            if (oportunidadCosto.IdTipoCosto == Numeric.Uno)
            {

                var objOportunidadCosto = new OportunidadCosto()
                {
                    IdOportunidadCosto=oportunidadCosto.IdOportunidadCosto,
                    Monto = oportunidadCosto.Monto ,
                    Instalacion = oportunidadCosto.Instalacion,
                    CostoSegmentoSatelital = oportunidadCosto.CostoSegmentoSatelital,
                    IdUsuarioEdicion = oportunidadCosto.IdUsuarioEdicion,
                    FechaEdicion = fecha
                };
                iOportunidadCostoDal.ActualizarPorCampos(objOportunidadCosto, x => x.IdOportunidadCosto, x=>x.Instalacion,
                 x => x.CostoSegmentoSatelital, x => x.Monto, x => x.FechaEdicion, x => x.IdUsuarioEdicion);
                iOportunidadCostoDal.UnitOfWork.Commit();

                respuesta.Id = objOportunidadCosto.IdOportunidadCosto;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarOportunidadCosto;
            }

            if (oportunidadCosto.IdTipoCosto == Numeric.Dos || oportunidadCosto.IdTipoCosto == Numeric.Tres || oportunidadCosto.IdTipoCosto == Numeric.Cinco || oportunidadCosto.IdTipoCosto == Numeric.Seis)
            {
                var objOportunidadCosto = new OportunidadCosto()
                {
                    IdOportunidadCosto = oportunidadCosto.IdOportunidadCosto,
                    Monto = oportunidadCosto.Monto,
                    IdUsuarioEdicion=oportunidadCosto.IdUsuarioEdicion,
                    FechaEdicion= fecha                                   
                };
                iOportunidadCostoDal.ActualizarPorCampos(objOportunidadCosto, x => x.IdOportunidadCosto, x => x.Monto, x => x.FechaEdicion, x => x.IdUsuarioEdicion);
                iOportunidadCostoDal.UnitOfWork.Commit();

                respuesta.Id = objOportunidadCosto.IdOportunidadCosto;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarOportunidadCosto;

            }
            if (oportunidadCosto.IdTipoCosto ==Numeric.Siete)
            {
                var objOportunidadCosto = new OportunidadCosto()
                {
                    Monto =oportunidadCosto.PorcentajeGarantizado * oportunidadCosto.Monto,
                    PorcentajeGarantizado = oportunidadCosto.PorcentajeGarantizado,  
                    IdUsuarioEdicion = oportunidadCosto.IdUsuarioEdicion,
                    FechaEdicion = fecha
                };
                iOportunidadCostoDal.ActualizarPorCampos(objOportunidadCosto, x => x.PorcentajeGarantizado, 
                    x => x.FechaEdicion, x => x.IdUsuarioEdicion);
                iOportunidadCostoDal.UnitOfWork.Commit();

                respuesta.Id = objOportunidadCosto.IdOportunidadCosto;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarOportunidadCosto;


            }

            if (oportunidadCosto.IdTipoCosto == Numeric.Ocho)
            {
                var objOportunidadCosto = new OportunidadCosto()
                {

                    PorcentajeGarantizado = oportunidadCosto.PorcentajeGarantizado,
                    PorcentajeSobresuscripcion = oportunidadCosto.PorcentajeSobresuscripcion,
                    CostoSegmentoSatelital = oportunidadCosto.CostoSegmentoSatelital,
                    InvAntenaHubUSD = oportunidadCosto.InvAntenaHubUSD,
                    AntenaCasaClienteUSD = oportunidadCosto.AntenaCasaClienteUSD,
                    Instalacion = oportunidadCosto.Instalacion,
                    IdUsuarioEdicion = oportunidadCosto.IdUsuarioEdicion,
                    FechaEdicion = fecha
                };
                iOportunidadCostoDal.ActualizarPorCampos(objOportunidadCosto, x => x.PorcentajeGarantizado, x => x.PorcentajeSobresuscripcion, x => x.CostoSegmentoSatelital, x => x.InvAntenaHubUSD,
                     x => x.AntenaCasaClienteUSD, x => x.Instalacion,
                    x => x.FechaEdicion, x => x.IdUsuarioEdicion);
                iOportunidadCostoDal.UnitOfWork.Commit();

                respuesta.Id = objOportunidadCosto.IdOportunidadCosto;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarOportunidadCosto;


            }

            if (oportunidadCosto.IdTipoCosto == Numeric.Nuevo ||  oportunidadCosto.IdTipoCosto == Numeric.Diez)
            {
                var objOportunidadCosto = new OportunidadCosto()
                {

                    CostoSegmentoSatelital = oportunidadCosto.CostoSegmentoSatelital,
                    IdUsuarioEdicion = oportunidadCosto.IdUsuarioEdicion,
                    FechaEdicion = fecha
                };
                iOportunidadCostoDal.ActualizarPorCampos(objOportunidadCosto, x => x.CostoSegmentoSatelital,
                    x => x.FechaEdicion, x => x.IdUsuarioEdicion);
                iOportunidadCostoDal.UnitOfWork.Commit();

                respuesta.Id = objOportunidadCosto.IdOportunidadCosto;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarOportunidadCosto;


            }


            return respuesta;
        }
        public ProcesoResponse RegistrarOportunidadCosto(OportunidadCostoDtoRequest oportunidadCosto)
        {
            
            var valida = iOportunidadCostoDal.GetFilteredAsNoTracking(x => x.IdEstado == oportunidadCosto.IdEstado &&
                                                              x.IdOportunidadLineaNegocio == oportunidadCosto.IdOportunidadLineaNegocio &&
                                                              x.IdCosto == oportunidadCosto.IdCosto).Any();
            if (valida) {

                if (oportunidadCosto.IdOportunidadCosto != Numeric.Cero)
                {
                    var OportunidadCosto = new OportunidadCosto()
                    {
                        IdOportunidadLineaNegocio = oportunidadCosto.IdOportunidadLineaNegocio,
                        IdCosto = oportunidadCosto.IdCosto,
                        Monto = oportunidadCosto.Monto,
                        IdOportunidadCosto = oportunidadCosto.IdOportunidadCosto
                    };
                    iOportunidadCostoDal.ActualizarPorCampos(OportunidadCosto, x => x.IdOportunidadLineaNegocio, x => x.IdCosto, x => x.IdOportunidadCosto,x => x.Monto);
                    iOportunidadCostoDal.UnitOfWork.Commit();

                    respuesta.Id = OportunidadCosto.IdOportunidadCosto;
                    respuesta.TipoRespuesta = Proceso.Valido;
                }
                else
                {
                    var costo = iOportunidadCostoDal.GetFilteredAsNoTracking(x => x.IdEstado == oportunidadCosto.IdEstado &&
                                                              x.IdOportunidadLineaNegocio == oportunidadCosto.IdOportunidadLineaNegocio &&
                                                              x.IdCosto == oportunidadCosto.IdCosto).Single();

                    respuesta.Id = costo.IdOportunidadCosto;
                    respuesta.TipoRespuesta = Proceso.Valido;
                }
                
            }
            else
            {
                var OportunidadCosto = new OportunidadCosto()
                {
                    IdOportunidadLineaNegocio = oportunidadCosto.IdOportunidadLineaNegocio,
                    IdCosto = oportunidadCosto.IdCosto,
                    IdOportunidadCosto = oportunidadCosto.IdOportunidadCosto,
                    IdTipoCosto = oportunidadCosto.IdTipoCosto,
                    Descripcion = oportunidadCosto.Descripcion,
                    Monto = oportunidadCosto.Monto,
                    VelocidadSubidaKBPS = oportunidadCosto.VelocidadSubidaKBPS,
                    PorcentajeGarantizado = oportunidadCosto.PorcentajeGarantizado,
                    PorcentajeSobresuscripcion = oportunidadCosto.PorcentajeSobresuscripcion,
                    CostoSegmentoSatelital = oportunidadCosto.CostoSegmentoSatelital,
                    InvAntenaHubUSD = oportunidadCosto.InvAntenaHubUSD,
                    AntenaCasaClienteUSD = oportunidadCosto.AntenaCasaClienteUSD,
                    Instalacion = oportunidadCosto.Instalacion,
                    IdUnidadConsumo = oportunidadCosto.IdUnidadConsumo,
                    IdTipificacion = oportunidadCosto.IdTipificacion,
                    CodigoModelo = oportunidadCosto.CodigoModelo,
                    Modelo = oportunidadCosto.Modelo,
                    IdEstado = oportunidadCosto.IdEstado,
                    FechaCreacion = oportunidadCosto.FechaCreacion,
                    IdUsuarioCreacion = oportunidadCosto.IdUsuarioCreacion
                };
                iOportunidadCostoDal.Add(OportunidadCosto);
                iOportunidadCostoDal.UnitOfWork.Commit();

                respuesta.Id = OportunidadCosto.IdOportunidadCosto;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarOportunidadCosto;
            }
            return respuesta;
        }
    }
}
