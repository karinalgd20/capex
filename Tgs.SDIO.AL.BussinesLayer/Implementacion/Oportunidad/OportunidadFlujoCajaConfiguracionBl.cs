﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadFlujoCajaConfiguracionBl : IOportunidadFlujoCajaConfiguracionBl
    {
        readonly IOportunidadFlujoCajaConfiguracionDal iOportunidadFlujoCajaConfiguracionDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public OportunidadFlujoCajaConfiguracionBl(IOportunidadFlujoCajaConfiguracionDal IOportunidadFlujoCajaConfiguracionDal)
        {
            iOportunidadFlujoCajaConfiguracionDal = IOportunidadFlujoCajaConfiguracionDal;
        }

        public ProcesoResponse ActualizarOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            if (flujocaja.IdPeriodos == 1)
            {
                flujocaja.Meses = 1; //nos aseguramos que si el periodo es del tipo unica vez el mes se grabe como 1
            }

            var oportunidadFlujoCajaConfiguracion = new OportunidadFlujoCajaConfiguracion()
            {
                IdFlujoCaja = flujocaja.IdFlujoCaja,
                IdFlujoCajaConfiguracion = flujocaja.IdFlujoCajaConfiguracion,
                Ponderacion = flujocaja.Ponderacion,
                CostoPreOperativo = flujocaja.CostoPreOperativo,
                Inicio = flujocaja.Inicio,
                Meses = flujocaja.Meses,
                IdEstado = flujocaja.IdEstado,
                FechaEdicion = flujocaja.FechaEdicion,
                IdUsuarioEdicion = flujocaja.IdUsuarioEdicion
            };

            iOportunidadFlujoCajaConfiguracionDal.ActualizarPorCampos(oportunidadFlujoCajaConfiguracion, x => x.IdFlujoCaja, x => x.IdFlujoCajaConfiguracion, x => x.Ponderacion,
                                                                         x => x.CostoPreOperativo, x => x.Inicio, x => x.Meses, x => x.IdEstado, x => x.FechaEdicion, x=> x.IdUsuarioEdicion);
            iOportunidadFlujoCajaConfiguracionDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (oportunidadFlujoCajaConfiguracion.IdEstado == Estados.Inactivo) ? MensajesGeneralOportunidad.EliminarFlujoCajaConfiguracion : MensajesGeneralOportunidad.ActualizarFlujoCajaConfiguracion;


            return respuesta;
        }

        public List<OportunidadFlujoCajaConfiguracionDtoResponse> ListaOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            var OportunidadFlujoCajaConfiguracion = iOportunidadFlujoCajaConfiguracionDal.GetFilteredAsNoTracking(x => x.IdFlujoCaja == flujocaja.IdFlujoCaja &&
                                                                              x.IdEstado == flujocaja.IdEstado).ToList();
            return OportunidadFlujoCajaConfiguracion.Select(x => new OportunidadFlujoCajaConfiguracionDtoResponse
            {
                IdFlujoCaja = x.IdFlujoCaja,
                IdFlujoCajaConfiguracion = x.IdFlujoCajaConfiguracion,
                Ponderacion = x.Ponderacion,
                CostoPreOperativo = x.CostoPreOperativo,
                Inicio = x.Inicio,
                Meses = x.Meses
            }).ToList();
        }

        public List<OportunidadFlujoCajaConfiguracionDtoResponse> ListarOportunidadFlujoCajaConfiguracionBandeja(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaConfiguracionDal.ListarOportunidadFlujoCajaConfiguracionBandeja(flujocaja);
        }

        public OportunidadFlujoCajaConfiguracionDtoResponse ObtenerOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
           return iOportunidadFlujoCajaConfiguracionDal.ObtenerOportunidadFlujoCajaConfiguracion(flujocaja);
        }

        public ProcesoResponse RegistrarOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            var oportunidadFlujoCajaConfiguracion = new OportunidadFlujoCajaConfiguracion()
            {
                IdFlujoCaja = flujocaja.IdFlujoCaja, 
                Ponderacion = flujocaja.Ponderacion,
                CostoPreOperativo = flujocaja.CostoPreOperativo,
                Inicio = flujocaja.Inicio,
                Meses = flujocaja.Meses,
                IdEstado = flujocaja.IdEstado,
                FechaCreacion = flujocaja.FechaCreacion,
                IdUsuarioCreacion = flujocaja.IdUsuarioCreacion
            };

            iOportunidadFlujoCajaConfiguracionDal.Add(oportunidadFlujoCajaConfiguracion);
            iOportunidadFlujoCajaConfiguracionDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarFlujoCajaConfiguracion;
            respuesta.Id = oportunidadFlujoCajaConfiguracion.IdFlujoCajaConfiguracion;
            return respuesta;
        }

        public OportunidadFlujoCajaConfiguracionDtoResponse ObtenerOportunidadFlujoCajaConfiguracionPorId(OportunidadFlujoCajaConfiguracionDtoRequest flujocaja)
        {
            var OportunidadFlujoCajaConfiguracion = iOportunidadFlujoCajaConfiguracionDal.GetFilteredAsNoTracking(x => x.IdFlujoCaja == flujocaja.IdFlujoCaja );

            var valida = OportunidadFlujoCajaConfiguracion.Any();
            if (valida)
            {

                return OportunidadFlujoCajaConfiguracion.Select(x => new OportunidadFlujoCajaConfiguracionDtoResponse
                {
                    IdFlujoCaja = x.IdFlujoCaja,
                    IdFlujoCajaConfiguracion = x.IdFlujoCajaConfiguracion,
                    Ponderacion = x.Ponderacion,
                    CostoPreOperativo = x.CostoPreOperativo,
                    Inicio = x.Inicio,
                    IdEstado = x.IdEstado,
                    Meses = x.Meses
                }).Single();
            }
            else
            {
                return null;
            }


        }
    }
}
