﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadLineaNegocioBl : IOportunidadLineaNegocioBl
    {
        readonly IOportunidadLineaNegocioDal iOportunidadLineaNegocioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public OportunidadLineaNegocioBl(IOportunidadLineaNegocioDal IOportunidadLineaNegocioDal)
        {
            iOportunidadLineaNegocioDal = IOportunidadLineaNegocioDal;
        }

        public ProcesoResponse ActualizarOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)
        {
            DateTime fecha = DateTime.Now;
            var objOportunidadLineaNegocio = new OportunidadLineaNegocio()
            {
                IdOportunidadLineaNegocio = oportunidadLineaNegocio.IdOportunidadLineaNegocio,
                IdOportunidad = oportunidadLineaNegocio.IdOportunidad,
                IdUsuarioEdicion = oportunidadLineaNegocio.IdUsuarioEdicion,
                FechaEdicion = fecha,
                IdLineaNegocio = oportunidadLineaNegocio.IdLineaNegocio
            };

            iOportunidadLineaNegocioDal.ActualizarPorCampos(objOportunidadLineaNegocio, x => x.IdOportunidadLineaNegocio
            , x => x.IdUsuarioEdicion, x => x.FechaEdicion, x => x.IdLineaNegocio);

            iOportunidadLineaNegocioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            //respuesta.Mensaje = MensajesGeneralComun.ActualizarOportunidadLineaNegocio;

            return respuesta;
        }

        public ProcesoResponse InhabilitarOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)
        {
            var objOportunidadLineaNegocio = new OportunidadLineaNegocio()
            {
                IdOportunidadLineaNegocio = oportunidadLineaNegocio.IdOportunidadLineaNegocio,
                IdEstado = oportunidadLineaNegocio.IdEstado,
                IdUsuarioEdicion = oportunidadLineaNegocio.IdUsuarioEdicion,
                FechaEdicion = oportunidadLineaNegocio.FechaEdicion
            };

            iOportunidadLineaNegocioDal.ActualizarPorCampos(objOportunidadLineaNegocio, x => x.IdOportunidadLineaNegocio, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iOportunidadLineaNegocioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            //respuesta.Mensaje = (OportunidadLineaNegocio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarOportunidadLineaNegocio : MensajesGeneralComun.ActualizarOportunidadLineaNegocio;
            
            return respuesta;
        }
        
        public OportunidadLineaNegocioDtoResponse ObtenerOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)
        {
            var objOportunidadLineaNegocio = iOportunidadLineaNegocioDal.GetFiltered(x => x.IdOportunidad == oportunidadLineaNegocio.IdOportunidad);
            return objOportunidadLineaNegocio.Select(x => new OportunidadLineaNegocioDtoResponse
            {
                IdOportunidadLineaNegocio = x.IdOportunidadLineaNegocio,
                IdOportunidad = x.IdOportunidad,
                IdLineaNegocio = x.IdLineaNegocio

            }).Single();

        }

        public ProcesoResponse RegistrarOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)
        {
            DateTime fecha = DateTime.Now;
          

            
          
      

            var objLineaNegocio = iOportunidadLineaNegocioDal.GetFiltered(p => p.IdOportunidad == oportunidadLineaNegocio.IdOportunidad);


            var valida = objLineaNegocio.Any();
            if (valida)
            {
                var objResponse = objLineaNegocio.Select(x => new OportunidadLineaNegocioDtoResponse
                {
                    IdLineaNegocio = x.IdLineaNegocio
                }).OrderByDescending(t => t.IdOportunidad).ToList().First(); ;


                if (objResponse.IdLineaNegocio != oportunidadLineaNegocio.IdLineaNegocio)
                {
                    var objOportunidadLineaNegocio = new OportunidadLineaNegocio()
                    {
                        IdOportunidad = oportunidadLineaNegocio.IdOportunidad,
                        IdLineaNegocio = oportunidadLineaNegocio.IdLineaNegocio,
                        IdUsuarioCreacion = oportunidadLineaNegocio.IdUsuarioCreacion,
                        IdEstado = Estados.Activo,
                        FechaCreacion = fecha
                    };

                    iOportunidadLineaNegocioDal.Add(objOportunidadLineaNegocio);
                    iOportunidadLineaNegocioDal.UnitOfWork.Commit();
                }

            }
            else
            {
                var objOportunidadLineaNegocio = new OportunidadLineaNegocio()
                {
                    IdOportunidad = oportunidadLineaNegocio.IdOportunidad,
                    IdLineaNegocio = oportunidadLineaNegocio.IdLineaNegocio,
                    IdUsuarioCreacion = oportunidadLineaNegocio.IdUsuarioCreacion,
                    IdEstado = Estados.Activo,
                    FechaCreacion = fecha
                };

                iOportunidadLineaNegocioDal.Add(objOportunidadLineaNegocio);
                iOportunidadLineaNegocioDal.UnitOfWork.Commit();

                respuesta.Id = objOportunidadLineaNegocio.IdOportunidadLineaNegocio;

            }






            respuesta.TipoRespuesta = Proceso.Valido;

            return respuesta;
        }
        public List<OportunidadLineaNegocioDtoResponse> ObtenerTodasOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)
        {
            var objOportunidadLineaNegocio = iOportunidadLineaNegocioDal.GetFiltered(x => x.IdOportunidad == oportunidadLineaNegocio.IdOportunidad);
            return objOportunidadLineaNegocio.Select(x => new OportunidadLineaNegocioDtoResponse
            {
                IdOportunidadLineaNegocio = x.IdOportunidadLineaNegocio,
                IdOportunidad = x.IdOportunidad,
                IdLineaNegocio = x.IdLineaNegocio

            }).ToList();

        }


        public List<ListaDtoResponse> ListarLineaNegocioPorIdOportunidad(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)
        {
            return iOportunidadLineaNegocioDal.ListarLineaNegocioPorIdOportunidad(oportunidadLineaNegocio);
        }

        public OportunidadDtoResponse ObtenerOportunidadPorLineaNegocio(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio){

            return iOportunidadLineaNegocioDal.ObtenerOportunidadPorLineaNegocio(oportunidadLineaNegocio);

        }

        public ProcesoResponse ActualizarOportunidadLineaNegocioGo(OportunidadLineaNegocioDtoRequest oportunidadLineaNegocio)
        {
            DateTime fecha = DateTime.Now;
            var objOportunidadLineaNegocio = new OportunidadLineaNegocio()
            {
                IdOportunidadLineaNegocio = oportunidadLineaNegocio.IdOportunidadLineaNegocio,
                FlagGo = oportunidadLineaNegocio.FlagGo,
                IdUsuarioEdicion = oportunidadLineaNegocio.IdUsuarioEdicion,
                FechaEdicion = fecha
            };

            iOportunidadLineaNegocioDal.ActualizarPorCampos(objOportunidadLineaNegocio, x => x.FlagGo, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iOportunidadLineaNegocioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;

            return respuesta;
        }


    }
}
