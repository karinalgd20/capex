﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class SubServicioDatosCaratulaBl : ISubServicioDatosCaratulaBl
    {
        readonly ISubServicioDatosCaratulaDal iSubServicioDatosCaratulaDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SubServicioDatosCaratulaBl(ISubServicioDatosCaratulaDal ISubServicioDatosCaratulaDal)
        {
            iSubServicioDatosCaratulaDal = ISubServicioDatosCaratulaDal;
        }

        public ProcesoResponse ActualizarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {   
            var SubServicioDatosCaratula = new SubServicioDatosCaratula()
            {
                IdFlujoCaja = SubServicio.IdFlujoCaja,
                IdSubServicioDatosCaratula = SubServicio.IdSubServicioDatosCaratula,
                Circuito = SubServicio.Circuito,
                IdMedio = SubServicio.IdMedio,
                IdTipoEnlace = SubServicio.IdTipoEnlace,
                IdActivoPasivo = SubServicio.IdActivoPasivo,
                IdLocalidad = SubServicio.IdLocalidad,
                NumeroMeses = SubServicio.NumeroMeses,
                MontoUnitarioMensual = SubServicio.MontoUnitarioMensual,
                MontoTotalMensual = SubServicio.MontoTotalMensual,
                NumeroMesInicioGasto = SubServicio.NumeroMesInicioGasto,
                FlagRenovacion = SubServicio.FlagRenovacion,
                Instalacion = SubServicio.Instalacion,
                Desinstalacion = SubServicio.Desinstalacion,
                PU = SubServicio.PU,
                Alquiler = SubServicio.Alquiler,
                Factor = SubServicio.Factor,
                ValorCuota = SubServicio.ValorCuota,
                CC = SubServicio.CC,
                CCQProvincia = SubServicio.CCQProvincia,
                CCQBK = SubServicio.CCQBK,
                CCQCAPEX = SubServicio.CCQCAPEX,
                TIWS = SubServicio.TIWS,
                RADIO = SubServicio.RADIO,
                IdEstado = SubServicio.IdEstado,
                FechaEdicion = SubServicio.FechaEdicion,
                IdUsuarioEdicion = SubServicio.IdUsuarioEdicion
            };

            iSubServicioDatosCaratulaDal.ActualizarPorCampos(SubServicioDatosCaratula, x => x.IdFlujoCaja, x => x.IdSubServicioDatosCaratula, x => x.Circuito, x => x.IdMedio, x => x.IdTipoEnlace, x => x.IdActivoPasivo,
                x => x.IdLocalidad, x => x.NumeroMeses, x => x.MontoUnitarioMensual, x => x.MontoTotalMensual, x => x.NumeroMesInicioGasto, x => x.FlagRenovacion,
                x => x.Instalacion, x => x.Desinstalacion, x => x.PU, x => x.Alquiler, x => x.Factor, x => x.ValorCuota, x => x.CC, x => x.CCQProvincia, x => x.CCQBK, x => x.CCQCAPEX, x => x.TIWS, x => x.RADIO, x => x.IdEstado, x => x.FechaEdicion, x => x.IdUsuarioEdicion);
            iSubServicioDatosCaratulaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (SubServicio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarSubServicio : MensajesGeneralComun.ActualizarSubServicio;


            return respuesta;
        }

        public SubServicioDatosCaratulaDtoResponse ObtenerSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {
            var lista = iSubServicioDatosCaratulaDal.GetFilteredAsNoTracking(x => x.IdEstado == SubServicio.IdEstado &&
                                                                             x.IdSubServicioDatosCaratula == SubServicio.IdSubServicioDatosCaratula).ToList();
            return lista.Select(x => new SubServicioDatosCaratulaDtoResponse
            {
                IdFlujoCaja = x.IdFlujoCaja,
                IdSubServicioDatosCaratula = x.IdSubServicioDatosCaratula,
                Circuito = x.Circuito,
                IdMedio = x.IdMedio,
                IdTipoEnlace = x.IdTipoEnlace,
                IdActivoPasivo = x.IdActivoPasivo,
                IdLocalidad = x.IdLocalidad,
                NumeroMeses = x.NumeroMeses,
                MontoUnitarioMensual = x.MontoUnitarioMensual,
                MontoTotalMensual = x.MontoTotalMensual,
                NumeroMesInicioGasto = x.NumeroMesInicioGasto,
                FlagRenovacion = x.FlagRenovacion,
                Instalacion = x.Instalacion,
                Desinstalacion = x.Desinstalacion,
                PU = x.PU,
                Alquiler = x.Alquiler,
                Factor = x.Factor,
                ValorCuota = x.ValorCuota,
                CC = x.CC,
                CCQProvincia = x.CCQProvincia,
                CCQBK = x.CCQBK,
                CCQCAPEX = x.CCQCAPEX,
                TIWS = x.TIWS,
                RADIO = x.RADIO
            }).Single();
        }

        public List<SubServicioDatosCaratulaDtoResponse> ListarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {
            return iSubServicioDatosCaratulaDal.ListarSubServicioDatosCaratula(SubServicio);
        }

        public ProcesoResponse RegistrarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {

            if (SubServicio.IdSubServicioDatosCaratula != Numeric.Cero)
            {
                respuesta = ActualizarSubServicioDatosCaratula(SubServicio);
            }
            else
            {
                var objSubServicio = new SubServicioDatosCaratula()
                {
                    IdFlujoCaja = SubServicio.IdFlujoCaja,
                    IdSubServicioDatosCaratula = SubServicio.IdSubServicioDatosCaratula,
                    Circuito = SubServicio.Circuito,
                    IdMedio = SubServicio.IdMedio,
                    IdTipoEnlace = SubServicio.IdTipoEnlace,
                    IdActivoPasivo = SubServicio.IdActivoPasivo,
                    IdLocalidad = SubServicio.IdLocalidad,
                    NumeroMeses = SubServicio.NumeroMeses,
                    MontoUnitarioMensual = SubServicio.MontoUnitarioMensual,
                    MontoTotalMensual = SubServicio.MontoTotalMensual,
                    NumeroMesInicioGasto = SubServicio.NumeroMesInicioGasto,
                    FlagRenovacion = SubServicio.FlagRenovacion,
                    Instalacion = SubServicio.Instalacion,
                    Desinstalacion = SubServicio.Desinstalacion,
                    PU = SubServicio.PU,
                    Alquiler = SubServicio.Alquiler,
                    Factor = SubServicio.Factor,
                    ValorCuota = SubServicio.ValorCuota,
                    CC = SubServicio.CC,
                    CCQProvincia = SubServicio.CCQProvincia,
                    CCQBK = SubServicio.CCQBK,
                    CCQCAPEX = SubServicio.CCQCAPEX,
                    TIWS = SubServicio.TIWS,
                    RADIO = SubServicio.RADIO,
                    IdEstado = SubServicio.IdEstado,
                    FechaCreacion = SubServicio.FechaCreacion,
                    IdUsuarioCreacion = SubServicio.IdUsuarioCreacion
                };
                iSubServicioDatosCaratulaDal.Add(objSubServicio);
                iSubServicioDatosCaratulaDal.UnitOfWork.Commit();

                respuesta.Id = objSubServicio.IdSubServicioDatosCaratula;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarSubServicio;
            }

            return respuesta;
        }

        public ProcesoResponse EliminarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {
            throw new NotImplementedException();
        }
    }
}
