﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class LineaPestanaBl : ILineaPestanaBl
    {
        readonly ILineaPestanaDal iLineaPestanaDal;
        public LineaPestanaBl(ILineaPestanaDal ILineaPestanaDal)
        {
            iLineaPestanaDal = ILineaPestanaDal;
        }

        public List<LineaPestanaDtoResponse> ListarLineaPestana(LineaPestanaDtoRequest linea)
        {
            var objLinea = iLineaPestanaDal.GetFiltered(p => p.IdEstado == linea.IdEstado);

            return objLinea.Select(x => new LineaPestanaDtoResponse
            {
                IdLineaProducto = x.IdLineaNegocio,
                IdPestana = x.IdPestana,
                Descripcion = x.Descripcion,
                IdEstado = x.IdEstado
            }).ToList();
        }
    }
}
