﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Util.Funciones;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadFlujoCajaDetalleBl : IOportunidadFlujoCajaDetalleBl
    {
        readonly IOportunidadFlujoCajaDetalleDal iOportunidadFlujoCajaDetalleDal;
        readonly IOportunidadCalculadoDal iOportunidadCalculadoDal;
        readonly IOportunidadParametroBl iOportunidadParametroBl;
        readonly IOportunidadIndicadorFinancieroBl iOportunidadIndicadorFinancieroBl;
        readonly IOportunidadLineaNegocioBl iOportunidadLineaNegocioBl;
        ProcesoResponse respuesta = new ProcesoResponse();

        public OportunidadFlujoCajaDetalleBl(IOportunidadFlujoCajaDetalleDal IOportunidadFlujoCajaDetalleDal,
                                             IOportunidadCalculadoDal IOportunidadCalculadoDal,
                                             IOportunidadParametroBl IOportunidadParametroBl,
                                             IOportunidadIndicadorFinancieroBl IOportunidadIndicadorFinancieroBl,
                                             IOportunidadLineaNegocioBl IOportunidadLineaNegocioBl)
        {
            iOportunidadFlujoCajaDetalleDal = IOportunidadFlujoCajaDetalleDal;
            iOportunidadCalculadoDal = IOportunidadCalculadoDal;
            iOportunidadParametroBl = IOportunidadParametroBl;
            iOportunidadIndicadorFinancieroBl = IOportunidadIndicadorFinancieroBl;
            iOportunidadLineaNegocioBl = IOportunidadLineaNegocioBl;
        }

        public ProcesoResponse ActualizarOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalle)
        {
            var oportunidadFlujoCajaDetalle = new Entities.Entities.Oportunidad.OportunidadFlujoCajaDetalle()
            {
                IdFlujoCajaConfiguracion = detalle.IdFlujoCajaConfiguracion,
                Anio = detalle.Anio,
                Mes = detalle.Mes,
                Monto = detalle.Monto,
                IdEstado = detalle.IdEstado,
                IdUsuarioEdicion = detalle.IdUsuarioEdicion,
                FechaEdicion = detalle.FechaEdicion
            };

            iOportunidadFlujoCajaDetalleDal.ActualizarPorCampos(oportunidadFlujoCajaDetalle, x => x.IdFlujoCajaConfiguracion, x => x.Anio, x => x.Mes, x => x.Monto, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
            iOportunidadFlujoCajaDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (detalle.IdEstado == Estados.Inactivo) ? MensajesGeneralOportunidad.EliminarFlujoCajaDetalle : MensajesGeneralOportunidad.ActualizarFlujoCajaDetalle;

            return respuesta;
        }

        public OportunidadFlujoCajaDetalleDtoResponse GeneraProyectadoOportunidadFlujoCaja(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {
            var resultado = new OportunidadFlujoCajaDetalleDtoResponse();

            flujocaja.TipoFicha = TipoFicha.FichaContable;
            var fichaContable = iOportunidadFlujoCajaDetalleDal.GeneraProyectadoOportunidadFlujoCaja(flujocaja);

            var linea = new OportunidadLineaNegocioDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdEstado = flujocaja.IdEstado
            };
            var oportunidad = iOportunidadLineaNegocioBl.ObtenerOportunidadPorLineaNegocio(linea);

            var TiempoProyecto = oportunidad.TiempoProyecto;

            var TiempoImplantacion = oportunidad.TiempoImplantacion;

            OportunidadIndicadorFinancieroVNA(flujocaja, ConceptoFinanciero.Ingresos, IndicadoresFinancieros.VanIngresos, TiempoProyecto, TiempoImplantacion, 1);
            OportunidadIndicadorFinancieroVNA(flujocaja, ConceptoFinanciero.Inversion, IndicadoresFinancieros.VanInversiones, TiempoProyecto, TiempoImplantacion, 0);
            OportunidadIndicadorFinancieroVNA(flujocaja, ConceptoFinanciero.FlujoNeto, IndicadoresFinancieros.VanFlujoNeto, TiempoProyecto, TiempoImplantacion, 0);

            flujocaja.TipoFicha = TipoFicha.FichaContable;
            OportunidadIndicadorFinancieroVNA(flujocaja, ConceptoFinanciero.FlujoNeto, IndicadoresFinancieros.Van, TiempoProyecto, TiempoImplantacion,0);

            flujocaja.TipoFicha = TipoFicha.FichaFinanciero;

            FlujoNetoEntreIngresos(flujocaja, IndicadoresFinancieros.VanFlujoNeto, IndicadoresFinancieros.VanIngresos, IndicadoresFinancieros.VanFlujoNetoVanIngresos);
            FlujoNetoEntreIngresos(flujocaja, IndicadoresFinancieros.VanFlujoNeto, IndicadoresFinancieros.VanInversiones, IndicadoresFinancieros.VanFlujoNetoVanInversion);

            flujocaja.TipoFicha = TipoFicha.FichaFinanciero;
            PeriodoRecuperoFinanciero(flujocaja, TiempoProyecto, TiempoImplantacion);
            RegistrarTir(flujocaja, TiempoProyecto, TiempoImplantacion);
            CalculoGo(flujocaja);

            return resultado;
        }

        public void CalculoGo(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {
            var linea = new OportunidadLineaNegocioDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdEstado = flujocaja.IdEstado
            };
            var oportunidad = iOportunidadLineaNegocioBl.ObtenerOportunidadPorLineaNegocio(linea);

            var oportunidadIndicadorFinanciero1 = new OportunidadIndicadorFinancieroDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdIndicadorFinanciero = IndicadoresFinancieros.VanFlujoNetoVanIngresos,
                IdEstado = flujocaja.IdEstado,
                TipoFicha = flujocaja.TipoFicha.ToString()
            };
            var indicador1 = iOportunidadIndicadorFinancieroBl.ObtenerOportunidadIndicadorFinanciero(oportunidadIndicadorFinanciero1);

            var oportunidadIndicadorFinanciero2 = new OportunidadIndicadorFinancieroDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdIndicadorFinanciero = IndicadoresFinancieros.PeriodoRecuperoFinanciero,
                IdEstado = flujocaja.IdEstado,
                TipoFicha = flujocaja.TipoFicha.ToString()
            };
            var indicador2 = iOportunidadIndicadorFinancieroBl.ObtenerOportunidadIndicadorFinanciero(oportunidadIndicadorFinanciero2);

            if (indicador1 != null && indicador2 != null)
            {
                var A = (oportunidad.Periodo / 12);
                var B = indicador1.Monto;
                var C = indicador2.Monto;
                var D = oportunidad.TiempoImplantacion;
                string Resultado = string.Empty;
                switch (A)
                {
                    case 1: if (B > Convert.ToDecimal(ValoresGo.Valor1) && C < (ValoresGo.caso1 + D)) Resultado = "S"; break;
                    case 2: if (B > Convert.ToDecimal(ValoresGo.Valor1) && C < (ValoresGo.caso2 + D)) Resultado = "S"; break;
                    case 3: if (B > Convert.ToDecimal(ValoresGo.Valor1) && C < (ValoresGo.caso3 + D)) Resultado = "S"; break;
                    case 4: if (B > Convert.ToDecimal(ValoresGo.Valor1) && C < (ValoresGo.caso4 + D)) Resultado = "S"; break;
                    case 5: if (B > Convert.ToDecimal(ValoresGo.Valor1) && C < (ValoresGo.caso5 + D)) Resultado = "S"; break;
                    default: Resultado = "N"; break;
                };

                var oportunidadLinea = new OportunidadLineaNegocioDtoRequest()
                {
                    IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                    FlagGo = Resultado,
                    IdUsuarioEdicion = flujocaja.IdUsuarioEdicion
                };

                var resultado = iOportunidadLineaNegocioBl.ActualizarOportunidadLineaNegocioGo(oportunidadLinea);
            }
        }


        public void OportunidadIndicadorFinancieroVNA(OportunidadFlujoCajaDetalleDtoRequest flujocaja, int ConceptoFinanciero, int IndicadoresFinancieros, int? TiempoProyecto, int? TiempoImplantacion, int Indicador)
        {

            var oportunidadParametro = new OportunidadParametroDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio
            };

            var oportunidadCalculado = new OportunidadCalculadoDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdConceptoFinanciero = ConceptoFinanciero,
                IdEstado = flujocaja.IdEstado,
                TipoFicha = flujocaja.TipoFicha.ToString()
            };

            var TasaMensual = iOportunidadParametroBl.ObtenerParametroPorOportunidadLineaNegocio(oportunidadParametro).Where(x => x.IdParametro == Parametro.TasaDescuentoMensual).Single();

            var flujoNeto = iOportunidadCalculadoDal.ListaOportunidadCalculado(oportunidadCalculado);

            int iDuracionOportunidad = Convert.ToInt32(TiempoProyecto);
            decimal Monto1 = 0;
            if (Indicador == 1 && TiempoImplantacion > 0)
            {
                var dto = new OportunidadCalculadoDtoResponse()
                {
                    Monto = 0
                };

                for (int i = 0; i < TiempoImplantacion; i++)
                {
                    flujoNeto.Insert(i, dto);
                }
                Monto1 = flujoNeto[0].Monto; 
            }

            if (flujoNeto.Count > 0)
            {
                var VNA = ObtenerVNA(iDuracionOportunidad, TasaMensual.Valor, flujoNeto);
                

                var oportunidadIndicadorFinanciero = new OportunidadIndicadorFinancieroDtoRequest()
                {
                    IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                    IdIndicadorFinanciero = IndicadoresFinancieros,
                    Anio = flujocaja.Anio,
                    Monto = (Convert.ToDecimal(VNA) + Monto1),
                    IdEstado = flujocaja.IdEstado,
                    FechaCreacion = DateTime.Now,
                    IdUsuarioCreacion = flujocaja.IdUsuarioCreacion,
                    TipoFicha = flujocaja.TipoFicha.ToString()
                };
                var IndicadorFinanciero = iOportunidadIndicadorFinancieroBl.InsertarOportunidadIndicadorFinanciero(oportunidadIndicadorFinanciero);
            }
        }

        public void FlujoNetoEntreIngresos(OportunidadFlujoCajaDetalleDtoRequest flujocaja, int Indicador1, int Indicador2, int iIndicadorFinanciero)
        {
            var oportunidadIndicadorFinanciero1 = new OportunidadIndicadorFinancieroDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdIndicadorFinanciero = Indicador1,
                IdEstado = flujocaja.IdEstado,
                TipoFicha = flujocaja.TipoFicha.ToString()
            };
            var indicador1 = iOportunidadIndicadorFinancieroBl.ObtenerOportunidadIndicadorFinanciero(oportunidadIndicadorFinanciero1);

            var oportunidadIndicadorFinanciero2 = new OportunidadIndicadorFinancieroDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdIndicadorFinanciero = Indicador2,
                IdEstado = flujocaja.IdEstado,
                TipoFicha = flujocaja.TipoFicha.ToString()
            };
            var indicador2 = iOportunidadIndicadorFinancieroBl.ObtenerOportunidadIndicadorFinanciero(oportunidadIndicadorFinanciero2);

            if (indicador1 != null && indicador2 != null)
            {
                var oportunidadIndicadorFinanciero = new OportunidadIndicadorFinancieroDtoRequest()
                {
                    IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                    IdIndicadorFinanciero = iIndicadorFinanciero,
                    Anio = flujocaja.Anio,
                    Monto = (indicador2.Monto == 0 || indicador1.Monto == 0) ? 0 : (indicador1.Monto / indicador2.Monto),
                    IdEstado = flujocaja.IdEstado,
                    FechaCreacion = DateTime.Now,
                    IdUsuarioCreacion = flujocaja.IdUsuarioCreacion,
                    TipoFicha = flujocaja.TipoFicha.ToString()
                };
                var IndicadorFinanciero = iOportunidadIndicadorFinancieroBl.InsertarOportunidadIndicadorFinanciero(oportunidadIndicadorFinanciero);
            }
        }

        public void RegistrarOportunidadIndicador(OportunidadFlujoCajaDetalleDtoRequest flujocaja, int indicador1, int indicador2)
        {
            var oportunidadIndicadorFinanciero1 = new OportunidadIndicadorFinancieroDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdIndicadorFinanciero = indicador1,
                IdEstado = flujocaja.IdEstado,
                TipoFicha = flujocaja.TipoFicha.ToString()
            };
            var monto1 = iOportunidadIndicadorFinancieroBl.ObtenerOportunidadIndicadorFinanciero(oportunidadIndicadorFinanciero1);
            if (monto1 != null)
            {
                var oportunidadIndicadorFinanciero = new OportunidadIndicadorFinancieroDtoRequest()
                {
                    IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                    IdIndicadorFinanciero = indicador2,
                    Anio = flujocaja.Anio,
                    Monto = monto1.Monto,
                    IdEstado = flujocaja.IdEstado,
                    FechaCreacion = DateTime.Now,
                    IdUsuarioCreacion = flujocaja.IdUsuarioCreacion,
                    TipoFicha = flujocaja.TipoFicha.ToString()
                };
                var IndicadorFinanciero = iOportunidadIndicadorFinancieroBl.InsertarOportunidadIndicadorFinanciero(oportunidadIndicadorFinanciero);
            }
        }

        public void PeriodoRecuperoFinanciero(OportunidadFlujoCajaDetalleDtoRequest flujocaja, int? TiempoProyecto, int? TiempoImplantacion)
        {
            var oportunidadParametro = new OportunidadParametroDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio
            };

            var oportunidadCalculado = new OportunidadCalculadoDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdConceptoFinanciero = ConceptoFinanciero.FlujoNeto,
                IdEstado = flujocaja.IdEstado,
                TipoFicha = flujocaja.TipoFicha.ToString()
            };

            var TasaMensual = iOportunidadParametroBl.ObtenerParametroPorOportunidadLineaNegocio(oportunidadParametro).Where(x => x.IdParametro == Parametro.TasaDescuentoMensual).Single();

            var flujoNeto = iOportunidadCalculadoDal.ListaOportunidadCalculado(oportunidadCalculado);

            var dto = new OportunidadCalculadoDtoResponse()
            {
                Monto = 0
            };

            for (int i = 0; i < TiempoImplantacion; i++)
            {
                flujoNeto.Insert(i, dto);
            }

            int iDuracionOportunidad = Convert.ToInt32(TiempoProyecto);

            var Periodo = ObtenerPeriodo(Convert.ToDouble(TasaMensual.Valor), iDuracionOportunidad, flujoNeto);

            var oportunidadIndicadorFinanciero = new OportunidadIndicadorFinancieroDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdIndicadorFinanciero = IndicadoresFinancieros.PeriodoRecuperoFinanciero,
                Anio = flujocaja.Anio,
                Monto = Periodo,
                IdEstado = flujocaja.IdEstado,
                FechaCreacion = DateTime.Now,
                IdUsuarioCreacion = flujocaja.IdUsuarioCreacion,
                TipoFicha = flujocaja.TipoFicha.ToString()
            };

            var IndicadorFinanciero = iOportunidadIndicadorFinancieroBl.InsertarOportunidadIndicadorFinanciero(oportunidadIndicadorFinanciero);
        }

        public double ObtenerVNA(int iDuracionOportunidad, decimal TasaDescuentoMensual, List<OportunidadCalculadoDtoResponse> Lista)
        {
            var Formula = new Formulas();
            double[] Ingresos = new double[Lista.Count];

            for (int i = 1; i <= Lista.Count; i++)
            {
                Ingresos[i - 1] = Convert.ToDouble(Lista[i - 1].Monto);
            }

            double VNA = Formula.CalcularVANIngresos(Convert.ToDouble(TasaDescuentoMensual), Ingresos);

            return VNA;
        }

        public int ObtenerPeriodo(double TasaDescuentoMensual, int iDuracionOportunidad, List<OportunidadCalculadoDtoResponse> Lista)
        {
            var Formula = new Formulas();
            int PeriodoRecupero = 0;
            var neto = new List<double>();

            for (int i = 0; i < Lista.Count; i++)
            {
                neto.Add(Convert.ToDouble(Lista[i].Monto));
            }

            int iSumatoriaLinea4 = Formula.CalcularPeriodoRecupero(neto, iDuracionOportunidad, TasaDescuentoMensual);
            if (iSumatoriaLinea4 >= iDuracionOportunidad)
                PeriodoRecupero = 0;
            else
                PeriodoRecupero = iSumatoriaLinea4;

            return PeriodoRecupero;
        }

        public void RegistrarTir(OportunidadFlujoCajaDetalleDtoRequest flujocaja, int? TiempoProyecto, int? TiempoImplantacion)
        {
            var oportunidadParametro = new OportunidadParametroDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio
            };

            var oportunidadCalculado = new OportunidadCalculadoDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdConceptoFinanciero = ConceptoFinanciero.FlujoNeto,
                IdEstado = flujocaja.IdEstado,
                TipoFicha = flujocaja.TipoFicha.ToString()
            };

            var TasaMensual = iOportunidadParametroBl.ObtenerParametroPorOportunidadLineaNegocio(oportunidadParametro).Where(x => x.IdParametro == Parametro.TasaDescuentoMensual).Single();

            var flujoNeto = iOportunidadCalculadoDal.ListaOportunidadCalculado(oportunidadCalculado);
            int iDuracionOportunidad = Convert.ToInt32(TiempoProyecto);

            var dto = new OportunidadCalculadoDtoResponse()
            {
                Monto = 0
            };
            if (TiempoImplantacion == 0) TiempoImplantacion = 1;
            for (int i = 0; i < TiempoImplantacion; i++)
            {
                flujoNeto.Insert(i, dto);
            }

            var TIR = ObtenerTIR(iDuracionOportunidad, Convert.ToDouble(TasaMensual.Valor), flujoNeto);

            var oportunidadIndicadorFinanciero = new OportunidadIndicadorFinancieroDtoRequest()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdIndicadorFinanciero = IndicadoresFinancieros.Tir,
                Anio = flujocaja.Anio,
                Monto = Convert.ToDecimal(TIR) * 100,
                IdEstado = flujocaja.IdEstado,
                FechaCreacion = DateTime.Now,
                IdUsuarioCreacion = flujocaja.IdUsuarioCreacion,
                TipoFicha = flujocaja.TipoFicha.ToString()
            };

            var IndicadorFinanciero = iOportunidadIndicadorFinancieroBl.InsertarOportunidadIndicadorFinanciero(oportunidadIndicadorFinanciero);

            iOportunidadFlujoCajaDetalleDal.RegistrarTirAnual(flujocaja);

        }

        public double ObtenerTIR(int iDuracionOportunidad, double TasaDescuentoMensual, List<OportunidadCalculadoDtoResponse> Lista)
        {
            var Formula = new Formulas();
            double[] Ingresos = new double[Lista.Count];
            TasaDescuentoMensual = -0.1;
            for (int i = 0; i < Lista.Count; i++)
            {
                Ingresos[i] = Convert.ToDouble(Lista[i].Monto);
            }

            double TIR = Formula.CalculoTIR(TasaDescuentoMensual, Ingresos);

            return TIR;
        }

        public List<OportunidadFlujoCajaDetalleDtoResponse> ListaOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {
            throw new NotImplementedException();
        }

        public List<OportunidadFlujoCajaDetalleDtoResponse> ListarOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {
            throw new NotImplementedException();
        }


        public OportunidadFlujoCajaDetalleDtoResponse ObtenerOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {



            var objServicioCMI = iOportunidadFlujoCajaDetalleDal.GetFiltered(x => x.IdFlujoCajaConfiguracion == flujocaja.IdFlujoCajaConfiguracion);

            var valida = objServicioCMI.Any();
            if (valida)
            {
                return objServicioCMI.Select(x => new OportunidadFlujoCajaDetalleDtoResponse
                {
                    Anio = x.Anio,
                    Mes = x.Mes,
                    Monto = x.Monto,
                    IdUsuarioCreacion = x.IdUsuarioCreacion,
                    FechaCreacion = x.FechaCreacion,
                    IdEstado = x.IdEstado

                }).Single();
            }
            else
            {
                return null;
            }


        }
        public ProcesoResponse RegistrarOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalle)
        {
            var oportunidadFlujoCajaDetalle = new Entities.Entities.Oportunidad.OportunidadFlujoCajaDetalle()
            {
                IdFlujoCajaConfiguracion = detalle.IdFlujoCajaConfiguracion,
                Anio = detalle.Anio,
                Mes = detalle.Mes,
                Monto = detalle.Monto,
                IdEstado = detalle.IdEstado,
                IdUsuarioCreacion = detalle.IdUsuarioCreacion,
                FechaCreacion = detalle.FechaCreacion
            };

            iOportunidadFlujoCajaDetalleDal.Add(oportunidadFlujoCajaDetalle);
            iOportunidadFlujoCajaDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarFlujoCajaDetalle;

            return respuesta;
        }
    }
}
