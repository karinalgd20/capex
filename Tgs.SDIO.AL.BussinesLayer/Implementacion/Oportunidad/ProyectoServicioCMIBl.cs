﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class ProyectoServicioCMIBl : IProyectoServicioCMIBl
    {
        readonly IProyectoServicioCMIDal iProyectoServicioCMIDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ProyectoServicioCMIBl(IProyectoServicioCMIDal IProyectoServicioCMIDal)
        {
            iProyectoServicioCMIDal = IProyectoServicioCMIDal;
        }
        public ProcesoResponse ActualizarProyectoServicioCMI(ProyectoServicioCMIDtoRequest proyecto)
        {
            try
            {
                var objServicio = new ProyectoServicioCMI()
                {
                    IdProyecto = proyecto.IdProyecto,
                    IdLineaProducto = proyecto.IdLineaProducto,
                    IdServicioCMI = proyecto.IdServicioCMI,
                    Porcentaje = proyecto.Porcentaje,
                    IdAnalista = proyecto.IdAnalista,
                    IdEstado = proyecto.IdEstado,
                    IdUsuarioEdicion = proyecto.IdUsuarioEdicion,
                    FechaEdicion = proyecto.FechaEdicion
                };

                iProyectoServicioCMIDal.Modify(objServicio);
                iProyectoServicioCMIDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (proyecto.IdEstado == Estados.Inactivo) ? MensajesGeneralOportunidad.EliminarServicioCMI: MensajesGeneralOportunidad.ActualizarServicioCMI;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }       
        public List<ProyectoServicioCMIDtoResponse> ListarProyectoServicioCMI(ProyectoServicioCMIDtoRequest proyecto)
        {
            return iProyectoServicioCMIDal.ListarProyectoServicioCMI(proyecto);
        }       
        public ProcesoResponse RegistrarProyectoServicioCMI(ProyectoServicioCMIDtoRequest proyecto)
        {
            try
            {
                var objServicio = new ProyectoServicioCMI()
                {
                    IdProyecto = proyecto.IdProyecto,
                    IdLineaProducto = proyecto.IdLineaProducto,
                    IdServicioCMI = proyecto.IdServicioCMI,
                    Porcentaje = proyecto.Porcentaje,
                    IdAnalista = proyecto.IdAnalista,
                    IdEstado = proyecto.IdEstado,
                    IdUsuarioCreacion = proyecto.IdUsuarioCreacion,
                    FechaCreacion = proyecto.FechaCreacion
                };

                iProyectoServicioCMIDal.Add(objServicio);
                iProyectoServicioCMIDal.UnitOfWork.Commit();

                respuesta.Id = objServicio.IdProyecto;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarServicioCMI;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }

    }


}
