﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadBl : IOportunidadBl
    {
        readonly IOportunidadDal iOportunidadDal;
        readonly IOportunidadParametroBl iOportunidadParametroBl;
        readonly IOportunidadLineaNegocioBl iOportunidadLineaNegocioBl;
        readonly IOportunidadFlujoEstadoBl iOportunidadFlujoEstadoBl;
        readonly IOportunidadDocumentoBl iOportunidadDocumentoBl;
        readonly IOportunidadCostoBl iOportunidadCostoBl;
        readonly IOportunidadTipoCambioBl iOportunidadTipoCambioBl;
        readonly IOportunidadTipoCambioDetalleBl iOportunidadTipoCambioDetalleBl;
        readonly IOportunidadServicioCMIBl iOportunidadServicioCMIBl;
        readonly IOportunidadFlujoCajaBl iOportunidadFlujoCajaBl;
        readonly IOportunidadFlujoCajaConfiguracionBl iOportunidadFlujoCajaConfiguracionBl;
        readonly IOportunidadFlujoCajaDetalleBl iOportunidadFlujoCajaDetalleBl;

        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadBl(IOportunidadDal IOportunidadDal, IOportunidadLineaNegocioBl IOportunidadLineaNegocioBl,
            IOportunidadFlujoEstadoBl IOportunidadFlujoEstadoBl, IOportunidadDocumentoBl IOportunidadDocumentoBl,
            IOportunidadCostoBl IOportunidadCostoBl, IOportunidadTipoCambioBl IOportunidadTipoCambioBl,
            IOportunidadTipoCambioDetalleBl IOportunidadTipoCambioDetalleBl, IOportunidadServicioCMIBl IOportunidadServicioCMIBl,
            IOportunidadFlujoCajaBl IOportunidadFlujoCajaBl, IOportunidadFlujoCajaConfiguracionBl IOportunidadFlujoCajaConfiguracionBl,
            IOportunidadFlujoCajaDetalleBl IOportunidadFlujoCajaDetalleBl,IOportunidadParametroBl IOportunidadParametroBl)
        {
            iOportunidadDal = IOportunidadDal;
            iOportunidadParametroBl =IOportunidadParametroBl;
            iOportunidadLineaNegocioBl = IOportunidadLineaNegocioBl;
            iOportunidadFlujoEstadoBl = IOportunidadFlujoEstadoBl;
            iOportunidadDocumentoBl = IOportunidadDocumentoBl;
            iOportunidadCostoBl = IOportunidadCostoBl;
            iOportunidadTipoCambioBl = IOportunidadTipoCambioBl;
            iOportunidadTipoCambioDetalleBl = IOportunidadTipoCambioDetalleBl;
            iOportunidadServicioCMIBl = IOportunidadServicioCMIBl;
            iOportunidadFlujoCajaBl = IOportunidadFlujoCajaBl;
            iOportunidadFlujoCajaConfiguracionBl = IOportunidadFlujoCajaConfiguracionBl;
            iOportunidadFlujoCajaDetalleBl = IOportunidadFlujoCajaDetalleBl;
        }



        public ProcesoResponse ActualizarOportunidad(OportunidadDtoRequest oportunidad)
        {
            DateTime fecha = DateTime.Now;

            respuesta.GrabarAccion = Generales.EstadoLogico.Falso;


            if (oportunidad.IdEstado == Generales.Estados.EnProceso && oportunidad.CodPerfil != Generales.TipoPerfil.Coordinador_FIN && oportunidad.CodPerfil != Generales.TipoPerfil.Analista_FIN)
            {

                respuesta.GrabarAccion = Generales.EstadoLogico.Verdadero;

            }
            if (oportunidad.IdEstado == Generales.Estados.registrado && oportunidad.CodPerfil != Generales.TipoPerfil.Analista_FIN && oportunidad.CodPerfil != Generales.TipoPerfil.Preventa)
            {

                respuesta.GrabarAccion = Generales.EstadoLogico.Verdadero;
            }
            if (oportunidad.IdEstado == Generales.Estados.EvaluacionEconomica && oportunidad.CodPerfil != Generales.TipoPerfil.Preventa && oportunidad.CodPerfil != Generales.TipoPerfil.Coordinador_FIN)
            {
                respuesta.GrabarAccion = Generales.EstadoLogico.Verdadero;
            }
            if (oportunidad.IdEstado == Generales.Estados.Aprobado && oportunidad.CodPerfil != Generales.TipoPerfil.Coordinador_FIN & oportunidad.CodPerfil != Generales.TipoPerfil.Analista_FIN)
            {

                respuesta.GrabarAccion = Generales.EstadoLogico.Verdadero;

            }
            if (oportunidad.IdEstado == Generales.Estados.Rechazado && oportunidad.CodPerfil != Generales.TipoPerfil.Coordinador_FIN && oportunidad.CodPerfil != Generales.TipoPerfil.Analista_FIN)
            {
                respuesta.GrabarAccion = Generales.EstadoLogico.Verdadero;
            }


            if (oportunidad.IdEstado == Generales.Estados.Ganado || oportunidad.IdEstado == Generales.Estados.Rechazado)
                //|| oportunidad.IdEstado == Generales.Estados.Rechazado
                //|| oportunidad.IdEstado == Generales.Estados.Cancelado || oportunidad.IdEstado == Generales.Estados.RechazadoPorCliente
            {
                //var objOportunidad = new Entities.Entities.Oportunidad.Oportunidad()
                //{
                //    IdOportunidad = oportunidad.IdOportunidad,
                //    IdCliente = oportunidad.IdCliente,
                //    Descripcion = oportunidad.Descripcion,
                //    NumeroSalesForce = oportunidad.NumeroSalesForce,
                //    NumeroCaso = oportunidad.NumeroCaso,
                //    Alcance = oportunidad.Alcance,
                //    Periodo = oportunidad.Periodo,
                //    IdEstado = oportunidad.IdEstado,
                //    TiempoImplantacion = oportunidad.TiempoImplantacion,
                //    IdTipoServicio = oportunidad.IdTipoServicio,
                //    IdAnalistaFinanciero = oportunidad.IdAnalistaFinanciero,
                //    IdProductManager = oportunidad.IdProductManager,
                //    IdProyectoAnterior = oportunidad.IdProyectoAnterior,
                //    IdCoordinadorFinanciero = oportunidad.IdCoordinadorFinanciero,
                //    IdTipoProyecto = oportunidad.IdTipoProyecto,
                //    FechaEdicion = fecha,
                //    IdUsuarioEdicion = oportunidad.IdUsuarioEdicion,
                //    Fecha = oportunidad.Fecha,
                //    TiempoProyecto = oportunidad.Periodo + oportunidad.TiempoImplantacion
                //};

                //iOportunidadDal.ActualizarPorCampos(objOportunidad, x => x.IdOportunidad, x => x.IdCliente, x => x.Descripcion,
                //  x => x.NumeroSalesForce, x => x.NumeroCaso, x => x.Alcance, x => x.Periodo, x => x.IdEstado, x => x.TiempoImplantacion,
                //   x => x.IdTipoServicio, x => x.IdAnalistaFinanciero, x => x.IdProductManager, x => x.IdCoordinadorFinanciero,
                //    x => x.IdTipoProyecto, x => x.FechaEdicion, x => x.IdUsuarioEdicion, x => x.Fecha, x => x.TiempoProyecto,
                //    x => x.IdProyectoAnterior);

                //iOportunidadDal.UnitOfWork.Commit();

                //respuesta.TipoRespuesta = Proceso.Valido;
                //respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarOportunidad;


                var objOportunidad = new Entities.Entities.Oportunidad.Oportunidad()
                {

                 
                    IdOportunidad = oportunidad.IdOportunidad,
                    IdEstado = oportunidad.IdEstado,
                    FechaEdicion = fecha,
                    IdUsuarioEdicion = oportunidad.IdUsuarioEdicion,

                };

                iOportunidadDal.ActualizarPorCampos(objOportunidad, x => x.IdOportunidad, x => x.IdEstado,
                    x => x.FechaEdicion, x => x.IdUsuarioEdicion);

                iOportunidadDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarOportunidad;

            }
            else
            {
                var objOportunidad = new Entities.Entities.Oportunidad.Oportunidad()
                {
                    IdOportunidad = oportunidad.IdOportunidad,
                    IdCliente = oportunidad.IdCliente,
                    Descripcion = oportunidad.Descripcion,
                    NumeroSalesForce = oportunidad.NumeroSalesForce,
                    NumeroCaso = oportunidad.NumeroCaso,
                    Alcance = oportunidad.Alcance,
                    Periodo = oportunidad.Periodo,
                    IdEstado = oportunidad.IdEstado,
                    TiempoImplantacion = oportunidad.TiempoImplantacion,
                    IdTipoServicio = oportunidad.IdTipoServicio,
                    IdAnalistaFinanciero = oportunidad.IdAnalistaFinanciero,
                    IdProductManager = oportunidad.IdProductManager,
                    IdProyectoAnterior = oportunidad.IdProyectoAnterior,
                    IdCoordinadorFinanciero = oportunidad.IdCoordinadorFinanciero,
                    IdTipoProyecto = oportunidad.IdTipoProyecto,
                    FechaEdicion = fecha,
                    IdUsuarioEdicion = oportunidad.IdUsuarioEdicion,
                    Fecha = oportunidad.Fecha,
                    TiempoProyecto = oportunidad.Periodo + oportunidad.TiempoImplantacion
                };

                iOportunidadDal.ActualizarPorCampos(objOportunidad, x => x.IdOportunidad, x => x.IdCliente, x => x.Descripcion,
                  x => x.NumeroSalesForce, x => x.NumeroCaso, x => x.Alcance, x => x.Periodo, x => x.IdEstado, x => x.TiempoImplantacion,
                   x => x.IdTipoServicio, x => x.IdAnalistaFinanciero, x => x.IdProductManager, x => x.IdCoordinadorFinanciero,
                    x => x.IdTipoProyecto, x => x.FechaEdicion, x => x.IdUsuarioEdicion, x => x.Fecha, x => x.TiempoProyecto,
                    x => x.IdProyectoAnterior);

                iOportunidadDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Id = objOportunidad.IdOportunidad;
                respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarOportunidad;

            }





            return respuesta;
        }
        public List<OportunidadDtoResponse> ListarProyectadoOIBDA(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadDal.ListarProyectadoOIBDA(oportunidad);
        }
        public List<OportunidadDtoResponse> ListarOportunidad(OportunidadDtoRequest oportunidad)
        {
            throw new NotImplementedException();
        }
        public OportunidadPaginadoDtoResponse ListarOportunidadCabecera(OportunidadDtoRequest oportunidad)
        {
           // var lista = iOportunidadDal.ListarOportunidadCabecera(oportunidad);

            var oportunidadDtoResponse = new OportunidadPaginadoDtoResponse();
                                                                                        
            oportunidadDtoResponse.ListOportunidadPaginadoDtoResponse = iOportunidadDal.ListarOportunidadCabecera(oportunidad);

            if (oportunidadDtoResponse.ListOportunidadPaginadoDtoResponse.Count > Numeric.Cero)
            {
                oportunidadDtoResponse.TotalItemCount =  oportunidadDtoResponse.ListOportunidadPaginadoDtoResponse[0].TotalRow;

            }

            return oportunidadDtoResponse; 
        }
        public OportunidadDtoResponse ObtenerOportunidad(OportunidadDtoRequest oportunidad)
        {
            var objOportunidad = iOportunidadDal.GetFiltered(p => p.IdOportunidad == oportunidad.IdOportunidad);

            return objOportunidad.Select(x => new OportunidadDtoResponse
            {

                IdOportunidad = x.IdOportunidad,
                IdTipoEmpresa = x.IdTipoEmpresa,
                IdCliente = x.IdCliente,
                Descripcion = x.Descripcion,
                NumeroSalesForce = x.NumeroSalesForce,
                NumeroCaso = x.NumeroCaso,
                Alcance = x.Alcance,
                Periodo = x.Periodo,
                TiempoImplantacion = x.TiempoImplantacion,
                IdTipoServicio = x.IdTipoServicio,
                IdEstado = x.IdEstado,
                IdAnalistaFinanciero = x.IdAnalistaFinanciero,
                IdProductManager = x.IdProductManager,
                IdPreVenta = x.IdPreVenta,
                IdCoordinadorFinanciero = x.IdCoordinadorFinanciero,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                IdTipoProyecto = x.IdTipoProyecto,
                FechaCreacion = x.FechaCreacion,
                Fecha = x.Fecha,
                Version = x.Version,
                VersionPadre = x.VersionPadre,
                FlagGanador = x.FlagGanador,
                Agrupador = x.Agrupador,
                TiempoProyecto = x.TiempoProyecto,
                IdTipoCambio = x.IdTipoCambio,
                IdProyectoAnterior = x.IdProyectoAnterior

            }).Single();
        }
        public OportunidadDtoResponse ObtenerOportunidadId(OportunidadDtoRequest oportunidad)
        {
            var objOportunidad = iOportunidadDal.GetFiltered(p => p.IdOportunidad == oportunidad.IdOportunidad);

            return objOportunidad.Select(x => new OportunidadDtoResponse
            {
                IdOportunidad = x.IdOportunidad,
                Version = x.Version,
                IdCliente = x.IdCliente,
                Oportunidad = x.Descripcion,
                IdPreVenta = x.IdPreVenta,
                IdProductManager = x.IdProductManager,
                IdAnalistaFinanciero = x.IdAnalistaFinanciero,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                IdUsuarioModifica = x.IdUsuarioEdicion,
                IdEstado = x.IdEstado
            }).Single();
        }
        public ProcesoResponse OportunidadGanador(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadDal.OportunidadGanador(oportunidad);
        }
        public ProcesoResponse OportunidadInactivar(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadDal.OportunidadInactivar(oportunidad);
        }
        public ProcesoResponse RegistrarOportunidad(OportunidadDtoRequest oportunidad)
        {

            var Id = iOportunidadDal.OportunidadUltimoId();

 
         var objOportunidad = new Entities.Entities.Oportunidad.Oportunidad()
            {
                IdTipoEmpresa = Generales.Numeric.Uno,
                IdCliente = oportunidad.IdCliente,
                Descripcion = oportunidad.Descripcion,
                NumeroSalesForce = oportunidad.NumeroSalesForce,
                NumeroCaso = oportunidad.NumeroCaso,
                Alcance = oportunidad.Alcance,
                Periodo = oportunidad.Periodo,
                TiempoImplantacion = oportunidad.TiempoImplantacion,
                IdTipoServicio = oportunidad.IdTipoServicio,
                IdEstado = oportunidad.IdEstado,
                IdAnalistaFinanciero = oportunidad.IdAnalistaFinanciero,
                IdProductManager = oportunidad.IdProductManager,
                IdPreVenta = oportunidad.IdPreVenta,
                IdCoordinadorFinanciero = oportunidad.IdCoordinadorFinanciero,
                IdUsuarioCreacion = oportunidad.IdUsuarioCreacion,
                IdTipoProyecto = oportunidad.IdTipoProyecto,
                FechaCreacion = oportunidad.FechaCreacion,
                Fecha = oportunidad.Fecha,
                Version = Generales.Numeric.Uno,
                VersionPadre = Id + Generales.Numeric.Uno,
                FlagGanador = Generales.Numeric.Cero,
                Agrupador = Id + Generales.Numeric.Uno,
                TiempoProyecto = oportunidad.Periodo + oportunidad.TiempoImplantacion,
                IdTipoCambio = Generales.Numeric.Uno,
                IdProyectoAnterior = oportunidad.IdProyectoAnterior


            };

            iOportunidadDal.Add(objOportunidad);
            iOportunidadDal.UnitOfWork.Commit();


            respuesta.GrabarAccion = Generales.EstadoLogico.Falso;


            if (objOportunidad.IdEstado == Generales.Estados.EnProceso && oportunidad.CodPerfil != Generales.TipoPerfil.Coordinador_FIN && oportunidad.CodPerfil != Generales.TipoPerfil.Analista_FIN)
            {

                respuesta.GrabarAccion = Generales.EstadoLogico.Verdadero;

            }
            if (objOportunidad.IdEstado == Generales.Estados.registrado && oportunidad.CodPerfil != Generales.TipoPerfil.Analista_FIN && oportunidad.CodPerfil != Generales.TipoPerfil.Preventa)
            {

                respuesta.GrabarAccion = Generales.EstadoLogico.Verdadero;
            }
            if (objOportunidad.IdEstado == Generales.Estados.EvaluacionEconomica && oportunidad.CodPerfil != Generales.TipoPerfil.Preventa && oportunidad.CodPerfil != Generales.TipoPerfil.Coordinador_FIN)
            {
                respuesta.GrabarAccion = Generales.EstadoLogico.Verdadero;
            }
            if (objOportunidad.IdEstado == Generales.Estados.Aprobado && oportunidad.CodPerfil != Generales.TipoPerfil.Coordinador_FIN & oportunidad.CodPerfil != Generales.TipoPerfil.Analista_FIN)
            {

                respuesta.GrabarAccion = Generales.EstadoLogico.Verdadero;

            }
            if (objOportunidad.IdEstado == Generales.Estados.Rechazado && oportunidad.CodPerfil != Generales.TipoPerfil.Coordinador_FIN && oportunidad.CodPerfil != Generales.TipoPerfil.Analista_FIN)
            {
                respuesta.GrabarAccion = Generales.EstadoLogico.Verdadero;
            }


            respuesta.Id = objOportunidad.IdOportunidad;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarOportunidad;


            var objOportunidad_a = new Entities.Entities.Oportunidad.Oportunidad()
            {
                IdOportunidad = objOportunidad.IdOportunidad,
                Agrupador = objOportunidad.IdOportunidad,
                VersionPadre = objOportunidad.IdOportunidad
            };
            iOportunidadDal.ActualizarPorCampos(objOportunidad_a, x => x.IdOportunidad, x => x.Agrupador, x => x.VersionPadre);
            iOportunidadDal.UnitOfWork.Commit();
            return respuesta;
        }
  

        public ProcesoResponse RegistrarVersionOportunidad(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadDal.RegistrarVersionOportunidad(oportunidad);
        }
        public List<OportunidadDtoResponse> ObtenerVersiones(OportunidadDtoRequest oportunidad)
        {

            List<OportunidadDtoResponse> data = new List<OportunidadDtoResponse>();
            OportunidadDtoResponse obj = new OportunidadDtoResponse();

            var objOportunidad = iOportunidadDal.GetFiltered(p => p.IdOportunidad == oportunidad.IdOportunidad).Single();



            var dto = iOportunidadDal.GetFiltered(p => p.Agrupador == objOportunidad.Agrupador).OrderByDescending(t => t.Version).ToList();



            foreach (var part in dto)
            {
                data.Add(new OportunidadDtoResponse()
                {
                    IdOportunidad = part.IdOportunidad,
                    Version = part.Version,
                    Descripcion = part.Descripcion,
                    IdPreVenta = part.IdPreVenta,
                    IdProductManager = part.IdProductManager,
                    IdAnalistaFinanciero = part.IdAnalistaFinanciero,
                    IdCoordinadorFinanciero = part.IdCoordinadorFinanciero,
                    IdUsuarioCreacion = part.IdUsuarioCreacion,
                    IdUsuarioModifica = part.IdUsuarioEdicion,
                    IdEstado = part.IdEstado
                });

            }

            return data;
        }
        public ProcesoResponse NuevaVersion(OportunidadDtoRequest oportunidad)


        {

            DateTime fecha = DateTime.Now;
            OportunidadLineaNegocioDtoRequest oportunidadLineaNegocioRequest = new OportunidadLineaNegocioDtoRequest();
            OportunidadLineaNegocioDtoResponse oportunidadLineaNegocioResponse = new OportunidadLineaNegocioDtoResponse();


            OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado = new OportunidadFlujoEstadoDtoRequest();
            OportunidadDocumentoDtoRequest oportunidadDocumento = new OportunidadDocumentoDtoRequest();
            OportunidadTipoCambioDtoRequest oportunidadTipoCambio = new OportunidadTipoCambioDtoRequest();
            OportunidadTipoCambioDetalleDtoRequest oportunidadTipoCambioDetalle = new OportunidadTipoCambioDetalleDtoRequest();

            OportunidadParametroDtoRequest oportunidadParametro = new OportunidadParametroDtoRequest();

            oportunidad.FechaCreacion = fecha;
            //Oportunidad
            var objOportunidad = ObtenerOportunidad(oportunidad);


            var objOportunidadNuevaVersion = RegistrarOportunidadVersion(objOportunidad);
            //Flujo Estado
            oportunidadFlujoEstado.IdOportunidad = objOportunidadNuevaVersion.Id;
            oportunidadFlujoEstado.IdUsuarioCreacion = oportunidad.IdUsuarioCreacion;
            oportunidadFlujoEstado.IdEstado = Generales.Numeric.Uno;

            var responseOportunidadFlujoEstado = iOportunidadFlujoEstadoBl.RegistrarOportunidadFlujoEstado(oportunidadFlujoEstado);
            //

            //Linea Negocio
            oportunidadLineaNegocioRequest.IdOportunidad = objOportunidad.IdOportunidad;

            var objOportunidadLineaNegocio = iOportunidadLineaNegocioBl.ObtenerTodasOportunidadLineaNegocio(oportunidadLineaNegocioRequest);


            List<OportunidadLineaNegocioDtoRequest> ListaOportunidadLineaNegocioDtoRequest = new List<OportunidadLineaNegocioDtoRequest>();
            List<OportunidadLineaNegocioDtoResponse> ListaOportunidadLineaNegocioDtoResponseNuevo = new List<OportunidadLineaNegocioDtoResponse>();
            List<OportunidadParametroDtoRequest> ListOportunidadParametroDtoRequest = new List<OportunidadParametroDtoRequest>();
            List<OportunidadParametroDtoResponse> ListOportunidadParametroDtoResponse = new List<OportunidadParametroDtoResponse>();
            if (objOportunidadLineaNegocio!=null) {

                foreach (var part in objOportunidadLineaNegocio)
                {
                    var i = Numeric.Cero;


                    ListaOportunidadLineaNegocioDtoRequest.Add(new OportunidadLineaNegocioDtoRequest()
                    {
                        IdOportunidad = objOportunidadNuevaVersion.Id,
                        IdUsuarioCreacion = oportunidad.IdUsuarioCreacion,
                        IdLineaNegocio = part.IdLineaNegocio,
                        IdEstado = Numeric.Uno,
                        FechaCreacion = fecha

                    });


                    var objOportunidadLineaNuevaVersion = iOportunidadLineaNegocioBl.RegistrarOportunidadLineaNegocio(ListaOportunidadLineaNegocioDtoRequest[i]);
                    // lista de nuevas LineaNegocio

                    //OportunidadParametro

                    oportunidadParametro.IdOportunidadLineaNegocio = part.IdOportunidadLineaNegocio;

                    //inicial linea
                    oportunidadDocumento.IdOportunidadLineaNegocio = part.IdOportunidadLineaNegocio;
                    oportunidadTipoCambio.IdOportunidadLineaNegocio = part.IdOportunidadLineaNegocio;


                    ListOportunidadParametroDtoResponse = iOportunidadParametroBl.ObtenerParametroPorOportunidadLineaNegocio(oportunidadParametro);

                    var objOportunidadDocumento = iOportunidadDocumentoBl.ObtenerDocumentoPorOportunidadLineaNegocio(oportunidadDocumento);

                    var objOportunidadTipoCambio = iOportunidadTipoCambioBl.ObtenerTipoCambioPorOportunidadLineaNegocio(oportunidadTipoCambio);

                    //registrar

                    if (ListOportunidadParametroDtoResponse != null)
                    {
                        //Documento
                        var param = Numeric.Cero;

                        foreach (var partParametro in ListOportunidadParametroDtoResponse)
                        {
                            ListOportunidadParametroDtoRequest.Add(new OportunidadParametroDtoRequest()
                        {
                            IdOportunidadLineaNegocio = objOportunidadLineaNuevaVersion.Id,
                            IdParametro = partParametro.IdParametro, 
                            Valor = partParametro.Valor,
                            Valor2 = partParametro.Valor2,

                            IdUsuarioCreacion = oportunidad.IdUsuarioCreacion,                    
                            IdEstado = Generales.Numeric.Uno,
                            FechaCreacion = oportunidad.FechaCreacion

                        });
                            iOportunidadParametroBl.RegistrarOportunidadParametro(ListOportunidadParametroDtoRequest[param]);
                        }
                       
                        param ++;
                    }

                    if (objOportunidadDocumento != null)
                    {        
                    //Documento
                        oportunidadDocumento.IdOportunidadLineaNegocio = objOportunidadLineaNuevaVersion.Id;
                        oportunidadDocumento.IdEstado = Generales.Numeric.Uno;
                        oportunidadDocumento.IdUsuarioCreacion = oportunidad.IdUsuarioCreacion;
                        oportunidadDocumento.FechaCreacion = oportunidad.FechaCreacion;
                        oportunidadDocumento.TipoDocumento = objOportunidadDocumento.TipoDocumento;
                        oportunidadDocumento.Descripcion = objOportunidadDocumento.Descripcion;
                        oportunidadDocumento.IdFlujoCaja = objOportunidadDocumento.IdFlujoCaja;

                        var objOportunidadDocumentoNuevaVersion = iOportunidadDocumentoBl.RegistrarOportunidadDocumento(oportunidadDocumento);
                    }

                    //Tipo Cambio
                    if (objOportunidadTipoCambio != null)
                    {
                        oportunidadTipoCambio.IdOportunidadLineaNegocio = objOportunidadLineaNuevaVersion.Id;
                    oportunidadTipoCambio.IdEstado = Generales.Numeric.Uno;
                    oportunidadTipoCambio.IdUsuarioCreacion = oportunidad.IdUsuarioCreacion;
                    oportunidadTipoCambio.FechaCreacion = oportunidad.FechaCreacion;


                    var objOportunidadTipoCambioNuevaVersion = iOportunidadTipoCambioBl.RegistrarOportunidadTipoCambio(oportunidadTipoCambio);

                       
                            oportunidadTipoCambioDetalle.IdTipoCambioDetalle = objOportunidadTipoCambio.IdTipoCambioOportunidad;



                            var objOportunidadTipoCambioDetalle = iOportunidadTipoCambioDetalleBl.ObtenerTodasOportunidadTipoCambioDetalle(oportunidadTipoCambioDetalle);


                            if (objOportunidadTipoCambioDetalle != null)
                            {
                                List<OportunidadTipoCambioDetalleDtoRequest> ListaOportunidadTipoCambioDetalleDtoRequestNuevo = new List<OportunidadTipoCambioDetalleDtoRequest>();
                                List<OportunidadTipoCambioDetalleDtoResponse> ListaOportunidadTipoCambioDetalleDtoResponseNuevo = new List<OportunidadTipoCambioDetalleDtoResponse>();


                                foreach (var parte in objOportunidadTipoCambioDetalle)
                                {
                                    var y = Numeric.Cero;


                                    ListaOportunidadTipoCambioDetalleDtoRequestNuevo.Add(new OportunidadTipoCambioDetalleDtoRequest()
                                    {
                                        Monto = parte.Monto,
                                        Anio = parte.Anio,
                                        IdTipificacion = parte.IdTipificacion,
                                        IdMoneda = parte.IdMoneda,
                                        IdTipoCambioOportunidad = objOportunidadTipoCambioNuevaVersion.Id,
                                        IdUsuarioCreacion = oportunidad.IdUsuarioCreacion,
                                        IdEstado = Estados.Activo,
                                        FechaCreacion = oportunidad.FechaCreacion

                                    });
                                    var objOportunidadTipoCambioDetalleNuevaVersion = iOportunidadTipoCambioDetalleBl.RegistrarOportunidadTipoCambioDetalle(ListaOportunidadTipoCambioDetalleDtoRequestNuevo[y]);
                                    // var objOportunidadLineaNuevaVersion = iOportunidadLineaNegocioBl.RegistrarOportunidadLineaNegocio(ListaOportunidadTipoCambioDetalleDtoRequestNuevo[y]);
                                    // lista de nuevas LineaNegocio

                                    y++;

                                }
                            }
                       

                    }
                    //


                 

                    //Servicio CMI
                    OportunidadServicioCMIDtoRequest oportunidadServicioCMI = new OportunidadServicioCMIDtoRequest();
                    List<OportunidadServicioCMIDtoRequest> ListaOportunidadServicioCMIDtoRequestNuevo = new List<OportunidadServicioCMIDtoRequest>();
                    List<OportunidadServicioCMIDtoResponse> ListaOportunidadServicioCMIDtoResponseNuevo = new List<OportunidadServicioCMIDtoResponse>();


                    oportunidadServicioCMI.IdOportunidadLineaNegocio = part.IdOportunidadLineaNegocio;


                    var objOportunidadServicioCMI = iOportunidadServicioCMIBl.ObtenerTodosOportunidadServicioCMI(oportunidadServicioCMI);
                    if (objOportunidadServicioCMI != null)
                    {
                        foreach (var partCMI in objOportunidadServicioCMI)
                    {
                        var z = Numeric.Cero;


                        ListaOportunidadServicioCMIDtoRequestNuevo.Add(new OportunidadServicioCMIDtoRequest()
                        {

                            IdOportunidadLineaNegocio = objOportunidadLineaNuevaVersion.Id,
                            IdServicioCMI=partCMI.IdServicioCMI,
                            Porcentaje=partCMI.Porcentaje,
                            IdAnalista=partCMI.IdAnalista,
                            IdUsuarioCreacion = oportunidad.IdUsuarioCreacion,
                            IdEstado = Generales.Numeric.Uno,
                            FechaCreacion = oportunidad.FechaCreacion

                        });
                        var objOportunidadTipoCambioDetalleNuevaVersion = iOportunidadServicioCMIBl.RegistrarOportunidadServicioCMI(ListaOportunidadServicioCMIDtoRequestNuevo[z]);


                        z++;

                    }
                    }
                    OportunidadFlujoCajaDtoRequest oportunidadFlujoCaja = new OportunidadFlujoCajaDtoRequest();
                    List<OportunidadFlujoCajaDtoRequest> ListaOportunidadFlujoCajaDtoRequestNuevo = new List<OportunidadFlujoCajaDtoRequest>();
                    List<OportunidadFlujoCajaDtoResponse> ListaOportunidadFlujoCajaDtoResponseNuevo = new List<OportunidadFlujoCajaDtoResponse>();


                    oportunidadFlujoCaja.IdOportunidadLineaNegocio = part.IdOportunidadLineaNegocio;

                    var objOportunidadFlujoCaja = iOportunidadFlujoCajaBl.ObtenerTodosOportunidadFlujoCaja(oportunidadFlujoCaja);
                    if (objOportunidadFlujoCaja != null)
                    {


                        foreach (var partFlujoCaja in objOportunidadFlujoCaja)
                    {
                        var p = Numeric.Cero;

                           partFlujoCaja.IdServicio = (partFlujoCaja.IdServicio == Numeric.Cero) ? null: partFlujoCaja.IdServicio;
  

                            ListaOportunidadFlujoCajaDtoRequestNuevo.Add(new OportunidadFlujoCajaDtoRequest()
                            {

                                IdOportunidadLineaNegocio = objOportunidadLineaNuevaVersion.Id,
                                IdAgrupador = partFlujoCaja.IdAgrupador,
                                IdTipoCosto = partFlujoCaja.IdTipoCosto,
                                Descripcion = partFlujoCaja.Descripcion,
                                IdProveedor = partFlujoCaja.IdProveedor,
                                IdPeriodos = partFlujoCaja.IdPeriodos,
                                IdPestana = partFlujoCaja.IdPestana,
                                IdGrupo = partFlujoCaja.IdGrupo,
                                IdCasoNegocio = partFlujoCaja.IdCasoNegocio,
                                IdServicio = partFlujoCaja.IdServicio,
                                IdSubServicio = partFlujoCaja.IdSubServicio,
                                Cantidad=partFlujoCaja.Cantidad,
                                CostoUnitario=partFlujoCaja.CostoUnitario,
                                ContratoMarco=partFlujoCaja.ContratoMarco,
                                IdMoneda=partFlujoCaja.IdMoneda,
                                FlagSISEGO=partFlujoCaja.FlagSISEGO,
                                IdServicioCMI=partFlujoCaja.IdServicioCMI,
                                IdOportunidadCosto=partFlujoCaja.IdOportunidadCosto,
                                IdTipoSubServicio =  partFlujoCaja.IdTipoSubServicio,
                                IdUsuarioCreacion = oportunidad.IdUsuarioCreacion,
                                IdEstado = Generales.Numeric.Uno,
                                FechaCreacion = oportunidad.FechaCreacion

                            });


                            var objOportunidadFlujoCajaNuevaVersion = iOportunidadFlujoCajaBl.RegistrarOportunidadFlujoCaja(ListaOportunidadFlujoCajaDtoRequestNuevo[p]);

                        OportunidadFlujoCajaConfiguracionDtoRequest objOportunidadFlujoCajaConfiguracionRequest = new OportunidadFlujoCajaConfiguracionDtoRequest();

                        objOportunidadFlujoCajaConfiguracionRequest.IdFlujoCaja = partFlujoCaja.IdFlujoCaja;



                        var objOportunidadFlujoCajaConfiguracion = iOportunidadFlujoCajaConfiguracionBl.ObtenerOportunidadFlujoCajaConfiguracionPorId(objOportunidadFlujoCajaConfiguracionRequest);
                            if (objOportunidadFlujoCajaConfiguracion != null)
                            {
                                objOportunidadFlujoCajaConfiguracionRequest.IdFlujoCaja = objOportunidadFlujoCajaNuevaVersion.Id;
                        objOportunidadFlujoCajaConfiguracionRequest.Ponderacion = objOportunidadFlujoCajaConfiguracion.Ponderacion;
                        objOportunidadFlujoCajaConfiguracionRequest.CostoPreOperativo = objOportunidadFlujoCajaConfiguracion.CostoPreOperativo;
                        objOportunidadFlujoCajaConfiguracionRequest.Inicio = objOportunidadFlujoCajaConfiguracion.Inicio;
                        objOportunidadFlujoCajaConfiguracionRequest.Meses = objOportunidadFlujoCajaConfiguracion.Meses;
                        objOportunidadFlujoCajaConfiguracionRequest.IdEstado = objOportunidadFlujoCajaConfiguracion.IdEstado;
                        objOportunidadFlujoCajaConfiguracionRequest.FechaCreacion = oportunidad.FechaCreacion;
                        objOportunidadFlujoCajaConfiguracionRequest.IdUsuarioCreacion = oportunidad.IdUsuarioCreacion;


                        var objOportunidadFlujoCajaConfiguracionNuevaVersion = iOportunidadFlujoCajaConfiguracionBl.RegistrarOportunidadFlujoCajaConfiguracion(objOportunidadFlujoCajaConfiguracionRequest);
                                if (objOportunidadFlujoCajaConfiguracion != null)
                                {

                                    OportunidadFlujoCajaDetalleDtoRequest objOportunidadFlujoCajaDetalleRequest = new OportunidadFlujoCajaDetalleDtoRequest();
                                    objOportunidadFlujoCajaDetalleRequest.IdFlujoCajaConfiguracion = objOportunidadFlujoCajaConfiguracion.IdFlujoCajaConfiguracion;

                                    var objOportunidadFlujoCajaConfiguracionDetalle = iOportunidadFlujoCajaDetalleBl.ObtenerOportunidadFlujoCajaDetalle(objOportunidadFlujoCajaDetalleRequest);
                                       if (objOportunidadFlujoCajaConfiguracionDetalle != null)
                                    {
                                    objOportunidadFlujoCajaDetalleRequest.IdFlujoCajaConfiguracion = objOportunidadFlujoCajaConfiguracionNuevaVersion.Id;
                                    objOportunidadFlujoCajaDetalleRequest.Anio = objOportunidadFlujoCajaConfiguracionDetalle.Anio;
                                    objOportunidadFlujoCajaDetalleRequest.Mes = objOportunidadFlujoCajaConfiguracionDetalle.Mes;
                                    objOportunidadFlujoCajaDetalleRequest.Monto = objOportunidadFlujoCajaConfiguracionDetalle.Monto;
                                    objOportunidadFlujoCajaDetalleRequest.IdEstado = objOportunidadFlujoCajaConfiguracionDetalle.IdEstado;
                                    objOportunidadFlujoCajaDetalleRequest.IdUsuarioCreacion = oportunidad.IdUsuarioCreacion;
                                    objOportunidadFlujoCajaDetalleRequest.FechaCreacion = oportunidad.FechaCreacion;




                                    var objOportunidadFlujoCajaConfiguracionDetalleNuevaVersion = iOportunidadFlujoCajaDetalleBl.RegistrarOportunidadFlujoCajaDetalle(objOportunidadFlujoCajaDetalleRequest);



                                    }
                                }
                            }

                        

                        p++;

                    }

                    }




                    i++;

                }
            }

           

            respuesta.TipoRespuesta = Proceso.Valido;


            return respuesta;
        }

        public ProcesoResponse RegistrarOportunidadVersion(OportunidadDtoResponse oportunidad)
        {
            OportunidadDtoRequest objOport = new OportunidadDtoRequest();
            objOport.Agrupador = oportunidad.Agrupador;
            var Id = iOportunidadDal.OportunidadUltimoId();
            var version = iOportunidadDal.ObtenerNumeroVersionesOportunidad(objOport);
            var objOportunidad = new Entities.Entities.Oportunidad.Oportunidad()
            {
                IdTipoEmpresa = Generales.Numeric.Uno,
                IdCliente = oportunidad.IdCliente,
                Descripcion = oportunidad.Descripcion,
                NumeroSalesForce = oportunidad.NumeroSalesForce,
                NumeroCaso = oportunidad.NumeroCaso,
                Alcance = oportunidad.Alcance,
                Periodo = oportunidad.Periodo,
                TiempoImplantacion = oportunidad.TiempoImplantacion,
                IdTipoServicio = oportunidad.IdTipoServicio,
                IdEstado = Generales.Estados.Activo,
                IdAnalistaFinanciero = oportunidad.IdAnalistaFinanciero,
                IdProductManager = oportunidad.IdProductManager,
                IdPreVenta = oportunidad.IdPreVenta,
                IdCoordinadorFinanciero = oportunidad.IdCoordinadorFinanciero,
                IdUsuarioCreacion = oportunidad.IdUsuarioCreacion,
                IdTipoProyecto = oportunidad.IdTipoProyecto,
                FechaCreacion = oportunidad.FechaCreacion,
                Fecha = oportunidad.Fecha,
                Version = version.Versiones+Generales.Numeric.Uno, 
                FlagGanador = Generales.Numeric.Cero,
                Agrupador = oportunidad.Agrupador,
                TiempoProyecto = oportunidad.Periodo + oportunidad.TiempoImplantacion,
                IdTipoCambio = Generales.Numeric.Uno,
                IdProyectoAnterior = oportunidad.IdProyectoAnterior


            };


            iOportunidadDal.Add(objOportunidad);
            iOportunidadDal.UnitOfWork.Commit();

            respuesta.Id = objOportunidad.IdOportunidad;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarOportunidad;


            var objOportunidad_a = new Entities.Entities.Oportunidad.Oportunidad()
            {
                IdOportunidad = objOportunidad.IdOportunidad,
                VersionPadre = objOportunidad.IdOportunidad
            };


            iOportunidadDal.ActualizarPorCampos(objOportunidad_a, x => x.IdOportunidad, x => x.VersionPadre);

            iOportunidadDal.UnitOfWork.Commit();

            return respuesta;
        }



    }
}
