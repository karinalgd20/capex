﻿using System;

using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;
using System.Linq;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadParametroBl : IOportunidadParametroBl
    {
        readonly IOportunidadParametroDal iOportunidadParametroDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadParametroBl(IOportunidadParametroDal IOportunidadParametroDal)
        {
            iOportunidadParametroDal = IOportunidadParametroDal;
        }


        public ProcesoResponse ActualizarOportunidadParametro(OportunidadParametroDtoRequest oportunidadParametro)
        {

            DateTime fecha = DateTime.Now;

            var objOportunidadParametro = new OportunidadParametro()
            {
                IdOportunidadParametro = oportunidadParametro.IdOportunidadParametro,
                Valor=oportunidadParametro.Valor,
               // IdEstado = oportunidadParametro.IdEstado,
                IdUsuarioEdicion = oportunidadParametro.IdUsuarioEdicion,
                FechaEdicion = fecha,


            };


            iOportunidadParametroDal.ActualizarPorCampos(objOportunidadParametro, x => x.IdOportunidadParametro, x => x.Valor
            ,  x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iOportunidadParametroDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            // respuesta.Mensaje = MensajesGeneralComun.ActualizarOportunidadParametro;



            return respuesta;
        }

        public ProcesoResponse InhabilitarOportunidadParametro(OportunidadParametroDtoRequest oportunidadParametro)
        {

            DateTime fecha = DateTime.Now;

            var objOportunidadParametro = new OportunidadParametro()
            {
                IdOportunidadParametro = oportunidadParametro.IdOportunidadParametro,
                IdEstado = oportunidadParametro.IdEstado,
                IdUsuarioEdicion = oportunidadParametro.IdUsuarioEdicion,
                FechaEdicion = fecha
            };


            iOportunidadParametroDal.ActualizarPorCampos(objOportunidadParametro, x => x.IdOportunidadParametro, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iOportunidadParametroDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            //  respuesta.Mensaje = MensajesGeneralComun.EliminarOportunidadParametro;



            return respuesta;
        }

        public List<ListaDtoResponse> ListarOportunidadParametros(OportunidadParametroDtoRequest oportunidadParametro)
        {
            var query = iOportunidadParametroDal.GetFilteredAsNoTracking(x => x.IdEstado == oportunidadParametro.IdEstado
            && oportunidadParametro.IdEstado == oportunidadParametro.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdOportunidadParametro.ToString(),
                // Descripcion = x.Descripcion
            }).OrderBy(x => x.Descripcion).ToList();
        }

        public OportunidadParametroDtoResponse ObtenerOportunidadParametro(OportunidadParametroDtoRequest oportunidadParametro)
        {
            return iOportunidadParametroDal.ObtenerOportunidadParametro(oportunidadParametro);

        }
        public List<OportunidadParametroDtoResponse> ObtenerParametroPorOportunidadLineaNegocio(OportunidadParametroDtoRequest oportunidadParametro)
        {
            var objOportunidadParametro = iOportunidadParametroDal.GetFiltered(x => x.IdOportunidadLineaNegocio == oportunidadParametro.IdOportunidadLineaNegocio).ToList();
            var valida = objOportunidadParametro.Any();
            if (valida)
            {
                return objOportunidadParametro.Select(x => new OportunidadParametroDtoResponse
                {
                    IdOportunidadLineaNegocio = x.IdOportunidadLineaNegocio,
                    IdParametro = x.IdParametro,
                    Valor = x.Valor,
                    Valor2 = x.Valor2
                }).ToList();
            }
            else
            {
                return null;
            }

        }
        public OportunidadParametroPaginadoDtoResponse ListaOportunidadParametroPaginado(OportunidadParametroDtoRequest oportunidadParametro)
        {
            return iOportunidadParametroDal.ListaOportunidadParametroPaginado(oportunidadParametro);

           
        }
        public ProcesoResponse RegistrarOportunidadParametro(OportunidadParametroDtoRequest oportunidadParametro)
        {
            DateTime fecha = DateTime.Now;
            var objOportunidadParametro = new OportunidadParametro()
            {

                IdOportunidadLineaNegocio = oportunidadParametro.IdOportunidadLineaNegocio,
                IdParametro = oportunidadParametro.IdParametro,
                Valor = oportunidadParametro.Valor,
                Valor2 = oportunidadParametro.Valor2,
                IdEstado = Estados.Activo,
                IdUsuarioCreacion = oportunidadParametro.IdUsuarioCreacion,
                FechaCreacion = fecha
            };

            iOportunidadParametroDal.Add(objOportunidadParametro);
            iOportunidadParametroDal.UnitOfWork.Commit();


            respuesta.TipoRespuesta = Proceso.Valido;
            // respuesta.Mensaje = MensajesGeneralComun.RegistrarOportunidadParametro;
            respuesta.Id = objOportunidadParametro.IdOportunidadParametro;


            return respuesta;
        }
    }
}