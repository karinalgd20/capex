﻿using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class PlantillaImplantacionBl : IPlantillaImplantacionBl
    {
        ProcesoResponse respuesta = new ProcesoResponse();
        readonly IPlantillaImplantacionDal iPlantillaImplantacionDal;
        public PlantillaImplantacionBl(IPlantillaImplantacionDal plantillaImplantacionDal)
        {
            iPlantillaImplantacionDal = plantillaImplantacionDal;
        }

        #region - Transaccionales -
        public ProcesoResponse RegistrarPlantillaImplantacion(PlantillaImplantacionDtoRequest plantillaImplantacion)
        {
            var item = new PlantillaImplantacion()
            {
                IdOportunidad = plantillaImplantacion.IdOportunidad,
                IdTipoEnlaceCircuitoDatos = plantillaImplantacion.IdTipoEnlaceCircuitoDatos,
                IdMedioCircuitoDatos = plantillaImplantacion.IdMedioCircuitoDatos,
                IdServicioGrupoCircuitoDatos = plantillaImplantacion.IdServicioGrupoCircuitoDatos,
                IdTipoCircuitoDatos = plantillaImplantacion.IdTipoCircuitoDatos,
                NumeroCircuitoDatos = plantillaImplantacion.NumeroCircuitoDatos,
                IdCostoCircuitoDatos = plantillaImplantacion.IdCostoCircuitoDatos,
                ConectadoCircuitoDatos = plantillaImplantacion.ConectadoCircuitoDatos,
                LargaDistanciaNacionalCircuitoDatos = plantillaImplantacion.LargaDistanciaNacionalCircuitoDatos,
                IdVozTos5CaudalAntiguo = plantillaImplantacion.IdVozTos5CaudalAntiguo,
                IdVozTos4CaudalAntiguo = plantillaImplantacion.IdVozTos4CaudalAntiguo,
                IdVozTos3CaudalAntiguo = plantillaImplantacion.IdVozTos3CaudalAntiguo,
                IdVozTos2CaudalAntiguo = plantillaImplantacion.IdVozTos2CaudalAntiguo,
                IdVozTos1CaudalAntiguo = plantillaImplantacion.IdVozTos1CaudalAntiguo,
                IdVozTos0CaudalAntiguo = plantillaImplantacion.IdVozTos0CaudalAntiguo,
                UltimaMillaCircuitoDatos = plantillaImplantacion.UltimaMillaCircuitoDatos,
                VrfCircuitoDatos = plantillaImplantacion.VrfCircuitoDatos,
                EquipoCpeCircuitoDatos = plantillaImplantacion.EquipoCpeCircuitoDatos,
                EquipoTerminalCircuitoDatos = plantillaImplantacion.EquipoTerminalCircuitoDatos,
                IdAccionIsis = plantillaImplantacion.IdAccionIsis,
                IdTipoEnlaceServicioOfertado = plantillaImplantacion.IdTipoEnlaceServicioOfertado,
                IdMedioServicioOfertado = plantillaImplantacion.IdMedioServicioOfertado,
                IdServicioGrupoServicioOfertado = plantillaImplantacion.IdServicioGrupoServicioOfertado,
                IdCostoServicioOfertado = plantillaImplantacion.IdCostoServicioOfertado,
                IdTipoServicioOfertado = plantillaImplantacion.IdTipoServicioOfertado,
                NumeroServicioOfertado = plantillaImplantacion.NumeroServicioOfertado,
                LargaDistanciaNacionalServicioOfertado = plantillaImplantacion.LargaDistanciaNacionalServicioOfertado,
                IdVozTos5ServicioOfertado = plantillaImplantacion.IdVozTos5ServicioOfertado,
                IdVozTos4ServicioOfertado = plantillaImplantacion.IdVozTos4ServicioOfertado,
                IdVozTos3ServicioOfertado = plantillaImplantacion.IdVozTos3ServicioOfertado,
                IdVozTos2ServicioOfertado = plantillaImplantacion.IdVozTos2ServicioOfertado,
                IdVozTos1ServicioOfertado = plantillaImplantacion.IdVozTos1ServicioOfertado,
                IdVozTos0ServicioOfertado = plantillaImplantacion.IdVozTos0ServicioOfertado,
                UltimaMillaServicioOfertado = plantillaImplantacion.UltimaMillaServicioOfertado,
                VrfServicioOfertado = plantillaImplantacion.VrfServicioOfertado,
                EquipoCpeServicioOfertado = plantillaImplantacion.EquipoCpeServicioOfertado,
                ObservacionServicioOfertado = plantillaImplantacion.ObservacionServicioOfertado,
                EquipoTerminalServicioOfertado = plantillaImplantacion.EquipoTerminalServicioOfertado,
                GarantizadoBw = plantillaImplantacion.GarantizadoBw,
                Propiedad = plantillaImplantacion.Propiedad,
                RecursoTransporte = plantillaImplantacion.RecursoTransporte,
                RouterSwitchCentralTelefonica = plantillaImplantacion.RouterSwitchCentralTelefonica,
                ComponentesRouter = plantillaImplantacion.ComponentesRouter,
                TipoAntena = plantillaImplantacion.TipoAntena,
                SegmentoSatelital = plantillaImplantacion.SegmentoSatelital,
                CoordenadasUbicacion = plantillaImplantacion.CoordenadasUbicacion,
                PozoTierra = plantillaImplantacion.PozoTierra,
                Ups = plantillaImplantacion.Ups,
                EquipoTelefonico = plantillaImplantacion.EquipoTelefonico,
                IdEstado = plantillaImplantacion.IdEstado,
                IdUsuarioCreacion = plantillaImplantacion.IdUsuarioCreacion,
                FechaCreacion = plantillaImplantacion.FechaCreacion
            };

            iPlantillaImplantacionDal.Add(item);
            iPlantillaImplantacionDal.UnitOfWork.Commit();

            respuesta.Id = item.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;

        }
        public ProcesoResponse InactivarPlantillaImplantacion(PlantillaImplantacionDtoRequest plantillaImplantacion)
        {
            var item = iPlantillaImplantacionDal.Get(plantillaImplantacion.Id);
            item.IdEstado = plantillaImplantacion.IdEstado;
            item.IdUsuarioEdicion = plantillaImplantacion.IdUsuarioEdicion;
            item.FechaEdicion = plantillaImplantacion.FechaEdicion;

            iPlantillaImplantacionDal.Modify(item);
            iPlantillaImplantacionDal.UnitOfWork.Commit();

            respuesta.Id = item.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.InactivoRegistro;
            return respuesta;
        }
        #endregion

        #region - No Transaccionales -
        public PlantillaImplantacionPaginadoDtoResponse ListarPlantillaImplantacionPaginado(PlantillaImplantacionDtoRequest filtro)
        {
            return iPlantillaImplantacionDal.ListarPlantillaImplantacionPaginado(filtro);
        }
        #endregion
    }
}