﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadTipoCambioDetalleBl : IOportunidadTipoCambioDetalleBl
    {
        readonly IOportunidadTipoCambioDetalleDal iOportunidadTipoCambioDetalleDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadTipoCambioDetalleBl(IOportunidadTipoCambioDetalleDal IOportunidadTipoCambioDetalleDal)
        {
            iOportunidadTipoCambioDetalleDal = IOportunidadTipoCambioDetalleDal;
        }

        public ProcesoResponse ActualizarOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest OportunidadTipoCambioDetalle)
        {
            DateTime fecha = DateTime.Now;
            var objOportunidadTipoCambioDetalle = new OportunidadTipoCambioDetalle()
            {

                IdTipoCambioDetalle = OportunidadTipoCambioDetalle.IdTipoCambioDetalle,
                Anio= OportunidadTipoCambioDetalle.Anio,
                Monto= OportunidadTipoCambioDetalle.Monto,
                IdUsuarioEdicion = OportunidadTipoCambioDetalle.IdUsuarioEdicion,
                FechaEdicion = fecha
            };
            iOportunidadTipoCambioDetalleDal.ActualizarPorCampos(objOportunidadTipoCambioDetalle, x => x.IdTipoCambioDetalle, x => x.Anio, x => x.Monto, x => x.IdUsuarioEdicion, x => x.FechaEdicion);
            iOportunidadTipoCambioDetalleDal.UnitOfWork.Commit();


            respuesta.TipoRespuesta = Proceso.Valido;


            return respuesta;
        }
  

        public OportunidadTipoCambioDetalleDtoResponse ObtenerOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest OportunidadTipoCambioDetalle)
        {
            var objOportunidadTipoCambioDetalle = iOportunidadTipoCambioDetalleDal.GetFiltered(x => x.IdTipoCambioDetalle == OportunidadTipoCambioDetalle.IdTipoCambioDetalle &&
                                                       x.IdEstado == OportunidadTipoCambioDetalle.IdEstado);
            return objOportunidadTipoCambioDetalle.Select(x => new OportunidadTipoCambioDetalleDtoResponse
            {
                IdTipoCambioDetalle = x.IdTipoCambioDetalle,
                IdMoneda = x.IdMoneda,
                IdTipificacion = x.IdTipificacion,
                Anio = x.Anio,
                Monto = x.Monto,
                IdEstado = x.IdEstado
            }).Single();
        }

        public ProcesoResponse RegistrarOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest OportunidadTipoCambioDetalle)
        {

            var objOportunidadTipoCambioDetalle = new OportunidadTipoCambioDetalle()
            {

                IdEstado = OportunidadTipoCambioDetalle.IdEstado,
                IdUsuarioCreacion = OportunidadTipoCambioDetalle.IdUsuarioCreacion,
                FechaCreacion = Convert.ToDateTime(OportunidadTipoCambioDetalle.FechaCreacion)
            };

            iOportunidadTipoCambioDetalleDal.Add(objOportunidadTipoCambioDetalle);
            iOportunidadTipoCambioDetalleDal.UnitOfWork.Commit();

            respuesta.Id = objOportunidadTipoCambioDetalle.IdTipoCambioDetalle;
            respuesta.TipoRespuesta = Proceso.Valido;

            return respuesta;
        }

        public List<OportunidadTipoCambioDetalleDtoResponse> ObtenerTodasOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest OportunidadTipoCambioDetalle)
        {
            var objOportunidadTipoCambioDetalle = iOportunidadTipoCambioDetalleDal.GetFiltered(x => x.IdTipoCambioOportunidad == OportunidadTipoCambioDetalle.IdTipoCambioOportunidad &&
                                                       x.IdEstado == OportunidadTipoCambioDetalle.IdEstado);

            var valida = objOportunidadTipoCambioDetalle.Any();
            if (valida)
            {

                return objOportunidadTipoCambioDetalle.Select(x => new OportunidadTipoCambioDetalleDtoResponse
                {

                    IdMoneda = x.IdMoneda,
                    IdTipificacion = x.IdTipificacion,
                    Anio = x.Anio,
                    Monto = x.Monto
                }).ToList();

            }
            else
            {
                return null;
            }



        }
    }
}
