﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class VisitaDetalleBl: IVisitaDetalleBl
    {
        readonly IVisitaDetalleDal iVisitaDetalleDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public VisitaDetalleBl(IVisitaDetalleDal IVisitaDetalleDal)
        {
            iVisitaDetalleDal = IVisitaDetalleDal;
        }

        #region - Transaccionales -
        public ProcesoResponse RegistrarVisitaDetalle(VisitaDetalleDtoRequest visitaDetalle)
        {
            var entidad = new VisitaDetalle
            {
                IdVisita = visitaDetalle.IdVisita,
                FechaAcuerdo = visitaDetalle.FechaAcuerdo,
                DescripcionPunto = visitaDetalle.DescripcionPunto,
                Responsable = visitaDetalle.Responsable,
                Cumplimiento = visitaDetalle.Cumplimiento,
                IdEstado = visitaDetalle.IdEstado,
                IdUsuarioCreacion = visitaDetalle.IdUsuarioCreacion,
                FechaCreacion = visitaDetalle.FechaCreacion
            };
            iVisitaDetalleDal.Add(entidad);
            iVisitaDetalleDal.UnitOfWork.Commit();

            respuesta.Id = entidad.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarVisitaDetalle;
            return respuesta;
        }
        public ProcesoResponse InhabilitarVisitaDetalle(VisitaDetalleDtoRequest visitaDetalle)
        {
            var entidad = iVisitaDetalleDal.Get(visitaDetalle.Id);
            entidad.IdEstado = visitaDetalle.IdEstado;
            entidad.IdUsuarioEdicion = visitaDetalle.IdUsuarioEdicion;
            entidad.FechaEdicion = visitaDetalle.FechaEdicion;
            iVisitaDetalleDal.Modify(entidad);
            iVisitaDetalleDal.UnitOfWork.Commit();

            respuesta.Id = entidad.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.InhabilitarVisitaDetalle;
            return respuesta;
        }
        #endregion

        #region  - No Transaccionales -
        public VisitaDetallePaginadoDtoResponse ListarVisitaDetallePaginado(VisitaDetalleDtoRequest visitaDetalle)
        {
            return iVisitaDetalleDal.ListarVisitaDetallePaginado(visitaDetalle);
        }
        #endregion
    }
}
