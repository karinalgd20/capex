﻿using System;
using System.Linq;

using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Generales.Estados;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class SedeBl : ISedeBl
    {
        readonly ISedeDal iSedeDal;
        readonly ISedeInstalacionDal iSedeInstalacionDal;
        readonly ILogEstadoSisegoDal iLogEstadoSisegoDal;
        readonly IServicioSedeInstalacionDal iServicioSedeInstalacionDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SedeBl
            (
            ISedeDal sedeDal, 
            ISedeInstalacionDal isedeInstalacionDal, 
            ILogEstadoSisegoDal ilogEstadoSisegoDal, 
            IServicioSedeInstalacionDal servicioSedeInstalacionDal
            )
        {
            iSedeDal = sedeDal;
            iSedeInstalacionDal = isedeInstalacionDal;
            iLogEstadoSisegoDal = ilogEstadoSisegoDal;
            iServicioSedeInstalacionDal = servicioSedeInstalacionDal;
        }

        #region - Transaccionales -
        public ProcesoResponse RegistrarSede(SedeDtoRequest request)
        {
            OportunidadDtoRequest ob = new OportunidadDtoRequest();
            ob.IdOportunidad = request.IdOportunidad;

            var cliente = ObtenerClientePorIdOportunidad(ob);

            var Ubigeo = ObtenerCodUbigeo(request);

            var sede = new Sede()
            {
                IdTipoEnlace = request.IdTipoEnlace,
                Codigo = request.Codigo,
                IdCliente = cliente.IdCliente,
                IdTipoSede = request.IdTipoSede,
                IdAccesoCliente = request.IdAccesoCliente,
                IdTendidoExterno = request.IdTendidoExterno,
                NumeroPisos = request.NumeroPisos,
                IdUbigeo = Ubigeo.Id,
                IdTipoVia = request.IdTipoVia,
                Latitud = request.Latitud,
                Longitud = request.Longitud,
                Direccion = request.Direccion,
                IdTipoServicio = request.IdTipoServicio,
                Piso = request.Piso,
                Interior = request.Interior,
                Manzana = request.Manzana,
                Lote = request.Lote,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = request.IdUsuarioCreacion,
                FechaCreacion = DateTime.Now,
            };

            iSedeDal.Add(sede);
            iSedeDal.UnitOfWork.Commit();

            var sedeInstalacion = new SedeInstalacion()
            {
                IdSede = sede.Id,
                IdOportunidad = request.IdOportunidad,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = request.IdUsuarioCreacion,
                FechaCreacion = DateTime.Now,
                DesRequerimiento = request.DesRequerimiento,
                FlgCotizacionReferencial = request.FlgCotizacionReferencial,
                FlgRequisitosEspeciales = request.FlgRequisitosEspeciales,
            };


            iSedeInstalacionDal.Add(sedeInstalacion);
            iSedeInstalacionDal.UnitOfWork.Commit();


            var logEstadoSisego = new LogEstadoSisego()
            {
                Descripcion = "Sin Asignar",
                IdSedeInstalacion = sedeInstalacion.Id,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = request.IdUsuarioCreacion,
                FechaCreacion = DateTime.Now,
            };

            iLogEstadoSisegoDal.Add(logEstadoSisego);
            iLogEstadoSisegoDal.UnitOfWork.Commit();


            respuesta.Id = sede.Id;
            respuesta.IdDetalle = sedeInstalacion.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;

        }
        public ProcesoResponse ActualizarSede(SedeDtoRequest request)
        {
            var obj = new OportunidadDtoRequest();

            obj.IdOportunidad = request.IdOportunidad;

            var Cliente = ObtenerClientePorIdOportunidad(obj);
            var Ubigeo = ObtenerCodUbigeo(request);
            var areaAntigua = ObtenerSedePorId(request);
            var areaSeguimiento = new Sede()
            {
                Id = request.Id,
                IdTipoEnlace = request.IdTipoEnlace,
                IdCliente = Cliente.IdCliente,
                Codigo = request.Codigo,
                IdTipoSede = request.IdTipoSede,
                IdAccesoCliente = request.IdAccesoCliente,
                IdTendidoExterno = request.IdTendidoExterno,
                NumeroPisos = request.NumeroPisos,
                IdUbigeo = Ubigeo.Id,
                IdTipoVia = request.IdTipoVia,
                Latitud = request.Latitud,
                Longitud = request.Longitud,
                Direccion = request.Direccion,
                IdTipoServicio = request.IdTipoServicio,
                Piso = request.Piso,
                Interior = request.Interior,
                Manzana = request.Manzana,
                Lote = request.Lote,
                IdEstado = request.IdEstado,
                IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                FechaCreacion = areaAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuarioEdicion,
                FechaEdicion = DateTime.Now,
            };
            iSedeDal.Modify(areaSeguimiento);
            iSedeDal.UnitOfWork.Commit();

            var areSedeInsta = new SedeInstalacion()
            {
                Id = request.IdSedeInstalacion,
                IdSede = request.Id,
                IdOportunidad = request.IdOportunidad,
                IdEstado = Estados.Activo,
                IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                FechaCreacion = areaAntigua.FechaCreacion,

                DesRequerimiento = request.DesRequerimiento,
                FlgCotizacionReferencial = request.FlgCotizacionReferencial,
                FlgRequisitosEspeciales = request.FlgRequisitosEspeciales,
                IdUsuarioEdicion = request.IdUsuarioEdicion,
                FechaEdicion = DateTime.Now,
            };

            iSedeInstalacionDal.Modify(areSedeInsta);
            iSedeInstalacionDal.UnitOfWork.Commit();
            respuesta.Id = request.Id;
            respuesta.IdDetalle = request.IdSedeInstalacion;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;
            return respuesta;
        }
        public ProcesoResponse EliminarSede(SedeDtoRequest request)
        {
            var areaAntigua = ObtenerSedePorId(request);
            var areaAntiguaSede = ObtenerSedeInstalacionPorId(request);

            var areaSedeInsta = new SedeInstalacion()
            {
                Id = request.IdSedeInstalacion,
                IdSede = request.IdSede,
                IdOportunidad = areaAntiguaSede.IdOportunidad,
                CodSisego = areaAntigua.CodSisego,
                IdEstado = EstadoSedeInstalacion.Eliminado,
                IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                FechaCreacion = areaAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuarioEdicion,
                FechaEdicion = DateTime.Now,
                DesRequerimiento = areaAntigua.DesRequerimiento,
                FlgCotizacionReferencial = areaAntigua.FlgCotizacionReferencial,
                FlgRequisitosEspeciales = areaAntigua.FlgRequisitosEspeciales
            };

            iSedeInstalacionDal.Modify(areaSedeInsta);
            iSedeInstalacionDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;
            return respuesta;
        }
        public ProcesoResponse AgregarSedeInstalacion(SedeDtoRequest request)
        {
            foreach (var sede in request.ListSedeSelect)
            {
                var sedeInstalada = obtenerSedeInstalada(Convert.ToInt32(sede.id), request.IdOportunidad);

                if (sedeInstalada == null)
                {
                    var sedeInstalacion = new SedeInstalacion()
                    {
                        IdSede = Convert.ToInt32(sede.id),
                        IdOportunidad = request.IdOportunidad,
                        IdEstado = Estados.Activo,
                        IdUsuarioCreacion = request.IdUsuarioCreacion,
                        FechaCreacion = DateTime.Now,
                        //DesRequerimiento = request.DesRequerimiento,
                        //FlgCotizacionReferencial = request.FlgCotizacionReferencial,
                        //FlgRequisitosEspeciales = request.FlgRequisitosEspeciales,
                    };
                    iSedeInstalacionDal.Add(sedeInstalacion);
                    iSedeInstalacionDal.UnitOfWork.Commit();

                    var logEstadoSisego = new LogEstadoSisego()
                    {
                        Descripcion = LogEstados.Estado1,
                        IdSedeInstalacion = sedeInstalacion.Id,
                        IdEstado = Estados.Activo,
                        IdUsuarioCreacion = request.IdUsuarioCreacion,
                        FechaCreacion = DateTime.Now,
                    };
                    iLogEstadoSisegoDal.Add(logEstadoSisego);
                    iLogEstadoSisegoDal.UnitOfWork.Commit();
                }
            }

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;
            return respuesta;
        }
        public ProcesoResponse EliminarServicios(ServicioSedeInstalacionDtoRequest request)
        {
            var areaAntigua = ObtenerServicioSedeInstalacionPorId(request);

            var areaSedeInsta = new ServicioSedeInstalacion()
            {
                Id = request.IdServicioSedeInstalacion,
                IdSedeInstalacion = areaAntigua.IdSedeInstalacion,
                IdFlujoCaja = areaAntigua.IdFlujoCaja,
                IdEstado = EstadoSedeInstalacion.Eliminado,
                IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                FechaCreacion = areaAntigua.FechaCreacion,
                IdUsuarioEdicion = request.IdUsuarioEdicion,
                FechaEdicion = DateTime.Now,
            };

            iServicioSedeInstalacionDal.Remove(areaSedeInsta);
            iServicioSedeInstalacionDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;
            return respuesta;
        }
        #endregion

        #region - No Transaccionales -
        public SedePaginadoDtoResponse ListarSedeInstalacion(OportunidadDtoRequest request)
        {
            var sedeDtoResponse = new SedePaginadoDtoResponse();

            sedeDtoResponse.ListSedeDto = iSedeDal.ListarSedeInstalacion(request);

            if (sedeDtoResponse.ListSedeDto.Count > Numeric.Cero)
            {
                sedeDtoResponse.TotalItemCount = sedeDtoResponse.ListSedeDto[0].TotalRow;
            }


            return sedeDtoResponse;
        }
        public SedePaginadoDtoResponse ListarSedeCliente(OportunidadDtoRequest request)
        {
            return iSedeDal.ListarSedeCliente(request);
        }
        public ClienteDtoResponse ObtenerClientePorIdOportunidad(OportunidadDtoRequest request)
        {
            return iSedeDal.ObtenerClientePorIdOportunidad(request);
        }
        public SedeDtoResponse ObtenerCodUbigeo(SedeDtoRequest request)
        {
            return iSedeDal.ObtenerCodUbigeo(request);
        }
        public SedeInstalacionDtoResponse obtenerSedeInstalada(int IdSede, int IdOportunidad)
        {
            return iSedeDal.obtenerSedeInstalada(IdSede, IdOportunidad);
        }
        public SedeDtoResponse ObtenerSedePorId(SedeDtoRequest request)
        {
            return iSedeDal.ObtenerSedePorId(request);
        }
        public ServicioSedeInstalacionDtoResponse ObtenerServicioSedeInstalacionPorId(ServicioSedeInstalacionDtoRequest request)
        {
            return iSedeDal.ObtenerServicioSedeInstalacionPorId(request);
        }
        public SedeInstalacionDtoResponse ObtenerSedeInstalacionPorId(SedeDtoRequest request)
        {
            var query = iSedeInstalacionDal.GetFilteredAsNoTracking(x => x.IdSede == request.IdSede && (x.IdEstado == Activo || x.IdEstado == Inactivo)).ToList();
            return query.Select(x => new SedeInstalacionDtoResponse
            {
                Id = x.Id,
                IdSede = x.IdSede,
                IdOportunidad = x.IdOportunidad,
                CodSisego = x.CodSisego,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion
            }).FirstOrDefault();
        }
        public UbigeoDtoResponse ObtenerCodDetalleUbigeo(int Id)
        {
            return iSedeDal.ObtenerCodDetalleUbigeo(Id);
        }
        public SedePaginadoDtoResponse ListarSedeServiciosPaginado(SedeDtoRequest request)
        {
            return iSedeDal.ListarSedeServiciosPaginado(request);
        }
        public SedeDtoResponse DireccionBuscar(SedeDtoRequest request)
        {
            return iSedeDal.DireccionBuscar(request);
        }
        public SedeDtoResponse DetalleSedePorId(SedeDtoRequest request)
        {
            return iSedeDal.DetalleSedePorId(request);
        }
        #endregion
    }
}
