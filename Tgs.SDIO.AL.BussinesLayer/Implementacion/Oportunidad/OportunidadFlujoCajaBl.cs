﻿using System.Collections.Generic;
using System.Linq;

using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using System;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadFlujoCajaBl : IOportunidadFlujoCajaBl
    {
        readonly IOportunidadFlujoCajaDal iOportunidadFlujoCajaDal;
        readonly IPestanaGrupoBl iPestanaGrupoBl;
        readonly IOportunidadFlujoCajaConfiguracionBl iOportunidadFlujoCajaConfiguracionBl;   
        readonly ISubServicioDatosCapexBl iSubServicioDatosCapexBl;
        readonly ISubServicioDatosCaratulaBl iSubServicioDatosCaratulaBl;
        readonly IOportunidadServicioCMIBl iOportunidadServicioCMIBl;
        readonly IServicioBl iServicioBl;
        readonly IServicioSubServicioBl iServicioSubServicioBl;
        readonly ISedeInstalacionDal iSedeInstalacionDal;
        readonly IServicioSedeInstalacionDal iServicioSedeInstalacionDal;
        readonly IServicioSubServicioDal iServicioSubServicioDal;
        readonly ISubServicioDatosCapexDal iSubServicioDatosCapexDal;
        readonly ISubServicioDatosCaratulaDal iSubServicioDatosCaratulaDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadFlujoCajaBl(
            IOportunidadFlujoCajaDal IOportunidadFlujoCajaDal, 
            IOportunidadFlujoCajaConfiguracionBl IOportunidadFlujoCajaConfiguracionBl,
            ISubServicioDatosCapexBl ISubServicioDatosCapexBl, 
            ISubServicioDatosCaratulaBl ISubServicioDatosCaratulaBl, 
            IPestanaGrupoBl IPestanaGrupoBl,
            IOportunidadServicioCMIBl IOportunidadServicioCMIBl, 
            IServicioBl IServicioBl,
            ISedeInstalacionDal ISedeInstalacionDal,
            IServicioSedeInstalacionDal IServicioSedeInstalacionDal,
            IServicioSubServicioBl IServicioSubServicioBl,
            IServicioSubServicioDal IServicioSubServicioDal,
            ISubServicioDatosCapexDal ISubServicioDatosCapexDal,
            ISubServicioDatosCaratulaDal ISubServicioDatosCaratulaDal)
     
        {
            iOportunidadFlujoCajaDal = IOportunidadFlujoCajaDal;
            iPestanaGrupoBl = IPestanaGrupoBl;
            iOportunidadServicioCMIBl = IOportunidadServicioCMIBl;
            iServicioBl = IServicioBl;
            iSedeInstalacionDal = ISedeInstalacionDal;
            iOportunidadFlujoCajaConfiguracionBl = IOportunidadFlujoCajaConfiguracionBl;
            iSubServicioDatosCaratulaBl = ISubServicioDatosCaratulaBl;
            iSubServicioDatosCapexBl = ISubServicioDatosCapexBl;
            iServicioSedeInstalacionDal = IServicioSedeInstalacionDal;
            iServicioSubServicioBl = IServicioSubServicioBl;
            iServicioSubServicioDal = IServicioSubServicioDal;
            iSubServicioDatosCapexDal = ISubServicioDatosCapexDal;
            iSubServicioDatosCaratulaDal = ISubServicioDatosCaratulaDal;
        }

        #region - Transaccionales -
        public ProcesoResponse RegistrarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {

            var oportunidadFlujoCaja = new OportunidadFlujoCaja()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdAgrupador = flujocaja.IdAgrupador,
                IdTipoCosto = flujocaja.IdTipoCosto,
                Descripcion = flujocaja.Descripcion,
                IdProveedor = flujocaja.IdProveedor,
                IdPeriodos = flujocaja.IdPeriodos,
                IdPestana = flujocaja.IdPestana,
                IdGrupo = flujocaja.IdGrupo,
                IdCasoNegocio = flujocaja.IdCasoNegocio,
                IdServicio = flujocaja.IdServicio,
                Cantidad = flujocaja.Cantidad,
                CostoUnitario = flujocaja.CostoUnitario,
                ContratoMarco = flujocaja.ContratoMarco,
                IdMoneda = flujocaja.IdMoneda,
                FlagSISEGO = flujocaja.FlagSISEGO,
                IdServicioCMI = flujocaja.IdServicioCMI,
                IdEstado = flujocaja.IdEstado,
                IdUsuarioCreacion = flujocaja.IdUsuarioCreacion,
                FechaCreacion = flujocaja.FechaCreacion,
                IdTipoSubServicio = flujocaja.IdTipoSubServicio ,

                IdOportunidadCosto=flujocaja.IdOportunidadCosto,
                IdSubServicio= flujocaja.IdSubServicio
            };

            iOportunidadFlujoCajaDal.Add(oportunidadFlujoCaja);
            iOportunidadFlujoCajaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarFlujoCaja;
            respuesta.Id = oportunidadFlujoCaja.IdFlujoCaja;
            return respuesta;
        }
        public ProcesoResponse ActualizarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var oportunidadFlujoCaja = new OportunidadFlujoCaja()
            {
                IdFlujoCaja = flujocaja.IdFlujoCaja,
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                Cantidad = flujocaja.Cantidad,
                CostoUnitario = flujocaja.CostoUnitario,
                IdOportunidadCosto = flujocaja.IdOportunidadCosto,
                IdEstado = flujocaja.IdEstado,
                FechaEdicion = flujocaja.FechaEdicion,
                IdUsuarioEdicion = flujocaja.IdUsuarioEdicion,
                IdTipoSubServicio = flujocaja.IdTipoSubServicio

            };

            iOportunidadFlujoCajaDal.ActualizarPorCampos(oportunidadFlujoCaja, x => x.IdFlujoCaja, x => x.IdOportunidadLineaNegocio, x => x.Cantidad, x => x.CostoUnitario,
                x => x.IdOportunidadCosto, x => x.IdEstado, x => x.FechaEdicion, x => x.IdUsuarioEdicion, x => x.IdTipoSubServicio);
            iOportunidadFlujoCajaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (oportunidadFlujoCaja.IdEstado == Estados.Inactivo) ? MensajesGeneralOportunidad.EliminarFlujoCaja : MensajesGeneralOportunidad.ActualizarFlujoCaja;

            return respuesta;
        }
        public ProcesoResponse ActualizarPeriodoOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var oportunidadFlujoCaja = new OportunidadFlujoCaja()
            {
                IdFlujoCaja = flujocaja.IdFlujoCaja,
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdPeriodos = flujocaja.IdPeriodos,
                IdEstado = flujocaja.IdEstado,
                FechaEdicion = flujocaja.FechaEdicion,
                IdUsuarioEdicion = flujocaja.IdUsuarioEdicion,
                IdMoneda=flujocaja.IdMoneda

            };

            iOportunidadFlujoCajaDal.ActualizarPorCampos(oportunidadFlujoCaja, x => x.IdFlujoCaja, x => x.IdOportunidadLineaNegocio, 
                x => x.IdPeriodos, x => x.IdEstado, x => x.FechaEdicion, x => x.IdUsuarioEdicion,x=>x.IdMoneda);
            iOportunidadFlujoCajaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarFlujoCaja;

            return respuesta;
        }
        public ProcesoResponse ActualizarSubServicioConfiguracionAdicional(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var oportunidadFlujoCaja = new OportunidadFlujoCaja()
            {
                IdFlujoCaja = flujocaja.IdFlujoCaja,
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdPeriodos = flujocaja.IdPeriodos,
                IdEstado = flujocaja.IdEstado,
                FechaEdicion = flujocaja.FechaEdicion,
                IdUsuarioEdicion = flujocaja.IdUsuarioEdicion

            };

            iOportunidadFlujoCajaDal.ActualizarPorCampos(oportunidadFlujoCaja, x => x.IdFlujoCaja, x => x.IdOportunidadLineaNegocio, x => x.IdPeriodos, x => x.IdEstado, x => x.FechaEdicion, x => x.IdUsuarioEdicion);
            iOportunidadFlujoCajaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarFlujoCaja;

            return respuesta;
        }
        public ProcesoResponse EliminarCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaDal.EliminarCasoNegocio(casonegocio);
        }
        public ProcesoResponse InhabilitarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {


            var lista = iSubServicioDatosCaratulaDal.GetFilteredAsNoTracking(x => x.IdSubServicioDatosCaratula == flujocaja.IdSubServicioDatosCaratula).ToList();
            var objFlujoCaja = lista.Select(x => new OportunidadFlujoCajaDtoResponse
            {
                IdFlujoCaja = x.IdFlujoCaja

            }).Single();

            var oportunidadFlujoCaja = new OportunidadFlujoCaja()
            {
                IdFlujoCaja = objFlujoCaja.IdFlujoCaja,
                IdEstado = Generales.Estados.Inactivo

            };

            iOportunidadFlujoCajaDal.ActualizarPorCampos(oportunidadFlujoCaja, x => x.IdFlujoCaja, x => x.IdEstado, x => x.FechaEdicion, x => x.IdUsuarioEdicion);
            iOportunidadFlujoCajaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.EliminarFlujoCaja;

            return respuesta;
        }
        public ProcesoResponse InhabilitarOportunidadEcapex(OportunidadFlujoCajaDtoRequest flujocaja)
        {

       var lista = iSubServicioDatosCapexDal.GetFilteredAsNoTracking(x => x.IdSubServicioDatosCapex == flujocaja.IdSubServicioDatosCapex).ToList();
           var objFlujoCaja=  lista.Select(x => new OportunidadFlujoCajaDtoResponse
            {
                IdFlujoCaja = x.IdFlujoCaja
             
            }).Single();

            var oportunidadFlujoCaja = new OportunidadFlujoCaja()
            {
                IdFlujoCaja = objFlujoCaja.IdFlujoCaja,
                IdEstado = Generales.Estados.Inactivo

            };

            iOportunidadFlujoCajaDal.ActualizarPorCampos(oportunidadFlujoCaja, x => x.IdFlujoCaja, x => x.IdEstado, x => x.FechaEdicion, x => x.IdUsuarioEdicion);
            iOportunidadFlujoCajaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.EliminarFlujoCaja;

            return respuesta;
        }
        #endregion

        #region - No Transaccionales -
        public List<OportunidadFlujoCajaDtoResponse> ListaOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var lista = iOportunidadFlujoCajaDal.GetFilteredAsNoTracking(x => x.IdOportunidadLineaNegocio == flujocaja.IdOportunidadLineaNegocio &&
                                                                              x.IdEstado == Estados.Activo).ToList();
            return lista.Select(x => new OportunidadFlujoCajaDtoResponse
            {
                IdAgrupador = x.IdAgrupador,
                IdTipoCosto = x.IdTipoCosto,
                Descripcion = x.Descripcion,
                IdPeriodos = x.IdPeriodos,
                IdPestana = x.IdPestana,
                IdGrupo = x.IdGrupo
            }).ToList();
        }
        public List<ListaDtoResponse> ListarPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var lista = iOportunidadFlujoCajaDal.ListarPorIdFlujoCaja(flujocaja);
            return lista;
        }
        public List<ListaDtoResponse> ListaServicioPorOportunidadLineaNegocio(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaDal.ListaServicioPorOportunidadLineaNegocio(flujocaja);
        }
        public List<OportunidadFlujoCajaDtoResponse> ListarOportunidadFlujoCajaBandeja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaDal.ListarOportunidadFlujoCajaBandeja(flujocaja);
        }
        public List<ListaDtoResponse> ListaAnoMesProyecto(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaDal.ListaAnoMesProyecto(flujocaja);
        }
        public OportunidadFlujoCajaPaginadoDtoResponse ListarServiciosCircuitosPaginado(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaDal.ListarServiciosCircuitosPaginado(flujocaja);
        }
        public OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var oportunidadFlujoCajaPaginadoDtoResponse = new OportunidadFlujoCajaPaginadoDtoResponse();

            oportunidadFlujoCajaPaginadoDtoResponse.ListOportunidadFlujoCajaDtoResponse = iOportunidadFlujoCajaDal.ListarDetalleOportunidadCaratula(casonegocio);

            if (oportunidadFlujoCajaPaginadoDtoResponse.ListOportunidadFlujoCajaDtoResponse.Count > Numeric.Cero)
            {
                oportunidadFlujoCajaPaginadoDtoResponse.TotalItemCount = oportunidadFlujoCajaPaginadoDtoResponse.ListOportunidadFlujoCajaDtoResponse[0].TotalRow;
            }

            return oportunidadFlujoCajaPaginadoDtoResponse;
        }

        public OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadCaratulaTotal(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var oportunidadFlujoCajaPaginadoDtoResponse = new OportunidadFlujoCajaPaginadoDtoResponse();

            oportunidadFlujoCajaPaginadoDtoResponse.ListOportunidadFlujoCajaDtoResponse = iOportunidadFlujoCajaDal.ListarDetalleOportunidadCaratulaTotal(casonegocio);
      

            return oportunidadFlujoCajaPaginadoDtoResponse;
        }

        public OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var oportunidadFlujoCajaPaginadoDtoResponse = new OportunidadFlujoCajaPaginadoDtoResponse();

            oportunidadFlujoCajaPaginadoDtoResponse.ListOportunidadFlujoCajaDtoResponse = iOportunidadFlujoCajaDal.ListarDetalleOportunidadEcapex(casonegocio);

            if (oportunidadFlujoCajaPaginadoDtoResponse.ListOportunidadFlujoCajaDtoResponse.Count > Numeric.Cero)
            {
                oportunidadFlujoCajaPaginadoDtoResponse.TotalItemCount = oportunidadFlujoCajaPaginadoDtoResponse.ListOportunidadFlujoCajaDtoResponse[0].TotalRow;
            }

            return oportunidadFlujoCajaPaginadoDtoResponse;

        }
        public OportunidadFlujoCajaPaginadoDtoResponse ListarDetalleOportunidadEcapexTotal(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var oportunidadFlujoCajaPaginadoDtoResponse = new OportunidadFlujoCajaPaginadoDtoResponse();

            oportunidadFlujoCajaPaginadoDtoResponse.ListOportunidadFlujoCajaDtoResponse = iOportunidadFlujoCajaDal.ListarDetalleOportunidadEcapexTotal(casonegocio);
          
            return oportunidadFlujoCajaPaginadoDtoResponse;

        }
        public OportunidadFlujoCajaPaginadoDtoResponse ListarSubservicioPaginado(OportunidadFlujoCajaDtoRequest oportunidad)
        {
            return iOportunidadFlujoCajaDal.ListarSubservicioPaginado(oportunidad);
        }
        public OportunidadFlujoCajaDtoResponse ObtenerOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var oportunidadFlujoCaja = iOportunidadFlujoCajaDal.GetFilteredAsNoTracking(x => x.IdOportunidadLineaNegocio == flujocaja.IdOportunidadLineaNegocio &&
                                                                              x.IdFlujoCaja == flujocaja.IdFlujoCaja &&
                                                                              x.IdEstado == flujocaja.IdEstado);
            return oportunidadFlujoCaja.Select(x => new OportunidadFlujoCajaDtoResponse
            {
                IdAgrupador = x.IdAgrupador,
                IdTipoCosto = x.IdTipoCosto,
                Descripcion = x.Descripcion,
                IdPeriodos = x.IdPeriodos,
                IdPestana = x.IdPestana,
                IdGrupo = x.IdGrupo ,
                IdServicio=x.IdServicio,
                AgrupadorServicio = x.AgrupadorServicio
            }).Single();
        }
        public List<OportunidadFlujoCajaDtoResponse> ObtenerTodosOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var oportunidadFlujoCaja = iOportunidadFlujoCajaDal.GetFilteredAsNoTracking(x => x.IdOportunidadLineaNegocio == flujocaja.IdOportunidadLineaNegocio);

            var valida = oportunidadFlujoCaja.Any();
            if (valida)
            {

                return oportunidadFlujoCaja.Select(x => new OportunidadFlujoCajaDtoResponse
                {
                    IdAgrupador = x.IdAgrupador,
                    IdTipoCosto = x.IdTipoCosto,
                    Descripcion = x.Descripcion,
                    IdPeriodos = x.IdPeriodos,
                    IdPestana = x.IdPestana,
                    IdGrupo = x.IdGrupo
                }).ToList();
            }
            else
            {
                return null;
            }

        }
        public ProcesoResponse RegistrarComponente(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            DateTime fecha = DateTime.Now;

            PestanaGrupoDtoRequest objPestanaGrupoDtoRequest = new PestanaGrupoDtoRequest();
            OportunidadFlujoCajaConfiguracionDtoRequest objFujoCajaConfiguracionRequest = new OportunidadFlujoCajaConfiguracionDtoRequest();
            SubServicioDatosCaratulaDtoRequest objSubServicioDatosCaratulaDtoRequest = new SubServicioDatosCaratulaDtoRequest();
            SubServicioDatosCapexDtoRequest objSubServicioDatosCapexDtoRequest = new SubServicioDatosCapexDtoRequest();


            ServicioDtoRequest objServicioDtoRequest = new ServicioDtoRequest();

            OportunidadServicioCMIDtoRequest objOportunidadServicioCMIDtoRequest = new OportunidadServicioCMIDtoRequest();

            //if (flujocaja.IdServicio == null)
            //{

                objPestanaGrupoDtoRequest.IdGrupo = Convert.ToInt32(flujocaja.IdGrupo);
           
                var responsePestanaGrupo = iPestanaGrupoBl.ObtenerPestanaGrupo(objPestanaGrupoDtoRequest);
          
               

            if (flujocaja.IdFlujoCaja >Numeric.Cero)
            {
                flujocaja.IdEstado = Estados.Activo;
            var objAgrupador=    ObtenerOportunidadFlujoCaja(flujocaja);

                flujocaja.AgrupadorServicio = objAgrupador.AgrupadorServicio;
                flujocaja.IdServicio = objAgrupador.IdServicio;
                flujocaja.IdFlujoCaja = Numeric.Cero;
            }


                flujocaja.IdPestana = responsePestanaGrupo.IdPestana;
                flujocaja.FechaCreacion = fecha;
                flujocaja.IdEstado = Estados.Activo;
                var responseFlujoCaja = RegistrarOportunidadFlujoCaja(flujocaja);


                objFujoCajaConfiguracionRequest.IdFlujoCaja = responseFlujoCaja.Id;


                objFujoCajaConfiguracionRequest.Ponderacion = (flujocaja.Ponderacion == null || flujocaja.Ponderacion == Numeric.Cero)? 100: flujocaja.Ponderacion;
                objFujoCajaConfiguracionRequest.Inicio = (flujocaja.Inicio == null || flujocaja.Inicio == Numeric.Cero) ? 1 : flujocaja.Inicio;
                objFujoCajaConfiguracionRequest.IdEstado = Estados.Activo;
                objFujoCajaConfiguracionRequest.IdUsuarioCreacion = flujocaja.IdUsuarioCreacion;
                objFujoCajaConfiguracionRequest.FechaCreacion = fecha;


               var responseFlujoCajaConfiguracion = iOportunidadFlujoCajaConfiguracionBl.RegistrarOportunidadFlujoCajaConfiguracion(objFujoCajaConfiguracionRequest);


                if (responsePestanaGrupo.IdPestana == Numeric.Uno)
                {

                    objSubServicioDatosCaratulaDtoRequest.IdFlujoCaja = responseFlujoCaja.Id;
                    objSubServicioDatosCaratulaDtoRequest.IdTipoEnlace = Numeric.Uno;
                    objSubServicioDatosCaratulaDtoRequest.IdEstado = Estados.Activo;
                    objSubServicioDatosCaratulaDtoRequest.IdUsuarioCreacion = flujocaja.IdUsuarioCreacion;
                    objSubServicioDatosCaratulaDtoRequest.FechaCreacion = fecha;

                    iSubServicioDatosCaratulaBl.RegistrarSubServicioDatosCaratula(objSubServicioDatosCaratulaDtoRequest);

                }

               else
                {
                    objSubServicioDatosCapexDtoRequest.IdFlujoCaja = responseFlujoCaja.Id;
           
                    objSubServicioDatosCapexDtoRequest.IdEstado = Estados.Activo;
                    objSubServicioDatosCapexDtoRequest.IdUsuarioCreacion = flujocaja.IdUsuarioCreacion;
                    objSubServicioDatosCapexDtoRequest.FechaCreacion = fecha;

                    iSubServicioDatosCapexBl.RegistrarSubServicioDatosCapex(objSubServicioDatosCapexDtoRequest);


                }


        //}
         //else
         //   {
         //       objPestanaGrupoDtoRequest.IdGrupo = Convert.ToInt32(flujocaja.IdGrupo);
         //       var responsePestanaGrupo = iPestanaGrupoBl.ObtenerPestanaGrupo(objPestanaGrupoDtoRequest);
         //       objServicioDtoRequest.IdServicio = Convert.ToInt32(flujocaja.IdServicio);
         //       var responseServicio = iServicioBl.ObtenerServicio(objServicioDtoRequest);


         //       flujocaja.IdServicioCMI = responseServicio.IdServicioCMI;
         //       flujocaja.IdPestana = responsePestanaGrupo.IdPestana;
         //       flujocaja.FechaCreacion = fecha;
         //       flujocaja.IdEstado = Estados.Activo;

         //       var responseFlujoCaja = RegistrarOportunidadFlujoCaja(flujocaja);


         //       //objOportunidadServicioCMIDtoRequest.IdServicioCMI =Convert.ToInt32(responseServicio.IdServicioCMI);
         //       //objOportunidadServicioCMIDtoRequest.IdOportunidadLineaNegocio = flujocaja.IdLineaNegocio;
         //       //iOportunidadServicioCMIBl.RegistrarOportunidadServicioCMI(objOportunidadServicioCMIDtoRequest);



         //       if (flujocaja.IdPestana == Numeric.Uno)
         //       {

         //           objSubServicioDatosCaratulaDtoRequest.IdFlujoCaja = responseFlujoCaja.Id;
         //           objSubServicioDatosCaratulaDtoRequest.IdTipoEnlace = Numeric.Uno;
         //           objSubServicioDatosCaratulaDtoRequest.IdEstado = Estados.Activo;
         //           objSubServicioDatosCaratulaDtoRequest.IdUsuarioCreacion = flujocaja.IdUsuarioCreacion;
         //           objSubServicioDatosCaratulaDtoRequest.FechaCreacion = fecha;

         //           iSubServicioDatosCaratulaBl.RegistrarSubServicioDatosCaratula(objSubServicioDatosCaratulaDtoRequest);
         //       }
         //       else
         //       {
         //           objSubServicioDatosCapexDtoRequest.IdFlujoCaja = responseFlujoCaja.Id;
         //           objSubServicioDatosCapexDtoRequest.IdEstado = Estados.Activo;
         //           objSubServicioDatosCapexDtoRequest.IdUsuarioCreacion = flujocaja.IdUsuarioCreacion;
         //           objSubServicioDatosCapexDtoRequest.FechaCreacion = fecha;
         //           iSubServicioDatosCapexBl.RegistrarSubServicioDatosCapex(objSubServicioDatosCapexDtoRequest);
         //       }

           // }

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje ="Componente se registro con exito";
            return respuesta;
        }
        public ProcesoResponse RegistrarServicio(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            DateTime fecha = DateTime.Now;

            PestanaGrupoDtoRequest objPestanaGrupoDtoRequest = new PestanaGrupoDtoRequest();
            OportunidadFlujoCajaConfiguracionDtoRequest objFujoCajaConfiguracionRequest = new OportunidadFlujoCajaConfiguracionDtoRequest();
            SubServicioDatosCaratulaDtoRequest objSubServicioDatosCaratulaDtoRequest = new SubServicioDatosCaratulaDtoRequest();
            SubServicioDatosCapexDtoRequest objSubServicioDatosCapexDtoRequest = new SubServicioDatosCapexDtoRequest();


            ServicioDtoRequest objServicioDtoRequest = new ServicioDtoRequest();

            OportunidadServicioCMIDtoRequest objOportunidadServicioCMIDtoRequest = new OportunidadServicioCMIDtoRequest();

            //if (flujocaja.IdServicio == null)
            //{

            objPestanaGrupoDtoRequest.IdGrupo = Convert.ToInt32(flujocaja.IdGrupo);

            var responsePestanaGrupo = iPestanaGrupoBl.ObtenerPestanaGrupo(objPestanaGrupoDtoRequest);



            if (flujocaja.IdFlujoCaja > Numeric.Cero)
            {
                flujocaja.IdEstado = Estados.Activo;
                var objAgrupador = ObtenerOportunidadFlujoCaja(flujocaja);

                flujocaja.AgrupadorServicio = objAgrupador.AgrupadorServicio;
                flujocaja.IdServicio = objAgrupador.IdServicio;
                flujocaja.IdFlujoCaja = Numeric.Cero;
            }
            objServicioDtoRequest.IdServicio = Convert.ToInt32(flujocaja.IdServicio);
            var responseServicio = iServicioBl.ObtenerServicio(objServicioDtoRequest);

            flujocaja.IdPestana = responsePestanaGrupo.IdPestana;
            flujocaja.FechaCreacion = fecha;
            flujocaja.IdEstado = Estados.Activo;
            flujocaja.IdServicioCMI = responseServicio.IdServicioCMI;

          var responseFlujoCaja = RegistrarOportunidadFlujoCaja(flujocaja);


            
            objOportunidadServicioCMIDtoRequest.IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio;
            var responseServicioCMI=  iOportunidadServicioCMIBl.ObtenerTodosOportunidadServicioCMI(objOportunidadServicioCMIDtoRequest);
            if (responseServicioCMI == null)
            {
                objOportunidadServicioCMIDtoRequest.IdServicioCMI = Convert.ToInt32(responseServicio.IdServicioCMI);
                objOportunidadServicioCMIDtoRequest.IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio;
                objOportunidadServicioCMIDtoRequest.IdEstado = Estados.Activo;
                objOportunidadServicioCMIDtoRequest.IdUsuarioCreacion = flujocaja.IdUsuarioCreacion;
                objOportunidadServicioCMIDtoRequest.FechaCreacion = fecha;

                iOportunidadServicioCMIBl.RegistrarOportunidadServicioCMI(objOportunidadServicioCMIDtoRequest);
            }



            objFujoCajaConfiguracionRequest.IdFlujoCaja = responseFlujoCaja.Id;


            objFujoCajaConfiguracionRequest.Ponderacion = (flujocaja.Ponderacion == null || flujocaja.Ponderacion == Numeric.Cero) ? 100 : flujocaja.Ponderacion;
            objFujoCajaConfiguracionRequest.Inicio = (flujocaja.Inicio == null || flujocaja.Inicio == Numeric.Cero) ? 1 : flujocaja.Inicio;
            objFujoCajaConfiguracionRequest.IdEstado = Estados.Activo;
            objFujoCajaConfiguracionRequest.IdUsuarioCreacion = flujocaja.IdUsuarioCreacion;
            objFujoCajaConfiguracionRequest.FechaCreacion = fecha;


            var responseFlujoCajaConfiguracion = iOportunidadFlujoCajaConfiguracionBl.RegistrarOportunidadFlujoCajaConfiguracion(objFujoCajaConfiguracionRequest);






            if (responsePestanaGrupo.IdPestana == Numeric.Uno)
            {

                objSubServicioDatosCaratulaDtoRequest.IdFlujoCaja = responseFlujoCaja.Id;
                objSubServicioDatosCaratulaDtoRequest.IdTipoEnlace = Numeric.Uno;
                objSubServicioDatosCaratulaDtoRequest.IdEstado = Estados.Activo;
                objSubServicioDatosCaratulaDtoRequest.IdUsuarioCreacion = flujocaja.IdUsuarioCreacion;
                objSubServicioDatosCaratulaDtoRequest.FechaCreacion = fecha;

                iSubServicioDatosCaratulaBl.RegistrarSubServicioDatosCaratula(objSubServicioDatosCaratulaDtoRequest);

            }

            else
            {
                objSubServicioDatosCapexDtoRequest.IdFlujoCaja = responseFlujoCaja.Id;

                objSubServicioDatosCapexDtoRequest.IdEstado = Estados.Activo;
                objSubServicioDatosCapexDtoRequest.IdUsuarioCreacion = flujocaja.IdUsuarioCreacion;
                objSubServicioDatosCapexDtoRequest.FechaCreacion = fecha;

                iSubServicioDatosCapexBl.RegistrarSubServicioDatosCapex(objSubServicioDatosCapexDtoRequest);


            }




            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = "Servicio se registro con exito";
            return respuesta;
        }
        public ProcesoResponse RegistrarServicioComponente(ServicioSubServicioDtoRequest servicioSubServicio)
        {
           //ServicioSubServicioDtoResponse objServicioSubServicioResponse = new ServicioSubServicioDtoResponse();
            ServicioSubServicioDtoRequest objServicioSubServicioRequest = new ServicioSubServicioDtoRequest();
            var  objServicioResponse  = iServicioSubServicioDal.GetFilteredAsNoTracking(x => x.IdServicio == servicioSubServicio.IdServicio &&  x.IdSubServicio== null);


         var valor =  objServicioResponse.Select(x => new ServicioSubServicioDtoResponse
            {

                IdSubServicio = x.IdSubServicio,
                IdServicio = x.IdServicio,
                IdTipoCosto = x.IdTipoCosto,
                IdProveedor = x.IdProveedor,
                ContratoMarco = x.ContratoMarco,
                IdPeriodos = x.IdPeriodos,
                Inicio = x.Inicio,
                Ponderacion = x.Ponderacion,
                IdMoneda = x.IdMoneda,
                IdGrupo = x.IdGrupo,
                FlagSISEGO = x.FlagSISEGO

            }).Single();
            OportunidadFlujoCajaDtoRequest flujocajaServicio = new OportunidadFlujoCajaDtoRequest();
          

            flujocajaServicio.IdOportunidadLineaNegocio = servicioSubServicio.IdOportunidadLineaNegocio;
            flujocajaServicio.IdServicio = valor.IdServicio;
            flujocajaServicio.IdSubServicio = valor.IdSubServicio;
            flujocajaServicio.IdProveedor = valor.IdProveedor;
            flujocajaServicio.ContratoMarco = valor.ContratoMarco;
            flujocajaServicio.IdPeriodos = valor.IdPeriodos;
            flujocajaServicio.Inicio = valor.Inicio;
            flujocajaServicio.Ponderacion = valor.Ponderacion;
            flujocajaServicio.IdMoneda = valor.IdMoneda;
            flujocajaServicio.IdGrupo = valor.IdGrupo;
            flujocajaServicio.FlagSISEGO = valor.FlagSISEGO;
            flujocajaServicio.IdUsuarioCreacion = servicioSubServicio.IdUsuarioCreacion; 
             RegistrarServicio(flujocajaServicio);

            if(servicioSubServicio.ListServicioSelect !=null) { 
                                                           
            foreach (var componente in servicioSubServicio.ListServicioSelect)
            {

                objServicioSubServicioRequest.IdServicioSubServicio =Convert.ToInt32(componente.id);
                objServicioSubServicioRequest.IdEstado = Estados.Activo;
                var objComponenteResponse= iServicioSubServicioBl.ObtenerServicioSubServicio(objServicioSubServicioRequest);




            OportunidadFlujoCajaDtoRequest flujocajaComponente = new OportunidadFlujoCajaDtoRequest();

                //    flujocajaComponente.
                flujocajaComponente.IdOportunidadLineaNegocio = servicioSubServicio.IdOportunidadLineaNegocio;
                flujocajaComponente.IdServicio = objComponenteResponse.IdServicio;
                flujocajaComponente.IdSubServicio = objComponenteResponse.IdSubServicio;
                flujocajaComponente.IdProveedor = objComponenteResponse.IdProveedor;
                flujocajaComponente.ContratoMarco = objComponenteResponse.ContratoMarco;
                flujocajaComponente.IdPeriodos = objComponenteResponse.IdPeriodos;
                flujocajaComponente.Inicio = objComponenteResponse.Inicio;
                flujocajaComponente.Ponderacion = objComponenteResponse.Ponderacion;
                flujocajaComponente.IdMoneda = objComponenteResponse.IdMoneda;
                flujocajaComponente.IdGrupo = objComponenteResponse.IdGrupo;
                flujocajaComponente.FlagSISEGO = objComponenteResponse.FlagSISEGO;
                flujocajaComponente.IdUsuarioCreacion = servicioSubServicio.IdUsuarioCreacion;
                    RegistrarComponente(flujocajaComponente);

            }

            }
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = "Servicio se registro con exito";
            return respuesta;
        }
        public ProcesoResponse RegistrarCasoNegocioServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {

            //ServicioSubServicioDtoResponse objServicioSubServicioResponse = new ServicioSubServicioDtoResponse();
            foreach (var servicio in servicioSubServicio.ListServicioSelect)
            {
                int IdServicioSubServicio = Convert.ToInt32(servicio.id);


                ServicioSubServicioDtoRequest objServicioSubServicioRequest = new ServicioSubServicioDtoRequest();
                var objServicioSubServicioResponse = iServicioSubServicioDal.GetFilteredAsNoTracking(x => x.IdServicioSubServicio == IdServicioSubServicio).Single();
                int IdServicio =objServicioSubServicioResponse.IdServicio;

                var objServicioResponse = iServicioSubServicioDal.GetFilteredAsNoTracking(x => x.IdServicio == IdServicio && x.IdSubServicio == null).Single();

                OportunidadFlujoCajaDtoRequest flujocajaServicio = new OportunidadFlujoCajaDtoRequest();


                flujocajaServicio.IdOportunidadLineaNegocio = servicioSubServicio.IdOportunidadLineaNegocio;
                flujocajaServicio.IdServicio = objServicioResponse.IdServicio;
                flujocajaServicio.IdSubServicio = objServicioResponse.IdSubServicio;
                flujocajaServicio.IdProveedor = objServicioResponse.IdProveedor;
                flujocajaServicio.ContratoMarco = objServicioResponse.ContratoMarco;
                flujocajaServicio.IdPeriodos = objServicioResponse.IdPeriodos;
                flujocajaServicio.Inicio = objServicioResponse.Inicio;
                flujocajaServicio.Ponderacion = objServicioResponse.Ponderacion;
                flujocajaServicio.IdMoneda = objServicioResponse.IdMoneda;
                flujocajaServicio.IdGrupo = objServicioResponse.IdGrupo;
                flujocajaServicio.FlagSISEGO = objServicioResponse.FlagSISEGO;
                flujocajaServicio.IdUsuarioCreacion = servicioSubServicio.IdUsuarioCreacion;


                RegistrarServicio(flujocajaServicio);



                var objComponenteResponse = iServicioSubServicioDal.GetFilteredAsNoTracking(x => x.IdServicioSubServicio == IdServicioSubServicio).Single();

                if (objComponenteResponse != null)
                {
                    OportunidadFlujoCajaDtoRequest flujocajaComponente = new OportunidadFlujoCajaDtoRequest();

                    //    flujocajaComponente.
                    flujocajaComponente.IdOportunidadLineaNegocio = servicioSubServicio.IdOportunidadLineaNegocio;
                    flujocajaComponente.IdServicio = objComponenteResponse.IdServicio;
                    flujocajaComponente.IdSubServicio = objComponenteResponse.IdSubServicio;
                    flujocajaComponente.IdProveedor = objComponenteResponse.IdProveedor;
                    flujocajaComponente.ContratoMarco = objComponenteResponse.ContratoMarco;
                    flujocajaComponente.IdPeriodos = objComponenteResponse.IdPeriodos;
                    flujocajaComponente.Inicio = objComponenteResponse.Inicio;
                    flujocajaComponente.Ponderacion = objComponenteResponse.Ponderacion;
                    flujocajaComponente.IdMoneda = objComponenteResponse.IdMoneda;
                    flujocajaComponente.IdGrupo = objComponenteResponse.IdGrupo;
                    flujocajaComponente.FlagSISEGO = objComponenteResponse.FlagSISEGO;
                    flujocajaComponente.IdUsuarioCreacion = servicioSubServicio.IdUsuarioCreacion;
                    RegistrarComponente(flujocajaComponente);

                }


            }


       
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = "Plantilla se registro con exito";
            return respuesta;
        }
        public List<OportunidadFlujoCajaDtoResponse> GeneraCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaDal.GeneraCasoNegocio(casonegocio);
        }
        public List<OportunidadFlujoCajaDtoResponse> GeneraServicio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaDal.GeneraServicio(casonegocio);
        }
        public OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaDal.ObtenerDetalleOportunidadEcapex(casonegocio);
        }
        public OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaDal.ObtenerDetalleOportunidadCaratula(casonegocio);
        }
        public OportunidadFlujoCajaDtoResponse ObtenerSubServicio(OportunidadFlujoCajaDtoRequest oportunidad)
        {
            return iOportunidadFlujoCajaDal.ObtenerSubServicio(oportunidad);
        }
        public OportunidadFlujoCajaDtoResponse ObtenerServicioPorIdFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaDal.ObtenerServicioPorIdFlujoCaja(flujocaja);
        }
        #endregion
    }
}