﻿using System;

using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class ContactoBl: IContactoBl
    {
        readonly IContactoDal iContactoDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public ContactoBl(IContactoDal IContactoDal)
        {
            iContactoDal = IContactoDal;
        }

        #region - Transacionales -

        public ProcesoResponse RegistrarContacto(ContactoDtoRequest contacto)
        {
            try
            {
                var item = new Contacto
                {
                    IdSede = contacto.IdSede,
                    NombreApellidos = contacto.NombreApellidos,
                    NumeroTelefono = contacto.NumeroTelefono,
                    NumeroCelular = contacto.NumeroCelular,
                    CorreoElectronico = contacto.CorreoElectronico,
                    Cargo = contacto.Cargo,
                    IdEstado = contacto.IdEstado,
                    IdUsuarioCreacion = contacto.IdUsuarioCreacion,
                    FechaCreacion = contacto.FechaCreacion
                };

                iContactoDal.Add(item);
                iContactoDal.UnitOfWork.Commit();

                respuesta.Id = item.Id;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarContacto;
            }
            catch (Exception ex)
            {
                respuesta.Id = 0;
                respuesta.TipoRespuesta = Proceso.Invalido;
                respuesta.Mensaje = ex.Message.ToString();
                respuesta.Detalle = "Error";
            }

            return respuesta;
        }
        public ProcesoResponse ActualizarContacto(ContactoDtoRequest contacto)
        {
            var entidad = iContactoDal.Get(contacto.Id);
            entidad.NombreApellidos = contacto.NombreApellidos;
            entidad.NumeroTelefono = contacto.NumeroTelefono;
            entidad.NumeroCelular = contacto.NumeroCelular;
            entidad.CorreoElectronico = contacto.CorreoElectronico;
            entidad.Cargo = contacto.Cargo;
            entidad.IdUsuarioEdicion = contacto.IdUsuarioEdicion;
            entidad.FechaEdicion = contacto.FechaEdicion;

            iContactoDal.Modify(entidad);
            iContactoDal.UnitOfWork.Commit();

            respuesta.Id = entidad.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarContacto;
            return respuesta;
        }
        public ProcesoResponse InactivarContacto(ContactoDtoRequest contacto)
        {
            var entidad = iContactoDal.Get(contacto.Id);
            entidad.IdEstado = 0;
            entidad.IdUsuarioEdicion = contacto.IdUsuarioEdicion;
            entidad.FechaEdicion = contacto.FechaEdicion;

            iContactoDal.Modify(entidad);
            iContactoDal.UnitOfWork.Commit();

            respuesta.Id = entidad.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.InactivarContacto;
            return respuesta;
        }

        #endregion

        #region - No Transacionales -

        public ContactoPaginadoDtoResponse ListarContactoPaginado(ContactoDtoRequest contacto)
        {
            return iContactoDal.ListarContactoPaginado(contacto);
        }
        public ContactoDtoResponse ObtenerContactoPorId(ContactoDtoRequest contacto)
        {
            var entidad = iContactoDal.Get(contacto.Id);
            return new ContactoDtoResponse
            {
                Id = entidad.Id,
                NombreApellidos = entidad.NombreApellidos,
                NumeroTelefono = entidad.NumeroTelefono,
                NumeroCelular = entidad.NumeroCelular,
                CorreoElectronico = entidad.CorreoElectronico,
                Cargo = entidad.Cargo
            };
        }

        #endregion
    }
}