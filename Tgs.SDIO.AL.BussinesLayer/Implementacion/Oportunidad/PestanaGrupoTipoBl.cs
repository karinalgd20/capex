﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class PestanaGrupoTipoBl : IPestanaGrupoTipoBl
    {
        readonly IPestanaGrupoTipoDal iPestanaGrupoTipoDal;
        public PestanaGrupoTipoBl(IPestanaGrupoTipoDal IPestanaGrupoTipoDal)
        {
            iPestanaGrupoTipoDal = IPestanaGrupoTipoDal;
        }
        public List<ListaDtoResponse> ListarPestanaGrupoTipo(PestanaGrupoTipoDtoRequest pestanaGrupoTipo)
        {

            return iPestanaGrupoTipoDal.ListarPestanaGrupoTipo(pestanaGrupoTipo);
        }
    }
}
