﻿using System;
using System.Collections.Generic;
using System.Linq;

using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Util.Mensajes.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class ServicioSedeInstalacionBl: IServicioSedeInstalacionBl
    {
        readonly IServicioSedeInstalacionDal iServicioSedeInstalacionDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ServicioSedeInstalacionBl(IServicioSedeInstalacionDal IServicioSedeInstalacionDal)
        {
            iServicioSedeInstalacionDal = IServicioSedeInstalacionDal;
        }

        #region  - Transaccionales -
        public ProcesoResponse RegistrarServicioSedeInstalacion(ServicioSedeInstalacionDtoRequest servicioSedeInstalacion)
        {
            try
            {
                ServicioSedeInstalacion item = new ServicioSedeInstalacion();
                servicioSedeInstalacion.ListaServicios.ForEach(x =>
                {
                    item = new ServicioSedeInstalacion
                    {
                        IdSedeInstalacion = servicioSedeInstalacion.IdSedeInstalacion,
                        IdFlujoCaja = Convert.ToInt32(x.id),
                        IdEstado = servicioSedeInstalacion.IdEstado,
                        IdUsuarioCreacion = servicioSedeInstalacion.IdUsuarioCreacion,
                        FechaCreacion = servicioSedeInstalacion.FechaCreacion
                    };

                    iServicioSedeInstalacionDal.Add(item);
                });

                iServicioSedeInstalacionDal.UnitOfWork.Commit();

                respuesta.Id = item.Id;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarContacto;
            }
            catch (Exception ex)
            {
                respuesta.Id = 0;
                respuesta.TipoRespuesta = Proceso.Invalido;
                respuesta.Mensaje = ex.Message.ToString();
                respuesta.Detalle = "Error";
            }

            return respuesta;
        }
        #endregion

        #region - No Transacionales -
        #endregion
    }
}