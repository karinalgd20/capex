﻿using System.Linq;

using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.Util.Mensajes.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class VisitaBl : IVisitaBl
    {
        readonly IVisitaDal iVisitaDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public VisitaBl(IVisitaDal IVisitaDal)
        {
            iVisitaDal = IVisitaDal;
        }

        #region - Transaccionales -
        public ProcesoResponse RegistrarVisita(VisitaDtoRequest visita)
        {
            var entidad = new Visita
            {
                IdOportunidad = visita.IdOportunidad,
                PreVentaVisitante = visita.PreVentaVisitante,
                ClienteVisitado = visita.ClienteVisitado,
                IdTipo = visita.IdTipo,
                Fecha = visita.Fecha,
                HoraInicio = visita.HoraInicio,
                HoraFin = visita.HoraFin,
                IdEstado = visita.IdEstado,
                IdUsuarioCreacion = visita.IdUsuarioCreacion,
                FechaCreacion = visita.FechaCreacion
            };

            iVisitaDal.Add(entidad);
            iVisitaDal.UnitOfWork.Commit();

            respuesta.Id = entidad.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarVisita;
            return respuesta;
        }
        public ProcesoResponse ActualizarVisita(VisitaDtoRequest visita)
        {
            var entidad = iVisitaDal.Get(visita.Id);
            entidad.PreVentaVisitante = visita.PreVentaVisitante;
            entidad.ClienteVisitado = visita.ClienteVisitado;
            entidad.IdTipo = visita.IdTipo;
            entidad.Fecha = visita.Fecha;
            entidad.HoraInicio = visita.HoraInicio;
            entidad.HoraFin = visita.HoraFin;
            entidad.IdUsuarioEdicion = visita.IdUsuarioEdicion;
            entidad.FechaEdicion = visita.FechaEdicion;
            iVisitaDal.Modify(entidad);
            iVisitaDal.UnitOfWork.Commit();

            respuesta.Id = entidad.Id;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarVisita;
            return respuesta;
        }
        #endregion

        #region  - No Transaccionales -
        public VisitaDtoResponse ObtenerVisitaPorIdOportunidad(VisitaDtoRequest visita)
        {
            var entidad = iVisitaDal.GetFiltered(x => x.IdOportunidad.Equals(visita.IdOportunidad)).FirstOrDefault();
            if (entidad != null)
            {
                var visitaDto = new VisitaDtoResponse
                {
                    Id = entidad.Id,
                    IdOportunidad = entidad.IdOportunidad,
                    PreVentaVisitante = entidad.PreVentaVisitante,
                    ClienteVisitado = entidad.ClienteVisitado,
                    IdTipo = entidad.IdTipo,
                    Fecha = entidad.Fecha,
                    HoraInicio = entidad.HoraInicio,
                    HoraFin = entidad.HoraFin
                };

                return visitaDto;
            }

            return null;
        }
        #endregion
    }
}
