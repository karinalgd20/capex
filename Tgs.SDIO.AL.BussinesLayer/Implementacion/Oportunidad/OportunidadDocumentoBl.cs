﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadDocumentoBl : IOportunidadDocumentoBl
    {
        readonly IOportunidadDocumentoDal iOportunidadDocumentoDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadDocumentoBl(IOportunidadDocumentoDal IOportunidadDocumentoDal)
        {
            iOportunidadDocumentoDal = IOportunidadDocumentoDal;
        }

        public ProcesoResponse ActualizarOportunidadDocumento(OportunidadDocumentoDtoRequest oportunidadDocumento)
        {

            var objOportunidadDocumento = new OportunidadDocumento()
            {
                IdDocumento = oportunidadDocumento.IdDocumento,
                Descripcion = oportunidadDocumento.Descripcion,
                IdEstado = oportunidadDocumento.IdEstado,
                IdUsuarioEdicion = oportunidadDocumento.IdUsuarioEdicion,
                FechaEdicion = oportunidadDocumento.FechaEdicion
            };

            iOportunidadDocumentoDal.Modify(objOportunidadDocumento);
            iOportunidadDocumentoDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
        

            return respuesta;
        }


        public OportunidadDocumentoDtoResponse ObtenerOportunidadDocumento(OportunidadDocumentoDtoRequest oportunidadDocumento)
        {
            var objOportunidadDocumento = iOportunidadDocumentoDal.GetFiltered(x => x.IdDocumento == oportunidadDocumento.IdDocumento &&
                                                       x.IdEstado == oportunidadDocumento.IdEstado);
            return objOportunidadDocumento.Select(x => new OportunidadDocumentoDtoResponse
            {
               
                IdEstado = x.IdEstado
            }).Single();

        }
        public OportunidadDocumentoDtoResponse ObtenerDocumentoPorOportunidadLineaNegocio(OportunidadDocumentoDtoRequest oportunidadDocumento)
        {
            
            var objOportunidadDocumento = iOportunidadDocumentoDal.GetFiltered(x => x.IdOportunidadLineaNegocio == oportunidadDocumento.IdOportunidadLineaNegocio);
            var valida = objOportunidadDocumento.Any();
            if (valida) { 

            return objOportunidadDocumento.Select(x => new OportunidadDocumentoDtoResponse
            {

                IdOportunidadLineaNegocio=x.IdOportunidadLineaNegocio,
                IdFlujoCaja=x.IdFlujoCaja,
                TipoDocumento = x.TipoDocumento,
                RutaDocumento=x.RutaDocumento,
                Descripcion= x.Descripcion,
                IdTipoDocumento=x.IdTipoDocumento,
                IdEstado=x.IdEstado,
                IdUsuarioCreacion=x.IdUsuarioCreacion,
                FechaCreacion=x.FechaCreacion

            }).Single();
            }
            else
            {
                return null;
            }
        }
        public List<ListaDtoResponse> ListarOportunidadDocumento(OportunidadDocumentoDtoRequest oportunidadDocumento)
        {
            var query = iOportunidadDocumentoDal.GetFilteredAsNoTracking(x => x.IdEstado == oportunidadDocumento.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdDocumento.ToString(),
                Descripcion = x.Descripcion
            }).ToList();

        }
        public ProcesoResponse RegistrarOportunidadDocumento(OportunidadDocumentoDtoRequest oportunidadDocumento)
        {

            var objOportunidadDocumento = new OportunidadDocumento()
            {
                IdDocumento = oportunidadDocumento.IdDocumento,
                Descripcion = oportunidadDocumento.Descripcion,
                IdEstado = oportunidadDocumento.IdEstado,
                IdUsuarioCreacion = oportunidadDocumento.IdUsuarioCreacion,
                FechaCreacion = Convert.ToDateTime(oportunidadDocumento.FechaCreacion)
            };

            iOportunidadDocumentoDal.Add(objOportunidadDocumento);
            iOportunidadDocumentoDal.UnitOfWork.Commit();

            respuesta.Id = objOportunidadDocumento.IdDocumento;
            respuesta.TipoRespuesta = Proceso.Valido;
     
            return respuesta;
        }
    }
}
