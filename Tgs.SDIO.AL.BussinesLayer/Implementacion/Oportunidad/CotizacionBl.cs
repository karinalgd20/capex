﻿using System;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;



namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class CotizacionBl : ICotizacionBl
    {
        readonly ICotizacionDal iCotizacionDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public CotizacionBl(ICotizacionDal ICotizacionDal)
        {
            iCotizacionDal = ICotizacionDal;
        }
        public CotizacionPaginadoDtoResponse ListarCotizacionPaginado(CotizacionDtoRequest cotizacion)
        {
            return iCotizacionDal.ListarCotizacionPaginado(cotizacion);
        }
    }
}
