﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class ConceptoDatosCapexBl : IConceptoDatosCapexBl
    {
        readonly IConceptoDatosCapexDal iConceptoDatosCapexDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ConceptoDatosCapexBl(IConceptoDatosCapexDal IConceptoDatosCapexDal)
        {
            iConceptoDatosCapexDal = IConceptoDatosCapexDal;
        }

        public ProcesoResponse ActualizarConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex)
        {
          
                var objConcepto = new ConceptoDatosCapex()
                {
                    AnioCertificado = capex.AnioCertificado,
                    AnioComprometido = capex.AnioComprometido,
                    AnioRecupero = capex.AnioRecupero,
                    Cantidad = capex.Cantidad,
                    CapexDolares = capex.CapexDolares,
                    CapexSoles = capex.CapexSoles,
                    Circuito = capex.Circuito,
                    CostoUnitario = capex.CostoUnitario,
                    CostoUnitarioAntiguo = capex.CostoUnitarioAntiguo,
                    FechaEdicion = capex.FechaEdicion,
                    Garantizado = capex.Garantizado,
                    IdBWOportunidad = capex.IdBWOportunidad,
                    IdEstado = capex.IdEstado,
                    IdTipo = capex.IdTipo,
                    IdUsuarioEdicion = capex.IdUsuarioEdicion,
                    Medio = capex.Medio,
                    MesCertificado = capex.MesCertificado,
                    MesComprometido = capex.MesComprometido,
                    MesesAntiguedad = capex.MesesAntiguedad,
                    MesRecupero = capex.MesRecupero,
                    SISEGO = capex.SISEGO,
                    TotalCapex = capex.TotalCapex,
                    ValorResidualSoles = capex.ValorResidualSoles,
                };

                iConceptoDatosCapexDal.Modify(objConcepto);
                iConceptoDatosCapexDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (capex.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarConcepto : MensajesGeneralComun.ActualizarConcepto;
         
      


            return respuesta;
        }

        public List<ConceptoDatosCapexDtoResponse> ConceptosDatoCapex(ConceptoDatosCapexDtoRequest capex)
        {
            var lista = iConceptoDatosCapexDal.GetFilteredAsNoTracking(x => x.IdEstado == capex.IdEstado &&
                                                                             x.IdServicioConcepto == capex.IdServicioConcepto).ToList();
            return lista.Select(x => new ConceptoDatosCapexDtoResponse
            {
                AEReducido = x.AEReducido,
                AnioCertificado = x.AnioCertificado,
                AnioComprometido = x.AnioComprometido,
                AnioRecupero = x.AnioRecupero,
                MesesAntiguedad = x.Antiguedad,
                Cantidad = x.Cantidad,
                CapexDolares = x.CapexDolares,
                CapexInstalacion = x.CapexInstalacion,
                CapexSoles = x.CapexSoles,
                Circuito = x.Circuito,
                Combo = x.Combo,
                CostoUnitario = x.CostoUnitario,
                CostoUnitarioAntiguo = x.CostoUnitarioAntiguo,
                Cruce = x.Cruce,
                Garantizado = x.Garantizado,
                IdBWOportunidad = x.IdBWOportunidad,
                IdConceptoDatosCapex = x.IdConceptoDatosCapex,
                IdEstado = x.IdEstado,
                IdServicioConcepto = x.IdServicioConcepto,
                IdTipo = x.IdTipo,
                Marca = x.Marca,
                Medio = x.Medio,
                MesCertificado = x.MesCertificado,
                MesComprometido = x.MesComprometido,
                MesRecupero = x.MesRecupero,
                Modelo = x.Modelo,
                SISEGO = x.SISEGO,
                TipoEquipo = x.Tipo,
                TotalCapex = x.TotalCapex,
                ValorResidualSoles = x.ValorResidualSoles
            }).ToList();
        }

        public List<ConceptoDatosCapexDtoResponse> ListarConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex)
        {
            capex.IdPestana = Plantilla.ECapex;
            capex.IdEstado = Estados.Activo;
            
            return iConceptoDatosCapexDal.ListarConceptoDatoCapex(capex);
        }

        public ConceptoDatosCapexDtoResponse ObtenerConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex)
        {
            var lista = iConceptoDatosCapexDal.GetFilteredAsNoTracking(x => x.IdEstado == capex.IdEstado &&
                                                                           x.IdServicioConcepto == capex.IdServicioConcepto).ToList();
            return lista.Select(x => new ConceptoDatosCapexDtoResponse
            {
                AEReducido = x.AEReducido,
                AnioCertificado = x.AnioCertificado,
                AnioComprometido = x.AnioComprometido,
                AnioRecupero = x.AnioRecupero,
                MesesAntiguedad = x.Antiguedad,
                Cantidad = x.Cantidad,
                CapexDolares = x.CapexDolares,
                CapexInstalacion = x.CapexInstalacion,
                CapexSoles = x.CapexSoles,
                Circuito = x.Circuito,
                Combo = x.Combo,
                CostoUnitario = x.CostoUnitario,
                CostoUnitarioAntiguo = x.CostoUnitarioAntiguo,
                Cruce = x.Cruce,
                Garantizado = x.Garantizado,
                IdBWOportunidad = x.IdBWOportunidad,
                IdConceptoDatosCapex = x.IdConceptoDatosCapex,
                IdEstado = x.IdEstado,
                IdServicioConcepto = x.IdServicioConcepto,
                IdTipo = x.IdTipo,
                Marca = x.Marca,
                Medio = x.Medio,
                MesCertificado = x.MesCertificado,
                MesComprometido = x.MesComprometido,
                MesRecupero = x.MesRecupero,
                Modelo = x.Modelo,
                SISEGO = x.SISEGO,
                TipoEquipo = x.Tipo,
                TotalCapex = x.TotalCapex,
                ValorResidualSoles = x.ValorResidualSoles
            }).Single();
        }

        public ProcesoResponse RegistrarConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex)
        {
            try
            {
                if (capex.IdConceptoDatosCapex != Numeric.Cero)
                {
                    respuesta = ActualizarConceptoDatoCapex(capex);
                }
                else
                {
                    var objConcepto = new ConceptoDatosCapex()
                    {
                        AnioCertificado = capex.AnioCertificado,
                        AnioComprometido = capex.AnioComprometido,
                        AnioRecupero = capex.AnioRecupero,
                        Cantidad = capex.Cantidad,
                        CapexDolares = capex.CapexDolares,
                        CapexSoles = capex.CapexSoles,
                        Circuito = capex.Circuito,
                        CostoUnitario = capex.CostoUnitario,
                        CostoUnitarioAntiguo = capex.CostoUnitarioAntiguo,
                        FechaEdicion = capex.FechaEdicion,
                        Garantizado = capex.Garantizado,
                        IdBWOportunidad = capex.IdBWOportunidad,
                        IdEstado = capex.IdEstado,
                        IdTipo = capex.IdTipo,
                        IdUsuarioEdicion = capex.IdUsuarioEdicion,
                        Medio = capex.Medio,
                        MesCertificado = capex.MesCertificado,
                        MesComprometido = capex.MesComprometido,
                        MesesAntiguedad = capex.MesesAntiguedad,
                        MesRecupero = capex.MesRecupero,
                        SISEGO = capex.SISEGO,
                        TotalCapex = capex.TotalCapex,
                        ValorResidualSoles = capex.ValorResidualSoles,
                    };

                    iConceptoDatosCapexDal.Add(objConcepto);
                    iConceptoDatosCapexDal.UnitOfWork.Commit();

                    respuesta.Id = objConcepto.IdConceptoDatosCapex;
                    respuesta.TipoRespuesta = Proceso.Valido;
                    respuesta.Mensaje = MensajesGeneralComun.RegistrarConcepto;
                }
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, null, Generales.Sistemas.Oportunidad);
            }

            return respuesta;
        }

        public ProcesoResponse RegistrarEstudiosEspeciales(ProyectoServicioConceptoDtoRequest servicio, ConceptoDatosCapexDtoRequest capex)
        {
            throw new NotImplementedException();
        }
    }
}
