﻿using System;
using System.Collections.Generic;
using Tgs.SDIO.AL.ProxyServicio.WsUsuarioPerfil;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.AgenteServicio
{
    public static class AgenteServicioUsuarioPerfil
    {
        public static ProcesoResponse ActualizarEstadoUsuarioPerfil(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioPerfilSoapClient())
            {
                var usuarioProxy = new BEUsuarioPerfil
                {
                    IdUsuarioSistemaEmpresa = usuarioPerfilDtoRequest.IdUsuarioSistemaEmpresa,
                    IdPerfil = usuarioPerfilDtoRequest.IdPerfil,
                    IdEstadoRegistro = Convert.ToInt32(usuarioPerfilDtoRequest.IdEstado),
                    IdUsuarioModifica= usuarioPerfilDtoRequest.IdUsuarioEdicion,
                    IdUsuarioRegistro = usuarioPerfilDtoRequest.IdUsuarioCreacion,
                    FechaRegistro = usuarioPerfilDtoRequest.FechaCreacion,
                    FechaModifica = usuarioPerfilDtoRequest.FechaEdicion
                };

                return proxy.ActualizarEstadoUsuarioPerfil(usuarioProxy);
            }
        } 

        public static List<BEPerfil> ListarPerfilesPorSistema()
        {
            using (var proxy = new RAIS_UsuarioPerfilSoapClient())
            { 
                return proxy.ListarPerfilesPorSistema(Configuracion.CodigoAplicacion);
            }
        }

        public static List<BEUsuarioPerfil> ListarPerfilesAsignados(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioPerfilSoapClient())
            {
                var usuarioPerfilProxy = new BEUsuarioPerfil
                {
                    IdUsuarioAsignado = usuarioPerfilDtoRequest.IdUsuarioAsignado,
                    IdEstadoRegistro = Convert.ToInt32(usuarioPerfilDtoRequest.IdEstado),
                    Pagina = new Pagina
                    {
                        NroPagina = usuarioPerfilDtoRequest.NroPagina,
                        RegistrosPagina = usuarioPerfilDtoRequest.RegistrosPagina
                    }
                };  

                return proxy.ListarPerfilesAsignados(usuarioPerfilProxy);
            }
        }

        public static ProcesoResponse RegistrarUsuarioPerfil(UsuarioPerfilDtoRequest usuarioPerfilDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioPerfilSoapClient())
            {
                var usuarioProxy = new BEUsuarioPerfil
                {
                    IdUsuarioSistemaEmpresa = usuarioPerfilDtoRequest.IdUsuarioSistemaEmpresa,
                    IdPerfil = usuarioPerfilDtoRequest.IdPerfil,
                    IdEstadoRegistro = Convert.ToInt32(usuarioPerfilDtoRequest.IdEstado),
                    IdUsuarioRegistro = usuarioPerfilDtoRequest.IdUsuarioCreacion,
                    IdUsuarioModifica = usuarioPerfilDtoRequest.IdUsuarioEdicion,
                    FechaModifica= usuarioPerfilDtoRequest.FechaEdicion,
                    FechaRegistro = usuarioPerfilDtoRequest.FechaCreacion
                };

                return  proxy.RegistrarUsuarioPerfil(usuarioProxy);
            }
        }

    }
}
