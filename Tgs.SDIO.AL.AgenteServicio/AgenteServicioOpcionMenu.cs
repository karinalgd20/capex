﻿using System.Data;
using Tgs.SDIO.AL.ProxyServicio.WsRaisAccesoSistema;

namespace Tgs.SDIO.AL.AgenteServicio
{
    public static class AgenteServicioOpcionMenu
    {
        public static DataSet ListarOpcionesUsuario(int idUsuarioEmpresa)
        {
            using (var proxy = new RAIS_AccesoSoapClient())
            {
                return proxy.ListarOpciones_X_UsuarioSistema(idUsuarioEmpresa);
            }
        }
    }
}
