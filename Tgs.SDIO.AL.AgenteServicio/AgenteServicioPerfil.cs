﻿using System.Data;
using Tgs.SDIO.AL.ProxyServicio.WsRaisAccesoSistema;

namespace Tgs.SDIO.AL.AgenteServicio
{
    public static class AgenteServicioPerfil
    {
        public static DataSet ObtenerPerfiles(int idUsuarioSistema)
        {
            using (var proxy = new RAIS_AccesoSoapClient())
            {
                return proxy.ListarPerfiles(idUsuarioSistema);
            }
        }
    }
}
