﻿using System;
using System.Data;
using Tgs.SDIO.AL.ProxyServicio.WsRaisAccesoSistema;
using Tgs.SDIO.AL.ProxyServicio.WsRaisUsuario;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.AgenteServicio
{
    public static class AgenteServicioUsuario
    {
        public static ProxyServicio.WsRaisAccesoSistema.BEUsuario ObtenerUsuarioPorLogin(string login)
        {
            using (var proxy = new RAIS_AccesoSoapClient())
            {
                return proxy.ObtenerUsuario(login);
            }
        }

        public static ProxyServicio.WsRaisUsuario.BEUsuario ObtenerUsuarioPorId(int idUsuario)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                return proxy.ObtenerUsuarioPorId(idUsuario);
            }
        }

        public static DataSet ValidarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_AccesoSoapClient())
            {
                return proxy.ValidarInicioSesionSecundario(
                    Configuracion.CodigoAplicacion,
                    usuarioDtoRequest.Login,
                    usuarioDtoRequest.Password,
                    usuarioDtoRequest.NroIntentos, 
                    Configuracion.CodigoEmpresa);
            }
        } 

        public static string EnvioClaveUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                return proxy.EnvioClaveUsuario(usuarioDtoRequest.Login,
                    usuarioDtoRequest.CorreoElectronico,
                    usuarioDtoRequest.UsuarioEnvia);
            }
        }

        public static BEEntidad[] ObtenerAmbitoUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                return proxy.ObtenerAmbitoUsuario(usuarioDtoRequest.Login, usuarioDtoRequest.CodigoSistema);
            }
        }

        public static ProcesoResponse RegistrarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                var usuarioProxy = new ProxyServicio.WsRaisUsuario.BEUsuario
                {
                    Login = usuarioDtoRequest.Login,
                    Password = usuarioDtoRequest.Password,
                    Apellidos_Usuario = usuarioDtoRequest.Apellidos,
                    Nombre_Usuario = usuarioDtoRequest.Nombres,
                    CIP_Usuario = usuarioDtoRequest.CodigoCip,
                    Tipo_Usuario = usuarioDtoRequest.TipoUsuario,
                    IdEmpresa = Configuracion.CodigoEmpresa,
                    FH_Registro = usuarioDtoRequest.FechaCreacion,
                    IdEstadoRegistro = Convert.ToInt32(usuarioDtoRequest.IdEstado),
                    IdUsuario_Registro = usuarioDtoRequest.IdUsuarioCreacion,
                    Correo_Electronico = usuarioDtoRequest.CorreoElectronico,
                    Fecha_Caducidad = usuarioDtoRequest.FechaCaducidad,
                    Numero_Intentos_Fallidos = usuarioDtoRequest.IntentosFallidos,
                    Flag_Solicitar_Cambio_Clave= usuarioDtoRequest.SolicitaCambioClave
                };
              
                var usuarioSistemaEmpresaProxy = new BEUsuarioSistemaEmpresa
                {
                    SistemaEmpresa = new BESistemaEmpresa {
                        IdEmpresa = Configuracion.CodigoEmpresa,
                        IdSistema = Configuracion.IdSistema
                    },
                    IdTurno = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.IdTurno,
                    FechaInicio = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.FechaInicio,
                    FechaFin = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.FechaFin,
                    EstadoRegistro = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.EstadoRegistro,
                    IdUsuarioRegistra = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.IdUsuarioRegistra,
                    FechaRegistro = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.FechaRegistro,
                    FechaModifica = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.FechaRegistro
                };

                return proxy.RegistrarUsuarioSistema(usuarioProxy, usuarioSistemaEmpresaProxy); 
            }
        }

        public static ProcesoResponse ActualizarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                var usuarioProxy = new ProxyServicio.WsRaisUsuario.BEUsuario
                {
                    IdUsuario= usuarioDtoRequest.IdUsuario,
                    Apellidos_Usuario = usuarioDtoRequest.Apellidos,
                    Nombre_Usuario = usuarioDtoRequest.Nombres,
                    Correo_Electronico = usuarioDtoRequest.CorreoElectronico,                    
                    Numero_Intentos_Fallidos = usuarioDtoRequest.IntentosFallidos,
                    Fecha_Caducidad = usuarioDtoRequest.FechaCaducidad,
                    Flag_Solicitar_Cambio_Clave = usuarioDtoRequest.SolicitaCambioClave,
                    IdUsuario_Modifica = usuarioDtoRequest.IdUsuarioEdicion,
                    IdEstadoRegistro=Convert.ToInt32(usuarioDtoRequest.IdEstado)
                }; 

                return proxy.ActualizarUsuario(usuarioProxy);
            }
        }

        public static DataSet CambiarPassword(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                return proxy.CambiarPassword(usuarioDtoRequest.Login, usuarioDtoRequest.Password, usuarioDtoRequest.NuevoPassword);
            }
        }

        public static DataSet ObtenerUsuariosPorPerfil(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_AccesoSoapClient())
            {
                return proxy.ListarUsuarios_X_Perfil(usuarioDtoRequest.CodigoSistema, usuarioDtoRequest.CodigoPerfil, usuarioDtoRequest.IdEmpresa);
            }
        }

        public static ProcesoResponse ActualizarEstadoUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                var usuarioProxy = new ProxyServicio.WsRaisUsuario.BEUsuario
                {
                    IdUsuario = usuarioDtoRequest.IdUsuario, 
                    IdEstadoRegistro = Convert.ToInt32(usuarioDtoRequest.IdEstado),
                    IdUsuario_Modifica = usuarioDtoRequest.IdUsuarioEdicion
                };

                return proxy.ActualizarEstadoUsuario(usuarioProxy);
            }
        }

        public static ProxyServicio.WsRaisUsuario.BEUsuario[] ListarUsuariosPaginado(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                var usuarioProxy = new ProxyServicio.WsRaisUsuario.BEUsuario
                {
                    
                    Apellidos_Usuario = usuarioDtoRequest.Apellidos,
                    Nombre_Usuario = usuarioDtoRequest.Nombres,
                    Login = usuarioDtoRequest.Login,
                    CodigoSistema = Configuracion.CodigoAplicacion,
                    IdEmpresa = usuarioDtoRequest.IdEmpresa, 
                    Pagina = new ProxyServicio.WsRaisUsuario.Pagina {
                        NroPagina= usuarioDtoRequest.NroPagina,
                        RegistrosPagina= usuarioDtoRequest.RegistrosPagina
                    }                   
                };

                return proxy.ListarUsuariosPaginado(usuarioProxy);
            }
        }

        public static ProxyServicio.WsRaisUsuario.BEUsuario[] ListarUsuariosFiltro(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                var usuarioProxy = new ProxyServicio.WsRaisUsuario.BEUsuario
                { 
                    Apellidos_Usuario = usuarioDtoRequest.Nombres,                  
                    CodigoSistema = usuarioDtoRequest.CodigoSistema
                };

                return proxy.ListarUsuariosFiltro(usuarioProxy);
            }
        }

    }
}
